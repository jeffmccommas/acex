"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var datacustodianscopeselection_service_1 = require("./datacustodianscopeselection.service");
var app_service_1 = require("../app.service");
var DataCustodianScopeSelection = (function () {
    function DataCustodianScopeSelection(dataCustodianScopeSelectionService, appService, elementRef) {
        this.enableLoader = true;
        this.elementRef = elementRef;
        this.parameters = "params";
        this.tabKey = "1";
        this.errorMessage = "error";
        this.dataCustodianScopeSelectionService = dataCustodianScopeSelectionService;
        this.appService = appService;
        this.fromUTest = false;
        if (this.dataCustodianScopeSelectionService.res !== undefined) {
            this.result = dataCustodianScopeSelectionService.res;
            this.fromUTest = this.dataCustodianScopeSelectionService.fromUnitTest;
        }
    }
    DataCustodianScopeSelection.prototype.ngOnInit = function () {
    };
    DataCustodianScopeSelection.prototype.loadDataCustodianScopeSelection = function () {
        var _this = this;
        this.result = {};
        this.dataCustodianScopeSelectionService.GetDataCustodianScopeSelection(this.tabKey, this.parameters)
            .subscribe(function (res) { return _this.result = res; }, function (err) { return _this.appService.logError(_this, err); });
        this.enableLoader = false;
    };
    DataCustodianScopeSelection.prototype.updateScopes = function () {
    };
    DataCustodianScopeSelection = __decorate([
        core_1.Component({
            selector: 'datacustodianscopeselection',
            template: "<h1>Data Custodian (Aclara) Scope Selection Page</h1><p>The Retail Customer is redirect to this page where they can select which scopes (data types) they would like to share with the Third Party application.</p>",
            providers: [datacustodianscopeselection_service_1.DataCustodianScopeSelectionService, app_service_1.AppService]
        }),
        __metadata("design:paramtypes", [datacustodianscopeselection_service_1.DataCustodianScopeSelectionService, app_service_1.AppService, core_1.ElementRef])
    ], DataCustodianScopeSelection);
    return DataCustodianScopeSelection;
}());
exports.DataCustodianScopeSelection = DataCustodianScopeSelection;

//# sourceMappingURL=datacustodianscopeselection.js.map
