﻿import "rxjs/add/operator/map";
import { Injectable, Input } from "@angular/core"
import { Http } from "@angular/http";

@Injectable()
export class DataCustodianScopeSelectionService {
    @Input() res: Object;
    @Input() fromUnitTest: Boolean;
    http: Http;
    constructor(http: Http) {
        this.http = http;
    }

    GetDataCustodianScopeSelection(tabKey, params) {
        return this.http.get(`./DataCustodianScopeSelection/Get?tabKey=${tabKey}&parameters=${window.btoa(params)}`).map(res => res.json());
    }

}

