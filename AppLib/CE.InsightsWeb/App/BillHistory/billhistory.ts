﻿import { Component, ElementRef, OnInit } from "@angular/core";
import { BillHistoryService} from "./billhistory.service";
import { AppService, IAppParams } from "../app.service";

@Component({
    selector: 'billhistory',
    templateUrl: 'App/BillHistory/billhistory.html',
    providers: [BillHistoryService, AppService]
})
export class BillHistory implements OnInit {
    static fromUnitTest = "unittest";
    elementRef: ElementRef;
    result: any;
    bills: any;
    rowCount: any;
    paginate: Boolean;
    pages: any;
    currentPage: number;
    totalPages: number;
    nextDisabled: Boolean;
    prevDisabled: Boolean;
    error: Object;
    enableLoader: Boolean;
    errorOccurred: Boolean;
    errorMessage: string;
    billhistoryService: BillHistoryService;
    appService: AppService;
    params: Array<IAppParams>;
    parameters: string;
    tabKey: string;
    fromUTest: Boolean;

    constructor(billhistoryService: BillHistoryService, appService: AppService, elementRef: ElementRef) {
        this.enableLoader = true;
        this.elementRef = elementRef;
        this.params = appService.getParams();
        this.parameters = this.params[0].parameters;
        this.tabKey = this.params[0].tabKey;
        this.errorMessage = this.params[0].errorMessage;
        this.billhistoryService = billhistoryService;
        this.appService = appService;
        this.prevDisabled = true;

        if (this.billhistoryService.res !== undefined) {
            this.result = billhistoryService.res;
            this.fromUTest = this.billhistoryService.fromUnitTest;
        }
    }

    ngOnInit() {

        if (this.billhistoryService.getBillHistory("", "").toString() !== BillHistory.fromUnitTest) {
            this.loadbillhistory();
        }
    }

    loadbillhistory() {
        this.result = {};
        this.billhistoryService.getBillHistory(this.tabKey, this.parameters)
                .subscribe(res => this.result = res,
                err => this.appService.logError(this, err),
                ()=>this.getBills(0)
                );
        
        this.enableLoader = false;
        this.currentPage = 0;
    }

    getBills(page) {
        this.bills = {};
        this.rowCount = parseInt(this.result.RowCount);
        var start = page * this.rowCount;
        this.bills = this.result.Bills.slice(start,(start+this.rowCount));
        if (this.result.Bills.length > (this.rowCount+1)) {
            this.paginate = true;
        }
        if (this.currentPage===0) {
            this.generatePages();
        }
    }

    generatePages() {
        //Calculating no. of pages for bills
        var numItems = this.result.Bills.length - 1;
        this.totalPages = Math.ceil(numItems / this.rowCount);
        this.pages = [];
        for (var i = 0; i < this.totalPages; i++) {
            this.pages.push(i + 1);
        }
    }

    pageChange(direction) {
        if (direction==="prev") {
            if (this.currentPage>0) {
                this.nextDisabled = false;
                this.currentPage = this.currentPage - 1;
                this.getBills(this.currentPage);
            }
            if (this.currentPage===0) {
                this.prevDisabled = true;
            }
        }
        if (direction==="next") {
            if (this.currentPage < (this.totalPages - 1)) {
                this.prevDisabled = false;
                this.currentPage = this.currentPage + 1;
                this.getBills(this.currentPage);
            }
            if (this.currentPage===(this.totalPages-1)) {
                this.nextDisabled = true;
            }
        }
        if (direction!=="prev" && direction!=="next")
        {
            this.currentPage = parseInt(direction);
            if (this.currentPage===1) {
                this.prevDisabled = true;
                this.nextDisabled = false;
            } else {
                this.prevDisabled = false;
                
            }
            if (this.currentPage===this.totalPages) {
                this.nextDisabled = true;
                this.prevDisabled = false;
            } else {
                this.nextDisabled = false;
            }
            this.currentPage = this.currentPage - 1;
            this.getBills(this.currentPage);
        }
    }

    isCurrentPage(p) {
        if (p===(this.currentPage+1)) {
            return true;
        }
        return false;
    }

    isLastBill(bill) {
        if (JSON.stringify(this.result.Bills[this.result.Bills.length-1])===JSON.stringify(bill)) {
            return true;
        } else {
            return false;
        }
    }
}