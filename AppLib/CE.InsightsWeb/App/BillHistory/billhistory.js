"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var billhistory_service_1 = require("./billhistory.service");
var app_service_1 = require("../app.service");
var BillHistory = (function () {
    function BillHistory(billhistoryService, appService, elementRef) {
        this.enableLoader = true;
        this.elementRef = elementRef;
        this.params = appService.getParams();
        this.parameters = this.params[0].parameters;
        this.tabKey = this.params[0].tabKey;
        this.errorMessage = this.params[0].errorMessage;
        this.billhistoryService = billhistoryService;
        this.appService = appService;
        this.prevDisabled = true;
        if (this.billhistoryService.res !== undefined) {
            this.result = billhistoryService.res;
            this.fromUTest = this.billhistoryService.fromUnitTest;
        }
    }
    BillHistory_1 = BillHistory;
    BillHistory.prototype.ngOnInit = function () {
        if (this.billhistoryService.getBillHistory("", "").toString() !== BillHistory_1.fromUnitTest) {
            this.loadbillhistory();
        }
    };
    BillHistory.prototype.loadbillhistory = function () {
        var _this = this;
        this.result = {};
        this.billhistoryService.getBillHistory(this.tabKey, this.parameters)
            .subscribe(function (res) { return _this.result = res; }, function (err) { return _this.appService.logError(_this, err); }, function () { return _this.getBills(0); });
        this.enableLoader = false;
        this.currentPage = 0;
    };
    BillHistory.prototype.getBills = function (page) {
        this.bills = {};
        this.rowCount = parseInt(this.result.RowCount);
        var start = page * this.rowCount;
        this.bills = this.result.Bills.slice(start, (start + this.rowCount));
        if (this.result.Bills.length > (this.rowCount + 1)) {
            this.paginate = true;
        }
        if (this.currentPage === 0) {
            this.generatePages();
        }
    };
    BillHistory.prototype.generatePages = function () {
        var numItems = this.result.Bills.length - 1;
        this.totalPages = Math.ceil(numItems / this.rowCount);
        this.pages = [];
        for (var i = 0; i < this.totalPages; i++) {
            this.pages.push(i + 1);
        }
    };
    BillHistory.prototype.pageChange = function (direction) {
        if (direction === "prev") {
            if (this.currentPage > 0) {
                this.nextDisabled = false;
                this.currentPage = this.currentPage - 1;
                this.getBills(this.currentPage);
            }
            if (this.currentPage === 0) {
                this.prevDisabled = true;
            }
        }
        if (direction === "next") {
            if (this.currentPage < (this.totalPages - 1)) {
                this.prevDisabled = false;
                this.currentPage = this.currentPage + 1;
                this.getBills(this.currentPage);
            }
            if (this.currentPage === (this.totalPages - 1)) {
                this.nextDisabled = true;
            }
        }
        if (direction !== "prev" && direction !== "next") {
            this.currentPage = parseInt(direction);
            if (this.currentPage === 1) {
                this.prevDisabled = true;
                this.nextDisabled = false;
            }
            else {
                this.prevDisabled = false;
            }
            if (this.currentPage === this.totalPages) {
                this.nextDisabled = true;
                this.prevDisabled = false;
            }
            else {
                this.nextDisabled = false;
            }
            this.currentPage = this.currentPage - 1;
            this.getBills(this.currentPage);
        }
    };
    BillHistory.prototype.isCurrentPage = function (p) {
        if (p === (this.currentPage + 1)) {
            return true;
        }
        return false;
    };
    BillHistory.prototype.isLastBill = function (bill) {
        if (JSON.stringify(this.result.Bills[this.result.Bills.length - 1]) === JSON.stringify(bill)) {
            return true;
        }
        else {
            return false;
        }
    };
    BillHistory.fromUnitTest = "unittest";
    BillHistory = BillHistory_1 = __decorate([
        core_1.Component({
            selector: 'billhistory',
            template: "<Loader *ngIf=\"enableLoader\"></Loader><div *ngIf=\"!enableLoader\"><div *ngIf=\"errorOccurred\"><div class=\"panel panel-default\"><div class=\"alert alert-danger\"><span class=\"glyphicon glyphicon-exclamation-sign xs-mr-10\" aria-hidden=\"true\"></span><strong>{{errorMessage}}</strong> <span class=\"sr-only\">Error:</span></div></div></div><div *ngIf=\"!errorOccurred\" class=\"panel panel-default\"><div *ngIf=\"result.ShowTitle == 'true' && result.Title !== ''\" class=\"panel-heading\"><h1 class=\"panel-title\" role=\"presentation\">{{result.Title}}</h1></div><div *ngIf=\"result.NoBills == true\" class=\"alert alert-warning\"><span class=\"glyphicon glyphicon-alert xs-mr-10\"></span> <strong>{{result.NoBillsText}}</strong></div><div *ngIf=\"result.ShowIntro == 'true' && result.ShowIntro !== ''\" class=\"intro-text\" style=\"white-space: pre-line\">{{result.IntroText}}</div><div *ngIf=\"result.NoBills !== true\" class=\"table table-striped table-responsive xs-mb-0\" role=\"grid\"><div class=\"billing-history table-responsive\" role=\"grid\"><table class=\"table table-hover table-striped billing-history\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\"><thead><tr><th class=\"navbar navbar-default navbar-table-cell\">{{result.BillDate}}</th><th *ngIf=\"result.ElectricServiceCount > 0 && result.Commodities.indexOf(('electric')) !== -1\" class=\"navbar navbar-default navbar-table-cell\">{{result.ElectricAmountDue}}</th><th *ngIf=\"result.WaterServiceCount > 0 && result.Commodities.indexOf(('water')) !== -1\" class=\"navbar navbar-default navbar-table-cell\">{{result.WaterAmountDue}}</th><th *ngIf=\"result.GasServiceCount > 0 && result.Commodities.indexOf(('gas')) !== -1\" class=\"navbar navbar-default navbar-table-cell\">{{result.GasAmountDue}}</th><th class=\"navbar navbar-default navbar-table-cell\">{{result.TotalAmountDue}}</th><th *ngIf=\"result.ShowActions == 'true'\" class=\"navbar navbar-default navbar-table-cell\">{{result.Actions}}</th></tr></thead><tbody><tr *ngFor=\"let b of bills\"><td>{{b.BillDate}}</td><td *ngIf=\"result.ElectricServiceCount > 0 && result.Commodities.indexOf(('electric')) !== -1\">{{b.ElectricCost}}</td><td *ngIf=\"result.WaterServiceCount > 0 && result.Commodities.indexOf(('water')) !== -1\">{{b.WaterCost}}</td><td *ngIf=\"result.GasServiceCount > 0 && result.Commodities.indexOf(('gas')) !== -1\">{{b.GasCost}}</td><td>{{b.TotalCost}}</td><td><div *ngIf=\"result.ShowActions == 'true' && !isLastBill(b)\"><i class=\"fa fa-exchange\" aria-hidden=\"true\"></i> <a href=\"{{b.CompareLink}}\">{{result.CompareLink}}</a></div></td></tr></tbody><tfoot><tr><td height=\"60\" class=\"bill-footer hideEmptyCont\" colspan=\"6\" valign=\"middle\"><div class=\"col-sm-12 view-all\"><div class=\"btn-group btn-block text-center\"><ul *ngIf=\"paginate\" class=\"pagination pagination-block\"><li [ngClass]=\"{'disabled':prevDisabled}\" (click)=\"pageChange('prev')\"><a class=\"arrow\" href=\"#\" onclick=\"return false;\"><img src=\"Content/Images/icon-page-left.png\" class=\"bill_left_nav\" alt=\"previous\"></a></li><li *ngFor=\"let p of pages\" class=\"pn\" [ngClass]=\"{'active':isCurrentPage(p)}\"><a href=\"#\" onclick=\"return false;\" (click)=\"pageChange(p)\">{{p}}</a></li><li [ngClass]=\"{'disabled':nextDisabled}\" (click)=\"pageChange('next')\"><a class=\"arrow\" href=\"#\" onclick=\"return false;\"><img src=\"Content/Images/icon-page-right.png\" class=\"bill_right_nav\" alt=\"next\"></a></li></ul><div class=\"clearb\"></div></div></div></td></tr></tfoot></table></div></div><div *ngIf=\"result.ShowFooterText === 'true' || result.ShowFooterLink === 'true' || result.ShowBillHistoryLink === 'true'\" class=\"panel-footer\"><div *ngIf=\"result.ShowBillHistoryLink === 'true'\"><a href=\"{{result.BillHistoryLink}}\">{{result.ViewBillHistory}}</a></div><div *ngIf=\"result.ShowFooterText === 'true'\" class=\"footertext\">{{result.Footer}}</div><div *ngIf=\"result.ShowFooterLink === 'true'\" class=\"footerlink\"><a href=\"{{result.FooterLink}}\">{{result.FooterLinkText}}</a></div></div></div></div>",
            providers: [billhistory_service_1.BillHistoryService, app_service_1.AppService]
        }),
        __metadata("design:paramtypes", [billhistory_service_1.BillHistoryService, app_service_1.AppService, core_1.ElementRef])
    ], BillHistory);
    return BillHistory;
    var BillHistory_1;
}());
exports.BillHistory = BillHistory;

//# sourceMappingURL=billhistory.js.map
