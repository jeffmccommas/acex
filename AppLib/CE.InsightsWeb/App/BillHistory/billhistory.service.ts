﻿import "rxjs/add/operator/map";
import { Injectable, Input } from "@angular/core"
import { Http } from "@angular/http";

@Injectable()
export class BillHistoryService {
    @Input() res: Object;
    @Input() fromUnitTest: Boolean;
    http: Http;
    constructor(http: Http) {
        this.http = http;
    }

    getBillHistory(tabKey, params) {
        return this.http.get(`./BillHistory/Get?tabKey=${tabKey}&parameters=${window.btoa(params)}`).map(res => res.json());
    }

}

