"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var threshold_service_1 = require("./threshold.service");
var app_service_1 = require("../app.service");
var Threshold = (function () {
    function Threshold(thresholdService, appService, elementRef) {
        this.enableLoader = true;
        this.elementRef = elementRef;
        this.params = appService.getParams();
        this.parameters = this.params[0].parameters;
        this.tabKey = this.params[0].tabKey;
        this.errorMessage = this.params[0].errorMessage;
        this.thresholdService = thresholdService;
        this.appService = appService;
        this.fromUTest = false;
        this.result = {};
        if (this.thresholdService.res !== undefined) {
            this.result = thresholdService.res;
            this.fromUTest = this.thresholdService.fromUnitTest;
        }
    }
    Threshold_1 = Threshold;
    Threshold.prototype.ngOnInit = function () {
        if (this.thresholdService.getThreshold("", "", "").toString() !== Threshold_1.fromUnitTest) {
            this.loadThreshold(null);
        }
    };
    Threshold.prototype.loadThreshold = function (commodityKey) {
        var _this = this;
        this.thresholdService.getThreshold(this.tabKey, this.parameters, commodityKey)
            .subscribe(function (res) { return _this.result = res; }, function (err) { return _this.appService.logError(_this, err); }, function () { return _this.disableLoader(); });
    };
    Threshold.prototype.disableLoader = function () {
        var _this = this;
        if (!this.fromUTest) {
            setTimeout(function () { return _this.appService.setHtml(_this.elementRef); }, 1);
        }
        if (this.result === undefined || this.result === null) {
            this.appService.logError(this, '');
        }
        this.setupCommodities();
        this.setupCurrentWidths();
        this.enableLoader = false;
    };
    Threshold.prototype.setupCommodities = function () {
        var coms = this.result.Commodities.split(",");
        var labels = this.result.CommoditiesText.split(",");
        var icons = this.result.CommoditiesIconFonts.split(",");
        if (coms.length > 1) {
            this.commodities = [];
            for (var i = 0; i < coms.length; i++) {
                var com = new Commodity();
                com.key = coms[i];
                com.iconFont = icons[i];
                com.showLabel = this.result.ShowCommodityLabels;
                com.label = labels[i];
                this.commodities.push(com);
            }
        }
    };
    Threshold.prototype.setupCurrentWidths = function () {
        var tiers = this.result.TierBoundariesCurrent;
        for (var i = 0; i < tiers.length; i++) {
            if (!this.inTierHit) {
                if (tiers[i].InTier) {
                    this.inTierHit = true;
                    this.inTierColor = tiers[i].TierColor;
                    tiers[i].TierPercentage = ((tiers[i].InTierPercentage / 100) * tiers[i].TierPercentage) + '%';
                }
            }
            else {
                tiers[i].TierPercentage = "0%";
            }
        }
    };
    Threshold.fromUnitTest = "unittest";
    Threshold = Threshold_1 = __decorate([
        core_1.Component({
            selector: 'threshold',
            template: "<Loader *ngIf=\"enableLoader\"></Loader><div *ngIf=\"!enableLoader\"><div *ngIf=\"errorOccurred\"><div class=\"panel panel-default\"><div class=\"alert alert-danger\"><span class=\"glyphicon glyphicon-exclamation-sign xs-mr-10\" aria-hidden=\"true\"></span><strong>{{errorMessage}}</strong> <span class=\"sr-only\">Error:</span></div></div></div><div *ngIf=\"!errorOccurred\" class=\"panel panel-default panel-threshold\"><div *ngIf=\"result.ShowTitle == 'true' && result.Title !== ''\" class=\"panel-heading\"><h1 class=\"panel-title\" role=\"presentation\">{{result.Title}}</h1></div><div *ngIf=\"result.NonTier == true\" class=\"alert alert-warning\"><span class=\"glyphicon glyphicon-alert xs-mr-10\"></span> <strong>{{result.NonTierText}}</strong></div><div *ngIf=\"result.BillToDate.NoBillToDate === true && result.BillSummary.NoBills === true\" class=\"alert alert-warning\"><span class=\"glyphicon glyphicon-alert xs-mr-10\"></span> <strong>{{result.NoBillsText}}</strong></div><div *ngIf=\"(result.BillToDate.NoBillToDate === false || result.BillSummary.NoBills === false) && result.NonTier == false\" class=\"panel-body xs-pb-0\"><h2 *ngIf=\"result.ShowSubTitle == 'true' && result.SubTitle !== ''\" role=\"presentation\" class=\"subtitle\">{{result.SubTitle}}</h2><div *ngIf=\"result.ShowIntro == 'true' && result.ShowIntro !== ''\" class=\"intro-text\" style=\"white-space: pre-line\">{{result.IntroText}}</div><div class=\"panel-body xs-pb-0\"><div *ngIf=\"commodities && commodities.length > 1\" class=\"panel-body utility\"><div class=\"utility-icons pull-right\"><div class=\"u-icon-focus\"><a *ngFor=\"let c of commodities;\" href=\"#\" (click)=\"loadThreshold(c.key)\" onclick=\"return false;\" class=\"btn btn-default icon__gas\" role=\"button\"><span class=\"sr-only\">{{c.label}} commodity</span> <span class=\"icon {{c.iconFont}}\"></span> <span *ngIf=\"c.ShowCommodityLabels == 'true'\" class=\"hidden-xs\">{{c.label}}</span> <span class=\"print-only\">{{c.label}}</span></a></div></div></div><div *ngIf=\"result.BillToDate.NoBillToDate === true\" class=\"current-bar\"><h3 class=\"pull-left\">{{result.BilledUseLabel}} {{result.BillStartDate}} {{result.DateTo}} {{result.BillEndDate}}</h3><h3 class=\"pull-right\">{{result.BillTotalServiceUse}} {{result.UOM}}</h3><span class=\"pull-right my-current\" [style.background-color]=\"inTierColor\">&nbsp;</span></div><div *ngIf=\"result.BillToDate.NoBillToDate === false\" class=\"current-bar\"><h3 class=\"pull-left\">{{result.UseToDateLabel}} <span>{{result.BillToDate.DaysLeftInBillPeriod}} {{result.DaysLeftLabel}}</span></h3><h3 class=\"pull-right\">{{result.UseToDate}} {{result.UOM}}</h3><span class=\"pull-right my-current\" [style.background-color]=\"inTierColor\">&nbsp;</span></div><div class=\"threshold-bar\"><div *ngFor=\"let z of result.TierBoundariesCurrent | objToArr;\" class=\"tier\" [style.width]=\"z.TierPercentage\" [style.background-color]=\"z.TierColor\"></div></div><div class=\"pricing-bar\"><h3 class=\"pull-left\">{{result.PricingTiersLabel}}</h3></div><div class=\"threshold-bar\"><div *ngFor=\"let t of result.TierBoundaries | objToArr; let i = index;\" class=\"tier\" [style.width]=\"t.TierPercentage + '%'\" [style.background-color]=\"t.TierColor\"></div></div><div id=\"iws_th_tbl\" class=\"table-responsive\"><table class=\"table table-hover\" role=\"presentation\"><tbody><tr *ngFor=\"let t of result.TierBoundaries | objToArr; let i = index;\" role=\"row\"><th [style.background-color]=\"t.TierColor\" role=\"rowheader\"></th><td role=\"rowheader\">{{t.TierLabel}}</td><td role=\"gridcell\">{{t.TierRangeLabel}}</td><td role=\"gridcell\">{{t.UseChargesLabel}}</td></tr></tbody></table></div><div class=\"panel-body panel-projection-info xs-pb-0\"><p *ngIf=\"result.BillToDate.NoBillToDate === true\" class=\"projected\">{{result.BilledUseMessage}} <span>{{result.BillTotalServiceUse}} {{result.UOM}}.</span></p><p *ngIf=\"result.BillToDate.NoBillToDate === false\" class=\"projected\">{{result.ProjectedUseMessage}} <span>{{result.ProjectedUse}} {{result.UOM}}</span></p><p *ngIf=\"result.BillToDate.NoBillToDate === false\" class=\"average\">{{result.AveragedailyUseMessage}} {{result.AverageDailyUse}} {{result.UOM}}</p></div></div></div></div><div *ngIf=\"result.ShowFooterText === 'true' || result.ShowFooterLink === 'true'\" class=\"panel-footer\"><div *ngIf=\"result.ShowFooterText === 'true'\" class=\"footertext\">{{result.Footer}}</div><div *ngIf=\"result.ShowFooterLink === 'true'\" class=\"footerlink\"><a href=\"{{result.FooterLink}}\">{{result.FooterLinkText}}</a></div></div></div>",
            providers: [threshold_service_1.ThresholdService, app_service_1.AppService]
        }),
        __metadata("design:paramtypes", [threshold_service_1.ThresholdService, app_service_1.AppService, core_1.ElementRef])
    ], Threshold);
    return Threshold;
    var Threshold_1;
}());
exports.Threshold = Threshold;
var Commodity = (function () {
    function Commodity() {
    }
    return Commodity;
}());
exports.Commodity = Commodity;

//# sourceMappingURL=threshold.js.map
