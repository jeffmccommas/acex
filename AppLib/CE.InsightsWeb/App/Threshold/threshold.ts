﻿import { Component, ElementRef, OnInit } from "@angular/core";
import { ThresholdService } from "./threshold.service";
import { AppService, IAppParams } from "../app.service";

@Component({
    selector: 'threshold',
    templateUrl: 'App/Threshold/threshold.html',
    providers: [ThresholdService, AppService]
})
export class Threshold implements OnInit {
    static fromUnitTest = "unittest";
    elementRef: ElementRef;
    result: any;
    error: Object;
    enableLoader: Boolean;
    errorOccurred: Boolean;
    errorMessage: string;
    thresholdService: ThresholdService;
    appService: AppService;
    params: Array<IAppParams>;
    parameters: string;
    tabKey: string;
    fromUTest: Boolean;
    showCommodityLabels: Boolean;
    commodities: Array<Commodity>;
    inTierColor: string;
    inTierHit: Boolean;

    constructor(thresholdService: ThresholdService, appService: AppService, elementRef: ElementRef) {
        this.enableLoader = true;
        this.elementRef = elementRef;
        this.params = appService.getParams();
        this.parameters = this.params[0].parameters;
        this.tabKey = this.params[0].tabKey;
        this.errorMessage = this.params[0].errorMessage;
        this.thresholdService = thresholdService;
        this.appService = appService;
        this.fromUTest = false;
        this.result = {};
        if (this.thresholdService.res !== undefined) {
            this.result = thresholdService.res;
            this.fromUTest = this.thresholdService.fromUnitTest;
        }
    }

    ngOnInit() {
        if (this.thresholdService.getThreshold("", "", "").toString() !== Threshold.fromUnitTest) {
            this.loadThreshold(null);
        }
    }

    loadThreshold(commodityKey) {
        this.thresholdService.getThreshold(this.tabKey, this.parameters, commodityKey)
            .subscribe(res => this.result = res,
                err => this.appService.logError(this, err),
                () => this.disableLoader()
            );
    }

    disableLoader() {
        if (!this.fromUTest) {
            setTimeout(() => this.appService.setHtml(this.elementRef), 1);
        }
        if (this.result === undefined || this.result === null) {
            this.appService.logError(this, '');
        }

        this.setupCommodities();
        this.setupCurrentWidths();
        this.enableLoader = false;
    }

    setupCommodities() {
        const coms = this.result.Commodities.split(",");
        const labels = this.result.CommoditiesText.split(",");
        const icons = this.result.CommoditiesIconFonts.split(",");
        if (coms.length > 1) {
            this.commodities = [];
            for (let i = 0; i < coms.length; i++) {
                const com = new Commodity();
                com.key = coms[i];
                com.iconFont = icons[i];
                com.showLabel = this.result.ShowCommodityLabels;
                com.label = labels[i];
                this.commodities.push(com);
            }
        }
    }

    setupCurrentWidths() {
        const tiers = this.result.TierBoundariesCurrent;
        for (let i = 0; i < tiers.length; i++) {
            if (!this.inTierHit) {
                if (tiers[i].InTier) {
                    this.inTierHit = true;
                    this.inTierColor = tiers[i].TierColor;
                    tiers[i].TierPercentage = ((tiers[i].InTierPercentage / 100) * tiers[i].TierPercentage) + '%';
                }

            } else {
                tiers[i].TierPercentage = "0%";
            }
        }
    }

}

export class Commodity {
    key: string;
    iconFont: string;
    showLabel: Boolean;
    label: string;
}