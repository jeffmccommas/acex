﻿import { Component, AfterViewInit } from "@angular/core";

@Component({
    selector: 'app',
    template: ''
})
export class App implements AfterViewInit {
    ngAfterViewInit() {
        this.setupLayoutForNonAngularWidgets();
    }

    setupLayoutForNonAngularWidgets() {
        aclaraJQuery(document).ready(function() {
            aclaraJQuery("[id$='_html']").each(function () {
                var id = aclaraJQuery(this).attr('id');
                const id1 = id.substring(0, id.length - 5);
                aclaraJQuery("[id*='" + id1 + "']").each(function () {
                    const cnt = aclaraJQuery(this).find('script').length;
                    if (cnt > 0) {
                        const oId = aclaraJQuery(this).attr("id") + "test";
                        aclaraJQuery(this).attr("id", oId);
                        aclaraJQuery(this).parent().parent().parent().attr("id", "deleteMe");
                        aclaraJQuery('#' + oId).appendTo('#' + id);
                        const parent = aclaraJQuery('#' + id).parent().attr('id');
                        aclaraJQuery('#' + oId).appendTo('#' + parent);
                        aclaraJQuery('#' + id).remove();
                    }
                });
            });
            aclaraJQuery('#deleteMe').remove();

            aclaraJQuery("[id$='test']").each(function () {
                aclaraJQuery(this).unwrap();
                const id = aclaraJQuery(this).attr('id');
                aclaraJQuery(this).attr('id', id.substring(0, id.length - 4));
            });

            if (typeof initialize == 'function') {
                initialize();
            }
        });
    }
}


