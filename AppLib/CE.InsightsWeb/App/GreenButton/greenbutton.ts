﻿import { Component, ElementRef, OnInit } from "@angular/core";
import { AppService, IAppParams } from "../app.service";
import { GreenButtonService } from "./greenbutton.service";

@Component({
    selector: 'greenbutton',
    templateUrl: 'App/GreenButton/greenbutton.html',
    providers: [GreenButtonService, AppService]
})

export class GreenButton implements OnInit { 
    pending: Boolean = false;
    elementRef: ElementRef;
    appService: AppService;
    params: Array<IAppParams>;
    postEvent: Object;
    parameters: string;
    tabKey: string;
    downloadType: string;
    enableLoader: Boolean;
    errorOccurred: Boolean;
    invalidDates: Boolean;
    noBillData: Boolean;
    noAmiData: Boolean;
    errorMessage: string;
    greenButtonService: GreenButtonService;
    result: any;
    downloadDetails: any;
    startDate: Date;
    endDate: Date;
    static fromUnitTest = "unittest";
    fromUTest: Boolean;
    showPanel: Boolean;
    
    constructor(greenButtonService: GreenButtonService, appService: AppService, elementRef: ElementRef) {
        this.enableLoader = true;
        this.showPanel = false;
        this.noAmiData = false;
        this.noBillData = false;
        this.elementRef = elementRef;
        this.params = appService.getParams();
        this.parameters = this.params[0].parameters;
        this.errorMessage = this.params[0].errorMessage;
        this.tabKey = this.params[0].tabKey;
        this.appService = appService;
        this.greenButtonService = greenButtonService;
        this.downloadType = "ami";
        if (this.greenButtonService.res !== undefined) {
            this.result = this.greenButtonService.res;
            this.fromUTest = this.greenButtonService.fromUnitTest;
        }
        this.startDate = new Date();
        this.endDate = new Date();

    }

    ngOnInit() {
        if (this.greenButtonService.getGreenButton("", "", "").toString() !== GreenButton.fromUnitTest) {
            this.loadGreenButton();
        }
    }

    loadGreenButton() {
        this.result = {};
        this.greenButtonService.getGreenButton(this.tabKey, this.parameters, this.downloadType)
            .subscribe(res => this.result = res,
            err => this.appService.logError(this, err),
            () => this.disableLoader()
            );     
    }

    disableLoader() {
        if (!this.fromUTest) {
            setTimeout(() => this.appService.setHtml(this.elementRef), 1);
            setTimeout(() => this.appService.setIframeExternalClass(this.elementRef), 1);                        
        }
        if (this.result === undefined || this.result === null) { // if (1 === 1) {
            this.appService.logError(this, '');
        }
        this.enableLoader = false;
    }

    checkDownloadType() {
        if (this.result.HideUsageDownload && !this.result.HideBillDownload)
            this.downloadType = "bill";
        return this.downloadType;
    }

    toggleView(dt) {
        this.downloadType = dt;
    }

    togglePanel() {
        this.showPanel = !this.showPanel;
        setTimeout(() => this.appService.setIframeExternalClass(this.elementRef), 1);  
    }

    download() {
        if (this.validateDates()) {
            return;
        }

        this.pending = true;
        this.result.NoBill = false;
        this.result.NoAmi = false;

        this.downloadDetails = {};

        if (this.greenButtonService.getGreenButton("", "", "").toString() !== GreenButton.fromUnitTest) {
            this.greenButtonService.getDownloadDetails(this.tabKey,
                                                        this.parameters,
                                                        this.downloadType,
                                                        this.startDate.toLocaleDateString(),
                                                        this.endDate.toLocaleDateString())
                .subscribe(res => this.downloadDetails = res,
                err => this.appService.logError(this, err),
                () => this.getFile()
                );
        } else {
            this.downloadDetails = this.greenButtonService.getDownloadDetails(this.tabKey,
                this.parameters,
                this.downloadType,
                this.startDate
                    .toLocaleDateString(),
                this.endDate
                    .toLocaleDateString());
            this.getFile();
        }
    }

    getFile() {

        if (this.downloadDetails.NoBill) {
            this.result.NoBill = true;
        }
        else if (this.downloadDetails.NoAmi) {
            this.result.NoAmi = true;
        }
        else {

            const xhr = new XMLHttpRequest();
            const param = encodeURIComponent(this.parameters);
            const url = `./GreenButton/Download?tabkey=${this
                .tabKey}&parameters=${window.btoa(this.parameters)}&type=${this.downloadType}&startDate=${this.startDate
                    .toLocaleDateString()}&endDate=${this.endDate.toLocaleDateString()}`;
            xhr.open('GET', url, true);
            xhr.responseType = 'blob';
            xhr.onreadystatechange = function () {
                setTimeout(() => { this.pending = false; }, 0);
                if (xhr.readyState === 4 && xhr.status === 200) {
                    const nobill = xhr.getResponseHeader('NoBill');
                    const noami = xhr.getResponseHeader('NoAmi');
                    const disposition = xhr.getResponseHeader('Content-Disposition');
                    const filename = disposition.substring(disposition.indexOf('=') + 1);
                    const blob = new Blob([this.response], { type: "application/zip" });
                    //if (this.greenButtonService.getGreenButton("", "", "") !== this.fromUnitTest) {
                        //if (nobill === "True") {
                        //    this.result.NoBill = true;
                        //} else if (noami === "True") {
                        //    this.result.NoAmi = true;
                        //} else {
                            saveAs(blob, filename);
                        //}
                    //}
                }
            };
            xhr.send();

            if (this.downloadType == "ami") {
                this.postEvents("Green Button AMI Download");
            }
            else if (this.downloadType == "bill") {
                this.postEvents("Green Button Bill Download");
            }

        }
        this.pending = false;

    }

    postEvents(eventType) {
        const ei = [];
        ei.push({ key: "TabId", value: this.tabKey });

        this.appService.postEvents(ei, this.parameters, eventType)
            .subscribe(res => this.postEvent = res,
            err => this.appService.logError(this, err)
            );
    }

    validateDates() {
        this.invalidDates = false;
        if (this.startDate > this.endDate) {
            this.invalidDates = true;
        }
        return this.invalidDates;
    }
}
