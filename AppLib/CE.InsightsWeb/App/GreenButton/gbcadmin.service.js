"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
require("rxjs/add/operator/map");
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var GBCAdminService = (function () {
    function GBCAdminService(http) {
        this.http = http;
    }
    GBCAdminService.prototype.GetGBCPrivs = function () {
        return su;
    };
    GBCAdminService.prototype.GetThirdPartyApplications = function () {
        return this.http.get("./GreenButtonConnectAdmin/Get").map(function (res) { return res.json(); });
    };
    GBCAdminService.prototype.GetNewApp = function () {
        return this.http.get("./GreenButtonConnectAdmin/GetNewApp").map(function (res) { return res.json(); });
    };
    GBCAdminService.prototype.SaveApp = function (data) {
        return this.http.post("./GreenButtonConnectAdmin/Post", JSON.stringify(data)).map(function (res) { return res.json(); });
    };
    return GBCAdminService;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], GBCAdminService.prototype, "res", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Boolean)
], GBCAdminService.prototype, "fromUnitTest", void 0);
GBCAdminService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], GBCAdminService);
exports.GBCAdminService = GBCAdminService;
//# sourceMappingURL=gbcadmin.service.js.map