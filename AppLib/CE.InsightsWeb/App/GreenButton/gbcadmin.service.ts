﻿import "rxjs/add/operator/map";
import {Injectable, Input} from "@angular/core"
import {Http} from "@angular/http";

@Injectable()
export class GBCAdminService {
    @Input() res: Object;
    @Input() fromUnitTest: Boolean;
    http: Http;
    constructor(http: Http) {
        this.http = http;
    }

    GetGBCPrivs() {
        return su;
    }

    GetThirdPartyApplications() {
        return this.http.get(`./GreenButtonConnectAdmin/Get`).map(res => res.json());
    }

    GetNewApp() {
        return this.http.get(`./GreenButtonConnectAdmin/GetNewApp`).map(res => res.json());
    }

    SaveApp(data) {
        return this.http.post(`./GreenButtonConnectAdmin/Post`, JSON.stringify(data)).map(res => res.json());
    }

}

