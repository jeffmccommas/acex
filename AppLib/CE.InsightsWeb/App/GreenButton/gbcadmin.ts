﻿import { Component, ElementRef, OnInit } from "@angular/core";
import { AppService, IAppParams } from "../app.service";
import { GBCAdminService } from "./gbcadmin.service";

@Component({
    selector: 'gbcadmin',
    templateUrl: 'App/GreenButton/gbcadmin.html',
    providers: [GBCAdminService, AppService]
})

export class GBCAdmin implements OnInit { 
    tabKey: string;
    pending: Boolean = false;
    elementRef: ElementRef;
    appService: AppService;
    params: Array<IAppParams>;
    postEvent: Object;
    parameters: string;
    enableLoader: Boolean;
    errorOccurred: Boolean;
    errorMessage: string;
    gbcAdminService: GBCAdminService;
    result: any;
    static fromUnitTest = "unittest";
    fromUTest: Boolean; 
    showDetails: Boolean;
    selectedApp: any; 
    postResponse: Object;
    saved: Boolean;
    scope: string;
    mode: string;
    showPopover: Boolean;
    popoverMsg: string;
    savedMsg: string;
    su: string;
    checked: boolean;

    constructor(gbcAdminService: GBCAdminService, appService: AppService, elementRef: ElementRef) {
        this.enableLoader = true;
        this.selectedApp = {};
        this.elementRef = elementRef;
        this.saved = false;
        
        //this.parameters = this.params[0].parameters;
        //this.errorMessage = this.params[0].errorMessage;
        //this.tabKey = this.params[0].tabKey;
        this.appService = appService;
        this.gbcAdminService = gbcAdminService;
        this.popoverMsg = "Warning, changing the dataCustodianApplicationStatus to Revoked will result in revocation of all authorizations for this application. Do you want to continue?";
        this.showPopover = false;

        if (this.gbcAdminService.res !== undefined) {
            this.result = this.gbcAdminService.res;
            this.fromUTest = this.gbcAdminService.fromUnitTest;
        }
    }

    ngOnInit() {
        if (this.gbcAdminService.GetThirdPartyApplications().toString() !== GBCAdmin.fromUnitTest) {
            this.loadThirdPartyApplications();
        }
    }

    loadThirdPartyApplications() {
        this.result = {};
        this.gbcAdminService.GetThirdPartyApplications()
            .subscribe(res => this.result = res,
            err => this.appService.logError(this, err),
            () => this.disableLoader()
            );   
       
    }

    disableLoader() {
        if (!this.fromUTest) {
            this.su = this.gbcAdminService.GetGBCPrivs();
            setTimeout(() => this.appService.setHtml(this.elementRef), 1);
            setTimeout(() => this.appService.setIframeExternalClass(this.elementRef), 1);                        
        }
        if (this.result === undefined || this.result === null) { // if (1 === 1) {
            this.appService.logError(this, '');
        } 
        this.showDetails = false;
        this.showPopover = false;
        this.enableLoader = false; 
    }

    editAppInfo(app)
    {
        this.selectedApp = app.GreenButtonApplicationInformation;
        this.selectedApp.scope = app.ScopeString;
        this.mode = "edit";
        this.savedMsg = "Green Button App updated successfully.";
        this.showDetails = true;

        if (this.selectedApp.dataCustodianApplicationStatus == "4") { //Revoked, show popover warning message
            this.showPopover = true
        }
        else {
            this.showPopover = false;
        } 

    }

    saveApp() {

        const data = {
            "greenButtonApplicationInformation": this.selectedApp,
            "scope": this.selectedApp.scope
        };

        this.gbcAdminService.SaveApp(data)
            .subscribe(res => this.postResponse = res, 
            err => this.appService.logError(this, err),
            () => this.appSaved()
            );
    }

    getNewApp() {
        this.gbcAdminService.GetNewApp()
            .subscribe(res => this.selectedApp = res,
            err => this.appService.logError(this, err),
            () => this.loadNewApp()
            );
    }

    loadNewApp() {
        this.savedMsg = "Green Button App added successfully.";
        this.mode = "add";
        this.showDetails = true;
        this.selectedApp.dataCustodianApplicationStatus = 1;
        this.selectedApp.thirdPartyApplicationStatus = 1;
        this.selectedApp.thirdPartyApplicationType = 1;
        this.selectedApp.thirdPartyApplicationUse = 1;
    }

    appSaved() {
        this.saved = true;
        if (!this.fromUTest) {
            this.loadThirdPartyApplications();
        }
    }

    updateValue(prop, value) {
        if (prop == "dataCustodianApplicationStatus")
        {
            if (value == "4") { //Revoked, show popover warning message
                this.showPopover = true
            }
            else {
                this.showPopover = false;
            }
            this.selectedApp.dataCustodianApplicationStatus = value;
        }
        else if (prop == "thirdPartyApplicationStatus") {
            this.selectedApp.thirdPartyApplicationStatus = value;
        }
        else if (prop == "thirdPartyApplicationType") {
            this.selectedApp.thirdPartyApplicationType = value;
        }
        else if (prop == "thirdPartyApplicationUse") {
            this.selectedApp.thirdPartyApplicationUse = value;
        }
    }

    setEnabled(enabled)
    {
        this.selectedApp.enabled = enabled;
    }

    optionSelected(prop, value) {
        var retVal = false;
        if (prop == "dataCustodianApplicationStatus" && this.selectedApp.dataCustodianApplicationStatus == value) 
        {
            retVal = true;           
        }
        else if (prop == "thirdPartyApplicationStatus" && this.selectedApp.thirdPartyApplicationStatus == value) 
        {
            retVal = true;
        } 
        else if (prop == "thirdPartyApplicationType" && this.selectedApp.thirdPartyApplicationType == value) {
            retVal = true;
        } 
        else if (prop == "thirdPartyApplicationUse" && this.selectedApp.thirdPartyApplicationUse == value) {
            retVal = true;
        } 
        return retVal; 
    }
     
    postEvents(eventType) {
        const ei = [];
        ei.push({ key: "TabId", value: this.tabKey });

        this.appService.postEvents(ei, this.parameters, eventType)
            .subscribe(res => this.postEvent = res,
            err => this.appService.logError(this, err)
            );
    }

}
