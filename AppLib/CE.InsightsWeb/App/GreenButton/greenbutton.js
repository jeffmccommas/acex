"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var app_service_1 = require("../app.service");
var greenbutton_service_1 = require("./greenbutton.service");
var GreenButton = (function () {
    function GreenButton(greenButtonService, appService, elementRef) {
        this.pending = false;
        this.enableLoader = true;
        this.showPanel = false;
        this.noAmiData = false;
        this.noBillData = false;
        this.elementRef = elementRef;
        this.params = appService.getParams();
        this.parameters = this.params[0].parameters;
        this.errorMessage = this.params[0].errorMessage;
        this.tabKey = this.params[0].tabKey;
        this.appService = appService;
        this.greenButtonService = greenButtonService;
        this.downloadType = "ami";
        if (this.greenButtonService.res !== undefined) {
            this.result = this.greenButtonService.res;
            this.fromUTest = this.greenButtonService.fromUnitTest;
        }
        this.startDate = new Date();
        this.endDate = new Date();
    }
    GreenButton_1 = GreenButton;
    GreenButton.prototype.ngOnInit = function () {
        if (this.greenButtonService.getGreenButton("", "", "").toString() !== GreenButton_1.fromUnitTest) {
            this.loadGreenButton();
        }
    };
    GreenButton.prototype.loadGreenButton = function () {
        var _this = this;
        this.result = {};
        this.greenButtonService.getGreenButton(this.tabKey, this.parameters, this.downloadType)
            .subscribe(function (res) { return _this.result = res; }, function (err) { return _this.appService.logError(_this, err); }, function () { return _this.disableLoader(); });
    };
    GreenButton.prototype.disableLoader = function () {
        var _this = this;
        if (!this.fromUTest) {
            setTimeout(function () { return _this.appService.setHtml(_this.elementRef); }, 1);
            setTimeout(function () { return _this.appService.setIframeExternalClass(_this.elementRef); }, 1);
        }
        if (this.result === undefined || this.result === null) {
            this.appService.logError(this, '');
        }
        this.enableLoader = false;
    };
    GreenButton.prototype.checkDownloadType = function () {
        if (this.result.HideUsageDownload && !this.result.HideBillDownload)
            this.downloadType = "bill";
        return this.downloadType;
    };
    GreenButton.prototype.toggleView = function (dt) {
        this.downloadType = dt;
    };
    GreenButton.prototype.togglePanel = function () {
        var _this = this;
        this.showPanel = !this.showPanel;
        setTimeout(function () { return _this.appService.setIframeExternalClass(_this.elementRef); }, 1);
    };
    GreenButton.prototype.download = function () {
        var _this = this;
        if (this.validateDates()) {
            return;
        }
        this.pending = true;
        this.result.NoBill = false;
        this.result.NoAmi = false;
        this.downloadDetails = {};
        if (this.greenButtonService.getGreenButton("", "", "").toString() !== GreenButton_1.fromUnitTest) {
            this.greenButtonService.getDownloadDetails(this.tabKey, this.parameters, this.downloadType, this.startDate.toLocaleDateString(), this.endDate.toLocaleDateString())
                .subscribe(function (res) { return _this.downloadDetails = res; }, function (err) { return _this.appService.logError(_this, err); }, function () { return _this.getFile(); });
        }
        else {
            this.downloadDetails = this.greenButtonService.getDownloadDetails(this.tabKey, this.parameters, this.downloadType, this.startDate
                .toLocaleDateString(), this.endDate
                .toLocaleDateString());
            this.getFile();
        }
    };
    GreenButton.prototype.getFile = function () {
        if (this.downloadDetails.NoBill) {
            this.result.NoBill = true;
        }
        else if (this.downloadDetails.NoAmi) {
            this.result.NoAmi = true;
        }
        else {
            var xhr_1 = new XMLHttpRequest();
            var param = encodeURIComponent(this.parameters);
            var url = "./GreenButton/Download?tabkey=" + this
                .tabKey + "&parameters=" + window.btoa(this.parameters) + "&type=" + this.downloadType + "&startDate=" + this.startDate
                .toLocaleDateString() + "&endDate=" + this.endDate.toLocaleDateString();
            xhr_1.open('GET', url, true);
            xhr_1.responseType = 'blob';
            xhr_1.onreadystatechange = function () {
                var _this = this;
                setTimeout(function () { _this.pending = false; }, 0);
                if (xhr_1.readyState === 4 && xhr_1.status === 200) {
                    var nobill = xhr_1.getResponseHeader('NoBill');
                    var noami = xhr_1.getResponseHeader('NoAmi');
                    var disposition = xhr_1.getResponseHeader('Content-Disposition');
                    var filename = disposition.substring(disposition.indexOf('=') + 1);
                    var blob = new Blob([this.response], { type: "application/zip" });
                    saveAs(blob, filename);
                }
            };
            xhr_1.send();
            if (this.downloadType == "ami") {
                this.postEvents("Green Button AMI Download");
            }
            else if (this.downloadType == "bill") {
                this.postEvents("Green Button Bill Download");
            }
        }
        this.pending = false;
    };
    GreenButton.prototype.postEvents = function (eventType) {
        var _this = this;
        var ei = [];
        ei.push({ key: "TabId", value: this.tabKey });
        this.appService.postEvents(ei, this.parameters, eventType)
            .subscribe(function (res) { return _this.postEvent = res; }, function (err) { return _this.appService.logError(_this, err); });
    };
    GreenButton.prototype.validateDates = function () {
        this.invalidDates = false;
        if (this.startDate > this.endDate) {
            this.invalidDates = true;
        }
        return this.invalidDates;
    };
    GreenButton.fromUnitTest = "unittest";
    GreenButton = GreenButton_1 = __decorate([
        core_1.Component({
            selector: 'greenbutton',
            template: "<Loader *ngIf=\"enableLoader\"></Loader><div *ngIf=\"!enableLoader\"><div *ngIf=\"errorOccurred\"><div class=\"panel panel-default\"><div *ngIf=\"result.ShowTitle == 'true' && result.Title !== ''\" class=\"panel-heading\"><h1 class=\"xs-mb-0\" role=\"presentation\">{{result.Title}}</h1></div><div class=\"alert alert-danger\"><span class=\"glyphicon glyphicon-hand-right\"></span> <strong>{{errorMessage}}</strong> <span class=\"sr-only\">Error:</span></div></div></div><div *ngIf=\"!errorOccurred\" class=\"panel panel-default greenbutton\"><div class=\"panel-heading greenbutton__heading\"><div class=\"row\"><div class=\"col-xs-5 text-left\"><h1 *ngIf=\"result.ShowTitle == 'true' && result.Title !== ''\" class=\"panel-title greenbutton__heading--h3\">{{result.Title}}</h1></div><div class=\"col-xs-7 greenbutton__info text-right xs-pr-0\"><div class=\"xs-pl-0\">{{result.BannerText}}</div><div class=\"xs-pl-0 toggle__btn toggle__lgt pull-right xs-ml-15\"><a class=\"btn btn-default xs-ml-10\" href=\"#\" onclick=\"return false;\" (click)=\"togglePanel()\" role=\"button\"><span *ngIf=\"!showPanel\">{{result.ExpandLabel}}</span> <span *ngIf=\"showPanel\">{{result.CollapseLabel}}</span> <span [ngClass]=\"showPanel ? 'icon icon-toggle-up' : 'icon icon-toggle-down'\"></span></a></div></div></div></div><div *ngIf=\"showPanel\"><div class=\"panel-body utility xs-mb-0 xs-pl-0 xs-pr-0\"><div class=\"sub-title xs-pb-15\" *ngIf=\"result.ShowSubTitle == 'true' && result.Title !== ''\">{{result.SubTitle}}</div><div class=\"intro-text\" *ngIf=\"result.ShowIntro == 'true' && result.Title !== ''\">{{result.IntroText}}</div></div><div class=\"panel-body xs-pl-30 xs-pr-30 xs-ml-15 xs-mr-15 xs-pb-0 xs-mb-0\"><div class=\"col-ms-8 col-md-9\"><div *ngIf=\"!result.HideUsageDownload\"><span [ngClass]=\"{'radio': !result.HideUsageDownload && !result.HideBillDownload}\"><label class=\"text-left\"><input type=\"radio\" *ngIf=\"!result.HideBillDownload\" name=\"optradio\" (click)=\"toggleView('ami')\" checked=\"checked\">{{result.DownloadUsage}}</label></span></div><div *ngIf=\"!result.HideUsageDownload && !result.HideBillDownload\"><span class=\"radio\"><label class=\"text-left\"><input type=\"radio\" name=\"optradio\" (click)=\"toggleView('bill')\">{{result.DownloadBills}}</label></span></div><div class=\"panel-body xs-pl-0 xs-pb-0 xs-mb-0\" *ngIf=\"!result.HideUsageDownload\"><div class=\"col-xs-12 xs-pl-0\" *ngIf=\"checkDownloadType() === 'ami'\"><div class=\"panel-default greenbutton__export no-shadow\"><div class=\"form-horizontal greenbutton__form\"><div class=\"panel-body xs-pl-0 xs-pb-0 xs-pt-10 xs-mb-0\"><p class=\"xs-mb-20\">{{result.DownloadAmiTitle}}</p><fieldset><div class=\"row\"><div class=\"col-sm-6\"><label class=\"col-sm-3 control-label\" for=\"textinput\">{{result.FromDate}}</label><div class=\"form-group text-xs-center text-ms-center text-sm-left\"><div class=\"col-sm-9\"><div class=\"input-group date\"><datepicker [(ngModel)]=\"startDate\" showWeeks=\"true\"></datepicker></div></div></div></div><div class=\"col-sm-6\"><label class=\"col-sm-3 control-label\" for=\"textinput\">{{result.ToDate}}</label><div class=\"form-group text-xs-center text-ms-center text-sm-left\"><div class=\"col-sm-9\"><div class=\"input-group date\"><datepicker [(ngModel)]=\"endDate\" showWeeks=\"true\"></datepicker></div></div></div></div></div></fieldset></div></div></div></div></div></div><div [ngClass]=\"result.HideUsageDownload ? 'col-ms-12 col-md-12 text-center' : 'col-ms-4 col-md-3'\"><div class=\"col-ms-12 xs-pl-0 xs-pr-0\" *ngIf=\"checkDownloadType() === 'bill'\"><div class=\"panel-default greenbutton__export no-shadow\"><div class=\"panel-body xs-mb-0 xs-pl-0 xs-pr-0\"><div class=\"form-horizontal greenbutton__form\"><fieldset class=\"text-center\"><div class=\"form-group xs-mb-0\"><input alt=\"Download my bills\" class=\"xs-mb-0 xs-mt-5\" (click)=\"download()\" style=\"width: 80px; height: auto\" type=\"image\" src=\"Content/Images/icon-greenbutton.png\"><p class=\"xs-mb-20 xs-ml-15 xs-mr-5 block error\" *ngIf=\"result.NoBill\">{{result.NoBillData}}</p><span class=\"fa fa-refresh fa-spin block\" *ngIf=\"pending\">{{result.DownloadingMessage}}</span></div></fieldset></div><p class=\"xs-mb-20 xs-mb-20 xs-ml-15 xs-mr-0 xs-mt-5\">{{result.DownloadBillsTitle}}</p></div></div></div><div class=\"col-ms-12 xs-pl-0 xs-pr-0\" *ngIf=\"downloadType === 'ami'\"><div class=\"panel-default greenbutton__export no-shadow\"><div class=\"panel-body xs-mb-0 xs-pl-0 xs-pr-0\"><div class=\"form-horizontal greenbutton__form\"><fieldset class=\"text-center\"><div class=\"form-group xs-mb-0\"><input alt=\"Download my usage for specific days\" class=\"xs-mb-0 xs-mt-5\" (click)=\"download()\" style=\"width: 80px; height: auto\" type=\"image\" src=\"Content/Images/icon-greenbutton.png\"><p *ngIf=\"invalidDates\">{{result.DateError}}</p><p class=\"xs-mb-20 xs-ml-15 xs-mr-5 block error\" *ngIf=\"result.NoAmi\">{{result.NoUsageData}}</p><span class=\"fa fa-refresh fa-spin block\" *ngIf=\"pending\">{{result.DownloadingMessage}}</span></div></fieldset></div></div></div></div></div></div><div *ngIf=\"result.ShowFooterText === 'true' || result.ShowFooterLink === 'true'\"><div class=\"panel-footer\"><div *ngIf=\"result.ShowFooterText === 'true'\" class=\"footertext\">{{result.Footer}}</div><div *ngIf=\"result.ShowFooterLink === 'true'\" class=\"footerlink dv-external-links\"><a href=\"{{result.FooterLink}}\">{{result.FooterLinkText}}</a></div></div></div></div></div></div>",
            providers: [greenbutton_service_1.GreenButtonService, app_service_1.AppService]
        }),
        __metadata("design:paramtypes", [greenbutton_service_1.GreenButtonService, app_service_1.AppService, core_1.ElementRef])
    ], GreenButton);
    return GreenButton;
    var GreenButton_1;
}());
exports.GreenButton = GreenButton;

//# sourceMappingURL=greenbutton.js.map
