﻿import "rxjs/add/operator/map";
import {Injectable, Input} from "@angular/core"
import {Http} from "@angular/http";

@Injectable()
export class GreenButtonService {
    @Input() res: Object;
    @Input() fromUnitTest: Boolean;
    http: Http;
    constructor(http: Http) {
        this.http = http;
    }

    getGreenButton(tabKey, params, type) {
        return this.http.get(`./GreenButton/Get?tabkey=${tabKey}&parameters=${window.btoa(params)}&type=${type}`).map(res => res.json());
    }

    getDownloadDetails(tabKey, params, type, startDate, endDate) {
        return this.http.get(`./GreenButton/DownloadDetails?tabkey=${tabKey}&parameters=${window.btoa(params)}&type=${type}&startDate=${startDate}&endDate=${endDate}`).map(res => res.json());
    }

}

