"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var App = (function () {
    function App() {
    }
    App.prototype.ngAfterViewInit = function () {
        this.setupLayoutForNonAngularWidgets();
    };
    App.prototype.setupLayoutForNonAngularWidgets = function () {
        aclaraJQuery(document).ready(function () {
            aclaraJQuery("[id$='_html']").each(function () {
                var id = aclaraJQuery(this).attr('id');
                var id1 = id.substring(0, id.length - 5);
                aclaraJQuery("[id*='" + id1 + "']").each(function () {
                    var cnt = aclaraJQuery(this).find('script').length;
                    if (cnt > 0) {
                        var oId = aclaraJQuery(this).attr("id") + "test";
                        aclaraJQuery(this).attr("id", oId);
                        aclaraJQuery(this).parent().parent().parent().attr("id", "deleteMe");
                        aclaraJQuery('#' + oId).appendTo('#' + id);
                        var parent_1 = aclaraJQuery('#' + id).parent().attr('id');
                        aclaraJQuery('#' + oId).appendTo('#' + parent_1);
                        aclaraJQuery('#' + id).remove();
                    }
                });
            });
            aclaraJQuery('#deleteMe').remove();
            aclaraJQuery("[id$='test']").each(function () {
                aclaraJQuery(this).unwrap();
                var id = aclaraJQuery(this).attr('id');
                aclaraJQuery(this).attr('id', id.substring(0, id.length - 4));
            });
            if (typeof initialize == 'function') {
                initialize();
            }
        });
    };
    App = __decorate([
        core_1.Component({
            selector: 'app',
            template: ''
        })
    ], App);
    return App;
}());
exports.App = App;

//# sourceMappingURL=app.js.map
