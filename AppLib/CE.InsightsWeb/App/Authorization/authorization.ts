﻿import { Component, ElementRef, OnInit } from "@angular/core";
import { AuthorizationService } from "./authorization.service";
import { AppService, IAppParams } from "../app.service";

@Component({
    selector: 'authorization',
    templateUrl: 'App/Authorization/authorization.html',
    providers: [AuthorizationService, AppService]
})
export class Authorization implements OnInit {
    elementRef: ElementRef;
    result: any;
    error: Object;
    postEvent: Object;
    enableLoader: Boolean;
    errorOccurred: Boolean;
    errorMessage: string;
    authorizationService: AuthorizationService;
    appService: AppService;
    params: Array<IAppParams>;
    parameters: string;
    tabKey: string;
    fromUTest: Boolean;

    constructor(authorizationService: AuthorizationService, appService: AppService, elementRef: ElementRef) {
        this.enableLoader = true;
        this.elementRef = elementRef;
        //this.params = appService.getParams();
        this.parameters = "params";//this.params[0].parameters;
        this.tabKey = "1"; //this.params[0].tabKey;
        this.errorMessage = "error"; //this.params[0].errorMessage;
        this.authorizationService = authorizationService;
        this.appService = appService;
        this.fromUTest = false;

        if (this.authorizationService.res !== undefined) {
            this.result = authorizationService.res;
            this.fromUTest = this.authorizationService.fromUnitTest;
        }
        
    }

    ngOnInit() {
        //this.loadAuthorization();
    }

    loadAuthorization() {
        this.result = {};
        this.authorizationService.GetAuthorization(this.tabKey, this.parameters)
                .subscribe(res => this.result = res,
                    err => this.appService.logError(this, err)
                );
 
        this.enableLoader = false;
    }

    authorize() {
               
    }

}