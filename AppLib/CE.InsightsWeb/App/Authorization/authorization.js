"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var authorization_service_1 = require("./authorization.service");
var app_service_1 = require("../app.service");
var Authorization = (function () {
    function Authorization(authorizationService, appService, elementRef) {
        this.enableLoader = true;
        this.elementRef = elementRef;
        this.parameters = "params";
        this.tabKey = "1";
        this.errorMessage = "error";
        this.authorizationService = authorizationService;
        this.appService = appService;
        this.fromUTest = false;
        if (this.authorizationService.res !== undefined) {
            this.result = authorizationService.res;
            this.fromUTest = this.authorizationService.fromUnitTest;
        }
    }
    Authorization.prototype.ngOnInit = function () {
    };
    Authorization.prototype.loadAuthorization = function () {
        var _this = this;
        this.result = {};
        this.authorizationService.GetAuthorization(this.tabKey, this.parameters)
            .subscribe(function (res) { return _this.result = res; }, function (err) { return _this.appService.logError(_this, err); });
        this.enableLoader = false;
    };
    Authorization.prototype.authorize = function () {
    };
    Authorization = __decorate([
        core_1.Component({
            selector: 'authorization',
            template: "<h1>oAuth 2 Authorization Form</h1><p>The Third Party Application redirects the retail customer to this page.</p><p>The Retail Customer is presented with an authentication form.</p><p>They can choose to accept/or decline authorization for the Third Party application to make automated requests for their Green Button data.</p>",
            providers: [authorization_service_1.AuthorizationService, app_service_1.AppService]
        }),
        __metadata("design:paramtypes", [authorization_service_1.AuthorizationService, app_service_1.AppService, core_1.ElementRef])
    ], Authorization);
    return Authorization;
}());
exports.Authorization = Authorization;

//# sourceMappingURL=authorization.js.map
