"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var unauthenticated_service_1 = require("./unauthenticated.service");
var app_service_1 = require("../app.service");
var Unauthenticated = (function () {
    function Unauthenticated(unauthenticatedService, appService, elementRef) {
        this.enableLoader = true;
        this.elementRef = elementRef;
        this.unauthenticatedService = unauthenticatedService;
        this.appService = appService;
        this.fromUTest = false;
        if (this.unauthenticatedService.res !== undefined) {
            this.result = unauthenticatedService.res;
            this.fromUTest = this.unauthenticatedService.fromUnitTest;
        }
    }
    Unauthenticated.prototype.ngOnInit = function () {
        this.loadContent();
    };
    Unauthenticated.prototype.loadContent = function () {
        this.enableLoader = false;
    };
    Unauthenticated.prototype.bindLoginForm = function () {
    };
    Unauthenticated = __decorate([
        core_1.Component({
            selector: 'unauthenticated',
            template: "<Loader *ngIf=\"enableLoader\"></Loader><div *ngIf=\"!enableLoader\"><div *ngIf=\"errorOccurred\"><div class=\"panel panel-default\"><div class=\"alert alert-danger\"><span class=\"glyphicon glyphicon-exclamation-sign xs-mr-10\" aria-hidden=\"true\"></span><strong>{{errorMessage}}</strong> <span class=\"sr-only\">Error:</span></div></div></div><div *ngIf=\"!errorOccurred\"><div id=\"bg\"></div><div class=\"Absolute-Center is-Responsive\"><div class=\"col-sm-12 col-md-10 col-md-offset-1\"><h2>Get Started</h2><h4>Register</h4><form action=\"\" id=\"loginForm\"><div class=\"form-group\"><label for=\"exampleInputEmail1\">Email address</label><input type=\"email\" class=\"form-control\" id=\"emailaddress\" placeholder=\"Email\"><div class=\"danger\">Enter a valid email</div></div><div class=\"form-group\"><label for=\"exampleInputZip\">Zip Code</label><input type=\"email\" class=\"form-control\" id=\"zipcode\" placeholder=\"Zip Code\"><div class=\"danger\">Enter a valid Zip Code</div></div><div class=\"form-group input-group\"><form action=\"?\" method=\"POST\"><div class=\"g-recaptcha\" data-sitekey=\"your_site_key\"></div><br><input type=\"submit\" value=\"Submit\"></form></div><button type=\"button\" class=\"btn btn-primary\">Create Account</button><br><p><a class=\"unauthenticated__loginlink\">I already have a login</a></p></form></div></div></div></div>",
            providers: [unauthenticated_service_1.UnauthenticatedService, app_service_1.AppService]
        }),
        __metadata("design:paramtypes", [unauthenticated_service_1.UnauthenticatedService, app_service_1.AppService, core_1.ElementRef])
    ], Unauthenticated);
    return Unauthenticated;
}());
exports.Unauthenticated = Unauthenticated;

//# sourceMappingURL=unauthenticated.js.map
