﻿import { Component, ElementRef, OnInit } from "@angular/core";
import { UnauthenticatedService } from "./unauthenticated.service";
import { AppService } from "../app.service";

@Component({
    selector: 'unauthenticated',
    templateUrl: 'App/Unauthenticated/unauthenticated.html',
    providers: [UnauthenticatedService, AppService]
})
export class Unauthenticated implements OnInit {
    elementRef: ElementRef;
    result: any;
    error: Object;
    postEvent: Object;
    enableLoader: Boolean;
    errorOccurred: Boolean;
    errorMessage: string;
    unauthenticatedService: UnauthenticatedService;
    appService: AppService;
    fromUTest: Boolean;

    constructor(unauthenticatedService: UnauthenticatedService, appService: AppService, elementRef: ElementRef) {
        this.enableLoader = true;
        this.elementRef = elementRef;
        this.unauthenticatedService = unauthenticatedService;
        this.appService = appService;
        this.fromUTest = false;

        if (this.unauthenticatedService.res !== undefined) {
            this.result = unauthenticatedService.res;
            this.fromUTest = this.unauthenticatedService.fromUnitTest;
        }
        
    }

    ngOnInit() {
        this.loadContent();
    }

    loadContent() {

        this.enableLoader = false;
    }

    bindLoginForm() {
                  
    }
    
}