﻿import "rxjs/add/operator/map";
import { Injectable, Input } from "@angular/core"
import { Http } from "@angular/http";

@Injectable()
export class UnauthenticatedService {
    @Input() res: Object;
    @Input() fromUnitTest: Boolean;
    http: Http;
    constructor(http: Http) {
        this.http = http;
    }

    GetContent(tabKey, params) {
        return this.http.get(`./Public/Get?tabKey=${tabKey}&parameters=${window.btoa(params)}`).map(res => res.json());
    }

}

