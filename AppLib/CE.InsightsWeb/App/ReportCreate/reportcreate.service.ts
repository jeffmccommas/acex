﻿import "rxjs/add/operator/map";
import { Injectable, Input } from "@angular/core"
import { Http } from "@angular/http";

@Injectable()
export class ReportCreateService {
    @Input() res: Object;
    @Input() fromUnitTest: Boolean;
    http: Http;
    constructor(http: Http) {
        this.http = http;
    }

    getReportCreate(tabKey, params) {
        return this.http.get(`./ReportCreate/Get?tabKey=${tabKey}&parameters=${window.btoa(params)}`).map(res => res.json());
    }

    //called through the common.js pdfKendoUIexport() method
    //postReportCreate(tabKey, params, file, title, notes, iscsr) {
    //    return this.http.post(`./ReportCreate/PostReportCreate?tabkey=${tabKey}&parameters=${window.btoa(params)}&file=${file}&title=${title}}&notes=${notes}&iscsr=${iscsr}`).map(res => res.json());

    //}

    getIsCsr(params) {
        return this.http.get(`./ReportCreate/GetIsCsr?parameters=${window.btoa(params)}`).map(res =>res.json());
    }
}

