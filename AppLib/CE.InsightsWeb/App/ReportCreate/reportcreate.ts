﻿import { Component, ElementRef, OnInit } from "@angular/core";
import { ReportCreateService } from "./reportcreate.service";
import { AppService, IAppParams } from "../app.service";

@Component({
    selector: 'reportcreate',
    templateUrl: 'App/ReportCreate/reportcreate.html',
    providers: [ReportCreateService, AppService]
})
export class ReportCreate implements OnInit {
    elementRef: ElementRef;
    result: any;
    isCsr: any;
    error: Object;
    reportError: Boolean;
    postError: Boolean;
    enableLoader: Boolean;
    errorOccurred: Boolean;
    errorMessage: string;
    pdfexport: Boolean;
    reportcreateService: ReportCreateService;
    appService: AppService;
    params: Array<IAppParams>;
    parameters: string;
    tabKey: string;
    fromUnitTs: string;
    static fromUnitTest = "unittest";
    fromUTest: Boolean;
    
    constructor(reportcreateService: ReportCreateService, appService: AppService, elementRef: ElementRef) {
        this.enableLoader = true;
        this.elementRef = elementRef;
        this.params = appService.getParams();
        this.parameters = this.params[0].parameters;
        this.tabKey = this.params[0].tabKey;
        this.errorMessage = this.params[0].errorMessage;
        this.reportcreateService = reportcreateService;
        this.appService = appService;
      
        this.reportError = false;
        this.postError = false;
        this.pdfexport = false;
        if (this.reportcreateService.res !== undefined) {
           this.result = reportcreateService.res;
           this.fromUTest = this.reportcreateService.fromUnitTest;
        }
    }

    ngOnInit() {
        if (this.reportcreateService.getReportCreate("", "").toString() !== ReportCreate.fromUnitTest) {
        this.checkIfCsr();
        this.loadReportContent();
	}
    }

    checkIfCsr() {     
            this.reportcreateService.getIsCsr(this.parameters)
                .subscribe(res => this.isCsr = res,
                err => this.appService.logError(this, err));            
    }

    loadReportContent() {
    this.result = {};
        if (!this.fromUTest) {
            this.reportcreateService.getReportCreate(this.tabKey, this.parameters)
                .subscribe(res => this.result = res,
                    err => this.appService.logError(this, err)
                );
        }
        this.enableLoader = false; 
    }

    pdfKendoUIExport(isCsr) {
        this.reportError = false;
        this.pdfexport = true;
        var reporttitle = this.result.DefaultReportTitle;
        var reportnotes = this.result.DefaultReportNotes;
        if (reporttitle !== "") {
            var csr;
            if (isCsr==="true")
                csr = true;
            else 
                csr = false;
            if (this.fromUnitTs !== ReportCreate.fromUnitTest) {
                this.appService.pdfExport(this.result.ExcludeWidgets, reporttitle, reportnotes, this.tabKey, csr);
            }
        } else {
            this.reportError = true;
        }
        this.pdfexport = false;
    }

    
}