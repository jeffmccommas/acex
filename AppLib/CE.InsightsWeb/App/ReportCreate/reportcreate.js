"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var reportcreate_service_1 = require("./reportcreate.service");
var app_service_1 = require("../app.service");
var ReportCreate = (function () {
    function ReportCreate(reportcreateService, appService, elementRef) {
        this.enableLoader = true;
        this.elementRef = elementRef;
        this.params = appService.getParams();
        this.parameters = this.params[0].parameters;
        this.tabKey = this.params[0].tabKey;
        this.errorMessage = this.params[0].errorMessage;
        this.reportcreateService = reportcreateService;
        this.appService = appService;
        this.reportError = false;
        this.postError = false;
        this.pdfexport = false;
        if (this.reportcreateService.res !== undefined) {
            this.result = reportcreateService.res;
            this.fromUTest = this.reportcreateService.fromUnitTest;
        }
    }
    ReportCreate_1 = ReportCreate;
    ReportCreate.prototype.ngOnInit = function () {
        if (this.reportcreateService.getReportCreate("", "").toString() !== ReportCreate_1.fromUnitTest) {
            this.checkIfCsr();
            this.loadReportContent();
        }
    };
    ReportCreate.prototype.checkIfCsr = function () {
        var _this = this;
        this.reportcreateService.getIsCsr(this.parameters)
            .subscribe(function (res) { return _this.isCsr = res; }, function (err) { return _this.appService.logError(_this, err); });
    };
    ReportCreate.prototype.loadReportContent = function () {
        var _this = this;
        this.result = {};
        if (!this.fromUTest) {
            this.reportcreateService.getReportCreate(this.tabKey, this.parameters)
                .subscribe(function (res) { return _this.result = res; }, function (err) { return _this.appService.logError(_this, err); });
        }
        this.enableLoader = false;
    };
    ReportCreate.prototype.pdfKendoUIExport = function (isCsr) {
        this.reportError = false;
        this.pdfexport = true;
        var reporttitle = this.result.DefaultReportTitle;
        var reportnotes = this.result.DefaultReportNotes;
        if (reporttitle !== "") {
            var csr;
            if (isCsr === "true")
                csr = true;
            else
                csr = false;
            if (this.fromUnitTs !== ReportCreate_1.fromUnitTest) {
                this.appService.pdfExport(this.result.ExcludeWidgets, reporttitle, reportnotes, this.tabKey, csr);
            }
        }
        else {
            this.reportError = true;
        }
        this.pdfexport = false;
    };
    ReportCreate.fromUnitTest = "unittest";
    ReportCreate = ReportCreate_1 = __decorate([
        core_1.Component({
            selector: 'reportcreate',
            template: "<Loader *ngIf=\"enableLoader\"></Loader><div *ngIf=\"!enableLoader\"><div *ngIf=\"errorOccurred\"><div class=\"panel panel-default\"><div class=\"alert alert-danger\"><span class=\"glyphicon glyphicon-exclamation-sign xs-mr-10\" aria-hidden=\"true\"></span><strong>{{errorMessage}}</strong> <span class=\"sr-only\">Error:</span></div></div></div><div *ngIf=\"!errorOccurred\"><a name=\"create-report-csr\"></a><div class=\"panel panel-default\"><div *ngIf=\"result.ShowTitle == 'true' && result.Title !== ''\" class=\"panel-heading\"><h1 class=\"panel-title\" role=\"presentation\">{{result.Title}}</h1></div><div class=\"hideEmptyCont\"><div class=\"panel-body xs-mb-0\"><h5 *ngIf=\"result.ShowSubTitle == 'true' && result.SubTitle !== ''\">{{result.SubTitle}}</h5><div *ngIf=\"result.ShowIntro == 'true' && result.ShowIntro !== ''\" class=\"intro-text xs-mb-0\" style=\"white-space: pre-line\">{{result.IntroText}}</div></div></div><a name=\"create-report\"></a><div class=\"panel-body xs-p-15 xs-pb-0\"><div [ngClass]=\"{'col-sm-5 xs-pl-0 xs-pr-0 xs-mr-0 ms-mr-15':isCsr=='true','xs-pl-0 xs-pr-0':isCsr!=='true'}\"><div class=\"form-group\" [ngClass]=\"{'sm-mr-15':isCsr=='true'}\"><label *ngIf=\"reportError\" id=\"lblErrorRC\" class=\"error\">{{result.NoTitle}}</label><label *ngIf=\"postError\" id=\"lblPostErrorRC\" class=\"error\">{{result.PostError}}</label><label class=\"sr-only\" for=\"inputTitle\">{{result.ReportTitle}}</label><input id=\"inputTitle\" class=\"form-control\" [ngClass]=\"{'btn-error':reportError}\" [(ngModel)]=\"result.DefaultReportTitle\" placeholder=\"{{result.ReportTitle}}\" value=\"{{result.DefaultReportTitle}}\" maxlength=\"100\" required></div></div><div [ngClass]=\"{'col-sm-5 xs-pl-0 xs-pr-0':isCsr=='true','xs-pl-0 xs-pr-0':isCsr!=='true'}\"><div class=\"form-group xs-mb-0\"><label class=\"sr-only\" for=\"inputNotes\">{{result.ReportNotes}}</label><textarea rows=\"4\" id=\"inputNotes\" type=\"text\" class=\"form-control\" [(ngModel)]=\"result.DefaultReportNotes\" placeholder=\"{{result.ReportNotes}}\" style=\"width: 100%\" maxlength=\"500\" value=\"{{result.DefaultReportNotes}}\"></textarea></div></div><div [ngClass]=\"{'col-sm-2 xs-pr-0 xs-pt-15 sm-pt-0 sm-pr-0':isCsr=='true','xs-pr-0 xs-pt-15':isCsr!=='true'}\"><div class=\"form-group text-right xs-mb-0\"><button style=\"white-space: normal\" class=\"btn btn-primary\" [ngClass]=\"{disabled:pdfexport}\" name=\"pdfExport\" type=\"button\" (click)=\"pdfKendoUIExport(isCsr)\">{{result.CreateReportButton}}</button></div></div></div></div><div *ngIf=\"result.ShowFooterText === 'true' || result.ShowFooterLink === 'true'\" class=\"panel-footer\"><div *ngIf=\"result.ShowFooterText === 'true'\" class=\"footertext\">{{result.Footer}}</div><div *ngIf=\"result.ShowFooterLink === 'true'\" class=\"footerlink\"><a href=\"{{result.FooterLink}}\">{{result.FooterLinkText}}</a></div></div></div></div>",
            providers: [reportcreate_service_1.ReportCreateService, app_service_1.AppService]
        }),
        __metadata("design:paramtypes", [reportcreate_service_1.ReportCreateService, app_service_1.AppService, core_1.ElementRef])
    ], ReportCreate);
    return ReportCreate;
    var ReportCreate_1;
}());
exports.ReportCreate = ReportCreate;

//# sourceMappingURL=reportcreate.js.map
