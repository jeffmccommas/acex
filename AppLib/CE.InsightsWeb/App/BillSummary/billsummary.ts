﻿import { Component, ElementRef, OnInit } from "@angular/core";
import { BillSummaryService } from "./billsummary.service";
import { AppService, IAppParams } from "../app.service";

@Component({
    selector: 'billsummary',
    templateUrl: 'App/BillSummary/billsummary.html',
    providers: [BillSummaryService, AppService]
})
export class BillSummary implements OnInit {
    static fromUnitTest = "unittest";
    elementRef: ElementRef;
    result: any;
    error: Object;
    enableLoader: Boolean;
    errorOccurred: Boolean;
    errorMessage: string;
    billsummaryService: BillSummaryService;
    appService: AppService;
    params: Array<IAppParams>;
    parameters: string;
    tabKey: string;
    fromUTest: Boolean;

    constructor(billsummaryService: BillSummaryService, appService: AppService, elementRef: ElementRef) {
        this.enableLoader = true;
        this.elementRef = elementRef;
        this.params = appService.getParams();
        this.parameters = this.params[0].parameters;
        this.tabKey = this.params[0].tabKey;
        this.errorMessage = this.params[0].errorMessage;
        this.billsummaryService = billsummaryService;
        this.appService = appService;

        if (this.billsummaryService.res !== undefined) {
            this.result = billsummaryService.res;
            this.fromUTest = this.billsummaryService.fromUnitTest;
        }
    }

    ngOnInit() {
        if (this.billsummaryService.getBillSummary("", "").toString() !== BillSummary.fromUnitTest) {
            this.loadBillSummary();
        }
    }

    loadBillSummary() {
        this.result = {};

        this.billsummaryService.getBillSummary(this.tabKey, this.parameters)
            .subscribe(res => this.result = res,
            err => this.appService.logError(this, err)
            );


        this.enableLoader = false;
    }
}