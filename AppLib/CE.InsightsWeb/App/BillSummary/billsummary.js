"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var billsummary_service_1 = require("./billsummary.service");
var app_service_1 = require("../app.service");
var BillSummary = (function () {
    function BillSummary(billsummaryService, appService, elementRef) {
        this.enableLoader = true;
        this.elementRef = elementRef;
        this.params = appService.getParams();
        this.parameters = this.params[0].parameters;
        this.tabKey = this.params[0].tabKey;
        this.errorMessage = this.params[0].errorMessage;
        this.billsummaryService = billsummaryService;
        this.appService = appService;
        if (this.billsummaryService.res !== undefined) {
            this.result = billsummaryService.res;
            this.fromUTest = this.billsummaryService.fromUnitTest;
        }
    }
    BillSummary_1 = BillSummary;
    BillSummary.prototype.ngOnInit = function () {
        if (this.billsummaryService.getBillSummary("", "").toString() !== BillSummary_1.fromUnitTest) {
            this.loadBillSummary();
        }
    };
    BillSummary.prototype.loadBillSummary = function () {
        var _this = this;
        this.result = {};
        this.billsummaryService.getBillSummary(this.tabKey, this.parameters)
            .subscribe(function (res) { return _this.result = res; }, function (err) { return _this.appService.logError(_this, err); });
        this.enableLoader = false;
    };
    BillSummary.fromUnitTest = "unittest";
    BillSummary = BillSummary_1 = __decorate([
        core_1.Component({
            selector: 'billsummary',
            template: "<Loader *ngIf=\"enableLoader\"></Loader><div *ngIf=\"!enableLoader\"><div *ngIf=\"errorOccurred\"><div class=\"panel panel-default\"><div class=\"alert alert-danger\"><span class=\"glyphicon glyphicon-exclamation-sign xs-mr-10\" aria-hidden=\"true\"></span><strong>{{errorMessage}}</strong> <span class=\"sr-only\">Error:</span></div></div></div><div *ngIf=\"!errorOccurred\" class=\"panel panel-default\"><div *ngIf=\"result.ShowTitle == 'true' && result.Title !== ''\" class=\"panel-heading\"><h1 class=\"panel-title\" role=\"presentation\">{{result.Title}}</h1></div><div *ngIf=\"result.NoBills == true\" class=\"alert alert-warning\"><span class=\"glyphicon glyphicon-alert xs-mr-10\"></span> <strong>{{result.NoBillsText}}</strong></div><div *ngIf=\"result.ShowIntro == 'true' && result.ShowIntro !== ''\" class=\"intro-text\" style=\"white-space: pre-line\">{{result.IntroText}}</div><ul *ngIf=\"result.NoBills !== true\" class=\"bill-statement list-group\"><li class=\"list-group-item\" *ngIf=\"result.TotalGasUsed > 0 && result.Commodities.indexOf(('gas')) !== -1\">{{result.TotalGasUse}}<p>{{result.TotalGasUsed}} {{result.GasUOM}}</p></li><li class=\"list-group-item\" *ngIf=\"result.TotalElectricityUsed > 0 && result.Commodities.indexOf(('electric')) !== -1\">{{result.TotalElectricUse}}<p>{{result.TotalElectricityUsed}} {{result.ElectricUOM}}</p></li><li class=\"list-group-item\" *ngIf=\"result.TotalWaterUsed > 0 && result.Commodities.indexOf(('water')) !== -1\">{{result.TotalWaterUse}}<p>{{result.TotalWaterUsed}} {{result.WaterUOM}}</p></li><li class=\"list-group-item\">{{result.AvgDailyCostText}}<p>{{result.AvgDailyCost}}</p></li><li class=\"list-group-item\">{{result.NumberOfDaysText}}<p>{{result.NumberOfDays}}</p></li><li class=\"list-group-item\">{{result.AvgTempText}}<p>{{result.AvgTemp}}&deg;{{result.TempFormat}}</p></li><li class=\"list-group-item\"><strong>{{result.AmountDueText}} {{result.DueDate}}</strong><p><strong>{{result.AmountDue}}</strong></p></li></ul><div *ngIf=\"result.ShowFooterText === 'true' || result.ShowFooterLink === 'true' || result.ShowBillHistoryLink === 'true'\" class=\"panel-footer\"><div *ngIf=\"result.ShowBillHistoryLink === 'true'\"><a href=\"{{result.BillHistoryLink}}\">{{result.ViewBillHistory}}</a></div><div *ngIf=\"result.ShowFooterText === 'true'\" class=\"footertext\">{{result.Footer}}</div><div *ngIf=\"result.ShowFooterLink === 'true'\" class=\"footerlink\"><a href=\"{{result.FooterLink}}\">{{result.FooterLinkText}}</a></div></div></div></div>",
            providers: [billsummary_service_1.BillSummaryService, app_service_1.AppService]
        }),
        __metadata("design:paramtypes", [billsummary_service_1.BillSummaryService, app_service_1.AppService, core_1.ElementRef])
    ], BillSummary);
    return BillSummary;
    var BillSummary_1;
}());
exports.BillSummary = BillSummary;

//# sourceMappingURL=billsummary.js.map
