"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var profile_service_1 = require("./profile.service");
var app_service_1 = require("../app.service");
var LongProfile = (function () {
    function LongProfile(profileService, appService, elementRef) {
        this.deepLinkWidgetTab = "";
        this.deepLinkWidgetSection = "";
        this.enableLoader = true;
        this.requiredValidationEnabled = false;
        this.deepLinkEnabled = true;
        this.elementRef = elementRef;
        this.params = appService.getParams();
        this.deepLinkVariables = appService.getDeepLinkVariables();
        this.parameters = this.params[0].parameters;
        this.tabKey = this.params[0].tabKey;
        this.errorMessage = this.params[0].errorMessage;
        this.profileService = profileService;
        this.appService = appService;
        this.isIos = this.appService.useRegularDropdown();
        this.setFocus = false;
        if (this.profileService.res !== undefined) {
            this.result = this.profileService.res;
            this.fromUTest = this.profileService.fromUnitTest;
        }
    }
    LongProfile_1 = LongProfile;
    LongProfile.prototype.ngOnInit = function () {
        if (this.deepLinkVariables[0].deepLinkWidget === LongProfile_1.widgetKey) {
            this.deepLinkWidgetTab = this.deepLinkVariables[0].deepLinkWidgetTab;
            this.deepLinkWidgetSection = this.deepLinkVariables[0].deepLinkWidgetSection;
            this.appService.clearGlobalVariables(LongProfile_1.widgetKey);
        }
        else {
            this.deepLinkEnabled = false;
        }
        if (this.profileService.getLongProfile("", "", "").toString() !== LongProfile_1.fromUnitTest) {
            this.loadProfile(this.deepLinkWidgetTab, this.deepLinkWidgetSection);
        }
    };
    LongProfile.prototype.loadProfile = function (t, s) {
        var _this = this;
        this.getTab(0, t, "", s);
        setTimeout(function () { return _this.deepLinkEnabled = false; }, 2000);
    };
    LongProfile.prototype.getTab = function (tIndex, tKey, tName, sectionKey) {
        var _this = this;
        this.profileService.getLongProfile(this.tabKey, tKey, this.parameters)
            .subscribe(function (res) { return _this.result = res; }, function (err) { return _this.appService.logError(_this, err); }, function () { return _this.disableLoader(tIndex, tKey, tName, sectionKey); });
    };
    LongProfile.prototype.getOverview = function (column) {
        var tabs = this.result.Tabs;
        this.colOneCount = 4;
        if (this.result.ShowOverviewTabColumnOneCount != null && this.result.ShowOverviewTabColumnOneCount.length > 0) {
            this.colOneCount = parseInt(this.result.ShowOverviewTabColumnOneCount) + 1;
        }
        var overview;
        if (column === 1) {
            overview = tabs.slice(1, this.colOneCount);
        }
        else {
            overview = tabs.slice(this.colOneCount);
        }
        return overview;
    };
    LongProfile.prototype.disableLoader = function (tIndex, tKey, tName, sectionKey) {
        var _this = this;
        if (!this.fromUTest) {
            setTimeout(function () { return _this.appService.setHtml(_this.elementRef); }, 1);
            setTimeout(function () { return _this.appService.setIframeExternalClass(_this.elementRef); }, 0);
        }
        this.enableLoader = false;
        if (this.result.ShowOverviewTab === "true") {
            this.tabSeed = 1;
        }
        else {
            this.tabSeed = 0;
        }
        if (tKey === "") {
            this.selectedTabIndex = 0;
            this.selectedTabKey = this.result.Tabs[0].Key;
            this.selectedTabName = this.result.Tabs[0].Name;
        }
        else {
            this.selectedTabIndex = tIndex;
            this.selectedTabKey = tKey;
            this.selectedTabName = tName;
        }
        this.saveButtonLabel = this.result.SaveButtonLabel;
        this.getSections(this.selectedTabKey, sectionKey);
        this.showHidePagerButtons();
        if (!this.fromUTest && this.selectedTabKey !== 'tab.overview') {
            setTimeout(function () { return _this.focusElement(); }, 0);
        }
    };
    LongProfile.prototype.getSections = function (tKey, sectionKey) {
        this.saved = false;
        var tbs = this.result.Tabs;
        for (var t in tbs) {
            if (tbs.hasOwnProperty(t)) {
                if (tbs[t].Sections !== null) {
                    if (tbs[t].Key === tKey) {
                        this.sections = tbs[t].Sections;
                        this.sectionCount = tbs[t].Sections.length;
                        if (sectionKey === "") {
                            this.getQuestions(this.sections[0].Key);
                        }
                        else {
                            this.getQuestions(sectionKey);
                        }
                    }
                }
            }
        }
        this.selectedTabKey = tKey;
    };
    LongProfile.prototype.getQuestions = function (sectionKey) {
        var _this = this;
        this.saved = false;
        var secs = this.sections;
        for (var s in secs) {
            if (secs.hasOwnProperty(s)) {
                if (secs[s].Key === sectionKey) {
                    this.questions = secs[s].Questions;
                    this.selectedSectionIndex = parseInt(s) + 1;
                    this.selectedSection = sectionKey;
                    this.selectedSectionTitle = secs[s].SectionTitle;
                    this.selectedSectionSubtitle = secs[s].SectionSubtitle;
                    this.selectedSectionImageUrl = secs[s].ImageUrl;
                    this.selectedSectionImageTitle = secs[s].ImageTitle;
                    this.selectedSectionDescription = secs[s].SectionDescription;
                    this.selectedSectionMarkdown = secs[s].SectionMarkdown;
                    break;
                }
            }
        }
        if (!this.fromUTest) {
            setTimeout(function () { return _this.focusElement(); }, 2000);
        }
        this.showHidePagerButtons();
    };
    LongProfile.prototype.getOptionText = function (com, q) {
        return q.Options.Text[com];
    };
    LongProfile.prototype.updateAnswer = function (key, value) {
        if (value !== "") {
            this.setFocus = false;
            var tabs = this.result.Tabs;
            for (var t in tabs) {
                if (tabs.hasOwnProperty(t)) {
                    if (tabs[t].Key === this.selectedTabKey) {
                        var sections = tabs[t].Sections;
                        for (var s in sections) {
                            if (sections.hasOwnProperty(s)) {
                                if (sections[s].Key === this.selectedSection) {
                                    var questions = sections[s].Questions;
                                    for (var q in questions) {
                                        if (questions.hasOwnProperty(q)) {
                                            if (questions[q].Key === key) {
                                                questions[q].Answer = value;
                                                return;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    };
    LongProfile.prototype.clickTab = function (tIndex, tKey, tName, sectionKey) {
        this.deepLinkEnabled = false;
        this.getTab(tIndex, tKey, tName, sectionKey);
    };
    LongProfile.prototype.dropDownSelectText = function (ans, q) {
        if (ans !== "") {
            for (var v in q.Options.Value) {
                if (q.Options.Value.hasOwnProperty(v)) {
                    if (q.Options.Value[v] === ans) {
                        return this.getOptionText(v, q);
                    }
                }
            }
        }
        else {
            return this.result.DropDownSelectText;
        }
        return null;
    };
    LongProfile.prototype.optionSelected = function (q, o) {
        if (q.Answer === o) {
            return true;
        }
        else {
            return false;
        }
    };
    LongProfile.prototype.sectionSelected = function (s) {
        if (s.Key === this.selectedSection) {
            return true;
        }
        else {
            return false;
        }
    };
    LongProfile.prototype.dropDownSectionText = function (s) {
        for (var v in s) {
            if (s.hasOwnProperty(v)) {
                if (s[v].Key === this.selectedSection) {
                    return s[v].SectionTitle;
                }
            }
        }
    };
    LongProfile.prototype.isTextBox = function (q) {
        var returnVal = false;
        if (q.Type === LongProfile_1.decimal || q.Type === LongProfile_1.integer) {
            if (q.Max > 50) {
                returnVal = true;
            }
        }
        else if (q.Type === LongProfile_1.text) {
            return true;
        }
        return returnVal;
    };
    LongProfile.prototype.isListBox = function (q) {
        var returnVal = false;
        if (q.Type === LongProfile_1.list) {
            returnVal = true;
        }
        else if (q.Type === LongProfile_1.decimal || q.Type === LongProfile_1.integer) {
            if (q.Max > 1 && q.Max < 51) {
                returnVal = true;
            }
        }
        return returnVal;
    };
    LongProfile.prototype.isRadioButton = function (q) {
        var returnVal = false;
        if (q.Type === LongProfile_1.boolean) {
            returnVal = true;
        }
        else if (q.Type === LongProfile_1.decimal || q.Type === LongProfile_1.integer) {
            if (parseInt(q.Max) === 1) {
                returnVal = true;
            }
        }
        return returnVal;
    };
    LongProfile.prototype.isTabActive = function (tabKey) {
        var returnVal = false;
        if (!this.deepLinkEnabled && tabKey === this.selectedTabKey) {
            returnVal = true;
        }
        return returnVal;
    };
    LongProfile.prototype.isRadioButtonActive = function (q, a) {
        var returnVal = false;
        if (q.Answer === a) {
            returnVal = true;
        }
        return returnVal;
    };
    LongProfile.prototype.isDeepLink = function () {
        var returnVal;
        if (this.deepLinkEnabled && this.deepLinkVariables[0].deepLinkWidget === LongProfile_1.widgetKey) {
            returnVal = true;
        }
        else {
            returnVal = false;
        }
        return returnVal;
    };
    LongProfile.prototype.isDeepLinkTab = function (tKey) {
        var returnVal;
        if (this.deepLinkEnabled && this.deepLinkVariables[0].deepLinkWidget === LongProfile_1.widgetKey && this.deepLinkVariables[0].deepLinkWidgetTab === tKey) {
            returnVal = true;
        }
        else {
            returnVal = false;
        }
        return returnVal;
    };
    LongProfile.prototype.pageBack = function () {
        if (this.selectedSectionIndex === 1 && this.selectedTabIndex !== this.tabSeed) {
            this.selectedTabIndex -= 1;
            var tab = this.result.Tabs[this.selectedTabIndex];
            this.getTab(this.selectedTabIndex, tab.Key, tab.Name, "");
        }
        else {
            this.selectedSectionIndex -= 1;
            this.getQuestions(this.sections[this.selectedSectionIndex - 1].Key);
            this.showHidePagerButtons();
        }
    };
    LongProfile.prototype.pageForward = function () {
        if (this.selectedSectionIndex === this.sectionCount && this.selectedTabIndex !== this.result.Tabs.length - 1) {
            this.selectedTabIndex += 1;
            var tab = this.result.Tabs[this.selectedTabIndex];
            this.getTab(this.selectedTabIndex, tab.Key, tab.Name, "");
        }
        else {
            this.selectedSectionIndex += 1;
            this.getQuestions(this.sections[this.selectedSectionIndex - 1].Key);
            this.showHidePagerButtons();
        }
    };
    LongProfile.prototype.focusElement = function () {
        var _this = this;
        var el = this.elementRef.nativeElement.querySelector("[id='iws_lp_section']");
        var focusEl = this.elementRef.nativeElement.querySelector("[id='focus-0']");
        setTimeout(function () { return scrollPage(_this.getOffset(el).top); }, 0);
        if (focusEl != null) {
            this.setFocus = true;
            setTimeout(function () { return focusEl.focus(); }, 0);
        }
    };
    LongProfile.prototype.getOffset = function (el) {
        el = el.getBoundingClientRect();
        return {
            left: el.left,
            top: el.top - 55
        };
    };
    LongProfile.prototype.checkFocus = function (z, i) {
        if (z === 0 && i === 0 && this.setFocus) {
            return true;
        }
        else {
            return false;
        }
    };
    LongProfile.prototype.showHidePagerButtons = function () {
        if (this.selectedTabIndex === this.tabSeed && this.selectedSectionIndex === 1) {
            this.showBackPagerButton = false;
        }
        else {
            this.showBackPagerButton = true;
        }
        if (this.selectedTabIndex === this.result.Tabs.length - 1 && this.selectedSectionIndex === this.sectionCount) {
            this.showForwardPagerButton = false;
        }
        else {
            this.showForwardPagerButton = true;
        }
    };
    LongProfile.prototype.postData = function () {
        var _this = this;
        this.saved = false;
        this.validateForm();
        if (this.formValid === true) {
            var data = {
                "profile": this.result,
                "parameters": this.parameters,
                "tabKey": this.selectedTabKey,
                "sectionKey": this.selectedSection
            };
            this.profileService.postLongProfile(data)
                .subscribe(function (res) { return _this.postResponse = res; }, function (err) { return _this.appService.logError(_this, err); }, function () { return _this.profileSaved(); });
        }
    };
    LongProfile.prototype.profileSaved = function () {
        this.saved = true;
        this.result.SaveButtonLabel = this.saveButtonLabel;
        this.postEvents();
    };
    LongProfile.prototype.postEvents = function () {
        var _this = this;
        var ei = [];
        ei.push({ key: "TabId", value: this.tabKey });
        this.appService.postEvents(ei, this.parameters, "Update Profile")
            .subscribe(function (res) { return _this.postEvent = res; }, function (err) { return _this.appService.logError(_this, err); });
    };
    LongProfile.prototype.validateForm = function () {
        var errorCount = 0;
        for (var q in this.questions) {
            if (this.questions.hasOwnProperty(q)) {
                if (this.requiredValidationEnabled === true && this.questions[q].Answer === "") {
                    this.questions[q].InvalidRequired = true;
                    errorCount += 1;
                }
                else {
                    this.questions[q].InvalidRequired = false;
                }
                if (this.isTextBox(this.questions[q])) {
                    var qs = this.questions[q];
                    var a = qs.Answer;
                    if (qs.Type == "text") {
                        if (a.length < parseInt(qs.Min) || a.length > parseInt(qs.Max)) {
                            this.questions[q].InvalidLength = true;
                            errorCount += 1;
                        }
                        else {
                            this.questions[q].InvalidLength = false;
                        }
                    }
                    else {
                        if (a !== "" && (this.appService.isNumeric(a) === false || (parseInt(a) < parseInt(qs.Min) || parseInt(a) > parseInt(qs.Max)))) {
                            this.questions[q].InvalidRange = true;
                            errorCount += 1;
                        }
                        else {
                            this.questions[q].InvalidRange = false;
                        }
                    }
                }
            }
        }
        if (errorCount === 0) {
            this.formValid = true;
        }
        else {
            this.formValid = false;
        }
    };
    LongProfile.decimal = "decimal";
    LongProfile.integer = "integer";
    LongProfile.boolean = "boolean";
    LongProfile.text = "text";
    LongProfile.list = "list";
    LongProfile.widgetKey = "longprofile";
    LongProfile.fromUnitTest = "unittest";
    LongProfile = LongProfile_1 = __decorate([
        core_1.Component({
            selector: 'longprofile',
            template: "<Loader *ngIf=\"enableLoader\"></Loader><div *ngIf=\"!enableLoader\" class=\"panel panel-default radius\"><div *ngIf=\"errorOccurred\"><div *ngIf=\"result.ShowTitle == 'true' && result.Title !== ''\" class=\"panel-heading\"><h1 role=\"presentation\">{{result.Title}}</h1></div><div class=\"alert alert-danger\"><span class=\"glyphicon glyphicon-hand-right\"></span> <strong>{{errorMessage}}</strong></div></div><div *ngIf=\"!errorOccurred\"><div *ngIf=\"result.ShowTitle == 'true'\" class=\"panel-heading\"><h1 role=\"presentation\">{{result.Title}}</h1></div><div *ngIf=\"result.ShowIntro == 'true'\" class=\"markdown\">{{result.IntroText}}</div><div id=\"iws_lp_progress_bar\" class=\"progress-bar-home-long\" style=\"padding: 0\"><nav class=\"navbar navbar-default navbar-icons\" role=\"navigation\"><ul class=\"nav navbar-nav nav-icons\" role=\"menubar\"><li *ngFor=\"let t of result.Tabs | objToArr; let i = index\" role=\"menuitem\" class=\"menu-icon\" [ngClass]=\"{'tab-deep-link tab-fade-in': isDeepLinkTab(t.Key), active: isTabActive(t.Key)}\"><a title=\"{{t.Name}}\" href=\"#\" class=\"navbar-link\" onclick=\"return false;\" (click)=\"clickTab(i, t.Key, t.Name, '')\"><span class=\"icon {{t.IconClass}}\"></span> <span class=\"navbar-link-text\">{{t.Name}}</span></a></li></ul></nav><div><div class=\"panel-default\"><div class=\"panel-body xs-mb-0 xs-pb-0\"><div class=\"col-xs-12 xs-pt-15 xs-pl-0\">{{result.OverviewText}}</div></div></div></div><div><div class=\"panel-default\"><div class=\"panel-body\"><div class=\"col-ms-6 xs-pl-0\"><div *ngIf=\"selectedTabKey == 'tab.overview'\" class=\"panel-body panel-profile panel-body-long-energy-profile xs-pt-0\" [ngClass]=\"{'div-deep-link div-fade-in': isDeepLink()}\" style=\"display: block\"><div class=\"overview\"><div *ngFor=\"let t of getOverview(1) | objToArr; let i = index\" class=\"overview-mod\" style=\"\"><div *ngFor=\"let s of t.Sections | objToArr; let y = index\"><div *ngIf=\"y==0\"><i class=\"icon icon-overview {{t.IconClass}}\"></i><p class=\"inline-block\">&nbsp;</p><h5 class=\"xs-mb-0 xs-ml-5\">{{t.Name}}</h5><p class=\"markdown\">{{t.Sections[0].SectionMarkdown}}</p></div><a href=\"#\" onclick=\"return false;\" (click)=\"getTab(i + 1, t.Key, t.Name, s.Key)\">{{s.SectionTitle}}</a></div></div></div></div></div><div class=\"col-ms-6\"><div *ngIf=\"selectedTabKey == 'tab.overview'\" class=\"panel-body panel-profile panel-body-long-energy-profile\" [ngClass]=\"{'div-deep-link div-fade-in': isDeepLink()}\" style=\"display: block\"><div class=\"overview\"><div *ngFor=\"let t of getOverview(2) | objToArr; let i = index\" class=\"overview-mod\" style=\"\"><div *ngFor=\"let s of t.Sections | objToArr; let y = index\"><div *ngIf=\"y==0\"><i class=\"icon icon-overview {{t.IconClass}}\"></i><p class=\"inline-block\">&nbsp;</p><h5 class=\"xs-mb-0 xs-ml-5\">{{t.Name}}</h5><p class=\"markdown\">{{t.Sections[0].SectionMarkdown}}</p></div><a href=\"#\" onclick=\"return false;\" (click)=\"getTab(i + colOneCount, t.Key, t.Name, s.Key)\">{{s.SectionTitle}}</a></div></div></div></div></div></div></div></div><div *ngIf=\"selectedTabKey != 'tab.overview'\" class=\"panel-profile\"><article class=\"article-profile col-ms-6 col-sm-6 col-lg-8 xs-pt-15\"><h4 *ngIf=\"sectionCount === 1\" id=\"iws_lp_section\" class=\"Select form-item-title\" role=\"presentation\">{{selectedSectionTitle}}</h4><div *ngIf=\"sectionCount > 1\" id=\"iws_lp_section\" class=\"form-group dropdown\"><h4 class=\"form-item-label\">Select a category below</h4><h5 class=\"Select form-item-label\">Refine your selection</h5><a *ngIf=\"!isIos\" href=\"#\" onclick=\"return false;\" role=\"button\" class=\"btn dropdown-btn dropdown-toggle\" [ngClass]=\"{'dropdown-deep-link dropdown-fade-in': isDeepLink()}\" data-toggle=\"dropdown\" aria-expanded=\"false\" style=\"\"><b id=\"iws_lp_selected_section\" class=\"Select\">{{dropDownSectionText(sections)}}</b> <span><img src=\"Content/Images/caret-darker.png\" height=\"6\" width=\"10\" alt=\"triangle image for button\"></span></a><ul *ngIf=\"!isIos\" id=\"iws_lp_sections\" class=\"dropdown-menu\" role=\"menu\"><li *ngFor=\"let s of sections | objToArr; let i = index\" class=\"save-list\" [attr.aria-labelledby]=\"s.SectionTitle\"><a href=\"#\" onclick=\"return false;\" (click)=\"getQuestions(s.Key)\">{{s.SectionTitle}}</a></li></ul><select *ngIf=\"isIos\" [(ngModel)]=\"selectedSection\" (change)=\"getQuestions($event.target.value)\" class=\"iosHeight\"><option *ngFor=\"let s of sections | objToArr; let i = index\" value=\"{{s.Key}}\">{{s.SectionTitle}}</option></select></div><div id=\"iws_lp_section_questions\" class=\"form-group\"><div *ngFor=\"let q of questions | objToArr; let z = index\"><div class=\"form-group\" *ngIf=\"isListBox(q)\"><div class=\"dropdown\"><h5 class=\"form-item-label\">{{q.Label}}</h5><a *ngIf=\"!isIos\" [id]=\"'focus-' + z\" href=\"#\" onclick=\"return false;\" role=\"button\" aria-level=\"1\" class=\"btn btn-default dropdown-toggle\" type=\"button\" data-toggle=\"dropdown\" aria-expanded=\"false\"><p class=\"select\">{{dropDownSelectText(q.Answer, q)}}</p><span><img src=\"Content/Images/caret-darker.png\" height=\"6\" width=\"10\" alt=\"triangle image for button\"></span></a><ul *ngIf=\"!isIos\" class=\"dropdown-menu\" role=\"menu\"><li *ngFor=\"let v of q.Options.Value | objToArr; let i = index\" class=\"save-list\"><a [id]=\"'focus-' + z\" href=\"#\" onclick=\"return false;\" (click)=\"updateAnswer(q.Key, v)\">{{ getOptionText(i, q) }}</a></li></ul></div><select *ngIf=\"isIos\" [id]=\"'focus-' + z\" (change)=\"updateAnswer(q.Key, $event.target.value)\" class=\"iosHeight\"><option value=\"\">{{result.DropDownSelectText}}</option><option *ngFor=\"let v of q.Options.Value | objToArr; let i = index\" [attr.selected]=\"optionSelected(q, v) ? '' : null\" value=\"{{v}}\">{{ getOptionText(i, q) }}</option></select><p *ngIf=\"q.InvalidRequired\" class=\"error\">{{result.NotCompleteMessage}}</p></div><div *ngIf=\"isTextBox(q)\"><div class=\"form-group\"><h5 class=\"form-item-label\">{{q.Label}}</h5><input [id]=\"'focus-' + z\" [(ngModel)]=\"q.Answer\" class=\"form-control energy-field\"></div><p *ngIf=\"q.InvalidRequired\" class=\"error\">{{result.NotCompleteMessage}}</p><p *ngIf=\"q.InvalidRange\" class=\"error\">{{q.RangeErrorMessage}}</p><p *ngIf=\"q.InvalidLength\" class=\"error\">{{q.InvalidLengthMessage}}</p></div><div class=\"form-group\" *ngIf=\"isRadioButton(q)\"><div><h5 class=\"form-item-label\">{{q.Label}}</h5><div class=\"btn-group\" data-toggle=\"buttons\"><label *ngFor=\"let v of q.Options.Value | objToArr; let i = index\" class=\"btn btn-default right\" [ngClass]=\"{active: isRadioButtonActive(q, v)}\" (click)=\"updateAnswer(q.Key, v)\" style=\"border: 1px solid let d4d4d4\"><input [id]=\"'focus-' + z\" type=\"radio\"> {{ getOptionText(i, q) }}</label></div></div><p *ngIf=\"q.InvalidRequired\" class=\"error\">{{result.NotCompleteMessage}}</p></div></div></div><div id=\"iws_lp_footer\" class=\"button-shell\" style=\"height: 60px; position: relative; clear: both\"><a id=\"iws_lp_save_msg\" *ngIf=\"saved\" style=\"display: table; text-decoration: none; cursor: pointer\" class=\"just-letting-you-know\">{{result.SavedMessage}}</a> <button id=\"iws_lp_save_button\" type=\"button\" class=\"btn action-btn xs-mt-10\" (click)=\"postData()\" tabindex=\"0\">{{result.SaveButtonLabel}}</button></div></article><aside class=\"aside-profile col-ms-6 col-sm-6 col-lg-4 ms-mt-30\"><div class=\"thumbnail\" style=\"padding: 0\"><img class=\"img-responsive hidden-xs\" alt=\"\" [attr.src]=\"selectedSectionImageUrl\"><div class=\"caption\"><h5 class=\"thumb-item-label\" id=\"iws_lp_section_subtitle\">{{selectedSectionSubtitle}}</h5><p id=\"iws_lp_section_description\">{{selectedSectionDescription}}</p></div></div></aside></div><nav *ngIf=\"selectedTabKey !== 'tab.overview'\" class=\"text-center pager\"><ul class=\"pagination pagination-sm\"><li class=\"acl-pager\" (click)=\"pageBack()\" *ngIf=\"showBackPagerButton\"><a onclick=\"return false;\" href=\"#\"><img src=\"Content/Images/icon-page-left.png\" alt=\"go back\"></a></li><li class=\"hidden-xs\" style=\"display: inline-flex\">{{selectedTabName}} ( {{selectedSectionIndex}} {{result.PageOf}} {{sectionCount}} ): {{selectedSectionTitle}}</li><li class=\"acl-pager\" (click)=\"pageForward()\" *ngIf=\"showForwardPagerButton\"><a onclick=\"return false;\" href=\"#\"><img src=\"Content/Images/icon-page-right.png\" alt=\"go forward\"></a></li></ul></nav><div *ngIf=\"result.ShowFooterText === 'true' || result.ShowFooterLink === 'true'\" class=\"panel-footer\"><div *ngIf=\"result.ShowFooterText === 'true'\" class=\"footertext\">{{result.Footer}}</div><div *ngIf=\"result.ShowFooterLink === 'true'\" class=\"footerlink dv-external-links\"><a href=\"{{result.FooterLink}}\">{{result.FooterLinkText}}</a></div></div></div></div></div>",
            providers: [profile_service_1.ProfileService, app_service_1.AppService]
        }),
        __metadata("design:paramtypes", [profile_service_1.ProfileService, app_service_1.AppService, core_1.ElementRef])
    ], LongProfile);
    return LongProfile;
    var LongProfile_1;
}());
exports.LongProfile = LongProfile;

//# sourceMappingURL=longprofile.js.map
