﻿import "rxjs/add/operator/map";
import { Injectable, Input } from "@angular/core"
import { Http } from "@angular/http";

@Injectable()
export class ProfileService {
    @Input() res: Object;
    @Input() fromUnitTest: Boolean;
    http: Http;
    constructor(http: Http) {
        this.http = http;
    }

    getLongProfile(tabKey, tKey, params) {
        return this.http.get(`./LongProfile/GetProfile?tabkey=${tabKey}&profileTabKey=${tKey}&parameters=${window.btoa(params)}`).map(res => res.json());
    }

    postLongProfile(data) {
        return this.http.post("./LongProfile/PostProfile", JSON.stringify(data)).map(res => res.json());
    }

    getProfile(tabKey, params) {
        return this.http.get(`./Profile/GetProfile?tabkey=${tabKey}&parameters=${window.btoa(params)}`).map(res => res.json());
    }

    postProfile(data) {
        return this.http.post("./Profile/PostProfile", JSON.stringify(data)).map(res => res.json());
    }
}

