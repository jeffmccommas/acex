﻿import { Component, ElementRef, OnInit } from "@angular/core";
import { ProfileService } from "./profile.service";
import { AppService, IAppParams, IDeepLinkVariables } from "../app.service";

@Component({
    selector: 'longprofile',
    templateUrl: 'App/Profile/longprofile.html',
    providers: [ProfileService, AppService]
})

export class LongProfile implements OnInit {
    elementRef: ElementRef;
    result: any;
    postResponse: Object;
    postEvent: Object;
    error: Object;
    static errorMessage: string;
    static decimal = "decimal"; 
    static integer = "integer";
    static boolean = "boolean";
    static text = "text";
    static list = "list";
    static widgetKey = "longprofile";
    static fromUnitTest = "unittest";
    enableLoader: Boolean;
    errorOccurred: Boolean;
    errorMessage: string;
    showBackPagerButton: Boolean;
    showForwardPagerButton: Boolean;
    tabSeed: number;
    sectionCount: number;
    selectedTabIndex: number;
    selectedTabKey: string;
    selectedTabName: string;
    selectedSectionIndex: number;
    selectedSection: string;
    selectedSectionTitle: string;
    selectedSectionSubtitle: string;
    selectedSectionImageUrl: string;
    selectedSectionImageTitle: string;
    selectedSectionDescription: string;
    selectedSectionMarkdown: string;
    sections: Object;
    questions: Object;
    profileService: ProfileService;
    appService: AppService;
    params: Array<IAppParams>;
    deepLinkVariables: Array<IDeepLinkVariables>;
    saved: Boolean;
    deepLinkEnabled: Boolean;
    formValid: Boolean;
    requiredValidationEnabled: Boolean; 
    saveButtonLabel: string;
    deepLinkWidgetTab = "";
    deepLinkWidgetSection = "";
    parameters: string;
    tabKey: string;
    fromUTest: Boolean;
    colOneCount: number;
    isIos: Boolean;
    setFocus: Boolean;

    constructor(profileService: ProfileService, appService: AppService, elementRef: ElementRef) {
        this.enableLoader = true;
        this.requiredValidationEnabled = false;
        this.deepLinkEnabled = true;
        this.elementRef = elementRef;
        this.params = appService.getParams();
        this.deepLinkVariables = appService.getDeepLinkVariables();
        this.parameters = this.params[0].parameters;
        this.tabKey = this.params[0].tabKey;
        this.errorMessage = this.params[0].errorMessage;
        this.profileService = profileService;
        this.appService = appService;
        this.isIos = this.appService.useRegularDropdown();
        //this.isIos = true;
        this.setFocus = false;
        if (this.profileService.res !== undefined) {
            this.result = this.profileService.res;
            this.fromUTest = this.profileService.fromUnitTest;
        }
    }

    ngOnInit() {
        if (this.deepLinkVariables[0].deepLinkWidget === LongProfile.widgetKey) {
            this.deepLinkWidgetTab = this.deepLinkVariables[0].deepLinkWidgetTab;
            this.deepLinkWidgetSection = this.deepLinkVariables[0].deepLinkWidgetSection;
            this.appService.clearGlobalVariables(LongProfile.widgetKey);
        } else {
            this.deepLinkEnabled = false;
        }

        if (this.profileService.getLongProfile("", "", "").toString() !== LongProfile.fromUnitTest) {
            this.loadProfile(this.deepLinkWidgetTab, this.deepLinkWidgetSection);
        }
    }

    loadProfile(t, s) {
        this.getTab(0, t, "", s);
        setTimeout(() => this.deepLinkEnabled = false, 2000);
    }

    getTab(tIndex, tKey, tName, sectionKey) {
        this.profileService.getLongProfile(this.tabKey, tKey, this.parameters)
            .subscribe(res => this.result = res,
            err => this.appService.logError(this, err),
            //() => console.log(this.result),
            () => this.disableLoader(tIndex, tKey, tName, sectionKey)
            );
    }

    getOverview(column) {
        const tabs = this.result.Tabs;
        this.colOneCount = 4;
        if (this.result.ShowOverviewTabColumnOneCount != null && this.result.ShowOverviewTabColumnOneCount.length > 0) {
             this.colOneCount = parseInt(this.result.ShowOverviewTabColumnOneCount) + 1;
        }

        var overview;
        if (column === 1) {
            overview = tabs.slice(1, this.colOneCount);
        } else {
            overview = tabs.slice(this.colOneCount);
        }

        return overview;
    }

    disableLoader(tIndex, tKey, tName, sectionKey) {
        if (!this.fromUTest) {
            setTimeout(() => this.appService.setHtml(this.elementRef), 1);
            setTimeout(() => this.appService.setIframeExternalClass(this.elementRef), 0);  
        }
        this.enableLoader = false;

        if (this.result.ShowOverviewTab === "true") {
            this.tabSeed = 1;
        } else {
            this.tabSeed = 0;
        }

        if (tKey === "") {
            this.selectedTabIndex = 0;
            this.selectedTabKey = this.result.Tabs[0].Key;
            this.selectedTabName = this.result.Tabs[0].Name;
        } else {
            this.selectedTabIndex = tIndex;
            this.selectedTabKey = tKey;
            this.selectedTabName = tName;
        }
        this.saveButtonLabel = this.result.SaveButtonLabel;
        this.getSections(this.selectedTabKey, sectionKey);
        this.showHidePagerButtons();

        if (!this.fromUTest && this.selectedTabKey !== 'tab.overview') {
            //setTimeout(() => this.elementRef.nativeElement.querySelector("[id='focus-0']").focus(), 100);
            setTimeout(() => this.focusElement(), 0);
        }  
    }

    getSections(tKey, sectionKey) {
        this.saved = false;
        const tbs = this.result.Tabs;
        for (let t in tbs) {
            if (tbs.hasOwnProperty(t)) {
                if (tbs[t].Sections !== null) {
                    if (tbs[t].Key === tKey) {
                        this.sections = tbs[t].Sections;
                        this.sectionCount = tbs[t].Sections.length;
                        if (sectionKey === "") {
                            this.getQuestions(this.sections[0].Key);
                        } else {
                            this.getQuestions(sectionKey);
                        }
                    }
                } 
            }
        }
        this.selectedTabKey = tKey;
    }

    getQuestions(sectionKey) {
        this.saved = false;
        const secs = this.sections;
        for (let s in secs) {
            if (secs.hasOwnProperty(s)) {
                if (secs[s].Key === sectionKey) {
                    this.questions = secs[s].Questions;
                    this.selectedSectionIndex = parseInt(s) + 1;
                    this.selectedSection = sectionKey;
                    this.selectedSectionTitle = secs[s].SectionTitle;
                    this.selectedSectionSubtitle = secs[s].SectionSubtitle;
                    this.selectedSectionImageUrl = secs[s].ImageUrl;
                    this.selectedSectionImageTitle = secs[s].ImageTitle;
                    this.selectedSectionDescription = secs[s].SectionDescription;
                    this.selectedSectionMarkdown = secs[s].SectionMarkdown;
                    break;
                }
            }
        }

        if (!this.fromUTest) {
            //setTimeout(() => this.elementRef.nativeElement.querySelector("[id='focus-0']").focus(), 100);
            setTimeout(() => this.focusElement(), 2000);
        }   

        this.showHidePagerButtons();
    }

    getOptionText(com, q) {
        return q.Options.Text[com];
    }

    updateAnswer(key, value) {
        if (value !== "")
        {
            this.setFocus = false;
            const tabs = this.result.Tabs;
            for (let t in tabs) {
                if (tabs.hasOwnProperty(t)) {
                    if (tabs[t].Key === this.selectedTabKey) {
                        const sections = tabs[t].Sections;
                        for (let s in sections) {
                            if (sections.hasOwnProperty(s)) {
                                if (sections[s].Key === this.selectedSection) {
                                    const questions = sections[s].Questions;
                                    for (let q in questions) {
                                        if (questions.hasOwnProperty(q)) {
                                            if (questions[q].Key === key) {
                                                questions[q].Answer = value;
                                                return;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    clickTab(tIndex, tKey, tName, sectionKey) {
        this.deepLinkEnabled = false;
        this.getTab(tIndex, tKey, tName, sectionKey);
    }

    dropDownSelectText(ans, q) {
        if (ans !== "") {
            for (let v in q.Options.Value) {
                if (q.Options.Value.hasOwnProperty(v)) {
                    if (q.Options.Value[v] === ans) {
                        return this.getOptionText(v, q);
                    }
                }
            }
        }
        else {
            return this.result.DropDownSelectText;
        }
        return null;
    }

    optionSelected(q, o) {
        if (q.Answer === o) {
            return true;
        }
        else 
        {
            return false;
        }
    }

    sectionSelected(s) {
        if (s.Key === this.selectedSection) {
            return true;
        }
        else {
            return false;
        }
    }

    dropDownSectionText(s) {
        for (let v in s) {
            if (s.hasOwnProperty(v)) {
                if (s[v].Key === this.selectedSection) {
                    return s[v].SectionTitle;
                }
            }
        }
    }

    isTextBox(q) {
        let returnVal = false;
        if (q.Type === LongProfile.decimal || q.Type === LongProfile.integer) {
            if (q.Max > 50) {
                returnVal = true;
            }
        }
        else if (q.Type === LongProfile.text) {
            return true;
        }
        return returnVal;
    }

    isListBox(q) {
        let returnVal = false;
        if (q.Type === LongProfile.list) {
            returnVal = true;
        }
        else if (q.Type === LongProfile.decimal || q.Type === LongProfile.integer) {
            if (q.Max > 1 && q.Max < 51) {
                returnVal = true;
            }
        }
        return returnVal;
    }

    isRadioButton(q) {
        let returnVal = false;
        if (q.Type === LongProfile.boolean) {
            returnVal = true;
        }
        else if (q.Type === LongProfile.decimal || q.Type === LongProfile.integer) {
            if (parseInt(q.Max) === 1) {
                returnVal = true;
            }
        }
        return returnVal;
    }

    isTabActive(tabKey) {
        let returnVal = false;
        if (!this.deepLinkEnabled && tabKey === this.selectedTabKey) {
            returnVal = true;
        }
        return returnVal;
    }

    isRadioButtonActive(q, a) {
        let returnVal = false;
        if (q.Answer === a) {
            returnVal = true;
        }
        return returnVal;
    }

    isDeepLink() {
        let returnVal: Boolean;
        if (this.deepLinkEnabled && this.deepLinkVariables[0].deepLinkWidget === LongProfile.widgetKey) {
            returnVal = true;

        } else {
            returnVal = false;
        }
        return returnVal;
    }

    isDeepLinkTab(tKey) {
        let returnVal: Boolean;
        if (this.deepLinkEnabled && this.deepLinkVariables[0].deepLinkWidget === LongProfile.widgetKey && this.deepLinkVariables[0].deepLinkWidgetTab === tKey) {
            returnVal = true;

        } else {
            returnVal = false; 
        }
        return returnVal;
    }
    
    pageBack() {
        if (this.selectedSectionIndex === 1 && this.selectedTabIndex !== this.tabSeed) {
            this.selectedTabIndex -= 1;
            const tab = this.result.Tabs[this.selectedTabIndex];
            this.getTab(this.selectedTabIndex, tab.Key, tab.Name, "");
        } else {
            this.selectedSectionIndex -= 1;
            this.getQuestions(this.sections[this.selectedSectionIndex - 1].Key);  
            this.showHidePagerButtons();
        }

    }

    pageForward() {
        if (this.selectedSectionIndex === this.sectionCount && this.selectedTabIndex !== this.result.Tabs.length - 1) {
            this.selectedTabIndex += 1;
            const tab = this.result.Tabs[this.selectedTabIndex];
            this.getTab(this.selectedTabIndex, tab.Key, tab.Name, "");
        } else {
            this.selectedSectionIndex += 1;
            this.getQuestions(this.sections[this.selectedSectionIndex - 1].Key);
            this.showHidePagerButtons();
        }
    }

    focusElement() {
        var el = this.elementRef.nativeElement.querySelector("[id='iws_lp_section']");
        var focusEl = this.elementRef.nativeElement.querySelector("[id='focus-0']");

        setTimeout(() => scrollPage(this.getOffset(el).top), 0);

        if (focusEl != null) {
            this.setFocus = true;
            setTimeout(() => focusEl.focus(), 0);
        }
    }

    getOffset(el) {
        el = el.getBoundingClientRect();
        return {
            left: el.left,
            top: el.top - 55
        }
    }

    checkFocus(z, i) {
        if (z === 0 && i === 0 && this.setFocus) {
            return true;
        } else {
            return false;
        }
    }

    showHidePagerButtons() {
        if (this.selectedTabIndex === this.tabSeed && this.selectedSectionIndex === 1) {
            this.showBackPagerButton = false;
        } else {
            this.showBackPagerButton = true;
        }

        if (this.selectedTabIndex === this.result.Tabs.length - 1 && this.selectedSectionIndex === this.sectionCount) {
            this.showForwardPagerButton = false;
        } else {
            this.showForwardPagerButton = true;
        }
    }

    postData() {
        this.saved = false;
        this.validateForm();

        if (this.formValid === true) {
            const data = {
                "profile": this.result,
                "parameters": this.parameters,
                "tabKey": this.selectedTabKey,
                "sectionKey": this.selectedSection
            };

            //this.result.SaveButtonLabel = "Saving...";
            this.profileService.postLongProfile(data)
                .subscribe(res => this.postResponse = res,
                err => this.appService.logError(this, err),
                () => this.profileSaved()
                );
        }
    }

    profileSaved() {
        this.saved = true;
        this.result.SaveButtonLabel = this.saveButtonLabel;
        this.postEvents();
    }

    postEvents() {
        const ei = [];
        ei.push({ key: "TabId", value: this.tabKey });

        this.appService.postEvents(ei, this.parameters, "Update Profile")
            .subscribe(res => this.postEvent = res,
            err => this.appService.logError(this, err)
            );
    }

    validateForm() {
        let errorCount = 0;
        for (let q in this.questions) {
            if (this.questions.hasOwnProperty(q)) {
                if (this.requiredValidationEnabled === true && this.questions[q].Answer === "") {
                    this.questions[q].InvalidRequired = true;
                    errorCount += 1;
                } else {
                    this.questions[q].InvalidRequired = false;
                }
                if (this.isTextBox(this.questions[q])) {
                    const qs = this.questions[q];
                    const a = qs.Answer;
                    if (qs.Type == "text") {
                        if (a.length < parseInt(qs.Min) || a.length > parseInt(qs.Max)) {
                            this.questions[q].InvalidLength = true;
                            errorCount += 1;
                        } else {
                            this.questions[q].InvalidLength = false;
                        }
                    } else {
                        if (a !== "" && (this.appService.isNumeric(a) === false || (parseInt(a) < parseInt(qs.Min) || parseInt(a) > parseInt(qs.Max)))) {
                            this.questions[q].InvalidRange = true;
                            errorCount += 1;
                        } else {
                            this.questions[q].InvalidRange = false;
                        }
                    }

                }
            }
        }

        if (errorCount === 0) {
            this.formValid = true;
        } else {
            this.formValid = false;
        }
    }
}






