﻿import { Component, ElementRef, OnInit } from "@angular/core";
import { ProfileService } from "./profile.service";
import { AppService, IAppParams } from "../app.service";

@Component({
    selector: 'profile',
    templateUrl: 'App/Profile/profile.html',
    providers: [ProfileService, AppService]
})
export class Profile implements OnInit {
    static fromUnitTest = "unittest";
    elementRef: ElementRef;
    result: any;
    postResponse: Object;
    error: Object;
    postEvent: Object;
    static parameters: string;
    static tabKey: string;
    errorOccurred: Boolean;
    errorMessage: string;
    enableLoader: Boolean;
    focusId: string;
    questions: Object;
    profileService: ProfileService;
    params: Array<IAppParams>;
    saved: Boolean;
    formValid: Boolean;
    appService: AppService;
    completeness: any;
    fromUTest: Boolean;
    isIos: Boolean;
    markdownComplete: Boolean;

    constructor(profileService: ProfileService, appService: AppService, elementRef: ElementRef) {
        this.elementRef = elementRef;
        this.enableLoader = true;
        this.params = appService.getParams();
        Profile.parameters = this.params[0].parameters;
        Profile.tabKey = this.params[0].tabKey;
        this.errorMessage = this.params[0].errorMessage;
        this.profileService = profileService;
        this.appService = appService;
        this.errorOccurred = false;
        this.isIos = this.appService.useRegularDropdown();
        //this.isIos = true;
        if (this.profileService.res !== undefined) {
            this.result = profileService.res;
            this.fromUTest = this.profileService.fromUnitTest;
        }
    }

    ngOnInit() {
        if (!this.fromUTest) {
            this.loadProfile();

        }
    }

    loadProfile() {
        this.result = {};
        if (!this.fromUTest) {
            this.profileService.getProfile(Profile.tabKey, Profile.parameters)
                .subscribe(res => this.result = res,
                error => this.appService.logError(this, error),
                () => this.disableLoader(false)
                );
        }
    }

    disableLoader(setFocus) {

        if (this.result !== null || (this.result.Tabs !== null && this.result.Completeness !== 0)) {
            this.completeness = this.result.Completeness + "%";
            if (this.result.Completeness < 100) {
                this.getQuestions();
            }
        }

        if (!this.fromUTest) {
            setTimeout(() => this.appService.setHtml(this.elementRef), 100);
            setTimeout(() => this.appService.setIframeExternalClass(this.elementRef), 100);
            if (setFocus) {
                setTimeout(() => this.focusElement(-1), 200);
            }
        }

        this.enableLoader = false;

    }

    focusElement(index) {
        var el;
        if (index == -1) {
            el = this.elementRef.nativeElement.querySelector("[id='iws_pr_top']");
            var focusEl = this.elementRef.nativeElement.querySelector("[id='validation-" + this.questions[0].Key + "']");
            if (focusEl != null)
            {
                setTimeout(() => focusEl.focus(), 200);
            }
            scrollPage(this.getOffset(el).top);
        }
        else {
            el = this.elementRef.nativeElement.querySelector("[id='validation-" + this.questions[index].Key + "']"); 
            setTimeout(() => el.focus(), 200);
            scrollPage(this.getOffset(el).top);
        }
    }

    getOffset(el) {
        el = el.getBoundingClientRect();
        return {
            left: el.left,
            top: el.top - 55
        }
    }

    getQuestions() {
        this.questions = this.result.Tabs[0].Sections[0].Questions;
    }

    getOptionText(com, q) {
        return q.Options.Text[com];
    }

    updateAnswer(key, value) {
        if (value != "") {
            const questions = this.questions;
            for (let q in questions) {
                if (questions.hasOwnProperty(q)) {
                    if (questions[q].Key === key) {
                        questions[q].Answer = value;
                        return;
                    }
                }
            }
        }
    }

    optionSelected(q, o) {
        if (q.Answer == o) {
            return true;
        }
        else {
            return false;
        }
    }

    dropDownSelectText(ans, q) {
        if (ans !== "") {
            for (let v in q.Options.Value) {
                if (q.Options.Value.hasOwnProperty(v)) {
                    if (q.Options.Value[v] === ans) {
                        return this.getOptionText(v, q);
                    }
                }
            }
        } else {
            return this.result.DropDownSelectText;
        }
    }


    isTextBox(q) {
        if (q.Type === "decimal" || q.Type === "integer") {
            if (q.Max > 50) {
                return true;
            }
        } else if (q.Type === "text") {
            return true;
        }
        return false;
    }

    isListBox(q) {
        if (q.Type === "list") {
            return true;
        } else if (q.Type === "decimal" || q.Type === "integer") {
            if (q.Max > 1 && q.Max < 51) {
                return true;
            }
        }
        return false;
    }

    isRadioButton(q) {
        if (q.Type === "boolean") {
            return true;
        } else if (q.Type === "decimal" || q.Type === "integer") {
            if (parseInt(q.Max) === 1) {
                return true;
            }
        }
        return false;
    }

    isRadioButtonActive(q, a) {
        let returnVal = false;
        if (q.Answer === a) {
            returnVal = true;
        }
        return returnVal;
    }

    postData() {
        this.saved = false;
        this.validateForm();

        if (this.formValid === true) {
            const data = {
                "profile": this.result,
                "parameters": Profile.parameters

            };

            this.profileService.postProfile(data)
                .subscribe(res => this.postResponse = res,
                err => this.appService.logError(this, err),
                () => this.profileSaved()
                );

        }
    }

    validateForm() {
        let errorCount = 0;
        let errorFound = false;
        for (let q in this.questions) {
            if (this.questions.hasOwnProperty(q)) {
                if (this.result.ShowRequiredKey === true && (this.questions[q].Answer === "") || !this.questions[q].Answer) {
                    this.questions[q].InvalidRequired = true;
                    errorFound = true;
                    errorCount += 1;
                } else {
                    this.questions[q].InvalidRequired = false;
                }
                if (this.isTextBox(this.questions[q]) && errorCount === 0) {
                    const qs = this.questions[q];
                    const a = qs.Answer;
                    if (qs.Type === "text") {
                        if (a.length < parseInt(qs.Min) || a.length > parseInt(qs.Max)) {
                            this.questions[q].InvalidLength = true;
                            errorFound = true;
                            errorCount += 1;
                        } else {
                            this.questions[q].InvalidLength = false;
                        }
                    } else if (errorCount === 0) {
                        if (a === "" || (this.appService.isNumeric(a) === false ||
                            (parseInt(a) < parseInt(qs.Min) || parseInt(a) > parseInt(qs.Max)))) {
                            this.questions[q].InvalidRange = true;
                            errorFound = true;
                            errorCount += 1;
                        } else {
                            this.questions[q].InvalidRange = false;
                        }
                    }
                }
            }

            if (errorCount == 1 && errorFound == true) {
                if (!this.fromUTest) {
                    setTimeout(() => this.focusElement(q), 200);                  
                    errorFound = false;
                }
            }
        }

        if (errorCount === 0) {
            this.formValid = true;
        } else {
            this.formValid = false;
        }
    }

    profileSaved() {
        this.saved = true;
        this.postEvents();
        if (!this.fromUTest) {
            this.profileService.getProfile(Profile.tabKey, Profile.parameters)
                .subscribe(res => this.result = res,
                error => this.appService.logError(this, error),
                () => this.disableLoader(true)
                );

        }
    }

    postEvents() {
        const ei = [];
        ei.push({ key: "TabId", value: Profile.tabKey });
        if (!this.fromUTest) {
            this.appService.postEvents(ei, Profile.parameters, "Update Profile")
                .subscribe(res => this.postEvent = res,
                err => this.appService.logError(this, err)
                );

        }
    }
}