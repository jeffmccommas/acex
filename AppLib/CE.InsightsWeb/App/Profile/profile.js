"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var profile_service_1 = require("./profile.service");
var app_service_1 = require("../app.service");
var Profile = (function () {
    function Profile(profileService, appService, elementRef) {
        this.elementRef = elementRef;
        this.enableLoader = true;
        this.params = appService.getParams();
        Profile_1.parameters = this.params[0].parameters;
        Profile_1.tabKey = this.params[0].tabKey;
        this.errorMessage = this.params[0].errorMessage;
        this.profileService = profileService;
        this.appService = appService;
        this.errorOccurred = false;
        this.isIos = this.appService.useRegularDropdown();
        if (this.profileService.res !== undefined) {
            this.result = profileService.res;
            this.fromUTest = this.profileService.fromUnitTest;
        }
    }
    Profile_1 = Profile;
    Profile.prototype.ngOnInit = function () {
        if (!this.fromUTest) {
            this.loadProfile();
        }
    };
    Profile.prototype.loadProfile = function () {
        var _this = this;
        this.result = {};
        if (!this.fromUTest) {
            this.profileService.getProfile(Profile_1.tabKey, Profile_1.parameters)
                .subscribe(function (res) { return _this.result = res; }, function (error) { return _this.appService.logError(_this, error); }, function () { return _this.disableLoader(false); });
        }
    };
    Profile.prototype.disableLoader = function (setFocus) {
        var _this = this;
        if (this.result !== null || (this.result.Tabs !== null && this.result.Completeness !== 0)) {
            this.completeness = this.result.Completeness + "%";
            if (this.result.Completeness < 100) {
                this.getQuestions();
            }
        }
        if (!this.fromUTest) {
            setTimeout(function () { return _this.appService.setHtml(_this.elementRef); }, 100);
            setTimeout(function () { return _this.appService.setIframeExternalClass(_this.elementRef); }, 100);
            if (setFocus) {
                setTimeout(function () { return _this.focusElement(-1); }, 200);
            }
        }
        this.enableLoader = false;
    };
    Profile.prototype.focusElement = function (index) {
        var el;
        if (index == -1) {
            el = this.elementRef.nativeElement.querySelector("[id='iws_pr_top']");
            var focusEl = this.elementRef.nativeElement.querySelector("[id='validation-" + this.questions[0].Key + "']");
            if (focusEl != null) {
                setTimeout(function () { return focusEl.focus(); }, 200);
            }
            scrollPage(this.getOffset(el).top);
        }
        else {
            el = this.elementRef.nativeElement.querySelector("[id='validation-" + this.questions[index].Key + "']");
            setTimeout(function () { return el.focus(); }, 200);
            scrollPage(this.getOffset(el).top);
        }
    };
    Profile.prototype.getOffset = function (el) {
        el = el.getBoundingClientRect();
        return {
            left: el.left,
            top: el.top - 55
        };
    };
    Profile.prototype.getQuestions = function () {
        this.questions = this.result.Tabs[0].Sections[0].Questions;
    };
    Profile.prototype.getOptionText = function (com, q) {
        return q.Options.Text[com];
    };
    Profile.prototype.updateAnswer = function (key, value) {
        if (value != "") {
            var questions = this.questions;
            for (var q in questions) {
                if (questions.hasOwnProperty(q)) {
                    if (questions[q].Key === key) {
                        questions[q].Answer = value;
                        return;
                    }
                }
            }
        }
    };
    Profile.prototype.optionSelected = function (q, o) {
        if (q.Answer == o) {
            return true;
        }
        else {
            return false;
        }
    };
    Profile.prototype.dropDownSelectText = function (ans, q) {
        if (ans !== "") {
            for (var v in q.Options.Value) {
                if (q.Options.Value.hasOwnProperty(v)) {
                    if (q.Options.Value[v] === ans) {
                        return this.getOptionText(v, q);
                    }
                }
            }
        }
        else {
            return this.result.DropDownSelectText;
        }
    };
    Profile.prototype.isTextBox = function (q) {
        if (q.Type === "decimal" || q.Type === "integer") {
            if (q.Max > 50) {
                return true;
            }
        }
        else if (q.Type === "text") {
            return true;
        }
        return false;
    };
    Profile.prototype.isListBox = function (q) {
        if (q.Type === "list") {
            return true;
        }
        else if (q.Type === "decimal" || q.Type === "integer") {
            if (q.Max > 1 && q.Max < 51) {
                return true;
            }
        }
        return false;
    };
    Profile.prototype.isRadioButton = function (q) {
        if (q.Type === "boolean") {
            return true;
        }
        else if (q.Type === "decimal" || q.Type === "integer") {
            if (parseInt(q.Max) === 1) {
                return true;
            }
        }
        return false;
    };
    Profile.prototype.isRadioButtonActive = function (q, a) {
        var returnVal = false;
        if (q.Answer === a) {
            returnVal = true;
        }
        return returnVal;
    };
    Profile.prototype.postData = function () {
        var _this = this;
        this.saved = false;
        this.validateForm();
        if (this.formValid === true) {
            var data = {
                "profile": this.result,
                "parameters": Profile_1.parameters
            };
            this.profileService.postProfile(data)
                .subscribe(function (res) { return _this.postResponse = res; }, function (err) { return _this.appService.logError(_this, err); }, function () { return _this.profileSaved(); });
        }
    };
    Profile.prototype.validateForm = function () {
        var _this = this;
        var errorCount = 0;
        var errorFound = false;
        var _loop_1 = function (q) {
            if (this_1.questions.hasOwnProperty(q)) {
                if (this_1.result.ShowRequiredKey === true && (this_1.questions[q].Answer === "") || !this_1.questions[q].Answer) {
                    this_1.questions[q].InvalidRequired = true;
                    errorFound = true;
                    errorCount += 1;
                }
                else {
                    this_1.questions[q].InvalidRequired = false;
                }
                if (this_1.isTextBox(this_1.questions[q]) && errorCount === 0) {
                    var qs = this_1.questions[q];
                    var a = qs.Answer;
                    if (qs.Type === "text") {
                        if (a.length < parseInt(qs.Min) || a.length > parseInt(qs.Max)) {
                            this_1.questions[q].InvalidLength = true;
                            errorFound = true;
                            errorCount += 1;
                        }
                        else {
                            this_1.questions[q].InvalidLength = false;
                        }
                    }
                    else if (errorCount === 0) {
                        if (a === "" || (this_1.appService.isNumeric(a) === false ||
                            (parseInt(a) < parseInt(qs.Min) || parseInt(a) > parseInt(qs.Max)))) {
                            this_1.questions[q].InvalidRange = true;
                            errorFound = true;
                            errorCount += 1;
                        }
                        else {
                            this_1.questions[q].InvalidRange = false;
                        }
                    }
                }
            }
            if (errorCount == 1 && errorFound == true) {
                if (!this_1.fromUTest) {
                    setTimeout(function () { return _this.focusElement(q); }, 200);
                    errorFound = false;
                }
            }
        };
        var this_1 = this;
        for (var q in this.questions) {
            _loop_1(q);
        }
        if (errorCount === 0) {
            this.formValid = true;
        }
        else {
            this.formValid = false;
        }
    };
    Profile.prototype.profileSaved = function () {
        var _this = this;
        this.saved = true;
        this.postEvents();
        if (!this.fromUTest) {
            this.profileService.getProfile(Profile_1.tabKey, Profile_1.parameters)
                .subscribe(function (res) { return _this.result = res; }, function (error) { return _this.appService.logError(_this, error); }, function () { return _this.disableLoader(true); });
        }
    };
    Profile.prototype.postEvents = function () {
        var _this = this;
        var ei = [];
        ei.push({ key: "TabId", value: Profile_1.tabKey });
        if (!this.fromUTest) {
            this.appService.postEvents(ei, Profile_1.parameters, "Update Profile")
                .subscribe(function (res) { return _this.postEvent = res; }, function (err) { return _this.appService.logError(_this, err); });
        }
    };
    Profile.fromUnitTest = "unittest";
    Profile = Profile_1 = __decorate([
        core_1.Component({
            selector: 'profile',
            template: "<Loader *ngIf=\"enableLoader\"></Loader><div *ngIf=\"!enableLoader\"><div *ngIf=\"errorOccurred\"><div class=\"alert alert-danger\"><span class=\"glyphicon glyphicon-exclamation-sign xs-mr-10\" aria-hidden=\"true\"></span> <strong>{{errorMessage}}</strong></div></div><div *ngIf=\"!errorOccurred\" class=\"panel panel-default shortProfile\"><div *ngIf=\"result.ShowTitle == 'true'\" class=\"panel-heading\"><h1 role=\"presentation\" class=\"panel-title\">{{result.Title}}</h1></div><div *ngIf=\"result.ShowIntro == 'true'\" class=\"panel-body xs-p-0\">{{result.IntroText}}</div><div id=\"iws_pr_top\" class=\"panel-body progressBar xs-mb-0 xs-pt-15 xs-pb-0\"><div class=\"progressBar__container\"><div class=\"progressBar__message col-xs-12 col-ms-4 xs-pl-0 xs-pr-0 ms-pr-0\"><h2 *ngIf=\"result.Completeness==100\" class=\"progressBar__title\" aria-label=\"Options for Your Home\">{{result.CompletenessTitle}}</h2><h2 *ngIf=\"result.Completeness!==100\" class=\"progressBar__title\" aria-label=\"Options for Your Home\">{{result.Tabs[0].Sections[0].SectionTitle}}</h2></div></div><div class=\"progressBar__bar-container col-xs-12 col-ms-8\"><div class=\"col-xs-12 col-ms-8 col-lg-6 pull-right xs-pr-0 xs-pl-0\"><div class=\"progress-shell ms-pl-15\"><div class=\"progress xs-mt-10\"><div *ngIf=\"result.Completeness==100\" class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"100%\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 100%\"></div><div *ngIf=\"result.Completeness!==100\" class=\"progress-bar\" role=\"progressbar\" [attr.aria-valuenow]=\"completeness\" aria-valuemin=\"0\" aria-valuemax=\"100\" [style.width]=\"completeness\"></div></div></div></div><div class=\"col-xs-12 col-ms-4 col-lg-6 pull-left xs-pr-0 xs-pl-0 sm-pr-15 text-right text-xs-center\"><div class=\"progressBar__completed sm-pr-15\">{{result.CompletenessLabel}}</div></div></div></div><div class=\"panel-body panel-body-energy-profile\"><hr class=\"dotted xs-m-0 xs-mb-15\"><div *ngIf=\"result.Completeness==100\"><div class=\"markdown\">{{result.CompletenessSubtitle}}</div><div class=\"markdown dv-external-links\">{{result.CompletenessDescription}}</div></div><div *ngIf=\"result.Completeness!==100\"><article class=\"article-profile col-ms-6 col-sm-6 col-lg-6 ms-p-0\"><div id=\"iws_pr_section_questions\" class=\"xs-pt-20\"><div *ngFor=\"let q of questions | objToArr\"><div class=\"panel-body xs-pl-0\" *ngIf=\"isListBox(q)\"><div class=\"dropdown\"><h5 *ngIf=\"result.ShowRequiredKey=='true'\" class=\"form-item-label\">{{q.Label}} {{result.RequiredSymbol}}</h5><h5 *ngIf=\"result.ShowRequiredKey!=='true'\" class=\"form-item-label\">{{q.Label}}</h5><a *ngIf=\"!isIos\" href=\"#\" onclick=\"return false;\" role=\"button\" [id]=\"'validation-' + q.Key\" aria-level=\"1\" class=\"btn btn-default dropdown-toggle\" [ngClass]=\"{'btn-error':q.InvalidRequired}\" type=\"button\" data-toggle=\"dropdown\" aria-expanded=\"false\"><p class=\"select\">{{dropDownSelectText(q.Answer, q)}}</p><span><img src=\"Content/Images/caret-darker.png\" height=\"6\" width=\"10\" alt=\"triangle image for button\"></span></a><ul *ngIf=\"!isIos\" class=\"dropdown-menu\" role=\"menu\"><li *ngFor=\"let v of q.Options.Value | objToArr; let i = index\" class=\"save-list\"><a href=\"#\" onclick=\"return false;\" (click)=\"updateAnswer(q.Key, v)\">{{ getOptionText(i, q) }}</a></li></ul><select *ngIf=\"isIos\" [id]=\"'validation-' + q.Key\" (change)=\"updateAnswer(q.Key, $event.target.value)\" class=\"iosHeight\"><option value=\"\">{{result.DropDownSelectText}}</option><option *ngFor=\"let v of q.Options.Value | objToArr; let i = index\" [attr.selected]=\"optionSelected(q, v) ? '' : null\" value=\"{{v}}\">{{ getOptionText(i, q) }}</option></select><p *ngIf=\"q.InvalidRequired\" class=\"error\">{{result.NotCompleteMessage}}</p></div></div><div *ngIf=\"isTextBox(q)\"><div class=\"panel-body xs-pl-0\"><h5 *ngIf=\"result.ShowRequiredKey=='true'\" class=\"form-item-label\">{{q.Label}} {{result.RequiredSymbol}}</h5><h5 *ngIf=\"result.ShowRequiredKey!=='true'\" class=\"form-item-label\">{{q.Label}}</h5><input [id]=\"'validation-' + q.Key\" [(ngModel)]=\"q.Answer\" class=\"form-control energy-field\" [ngClass]=\"{'btn-error': q.InvalidRequired||q.InvalidRange||q.InvalidLength}\"><p *ngIf=\"q.InvalidRequired\" class=\"error\">{{result.NotCompleteMessage}}</p><p *ngIf=\"q.InvalidRange\" class=\"error\">{{q.RangeErrorMessage}}</p><p *ngIf=\"q.InvalidLength\" class=\"error\">{{q.InvalidLengthMessage}}</p></div></div><div class=\"panel-body xs-pl-0\" *ngIf=\"isRadioButton(q)\"><div><h5 *ngIf=\"result.ShowRequiredKey=='true'\" class=\"form-item-label\">{{q.Label}} {{result.RequiredSymbol}}</h5><h5 *ngIf=\"result.ShowRequiredKey!=='true'\" class=\"form-item-label\">{{q.Label}}</h5><div class=\"btn-group\" [ngClass]=\"{'btn-error':q.InvalidRequired}\" data-toggle=\"buttons\"><label *ngFor=\"let v of q.Options.Value | objToArr; let i = index\" class=\"btn btn-default right\" [ngClass]=\"{active: isRadioButtonActive(q, v)}\" (click)=\"updateAnswer(q.Key, v)\" style=\"border: 1px solid let d4d4d4\"><input [id]=\"'validation-' + q.Key\" type=\"radio\"> {{ getOptionText(i, q) }}</label></div><p *ngIf=\"q.InvalidRequired\" class=\"error\">{{result.NotCompleteMessage}}</p></div></div></div></div><div class=\"col-xs-12 xs-pl-0\"><div class=\"required dv-external-links\" style=\"clear: left\"><div *ngIf=\"result.ShowRequiredKey=='true'\">{{result.RequiredSymbol}}{{result.RequiredKey}}</div><a *ngIf=\"result.ShowLongProfileLink=='true'\" [attr.href]=\"result.LongProfileLink\" class=\"show-profile block\">{{result.LongProfileLinkText}}</a></div><button type=\"button\" class=\"btn action-btn xs-mt-5\" (click)=\"postData()\" onclick=\"return false;\" title=\"Next\">{{result.NextButtonLabel}}</button></div></article><aside class=\"aside-profile col-ms-6 col-sm-6 col-lg-6 xs-pr-0 xs-pl-0\"><div class=\"thumbnail\"><img class=\"img-responsive hidden-xs\" [attr.src]=\"result.Tabs[0].Sections[0].ImageUrl\" alt=\"\" style=\"overflow: hidden; width: 100%\"><div class=\"caption\"><h5 class=\"thumb-item-label\">{{result.Tabs[0].Sections[0].SectionSubtitle}}</h5><span>{{result.Tabs[0].Sections[0].SectionDescription}}</span></div></div></aside></div><div *ngIf=\"result.ShowFooterText === 'true' || result.ShowFooterLink === 'true'\" class=\"panel-footer\"><div *ngIf=\"result.ShowFooterText === 'true'\" class=\"footertext\">{{result.Footer}}</div><div *ngIf=\"result.ShowFooterLink === 'true'\" class=\"footerlink dv-external-links\"><a href=\"{{result.FooterLink}}\">{{result.FooterLinkText}}</a></div></div></div></div></div>",
            providers: [profile_service_1.ProfileService, app_service_1.AppService]
        }),
        __metadata("design:paramtypes", [profile_service_1.ProfileService, app_service_1.AppService, core_1.ElementRef])
    ], Profile);
    return Profile;
    var Profile_1;
}());
exports.Profile = Profile;

//# sourceMappingURL=profile.js.map
