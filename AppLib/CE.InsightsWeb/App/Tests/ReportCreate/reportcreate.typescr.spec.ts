﻿import { ElementRef } from "@angular/core";
import { Http } from "@angular/http";
import { ReportCreate } from "../../ReportCreate/reportcreate";
import { ReportCreateService } from "../../ReportCreate/reportcreate.service";
import { AppService } from "../../app.service";

class MockElementRef implements ElementRef {
    nativeElement = {};
}

describe('ReportCreate Typescript Tests',
() => {
    it('ReportCreate check if CSR and load content().  Component main entry point.',
    () => {
        let http: Http;
        const appService: AppService = new AppService(http);
        const qlService: ReportCreateService= new ReportCreateService(http);
        let elementRef = new MockElementRef();
       
        appService.params = [
            {
                tabKey: "tab.billstatement",
                parameters:
                    "YbUGYEN1OhOANtb1zjkfy/hq/cI+rQ4HQlLIgZfqZL7OdgsF8ZKqzAw3eqO0PtMqSEW2CiNa9iOob71Xc8rETBZ0b0gp5N9mczstPSIn8k41jMUEo/KUHHOvQMT89omslD8Zxrj3CSA5CIYCEkMqiRFvtTquCvGi0GQTcZ19rqt1WAisugqWAXqjCKDEgwEAtTxKtAADOS1GKFAOc8O/vAn6PK0IcfRMmQ580IKEVhAnIE2UI2E8B3TjBR426Obt+NM4ppU46hcpbwY0IUil1ojq/lN/d8AzTriNnHjy3eVb2OyYDuFYOFopUUpX/v1qUrVgV7VD96UW2zEGS5zRb4N0/RHYCSwUE3qgwkzyOjUaOX3L9Jrwk9e4tNouzbEbjbXPjTwDcqbqUBk/Wn5eik5/64JISCsuqUNSz5GbjMN4F4WYoFT37PtjCq7KsV16YccmF3NObF9MfU45pgPyRI8Qas6g2fG/XdChanb7sRPGDj0C0do7/86T3PyOYJoV4v3+afpi7+iSGcfnYR7bhg==",
                errorMessage: ""
            }
        ];
        qlService.res = JSON
            .parse('{ "Footer": "", "FooterLink": "", "FooterLinkText": "", "IntroText": "", "LinkList": [{ "ActionPriority": 100, "Description": "","Key":"quicklink.paybill","Url":"www.aclara.com" }], "Title": "Quick Links"  }');

        const repcreate = new ReportCreate(qlService, appService, elementRef);
	     repcreate.fromUnitTs = "unittest";
        expect(repcreate.enableLoader).toBe(true);
        });

    it('ReportCreate pdfKendoUIExport(). Exports pdf by using app service pdfExport method.',
        () => {
            let http: Http;
            const appService: AppService = new AppService(http);
            const qlService: ReportCreateService = new ReportCreateService(http);
            let elementRef = new MockElementRef();
            appService.params = [
                {
                    tabKey: "tab.billstatement",
                    parameters:
                    "YbUGYEN1OhOANtb1zjkfy/hq/cI+rQ4HQlLIgZfqZL7OdgsF8ZKqzAw3eqO0PtMqSEW2CiNa9iOob71Xc8rETBZ0b0gp5N9mczstPSIn8k41jMUEo/KUHHOvQMT89omslD8Zxrj3CSA5CIYCEkMqiRFvtTquCvGi0GQTcZ19rqt1WAisugqWAXqjCKDEgwEAtTxKtAADOS1GKFAOc8O/vAn6PK0IcfRMmQ580IKEVhAnIE2UI2E8B3TjBR426Obt+NM4ppU46hcpbwY0IUil1ojq/lN/d8AzTriNnHjy3eVb2OyYDuFYOFopUUpX/v1qUrVgV7VD96UW2zEGS5zRb4N0/RHYCSwUE3qgwkzyOjUaOX3L9Jrwk9e4tNouzbEbjbXPjTwDcqbqUBk/Wn5eik5/64JISCsuqUNSz5GbjMN4F4WYoFT37PtjCq7KsV16YccmF3NObF9MfU45pgPyRI8Qas6g2fG/XdChanb7sRPGDj0C0do7/86T3PyOYJoV4v3+afpi7+iSGcfnYR7bhg==",
                    errorMessage: ""
                }
            ];
            qlService.res = JSON
                .parse('{ "Footer": "", "FooterLink": "", "FooterLinkText": "", "IntroText": "", "LinkList": [{ "ActionPriority": 100, "Description": "","Key":"quicklink.paybill","Url":"www.aclara.com" }], "Title": "Quick Links"  }');

            const repcreate = new ReportCreate(qlService, appService, elementRef);
            repcreate.fromUnitTs = "unittest";
            repcreate.result.DefaultReportTitle = "Title";
            repcreate.result.DefaultReportNotes = "Notes";
            repcreate.result.ExcludeWidgets = "";
            repcreate.tabKey = "CSR";

            //CSR export
            repcreate.pdfKendoUIExport("true");

            //User export
            repcreate.pdfKendoUIExport("false");

            //Empty title
            repcreate.result.DefaultReportTitle = "";
            repcreate.pdfKendoUIExport("");

            expect(repcreate.pdfexport).toBe(false);
        });
});