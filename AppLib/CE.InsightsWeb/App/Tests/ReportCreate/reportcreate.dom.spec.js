"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var platform_browser_1 = require("@angular/platform-browser");
var http_1 = require("@angular/http");
var testing_2 = require("@angular/platform-browser-dynamic/testing");
var reportcreate_1 = require("../../ReportCreate/reportcreate");
var reportcreate_service_1 = require("../../ReportCreate/reportcreate.service");
var app_service_1 = require("../../app.service");
var loader_1 = require("../../Loader/loader");
var MockElementRef = (function () {
    function MockElementRef() {
        this.nativeElement = {};
    }
    return MockElementRef;
}());
var MockAppService = (function (_super) {
    __extends(MockAppService, _super);
    function MockAppService() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.params = [{
                tabKey: "tab.csr", parameters: "WWJVR1lFTjFPaE9BTnRiMXpqa2Z5L2hxL2NJK3JRNEhRbExJZ1pmcVpMN09kZ3NGOFpLcXpBdzNlcU8wUHRNcVNFVzJDaU5hOWlPb2I3MVhjOHJFVEJaMGIwZ3A1TjltY3pzdFBTSW44azQxak1VRW8vS1VISE92UU1UODlvbXNsRDhaeHJqM0NTQTVDSVlDRWtNcWlSRnZ0VHF1Q3ZHaTBHUVRjWjE5cnF0MVdBaXN1Z3FXQVhxakNLREVnd0VBdFR4S3RBQURPUzFHS0ZBT2M4Ty92QW42UEswSWNmUk1tUTU4MElLRVZoQW5JRTJVSTJFOEIzVGpCUjQyNk9idCtOTTRwcFU0NmhjcGJ3WTBJVWlsMW1nSTBzWE01alFGNER6VVZtOURIVDJMQmNBSjByZ1pUN05mVG1ZUVdGcnBhRjE0MHY5cGl2SHFkYUZaNDF2NmkvRDVEc1hYaFA2QmpuYytDakJVWEw5TytZWkdEZ3dwbGJVS3NQTHlyNERiZS9yNEdyQUs3bS83RjJTWHZ4MU0wTnpLTnh0VXg5SGd4NXRpa0NmOURoWm9kVUFhK2x3VkUrMzRudUw2aUx4cWJyd2JNOUtDa2NhMTJENy96Z0JzZVY5aEdteXJ5T04yclkxZWI5ekdjSEVRNDBsK0Y3WXlycGZPRjFJMmdkbWJudFBEa25vd2pLVGw4OUlCRGw0TFlmQWtjT1BlZ2Q2N2lhSVF6ZnIxSjRDMzhtOGRWdTVvanQxMzZCRzFmUXBQ", errorMessage: "We’re sorry… There was a problem loading your information."
            }];
        return _this;
    }
    MockAppService.prototype.getParams = function () {
        return this.params;
    };
    MockAppService.prototype.pdfKendoUIExport = function () {
        return true;
    };
    return MockAppService;
}(app_service_1.AppService));
var MockReportCreateService = (function (_super) {
    __extends(MockReportCreateService, _super);
    function MockReportCreateService() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.unit = "unittest";
        return _this;
    }
    MockReportCreateService.prototype.getReportCreate = function (tabkey, params) {
        return this.unit;
    };
    MockReportCreateService.prototype.postReportCreate = function () {
        return "OK";
    };
    return MockReportCreateService;
}(reportcreate_service_1.ReportCreateService));
describe('Report Create PDF Tests', function () {
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.resetTestEnvironment();
        testing_1.TestBed.initTestEnvironment(testing_2.BrowserDynamicTestingModule, testing_2.platformBrowserDynamicTesting());
        core_1.ElementRef;
        testing_1.TestBed.configureTestingModule({
            imports: [platform_browser_1.BrowserModule, forms_1.FormsModule, http_1.HttpModule],
            declarations: [reportcreate_1.ReportCreate, loader_1.Loader]
        })
            .overrideComponent(reportcreate_1.ReportCreate, {
            remove: { providers: [app_service_1.AppService, reportcreate_service_1.ReportCreateService] },
            add: {
                providers: [
                    { provide: app_service_1.AppService, useClass: MockAppService },
                    { provide: reportcreate_service_1.ReportCreateService, useClass: MockReportCreateService }
                ]
            }
        })
            .compileComponents();
    }));
    it('should find input notes on create pdf report', testing_1.async(function () {
        var fixture = testing_1.TestBed.createComponent(reportcreate_1.ReportCreate);
        var app = fixture.componentInstance;
        app.enableLoader = false;
        var json = JSON.parse('{ "NoTitle":"Please enter a title for the report.", "CreateReportButton":"Create Report", "ReportNotes":"Notes", "ReportTitle":"Title", "DefaultReportNotes":"This report helps explain your energy usage and costs.", "DefaultReportTitle":"Your Energy Report", "ExcludeWidgets":"billhistory.greenbutton;myusage.greenbutton;dashboardbusiness.quicklinks;billhistory.quicklinks;billstatement.quicklinks;csr.reportcreate", "PostError":null, "Title":"PDF Report", "IntroText":"You can create a PDF version of this page. Enter a report title and optional notes and click ?Create Report.?", "SubTitle":"Create a PDF Report", "ShowTitle":"false", "ShowIntro":"false", "ShowSubTitle":"false", "Footer":"Sample footer", "FooterLink":"https:///www.aclara.com", "FooterLinkText":"Next steps", "FooterLinkType":null, "ShowFooterText":"false", "ShowFooterLink":"false" }');
        app.result = json;
        fixture.detectChanges();
        var pdfrptType = fixture.nativeElement.querySelectorAll("#inputNotes");
        expect(pdfrptType.length).toBe(1);
    }));
    it('should find input title on create pdf report', testing_1.async(function () {
        var fixture = testing_1.TestBed.createComponent(reportcreate_1.ReportCreate);
        var app = fixture.componentInstance;
        app.enableLoader = false;
        var json = JSON.parse('{ "NoTitle":"Please enter a title for the report.", "CreateReportButton":"Create Report", "ReportNotes":"Notes", "ReportTitle":"Title", "DefaultReportNotes":"This report helps explain your energy usage and costs.", "DefaultReportTitle":"Your Energy Report", "ExcludeWidgets":"billhistory.greenbutton;myusage.greenbutton;dashboardbusiness.quicklinks;billhistory.quicklinks;billstatement.quicklinks;csr.reportcreate", "PostError":null, "Title":"PDF Report", "IntroText":"You can create a PDF version of this page. Enter a report title and optional notes and click ?Create Report.?", "SubTitle":"Create a PDF Report", "ShowTitle":"false", "ShowIntro":"false", "ShowSubTitle":"false", "Footer":"Sample footer", "FooterLink":"https:///www.aclara.com", "FooterLinkText":"Next steps", "FooterLinkType":null, "ShowFooterText":"false", "ShowFooterLink":"false" }');
        app.result = json;
        fixture.detectChanges();
        var pdfrptType = fixture.nativeElement.querySelectorAll("#inputTitle");
        expect(pdfrptType.length).toBe(1);
    }));
    it('should find button with create report inner text', testing_1.async(function () {
        var fixture = testing_1.TestBed.createComponent(reportcreate_1.ReportCreate);
        var app = fixture.componentInstance;
        app.enableLoader = false;
        var json = JSON.parse('{ "NoTitle":"Please enter a title for the report.", "CreateReportButton":"Create Report", "ReportNotes":"Notes", "ReportTitle":"Title", "DefaultReportNotes":"This report helps explain your energy usage and costs.", "DefaultReportTitle":"Your Energy Report", "ExcludeWidgets":"billhistory.greenbutton;myusage.greenbutton;dashboardbusiness.quicklinks;billhistory.quicklinks;billstatement.quicklinks;csr.reportcreate", "PostError":null, "Title":"PDF Report", "IntroText":"You can create a PDF version of this page. Enter a report title and optional notes and click ?Create Report.?", "SubTitle":"Create a PDF Report", "ShowTitle":"false", "ShowIntro":"false", "ShowSubTitle":"false", "Footer":"Sample footer", "FooterLink":"https:///www.aclara.com", "FooterLinkText":"Next steps", "FooterLinkType":null, "ShowFooterText":"false", "ShowFooterLink":"false" }');
        app.result = json;
        fixture.detectChanges();
        var txtCreateRept = fixture.nativeElement.querySelectorAll("button");
        expect(txtCreateRept[0].innerText).toBe("Create Report");
    }));
});
//# sourceMappingURL=reportcreate.dom.spec.js.map