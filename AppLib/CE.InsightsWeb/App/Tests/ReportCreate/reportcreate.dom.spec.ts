﻿import { async, TestBed } from "@angular/core/testing";
import { ElementRef } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { HttpModule } from "@angular/http";
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from "@angular/platform-browser-dynamic/testing";
import { ReportCreate } from "../../ReportCreate/reportcreate";
import { ReportCreateService } from "../../ReportCreate/reportcreate.service";
import { AppService, IAppParams } from "../../app.service";
import { Loader } from "../../Loader/loader";

class MockElementRef implements ElementRef {
    nativeElement = {};
}

//mock premise and app services
class MockAppService extends AppService {
    params: Array<IAppParams> = [{
        tabKey: "tab.csr", parameters: "WWJVR1lFTjFPaE9BTnRiMXpqa2Z5L2hxL2NJK3JRNEhRbExJZ1pmcVpMN09kZ3NGOFpLcXpBdzNlcU8wUHRNcVNFVzJDaU5hOWlPb2I3MVhjOHJFVEJaMGIwZ3A1TjltY3pzdFBTSW44azQxak1VRW8vS1VISE92UU1UODlvbXNsRDhaeHJqM0NTQTVDSVlDRWtNcWlSRnZ0VHF1Q3ZHaTBHUVRjWjE5cnF0MVdBaXN1Z3FXQVhxakNLREVnd0VBdFR4S3RBQURPUzFHS0ZBT2M4Ty92QW42UEswSWNmUk1tUTU4MElLRVZoQW5JRTJVSTJFOEIzVGpCUjQyNk9idCtOTTRwcFU0NmhjcGJ3WTBJVWlsMW1nSTBzWE01alFGNER6VVZtOURIVDJMQmNBSjByZ1pUN05mVG1ZUVdGcnBhRjE0MHY5cGl2SHFkYUZaNDF2NmkvRDVEc1hYaFA2QmpuYytDakJVWEw5TytZWkdEZ3dwbGJVS3NQTHlyNERiZS9yNEdyQUs3bS83RjJTWHZ4MU0wTnpLTnh0VXg5SGd4NXRpa0NmOURoWm9kVUFhK2x3VkUrMzRudUw2aUx4cWJyd2JNOUtDa2NhMTJENy96Z0JzZVY5aEdteXJ5T04yclkxZWI5ekdjSEVRNDBsK0Y3WXlycGZPRjFJMmdkbWJudFBEa25vd2pLVGw4OUlCRGw0TFlmQWtjT1BlZ2Q2N2lhSVF6ZnIxSjRDMzhtOGRWdTVvanQxMzZCRzFmUXBQ", errorMessage: "We’re sorry… There was a problem loading your information." }];
    getParams(): Array<IAppParams> {
        return this.params;
    }

     pdfKendoUIExport(): Boolean  {
        return true;
     }

}

class MockReportCreateService extends ReportCreateService {
    public res: any;
    public unit: any = "unittest";

    getReportCreate(tabkey, params) {
        return this.unit;
    }

    postReportCreate() {
        return "OK";
    }
}

describe('Report Create PDF Tests',
    () => {
        beforeEach(async(() => {
            TestBed.resetTestEnvironment();
            TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting());
            ElementRef;
            TestBed.configureTestingModule({
                imports: [BrowserModule, FormsModule, HttpModule],
                declarations: [ReportCreate, Loader]
            })
                .overrideComponent(ReportCreate,
                {
                    remove: { providers: [AppService, ReportCreateService] },
                    add: {
                        providers: [
                            { provide: AppService, useClass: MockAppService },
                            { provide: ReportCreateService, useClass: MockReportCreateService }
                        ]
                    }
                })
                .compileComponents();
        }));


        it('should find input notes on create pdf report', async(() => {
            const fixture = TestBed.createComponent(ReportCreate);
            const app = fixture.componentInstance;
            app.enableLoader = false;
            var json = JSON.parse('{ "NoTitle":"Please enter a title for the report.", "CreateReportButton":"Create Report", "ReportNotes":"Notes", "ReportTitle":"Title", "DefaultReportNotes":"This report helps explain your energy usage and costs.", "DefaultReportTitle":"Your Energy Report", "ExcludeWidgets":"billhistory.greenbutton;myusage.greenbutton;dashboardbusiness.quicklinks;billhistory.quicklinks;billstatement.quicklinks;csr.reportcreate", "PostError":null, "Title":"PDF Report", "IntroText":"You can create a PDF version of this page. Enter a report title and optional notes and click ?Create Report.?", "SubTitle":"Create a PDF Report", "ShowTitle":"false", "ShowIntro":"false", "ShowSubTitle":"false", "Footer":"Sample footer", "FooterLink":"https:///www.aclara.com", "FooterLinkText":"Next steps", "FooterLinkType":null, "ShowFooterText":"false", "ShowFooterLink":"false" }');
            app.result = json;
            fixture.detectChanges();
            const pdfrptType = fixture.nativeElement.querySelectorAll("#inputNotes");
            expect(pdfrptType.length).toBe(1);
        }));


        it('should find input title on create pdf report', async(() => {
            const fixture = TestBed.createComponent(ReportCreate);
            const app = fixture.componentInstance;
            app.enableLoader = false;
            var json = JSON.parse('{ "NoTitle":"Please enter a title for the report.", "CreateReportButton":"Create Report", "ReportNotes":"Notes", "ReportTitle":"Title", "DefaultReportNotes":"This report helps explain your energy usage and costs.", "DefaultReportTitle":"Your Energy Report", "ExcludeWidgets":"billhistory.greenbutton;myusage.greenbutton;dashboardbusiness.quicklinks;billhistory.quicklinks;billstatement.quicklinks;csr.reportcreate", "PostError":null, "Title":"PDF Report", "IntroText":"You can create a PDF version of this page. Enter a report title and optional notes and click ?Create Report.?", "SubTitle":"Create a PDF Report", "ShowTitle":"false", "ShowIntro":"false", "ShowSubTitle":"false", "Footer":"Sample footer", "FooterLink":"https:///www.aclara.com", "FooterLinkText":"Next steps", "FooterLinkType":null, "ShowFooterText":"false", "ShowFooterLink":"false" }');
            app.result = json;
            fixture.detectChanges();
            const pdfrptType = fixture.nativeElement.querySelectorAll("#inputTitle");
            expect(pdfrptType.length).toBe(1);
        }));

        it('should find button with create report inner text', async(() => {
            const fixture = TestBed.createComponent(ReportCreate);
            const app = fixture.componentInstance;
            app.enableLoader = false;
            var json = JSON.parse('{ "NoTitle":"Please enter a title for the report.", "CreateReportButton":"Create Report", "ReportNotes":"Notes", "ReportTitle":"Title", "DefaultReportNotes":"This report helps explain your energy usage and costs.", "DefaultReportTitle":"Your Energy Report", "ExcludeWidgets":"billhistory.greenbutton;myusage.greenbutton;dashboardbusiness.quicklinks;billhistory.quicklinks;billstatement.quicklinks;csr.reportcreate", "PostError":null, "Title":"PDF Report", "IntroText":"You can create a PDF version of this page. Enter a report title and optional notes and click ?Create Report.?", "SubTitle":"Create a PDF Report", "ShowTitle":"false", "ShowIntro":"false", "ShowSubTitle":"false", "Footer":"Sample footer", "FooterLink":"https:///www.aclara.com", "FooterLinkText":"Next steps", "FooterLinkType":null, "ShowFooterText":"false", "ShowFooterLink":"false" }');
            app.result = json;
            fixture.detectChanges();
            const txtCreateRept = fixture.nativeElement.querySelectorAll("button");
            expect(txtCreateRept[0].innerText).toBe("Create Report");
        }));

    });