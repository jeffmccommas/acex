"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var platform_browser_1 = require("@angular/platform-browser");
var http_1 = require("@angular/http");
var testing_2 = require("@angular/platform-browser-dynamic/testing");
var promo_1 = require("../../Promo/promo");
var promo_service_1 = require("../../Promo/promo.service");
var app_service_1 = require("../../app.service");
var loader_1 = require("../../Loader/loader");
var pipe_1 = require("../../Helper/pipe");
var MockAppService = (function (_super) {
    __extends(MockAppService, _super);
    function MockAppService() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.params = [{ tabKey: "tab.dashboard", parameters: "YbUGYEN1OhOANtb1zjkfy/hq/cI+rQ4HQlLIgZfqZL7OdgsF8ZKqzAw3eqO0PtMqSEW2CiNa9iOob71Xc8rETBZ0b0gp5N9mczstPSIn8k41jMUEo/KUHHOvQMT89omslD8Zxrj3CSA5CIYCEkMqiRFvtTquCvGi0GQTcZ19rqt1WAisugqWAXqjCKDEgwEASuOTT5lYb5Zeaiq/uvBlY9iRFrK+c98iNuYmYNt7bo3aP/l17bzmWnKtytoTOOZiloaVfxEYRnPxVp4qvkav13vVbOe3Q6XjYo84y6fLYm6sZnkvIOzkNDxoUA9iLv1dow6BfTi2jRrGsU+uFNlW+1pVnvFIDY4nFruTPKkJ4KkO4djJI4VQDRubA9U4exxllTJJJz4BQXJ98xTr+/we3w==", errorMessage: "" }];
        _this.deepLinkVariables = [{ deepLinkWidget: "", deepLinkWidgetTab: "", deepLinkWidgetSection: "" }];
        return _this;
    }
    MockAppService.prototype.getParams = function () {
        return this.params;
    };
    MockAppService.prototype.getDeepLinkVariables = function () {
        return this.deepLinkVariables;
    };
    return MockAppService;
}(app_service_1.AppService));
var MockPromoService = (function (_super) {
    __extends(MockPromoService, _super);
    function MockPromoService() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.unit = "unittest";
        return _this;
    }
    MockPromoService.prototype.getPromos = function (tabkey, params) {
        return this.unit;
    };
    return MockPromoService;
}(promo_service_1.PromoService));
describe('Promo DOM Tests', function () {
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.resetTestEnvironment();
        testing_1.TestBed.initTestEnvironment(testing_2.BrowserDynamicTestingModule, testing_2.platformBrowserDynamicTesting());
        core_1.ElementRef;
        testing_1.TestBed.configureTestingModule({
            imports: [platform_browser_1.BrowserModule, forms_1.FormsModule, http_1.HttpModule],
            declarations: [promo_1.Promo, loader_1.Loader, pipe_1.ObjToArr]
        })
            .overrideComponent(promo_1.Promo, {
            remove: { providers: [app_service_1.AppService, promo_service_1.PromoService] },
            add: {
                providers: [
                    { provide: app_service_1.AppService, useClass: MockAppService },
                    { provide: promo_service_1.PromoService, useClass: MockPromoService }
                ]
            }
        }).compileComponents();
    }));
    xit('404 not found on npm test but ok on local run:Get Promo() should get a promo', testing_1.async(function () {
        var fixture = testing_1.TestBed.createComponent(promo_1.Promo);
        var app = fixture.componentInstance;
        var element = fixture.nativeElement;
        app.enableLoader = false;
        var json = JSON.parse('{ "Footer": "", "FooterLink": "", "FooterLinkText": "", "IntroText": "", "LinkList": [{ "ActionPriority": 100, "Description": "","Key":"promo.demoalertspromo","Url":"www.aclara.com" }], "Title": "Promos"  }');
        app.result = json;
        fixture.detectChanges();
        var sec = fixture.nativeElement.querySelectorAll("#iws_po_wrapper");
        expect(sec.length).toBe(1);
    }));
});
//# sourceMappingURL=promo.dom.spec.js.map