"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var promo_1 = require("../../Promo/promo");
var promo_service_1 = require("../../Promo/promo.service");
var app_service_1 = require("../../app.service");
var MockElementRef = (function () {
    function MockElementRef() {
        this.nativeElement = {};
    }
    return MockElementRef;
}());
describe('Promo Typescript Tests', function () {
    it('Promo loadpromos().  Component main entry point.', function () {
        var http;
        var appService = new app_service_1.AppService(http);
        var qlService = new promo_service_1.PromoService(http);
        var elementRef = new MockElementRef();
        appService.params = [{ tabKey: "tab.billstatement", parameters: "YbUGYEN1OhOANtb1zjkfy/hq/cI+rQ4HQlLIgZfqZL7OdgsF8ZKqzAw3eqO0PtMqSEW2CiNa9iOob71Xc8rETBZ0b0gp5N9mczstPSIn8k41jMUEo/KUHHOvQMT89omslD8Zxrj3CSA5CIYCEkMqiRFvtTquCvGi0GQTcZ19rqt1WAisugqWAXqjCKDEgwEAtTxKtAADOS1GKFAOc8O/vAn6PK0IcfRMmQ580IKEVhAnIE2UI2E8B3TjBR426Obt+NM4ppU46hcpbwY0IUil1ojq/lN/d8AzTriNnHjy3eVb2OyYDuFYOFopUUpX/v1qUrVgV7VD96UW2zEGS5zRb4N0/RHYCSwUE3qgwkzyOjUaOX3L9Jrwk9e4tNouzbEbjbXPjTwDcqbqUBk/Wn5eik5/64JISCsuqUNSz5GbjMN4F4WYoFT37PtjCq7KsV16YccmF3NObF9MfU45pgPyRI8Qas6g2fG/XdChanb7sRPGDj0C0do7/86T3PyOYJoV4v3+afpi7+iSGcfnYR7bhg==", errorMessage: "" }];
        qlService.res = JSON.parse('{ "Footer": "", "FooterLink": "", "FooterLinkText": "", "IntroText": "", "LinkList": [{ "ActionPriority": 100, "Description": "","Key":"quicklink.paybill","Url":"www.aclara.com" }], "Title": "Quick Links"  }');
        var promo = new promo_1.Promo(qlService, appService, elementRef);
        promo.fromUTest = true;
        promo.ngOnInit();
        expect(promo.enableLoader).toBe(false);
    });
    it('Promo loadImgbyWidth().  Load promo image by width.', function () {
        var http;
        var appService = new app_service_1.AppService(http);
        var qlService = new promo_service_1.PromoService(http);
        var elementRef = new MockElementRef();
        appService.params = [{ tabKey: "tab.billstatement", parameters: "YbUGYEN1OhOANtb1zjkfy/hq/cI+rQ4HQlLIgZfqZL7OdgsF8ZKqzAw3eqO0PtMqSEW2CiNa9iOob71Xc8rETBZ0b0gp5N9mczstPSIn8k41jMUEo/KUHHOvQMT89omslD8Zxrj3CSA5CIYCEkMqiRFvtTquCvGi0GQTcZ19rqt1WAisugqWAXqjCKDEgwEAtTxKtAADOS1GKFAOc8O/vAn6PK0IcfRMmQ580IKEVhAnIE2UI2E8B3TjBR426Obt+NM4ppU46hcpbwY0IUil1ojq/lN/d8AzTriNnHjy3eVb2OyYDuFYOFopUUpX/v1qUrVgV7VD96UW2zEGS5zRb4N0/RHYCSwUE3qgwkzyOjUaOX3L9Jrwk9e4tNouzbEbjbXPjTwDcqbqUBk/Wn5eik5/64JISCsuqUNSz5GbjMN4F4WYoFT37PtjCq7KsV16YccmF3NObF9MfU45pgPyRI8Qas6g2fG/XdChanb7sRPGDj0C0do7/86T3PyOYJoV4v3+afpi7+iSGcfnYR7bhg==", errorMessage: "" }];
        qlService.res = JSON.parse('{ "Footer": "", "FooterLink": "", "FooterLinkText": "", "IntroText": "", "LinkList": [{ "ActionPriority": 100, "Description": "","Key":"quicklink.paybill","Url":"www.aclara.com" }], "Title": "Quick Links"  }');
        var promo = new promo_1.Promo(qlService, appService, elementRef);
        promo.elementRef.nativeElement.childNodes = [{ nodeName: "DIV", id: "iws_po_wrapper", offsetWidth: 1, childNodes: [{ nodeName: "DIV", id: "iws_po_wrapper", offsetWidth: 1, childNodes: [{ nodeName: "DIV", id: "iws_po_wrapper", offsetWidth: 1 }] }] }];
        promo.loadImgbyWidth();
        expect(promo.width).toBe(1);
    });
    it('Promo popup().  post event on promo link click.', function () {
        var http;
        var appService = new app_service_1.AppService(http);
        var qlService = new promo_service_1.PromoService(http);
        var elementRef = new MockElementRef();
        appService.params = [{ tabKey: "tab.billstatement", parameters: "YbUGYEN1OhOANtb1zjkfy/hq/cI+rQ4HQlLIgZfqZL7OdgsF8ZKqzAw3eqO0PtMqSEW2CiNa9iOob71Xc8rETBZ0b0gp5N9mczstPSIn8k41jMUEo/KUHHOvQMT89omslD8Zxrj3CSA5CIYCEkMqiRFvtTquCvGi0GQTcZ19rqt1WAisugqWAXqjCKDEgwEAtTxKtAADOS1GKFAOc8O/vAn6PK0IcfRMmQ580IKEVhAnIE2UI2E8B3TjBR426Obt+NM4ppU46hcpbwY0IUil1ojq/lN/d8AzTriNnHjy3eVb2OyYDuFYOFopUUpX/v1qUrVgV7VD96UW2zEGS5zRb4N0/RHYCSwUE3qgwkzyOjUaOX3L9Jrwk9e4tNouzbEbjbXPjTwDcqbqUBk/Wn5eik5/64JISCsuqUNSz5GbjMN4F4WYoFT37PtjCq7KsV16YccmF3NObF9MfU45pgPyRI8Qas6g2fG/XdChanb7sRPGDj0C0do7/86T3PyOYJoV4v3+afpi7+iSGcfnYR7bhg==", errorMessage: "" }];
        qlService.res = JSON.parse('{ "Footer": "", "FooterLink": "", "FooterLinkText": "", "IntroText": "", "LinkList": [{ "ActionPriority": 100, "Description": "","Key":"quicklink.paybill","Url":"www.aclara.com" }], "Title": "Quick Links"  }');
        var promo = new promo_1.Promo(qlService, appService, elementRef);
        promo.fromUTest = true;
        promo.popUp("", "");
        expect(promo.fromUTest).toBe(true);
    });
    it('Promo postevents().  Post event when links clicked.', function () {
        var http;
        var appService = new app_service_1.AppService(http);
        var qlService = new promo_service_1.PromoService(http);
        var elementRef = new MockElementRef();
        appService.params = [{ tabKey: "tab.billstatement", parameters: "YbUGYEN1OhOANtb1zjkfy/hq/cI+rQ4HQlLIgZfqZL7OdgsF8ZKqzAw3eqO0PtMqSEW2CiNa9iOob71Xc8rETBZ0b0gp5N9mczstPSIn8k41jMUEo/KUHHOvQMT89omslD8Zxrj3CSA5CIYCEkMqiRFvtTquCvGi0GQTcZ19rqt1WAisugqWAXqjCKDEgwEAtTxKtAADOS1GKFAOc8O/vAn6PK0IcfRMmQ580IKEVhAnIE2UI2E8B3TjBR426Obt+NM4ppU46hcpbwY0IUil1ojq/lN/d8AzTriNnHjy3eVb2OyYDuFYOFopUUpX/v1qUrVgV7VD96UW2zEGS5zRb4N0/RHYCSwUE3qgwkzyOjUaOX3L9Jrwk9e4tNouzbEbjbXPjTwDcqbqUBk/Wn5eik5/64JISCsuqUNSz5GbjMN4F4WYoFT37PtjCq7KsV16YccmF3NObF9MfU45pgPyRI8Qas6g2fG/XdChanb7sRPGDj0C0do7/86T3PyOYJoV4v3+afpi7+iSGcfnYR7bhg==", errorMessage: "" }];
        qlService.res = JSON.parse('{ "Footer": "", "FooterLink": "", "FooterLinkText": "", "IntroText": "", "LinkList": [{ "ActionPriority": 100, "Description": "","Key":"quicklink.paybill","Url":"www.aclara.com" }], "Title": "Quick Links"  }');
        var promo = new promo_1.Promo(qlService, appService, elementRef);
        promo.fromUTest = true;
        promo.postEvents("", "", "Promo");
        expect(promo.fromUTest).toBe(true);
    });
});
//# sourceMappingURL=promo.typescr.spec.js.map