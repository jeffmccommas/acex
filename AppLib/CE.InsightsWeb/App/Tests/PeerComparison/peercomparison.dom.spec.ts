﻿import { async, TestBed } from "@angular/core/testing";
import { ElementRef } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { HttpModule } from "@angular/http";
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from "@angular/platform-browser-dynamic/testing";
import { PeerComparison } from "../../PeerComparison/peercomparison";
import { PeerComparisonService } from "../../PeerComparison/peercomparison.service";
import { AppService, IAppParams, IDeepLinkVariables } from "../../app.service";
import { Loader } from "../../Loader/loader";
import { ObjToArr } from "../../Helper/pipe";

//mock peer comparison and app services
class MockAppService extends AppService {
    public params: Array<IAppParams> = [{
        tabKey: "tab.dashboard", parameters: "YbUGYEN1OhOANtb1zjkfy/hq/cI+rQ4HQlLIgZfqZL5V2ctJGNmEOdmE/iQaQ3KRdd4nNk5O9zhAuNIFw31rraZcBWB+yaUPhzE8NVwEIR3WRDXDul5g9Yk+f4xrmrZBWNt/FG54px0wvZ5H4wacSTiC142VMtH0vqMPMq2eHcag6Kn93YVSEcJ/N6ZJ+ipuIMwH6Xe+NSxOxqV1aPQCDSoSRwcmkQ1fdn1NArAHTOGV2G+7KGn83yD3FVAV9KYvkfeFwM/a0sn64AaV/VrTq9b0jGR/GuRCj77AKAUncfoTwuciUnwjlVSpUEW+wA/pYt4RxLGRj5I/M6Wy0A539VIctxKtMmVg82mKBvk5u4IuQNgLmzHcfXbHd1CCX+2qaG9ruAfrIZa38pakZ2A9YWaOI5ygwYxgrw8raUV7tyeyLBCoS24EAcYiT1+rgEm9FLGrZe+D/00kzRT9y22tU88omqri/jPgHJLJQoyULMaqAZlkUN8bAyaENGJSDaZd", errorMessage: ""
    }];

    getParams(): Array<IAppParams> {
        return this.params;
    }
    getDeepLinkVariables(): Array<IDeepLinkVariables> {
        return this.deepLinkVariables;
    }
}

class MockPeerComparisonService extends PeerComparisonService {
    public res: any;
    public unit: any = "unittest";
    fromUTest = true;
    getPeerComparison(tabKey, params) {
        return this.unit;
    }

 }


 
describe('PeerComparison DOM Tests', () => {

    beforeEach(async(() => {
        TestBed.resetTestEnvironment();
        TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting());
        ElementRef;
        TestBed.configureTestingModule({
            imports: [BrowserModule, FormsModule, HttpModule],
            declarations: [PeerComparison, Loader, ObjToArr]
        })
            .overrideComponent(PeerComparison,
            {
                remove: { providers: [AppService, PeerComparisonService] },
                add: {
                    providers: [
                        { provide: AppService, useClass: MockAppService },
                        { provide: PeerComparisonService, useClass: MockPeerComparisonService }
                    ]
                }
            }).compileComponents();

    }));

    it('Should Get Your No results Generic Text When User with No Benchmark Data', async(() => {
        const fixture = TestBed.createComponent(PeerComparison);
        const app = fixture.componentInstance;

        app.enableLoader = false;
        const json = JSON.parse('{ "YourQty":null, "AverageQty":null, "EfficientQty":null, "NoBenchmarksText":"Check back after you have received three bills, and we will let you know how your usage compares to similar homes.", "Commodities":"gas,electric,water", "ShowCommodityLabels":null, "NoBenchmarks":true, "ElectricServiceCount":0, "WaterServiceCount":0, "GasServiceCount":0, "CommoditiesText":null, "CommoditiesIconFonts":null, "CostOrUsage":null, "YourQtyColor":null, "AverageQtyColor":null, "EfficientQtyColor":null, "Benchmarks":null, "Title":"Usage Summary", "IntroText":"How does your usage compare?", "SubTitle":"Compare", "ShowTitle":"true", "ShowIntro":"true", "ShowSubTitle":"", "Footer":"Similar Homes are homes with the same style, size and location as yours. Energy Efficient homes are similar homes that are in the lowest 15 percent of energy usage.", "FooterLink":"http:///www.aclara.com/", "FooterLinkText":"Next Steps", "FooterLinkType":null, "ShowFooterText":"true", "ShowFooterLink":"true" } ');
        app.result = json;
        //app.setupCommodities();

        fixture.detectChanges();
        const h1Title = fixture.nativeElement.querySelectorAll("div.alert.alert-warning");
        expect(h1Title["0"].innerText).toContain("Check back");
    }));


    it('Should Get Your Usage Title When Benchmark', async(() => {
        const fixture = TestBed.createComponent(PeerComparison);
        const app = fixture.componentInstance;
       
     
        app.enableLoader = false;
        const json = JSON.parse('{"YourQty":"Your Use Last Month","Title":"Usage Summary","IntroText":"How does your usage compare?","SubTitle":"Compare","YourQty":"Your Use Last Month","AverageQty":"Similar Homes","EfficientQty":"Energy Efficient Homes","NoBenchmarksText":"no benchmarks found","Commodities":"gas,electric,water","CommoditiesText":"Gas,Electric,Water","ShowTitle":"true","ShowIntro":"true","ShowCommodityLabels":"false","NoBenchmarks":false,"ElectricServiceCount":1,"WaterServiceCount":1,"GasServiceCount":1,"CommoditiesIconFonts":"icon-flame,icon-bolt,icon-drop","CostOrUsage":"usage","MyUnits":152,"MyUnitsPercentage":27.586206896551722,"AvgUnits":551,"AvgUnitsPercentage":100,"EfficientUnits":152,"EfficientUnitsPercentage":27.586206896551722,"MyUnitsFormatted":"152 kWh","AvgUnitsFormatted":"551 kWh","EfficientUnitsFormatted":"152 kWh","YourQtyColor":"#69B96A","AverageQtyColor":"#E4BF60","EfficientQtyColor":"#5CACDD","Footer":"Similar Homes are homes with the same style, size and location as yours. Energy Efficient homes are similar homes that are in the lowest 15 percent of energy usage.","FooterLinkText":"Next Steps","FooterLink":"http:xxx","ShowFooterLink":"false","ShowFooterText":"true","data":[{"MyUnitsPercentage":27.586206896551722}],"Benchmarks":[{"StartDate":"2016-07-01T00:00:00","EndDate":"2016-08-01T00:00:00","CommodityKey":"electric","GroupKey":"Default","GroupCount":7,"MyUsage":152,"AverageUsage":551,"EfficientUsage":152,"MyUsageFormatted":"152","AverageUsageFormatted":"551","EfficientUsageFormatted":"152","UOMKey":"kwh","MyCost":193,"AverageCost":192.25,"EfficientCost":182,"MyCostFormatted":"$193","AverageCostFormatted":"$192","EfficientCostFormatted":"$182","CostCurrencyKey":"usd","UOMText":"kWh","CostCurrencySymbol":"$","Measurements":[]},{"StartDate":"2016-07-01T00:00:00","EndDate":"2016-08-01T00:00:00","CommodityKey":"gas","GroupKey":"Default","GroupCount":7,"MyUsage":152,"AverageUsage":100,"EfficientUsage":104,"MyUsageFormatted":"152","AverageUsageFormatted":"100","EfficientUsageFormatted":"104","UOMKey":"ccf","MyCost":193,"AverageCost":100,"EfficientCost":95,"MyCostFormatted":"$193","AverageCostFormatted":"$100","EfficientCostFormatted":"$95","CostCurrencyKey":"usd","UOMText":"CCF","CostCurrencySymbol":"$","Measurements":[]},{"StartDate":"2016-07-01T00:00:00","EndDate":"2016-08-01T00:00:00","CommodityKey":"water","GroupKey":"Default","GroupCount":7,"MyUsage":152,"AverageUsage":100,"EfficientUsage":100,"MyUsageFormatted":"152","AverageUsageFormatted":"100","EfficientUsageFormatted":"100","UOMKey":"gal","MyCost":193,"AverageCost":100,"EfficientCost":100,"MyCostFormatted":"$193","AverageCostFormatted":"$100","EfficientCostFormatted":"$100","CostCurrencyKey":"usd","UOMText":"gal","CostCurrencySymbol":"$","Measurements":[]}]}');
        app.result = json;
        app.setupCommodities();

 
        fixture.detectChanges();
        const h1Title = fixture.nativeElement.querySelectorAll("h1");
        

        expect(h1Title["0"].innerText).toContain("Usage Summary");
    }));

    it('Should Get Your Cost Title When User with Benchmark', async(() => {
        const fixture = TestBed.createComponent(PeerComparison);
        const app = fixture.componentInstance;


        app.enableLoader = false;
        const json = JSON.parse('{"YourQty":"Your Cost Last Month","Title":"Cost Summary","IntroText":"How does your cost compare?","SubTitle":"Compare","YourQty":"Your Cost Last Month","AverageQty":"Similar Homes","EfficientQty":"Energy Efficient Homes","NoBenchmarksText":"no benchmarks found","Commodities":"gas,electric,water","CommoditiesText":"Gas,Electric,Water","ShowTitle":"true","ShowIntro":"true","ShowCommodityLabels":"false","NoBenchmarks":false,"ElectricServiceCount":1,"WaterServiceCount":1,"GasServiceCount":1,"CommoditiesIconFonts":"icon-flame,icon-bolt,icon-drop","CostOrUsage":"cost","MyUnits":152,"MyUnitsPercentage":27.586206896551722,"AvgUnits":551,"AvgUnitsPercentage":100,"EfficientUnits":152,"EfficientUnitsPercentage":27.586206896551722,"MyUnitsFormatted":"152 kWh","AvgUnitsFormatted":"551 kWh","EfficientUnitsFormatted":"152 kWh","YourQtyColor":"#69B96A","AverageQtyColor":"#E4BF60","EfficientQtyColor":"#5CACDD","Footer":"Similar Homes are homes with the same style, size and location as yours. Energy Efficient homes are similar homes that are in the lowest 15 percent of energy usage.","FooterLinkText":"Next Steps","FooterLink":"http:xxx","ShowFooterLink":"false","ShowFooterText":"true","data":[{"MyUnitsPercentage":27.586206896551722}],"Benchmarks":[{"StartDate":"2016-07-01T00:00:00","EndDate":"2016-08-01T00:00:00","CommodityKey":"electric","GroupKey":"Default","GroupCount":7,"MyUsage":152,"AverageUsage":551,"EfficientUsage":152,"MyUsageFormatted":"152","AverageUsageFormatted":"551","EfficientUsageFormatted":"152","UOMKey":"kwh","MyCost":193,"AverageCost":192.25,"EfficientCost":182,"MyCostFormatted":"$193","AverageCostFormatted":"$192","EfficientCostFormatted":"$182","CostCurrencyKey":"usd","UOMText":"kWh","CostCurrencySymbol":"$","Measurements":[]},{"StartDate":"2016-07-01T00:00:00","EndDate":"2016-08-01T00:00:00","CommodityKey":"gas","GroupKey":"Default","GroupCount":7,"MyUsage":152,"AverageUsage":100,"EfficientUsage":104,"MyUsageFormatted":"152","AverageUsageFormatted":"100","EfficientUsageFormatted":"104","UOMKey":"ccf","MyCost":193,"AverageCost":100,"EfficientCost":95,"MyCostFormatted":"$193","AverageCostFormatted":"$100","EfficientCostFormatted":"$95","CostCurrencyKey":"usd","UOMText":"CCF","CostCurrencySymbol":"$","Measurements":[]},{"StartDate":"2016-07-01T00:00:00","EndDate":"2016-08-01T00:00:00","CommodityKey":"water","GroupKey":"Default","GroupCount":7,"MyUsage":152,"AverageUsage":100,"EfficientUsage":100,"MyUsageFormatted":"152","AverageUsageFormatted":"100","EfficientUsageFormatted":"100","UOMKey":"gal","MyCost":193,"AverageCost":100,"EfficientCost":100,"MyCostFormatted":"$193","AverageCostFormatted":"$100","EfficientCostFormatted":"$100","CostCurrencyKey":"usd","UOMText":"gal","CostCurrencySymbol":"$","Measurements":[]}]}');
        app.result = json;
        app.setupCommodities();

        fixture.detectChanges();
        const h1Title = fixture.nativeElement.querySelectorAll("h1");
        expect(h1Title["0"].innerText).toContain("Cost Summary");
    }));

    it('Should Get Electric Cost After Click on Electric Icon', async(() => {
        const fixture = TestBed.createComponent(PeerComparison);
        const app = fixture.componentInstance;
        const element = fixture.nativeElement;
        app.enableLoader = false;
        const json = JSON.parse('{"YourQty":"Your Cost Last Month","Title":"Cost Summary","IntroText":"How does your cost compare?","SubTitle":"Compare","YourQty":"Your Cost Last Month","AverageQty":"Similar Homes","EfficientQty":"Energy Efficient Homes","NoBenchmarksText":"no benchmarks found","Commodities":"gas,electric,water","CommoditiesText":"Gas,Electric,Water","ShowTitle":"true","ShowIntro":"true","ShowCommodityLabels":"false","NoBenchmarks":false,"ElectricServiceCount":1,"WaterServiceCount":1,"GasServiceCount":1,"CommoditiesIconFonts":"icon-flame,icon-bolt,icon-drop","CostOrUsage":"cost","MyUnits":152,"MyUnitsPercentage":27.586206896551722,"AvgUnits":551,"AvgUnitsPercentage":100,"EfficientUnits":152,"EfficientUnitsPercentage":27.586206896551722,"MyUnitsFormatted":"152 kWh","AvgUnitsFormatted":"551 kWh","EfficientUnitsFormatted":"152 kWh","YourQtyColor":"#69B96A","AverageQtyColor":"#E4BF60","EfficientQtyColor":"#5CACDD","Footer":"Similar Homes are homes with the same style, size and location as yours. Energy Efficient homes are similar homes that are in the lowest 15 percent of energy usage.","FooterLinkText":"Next Steps","FooterLink":"http:xxx","ShowFooterLink":"false","ShowFooterText":"true","data":[{"MyUnitsPercentage":27.586206896551722}],"Benchmarks":[{"StartDate":"2016-07-01T00:00:00","EndDate":"2016-08-01T00:00:00","CommodityKey":"electric","GroupKey":"Default","GroupCount":7,"MyUsage":152,"AverageUsage":551,"EfficientUsage":152,"MyUsageFormatted":"152","AverageUsageFormatted":"551","EfficientUsageFormatted":"152","UOMKey":"kwh","MyCost":193,"AverageCost":192.25,"EfficientCost":182,"MyCostFormatted":"$193","AverageCostFormatted":"$192","EfficientCostFormatted":"$182","CostCurrencyKey":"usd","UOMText":"kWh","CostCurrencySymbol":"$","Measurements":[]},{"StartDate":"2016-07-01T00:00:00","EndDate":"2016-08-01T00:00:00","CommodityKey":"gas","GroupKey":"Default","GroupCount":7,"MyUsage":152,"AverageUsage":100,"EfficientUsage":104,"MyUsageFormatted":"152","AverageUsageFormatted":"100","EfficientUsageFormatted":"104","UOMKey":"ccf","MyCost":193,"AverageCost":100,"EfficientCost":95,"MyCostFormatted":"$193","AverageCostFormatted":"$100","EfficientCostFormatted":"$95","CostCurrencyKey":"usd","UOMText":"CCF","CostCurrencySymbol":"$","Measurements":[]},{"StartDate":"2016-07-01T00:00:00","EndDate":"2016-08-01T00:00:00","CommodityKey":"water","GroupKey":"Default","GroupCount":7,"MyUsage":152,"AverageUsage":100,"EfficientUsage":100,"MyUsageFormatted":"152","AverageUsageFormatted":"100","EfficientUsageFormatted":"100","UOMKey":"gal","MyCost":193,"AverageCost":100,"EfficientCost":100,"MyCostFormatted":"$193","AverageCostFormatted":"$100","EfficientCostFormatted":"$100","CostCurrencyKey":"usd","UOMText":"gal","CostCurrencySymbol":"$","Measurements":[]}]}');
        app.result = json;
        app.setupCommodities();

        fixture.detectChanges();
        var link = fixture.nativeElement.querySelectorAll("a.btn.btn-default.icon__electric.xs-ml-5");
        link[0].click();

        fixture.detectChanges();
        //verify electic costs show up after click on electric button (should switch from water);
        expect(element.textContent).toContain('$193');
    }));

    it('Should Get Electric Usage After Click on Electric Icon', async(() => {
        const fixture = TestBed.createComponent(PeerComparison);
        const app = fixture.componentInstance;
        const element = fixture.nativeElement;
        app.enableLoader = false;
        const json = JSON.parse('{"YourQty":"Your Use Last Month","Title":"Usage Summary","IntroText":"How does your usage compare?","SubTitle":"Compare","YourQty":"Your Use Last Month","AverageQty":"Similar Homes","EfficientQty":"Energy Efficient Homes","NoBenchmarksText":"no benchmarks found","Commodities":"gas,electric,water","CommoditiesText":"Gas,Electric,Water","ShowTitle":"true","ShowIntro":"true","ShowCommodityLabels":"false","NoBenchmarks":false,"ElectricServiceCount":1,"WaterServiceCount":1,"GasServiceCount":1,"CommoditiesIconFonts":"icon-flame,icon-bolt,icon-drop","CostOrUsage":"usage","MyUnits":152,"MyUnitsPercentage":27.586206896551722,"AvgUnits":551,"AvgUnitsPercentage":100,"EfficientUnits":152,"EfficientUnitsPercentage":27.586206896551722,"MyUnitsFormatted":"152 kWh","AvgUnitsFormatted":"551 kWh","EfficientUnitsFormatted":"152 kWh","YourQtyColor":"#69B96A","AverageQtyColor":"#E4BF60","EfficientQtyColor":"#5CACDD","Footer":"Similar Homes are homes with the same style, size and location as yours. Energy Efficient homes are similar homes that are in the lowest 15 percent of energy usage.","FooterLinkText":"Next Steps","FooterLink":"http:xxx","ShowFooterLink":"false","ShowFooterText":"true","data":[{"MyUnitsPercentage":27.586206896551722}],"Benchmarks":[{"StartDate":"2016-07-01T00:00:00","EndDate":"2016-08-01T00:00:00","CommodityKey":"electric","GroupKey":"Default","GroupCount":7,"MyUsage":152,"AverageUsage":551,"EfficientUsage":152,"MyUsageFormatted":"152","AverageUsageFormatted":"551","EfficientUsageFormatted":"152","UOMKey":"kwh","MyCost":193,"AverageCost":192.25,"EfficientCost":182,"MyCostFormatted":"$193","AverageCostFormatted":"$192","EfficientCostFormatted":"$182","CostCurrencyKey":"usd","UOMText":"kWh","CostCurrencySymbol":"$","Measurements":[]},{"StartDate":"2016-07-01T00:00:00","EndDate":"2016-08-01T00:00:00","CommodityKey":"gas","GroupKey":"Default","GroupCount":7,"MyUsage":152,"AverageUsage":100,"EfficientUsage":104,"MyUsageFormatted":"152","AverageUsageFormatted":"100","EfficientUsageFormatted":"104","UOMKey":"ccf","MyCost":193,"AverageCost":100,"EfficientCost":95,"MyCostFormatted":"$193","AverageCostFormatted":"$100","EfficientCostFormatted":"$95","CostCurrencyKey":"usd","UOMText":"CCF","CostCurrencySymbol":"$","Measurements":[]},{"StartDate":"2016-07-01T00:00:00","EndDate":"2016-08-01T00:00:00","CommodityKey":"water","GroupKey":"Default","GroupCount":7,"MyUsage":152,"AverageUsage":100,"EfficientUsage":100,"MyUsageFormatted":"152","AverageUsageFormatted":"100","EfficientUsageFormatted":"100","UOMKey":"gal","MyCost":193,"AverageCost":100,"EfficientCost":100,"MyCostFormatted":"$193","AverageCostFormatted":"$100","EfficientCostFormatted":"$100","CostCurrencyKey":"usd","UOMText":"gal","CostCurrencySymbol":"$","Measurements":[]}]}');
        app.result = json;
        app.setupCommodities();

        fixture.detectChanges();
        var link = fixture.nativeElement.querySelectorAll("a.btn.btn-default.icon__electric.xs-ml-5");
        link[0].click();

        fixture.detectChanges();
        //verify electic costs show up after click on electric button (should switch from water);
        expect(element.textContent).toContain('152 kWh');
    }));
});