"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var billhistory_1 = require("../../BillHistory/billhistory");
var billhistory_service_1 = require("../../BillHistory/billhistory.service");
var app_service_1 = require("../../app.service");
var MockElementRef = (function () {
    function MockElementRef() {
        this.nativeElement = {};
    }
    return MockElementRef;
}());
describe('BillHistory Typescript Tests', function () {
    it('BillHisotry loadBillHistory().  Component main entry point.', function () {
        var http;
        var appService = new app_service_1.AppService(http);
        var qlService = new billhistory_service_1.BillHistoryService(http);
        var elementRef = new MockElementRef();
        appService.params = [{ tabKey: "tab.billstatement", parameters: "YbUGYEN1OhOANtb1zjkfy/hq/cI+rQ4HQlLIgZfqZL7OdgsF8ZKqzAw3eqO0PtMqSEW2CiNa9iOob71Xc8rETBZ0b0gp5N9mczstPSIn8k41jMUEo/KUHHOvQMT89omslD8Zxrj3CSA5CIYCEkMqiRFvtTquCvGi0GQTcZ19rqt1WAisugqWAXqjCKDEgwEAtTxKtAADOS1GKFAOc8O/vAn6PK0IcfRMmQ580IKEVhAnIE2UI2E8B3TjBR426Obt+NM4ppU46hcpbwY0IUil1ojq/lN/d8AzTriNnHjy3eVb2OyYDuFYOFopUUpX/v1qUrVgV7VD96UW2zEGS5zRb4N0/RHYCSwUE3qgwkzyOjUaOX3L9Jrwk9e4tNouzbEbjbXPjTwDcqbqUBk/Wn5eik5/64JISCsuqUNSz5GbjMN4F4WYoFT37PtjCq7KsV16YccmF3NObF9MfU45pgPyRI8Qas6g2fG/XdChanb7sRPGDj0C0do7/86T3PyOYJoV4v3+afpi7+iSGcfnYR7bhg==", errorMessage: "" }];
        qlService.res = JSON.parse('{ "Footer": "", "FooterLink": "", "FooterLinkText": "", "IntroText": "", "LinkList": [{ "ActionPriority": 100, "Description": "","Key":"quicklink.paybill","Url":"www.aclara.com" }], "Title": "Quick Links"  }');
        var billhist = new billhistory_1.BillHistory(qlService, appService, elementRef);
        billhist.currentPage = 0;
        billhist.fromUTest = true;
        expect(billhist.currentPage).toBe(0);
    });
    it('BillHisotry getBills().  Loading bills for current page.', function () {
        var http;
        var appService = new app_service_1.AppService(http);
        var qlService = new billhistory_service_1.BillHistoryService(http);
        var elementRef = new MockElementRef();
        appService.params = [{ tabKey: "tab.billstatement", parameters: "YbUGYEN1OhOANtb1zjkfy/hq/cI+rQ4HQlLIgZfqZL7OdgsF8ZKqzAw3eqO0PtMqSEW2CiNa9iOob71Xc8rETBZ0b0gp5N9mczstPSIn8k41jMUEo/KUHHOvQMT89omslD8Zxrj3CSA5CIYCEkMqiRFvtTquCvGi0GQTcZ19rqt1WAisugqWAXqjCKDEgwEAtTxKtAADOS1GKFAOc8O/vAn6PK0IcfRMmQ580IKEVhAnIE2UI2E8B3TjBR426Obt+NM4ppU46hcpbwY0IUil1ojq/lN/d8AzTriNnHjy3eVb2OyYDuFYOFopUUpX/v1qUrVgV7VD96UW2zEGS5zRb4N0/RHYCSwUE3qgwkzyOjUaOX3L9Jrwk9e4tNouzbEbjbXPjTwDcqbqUBk/Wn5eik5/64JISCsuqUNSz5GbjMN4F4WYoFT37PtjCq7KsV16YccmF3NObF9MfU45pgPyRI8Qas6g2fG/XdChanb7sRPGDj0C0do7/86T3PyOYJoV4v3+afpi7+iSGcfnYR7bhg==", errorMessage: "" }];
        qlService.res = JSON.parse('{ "Footer": "", "FooterLink": "", "FooterLinkText": "", "IntroText": "", "LinkList": [{ "ActionPriority": 100, "Description": "","Key":"quicklink.paybill","Url":"www.aclara.com" }], "Title": "Quick Links"  }');
        var billhist = new billhistory_1.BillHistory(qlService, appService, elementRef);
        billhist.result.RowCount = 2;
        billhist.result.Bills = [1, 2, 3, 4, 5];
        billhist.currentPage = 0;
        billhist.result.Bills.length = 5;
        billhist.getBills(0);
        expect(billhist.paginate).toBe(true);
        expect(billhist.pages.length).toBeGreaterThan(0);
    });
    it('BillHisotry pageChange(direction).  Page change on clicking page number.', function () {
        var http;
        var appService = new app_service_1.AppService(http);
        var qlService = new billhistory_service_1.BillHistoryService(http);
        var elementRef = new MockElementRef();
        appService.params = [{ tabKey: "tab.billstatement", parameters: "YbUGYEN1OhOANtb1zjkfy/hq/cI+rQ4HQlLIgZfqZL7OdgsF8ZKqzAw3eqO0PtMqSEW2CiNa9iOob71Xc8rETBZ0b0gp5N9mczstPSIn8k41jMUEo/KUHHOvQMT89omslD8Zxrj3CSA5CIYCEkMqiRFvtTquCvGi0GQTcZ19rqt1WAisugqWAXqjCKDEgwEAtTxKtAADOS1GKFAOc8O/vAn6PK0IcfRMmQ580IKEVhAnIE2UI2E8B3TjBR426Obt+NM4ppU46hcpbwY0IUil1ojq/lN/d8AzTriNnHjy3eVb2OyYDuFYOFopUUpX/v1qUrVgV7VD96UW2zEGS5zRb4N0/RHYCSwUE3qgwkzyOjUaOX3L9Jrwk9e4tNouzbEbjbXPjTwDcqbqUBk/Wn5eik5/64JISCsuqUNSz5GbjMN4F4WYoFT37PtjCq7KsV16YccmF3NObF9MfU45pgPyRI8Qas6g2fG/XdChanb7sRPGDj0C0do7/86T3PyOYJoV4v3+afpi7+iSGcfnYR7bhg==", errorMessage: "" }];
        qlService.res = JSON.parse('{ "Footer": "", "FooterLink": "", "FooterLinkText": "", "IntroText": "", "LinkList": [{ "ActionPriority": 100, "Description": "","Key":"quicklink.paybill","Url":"www.aclara.com" }], "Title": "Quick Links"  }');
        var billhist = new billhistory_1.BillHistory(qlService, appService, elementRef);
        billhist.result.Bills = [1, 2, 3, 4, 5];
        billhist.totalPages = 1;
        billhist.pageChange('1');
        expect(billhist.currentPage).toBe(0);
        billhist.totalPages = 3;
        billhist.pageChange('2');
        expect(billhist.currentPage).toBe(1);
    });
    it('BillHisotry pageChange(direction).  Page change on clicking previous and next direction.', function () {
        var http;
        var appService = new app_service_1.AppService(http);
        var qlService = new billhistory_service_1.BillHistoryService(http);
        var elementRef = new MockElementRef();
        appService.params = [{ tabKey: "tab.billstatement", parameters: "YbUGYEN1OhOANtb1zjkfy/hq/cI+rQ4HQlLIgZfqZL7OdgsF8ZKqzAw3eqO0PtMqSEW2CiNa9iOob71Xc8rETBZ0b0gp5N9mczstPSIn8k41jMUEo/KUHHOvQMT89omslD8Zxrj3CSA5CIYCEkMqiRFvtTquCvGi0GQTcZ19rqt1WAisugqWAXqjCKDEgwEAtTxKtAADOS1GKFAOc8O/vAn6PK0IcfRMmQ580IKEVhAnIE2UI2E8B3TjBR426Obt+NM4ppU46hcpbwY0IUil1ojq/lN/d8AzTriNnHjy3eVb2OyYDuFYOFopUUpX/v1qUrVgV7VD96UW2zEGS5zRb4N0/RHYCSwUE3qgwkzyOjUaOX3L9Jrwk9e4tNouzbEbjbXPjTwDcqbqUBk/Wn5eik5/64JISCsuqUNSz5GbjMN4F4WYoFT37PtjCq7KsV16YccmF3NObF9MfU45pgPyRI8Qas6g2fG/XdChanb7sRPGDj0C0do7/86T3PyOYJoV4v3+afpi7+iSGcfnYR7bhg==", errorMessage: "" }];
        qlService.res = JSON.parse('{ "Footer": "", "FooterLink": "", "FooterLinkText": "", "IntroText": "", "LinkList": [{ "ActionPriority": 100, "Description": "","Key":"quicklink.paybill","Url":"www.aclara.com" }], "Title": "Quick Links"  }');
        var billhist = new billhistory_1.BillHistory(qlService, appService, elementRef);
        billhist.result.Bills = [1, 2, 3, 4, 5];
        billhist.currentPage = 1;
        billhist.pageChange("prev");
        expect(billhist.currentPage).toBe(0);
        billhist.currentPage = 0;
        billhist.pageChange("prev");
        expect(billhist.prevDisabled).toBe(true);
        billhist.totalPages = 2;
        billhist.currentPage = 0;
        billhist.pageChange("next");
        expect(billhist.currentPage).toBe(1);
        billhist.totalPages = 2;
        billhist.currentPage = 1;
        billhist.pageChange("next");
        expect(billhist.nextDisabled).toBe(true);
    });
    it('BillHisotry isCurrentPage().  Check for current page bills.', function () {
        var http;
        var appService = new app_service_1.AppService(http);
        var qlService = new billhistory_service_1.BillHistoryService(http);
        var elementRef = new MockElementRef();
        appService.params = [{ tabKey: "tab.billstatement", parameters: "YbUGYEN1OhOANtb1zjkfy/hq/cI+rQ4HQlLIgZfqZL7OdgsF8ZKqzAw3eqO0PtMqSEW2CiNa9iOob71Xc8rETBZ0b0gp5N9mczstPSIn8k41jMUEo/KUHHOvQMT89omslD8Zxrj3CSA5CIYCEkMqiRFvtTquCvGi0GQTcZ19rqt1WAisugqWAXqjCKDEgwEAtTxKtAADOS1GKFAOc8O/vAn6PK0IcfRMmQ580IKEVhAnIE2UI2E8B3TjBR426Obt+NM4ppU46hcpbwY0IUil1ojq/lN/d8AzTriNnHjy3eVb2OyYDuFYOFopUUpX/v1qUrVgV7VD96UW2zEGS5zRb4N0/RHYCSwUE3qgwkzyOjUaOX3L9Jrwk9e4tNouzbEbjbXPjTwDcqbqUBk/Wn5eik5/64JISCsuqUNSz5GbjMN4F4WYoFT37PtjCq7KsV16YccmF3NObF9MfU45pgPyRI8Qas6g2fG/XdChanb7sRPGDj0C0do7/86T3PyOYJoV4v3+afpi7+iSGcfnYR7bhg==", errorMessage: "" }];
        qlService.res = JSON.parse('{ "Footer": "", "FooterLink": "", "FooterLinkText": "", "IntroText": "", "LinkList": [{ "ActionPriority": 100, "Description": "","Key":"quicklink.paybill","Url":"www.aclara.com" }], "Title": "Quick Links"  }');
        var billhist = new billhistory_1.BillHistory(qlService, appService, elementRef);
        billhist.result.Bills = [1, 2, 3, 4, 5];
        billhist.currentPage = 1;
        expect(billhist.isCurrentPage(2)).toBe(true);
        billhist.currentPage = 1;
        expect(billhist.isCurrentPage(1)).toBe(false);
    });
    it('BillHisotry isLastBill(bill).  Check for last bill row.', function () {
        var http;
        var appService = new app_service_1.AppService(http);
        var qlService = new billhistory_service_1.BillHistoryService(http);
        var elementRef = new MockElementRef();
        appService.params = [{ tabKey: "tab.billstatement", parameters: "YbUGYEN1OhOANtb1zjkfy/hq/cI+rQ4HQlLIgZfqZL7OdgsF8ZKqzAw3eqO0PtMqSEW2CiNa9iOob71Xc8rETBZ0b0gp5N9mczstPSIn8k41jMUEo/KUHHOvQMT89omslD8Zxrj3CSA5CIYCEkMqiRFvtTquCvGi0GQTcZ19rqt1WAisugqWAXqjCKDEgwEAtTxKtAADOS1GKFAOc8O/vAn6PK0IcfRMmQ580IKEVhAnIE2UI2E8B3TjBR426Obt+NM4ppU46hcpbwY0IUil1ojq/lN/d8AzTriNnHjy3eVb2OyYDuFYOFopUUpX/v1qUrVgV7VD96UW2zEGS5zRb4N0/RHYCSwUE3qgwkzyOjUaOX3L9Jrwk9e4tNouzbEbjbXPjTwDcqbqUBk/Wn5eik5/64JISCsuqUNSz5GbjMN4F4WYoFT37PtjCq7KsV16YccmF3NObF9MfU45pgPyRI8Qas6g2fG/XdChanb7sRPGDj0C0do7/86T3PyOYJoV4v3+afpi7+iSGcfnYR7bhg==", errorMessage: "" }];
        qlService.res = JSON.parse('{ "Footer": "", "FooterLink": "", "FooterLinkText": "", "IntroText": "", "LinkList": [{ "ActionPriority": 100, "Description": "","Key":"quicklink.paybill","Url":"www.aclara.com" }], "Title": "Quick Links"  }');
        var billhist = new billhistory_1.BillHistory(qlService, appService, elementRef);
        billhist.result.Bills = [1, 2, 3, 4, 5];
        expect(billhist.isLastBill(5)).toBe(true);
        expect(billhist.isLastBill(2)).toBe(false);
    });
});
//# sourceMappingURL=billhistory.typescr.spec.js.map