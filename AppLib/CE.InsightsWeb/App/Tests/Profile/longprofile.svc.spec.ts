﻿import { inject, async, TestBed } from "@angular/core/testing";
import { Http, HttpModule, XHRBackend, Response, ResponseOptions } from "@angular/http";
import { MockBackend, MockConnection } from "@angular/http/testing";
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import {ProfileService} from "../../Profile/profile.service";

describe('TestProfileService -- Mock', () => {
    beforeEach(async(() => {
        TestBed.resetTestEnvironment();

        TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting());
        TestBed.configureTestingModule({
            imports: [HttpModule],
            providers: [
                ProfileService,
                { provide: XHRBackend, useClass: MockBackend }
            ]
        }).compileComponents();;
    }));

    it('can instantiate service when inject service',
        inject([ProfileService], (service: ProfileService) => {
            expect(service instanceof ProfileService).toBe(true);
        }));

    it('can provide the mockBackend as XHRBackend',
        inject([XHRBackend], (backend: MockBackend) => {
            expect(backend).not.toBeNull('backend should be provided');
        }));


    let backend: MockBackend;
    let service: ProfileService;
    let response: Response;

    beforeEach(inject([Http, XHRBackend], (http: Http, be: MockBackend) => {
        backend = be;
        service = new ProfileService(http);
        let options = new ResponseOptions({
            body: [
                {
                    id: "QA87HomeProf",
                    contentRendered: "<p><b>Test</b></p>",
                    contentMarkdown: "*Hi there*"
                }
            ]
        });
        response = new Response(options);
    }));

    it('should get long profile', async(inject([], () => {
        backend.connections.subscribe((c: MockConnection) => c.mockRespond(response));

        service.getLongProfile('', '', '').subscribe((blogs) => {
                expect(blogs.length).toBe(1);
                expect(blogs[0].id).toBe("QA87HomeProf");
                expect(blogs[0].contentRendered).toBe("<p><b>Test</b></p>");
            });
    })));

    it('should post long profile', async(inject([], () => {
        backend.connections.subscribe((c: MockConnection) => c.mockRespond(response));

        service.postLongProfile('').subscribe((blogs) => {
            expect(blogs.length).toBe(1);
            expect(blogs[0].id).toBe("QA87HomeProf");
            expect(blogs[0].contentRendered).toBe("<p><b>Test</b></p>");
        });
    })));

    it('should get short profile', async(inject([], () => {
        backend.connections.subscribe((c: MockConnection) => c.mockRespond(response));

        service.getProfile('', '').subscribe((blogs) => {
            expect(blogs.length).toBe(1);
            expect(blogs[0].id).toBe("QA87HomeProf");
            expect(blogs[0].contentRendered).toBe("<p><b>Test</b></p>");
        });
    })));

    it('should post short profile', async(inject([], () => {
        backend.connections.subscribe((c: MockConnection) => c.mockRespond(response));

        service.postProfile('').subscribe((blogs) => {
            expect(blogs.length).toBe(1);
            expect(blogs[0].id).toBe("QA87HomeProf");
            expect(blogs[0].contentRendered).toBe("<p><b>Test</b></p>");
        });
    })));
});