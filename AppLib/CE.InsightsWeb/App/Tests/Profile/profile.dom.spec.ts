﻿import { async, TestBed } from "@angular/core/testing";
import { ElementRef } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { HttpModule } from "@angular/http";
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from "@angular/platform-browser-dynamic/testing";
import { Profile } from "../../Profile/profile";
import { ProfileService } from "../../Profile/profile.service";
import { AppService, IAppParams, IDeepLinkVariables } from "../../app.service";
import { Loader } from "../../Loader/loader";
import { ObjToArr } from "../../Helper/pipe";

//mock profile and app services
class MockAppService extends AppService {
    public params: Array<IAppParams> = [{
        tabKey: "tab.dashboard", parameters: "YbUGYEN1OhOANtb1zjkfy/hq/cI+rQ4HQlLIgZfqZL5V2ctJGNmEOdmE/iQaQ3KRdd4nNk5O9zhAuNIFw31rraZcBWB+yaUPhzE8NVwEIR3WRDXDul5g9Yk+f4xrmrZBWNt/FG54px0wvZ5H4wacSTiC142VMtH0vqMPMq2eHcag6Kn93YVSEcJ/N6ZJ+ipuIMwH6Xe+NSxOxqV1aPQCDSoSRwcmkQ1fdn1NArAHTOGV2G+7KGn83yD3FVAV9KYvkfeFwM/a0sn64AaV/VrTq9b0jGR/GuRCj77AKAUncfoTwuciUnwjlVSpUEW+wA/pYt4RxLGRj5I/M6Wy0A539VIctxKtMmVg82mKBvk5u4IuQNgLmzHcfXbHd1CCX+2qaG9ruAfrIZa38pakZ2A9YWaOI5ygwYxgrw8raUV7tyeyLBCoS24EAcYiT1+rgEm9FLGrZe+D/00kzRT9y22tU88omqri/jPgHJLJQoyULMaqAZlkUN8bAyaENGJSDaZd", errorMessage: "" }];
    
    getParams(): Array<IAppParams> {
        return this.params;
    }
    getDeepLinkVariables(): Array<IDeepLinkVariables> {
        return this.deepLinkVariables;
    }
}

class MockProfService extends ProfileService {
    public res: any;
    public unit: any = "unittest";
    getProfile(tabKey, params) {
        return this.unit;
    }
}

describe('ShortProfile DOM Tests', () => {

    beforeEach(async(() => {
        TestBed.resetTestEnvironment();
        TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting());
        ElementRef;
        TestBed.configureTestingModule({
            imports: [BrowserModule, FormsModule, HttpModule],
            declarations: [Profile, Loader, ObjToArr]
        })
            .overrideComponent(Profile,
            {
                remove: { providers: [AppService, ProfileService] },
                add: {
                    providers: [
                        { provide: AppService, useClass: MockAppService },
                        { provide: ProfileService, useClass: MockProfService }
                    ]
                }
            }).compileComponents();

    }));

    xit('error params.map undefined - should have Next button and Title', async(() => {
        const fixture = TestBed.createComponent(Profile);
        const app = fixture.componentInstance;

        app.enableLoader = false;
        const json = JSON.parse('{"Completeness":20,"CompletenessTitle":"Youre done!","CompletenessSubtitle":"Next Step: Set a Savings Goal","CompletenessDescription":"**Save 5%:** What would I have to do?  To achieve this goal will require a few habit changes or low-cost home upgrades. You could achieve this by air drying one load of laundry a week instead of using a gas dryer, by turning your heating thermostat down by one degree all winter. Save 15%: What would I have to do? To achieve this goal you will need to make some big changes to your habit and several low-cost home upgrades, or possibly invest in a larger upgrade. You could achieve this by air drying all your laundry instead of using a gas dryer and installing more attic insulation, or by upgrading your heating system to the most efficient available.","CompletenessLabel":"Completed","NextButtonLabel":"Next","NotCompleteMessage":"Please answer this question","BackButtonLabel":null,"Title":"Energy Profile","IntroText":"","DropDownSelectText":"Select","ShowTitle":"true","ShowIntro":"","ShowSectionIntro":"","LoadingMessage":"Loading...","OverviewTabId":null,"Footer":"Sample footer text.","FooterLinkText":"Next Steps","FooterLink":"aclara.com/","ShowFooterLink":"false","ShowFooterText":"false","RequiredKey":"Required","RequiredSymbol":"*","LongProfileLinkText":"Show Advanced Profile","ShowRequiredKey":"true","ShowLongProfileLink":"true","LongProfileLink":"Page?enc=sPYGI5L/s9Mw7Kh/BWetXbMqdTEC05gj3zhW1T313FdlMy91yISWZAnZE7tRAb6ftGVwf2zLZdXMJ8w3L9u6WGXl9sWArTkjtx0FagTZsgdHvNGywN4w2mmpDwUEychdnrEJtJDxaOzg8pJ6AS98NblKSM0418c7Zqxz2B/zeu9Cqzd9T/j9JFiXW+MH1ez4qdIcHQpzbpV7bKfaRzQ/9awNLroDpdJ84fpRxRDID/qyPAsA2V7Hg3hKV4tq24qJnoMRKVK/XBZmlL2XPJGw9XHAQVRpPeQoKp8n5XqcrKxFNu4E4vf+HV+M7+tlIzcxnKQUrgeaHmEZwxYwpLAdNc6D4YJZPaJOUQoKMiXGyN1vRp4uByFOmH88Qrg4is/A2smysYdEAd24mzfDn9P+N94gw4Z+72H8hBeD9apqXfpdgWeKMVa39Hn+ILy90sCIVCx231iUmEHzXbfPTe5i/A==","ShowOverviewTab":null,"ShowOverviewTabColumnOneCount":null,"OverviewText":null,"SaveButtonLabel":null,"PageOf":null,"SavedMessage":null,"Tabs":[{"Key":null,"Name":null,"IconClass":null,"Sections":[{"Key":null,"SectionTitle":"Heating","SectionSubtitle":"The heat is on","SectionDescription":"The average home in the U.S. spends 45% of its total energy bills on heating. You can lower your heating energy use by improving the heating source (like the furnace or boiler), the distribution system (like the ducts or radiators), or the controls (like the thermostat settings).","SectionMarkdown":"","ImageUrl":"","ImageTitle":"244295881programmablethermostatneardoormedium","Questions":[{"Key":"heatsystem.fuel","Label":"What is the primary fuel used by your heating system?","Tooltip":null,"Type":"list","OptionText":"Gas,Electric,Oil,Propane,Solar,Wood,","OptionValue":"heatsystem.fuel.gas,heatsystem.fuel.electric,heatsystem.fuel.oil,heatsystem.fuel.propane,heatsystem.fuel.solar,heatsystem.fuel.wood","Min":"0","Max":"0","Answer":"","Level":"premise","RangeErrorMessage":null,"InvalidLengthMessage":null,"InvalidRequired":false,"InvalidRange":false,"InvalidLength":false,"Options":{"Value":["heatsystem.fuel.gas","heatsystem.fuel.electric","heatsystem.fuel.oil","heatsystem.fuel.propane","heatsystem.fuel.solar","heatsystem.fuel.wood"],"Text":["Gas","Electric","Oil","Propane","Solar","Wood"]}},{"Key":"heatsystem.style","Label":"What type of heating system do you have?","Tooltip":null,"Type":"list","OptionText":"Forced Air Furnace,Steam Boiler,Water Boiler,Air Source Heat Pump,Conventional Oil,Baseboard Resistance,Geothermal Heat Pump,Ground Source Heat Pump,Radiant,","OptionValue":"heatsystem.style.forcedairfurnace,heatsystem.style.steamboiler,heatsystem.style.waterboiler,heatsystem.style.airsourceheatpump,heatsystem.style.conventionaloil,heatsystem.style.baseboardresistance,heatsystem.style.geothermalheatpump,heatsystem.style.groundsourceheatpump,heatsystem.style.radiant","Min":"0","Max":"0","Answer":"","Level":"premise","RangeErrorMessage":null,"InvalidLengthMessage":null,"InvalidRequired":false,"InvalidRange":false,"InvalidLength":false,"Options":{"Value":["heatsystem.style.forcedairfurnace","heatsystem.style.steamboiler","heatsystem.style.waterboiler","heatsystem.style.airsourceheatpump","heatsystem.style.conventionaloil","heatsystem.style.baseboardresistance","heatsystem.style.geothermalheatpump","heatsystem.style.groundsourceheatpump","heatsystem.style.radiant"],"Text":["Forced Air Furnace","Steam Boiler","Water Boiler","Air Source Heat Pump","Conventional Oil","Baseboard Resistance","Geothermal Heat Pump","Ground Source Heat Pump","Radiant"]}},{"Key":"heatsystem.yearinstalledrange","Label":"When was your heating system installed?","Tooltip":null,"Type":"list","OptionText":"after 2015,2011 - 2015,2006 - 2010,2001 - 2005,1996 - 2000,1990 - 1995,1980s,1970s,1960s,1950s,before 1950,","OptionValue":"heatsystem.yearinstalledrange.2020,heatsystem.yearinstalledrange.2015,heatsystem.yearinstalledrange.2010,heatsystem.yearinstalledrange.2005,heatsystem.yearinstalledrange.2000,heatsystem.yearinstalledrange.1995,heatsystem.yearinstalledrange.1985,heatsystem.yearinstalledrange.1975,heatsystem.yearinstalledrange.1965,heatsystem.yearinstalledrange.1955,heatsystem.yearinstalledrange.before1950","Min":"0","Max":"0","Answer":"","Level":"premise","RangeErrorMessage":null,"InvalidLengthMessage":null,"InvalidRequired":false,"InvalidRange":false,"InvalidLength":false,"Options":{"Value":["heatsystem.yearinstalledrange.2020","heatsystem.yearinstalledrange.2015","heatsystem.yearinstalledrange.2010","heatsystem.yearinstalledrange.2005","heatsystem.yearinstalledrange.2000","heatsystem.yearinstalledrange.1995","heatsystem.yearinstalledrange.1985","heatsystem.yearinstalledrange.1975","heatsystem.yearinstalledrange.1965","heatsystem.yearinstalledrange.1955","heatsystem.yearinstalledrange.before1950"],"Text":["after 2015","2011 - 2015","2006 - 2010","2001 - 2005","1996 - 2000","1990 - 1995","1980s","1970s","1960s","1950s","before 1950"]}},{"Key":"secondaryheating.count","Label":"Do you have a secondary or back-up heating system?","Tooltip":null,"Type":"decimal","OptionText":"Yes,No","OptionValue":"1,0","Min":"0","Max":"1","Answer":"","Level":"premise","RangeErrorMessage":"Please enter a value between 0 – 1.","InvalidLengthMessage":null,"InvalidRequired":false,"InvalidRange":false,"InvalidLength":false,"Options":{"Value":["1","0"],"Text":["Yes","No"]}}]}]}]}');
         app.result = json;
        fixture.detectChanges();
        const butNxt = fixture.nativeElement.querySelectorAll("button");
        expect(butNxt.length).toBe(1);
        const h1Title = fixture.nativeElement.querySelectorAll("h1");
        expect(h1Title["0"].innerText).toContain("Energy Profile");
    }));

    xit('get params.map is not a function  when more than one test-- Next button click event', () => {
        const fixture = TestBed.createComponent(Profile);
        const app = fixture.componentInstance;

        app.enableLoader = false;
        const json = JSON.parse('{"Completeness":20,"CompletenessTitle":"Youre done!","CompletenessSubtitle":"Next Step: Set a Savings Goal","CompletenessDescription":"**Save 5%:** What would I have to do?  To achieve this goal will require a few habit changes or low-cost home upgrades. You could achieve this by air drying one load of laundry a week instead of using a gas dryer, by turning your heating thermostat down by one degree all winter. Save 15%: What would I have to do? To achieve this goal you will need to make some big changes to your habit and several low-cost home upgrades, or possibly invest in a larger upgrade. You could achieve this by air drying all your laundry instead of using a gas dryer and installing more attic insulation, or by upgrading your heating system to the most efficient available.","CompletenessLabel":"Completed","NextButtonLabel":"Next","NotCompleteMessage":"Please answer this question","BackButtonLabel":null,"Title":"Energy Profile","IntroText":"","DropDownSelectText":"Select","ShowTitle":"true","ShowIntro":"","ShowSectionIntro":"","LoadingMessage":"Loading...","OverviewTabId":null,"Footer":"Sample footer text.","FooterLinkText":"Next Steps","FooterLink":"aclara.com/","ShowFooterLink":"false","ShowFooterText":"false","RequiredKey":"Required","RequiredSymbol":"*","LongProfileLinkText":"Show Advanced Profile","ShowRequiredKey":"true","ShowLongProfileLink":"true","LongProfileLink":"Page?enc=sPYGI5L/s9Mw7Kh/BWetXbMqdTEC05gj3zhW1T313FdlMy91yISWZAnZE7tRAb6ftGVwf2zLZdXMJ8w3L9u6WGXl9sWArTkjtx0FagTZsgdHvNGywN4w2mmpDwUEychdnrEJtJDxaOzg8pJ6AS98NblKSM0418c7Zqxz2B/zeu9Cqzd9T/j9JFiXW+MH1ez4qdIcHQpzbpV7bKfaRzQ/9awNLroDpdJ84fpRxRDID/qyPAsA2V7Hg3hKV4tq24qJnoMRKVK/XBZmlL2XPJGw9XHAQVRpPeQoKp8n5XqcrKxFNu4E4vf+HV+M7+tlIzcxnKQUrgeaHmEZwxYwpLAdNc6D4YJZPaJOUQoKMiXGyN1vRp4uByFOmH88Qrg4is/A2smysYdEAd24mzfDn9P+N94gw4Z+72H8hBeD9apqXfpdgWeKMVa39Hn+ILy90sCIVCx231iUmEHzXbfPTe5i/A==","ShowOverviewTab":null,"ShowOverviewTabColumnOneCount":null,"OverviewText":null,"SaveButtonLabel":null,"PageOf":null,"SavedMessage":null,"Tabs":[{"Key":null,"Name":null,"IconClass":null,"Sections":[{"Key":null,"SectionTitle":"Heating","SectionSubtitle":"The heat is on","SectionDescription":"The average home in the U.S. spends 45% of its total energy bills on heating. You can lower your heating energy use by improving the heating source (like the furnace or boiler), the distribution system (like the ducts or radiators), or the controls (like the thermostat settings).","SectionMarkdown":"","ImageUrl":"","ImageTitle":"244295881programmablethermostatneardoormedium","Questions":[{"Key":"heatsystem.fuel","Label":"What is the primary fuel used by your heating system?","Tooltip":null,"Type":"list","OptionText":"Gas,Electric,Oil,Propane,Solar,Wood,","OptionValue":"heatsystem.fuel.gas,heatsystem.fuel.electric,heatsystem.fuel.oil,heatsystem.fuel.propane,heatsystem.fuel.solar,heatsystem.fuel.wood","Min":"0","Max":"0","Answer":"","Level":"premise","RangeErrorMessage":null,"InvalidLengthMessage":null,"InvalidRequired":false,"InvalidRange":false,"InvalidLength":false,"Options":{"Value":["heatsystem.fuel.gas","heatsystem.fuel.electric","heatsystem.fuel.oil","heatsystem.fuel.propane","heatsystem.fuel.solar","heatsystem.fuel.wood"],"Text":["Gas","Electric","Oil","Propane","Solar","Wood"]}},{"Key":"heatsystem.style","Label":"What type of heating system do you have?","Tooltip":null,"Type":"list","OptionText":"Forced Air Furnace,Steam Boiler,Water Boiler,Air Source Heat Pump,Conventional Oil,Baseboard Resistance,Geothermal Heat Pump,Ground Source Heat Pump,Radiant,","OptionValue":"heatsystem.style.forcedairfurnace,heatsystem.style.steamboiler,heatsystem.style.waterboiler,heatsystem.style.airsourceheatpump,heatsystem.style.conventionaloil,heatsystem.style.baseboardresistance,heatsystem.style.geothermalheatpump,heatsystem.style.groundsourceheatpump,heatsystem.style.radiant","Min":"0","Max":"0","Answer":"","Level":"premise","RangeErrorMessage":null,"InvalidLengthMessage":null,"InvalidRequired":false,"InvalidRange":false,"InvalidLength":false,"Options":{"Value":["heatsystem.style.forcedairfurnace","heatsystem.style.steamboiler","heatsystem.style.waterboiler","heatsystem.style.airsourceheatpump","heatsystem.style.conventionaloil","heatsystem.style.baseboardresistance","heatsystem.style.geothermalheatpump","heatsystem.style.groundsourceheatpump","heatsystem.style.radiant"],"Text":["Forced Air Furnace","Steam Boiler","Water Boiler","Air Source Heat Pump","Conventional Oil","Baseboard Resistance","Geothermal Heat Pump","Ground Source Heat Pump","Radiant"]}},{"Key":"heatsystem.yearinstalledrange","Label":"When was your heating system installed?","Tooltip":null,"Type":"list","OptionText":"after 2015,2011 - 2015,2006 - 2010,2001 - 2005,1996 - 2000,1990 - 1995,1980s,1970s,1960s,1950s,before 1950,","OptionValue":"heatsystem.yearinstalledrange.2020,heatsystem.yearinstalledrange.2015,heatsystem.yearinstalledrange.2010,heatsystem.yearinstalledrange.2005,heatsystem.yearinstalledrange.2000,heatsystem.yearinstalledrange.1995,heatsystem.yearinstalledrange.1985,heatsystem.yearinstalledrange.1975,heatsystem.yearinstalledrange.1965,heatsystem.yearinstalledrange.1955,heatsystem.yearinstalledrange.before1950","Min":"0","Max":"0","Answer":"","Level":"premise","RangeErrorMessage":null,"InvalidLengthMessage":null,"InvalidRequired":false,"InvalidRange":false,"InvalidLength":false,"Options":{"Value":["heatsystem.yearinstalledrange.2020","heatsystem.yearinstalledrange.2015","heatsystem.yearinstalledrange.2010","heatsystem.yearinstalledrange.2005","heatsystem.yearinstalledrange.2000","heatsystem.yearinstalledrange.1995","heatsystem.yearinstalledrange.1985","heatsystem.yearinstalledrange.1975","heatsystem.yearinstalledrange.1965","heatsystem.yearinstalledrange.1955","heatsystem.yearinstalledrange.before1950"],"Text":["after 2015","2011 - 2015","2006 - 2010","2001 - 2005","1996 - 2000","1990 - 1995","1980s","1970s","1960s","1950s","before 1950"]}},{"Key":"secondaryheating.count","Label":"Do you have a secondary or back-up heating system?","Tooltip":null,"Type":"decimal","OptionText":"Yes,No","OptionValue":"1,0","Min":"0","Max":"1","Answer":"","Level":"premise","RangeErrorMessage":"Please enter a value between 0 – 1.","InvalidLengthMessage":null,"InvalidRequired":false,"InvalidRange":false,"InvalidLength":false,"Options":{"Value":["1","0"],"Text":["Yes","No"]}}]}]}]}');
        app.result = json;
        fixture.detectChanges();
        // trigger the 'next button' link  
        fixture.nativeElement.querySelector('h1').click();
        // process the click event
        fixture.detectChanges();
        expect(true).toBe(true);

    });

    
});