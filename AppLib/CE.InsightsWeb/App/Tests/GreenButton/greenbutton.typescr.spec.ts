﻿import { ElementRef } from "@angular/core";
import { Http } from "@angular/http";
import {GreenButton} from "../../GreenButton/greenbutton";
import {GreenButtonService} from "../../GreenButton/greenbutton.service";
import {AppService} from "../../app.service";

class MockGreenButtonService extends GreenButtonService {
    res: any;
    public unit: any = "unittest";
    getGreenButton(tabKey, params, type) {
        return this.unit;
    }
    getDownloadDetails(tabKey, params, type, startDate, endDate) {
        return JSON.parse('{"Title":"Green Button Download","SubTitle":"Download your Smart Meter Data","IntroText":"The Green Button download gives utility customers easy and secure access to their energy usage information.","FromDate":"From: ","ToDate":"To: ","DownloadAmiTitle":"","DownloadBillsTitle":"","DownloadUsage":"Download my usage for specific days","DownloadBills":"Download my bills","DownloadingMessage":"","Footer":"Sample footer","FooterLinkText":"Next steps","NoBillData":"No bill data is available.","NoUsageData":"No usage data is available.","DateError":"Please enter an end date that is after the start date.","ShowTitle":"true","ShowIntro":"true","ShowSubTitle":"false","FooterLink":"https://www.aclara.com","ShowFooterLink":"false","ShowFooterText":"false","NoBill":false,"NoAmi":true,"Response":null}');
    }
}

describe('GreenButton Typescript Tests', () => {

    it('GreenButton disableLoader().  Component main entry point.', () => {
        let http: Http;
        const appService: AppService = new AppService(http);
        const greenButtonService: GreenButtonService = new GreenButtonService(http);
        let elementRef: ElementRef;
        appService.params = [{ tabKey: "tab.usage", parameters: "YbUGYEN1OhOANtb1zjkfy/hq/cI+rQ4HQlLIgZfqZL7OdgsF8ZKqzAw3eqO0PtMqSEW2CiNa9iOob71Xc8rETBZ0b0gp5N9mczstPSIn8k41jMUEo/KUHHOvQMT89omslD8Zxrj3CSA5CIYCEkMqiRFvtTquCvGi0GQTcZ19rqt1WAisugqWAXqjCKDEgwEASuOTT5lYb5Zeaiq/uvBlY9iRFrK+c98iNuYmYNt7bo3aP/l17bzmWnKtytoTOOZiloaVfxEYRnPxVp4qvkav13vVbOe3Q6XjYo84y6fLYm6sZnkvIOzkNDxoUA9iLv1dow6BfTi2jRrGsU+uFNlW+1pVnvFIDY4nFruTPKkJ4KkO4djJI4VQDRubA9U4exxllTJJJz4BQXJ98xTr+/we3w==", errorMessage: "" }];
        appService.deepLinkVariables = [{ deepLinkWidget: "", deepLinkWidgetTab: "", deepLinkWidgetSection: "" }];
        greenButtonService.res = JSON.parse('{"Title":"Green Button Download","SubTitle":"Download your Smart Meter Data","IntroText":"The Green Button download gives utility customers easy and secure access to their energy usage information.","FromDate":"From:","ToDate":"To:","DownloadAmiTitle":"","DownloadBillsTitle":"","DownloadUsage":"Download my usage for specific days","DownloadBills":"Download my bills","DownloadingMessage":"","Footer":"Sample footer","FooterLinkText":"Next steps","NoBillData":"No bill data is available.","NoUsageData":"No usage data is available.","DateError":"Please enter an end date that is after the start date.","ShowTitle":"true","ShowIntro":"true","ShowSubTitle":"false","FooterLink":"https://www.aclara.com","ShowFooterLink":"false","ShowFooterText":"false","NoBill":false,"NoAmi":false,"Response":null}');
        greenButtonService.fromUnitTest = true;
        const gb = new GreenButton(greenButtonService, appService, elementRef);
        gb.disableLoader();
        const loader = gb.enableLoader;
        expect(loader).toBe(false);
    });

    it('GreenButton toggleView().  Change download type.', () => {
        let http: Http;
        const appService: AppService = new AppService(http);
        const greenButtonService: GreenButtonService = new GreenButtonService(http);
        let elementRef: ElementRef;
        appService.params = [{ tabKey: "tab.usage", parameters: "YbUGYEN1OhOANtb1zjkfy/hq/cI+rQ4HQlLIgZfqZL7OdgsF8ZKqzAw3eqO0PtMqSEW2CiNa9iOob71Xc8rETBZ0b0gp5N9mczstPSIn8k41jMUEo/KUHHOvQMT89omslD8Zxrj3CSA5CIYCEkMqiRFvtTquCvGi0GQTcZ19rqt1WAisugqWAXqjCKDEgwEASuOTT5lYb5Zeaiq/uvBlY9iRFrK+c98iNuYmYNt7bo3aP/l17bzmWnKtytoTOOZiloaVfxEYRnPxVp4qvkav13vVbOe3Q6XjYo84y6fLYm6sZnkvIOzkNDxoUA9iLv1dow6BfTi2jRrGsU+uFNlW+1pVnvFIDY4nFruTPKkJ4KkO4djJI4VQDRubA9U4exxllTJJJz4BQXJ98xTr+/we3w==", errorMessage: "" }];
        appService.deepLinkVariables = [{ deepLinkWidget: "", deepLinkWidgetTab: "", deepLinkWidgetSection: "" }];
        greenButtonService.res = JSON.parse('{"Title":"Green Button Download","SubTitle":"Download your Smart Meter Data","IntroText":"The Green Button download gives utility customers easy and secure access to their energy usage information.","FromDate":"From:","ToDate":"To:","DownloadAmiTitle":"","DownloadBillsTitle":"","DownloadUsage":"Download my usage for specific days","DownloadBills":"Download my bills","DownloadingMessage":"","Footer":"Sample footer","FooterLinkText":"Next steps","NoBillData":"No bill data is available.","NoUsageData":"No usage data is available.","DateError":"Please enter an end date that is after the start date.","ShowTitle":"true","ShowIntro":"true","ShowSubTitle":"false","FooterLink":"https://www.aclara.com","ShowFooterLink":"false","ShowFooterText":"false","NoBill":false,"NoAmi":false,"Response":null}');
        const gb = new GreenButton(greenButtonService, appService, elementRef);
        gb.toggleView('ami');
        expect(gb.downloadType).toBe('ami');
    });

    it('GreenButton validateDates().  End date should be after start date', () => {
        let http: Http;
        const appService: AppService = new AppService(http);
        const greenButtonService: GreenButtonService = new GreenButtonService(http);
        let elementRef: ElementRef;
        appService.params = [{ tabKey: "tab.usage", parameters: "YbUGYEN1OhOANtb1zjkfy/hq/cI+rQ4HQlLIgZfqZL7OdgsF8ZKqzAw3eqO0PtMqSEW2CiNa9iOob71Xc8rETBZ0b0gp5N9mczstPSIn8k41jMUEo/KUHHOvQMT89omslD8Zxrj3CSA5CIYCEkMqiRFvtTquCvGi0GQTcZ19rqt1WAisugqWAXqjCKDEgwEASuOTT5lYb5Zeaiq/uvBlY9iRFrK+c98iNuYmYNt7bo3aP/l17bzmWnKtytoTOOZiloaVfxEYRnPxVp4qvkav13vVbOe3Q6XjYo84y6fLYm6sZnkvIOzkNDxoUA9iLv1dow6BfTi2jRrGsU+uFNlW+1pVnvFIDY4nFruTPKkJ4KkO4djJI4VQDRubA9U4exxllTJJJz4BQXJ98xTr+/we3w==", errorMessage: "" }];
        appService.deepLinkVariables = [{ deepLinkWidget: "", deepLinkWidgetTab: "", deepLinkWidgetSection: "" }];
        greenButtonService.res = JSON.parse('{"Title":"Green Button Download","SubTitle":"Download your Smart Meter Data","IntroText":"The Green Button download gives utility customers easy and secure access to their energy usage information.","FromDate":"From:","ToDate":"To:","DownloadAmiTitle":"","DownloadBillsTitle":"","DownloadUsage":"Download my usage for specific days","DownloadBills":"Download my bills","DownloadingMessage":"","Footer":"Sample footer","FooterLinkText":"Next steps","NoBillData":"No bill data is available.","NoUsageData":"No usage data is available.","DateError":"Please enter an end date that is after the start date.","ShowTitle":"true","ShowIntro":"true","ShowSubTitle":"false","FooterLink":"https://www.aclara.com","ShowFooterLink":"false","ShowFooterText":"false","NoBill":false,"NoAmi":false,"Response":null}');
        const gb = new GreenButton(greenButtonService, appService, elementRef);
        gb.startDate = new Date('1/1/2010');
        gb.endDate = new Date('1/1/2011');
        const res = gb.validateDates();
        expect(res).toBe(false);
    });

    it('GreenButton validateDates().  Invalid date range', () => {
        let http: Http;
        const appService: AppService = new AppService(http);
        const greenButtonService: GreenButtonService = new GreenButtonService(http);
        let elementRef: ElementRef;
        appService.params = [{ tabKey: "tab.usage", parameters: "YbUGYEN1OhOANtb1zjkfy/hq/cI+rQ4HQlLIgZfqZL7OdgsF8ZKqzAw3eqO0PtMqSEW2CiNa9iOob71Xc8rETBZ0b0gp5N9mczstPSIn8k41jMUEo/KUHHOvQMT89omslD8Zxrj3CSA5CIYCEkMqiRFvtTquCvGi0GQTcZ19rqt1WAisugqWAXqjCKDEgwEASuOTT5lYb5Zeaiq/uvBlY9iRFrK+c98iNuYmYNt7bo3aP/l17bzmWnKtytoTOOZiloaVfxEYRnPxVp4qvkav13vVbOe3Q6XjYo84y6fLYm6sZnkvIOzkNDxoUA9iLv1dow6BfTi2jRrGsU+uFNlW+1pVnvFIDY4nFruTPKkJ4KkO4djJI4VQDRubA9U4exxllTJJJz4BQXJ98xTr+/we3w==", errorMessage: "" }];
        appService.deepLinkVariables = [{ deepLinkWidget: "", deepLinkWidgetTab: "", deepLinkWidgetSection: "" }];
        greenButtonService.res = JSON.parse('{"Title":"Green Button Download","SubTitle":"Download your Smart Meter Data","IntroText":"The Green Button download gives utility customers easy and secure access to their energy usage information.","FromDate":"From:","ToDate":"To:","DownloadAmiTitle":"","DownloadBillsTitle":"","DownloadUsage":"Download my usage for specific days","DownloadBills":"Download my bills","DownloadingMessage":"","Footer":"Sample footer","FooterLinkText":"Next steps","NoBillData":"No bill data is available.","NoUsageData":"No usage data is available.","DateError":"Please enter an end date that is after the start date.","ShowTitle":"true","ShowIntro":"true","ShowSubTitle":"false","FooterLink":"https://www.aclara.com","ShowFooterLink":"false","ShowFooterText":"false","NoBill":false,"NoAmi":false,"Response":null}');
        const gb = new GreenButton(greenButtonService, appService, elementRef);
        gb.startDate = new Date('1/1/2011');
        gb.endDate = new Date('1/1/2010');
        const res = gb.validateDates();
        expect(res).toBe(true);
    });

    it('GreenButton download().  Download No AMI Data.', () => {
        let http: Http;
        const appService: AppService = new AppService(http);
        const greenButtonService: MockGreenButtonService = new MockGreenButtonService(http);
        let elementRef: ElementRef;
        appService.params = [{ tabKey: "tab.usage", parameters: "YbUGYEN1OhOANtb1zjkfy/hq/cI+rQ4HQlLIgZfqZL7OdgsF8ZKqzAw3eqO0PtMqSEW2CiNa9iOob71Xc8rETBZ0b0gp5N9mczstPSIn8k41jMUEo/KUHHOvQMT89omslD8Zxrj3CSA5CIYCEkMqiRFvtTquCvGi0GQTcZ19rqt1WAisugqWAXqjCKDEgwEASuOTT5lYb5Zeaiq/uvBlY9iRFrK+c98iNuYmYNt7bo3aP/l17bzmWnKtytoTOOZiloaVfxEYRnPxVp4qvkav13vVbOe3Q6XjYo84y6fLYm6sZnkvIOzkNDxoUA9iLv1dow6BfTi2jRrGsU+uFNlW+1pVnvFIDY4nFruTPKkJ4KkO4djJI4VQDRubA9U4exxllTJJJz4BQXJ98xTr+/we3w==", errorMessage: "" }];
        appService.deepLinkVariables = [{ deepLinkWidget: "", deepLinkWidgetTab: "", deepLinkWidgetSection: "" }];
        greenButtonService.res = JSON.parse('{"Title":"Green Button Download","SubTitle":"Download your Smart Meter Data","IntroText":"The Green Button download gives utility customers easy and secure access to their energy usage information.","FromDate":"From:","ToDate":"To:","DownloadAmiTitle":"","DownloadBillsTitle":"","DownloadUsage":"Download my usage for specific days","DownloadBills":"Download my bills","DownloadingMessage":"","Footer":"Sample footer","FooterLinkText":"Next steps","NoBillData":"No bill data is available.","NoUsageData":"No usage data is available.","DateError":"Please enter an end date that is after the start date.","ShowTitle":"true","ShowIntro":"true","ShowSubTitle":"false","FooterLink":"https://www.aclara.com","ShowFooterLink":"false","ShowFooterText":"false","NoBill":false,"NoAmi":false,"Response":null}');
        const gb = new GreenButton(greenButtonService, appService, elementRef);
        gb.download();
        expect(gb.result.NoAmi).toBe(true);
    });
    it('GreenButton checkDownloadType(). Check the hidebilldownload and hideusagedownload configuration.', () => {
        let http: Http;
        const appService: AppService = new AppService(http);
        const greenButtonService: MockGreenButtonService = new MockGreenButtonService(http);
        let elementRef: ElementRef;
        appService.params = [{ tabKey: "tab.usage", parameters: "YbUGYEN1OhOANtb1zjkfy/hq/cI+rQ4HQlLIgZfqZL7OdgsF8ZKqzAw3eqO0PtMqSEW2CiNa9iOob71Xc8rETBZ0b0gp5N9mczstPSIn8k41jMUEo/KUHHOvQMT89omslD8Zxrj3CSA5CIYCEkMqiRFvtTquCvGi0GQTcZ19rqt1WAisugqWAXqjCKDEgwEASuOTT5lYb5Zeaiq/uvBlY9iRFrK+c98iNuYmYNt7bo3aP/l17bzmWnKtytoTOOZiloaVfxEYRnPxVp4qvkav13vVbOe3Q6XjYo84y6fLYm6sZnkvIOzkNDxoUA9iLv1dow6BfTi2jRrGsU+uFNlW+1pVnvFIDY4nFruTPKkJ4KkO4djJI4VQDRubA9U4exxllTJJJz4BQXJ98xTr+/we3w==", errorMessage: "" }];
        appService.deepLinkVariables = [{ deepLinkWidget: "", deepLinkWidgetTab: "", deepLinkWidgetSection: "" }];
        greenButtonService.res = JSON.parse('{"Title":"Green Button Download","SubTitle":"Download your Smart Meter Data","IntroText":"The Green Button download gives utility customers easy and secure access to their energy usage information.","FromDate":"From:","ToDate":"To:","DownloadAmiTitle":"","DownloadBillsTitle":"","DownloadUsage":"Download my usage for specific days","DownloadBills":"Download my bills","DownloadingMessage":"","Footer":"Sample footer","FooterLinkText":"Next steps","NoBillData":"No bill data is available.","NoUsageData":"No usage data is available.","DateError":"Please enter an end date that is after the start date.","ShowTitle":"true","ShowIntro":"true","ShowSubTitle":"false","FooterLink":"https://www.aclara.com","ShowFooterLink":"false","ShowFooterText":"false","NoBill":false,"NoAmi":false,"HideUsageDownload":false,"HideBillDownload":false,"Response":null}');
        const gb = new GreenButton(greenButtonService, appService, elementRef);
        expect(gb.checkDownloadType()).toBe('ami');
        gb.result = JSON.parse('{"Title":"Green Button Download","SubTitle":"Download your Smart Meter Data","IntroText":"The Green Button download gives utility customers easy and secure access to their energy usage information.","FromDate":"From:","ToDate":"To:","DownloadAmiTitle":"","DownloadBillsTitle":"","DownloadUsage":"Download my usage for specific days","DownloadBills":"Download my bills","DownloadingMessage":"","Footer":"Sample footer","FooterLinkText":"Next steps","NoBillData":"No bill data is available.","NoUsageData":"No usage data is available.","DateError":"Please enter an end date that is after the start date.","ShowTitle":"true","ShowIntro":"true","ShowSubTitle":"false","FooterLink":"https://www.aclara.com","ShowFooterLink":"false","ShowFooterText":"false","NoBill":false,"NoAmi":false,"HideUsageDownload":true,"HideBillDownload":false,"Response":null}');
        expect(gb.checkDownloadType()).toBe('bill');
    });

});