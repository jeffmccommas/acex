"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var http_1 = require("@angular/http");
var testing_2 = require("@angular/http/testing");
var testing_3 = require("@angular/platform-browser-dynamic/testing");
var greenbutton_service_1 = require("../../GreenButton/greenbutton.service");
describe('TestGreenButtonService -- Mock', function () {
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.resetTestEnvironment();
        testing_1.TestBed.initTestEnvironment(testing_3.BrowserDynamicTestingModule, testing_3.platformBrowserDynamicTesting());
        testing_1.TestBed.configureTestingModule({
            imports: [http_1.HttpModule],
            providers: [
                greenbutton_service_1.GreenButtonService,
                { provide: http_1.XHRBackend, useClass: testing_2.MockBackend }
            ]
        }).compileComponents();
        ;
    }));
    it('can instantiate service when inject service', testing_1.inject([greenbutton_service_1.GreenButtonService], function (service) {
        expect(service instanceof greenbutton_service_1.GreenButtonService).toBe(true);
    }));
    it('can provide the mockBackend as XHRBackend', testing_1.inject([http_1.XHRBackend], function (backend) {
        expect(backend).not.toBeNull('backend should be provided');
    }));
    var backend;
    var service;
    var response;
    beforeEach(testing_1.inject([http_1.Http, http_1.XHRBackend], function (http, be) {
        backend = be;
        service = new greenbutton_service_1.GreenButtonService(http);
        var options = new http_1.ResponseOptions({
            body: [
                {
                    id: "QA87HomeProf",
                    contentRendered: "<p><b>Test</b></p>",
                    contentMarkdown: "*Hi there*"
                }
            ]
        });
        response = new http_1.Response(options);
    }));
    it('should get green button initial data for form', testing_1.async(testing_1.inject([], function () {
        backend.connections.subscribe(function (c) { return c.mockRespond(response); });
        service.getGreenButton('', '', '').subscribe(function (blogs) {
            expect(blogs.length).toBe(1);
            expect(blogs[0].id).toBe("QA87HomeProf");
            expect(blogs[0].contentRendered).toBe("<p><b>Test</b></p>");
        });
    })));
    it('should get green button download details', testing_1.async(testing_1.inject([], function () {
        backend.connections.subscribe(function (c) { return c.mockRespond(response); });
        service.getDownloadDetails('', '', '', '', '').subscribe(function (blogs) {
            expect(blogs.length).toBe(1);
            expect(blogs[0].id).toBe("QA87HomeProf");
            expect(blogs[0].contentRendered).toBe("<p><b>Test</b></p>");
        });
    })));
});
//# sourceMappingURL=greenbutton.svc.spec.js.map