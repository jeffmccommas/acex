"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var greenbutton_1 = require("../../GreenButton/greenbutton");
var greenbutton_service_1 = require("../../GreenButton/greenbutton.service");
var app_service_1 = require("../../app.service");
var MockGreenButtonService = (function (_super) {
    __extends(MockGreenButtonService, _super);
    function MockGreenButtonService() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.unit = "unittest";
        return _this;
    }
    MockGreenButtonService.prototype.getGreenButton = function (tabKey, params, type) {
        return this.unit;
    };
    MockGreenButtonService.prototype.getDownloadDetails = function (tabKey, params, type, startDate, endDate) {
        return JSON.parse('{"Title":"Green Button Download","SubTitle":"Download your Smart Meter Data","IntroText":"The Green Button download gives utility customers easy and secure access to their energy usage information.","FromDate":"From: ","ToDate":"To: ","DownloadAmiTitle":"","DownloadBillsTitle":"","DownloadUsage":"Download my usage for specific days","DownloadBills":"Download my bills","DownloadingMessage":"","Footer":"Sample footer","FooterLinkText":"Next steps","NoBillData":"No bill data is available.","NoUsageData":"No usage data is available.","DateError":"Please enter an end date that is after the start date.","ShowTitle":"true","ShowIntro":"true","ShowSubTitle":"false","FooterLink":"https://www.aclara.com","ShowFooterLink":"false","ShowFooterText":"false","NoBill":false,"NoAmi":true,"Response":null}');
    };
    return MockGreenButtonService;
}(greenbutton_service_1.GreenButtonService));
describe('GreenButton Typescript Tests', function () {
    it('GreenButton disableLoader().  Component main entry point.', function () {
        var http;
        var appService = new app_service_1.AppService(http);
        var greenButtonService = new greenbutton_service_1.GreenButtonService(http);
        var elementRef;
        appService.params = [{ tabKey: "tab.usage", parameters: "YbUGYEN1OhOANtb1zjkfy/hq/cI+rQ4HQlLIgZfqZL7OdgsF8ZKqzAw3eqO0PtMqSEW2CiNa9iOob71Xc8rETBZ0b0gp5N9mczstPSIn8k41jMUEo/KUHHOvQMT89omslD8Zxrj3CSA5CIYCEkMqiRFvtTquCvGi0GQTcZ19rqt1WAisugqWAXqjCKDEgwEASuOTT5lYb5Zeaiq/uvBlY9iRFrK+c98iNuYmYNt7bo3aP/l17bzmWnKtytoTOOZiloaVfxEYRnPxVp4qvkav13vVbOe3Q6XjYo84y6fLYm6sZnkvIOzkNDxoUA9iLv1dow6BfTi2jRrGsU+uFNlW+1pVnvFIDY4nFruTPKkJ4KkO4djJI4VQDRubA9U4exxllTJJJz4BQXJ98xTr+/we3w==", errorMessage: "" }];
        appService.deepLinkVariables = [{ deepLinkWidget: "", deepLinkWidgetTab: "", deepLinkWidgetSection: "" }];
        greenButtonService.res = JSON.parse('{"Title":"Green Button Download","SubTitle":"Download your Smart Meter Data","IntroText":"The Green Button download gives utility customers easy and secure access to their energy usage information.","FromDate":"From:","ToDate":"To:","DownloadAmiTitle":"","DownloadBillsTitle":"","DownloadUsage":"Download my usage for specific days","DownloadBills":"Download my bills","DownloadingMessage":"","Footer":"Sample footer","FooterLinkText":"Next steps","NoBillData":"No bill data is available.","NoUsageData":"No usage data is available.","DateError":"Please enter an end date that is after the start date.","ShowTitle":"true","ShowIntro":"true","ShowSubTitle":"false","FooterLink":"https://www.aclara.com","ShowFooterLink":"false","ShowFooterText":"false","NoBill":false,"NoAmi":false,"Response":null}');
        greenButtonService.fromUnitTest = true;
        var gb = new greenbutton_1.GreenButton(greenButtonService, appService, elementRef);
        gb.disableLoader();
        var loader = gb.enableLoader;
        expect(loader).toBe(false);
    });
    it('GreenButton toggleView().  Change download type.', function () {
        var http;
        var appService = new app_service_1.AppService(http);
        var greenButtonService = new greenbutton_service_1.GreenButtonService(http);
        var elementRef;
        appService.params = [{ tabKey: "tab.usage", parameters: "YbUGYEN1OhOANtb1zjkfy/hq/cI+rQ4HQlLIgZfqZL7OdgsF8ZKqzAw3eqO0PtMqSEW2CiNa9iOob71Xc8rETBZ0b0gp5N9mczstPSIn8k41jMUEo/KUHHOvQMT89omslD8Zxrj3CSA5CIYCEkMqiRFvtTquCvGi0GQTcZ19rqt1WAisugqWAXqjCKDEgwEASuOTT5lYb5Zeaiq/uvBlY9iRFrK+c98iNuYmYNt7bo3aP/l17bzmWnKtytoTOOZiloaVfxEYRnPxVp4qvkav13vVbOe3Q6XjYo84y6fLYm6sZnkvIOzkNDxoUA9iLv1dow6BfTi2jRrGsU+uFNlW+1pVnvFIDY4nFruTPKkJ4KkO4djJI4VQDRubA9U4exxllTJJJz4BQXJ98xTr+/we3w==", errorMessage: "" }];
        appService.deepLinkVariables = [{ deepLinkWidget: "", deepLinkWidgetTab: "", deepLinkWidgetSection: "" }];
        greenButtonService.res = JSON.parse('{"Title":"Green Button Download","SubTitle":"Download your Smart Meter Data","IntroText":"The Green Button download gives utility customers easy and secure access to their energy usage information.","FromDate":"From:","ToDate":"To:","DownloadAmiTitle":"","DownloadBillsTitle":"","DownloadUsage":"Download my usage for specific days","DownloadBills":"Download my bills","DownloadingMessage":"","Footer":"Sample footer","FooterLinkText":"Next steps","NoBillData":"No bill data is available.","NoUsageData":"No usage data is available.","DateError":"Please enter an end date that is after the start date.","ShowTitle":"true","ShowIntro":"true","ShowSubTitle":"false","FooterLink":"https://www.aclara.com","ShowFooterLink":"false","ShowFooterText":"false","NoBill":false,"NoAmi":false,"Response":null}');
        var gb = new greenbutton_1.GreenButton(greenButtonService, appService, elementRef);
        gb.toggleView('ami');
        expect(gb.downloadType).toBe('ami');
    });
    it('GreenButton validateDates().  End date should be after start date', function () {
        var http;
        var appService = new app_service_1.AppService(http);
        var greenButtonService = new greenbutton_service_1.GreenButtonService(http);
        var elementRef;
        appService.params = [{ tabKey: "tab.usage", parameters: "YbUGYEN1OhOANtb1zjkfy/hq/cI+rQ4HQlLIgZfqZL7OdgsF8ZKqzAw3eqO0PtMqSEW2CiNa9iOob71Xc8rETBZ0b0gp5N9mczstPSIn8k41jMUEo/KUHHOvQMT89omslD8Zxrj3CSA5CIYCEkMqiRFvtTquCvGi0GQTcZ19rqt1WAisugqWAXqjCKDEgwEASuOTT5lYb5Zeaiq/uvBlY9iRFrK+c98iNuYmYNt7bo3aP/l17bzmWnKtytoTOOZiloaVfxEYRnPxVp4qvkav13vVbOe3Q6XjYo84y6fLYm6sZnkvIOzkNDxoUA9iLv1dow6BfTi2jRrGsU+uFNlW+1pVnvFIDY4nFruTPKkJ4KkO4djJI4VQDRubA9U4exxllTJJJz4BQXJ98xTr+/we3w==", errorMessage: "" }];
        appService.deepLinkVariables = [{ deepLinkWidget: "", deepLinkWidgetTab: "", deepLinkWidgetSection: "" }];
        greenButtonService.res = JSON.parse('{"Title":"Green Button Download","SubTitle":"Download your Smart Meter Data","IntroText":"The Green Button download gives utility customers easy and secure access to their energy usage information.","FromDate":"From:","ToDate":"To:","DownloadAmiTitle":"","DownloadBillsTitle":"","DownloadUsage":"Download my usage for specific days","DownloadBills":"Download my bills","DownloadingMessage":"","Footer":"Sample footer","FooterLinkText":"Next steps","NoBillData":"No bill data is available.","NoUsageData":"No usage data is available.","DateError":"Please enter an end date that is after the start date.","ShowTitle":"true","ShowIntro":"true","ShowSubTitle":"false","FooterLink":"https://www.aclara.com","ShowFooterLink":"false","ShowFooterText":"false","NoBill":false,"NoAmi":false,"Response":null}');
        var gb = new greenbutton_1.GreenButton(greenButtonService, appService, elementRef);
        gb.startDate = new Date('1/1/2010');
        gb.endDate = new Date('1/1/2011');
        var res = gb.validateDates();
        expect(res).toBe(false);
    });
    it('GreenButton validateDates().  Invalid date range', function () {
        var http;
        var appService = new app_service_1.AppService(http);
        var greenButtonService = new greenbutton_service_1.GreenButtonService(http);
        var elementRef;
        appService.params = [{ tabKey: "tab.usage", parameters: "YbUGYEN1OhOANtb1zjkfy/hq/cI+rQ4HQlLIgZfqZL7OdgsF8ZKqzAw3eqO0PtMqSEW2CiNa9iOob71Xc8rETBZ0b0gp5N9mczstPSIn8k41jMUEo/KUHHOvQMT89omslD8Zxrj3CSA5CIYCEkMqiRFvtTquCvGi0GQTcZ19rqt1WAisugqWAXqjCKDEgwEASuOTT5lYb5Zeaiq/uvBlY9iRFrK+c98iNuYmYNt7bo3aP/l17bzmWnKtytoTOOZiloaVfxEYRnPxVp4qvkav13vVbOe3Q6XjYo84y6fLYm6sZnkvIOzkNDxoUA9iLv1dow6BfTi2jRrGsU+uFNlW+1pVnvFIDY4nFruTPKkJ4KkO4djJI4VQDRubA9U4exxllTJJJz4BQXJ98xTr+/we3w==", errorMessage: "" }];
        appService.deepLinkVariables = [{ deepLinkWidget: "", deepLinkWidgetTab: "", deepLinkWidgetSection: "" }];
        greenButtonService.res = JSON.parse('{"Title":"Green Button Download","SubTitle":"Download your Smart Meter Data","IntroText":"The Green Button download gives utility customers easy and secure access to their energy usage information.","FromDate":"From:","ToDate":"To:","DownloadAmiTitle":"","DownloadBillsTitle":"","DownloadUsage":"Download my usage for specific days","DownloadBills":"Download my bills","DownloadingMessage":"","Footer":"Sample footer","FooterLinkText":"Next steps","NoBillData":"No bill data is available.","NoUsageData":"No usage data is available.","DateError":"Please enter an end date that is after the start date.","ShowTitle":"true","ShowIntro":"true","ShowSubTitle":"false","FooterLink":"https://www.aclara.com","ShowFooterLink":"false","ShowFooterText":"false","NoBill":false,"NoAmi":false,"Response":null}');
        var gb = new greenbutton_1.GreenButton(greenButtonService, appService, elementRef);
        gb.startDate = new Date('1/1/2011');
        gb.endDate = new Date('1/1/2010');
        var res = gb.validateDates();
        expect(res).toBe(true);
    });
    it('GreenButton download().  Download No AMI Data.', function () {
        var http;
        var appService = new app_service_1.AppService(http);
        var greenButtonService = new MockGreenButtonService(http);
        var elementRef;
        appService.params = [{ tabKey: "tab.usage", parameters: "YbUGYEN1OhOANtb1zjkfy/hq/cI+rQ4HQlLIgZfqZL7OdgsF8ZKqzAw3eqO0PtMqSEW2CiNa9iOob71Xc8rETBZ0b0gp5N9mczstPSIn8k41jMUEo/KUHHOvQMT89omslD8Zxrj3CSA5CIYCEkMqiRFvtTquCvGi0GQTcZ19rqt1WAisugqWAXqjCKDEgwEASuOTT5lYb5Zeaiq/uvBlY9iRFrK+c98iNuYmYNt7bo3aP/l17bzmWnKtytoTOOZiloaVfxEYRnPxVp4qvkav13vVbOe3Q6XjYo84y6fLYm6sZnkvIOzkNDxoUA9iLv1dow6BfTi2jRrGsU+uFNlW+1pVnvFIDY4nFruTPKkJ4KkO4djJI4VQDRubA9U4exxllTJJJz4BQXJ98xTr+/we3w==", errorMessage: "" }];
        appService.deepLinkVariables = [{ deepLinkWidget: "", deepLinkWidgetTab: "", deepLinkWidgetSection: "" }];
        greenButtonService.res = JSON.parse('{"Title":"Green Button Download","SubTitle":"Download your Smart Meter Data","IntroText":"The Green Button download gives utility customers easy and secure access to their energy usage information.","FromDate":"From:","ToDate":"To:","DownloadAmiTitle":"","DownloadBillsTitle":"","DownloadUsage":"Download my usage for specific days","DownloadBills":"Download my bills","DownloadingMessage":"","Footer":"Sample footer","FooterLinkText":"Next steps","NoBillData":"No bill data is available.","NoUsageData":"No usage data is available.","DateError":"Please enter an end date that is after the start date.","ShowTitle":"true","ShowIntro":"true","ShowSubTitle":"false","FooterLink":"https://www.aclara.com","ShowFooterLink":"false","ShowFooterText":"false","NoBill":false,"NoAmi":false,"Response":null}');
        var gb = new greenbutton_1.GreenButton(greenButtonService, appService, elementRef);
        gb.download();
        expect(gb.result.NoAmi).toBe(true);
    });
    it('GreenButton checkDownloadType(). Check the hidebilldownload and hideusagedownload configuration.', function () {
        var http;
        var appService = new app_service_1.AppService(http);
        var greenButtonService = new MockGreenButtonService(http);
        var elementRef;
        appService.params = [{ tabKey: "tab.usage", parameters: "YbUGYEN1OhOANtb1zjkfy/hq/cI+rQ4HQlLIgZfqZL7OdgsF8ZKqzAw3eqO0PtMqSEW2CiNa9iOob71Xc8rETBZ0b0gp5N9mczstPSIn8k41jMUEo/KUHHOvQMT89omslD8Zxrj3CSA5CIYCEkMqiRFvtTquCvGi0GQTcZ19rqt1WAisugqWAXqjCKDEgwEASuOTT5lYb5Zeaiq/uvBlY9iRFrK+c98iNuYmYNt7bo3aP/l17bzmWnKtytoTOOZiloaVfxEYRnPxVp4qvkav13vVbOe3Q6XjYo84y6fLYm6sZnkvIOzkNDxoUA9iLv1dow6BfTi2jRrGsU+uFNlW+1pVnvFIDY4nFruTPKkJ4KkO4djJI4VQDRubA9U4exxllTJJJz4BQXJ98xTr+/we3w==", errorMessage: "" }];
        appService.deepLinkVariables = [{ deepLinkWidget: "", deepLinkWidgetTab: "", deepLinkWidgetSection: "" }];
        greenButtonService.res = JSON.parse('{"Title":"Green Button Download","SubTitle":"Download your Smart Meter Data","IntroText":"The Green Button download gives utility customers easy and secure access to their energy usage information.","FromDate":"From:","ToDate":"To:","DownloadAmiTitle":"","DownloadBillsTitle":"","DownloadUsage":"Download my usage for specific days","DownloadBills":"Download my bills","DownloadingMessage":"","Footer":"Sample footer","FooterLinkText":"Next steps","NoBillData":"No bill data is available.","NoUsageData":"No usage data is available.","DateError":"Please enter an end date that is after the start date.","ShowTitle":"true","ShowIntro":"true","ShowSubTitle":"false","FooterLink":"https://www.aclara.com","ShowFooterLink":"false","ShowFooterText":"false","NoBill":false,"NoAmi":false,"HideUsageDownload":false,"HideBillDownload":false,"Response":null}');
        var gb = new greenbutton_1.GreenButton(greenButtonService, appService, elementRef);
        expect(gb.checkDownloadType()).toBe('ami');
        gb.result = JSON.parse('{"Title":"Green Button Download","SubTitle":"Download your Smart Meter Data","IntroText":"The Green Button download gives utility customers easy and secure access to their energy usage information.","FromDate":"From:","ToDate":"To:","DownloadAmiTitle":"","DownloadBillsTitle":"","DownloadUsage":"Download my usage for specific days","DownloadBills":"Download my bills","DownloadingMessage":"","Footer":"Sample footer","FooterLinkText":"Next steps","NoBillData":"No bill data is available.","NoUsageData":"No usage data is available.","DateError":"Please enter an end date that is after the start date.","ShowTitle":"true","ShowIntro":"true","ShowSubTitle":"false","FooterLink":"https://www.aclara.com","ShowFooterLink":"false","ShowFooterText":"false","NoBill":false,"NoAmi":false,"HideUsageDownload":true,"HideBillDownload":false,"Response":null}');
        expect(gb.checkDownloadType()).toBe('bill');
    });
});
//# sourceMappingURL=greenbutton.typescr.spec.js.map