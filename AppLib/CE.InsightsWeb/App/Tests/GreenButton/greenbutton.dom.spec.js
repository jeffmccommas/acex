"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var platform_browser_1 = require("@angular/platform-browser");
var http_1 = require("@angular/http");
var testing_2 = require("@angular/platform-browser-dynamic/testing");
var greenbutton_1 = require("../../GreenButton/greenbutton");
var greenbutton_service_1 = require("../../GreenButton/greenbutton.service");
var app_service_1 = require("../../app.service");
var loader_1 = require("../../Loader/loader");
var ngx_bootstrap_1 = require("ngx-bootstrap");
var MockAppService = (function (_super) {
    __extends(MockAppService, _super);
    function MockAppService() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.params = [{ tabKey: "tab.usage", parameters: "WWJVR1lFTjFPaE9BTnRiMXpqa2Z5L2hxL2NJK3JRNEhRbExJZ1pmcVpMN09kZ3NGOFpLcXpBdzNlcU8wUHRNcVNFVzJDaU5hOWlPb2I3MVhjOHJFVEJaMGIwZ3A1TjltY3pzdFBTSW44azQxak1VRW8vS1VISE92UU1UODlvbXNsRDhaeHJqM0NTQTVDSVlDRWtNcWlSRnZ0VHF1Q3ZHaTBHUVRjWjE5cnF0MVdBaXN1Z3FXQVhxakNLREVnd0VBdFR4S3RBQURPUzFHS0ZBT2M4Ty92QW42UEswSWNmUk1tUTU4MElLRVZoQW5JRTJVSTJFOEIzVGpCUjQyNk9idCtOTTRwcFU0NmhjcGJ3WTBJVWlsMWlpNGN4V0ovYXZWM0ExSjZrcTRjbWtHNmE5cElJSXVPb3NRb0QwUEMyWm90SzlQU3ZMdyt3Y2xDNGZwbnBsbzlqSkVBZzVHbmNmeUVwMTcrYnozeTk4c1pnZ0xENEpMN08vNmJOVHVDSW03UVlpYkt0S2Z2eGVFVzVvNlU0Y1hGbGI0cU9Ebmw0QWx5TUlBdHBFemFvYnloYnhXZzVTZWZ5eHExRnFXMWdtMzZmS05mRlZPQ3RrelhxOVcvcjN0OVRUU2VjQmZrNHBTVkhpT2dwTCtpR0ZBZ1Q3dytIaTFzVlYybVRTWFp4MzdWek52dGNuRHBSc0VMaGNPL1FtSVNnPT0=&type=bill&startDate=7/29/2016&endDate=7/29/2016", errorMessage: "" }];
        return _this;
    }
    MockAppService.prototype.getParams = function () {
        return this.params;
    };
    MockAppService.prototype.getDeepLinkVariables = function () {
        return this.deepLinkVariables;
    };
    return MockAppService;
}(app_service_1.AppService));
var MockGreenButtonService = (function (_super) {
    __extends(MockGreenButtonService, _super);
    function MockGreenButtonService() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.unit = "unittest";
        return _this;
    }
    MockGreenButtonService.prototype.getGreenButton = function () {
        return this.unit;
    };
    MockGreenButtonService.prototype.getDownloadDetails = function () {
        return JSON.parse('{ "BannerText": "Your usage data to save, send or print", "FromDate": "From:", "ToDate": "To:", "DownloadAmiTitle": "Please select the date range for which you would like to download your usage.", "DownloadBillsTitle": "Download your bills total usage and total cost in Green Button format.", "DownloadUsage": "Download my usage for specific days", "DownloadBills": "Download my bills", "DownloadingMessage": "Downloading...", "NoBillData": "No bill data is available.", "NoUsageData": "No usage data is available.", "DateError": "Please enter an end date that is after the start date.", "ExpandLabel": "expand", "CollapseLabel": "collapse", "NoBill": false, "NoAmi": false, "HideBillDownload": false, "HideUsageDownload": false, "Response": null, "Title": "Green Button Download", "IntroText": "The green button download generates a zip file containing your billing or AMI data XML files. The zip file will also contain a .xslt stylesheet file that will format the XML file when you view it on a browser. After you click the green button, save the zip file to a location on your computer that you will remember. Then go to the location where you saved the file and extract the files from the zip file. Next, open the extracted billing or AMI xml file in your browser.", "SubTitle": "Download your Smart Meter Data", "ShowTitle": "true", "ShowIntro": "true", "ShowSubTitle": "false", "Footer": "Note that the Google Chrome browser has a security policy that prevents the browser from using the stylesheet when it displaying the XML file, so we recommend using another browser when opening the XML file. \nMost mobile browsers require a third-party app to open zip files, so we recommend using your desktop computer for green button download.\n", "FooterLink": "https:///www.aclara.com", "FooterLinkText": "Next steps", "FooterLinkType": null, "ShowFooterText": "true", "ShowFooterLink": "false" }');
    };
    return MockGreenButtonService;
}(greenbutton_service_1.GreenButtonService));
describe('GreenButton DOM Tests', function () {
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.resetTestEnvironment();
        testing_1.TestBed.initTestEnvironment(testing_2.BrowserDynamicTestingModule, testing_2.platformBrowserDynamicTesting());
        core_1.ElementRef;
        testing_1.TestBed.configureTestingModule({
            imports: [platform_browser_1.BrowserModule, forms_1.FormsModule, http_1.HttpModule, ngx_bootstrap_1.DatepickerModule.forRoot()],
            declarations: [greenbutton_1.GreenButton, loader_1.Loader]
        })
            .overrideComponent(greenbutton_1.GreenButton, {
            remove: { providers: [app_service_1.AppService, greenbutton_service_1.GreenButtonService] },
            add: {
                providers: [
                    { provide: app_service_1.AppService, useClass: MockAppService },
                    { provide: greenbutton_service_1.GreenButtonService, useClass: MockGreenButtonService }
                ]
            }
        }).compileComponents();
    }));
    it('Should show icon toggle down button with text expand', testing_1.async(function () {
        var fixture = testing_1.TestBed.createComponent(greenbutton_1.GreenButton);
        var app = fixture.componentInstance;
        var element = fixture.nativeElement;
        app.enableLoader = false;
        var json = JSON.parse('{"BannerText":"Your usage data to save, send or print", "DownloadAmiTitle":"Please select the date range for which you would like to download your usage.", "DownloadBillsTitle":"Download your bills total usage and total cost in Green Button format.", "DownloadUsage":"Download my usage for specific days", "DownloadBills":"Download my bills", "DownloadingMessage":"Downloading...", "NoBillData":"No bill data is available.", "NoUsageData":"No usage data is available.", "DateError":"Please enter an end date that is after the start date.", "ExpandLabel":"expand", "CollapseLabel":"collapse", "NoBill":false, "NoAmi":false, "HideBillDownload":false, "HideUsageDownload":false, "Response":null, "Title":"Green Button Download", "IntroText":"The green button download generates a zip file containing your billing or AMI data XML files. The zip file will also contain a .xslt stylesheet file that will format the XML file when you view it on a browser. After you click the green button, save the zip file to a location on your computer that you will remember. Then go to the location where you saved the file and extract the files from the zip file. Next, open the extracted billing or AMI xml file in your browser.", "SubTitle":"Download your Smart Meter Data", "ShowTitle":"true", "ShowIntro":"true", "ShowSubTitle":"false", "Footer":"Note that the Google Chrome browser has a security policy that prevents the browser from using the stylesheet when it displaying the XML file, so we recommend using another browser when opening the XML file. Most mobile browsers require a third-party app to open zip files, so we recommend using your desktop computer for green button download.", "FooterLink":"xxx", "FooterLinkText":"Next steps", "FooterLinkType":null, "ShowFooterText":"true", "ShowFooterLink":"false" }');
        app.result = json;
        fixture.detectChanges();
        var sec = fixture.nativeElement.querySelectorAll("#span.icon.icon-toggle-down");
        expect(element.textContent).toContain('expand');
    }));
    it('Should show icon toggle up button with text collapse', testing_1.async(function () {
        var fixture = testing_1.TestBed.createComponent(greenbutton_1.GreenButton);
        var app = fixture.componentInstance;
        var element = fixture.nativeElement;
        app.enableLoader = false;
        var json = JSON.parse('{"BannerText":"Your usage data to save, send or print", "DownloadAmiTitle":"Please select the date range for which you would like to download your usage.", "DownloadBillsTitle":"Download your bills total usage and total cost in Green Button format.", "DownloadUsage":"Download my usage for specific days", "DownloadBills":"Download my bills", "DownloadingMessage":"Downloading...", "NoBillData":"No bill data is available.", "NoUsageData":"No usage data is available.", "DateError":"Please enter an end date that is after the start date.", "ExpandLabel":"expand", "CollapseLabel":"collapse", "NoBill":false, "NoAmi":false, "HideBillDownload":false, "HideUsageDownload":false, "Response":null, "Title":"Green Button Download", "IntroText":"The green button download generates a zip file containing your billing or AMI data XML files. The zip file will also contain a .xslt stylesheet file that will format the XML file when you view it on a browser. After you click the green button, save the zip file to a location on your computer that you will remember. Then go to the location where you saved the file and extract the files from the zip file. Next, open the extracted billing or AMI xml file in your browser.", "SubTitle":"Download your Smart Meter Data", "ShowTitle":"true", "ShowIntro":"true", "ShowSubTitle":"false", "Footer":"Note that the Google Chrome browser has a security policy that prevents the browser from using the stylesheet when it displaying the XML file, so we recommend using another browser when opening the XML file. Most mobile browsers require a third-party app to open zip files, so we recommend using your desktop computer for green button download.", "FooterLink":"xxx", "FooterLinkText":"Next steps", "FooterLinkType":null, "ShowFooterText":"true", "ShowFooterLink":"false" }');
        app.result = json;
        fixture.detectChanges();
        var link = fixture.nativeElement.querySelector(".btn.btn-default.xs-ml-10");
        link.click();
        fixture.detectChanges();
        expect(element.textContent).toContain('collapse');
    }));
    it('Should show green button title when title is true', testing_1.async(function () {
        var fixture = testing_1.TestBed.createComponent(greenbutton_1.GreenButton);
        var app = fixture.componentInstance;
        var element = fixture.nativeElement;
        app.enableLoader = false;
        var json = JSON.parse('{"BannerText":"Your usage data to save, send or print", "DownloadAmiTitle":"Please select the date range for which you would like to download your usage.", "DownloadBillsTitle":"Download your bills total usage and total cost in Green Button format.", "DownloadUsage":"Download my usage for specific days", "DownloadBills":"Download my bills", "DownloadingMessage":"Downloading...", "NoBillData":"No bill data is available.", "NoUsageData":"No usage data is available.", "DateError":"Please enter an end date that is after the start date.", "ExpandLabel":"expand", "CollapseLabel":"collapse", "NoBill":false, "NoAmi":false, "HideBillDownload":false, "HideUsageDownload":false, "Response":null, "Title":"Green Button Download", "IntroText":"The green button download generates a zip file containing your billing or AMI data XML files. The zip file will also contain a .xslt stylesheet file that will format the XML file when you view it on a browser. After you click the green button, save the zip file to a location on your computer that you will remember. Then go to the location where you saved the file and extract the files from the zip file. Next, open the extracted billing or AMI xml file in your browser.", "SubTitle":"Download your Smart Meter Data", "ShowTitle":"true", "ShowIntro":"true", "ShowSubTitle":"false", "Footer":"Note that the Google Chrome browser has a security policy that prevents the browser from using the stylesheet when it displaying the XML file, so we recommend using another browser when opening the XML file. Most mobile browsers require a third-party app to open zip files, so we recommend using your desktop computer for green button download.", "FooterLink":"xxx", "FooterLinkText":"Next steps", "FooterLinkType":null, "ShowFooterText":"true", "ShowFooterLink":"false" }');
        app.result = json;
        fixture.detectChanges();
        var sec = fixture.nativeElement.querySelectorAll("#panel-title greenbutton__heading");
        expect(element.textContent).toContain('Green Button Download');
    }));
    it('Should show billdownload and usage ami download when hidebilldownload and hideusagedownload are both false', testing_1.async(function () {
        var fixture = testing_1.TestBed.createComponent(greenbutton_1.GreenButton);
        var app = fixture.componentInstance;
        var element = fixture.nativeElement;
        app.enableLoader = false;
        var json = JSON.parse('{"Title":"Green Button Download","SubTitle":"Download your Smart Meter Data","IntroText":"The Green Button download gives utility customers easy and secure access to their energy usage information.","FromDate":"From:","ToDate":"To:","DownloadAmiTitle":"","DownloadBillsTitle":"","DownloadUsage":"Download my usage for specific days","DownloadBills":"Download my bills","DownloadingMessage":"","Footer":"Sample footer","FooterLinkText":"Next steps","NoBillData":"No bill data is available.","NoUsageData":"No usage data is available.","DateError":"Please enter an end date that is after the start date.","ShowTitle":"true","ShowIntro":"true","ShowSubTitle":"false","FooterLink":"https:///www.aclara.com","ShowFooterLink":"false","ShowFooterText":"false","NoBill":false,"NoAmi":false, "HideBillDownload":false, "HideUsageDownload":false,"Response":null}');
        app.result = json;
        fixture.detectChanges();
        var link = fixture.nativeElement.querySelector(".btn.btn-default.xs-ml-10");
        link.click();
        fixture.detectChanges();
        expect(element.textContent).toContain('Download my bills');
        expect(element.textContent).toContain('Download my usage for specific days');
    }));
    it('Should only show billdownload option when hidebilldownload false and hideusagedownload true', testing_1.async(function () {
        var fixture = testing_1.TestBed.createComponent(greenbutton_1.GreenButton);
        var app = fixture.componentInstance;
        var element = fixture.nativeElement;
        app.enableLoader = false;
        var json = JSON.parse('{"BannerText":"Benchmark your usage against similar homes.","FromDate":"From:","ToDate":"To:","DownloadAmiTitle":"Please select the date range for which you would like to download your usage.","DownloadBillsTitle":"Download your bills total usage and total cost in Green Button format.","DownloadUsage":"Download my usage for specific days","DownloadBills":"Download my bills","DownloadingMessage":"Downloading...","NoBillData":"No bill data is available.","NoUsageData":"No usage data is available.","DateError":"Please enter an end date that is after the start date.","ExpandLabel":"expand","CollapseLabel":"collapse","NoBill":false,"NoAmi":false,"HideBillDownload":false,"HideUsageDownload":true,"Response":null,"Title":"Green Button Download","IntroText":"The green button download generates a zip file containing your billing or AMI data XML files. The zip file will also contain a .xslt stylesheet file that will format the XML file when you view it on a browser. After you click the green button, save the zip file to a location on your computer that you will remember. Then go to the location where you saved the file and extract the files from the zip file. Next, open the extracted billing or AMI xml file in your browser.","SubTitle":"Download your usage data to use with Energy Star�s Yardstick tool","ShowTitle":"true","ShowIntro":"true","ShowSubTitle":"false","Footer":"Often a third-party app is needed to open zip files, so we recommend using your desktop/laptop computer for green button download.","FooterLink":"www.aclara.com","FooterLinkText":"footerlinktext","FooterLinkType":null,"ShowFooterText":"true","ShowFooterLink":"false"}');
        app.result = json;
        fixture.detectChanges();
        var link = fixture.nativeElement.querySelector(".btn.btn-default.xs-ml-10");
        link.click();
        fixture.detectChanges();
        expect(element.textContent).toContain('Download your bills total usage and total cost in Green Button format.');
        expect(element.textContent).not.toContain('Download my usage for specific days');
    }));
    it('Should only show amiusage download option when hidebilldownload true and hideusagedownload false', testing_1.async(function () {
        var fixture = testing_1.TestBed.createComponent(greenbutton_1.GreenButton);
        var app = fixture.componentInstance;
        var element = fixture.nativeElement;
        app.enableLoader = false;
        var json = JSON.parse('{"BannerText":"Your usage data to save, send or print","FromDate":"From:","ToDate":"To:","DownloadAmiTitle":"Please select the date range for which you would like to download your usage.","DownloadBillsTitle":"Download your bills total usage and total cost in Green Button format.","DownloadUsage":"Download my usage for specific days","DownloadBills":"Download my bills","DownloadingMessage":"Downloading...","NoBillData":"No bill data is available.","NoUsageData":"No usage data is available.","DateError":"Please enter an end date that is after the start date.","ExpandLabel":"expand","CollapseLabel":"collapse","NoBill":false,"NoAmi":false,"HideBillDownload":true,"HideUsageDownload":false,"Response":null,"Title":"Green Button Download","IntroText":"The green button download generates a zip file containing your billing or AMI data XML files. The zip file will also contain a .xslt stylesheet file that will format the XML file when you view it on a browser. After you click the green button, save the zip file to a location on your computer that you will remember. Then go to the location where you saved the file and extract the files from the zip file. Next, open the extracted billing or AMI xml file in your browser.","SubTitle":"Download your Smart Meter Data","ShowTitle":"true","ShowIntro":"true","ShowSubTitle":"false","Footer":"Note that the Google Chrome browser has a security policy that prevents the browser from using the stylesheet when it displaying the XML file, so we recommend using another browser when opening the XML file. Most mobile browsers require a third-party app to open zip files.","FooterLink":"https:///www.aclara.com","FooterLinkText":"Next steps","FooterLinkType":null,"ShowFooterText":"true","ShowFooterLink":"false"}');
        app.result = json;
        fixture.detectChanges();
        var link = fixture.nativeElement.querySelector(".btn.btn-default.xs-ml-10");
        link.click();
        fixture.detectChanges();
        expect(element.textContent).toContain('Download my usage for specific days');
        expect(element.textContent).not.toContain('Download your bills total usage and total cost in Green Button format.');
    }));
});
//# sourceMappingURL=greenbutton.dom.spec.js.map