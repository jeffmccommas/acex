﻿import { inject, async, TestBed } from "@angular/core/testing";
import { Http, HttpModule, XHRBackend, Response, ResponseOptions } from "@angular/http";
import { MockBackend, MockConnection } from "@angular/http/testing";
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { GBCAdminService } from "../../GreenButton/gbcadmin.service";

describe('TestGBCAdminService -- Mock', () => {

    beforeEach(async(() => {
        TestBed.resetTestEnvironment();

        TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting());
        TestBed.configureTestingModule({
            imports: [HttpModule],
            providers: [
                GBCAdminService,
                { provide: XHRBackend, useClass: MockBackend }
            ]
        }).compileComponents();;
    }));

    it('can instantiate service when inject service',
        inject([GBCAdminService], (service: GBCAdminService) => {
            expect(service instanceof GBCAdminService).toBe(true);
        }));

    it('can provide the mockBackend as XHRBackend',
        inject([XHRBackend], (backend: MockBackend) => {
            expect(backend).not.toBeNull('backend should be provided');
        }));


    let backend: MockBackend;
    let service: GBCAdminService;
    let response: Response;

    beforeEach(inject([Http, XHRBackend], (http: Http, be: MockBackend) => {
        backend = be;
        service = new GBCAdminService(http);
        let options = new ResponseOptions({
            body: [
                {
                    id: "QA87HomeProf",
                    contentRendered: "<p><b>Test</b></p>",
                    contentMarkdown: "*Hi there*"
                }
            ]
        });
        response = new Response(options);
    }));

    it('should get collection of third party applications', async(inject([], () => {
        backend.connections.subscribe((c: MockConnection) => c.mockRespond(response));

        service.GetThirdPartyApplications().subscribe((blogs) => {
            expect(blogs.length).toBe(1);
            expect(blogs[0].id).toBe("QA87HomeProf");
            expect(blogs[0].contentRendered).toBe("<p><b>Test</b></p>");
        });
    })));

    it('should get an empty third party app object used for creating a new third party', async(inject([], () => {
        backend.connections.subscribe((c: MockConnection) => c.mockRespond(response));

        service.GetNewApp().subscribe((blogs) => {
            expect(blogs.length).toBe(1);
            expect(blogs[0].id).toBe("QA87HomeProf");
            expect(blogs[0].contentRendered).toBe("<p><b>Test</b></p>");
        });
    })));

    it('should get green button initial data for form', async(inject([], () => {
        backend.connections.subscribe((c: MockConnection) => c.mockRespond(response));

        service.SaveApp('').subscribe((blogs) => {
            expect(blogs.length).toBe(1);
            expect(blogs[0].id).toBe("QA87HomeProf");
            expect(blogs[0].contentRendered).toBe("<p><b>Test</b></p>");
        });
    })));

});

