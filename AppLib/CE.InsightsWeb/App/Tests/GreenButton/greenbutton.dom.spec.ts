﻿import { async, TestBed } from "@angular/core/testing";
import { ElementRef } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { HttpModule } from "@angular/http";
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from "@angular/platform-browser-dynamic/testing";
import { GreenButton } from "../../GreenButton/greenbutton";
import { GreenButtonService } from "../../GreenButton/greenbutton.service";
import { AppService, IAppParams, IDeepLinkVariables } from "../../app.service";
import { Loader } from "../../Loader/loader";
import { DatepickerModule } from 'ngx-bootstrap';

//mock green button and app services
class MockAppService extends AppService {
    params: Array<IAppParams> = [{ tabKey: "tab.usage", parameters: "WWJVR1lFTjFPaE9BTnRiMXpqa2Z5L2hxL2NJK3JRNEhRbExJZ1pmcVpMN09kZ3NGOFpLcXpBdzNlcU8wUHRNcVNFVzJDaU5hOWlPb2I3MVhjOHJFVEJaMGIwZ3A1TjltY3pzdFBTSW44azQxak1VRW8vS1VISE92UU1UODlvbXNsRDhaeHJqM0NTQTVDSVlDRWtNcWlSRnZ0VHF1Q3ZHaTBHUVRjWjE5cnF0MVdBaXN1Z3FXQVhxakNLREVnd0VBdFR4S3RBQURPUzFHS0ZBT2M4Ty92QW42UEswSWNmUk1tUTU4MElLRVZoQW5JRTJVSTJFOEIzVGpCUjQyNk9idCtOTTRwcFU0NmhjcGJ3WTBJVWlsMWlpNGN4V0ovYXZWM0ExSjZrcTRjbWtHNmE5cElJSXVPb3NRb0QwUEMyWm90SzlQU3ZMdyt3Y2xDNGZwbnBsbzlqSkVBZzVHbmNmeUVwMTcrYnozeTk4c1pnZ0xENEpMN08vNmJOVHVDSW03UVlpYkt0S2Z2eGVFVzVvNlU0Y1hGbGI0cU9Ebmw0QWx5TUlBdHBFemFvYnloYnhXZzVTZWZ5eHExRnFXMWdtMzZmS05mRlZPQ3RrelhxOVcvcjN0OVRUU2VjQmZrNHBTVkhpT2dwTCtpR0ZBZ1Q3dytIaTFzVlYybVRTWFp4MzdWek52dGNuRHBSc0VMaGNPL1FtSVNnPT0=&type=bill&startDate=7/29/2016&endDate=7/29/2016", errorMessage: "" }];

    getParams(): Array<IAppParams> {
        return this.params;
    }

    getDeepLinkVariables(): Array<IDeepLinkVariables> {
        return this.deepLinkVariables;
    }
}

class MockGreenButtonService extends GreenButtonService {
    res: any;
    public unit: any = "unittest";
    getGreenButton() {
        return this.unit;
    }
    getDownloadDetails() {
        return JSON.parse('{ "BannerText": "Your usage data to save, send or print", "FromDate": "From:", "ToDate": "To:", "DownloadAmiTitle": "Please select the date range for which you would like to download your usage.", "DownloadBillsTitle": "Download your bills total usage and total cost in Green Button format.", "DownloadUsage": "Download my usage for specific days", "DownloadBills": "Download my bills", "DownloadingMessage": "Downloading...", "NoBillData": "No bill data is available.", "NoUsageData": "No usage data is available.", "DateError": "Please enter an end date that is after the start date.", "ExpandLabel": "expand", "CollapseLabel": "collapse", "NoBill": false, "NoAmi": false, "HideBillDownload": false, "HideUsageDownload": false, "Response": null, "Title": "Green Button Download", "IntroText": "The green button download generates a zip file containing your billing or AMI data XML files. The zip file will also contain a .xslt stylesheet file that will format the XML file when you view it on a browser. After you click the green button, save the zip file to a location on your computer that you will remember. Then go to the location where you saved the file and extract the files from the zip file. Next, open the extracted billing or AMI xml file in your browser.", "SubTitle": "Download your Smart Meter Data", "ShowTitle": "true", "ShowIntro": "true", "ShowSubTitle": "false", "Footer": "Note that the Google Chrome browser has a security policy that prevents the browser from using the stylesheet when it displaying the XML file, so we recommend using another browser when opening the XML file. \nMost mobile browsers require a third-party app to open zip files, so we recommend using your desktop computer for green button download.\n", "FooterLink": "https:///www.aclara.com", "FooterLinkText": "Next steps", "FooterLinkType": null, "ShowFooterText": "true", "ShowFooterLink": "false" }');
    }
}

describe('GreenButton DOM Tests', () => {

    beforeEach(async(() => {
        TestBed.resetTestEnvironment();
        TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting());
        ElementRef;

        TestBed.configureTestingModule({
            imports: [BrowserModule, FormsModule, HttpModule, DatepickerModule.forRoot()],
            declarations: [GreenButton, Loader]
        })
            .overrideComponent(GreenButton,
            {
                remove: { providers: [AppService, GreenButtonService] },
                add: {
                    providers: [
                        { provide: AppService, useClass: MockAppService },
                        { provide: GreenButtonService, useClass: MockGreenButtonService }
                    ]
                }
            }).compileComponents();
    }));

    it('Should show icon toggle down button with text expand', async(() => {
        const fixture = TestBed.createComponent(GreenButton);
        const app = fixture.componentInstance;
        const element = fixture.nativeElement;
        app.enableLoader = false;
        var json = JSON.parse('{"BannerText":"Your usage data to save, send or print", "DownloadAmiTitle":"Please select the date range for which you would like to download your usage.", "DownloadBillsTitle":"Download your bills total usage and total cost in Green Button format.", "DownloadUsage":"Download my usage for specific days", "DownloadBills":"Download my bills", "DownloadingMessage":"Downloading...", "NoBillData":"No bill data is available.", "NoUsageData":"No usage data is available.", "DateError":"Please enter an end date that is after the start date.", "ExpandLabel":"expand", "CollapseLabel":"collapse", "NoBill":false, "NoAmi":false, "HideBillDownload":false, "HideUsageDownload":false, "Response":null, "Title":"Green Button Download", "IntroText":"The green button download generates a zip file containing your billing or AMI data XML files. The zip file will also contain a .xslt stylesheet file that will format the XML file when you view it on a browser. After you click the green button, save the zip file to a location on your computer that you will remember. Then go to the location where you saved the file and extract the files from the zip file. Next, open the extracted billing or AMI xml file in your browser.", "SubTitle":"Download your Smart Meter Data", "ShowTitle":"true", "ShowIntro":"true", "ShowSubTitle":"false", "Footer":"Note that the Google Chrome browser has a security policy that prevents the browser from using the stylesheet when it displaying the XML file, so we recommend using another browser when opening the XML file. Most mobile browsers require a third-party app to open zip files, so we recommend using your desktop computer for green button download.", "FooterLink":"xxx", "FooterLinkText":"Next steps", "FooterLinkType":null, "ShowFooterText":"true", "ShowFooterLink":"false" }');
      
        app.result = json;
        fixture.detectChanges();
        const sec = fixture.nativeElement.querySelectorAll("#span.icon.icon-toggle-down");
        expect(element.textContent).toContain('expand');
    }));


    it('Should show icon toggle up button with text collapse', async(() => {
        const fixture = TestBed.createComponent(GreenButton);
        const app = fixture.componentInstance;
        const element = fixture.nativeElement;
        app.enableLoader = false;
        var json = JSON.parse('{"BannerText":"Your usage data to save, send or print", "DownloadAmiTitle":"Please select the date range for which you would like to download your usage.", "DownloadBillsTitle":"Download your bills total usage and total cost in Green Button format.", "DownloadUsage":"Download my usage for specific days", "DownloadBills":"Download my bills", "DownloadingMessage":"Downloading...", "NoBillData":"No bill data is available.", "NoUsageData":"No usage data is available.", "DateError":"Please enter an end date that is after the start date.", "ExpandLabel":"expand", "CollapseLabel":"collapse", "NoBill":false, "NoAmi":false, "HideBillDownload":false, "HideUsageDownload":false, "Response":null, "Title":"Green Button Download", "IntroText":"The green button download generates a zip file containing your billing or AMI data XML files. The zip file will also contain a .xslt stylesheet file that will format the XML file when you view it on a browser. After you click the green button, save the zip file to a location on your computer that you will remember. Then go to the location where you saved the file and extract the files from the zip file. Next, open the extracted billing or AMI xml file in your browser.", "SubTitle":"Download your Smart Meter Data", "ShowTitle":"true", "ShowIntro":"true", "ShowSubTitle":"false", "Footer":"Note that the Google Chrome browser has a security policy that prevents the browser from using the stylesheet when it displaying the XML file, so we recommend using another browser when opening the XML file. Most mobile browsers require a third-party app to open zip files, so we recommend using your desktop computer for green button download.", "FooterLink":"xxx", "FooterLinkText":"Next steps", "FooterLinkType":null, "ShowFooterText":"true", "ShowFooterLink":"false" }');

        app.result = json;
        fixture.detectChanges();
        var link = fixture.nativeElement.querySelector(".btn.btn-default.xs-ml-10");
        link.click();
        fixture.detectChanges();
        expect(element.textContent).toContain('collapse'); 
    }));

    it('Should show green button title when title is true', async(() => {
        const fixture = TestBed.createComponent(GreenButton);
        const app = fixture.componentInstance;
        const element = fixture.nativeElement;
        app.enableLoader = false;
        var json = JSON.parse('{"BannerText":"Your usage data to save, send or print", "DownloadAmiTitle":"Please select the date range for which you would like to download your usage.", "DownloadBillsTitle":"Download your bills total usage and total cost in Green Button format.", "DownloadUsage":"Download my usage for specific days", "DownloadBills":"Download my bills", "DownloadingMessage":"Downloading...", "NoBillData":"No bill data is available.", "NoUsageData":"No usage data is available.", "DateError":"Please enter an end date that is after the start date.", "ExpandLabel":"expand", "CollapseLabel":"collapse", "NoBill":false, "NoAmi":false, "HideBillDownload":false, "HideUsageDownload":false, "Response":null, "Title":"Green Button Download", "IntroText":"The green button download generates a zip file containing your billing or AMI data XML files. The zip file will also contain a .xslt stylesheet file that will format the XML file when you view it on a browser. After you click the green button, save the zip file to a location on your computer that you will remember. Then go to the location where you saved the file and extract the files from the zip file. Next, open the extracted billing or AMI xml file in your browser.", "SubTitle":"Download your Smart Meter Data", "ShowTitle":"true", "ShowIntro":"true", "ShowSubTitle":"false", "Footer":"Note that the Google Chrome browser has a security policy that prevents the browser from using the stylesheet when it displaying the XML file, so we recommend using another browser when opening the XML file. Most mobile browsers require a third-party app to open zip files, so we recommend using your desktop computer for green button download.", "FooterLink":"xxx", "FooterLinkText":"Next steps", "FooterLinkType":null, "ShowFooterText":"true", "ShowFooterLink":"false" }');
      
        app.result = json;
        fixture.detectChanges();
        const sec = fixture.nativeElement.querySelectorAll("#panel-title greenbutton__heading");
        expect(element.textContent).toContain('Green Button Download');
    }));

    it('Should show billdownload and usage ami download when hidebilldownload and hideusagedownload are both false', async(() => {
        const fixture = TestBed.createComponent(GreenButton);
        const app = fixture.componentInstance;
        const element = fixture.nativeElement;
        app.enableLoader = false;
        var json = JSON.parse('{"Title":"Green Button Download","SubTitle":"Download your Smart Meter Data","IntroText":"The Green Button download gives utility customers easy and secure access to their energy usage information.","FromDate":"From:","ToDate":"To:","DownloadAmiTitle":"","DownloadBillsTitle":"","DownloadUsage":"Download my usage for specific days","DownloadBills":"Download my bills","DownloadingMessage":"","Footer":"Sample footer","FooterLinkText":"Next steps","NoBillData":"No bill data is available.","NoUsageData":"No usage data is available.","DateError":"Please enter an end date that is after the start date.","ShowTitle":"true","ShowIntro":"true","ShowSubTitle":"false","FooterLink":"https:///www.aclara.com","ShowFooterLink":"false","ShowFooterText":"false","NoBill":false,"NoAmi":false, "HideBillDownload":false, "HideUsageDownload":false,"Response":null}');

        app.result = json;
        fixture.detectChanges();
        var link = fixture.nativeElement.querySelector(".btn.btn-default.xs-ml-10");
        link.click();
        fixture.detectChanges();
        expect(element.textContent).toContain('Download my bills');
        expect(element.textContent).toContain('Download my usage for specific days');
    }));

    it('Should only show billdownload option when hidebilldownload false and hideusagedownload true', async(() => {
        const fixture = TestBed.createComponent(GreenButton);
        const app = fixture.componentInstance;
        const element = fixture.nativeElement;
        app.enableLoader = false;
        var json = JSON.parse('{"BannerText":"Benchmark your usage against similar homes.","FromDate":"From:","ToDate":"To:","DownloadAmiTitle":"Please select the date range for which you would like to download your usage.","DownloadBillsTitle":"Download your bills total usage and total cost in Green Button format.","DownloadUsage":"Download my usage for specific days","DownloadBills":"Download my bills","DownloadingMessage":"Downloading...","NoBillData":"No bill data is available.","NoUsageData":"No usage data is available.","DateError":"Please enter an end date that is after the start date.","ExpandLabel":"expand","CollapseLabel":"collapse","NoBill":false,"NoAmi":false,"HideBillDownload":false,"HideUsageDownload":true,"Response":null,"Title":"Green Button Download","IntroText":"The green button download generates a zip file containing your billing or AMI data XML files. The zip file will also contain a .xslt stylesheet file that will format the XML file when you view it on a browser. After you click the green button, save the zip file to a location on your computer that you will remember. Then go to the location where you saved the file and extract the files from the zip file. Next, open the extracted billing or AMI xml file in your browser.","SubTitle":"Download your usage data to use with Energy Star�s Yardstick tool","ShowTitle":"true","ShowIntro":"true","ShowSubTitle":"false","Footer":"Often a third-party app is needed to open zip files, so we recommend using your desktop/laptop computer for green button download.","FooterLink":"www.aclara.com","FooterLinkText":"footerlinktext","FooterLinkType":null,"ShowFooterText":"true","ShowFooterLink":"false"}');
        app.result = json;
        fixture.detectChanges();
        var link = fixture.nativeElement.querySelector(".btn.btn-default.xs-ml-10");
        link.click();
        fixture.detectChanges();
        expect(element.textContent).toContain('Download your bills total usage and total cost in Green Button format.');
        expect(element.textContent).not.toContain('Download my usage for specific days');
    }));

    it('Should only show amiusage download option when hidebilldownload true and hideusagedownload false', async(() => {
        const fixture = TestBed.createComponent(GreenButton);
        const app = fixture.componentInstance;
        const element = fixture.nativeElement;
        app.enableLoader = false;
        var json = JSON.parse('{"BannerText":"Your usage data to save, send or print","FromDate":"From:","ToDate":"To:","DownloadAmiTitle":"Please select the date range for which you would like to download your usage.","DownloadBillsTitle":"Download your bills total usage and total cost in Green Button format.","DownloadUsage":"Download my usage for specific days","DownloadBills":"Download my bills","DownloadingMessage":"Downloading...","NoBillData":"No bill data is available.","NoUsageData":"No usage data is available.","DateError":"Please enter an end date that is after the start date.","ExpandLabel":"expand","CollapseLabel":"collapse","NoBill":false,"NoAmi":false,"HideBillDownload":true,"HideUsageDownload":false,"Response":null,"Title":"Green Button Download","IntroText":"The green button download generates a zip file containing your billing or AMI data XML files. The zip file will also contain a .xslt stylesheet file that will format the XML file when you view it on a browser. After you click the green button, save the zip file to a location on your computer that you will remember. Then go to the location where you saved the file and extract the files from the zip file. Next, open the extracted billing or AMI xml file in your browser.","SubTitle":"Download your Smart Meter Data","ShowTitle":"true","ShowIntro":"true","ShowSubTitle":"false","Footer":"Note that the Google Chrome browser has a security policy that prevents the browser from using the stylesheet when it displaying the XML file, so we recommend using another browser when opening the XML file. Most mobile browsers require a third-party app to open zip files.","FooterLink":"https:///www.aclara.com","FooterLinkText":"Next steps","FooterLinkType":null,"ShowFooterText":"true","ShowFooterLink":"false"}');
        app.result = json;
        fixture.detectChanges();
        var link = fixture.nativeElement.querySelector(".btn.btn-default.xs-ml-10");
        link.click();
        fixture.detectChanges();
        expect(element.textContent).toContain('Download my usage for specific days');
        expect(element.textContent).not.toContain('Download your bills total usage and total cost in Green Button format.');
    }));

   });