﻿import { async, TestBed } from "@angular/core/testing";
import { ElementRef } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { HttpModule } from "@angular/http";
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from "@angular/platform-browser-dynamic/testing";
import { Threshold } from "../../Threshold/threshold";
import { ThresholdService } from "../../Threshold/threshold.service";
import { AppService, IAppParams, IDeepLinkVariables } from "../../app.service";
import { Loader } from "../../Loader/loader";
import { ObjToArr } from "../../Helper/pipe";

//mock profile and app services
class MockAppService extends AppService {
   public params: Array<IAppParams> = [{ tabKey: "tab.waterportal", parameters: "YbUGYEN1OhOANtb1zjkfy/hq/cI+rQ4HQlLIgZfqZL7OdgsF8ZKqzAw3eqO0PtMqSEW2CiNa9iOob71Xc8rETBZ0b0gp5N9mczstPSIn8k41jMUEo/KUHHOvQMT89omslD8Zxrj3CSA5CIYCEkMqiRFvtTquCvGi0GQTcZ19rqt1WAisugqWAXqjCKDEgwEAtTxKtAADOS1GKFAOc8O/vAn6PK0IcfRMmQ580IKEVhAnIE2UI2E8B3TjBR426Obt+NM4ppU46hcpbwY0IUil1ojq/lN/d8AzTriNnHjy3eVb2OyYDuFYOFopUUpX/v1qUrVgV7VD96UW2zEGS5zRb4N0/RHYCSwUE3qgwkzyOjUaOX3L9Jrwk9e4tNouzbEbjbXPjTwDcqbqUBk/Wn5eik5/64JISCsuqUNSz5GbjMN4F4WYoFT37PtjCq7KsV16YccmF3NObF9MfU45pgPyRI8Qas6g2fG/XdChanb7sRPGDj0C0do7/86T3PyOYJoV4v3+afpi7+iSGcfnYR7bhg==", errorMessage: "" }];



    public deepLinkVariables: Array<IDeepLinkVariables> = [{ deepLinkWidget: "", deepLinkWidgetTab: "", deepLinkWidgetSection: "" }];

    getParams(): Array<IAppParams> {
        return this.params;
    }

}

class MockThresholdService extends ThresholdService {
    public res: any;
    public unit: any = "unittest";
    getThreshold(tabKey, params, commodity) {
        return this.unit;
    }
}

describe('Threshold Widget DOM Tests', () => {

    beforeEach(async(() => {
        TestBed.resetTestEnvironment();
        TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting());
        ElementRef;
        TestBed.configureTestingModule({
            imports: [BrowserModule, FormsModule, HttpModule],
            declarations: [Threshold, Loader, ObjToArr]
        })
            .overrideComponent(Threshold,
            {
                remove: { providers: [AppService, ThresholdService] },
                add: {
                    providers: [
                        { provide: AppService, useClass: MockAppService },
                        { provide: ThresholdService, useClass: MockThresholdService }
                    ]
                }
            }).compileComponents();

    }));

    it("should get non tier rate message", () => {
        const fixture = TestBed.createComponent(Threshold);
        const app = fixture.componentInstance;
        app.enableLoader = false;
        var json = JSON.parse('{"Title":"Your Water Budget","IntroText":"Sample introtext","SubTitle":"Sample subtitle","ShowTitle":"true","ShowIntro":"false","Commodities":"water","CommoditiesText":"Water","CommoditiesIconFonts":null,"NoBillsText":"Your usage data is not available yet. Please check back later.","NonTier":true,"NonTierText":"This area shows how your usage falls into different rate tiers. This is not displayed because you are not on a tiered rate.","UseToDateLabel":"So far this month - ","DaysLeftLabel":"days left","PricingTiersLabel":"Pricing Tiers","PerUOM":null,"ProjectedUseMessage":"Your projected use this month is","AveragedailyUseMessage":"Projected use is based on your daily average use of","BilledUseMessage":"Your use on your last bill was","BilledUseLabel":"Billed use -","DateTo":"to","UseToDate":null,"ProjectedUse":null,"AverageDailyUse":"0.00","UOM":"CF","BillTotalServiceUse":"2,250","BillStartDate":"Oct 01 2016","BillEndDate":"Nov 01 2016","NoBills":false,"ShowSubTitle":"false","Footer":"Sample footer text","FooterLinkText":"Next steps","FooterLink":"","ShowFooterLink":"false","ShowFooterText":"false","RateMismatch":false,"BillSummary":{"Title":null,"IntroText":null,"TotalGasUse":null,"TotalElectricUse":null,"TotalWaterUse":null,"AvgDailyCostText":null,"NumberOfDaysText":null,"AvgTempText":null,"AmountDueText":null,"ViewBillHistory":null,"ShowTitle":null,"ShowIntro":null,"ShowBillHistoryLink":null,"Commodities":null,"BillHistoryLink":null,"CurrencyFormat":null,"NoBillsText":null,"TotalGasUsed":0,"TotalElectricityUsed":0,"TotalWaterUsed":2250,"GasUOM":"","ElectricUOM":"","WaterUOM":"CF","AvgDailyCost":"0.92","NumberOfDays":31,"AvgTemp":0,"AmountDue":"28.5","BillStartDateGas":null,"BillEndDateGas":null,"BillStartDateElectric":null,"BillEndDateElectric":null,"BillStartDateWater":"10/1/2016 12:00:00 AM","BillEndDateWater":"11/1/2016 12:00:00 AM","NoBills":false,"DueDate":null,"TempFormat":null,"Footer":null,"FooterLinkText":null,"FooterLink":null,"ShowFooterLink":null,"ShowFooterText":null,"RateClassGas":null,"RateClassElectric":null,"RateClassWater":"S1","RateClassSewer":null,"RateMismatchGas":false,"RateMismatchElectric":false,"RateMismatchWater":false,"RateMismatchSewer":false},"BillToDate":{"Title":"","SubTitle":"","IntroText":"","BillPeriod":null,"ElectricUseToDateText":"","GasUseToDateText":"","WaterUseToDateText":"","AvgDailyCostText":"","NumberOfDaysText":"","ProjectedCostText":"","CostToDateText":"","ElectricUseToDate":null,"GasUseToDate":null,"WaterUseToDate":null,"SewerUseToDate":null,"ElectricUseProjected":null,"GasUseProjected":null,"WaterUseProjected":null,"SewerUseProjected":null,"GasUOM":null,"ElectricUOM":null,"WaterUOM":null,"SewerUOM":null,"AvgDailyCost":null,"NumberOfDays":0,"DaysLeftInBillPeriod":0,"AvgTemp":0,"AverageDailyUseGas":0,"AverageDailyUseElectric":0,"AverageDailyUseWater":0,"AverageDailyUseSewer":0,"RateMismatchGas":false,"RateMismatchElectric":false,"RateMismatchWater":false,"RateMismatchSewer":false,"RateClassGas":null,"RateClassElectric":null,"RateClassWater":null,"RateClassSewer":null,"CostToDate":null,"ProjectedCost":null,"BillStatementTitle":"","BTDTitle":"","NoBillToDate":true,"Commodities":"","ShowTitle":"","ShowIntro":"","BTDFooter":"","ShowBTDBillPeriod":"","ShowBTDCostToDate":"","ShowBTDProjectedCost":"","ShowBTDElectricUseToDate":"","ShowBTDGasUseToDate":"","ShowBTDWaterUseToDate":"","ShowBTDAverageDailyCost":"","ShowBTDSewerUseToDate":"","Footer":"","FooterLinkText":"","FooterLink":"","ShowFooterLink":"","ShowFooterText":"","GUseToDate":0,"EUseToDate":0,"WUseToDate":0,"SUseToDate":0,"BillSummary":null},"TierBoundaries":[{"BaseOrTierKey":"Tier1","TimeOfUseKey":"Undefined","SeasonKey":"Winter","Threshold":16,"TierLabel":"Tier 1","TierColor":"#4169e1","TierRangeLabel":"Up to 16 CF","TierRange":16,"InTier":false,"InTierPercentage":0,"TierPercentage":34.78,"UseCharges":5.552,"UseChargesLabel":"$5.55 per CF"},{"BaseOrTierKey":"Tier2","TimeOfUseKey":"Undefined","SeasonKey":"Winter","Threshold":22,"TierLabel":"Tier 2","TierColor":"#09a985","TierRangeLabel":"16 - 22 CF","TierRange":6,"InTier":false,"InTierPercentage":0,"TierPercentage":13.04,"UseCharges":6.772,"UseChargesLabel":"$6.77 per CF"},{"BaseOrTierKey":"Tier3","TimeOfUseKey":"Undefined","SeasonKey":"Winter","Threshold":34,"TierLabel":"Tier 3","TierColor":"#F5A623","TierRangeLabel":"22 - 34 CF","TierRange":12,"InTier":false,"InTierPercentage":0,"TierPercentage":26.09,"UseCharges":7.518,"UseChargesLabel":"$7.52 per CF"},{"BaseOrTierKey":"Tier4","TimeOfUseKey":null,"SeasonKey":"Winter","Threshold":34,"TierLabel":"Tier 4","TierColor":"#f60","TierRangeLabel":"Above 34 CF","TierRange":12,"InTier":true,"InTierPercentage":33,"TierPercentage":26.09,"UseCharges":32.518,"UseChargesLabel":"$7.52 per CF"}]}');
        app.result = json;
        fixture.detectChanges();
        const lis = fixture.nativeElement.querySelectorAll("strong");
        expect(lis["0"].innerText).toContain("you are not on a tiered rate");
    });

    it('expression changed after it was checked - should get threshold tier 4 for water', async(() => {
        const fixture = TestBed.createComponent(Threshold);
        const app = fixture.componentInstance;
        app.enableLoader = false;
        var json = JSON.parse('{"Title":"Your Water Budget","IntroText":"Sample introtext","SubTitle":"Sample subtitle","ShowTitle":"true","ShowIntro":"false","Commodities":"water","CommoditiesText":"Water","CommoditiesIconFonts":"icon-drop","NoBillsText":"Your usage data is not available yet. Please check back later.","NonTier":false,"NonTierText":null,"UseToDateLabel":"So far this month - ","DaysLeftLabel":"days left","PricingTiersLabel":"Pricing Tiers","PerUOM":null,"ProjectedUseMessage":"Your projected use this month is","AveragedailyUseMessage":"Projected use is based on your daily average use of","BilledUseMessage":"Your use on your last bill was","BilledUseLabel":"Billed use -","DateTo":"to","UseToDate":null,"ProjectedUse":null,"AverageDailyUse":"0.00","UOM":"CF","BillTotalServiceUse":"2,250","BillStartDate":"Oct 01 2016","BillEndDate":"Nov 01 2016","NoBills":false,"ShowSubTitle":"false","Footer":"Sample footer text","FooterLinkText":"Next steps","FooterLink":"","ShowFooterLink":"false","ShowFooterText":"false","RateMismatch":false,"BillSummary":{"Title":null,"IntroText":null,"TotalGasUse":null,"TotalElectricUse":null,"TotalWaterUse":null,"AvgDailyCostText":null,"NumberOfDaysText":null,"AvgTempText":null,"AmountDueText":null,"ViewBillHistory":null,"ShowTitle":null,"ShowIntro":null,"ShowBillHistoryLink":null,"Commodities":null,"BillHistoryLink":null,"CurrencyFormat":null,"NoBillsText":null,"TotalGasUsed":0,"TotalElectricityUsed":0,"TotalWaterUsed":2250,"GasUOM":"","ElectricUOM":"","WaterUOM":"CF","AvgDailyCost":"0.92","NumberOfDays":31,"AvgTemp":0,"AmountDue":"28.5","BillStartDateGas":null,"BillEndDateGas":null,"BillStartDateElectric":null,"BillEndDateElectric":null,"BillStartDateWater":"10/1/2016 12:00:00 AM","BillEndDateWater":"11/1/2016 12:00:00 AM","NoBills":false,"DueDate":null,"TempFormat":null,"Footer":null,"FooterLinkText":null,"FooterLink":null,"ShowFooterLink":null,"ShowFooterText":null,"RateClassGas":null,"RateClassElectric":null,"RateClassWater":"S1","RateClassSewer":null,"RateMismatchGas":false,"RateMismatchElectric":false,"RateMismatchWater":false,"RateMismatchSewer":false}, "BillToDate":{"Title":"","SubTitle":"","IntroText":"","BillPeriod":null,"ElectricUseToDateText":"","GasUseToDateText":"","WaterUseToDateText":"","AvgDailyCostText":"","NumberOfDaysText":"","ProjectedCostText":"","CostToDateText":"","ElectricUseToDate":null,"GasUseToDate":null,"WaterUseToDate":null,"SewerUseToDate":null,"ElectricUseProjected":null,"GasUseProjected":null,"WaterUseProjected":null,"SewerUseProjected":null,"GasUOM":null,"ElectricUOM":null,"WaterUOM":null,"SewerUOM":null,"AvgDailyCost":null,"NumberOfDays":0,"DaysLeftInBillPeriod":0,"AvgTemp":0,"AverageDailyUseGas":0,"AverageDailyUseElectric":0,"AverageDailyUseWater":0,"AverageDailyUseSewer":0,"RateMismatchGas":false,"RateMismatchElectric":false,"RateMismatchWater":false,"RateMismatchSewer":false,"RateClassGas":null,"RateClassElectric":null,"RateClassWater":null,"RateClassSewer":null,"CostToDate":null,"ProjectedCost":null,"BillStatementTitle":"","BTDTitle":"","NoBillToDate":false,"Commodities":"","ShowTitle":"","ShowIntro":"","BTDFooter":"","ShowBTDBillPeriod":"","ShowBTDCostToDate":"","ShowBTDProjectedCost":"","ShowBTDElectricUseToDate":"","ShowBTDGasUseToDate":"","ShowBTDWaterUseToDate":"","ShowBTDAverageDailyCost":"","ShowBTDSewerUseToDate":"","Footer":"","FooterLinkText":"","FooterLink":"","ShowFooterLink":"","ShowFooterText":"","GUseToDate":0,"EUseToDate":0,"WUseToDate":0,"SUseToDate":0,"BillSummary":null},"TierBoundaries":[{"BaseOrTierKey":"Tier1","TimeOfUseKey":"Undefined","SeasonKey":"Winter","Threshold":16,"TierLabel":"Tier 1","TierColor":"#4169e1","TierRangeLabel":"Up to 16 CF","TierRange":16,"InTier":false,"InTierPercentage":0,"TierPercentage":34.78,"UseCharges":5.552,"UseChargesLabel":"$5.55 per CF"},{"BaseOrTierKey":"Tier2","TimeOfUseKey":"Undefined","SeasonKey":"Winter","Threshold":22,"TierLabel":"Tier 2","TierColor":"#09a985","TierRangeLabel":"16 - 22 CF","TierRange":6,"InTier":false,"InTierPercentage":0,"TierPercentage":13.04,"UseCharges":6.772,"UseChargesLabel":"$6.77 per CF"},{"BaseOrTierKey":"Tier3","TimeOfUseKey":"Undefined","SeasonKey":"Winter","Threshold":34,"TierLabel":"Tier 3","TierColor":"#F5A623","TierRangeLabel":"22 - 34 CF","TierRange":12,"InTier":false,"InTierPercentage":0,"TierPercentage":26.09,"UseCharges":7.518,"UseChargesLabel":"$7.52 per CF"},{"BaseOrTierKey":"Tier4","TimeOfUseKey":null,"SeasonKey":"Winter","Threshold":34,"TierLabel":"Tier 4","TierColor":"#f60","TierRangeLabel":"Above 34 CF","TierRange":12,"InTier":true,"InTierPercentage":33,"TierPercentage":26.09,"UseCharges":7.518,"UseChargesLabel":"$7.52 per CF"}]}');
        app.result = json;
        fixture.detectChanges();
        const lis = fixture.nativeElement.querySelectorAll("p");
        expect(lis["0"].innerText).toContain("Your projected use this month is");
    }));
});


