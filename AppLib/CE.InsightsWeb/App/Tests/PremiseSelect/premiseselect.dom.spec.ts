﻿//import { async, TestBed } from "@angular/core/testing";
//import { ElementRef } from "@angular/core";
//import { FormsModule } from "@angular/forms";
//import { BrowserModule } from "@angular/platform-browser";
//import { HttpModule } from "@angular/http";
//import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from "@angular/platform-browser-dynamic/testing";
//import { PremiseSelect } from "../../PremiseSelect/premiseselect";
//import { PremiseSelectService } from "../../PremiseSelect/premiseselect.service";
//import { AppService, IAppParams, IDeepLinkVariables } from "../../app.service";
//import { Loader } from "../../Loader/loader";
//import { ObjToArr } from "../../Helper/pipe";

//class MockElementRef implements ElementRef {
//    nativeElement = {};
//}

////mock premise and app services
//class MockAppService extends AppService {
//      appService.params = [{ tabKey: "tab.dashboard", parameters: "YbUGYEN1OhOANtb1zjkfy/hq/cI+rQ4HQlLIgZfqZL7OdgsF8ZKqzAw3eqO0PtMqSEW2CiNa9iOob71Xc8rETBZ0b0gp5N9mczstPSIn8k41jMUEo/KUHHOvQMT89omslD8Zxrj3CSA5CIYCEkMqiRFvtTquCvGi0GQTcZ19rqt1WAisugqWAXqjCKDEgwEAtTxKtAADOS1GKFAOc8O/vAn6PK0IcfRMmQ580IKEVhAnIE2UI2E8B3TjBR426Obt+NM4ppU46hcpbwY0IUil1ojq/lN/d8AzTriNnHjy3eVb2OyYDuFYOFopUUpX/v1qUrVgV7VD96UW2zEGS5zRb4N0/RHYCSwUE3qgwkzyOjUaOX3L9Jrwk9e4tNouzbEbjbXPjTwDcqbqUBk/Wn5eik5/64JISCsuqUNSz5GbjMN4F4WYoFT37PtjCq7KsV16YccmF3NObF9MfU45pgPyRI8Qas6g2fG/XdChanb7sRPGDj0C0do7/86T3PyOYJoV4v3+afpi7+iSGcfnYR7bhg==", errorMessage: "" }];
//      public deepLinkVariables: Array<IDeepLinkVariables> = [{ deepLinkWidget: "", deepLinkWidgetTab: "", deepLinkWidgetSection: "" }];

//    getParams(): Array<IAppParams> {
//        return this.params;
//    }
//    getDeepLinkVariables(): Array<IDeepLinkVariables> {
//        return this.deepLinkVariables;
//    }
//}

//class MockPremiseSelectService extends PremiseSelectService {
//    public res: any;
//    public unit: any = "unittest";

//    getAccountPremise(tabKey, params) {
//        return this.unit;
//    }
//}

//describe('Premise Select DOM Tests',
//    () => {
//        beforeEach(async(() => {
//            TestBed.resetTestEnvironment();
//            TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting());
//            ElementRef;
//            TestBed.configureTestingModule({
//                imports: [BrowserModule, FormsModule, HttpModule],
//                declarations: [PremiseSelect, Loader, ObjToArr]
//            })
//                .overrideComponent(PremiseSelect,
//                {
//                    remove: { providers: [AppService, PremiseSelectService] },
//                    add: {
//                        providers: [
//                            { provide: AppService, useClass: MockAppService },
//                            { provide: PremiseSelectService, useClass: MockPremiseSelectService }
//                        ]
//                    }
//                })
//                .compileComponents();
//        }));


//        xit('should get Account # single account multiservicepoint', async(() => {
//            const fixture = TestBed.createComponent(PremiseSelect);
//            const app = fixture.componentInstance;
//            const element = fixture.nativeElement;
//            app.enableLoader = false;
//            var json = JSON.parse('{"AccountNumberLabel":"Account #","LoadingMessage":"Loading account information...","ExpandLabel":"expand","CollapseLabel":"collapse","FirstName":"CAROLYN","LastName":"TRUMBULL","CustomerId":"993774751","AccountId":"1218337093","AccountList":[{"Id":"1218337093","Premises":[{"Id":"1218337000","Addr1":"15924 S Pannes Ave","Addr2":"","City":"COMPTON","StateProvince":"CA","PostalCode":"902214523","Country":"US","Service":[{"Id":"SC_1218337000","Type":"gas_total","Description":"gas total","ActiveDate":null,"InActiveDate":null,"CommodityKey":"gas","BudgetBillingInd":null,"ServicePoints":[{"Id":"SP_1218337000","Meters":[{"Id":"M1218337000","IsDeleted":null,"ReplacedMeterId":""}]}]},{"Id":"SC_189354704","Type":"electric_total","Description":"electric total","ActiveDate":null,"InActiveDate":null,"CommodityKey":"electric","BudgetBillingInd":null,"ServicePoints":[{"Id":"SP_189354704","Meters":[{"Id":"m189354704","IsDeleted":null,"ReplacedMeterId":""}]}]},{"Id":"SC_189354705","Type":"water_total","Description":"water total","ActiveDate":null,"InActiveDate":null,"CommodityKey":"water","BudgetBillingInd":null,"ServicePoints":[{"Id":"SP_189354705","Meters":[{"Id":"M189354705","IsDeleted":null,"ReplacedMeterId":""}]}]}],"Commodities":"Gas & Electric & Water","Selected":true,"Url":"Page?enc=o7FI+Wz+RYUjbcnOjKjEqLry5lf7U302ozMzaWuUsrYmL3er7HjaT/22LTj5iDWREVP770SG847oIxx5eiqLGulBoLXyxLfRCajOK6+yOVIsBmQlrMlmuDHv6tEjn/iVVw+F+Zsik87nOfqs+YGyUcopy31kHpsEvtaz/kssbDsVQP19K8MHZZ2M7J4PxyGSUe7fQUqO9YajalynWPcTQTYHqxvOHxf2bOk1YK3pV78BhuT+4HAxzcJAwK9ddP1HU"}]}],"NoAccounts":false,"ShowSinglePremise":true,"Title":null,"IntroText":null,"SubTitle":null,"ShowTitle":null,"ShowIntro":null,"ShowSubTitle":null,"Footer":null,"FooterLink":"","FooterLinkText":null,"FooterLinkType":null,"ShowFooterText":null,"ShowFooterLink":null}');
//            app.result = json;
//            fixture.detectChanges();
//            //expect(element.textContent).toContain('Account #');
//            expect(true).toBe(true);
//        }));

//        xit('should get Account # multiaccounts multipremise', async(() => {
//            const fixture = TestBed.createComponent(PremiseSelect);
//            const app = fixture.componentInstance;
//            const element = fixture.nativeElement;
//            app.enableLoader = false;
//            var json = JSON
//                .parse('{"AccountNumberLabel":"Account #","LoadingMessage":"Loading account information...","ExpandLabel":"expand","CollapseLabel":"collapse","FirstName":"CAROL","LastName":"ELK","CustomerId":"TC87_CustMulti2prem","AccountId":"TC87_AcctMulti2prem","AccountList":[{"Id":"TC87_AcctMulti2prem","Premises":[{"Id":"TC87_PremMulti2prem","Addr1":"15928 CHANDLER STREET","Addr2":"","City":"Worcester","StateProvince":"MA","PostalCode":"01604","Country":"US","Service":[{"Id":"SC_TC87_PremMulti2prem_gas","Type":"gas_total","Description":"gas total","ActiveDate":null,"InActiveDate":null,"CommodityKey":"gas","BudgetBillingInd":null,"ServicePoints":[{"Id":"SP_M_CustMulti2prem","Meters":[{"Id":"M_CustMulti2prem","IsDeleted":null,"ReplacedMeterId":""}]}]}],"Commodities":"Gas","Selected":true,"Url":"Page?enc=o7FI+Wz+RYUjbcnOjKjEqLry5lf7U302ozMzaWuUsrYmL3er7HjaT/22LTj5iDWREVP770SG847oIxx5eiqLGvW1LG5vrULl6mfetvnqnILczbkJ0auW8QCwopU6xapqlGH2ZwIYhtRXj4gL8GBR/WZOZWODjyc6BtVxGjNxHDjzzM24RbFLZ3C6Rqv1j65dYQLbdxxKwexD8vqN1/9ooA1GG5il+g4rqaqT5mqD15bjSNYPT5zgRL0J874KPvboIfTTe+vWk3XxZAqoZ6MuDEDqFWIiq840aznYYY5tlmrTcE4+93BWK+pFkvAROo9xug2hTWKN3itOjExGQdGY97LKqNxK5jlGQEVy8RG6T5eKG8h9IQ3hTzhpgkkTvVUbV3VVIV6+Ck5iArnUUUiml3cNTts2BAEkYaOJscQoyY2aAxyRYw+3vuEFVnvN8YJns+OFIkLkYP+np99ZA89P3c2AMLalRFJi9aNB7pTXTYUvT9+MgkkCqjB94DSQ9o139mqf+ZXF7mKFRilhCewf+3kiQwxm9JJjvzFvvxDDKTyQsUR2E05KgUoXGWfEl9aHbtVZtjrgVdVFICahFEvZVlJRhiq/K61uT1Ee4SdY5+RjCHnnewlzTKZPdhg8YPRPjA2241NB+MgEoNEwEQ/naQ=="},{"Id":"TC87_PremMulti2prem2","Addr1":"15929 CHANDLER STREET","Addr2":"","City":"Worcester","StateProvince":"MA","PostalCode":"01604","Country":"US","Service":[{"Id":"SC_TC87_PremMulti2prem2_electric","Type":"electric_total","Description":"electric total","ActiveDate":null,"InActiveDate":null,"CommodityKey":"electric","BudgetBillingInd":null,"ServicePoints":[{"Id":"SP_M_CustMulti2prem2_elec","Meters":[{"Id":"M_CustMulti2prem2_elec","IsDeleted":null,"ReplacedMeterId":""}]}]},{"Id":"SC_TC87_PremMulti2prem2_gas","Type":"gas_total","Description":"gas total","ActiveDate":null,"InActiveDate":null,"CommodityKey":"gas","BudgetBillingInd":null,"ServicePoints":[{"Id":"SP_M_CustMulti2prem2","Meters":[{"Id":"M_CustMulti2prem2","IsDeleted":null,"ReplacedMeterId":""}]}]}],"Commodities":"Electric & Gas","Selected":false,"Url":"Page?enc=o7FI+Wz+RYUjbcnOjKjEqLry5lf7U302ozMzaWuUsrYmL3er7HjaT/22LTj5iDWREVP770SG847oIxx5eiqLGvW1LG5vrULl6mfetvnqnILczbkJ0auW8QCwopU6xapqlGH2ZwIYhtRXj4gL8GBR/WZOZWODjyc6BtVxGjNxHDjzzM24RbFLZ3C6Rqv1j65dYQLbdxxKwexD8vqN1/9ooA1GG5il+g4rqaqT5mqD15bjSNYPT5zgRL0J874KPvboIfTTe+vWk3XxZAqoZ6MuDEDqFWIiq840aznYYY5tlmpA9jBHJR0B3HEKCACXo2UVMdUWbrFKmdPqB0oqJYwPPvD20ryVYMtxLaQhW3fXqlVQqz5A3W5/5hUMCBS4rsEzqzWz9YRxOxoaFENFsmRWbLu9ZVQaVO8GFUkfPo++qMdXeKxy9W8zcw0+PPxDirEtTTcxedGCNUh2j6MMwl8R6/GmqDGTQx8giFt1Mr4rhuBQMzSkzIoq0YZyQotWIas0vLvXZsUSpkdP2g+p7lTUKynWmreTzIEwLMYG9myrbMlRm2sKiYrKErzuvEZIbIdwsDHAMXGNo6LxiqhtLSzgMQC5EgIEFujgK3bsZMoWs10HJwAEMMxLMbOTYouE/oVMQMvWW5hOqqZGPEzA4Fkq2m+breOhdh+Hxp6cWuyeR5o="}]}],"NoAccounts":false,"ShowSinglePremise":true,"Title":null,"IntroText":null,"SubTitle":null,"ShowTitle":null,"ShowIntro":null,"ShowSubTitle":null,"Footer":null,"FooterLink":"","FooterLinkText":null,"FooterLinkType":null,"ShowFooterText":null,"ShowFooterLink":null}');
//            app.result = json;
//            fixture.detectChanges();
//            //expect(element.textContent).toContain('Account #');
//            expect(true).toBe(true);
//        }));

//        xit('should get expand link and toggle down', async(() => {
//            const fixture = TestBed.createComponent(PremiseSelect);
//            const app = fixture.componentInstance;
//            const element = fixture.nativeElement;
//            app.enableLoader = false;
//            var json = JSON
//                .parse('{"AccountNumberLabel":"Account #","LoadingMessage":"Loading account information...","ExpandLabel":"expand","CollapseLabel":"collapse","FirstName":"CAROL","LastName":"ELK","CustomerId":"TC87_CustMulti2prem","AccountId":"TC87_AcctMulti2prem","AccountList":[{"Id":"TC87_AcctMulti2prem","Premises":[{"Id":"TC87_PremMulti2prem","Addr1":"15928 CHANDLER STREET","Addr2":"","City":"Worcester","StateProvince":"MA","PostalCode":"01604","Country":"US","Service":[{"Id":"SC_TC87_PremMulti2prem_gas","Type":"gas_total","Description":"gas total","ActiveDate":null,"InActiveDate":null,"CommodityKey":"gas","BudgetBillingInd":null,"ServicePoints":[{"Id":"SP_M_CustMulti2prem","Meters":[{"Id":"M_CustMulti2prem","IsDeleted":null,"ReplacedMeterId":""}]}]}],"Commodities":"Gas","Selected":true,"Url":"Page?enc=o7FI+Wz+RYUjbcnOjKjEqLry5lf7U302ozMzaWuUsrYmL3er7HjaT/22LTj5iDWREVP770SG847oIxx5eiqLGvW1LG5vrULl6mfetvnqnILczbkJ0auW8QCwopU6xapqlGH2ZwIYhtRXj4gL8GBR/WZOZWODjyc6BtVxGjNxHDjzzM24RbFLZ3C6Rqv1j65dYQLbdxxKwexD8vqN1/9ooA1GG5il+g4rqaqT5mqD15bjSNYPT5zgRL0J874KPvboIfTTe+vWk3XxZAqoZ6MuDEDqFWIiq840aznYYY5tlmrTcE4+93BWK+pFkvAROo9xug2hTWKN3itOjExGQdGY97LKqNxK5jlGQEVy8RG6T5eKG8h9IQ3hTzhpgkkTvVUbV3VVIV6+Ck5iArnUUUiml3cNTts2BAEkYaOJscQoyY2aAxyRYw+3vuEFVnvN8YJns+OFIkLkYP+np99ZA89P3c2AMLalRFJi9aNB7pTXTYUvT9+MgkkCqjB94DSQ9o139mqf+ZXF7mKFRilhCewf+3kiQwxm9JJjvzFvvxDDKTyQsUR2E05KgUoXGWfEl9aHbtVZtjrgVdVFICahFEvZVlJRhiq/K61uT1Ee4SdY5+RjCHnnewlzTKZPdhg8YPRPjA2241NB+MgEoNEwEQ/naQ=="},{"Id":"TC87_PremMulti2prem2","Addr1":"15929 CHANDLER STREET","Addr2":"","City":"Worcester","StateProvince":"MA","PostalCode":"01604","Country":"US","Service":[{"Id":"SC_TC87_PremMulti2prem2_electric","Type":"electric_total","Description":"electric total","ActiveDate":null,"InActiveDate":null,"CommodityKey":"electric","BudgetBillingInd":null,"ServicePoints":[{"Id":"SP_M_CustMulti2prem2_elec","Meters":[{"Id":"M_CustMulti2prem2_elec","IsDeleted":null,"ReplacedMeterId":""}]}]},{"Id":"SC_TC87_PremMulti2prem2_gas","Type":"gas_total","Description":"gas total","ActiveDate":null,"InActiveDate":null,"CommodityKey":"gas","BudgetBillingInd":null,"ServicePoints":[{"Id":"SP_M_CustMulti2prem2","Meters":[{"Id":"M_CustMulti2prem2","IsDeleted":null,"ReplacedMeterId":""}]}]}],"Commodities":"Electric & Gas","Selected":false,"Url":"Page?enc=o7FI+Wz+RYUjbcnOjKjEqLry5lf7U302ozMzaWuUsrYmL3er7HjaT/22LTj5iDWREVP770SG847oIxx5eiqLGvW1LG5vrULl6mfetvnqnILczbkJ0auW8QCwopU6xapqlGH2ZwIYhtRXj4gL8GBR/WZOZWODjyc6BtVxGjNxHDjzzM24RbFLZ3C6Rqv1j65dYQLbdxxKwexD8vqN1/9ooA1GG5il+g4rqaqT5mqD15bjSNYPT5zgRL0J874KPvboIfTTe+vWk3XxZAqoZ6MuDEDqFWIiq840aznYYY5tlmpA9jBHJR0B3HEKCACXo2UVMdUWbrFKmdPqB0oqJYwPPvD20ryVYMtxLaQhW3fXqlVQqz5A3W5/5hUMCBS4rsEzqzWz9YRxOxoaFENFsmRWbLu9ZVQaVO8GFUkfPo++qMdXeKxy9W8zcw0+PPxDirEtTTcxedGCNUh2j6MMwl8R6/GmqDGTQx8giFt1Mr4rhuBQMzSkzIoq0YZyQotWIas0vLvXZsUSpkdP2g+p7lTUKynWmreTzIEwLMYG9myrbMlRm2sKiYrKErzuvEZIbIdwsDHAMXGNo6LxiqhtLSzgMQC5EgIEFujgK3bsZMoWs10HJwAEMMxLMbOTYouE/oVMQMvWW5hOqqZGPEzA4Fkq2m+breOhdh+Hxp6cWuyeR5o="}]}],"NoAccounts":false,"ShowSinglePremise":true,"Title":null,"IntroText":null,"SubTitle":null,"ShowTitle":null,"ShowIntro":null,"ShowSubTitle":null,"Footer":null,"FooterLink":"","FooterLinkText":null,"FooterLinkType":null,"ShowFooterText":null,"ShowFooterLink":null}');

//            app.result = json;
//            fixture.detectChanges();
//            //expect(element.textContent).toContain('expand');
//            expect(true).toBe(true);
//            //const linkToggle = fixture.nativeElement.querySelectorAll("span");
//            //expect(linkToggle[1].className).toContain('icon-toggle-down');
//        }));

//        xit('should get generic error message when no account show single premise false', async(() => {
//            const fixture = TestBed.createComponent(PremiseSelect);
//            const app = fixture.componentInstance;
//            const element = fixture.nativeElement;
//            app.enableLoader = false;
//            var json = JSON.parse('{"AccountNumberLabel":"Account #", "LoadingMessage":"Loading account information...", "ExpandLabel":"expand", "CollapseLabel":"collapse", "FirstName":null, "LastName":null, "CustomerId":null, "AccountId":null, "AccountList":null, "NoAccounts":true, "ShowSinglePremise":false, "Title":null, "IntroText":null, "SubTitle":null, "ShowTitle":null, "ShowIntro":null, "ShowSubTitle":null, "Footer":null, "FooterLink":"", "FooterLinkText":null, "FooterLinkType":null, "ShowFooterText":null, "ShowFooterLink":null }');

//            app.result = json;
//            fixture.detectChanges();
//            //expect(element.textContent).toContain('expand');
//            expect(true).toBe(true);
//            //const linkToggle = fixture.nativeElement.querySelectorAll("span");
//            //expect(linkToggle[1].className).toContain('icon-toggle-down');
//        }));
//   });