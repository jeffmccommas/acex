"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var platform_browser_1 = require("@angular/platform-browser");
var http_1 = require("@angular/http");
var testing_2 = require("@angular/platform-browser-dynamic/testing");
var quicklinks_1 = require("../../QuickLinks/quicklinks");
var quicklinks_service_1 = require("../../QuickLinks/quicklinks.service");
var app_service_1 = require("../../app.service");
var loader_1 = require("../../Loader/loader");
var pipe_1 = require("../../Helper/pipe");
var MockAppService = (function (_super) {
    __extends(MockAppService, _super);
    function MockAppService() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.params = [{ tabKey: "tab.billstatement", parameters: "WWJVR1lFTjFPaE9BTnRiMXpqa2Z5L2hxL2NJK3JRNEhRbExJZ1pmcVpMN09kZ3NGOFpLcXpBdzNlcU8wUHRNcVNFVzJDaU5hOWlPb2I3MVhjOHJFVEJaMGIwZ3A1TjltY3pzdFBTSW44azQxak1VRW8vS1VISE92UU1UODlvbXNsRDhaeHJqM0NTQTVDSVlDRWtNcWlSRnZ0VHF1Q3ZHaTBHUVRjWjE5cnF0MVdBaXN1Z3FXQVhxakNLREVnd0VBdFR4S3RBQURPUzFHS0ZBT2M4Ty92QW42UEswSWNmUk1tUTU4MElLRVZoQW5JRTJVSTJFOEIzVGpCUjQyNk9idCtOTTRwcFU0NmhjcGJ3WTBJVWlsMXB6WXhvS1NnNzcrSU51SG5tSi9XMmJTWUhieU9HcWo1N2lxRGZtK0x3aC9VV1JpcSszWk1YQWF5Tno1WFpWOGR5VnV0ZzRsMWw3OWVDWURCOXRhdFp6SXg2WU1PbWkyYmkzWU5GbHhNZUgzR3JsQ3FHSlhXZWZWWWVMVDFNU3V3ZGZkMzlLVmtVZ0ZxWjl6Q0dtUW84ekhXY0lxTDN2OUFUeUlHaVBKbjMwekFkMWQrVGRlOGtLcWNhL1NRTVdlZmxJQkloS3BGMmRpOVJ3YzN3R0RlS0pxZU5HbnJWOFhQVkdrL1FweUpoa2lGZWUvQkN0NGVIWHo2b1RHMFYyU3l3RWVUWVBqeHRiZXlKRVp3bFhOT3czTFVtTCtkdi9VQzhaUUxTQ3J2bzNW", errorMessage: "" }];
        _this.deepLinkVariables = [{ deepLinkWidget: "", deepLinkWidgetTab: "", deepLinkWidgetSection: "" }];
        return _this;
    }
    MockAppService.prototype.getParams = function () {
        return this.params;
    };
    MockAppService.prototype.getDeepLinkVariables = function () {
        return this.deepLinkVariables;
    };
    return MockAppService;
}(app_service_1.AppService));
var MockQLinkService = (function (_super) {
    __extends(MockQLinkService, _super);
    function MockQLinkService() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.unit = "unittest";
        return _this;
    }
    MockQLinkService.prototype.getQuickLinks = function () {
        return this.unit;
    };
    return MockQLinkService;
}(quicklinks_service_1.QuickLinksService));
describe('QuickLinks DOM Tests', function () {
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.resetTestEnvironment();
        testing_1.TestBed.initTestEnvironment(testing_2.BrowserDynamicTestingModule, testing_2.platformBrowserDynamicTesting());
        core_1.ElementRef;
        testing_1.TestBed.configureTestingModule({
            imports: [platform_browser_1.BrowserModule, forms_1.FormsModule, http_1.HttpModule],
            declarations: [quicklinks_1.QuickLinks, loader_1.Loader, pipe_1.ObjToArr]
        })
            .overrideComponent(quicklinks_1.QuickLinks, {
            remove: { providers: [app_service_1.AppService, quicklinks_service_1.QuickLinksService] },
            add: {
                providers: [
                    { provide: app_service_1.AppService, useClass: MockAppService },
                    { provide: quicklinks_service_1.QuickLinksService, useClass: MockQLinkService }
                ]
            }
        })
            .compileComponents();
    }));
    xit('should get QuickLinks', testing_1.async(function () {
        var fixture = testing_1.TestBed.createComponent(quicklinks_1.QuickLinks);
        var app = fixture.componentInstance;
        var element = fixture.nativeElement;
        app.enableLoader = false;
        var json = JSON.parse('{"Footer": "", "FooterLink": "", "FooterLinkText": "", "IntroText":"Quick Links", "LinkList": [{ "ActionPriority": 100, "Description": "","Key":"quicklink.paybill","Url":"www.aclara.com" }], "Title": "Quick Links"  }');
        app.result = json;
        fixture.detectChanges();
        expect(element.textContent).toContain('Quick Links');
    }));
});
//# sourceMappingURL=quicklinks.dom.spec.js.map