﻿import { async, TestBed } from "@angular/core/testing";
import { ElementRef } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { HttpModule } from "@angular/http";
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from "@angular/platform-browser-dynamic/testing";
import {QuickLinks} from "../../QuickLinks/quicklinks";
import { QuickLinksService } from "../../QuickLinks/quicklinks.service";
import { AppService, IAppParams, IDeepLinkVariables } from "../../app.service";
import { Loader } from "../../Loader/loader";
import { ObjToArr } from "../../Helper/pipe";

//mock quicklinks and app services 
class MockAppService extends AppService {
    public params: Array<IAppParams> = [{ tabKey: "tab.billstatement", parameters: "WWJVR1lFTjFPaE9BTnRiMXpqa2Z5L2hxL2NJK3JRNEhRbExJZ1pmcVpMN09kZ3NGOFpLcXpBdzNlcU8wUHRNcVNFVzJDaU5hOWlPb2I3MVhjOHJFVEJaMGIwZ3A1TjltY3pzdFBTSW44azQxak1VRW8vS1VISE92UU1UODlvbXNsRDhaeHJqM0NTQTVDSVlDRWtNcWlSRnZ0VHF1Q3ZHaTBHUVRjWjE5cnF0MVdBaXN1Z3FXQVhxakNLREVnd0VBdFR4S3RBQURPUzFHS0ZBT2M4Ty92QW42UEswSWNmUk1tUTU4MElLRVZoQW5JRTJVSTJFOEIzVGpCUjQyNk9idCtOTTRwcFU0NmhjcGJ3WTBJVWlsMXB6WXhvS1NnNzcrSU51SG5tSi9XMmJTWUhieU9HcWo1N2lxRGZtK0x3aC9VV1JpcSszWk1YQWF5Tno1WFpWOGR5VnV0ZzRsMWw3OWVDWURCOXRhdFp6SXg2WU1PbWkyYmkzWU5GbHhNZUgzR3JsQ3FHSlhXZWZWWWVMVDFNU3V3ZGZkMzlLVmtVZ0ZxWjl6Q0dtUW84ekhXY0lxTDN2OUFUeUlHaVBKbjMwekFkMWQrVGRlOGtLcWNhL1NRTVdlZmxJQkloS3BGMmRpOVJ3YzN3R0RlS0pxZU5HbnJWOFhQVkdrL1FweUpoa2lGZWUvQkN0NGVIWHo2b1RHMFYyU3l3RWVUWVBqeHRiZXlKRVp3bFhOT3czTFVtTCtkdi9VQzhaUUxTQ3J2bzNW", errorMessage: "" }];
    public deepLinkVariables: Array<IDeepLinkVariables> = [{ deepLinkWidget: "", deepLinkWidgetTab: "", deepLinkWidgetSection: "" }];

    getParams(): Array<IAppParams> {
        return this.params;
    }
    getDeepLinkVariables(): Array<IDeepLinkVariables> {
        return this.deepLinkVariables;
    }
}

class MockQLinkService extends QuickLinksService {
    public res: any;
    public unit: any = "unittest";
    getQuickLinks() {
        return this.unit;
    }
}

describe('QuickLinks DOM Tests',() => {
    beforeEach(async(() => {
        TestBed.resetTestEnvironment();
        TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting());
        ElementRef;
        TestBed.configureTestingModule({
                imports: [BrowserModule, FormsModule, HttpModule],
                declarations: [QuickLinks, Loader, ObjToArr]
            })
            .overrideComponent(QuickLinks,
            {
                remove: { providers: [AppService, QuickLinksService] },
                add: {
                    providers: [
                        { provide: AppService, useClass: MockAppService },
                        { provide: QuickLinksService, useClass: MockQLinkService }
                    ]
                }           
		 })
		 .compileComponents();
    }));


    xit('should get QuickLinks', async(() => {
            const fixture = TestBed.createComponent(QuickLinks);
            const app = fixture.componentInstance;
            const element = fixture.nativeElement;
            app.enableLoader = false;
            var json =  JSON.parse('{"Footer": "", "FooterLink": "", "FooterLinkText": "", "IntroText":"Quick Links", "LinkList": [{ "ActionPriority": 100, "Description": "","Key":"quicklink.paybill","Url":"www.aclara.com" }], "Title": "Quick Links"  }');
            app.result = json;
            fixture.detectChanges();
            //expect(true).toBe(true);
            expect(element.textContent).toContain('Quick Links');
    }));
});