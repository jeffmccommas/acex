﻿import { inject, async, TestBed } from "@angular/core/testing";
import { Http, HttpModule, XHRBackend, Response, ResponseOptions } from "@angular/http";
import { MockBackend, MockConnection } from "@angular/http/testing";
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { QuickLinksService } from "../../QuickLinks/quicklinks.service";

describe('TestProfileService -- Mock', () => {
    beforeEach(async(() => {
        TestBed.resetTestEnvironment();

        TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting());
        TestBed.configureTestingModule({
            imports: [HttpModule],
            providers: [
                QuickLinksService,
                { provide: XHRBackend, useClass: MockBackend }
            ]
        }).compileComponents();;
    }));

    it('can instantiate service when inject service',
        inject([QuickLinksService], (service: QuickLinksService) => {
            expect(service instanceof QuickLinksService).toBe(true);
        }));

    it('can provide the mockBackend as XHRBackend',
        inject([XHRBackend], (backend: MockBackend) => {
            expect(backend).not.toBeNull('backend should be provided');
        }));


    let backend: MockBackend;
    let service: QuickLinksService;
    let response: Response;

    beforeEach(inject([Http, XHRBackend], (http: Http, be: MockBackend) => {
        backend = be;
        service = new QuickLinksService(http);
        let options = new ResponseOptions({
            body: [
                {
                    id: "QA87Billing",
                    contentRendered: "<p><b>Test</b></p>",
                    contentMarkdown: "*Hi there*"
                }
            ]
        });
        response = new Response(options);
    }));

    it('should get quick links', async(inject([], () => {
        backend.connections.subscribe((c: MockConnection) => c.mockRespond(response));

        service.getQuickLinks('', '').subscribe((blogs) => {
            expect(blogs.length).toBe(1);
            expect(blogs[0].id).toBe("QA87Billing");
            expect(blogs[0].contentRendered).toBe("<p><b>Test</b></p>");
        });
    })));

});