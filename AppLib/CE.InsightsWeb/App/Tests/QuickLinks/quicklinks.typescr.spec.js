"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var quicklinks_1 = require("../../QuickLinks/quicklinks");
var quicklinks_service_1 = require("../../QuickLinks/quicklinks.service");
var app_service_1 = require("../../app.service");
var MockElementRef = (function () {
    function MockElementRef() {
        this.nativeElement = {};
    }
    return MockElementRef;
}());
describe('Quicklinks Typescript Tests', function () {
    it('Quicklinks loadlinks().  Component main entry point.', function () {
        var http;
        var appService = new app_service_1.AppService(http);
        var qlService = new quicklinks_service_1.QuickLinksService(http);
        var elementRef = new MockElementRef();
        appService.params = [{ tabKey: "tab.billstatement", parameters: "YbUGYEN1OhOANtb1zjkfy/hq/cI+rQ4HQlLIgZfqZL7OdgsF8ZKqzAw3eqO0PtMqSEW2CiNa9iOob71Xc8rETBZ0b0gp5N9mczstPSIn8k41jMUEo/KUHHOvQMT89omslD8Zxrj3CSA5CIYCEkMqiRFvtTquCvGi0GQTcZ19rqt1WAisugqWAXqjCKDEgwEAtTxKtAADOS1GKFAOc8O/vAn6PK0IcfRMmQ580IKEVhAnIE2UI2E8B3TjBR426Obt+NM4ppU46hcpbwY0IUil1ojq/lN/d8AzTriNnHjy3eVb2OyYDuFYOFopUUpX/v1qUrVgV7VD96UW2zEGS5zRb4N0/RHYCSwUE3qgwkzyOjUaOX3L9Jrwk9e4tNouzbEbjbXPjTwDcqbqUBk/Wn5eik5/64JISCsuqUNSz5GbjMN4F4WYoFT37PtjCq7KsV16YccmF3NObF9MfU45pgPyRI8Qas6g2fG/XdChanb7sRPGDj0C0do7/86T3PyOYJoV4v3+afpi7+iSGcfnYR7bhg==", errorMessage: "" }];
        qlService.res = JSON.parse('{ "Footer": "", "FooterLink": "", "FooterLinkText": "", "IntroText": "", "LinkList": [{ "ActionPriority": 100, "Description": "","Key":"quicklink.paybill","Url":"www.aclara.com" }], "Title": "Quick Links"  }');
        var quicklink = new quicklinks_1.QuickLinks(qlService, appService, elementRef);
        quicklink.fromUTest = true;
        quicklink.ngOnInit();
        expect(quicklink.enableLoader).toBe(false);
    });
    it('Quicklinks postevents().  Post event when links clicked.', function () {
        var http;
        var appService = new app_service_1.AppService(http);
        var qlService = new quicklinks_service_1.QuickLinksService(http);
        var elementRef = new MockElementRef();
        appService.params = [{ tabKey: "tab.billstatement", parameters: "YbUGYEN1OhOANtb1zjkfy/hq/cI+rQ4HQlLIgZfqZL7OdgsF8ZKqzAw3eqO0PtMqSEW2CiNa9iOob71Xc8rETBZ0b0gp5N9mczstPSIn8k41jMUEo/KUHHOvQMT89omslD8Zxrj3CSA5CIYCEkMqiRFvtTquCvGi0GQTcZ19rqt1WAisugqWAXqjCKDEgwEAtTxKtAADOS1GKFAOc8O/vAn6PK0IcfRMmQ580IKEVhAnIE2UI2E8B3TjBR426Obt+NM4ppU46hcpbwY0IUil1ojq/lN/d8AzTriNnHjy3eVb2OyYDuFYOFopUUpX/v1qUrVgV7VD96UW2zEGS5zRb4N0/RHYCSwUE3qgwkzyOjUaOX3L9Jrwk9e4tNouzbEbjbXPjTwDcqbqUBk/Wn5eik5/64JISCsuqUNSz5GbjMN4F4WYoFT37PtjCq7KsV16YccmF3NObF9MfU45pgPyRI8Qas6g2fG/XdChanb7sRPGDj0C0do7/86T3PyOYJoV4v3+afpi7+iSGcfnYR7bhg==", errorMessage: "" }];
        qlService.res = JSON.parse('{ "Footer": "", "FooterLink": "", "FooterLinkText": "", "IntroText": "", "LinkList": [{ "ActionPriority": 100, "Description": "","Key":"quicklink.paybill","Url":"www.aclara.com" }], "Title": "Quick Links"  }');
        var quicklink = new quicklinks_1.QuickLinks(qlService, appService, elementRef);
        quicklink.fromUTest = true;
        quicklink.postEvents("", "");
        expect(quicklink.fromUTest).toBe(true);
    });
});
//# sourceMappingURL=quicklinks.typescr.spec.js.map