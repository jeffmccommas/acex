﻿import { ElementRef } from "@angular/core";
import { Http } from "@angular/http";
import {ReportList} from "../../ReportList/reportlist";
import {ReportListService} from "../../ReportList/reportlist.service";
import {AppService, IAppParams, IDeepLinkVariables} from "../../app.service";

//mock profile and app services
class MockAppService extends AppService {
    public params: Array<IAppParams> = [{ tabKey: "tab.reportcsr", parameters: "WWJVR1lFTjFPaE9BTnRiMXpqa2Z5L2hxL2NJK3JRNEhRbExJZ1pmcVpMN09kZ3NGOFpLcXpBdzNlcU8wUHRNcVNFVzJDaU5hOWlPb2I3MVhjOHJFVEJaMGIwZ3A1TjltY3pzdFBTSW44azQxak1VRW8vS1VISE92UU1UODlvbXNsRDhaeHJqM0NTQTVDSVlDRWtNcWlSRnZ0VHF1Q3ZHaTBHUVRjWjE5cnF0MVdBaXN1Z3FXQVhxakNLREVnd0VBdFR4S3RBQURPUzFHS0ZBT2M4Ty92QW42UEswSWNmUk1tUTU4MElLRVZoQW5JRTJVSTJFOEIzVGpCUjQyNk9idCtOTTRwcFU0NmhjcGJ3WTBJVWlsMXJNVFl5TlcvQmd2RUp4R2JMOEFXc00vTW1jb0VCMmVveWliSHg5bnpKVkNYaXZ2aHBVVmE0ZHFZbk01MTVQejE0Tml0VWhrR1FwQWVsY3FjcVJGOXpFYUlpaUgxL210NU9VZjZtMng4ZE9jVkl0Y1lOdGxhcHB3dWZUMHA1Q3ZxYnNQWk1uKzEzY1dJMEN1dU5VWTk2Z2JHdmFaWGlkdjJYZkR3N3dpZ2pEcXlHanRIRXdsNTRhbFhnWVByb254ZU9Yc3hHeVdjNlpQeGV5OXVjb092ZW5tYU9naWtLb3NtYVpjYjVlckMvUGU5RXhQZnJkN3ROUkF4K2tmYlEzZWgrWUxRcDN3VHh4UWwzRkNxd2o0YVlRRm15bzhlbVJYRGxDOHE0MU9Icjdh", errorMessage: "" }];
    public deepLinkVariables: Array<IDeepLinkVariables> = [{ deepLinkWidget: "", deepLinkWidgetTab: "", deepLinkWidgetSection: "" }];

    getParams(): Array<IAppParams> {
        return this.params;
    }
    getDeepLinkVariables(): Array<IDeepLinkVariables> {
        return this.deepLinkVariables;
    }
}

class MockReportListService extends ReportListService {
    public res: any;
    loadReports() {
        return "unittest";
    }
    }
describe('ReportList Typescript Tests', () => {

    it('ReportList disableLoader().  Component main entry point.', () => {
        let http: Http;
        let sub: ReportList;
        let appService: AppService = new AppService(http);
        let subService: ReportListService= new ReportListService(http);
        let elementRef: ElementRef;
        appService.params = [{ tabKey: "tab.reportcsr", parameters: "WWJVR1lFTjFPaE9BTnRiMXpqa2Z5L2hxL2NJK3JRNEhRbExJZ1pmcVpMN09kZ3NGOFpLcXpBdzNlcU8wUHRNcVNFVzJDaU5hOWlPb2I3MVhjOHJFVEJaMGIwZ3A1TjltY3pzdFBTSW44azQxak1VRW8vS1VISE92UU1UODlvbXNsRDhaeHJqM0NTQTVDSVlDRWtNcWlSRnZ0VHF1Q3ZHaTBHUVRjWjE5cnF0MVdBaXN1Z3FXQVhxakNLREVnd0VBdFR4S3RBQURPUzFHS0ZBT2M4Ty92QW42UEswSWNmUk1tUTU4MElLRVZoQW5JRTJVSTJFOEIzVGpCUjQyNk9idCtOTTRwcFU0NmhjcGJ3WTBJVWlsMXJNVFl5TlcvQmd2RUp4R2JMOEFXc00vTW1jb0VCMmVveWliSHg5bnpKVkNYaXZ2aHBVVmE0ZHFZbk01MTVQejE0Tml0VWhrR1FwQWVsY3FjcVJGOXpFYUlpaUgxL210NU9VZjZtMng4ZE9jVkl0Y1lOdGxhcHB3dWZUMHA1Q3ZxYnNQWk1uKzEzY1dJMEN1dU5VWTk2Z2JHdmFaWGlkdjJYZkR3N3dpZ2pEcXlHanRIRXdsNTRhbFhnWVByb254ZU9Yc3hHeVdjNlpQeGV5OXVjb092ZW5tYU9naWtLb3NtYVpjYjVlckMvUGU5RXhQZnJkN3ROUkF4K2tmYlEzZWgrWUxRcDN3VHh4UWwzRkNxd2o0YVlRRm15bzhlbVJYRGxDOHE0MU9Icjdh", errorMessage: "" }];
        appService.deepLinkVariables = [{ deepLinkWidget: "", deepLinkWidgetTab: "", deepLinkWidgetSection: "" }];
        //subService.res = JSON.parse('{"Title":"Notification Settings","IntroText":"Alert options.","SubTitle":"Bill to Date Program","Footer":"Sample footer","FooterLinkText":"Next steps","ShowTitle":"true","ShowIntro":"true","ShowSubTitle":"true","FooterLink":"https://www.aclara.com","ShowFooterLink":"true","ShowFooterText":"true","SaveButtonLabel":"Save","SaveSuccess":"Notification settings saved.","SaveFailed":"Settings not saved.","NoData":null,"NoSubscription":false,"NotCompleteMessage":"Please answer this question","Disclaimer":"  ","CancelButtonLabel":"","Insights":"program1.billtodate,program1.accountprojectedcost,program1.serviceleveltieredthresholdapproaching,program1.servicelevelcostthreshold,program1.servicelevelusagethreshold","Channels":"email,sms","ChannelsText":"Email,Text","ServiceCount":3,"Notifications":[{"Name":"program1.billtodate","Title":"Send me a daily alert showing my bill-to-date.","Description":"Send me a daily alert showing my bill-to-date.","ImageUrl":null,"ImageTitle":null,"Icon":null,"Link":"","LinkText":"","LinkType":"","Threshold":0,"IsSelected":true,"Level":"account","AccountId":"1218337093","PremiseId":null,"ServiceId":null,"AllowedChannels":"file,email,sms,emailandsms","ThresholdType":null,"ThresholdMin":null,"ThresholdMax":null,"UomKey":null,"UomText":null,"CurrencySymbol":null,"Channel":"email","Channels":"email,sms,emailandsms","EmailSelected":true,"SmsSelected":false,"EmailEnabled":true,"SmsEnabled":true,"ThresholdDisabled":false,"CommodityKey":null,"ServiceLabel":" Usage (Service )","SingleServiceLabel":" Usage","InvalidRange":false,"InvalidRequired":false,"RangeErrorMessage":null,"ShowLabel":true},{"Name":"program1.accountprojectedcost","Title":"Send me a daily alert showing my projected bill amount.","Description":"Send me a daily alert showing my projected bill amount.","ImageUrl":null,"ImageTitle":null,"Icon":null,"Link":"","LinkText":"","LinkType":"","Threshold":0,"IsSelected":false,"Level":"account","AccountId":"1218337093","PremiseId":null,"ServiceId":null,"AllowedChannels":"file,email,sms,emailandsms","ThresholdType":null,"ThresholdMin":null,"ThresholdMax":null,"UomKey":null,"UomText":null,"CurrencySymbol":null,"Channel":"","Channels":"email,sms,emailandsms","EmailSelected":false,"SmsSelected":false,"EmailEnabled":true,"SmsEnabled":true,"ThresholdDisabled":false,"CommodityKey":null,"ServiceLabel":" Usage (Service )","SingleServiceLabel":" Usage","InvalidRange":false,"InvalidRequired":false,"RangeErrorMessage":null,"ShowLabel":false},{"Name":"program1.servicelevelcostthreshold","Title":"Notify me if my costs exceed this amount.","Description":"Cost","ImageUrl":null,"ImageTitle":null,"Icon":null,"Link":"","LinkText":"","LinkType":"","Threshold":25,"IsSelected":false,"Level":"service","AccountId":"1218337093","PremiseId":"1218337000","ServiceId":"SP_189354704","AllowedChannels":"file,email,sms,emailandsms","ThresholdType":"cost","ThresholdMin":1,"ThresholdMax":1000000,"UomKey":null,"UomText":null,"CurrencySymbol":"$","Channel":"","Channels":"email,sms,emailandsms","EmailSelected":false,"SmsSelected":false,"EmailEnabled":true,"SmsEnabled":true,"ThresholdDisabled":false,"CommodityKey":"electric","ServiceLabel":"Electric Usage (Service SP_189354704)","SingleServiceLabel":"Electric Usage","InvalidRange":false,"InvalidRequired":false,"RangeErrorMessage":"Please enter a value between 1.0 – 1000000.0.","ShowLabel":true},{"Name":"program1.servicelevelusagethreshold","Title":"Notify me if my usage exceeds this quantity ","Description":"Notify me if my usage exceeds this quantity.","ImageUrl":null,"ImageTitle":null,"Icon":null,"Link":"","LinkText":"","LinkType":"","Threshold":500,"IsSelected":false,"Level":"service","AccountId":"1218337093","PremiseId":"1218337000","ServiceId":"SP_189354704","AllowedChannels":"file,email,sms,emailandsms","ThresholdType":"usage","ThresholdMin":0.1,"ThresholdMax":1000000,"UomKey":"kwh","UomText":"kWh","CurrencySymbol":null,"Channel":"","Channels":"email,sms,emailandsms","EmailSelected":false,"SmsSelected":false,"EmailEnabled":true,"SmsEnabled":true,"ThresholdDisabled":false,"CommodityKey":"electric","ServiceLabel":"Electric Usage (Service SP_189354704)","SingleServiceLabel":"Electric Usage","InvalidRange":false,"InvalidRequired":false,"RangeErrorMessage":"Please enter a value between 0.1 – 1000000.0.","ShowLabel":false},{"Name":"program1.serviceleveltieredthresholdapproaching","Title":"Notify me when I am approaching a tier pricing limit.","Description":"This notification will let you know when your total usage to-date for the bill period is about to cross over a tiered pricing boundary. This will help you manage your costs because you will know when your usage is triggering higher prices.","ImageUrl":null,"ImageTitle":null,"Icon":null,"Link":"","LinkText":"","LinkType":"","Threshold":20,"IsSelected":false,"Level":"service","AccountId":"1218337093","PremiseId":"1218337000","ServiceId":"SP_189354704","AllowedChannels":"file,email,sms,emailandsms","ThresholdType":"percent","ThresholdMin":1,"ThresholdMax":100,"UomKey":null,"UomText":null,"CurrencySymbol":null,"Channel":"","Channels":"email,sms,emailandsms","EmailSelected":false,"SmsSelected":false,"EmailEnabled":true,"SmsEnabled":true,"ThresholdDisabled":false,"CommodityKey":"electric","ServiceLabel":"Electric Usage (Service SP_189354704)","SingleServiceLabel":"Electric Usage","InvalidRange":false,"InvalidRequired":false,"RangeErrorMessage":null,"ShowLabel":false},{"Name":"program1.servicelevelcostthreshold","Title":"Notify me if my costs exceed this amount.","Description":"Cost","ImageUrl":null,"ImageTitle":null,"Icon":null,"Link":"","LinkText":"","LinkType":"","Threshold":25,"IsSelected":true,"Level":"service","AccountId":"1218337093","PremiseId":"1218337000","ServiceId":"SP_M_1218337000_gas","AllowedChannels":"file,email,sms,emailandsms","ThresholdType":"cost","ThresholdMin":1,"ThresholdMax":1000000,"UomKey":null,"UomText":null,"CurrencySymbol":"$","Channel":"email","Channels":"email,sms,emailandsms","EmailSelected":true,"SmsSelected":false,"EmailEnabled":true,"SmsEnabled":true,"ThresholdDisabled":false,"CommodityKey":"electric","ServiceLabel":"Electric Usage (Service SP_M_1218337000_gas)","SingleServiceLabel":"Electric Usage","InvalidRange":false,"InvalidRequired":false,"RangeErrorMessage":"Please enter a value between 1.0 – 1000000.0.","ShowLabel":true},{"Name":"program1.servicelevelusagethreshold","Title":"Notify me if my usage exceeds this quantity ","Description":"Notify me if my usage exceeds this quantity.","ImageUrl":null,"ImageTitle":null,"Icon":null,"Link":"","LinkText":"","LinkType":"","Threshold":500,"IsSelected":false,"Level":"service","AccountId":"1218337093","PremiseId":"1218337000","ServiceId":"SP_M_1218337000_gas","AllowedChannels":"file,email,sms,emailandsms","ThresholdType":"usage","ThresholdMin":0.1,"ThresholdMax":1000000,"UomKey":"ccf","UomText":"CCF","CurrencySymbol":null,"Channel":"","Channels":"email,sms,emailandsms","EmailSelected":false,"SmsSelected":false,"EmailEnabled":true,"SmsEnabled":true,"ThresholdDisabled":false,"CommodityKey":"electric","ServiceLabel":"Electric Usage (Service SP_M_1218337000_gas)","SingleServiceLabel":"Electric Usage","InvalidRange":false,"InvalidRequired":false,"RangeErrorMessage":"Please enter a value between 0.1 – 1000000.0.","ShowLabel":false},{"Name":"program1.serviceleveltieredthresholdapproaching","Title":"Notify me when I am approaching a tier pricing limit.","Description":"This notification will let you know when your total usage to-date for the bill period is about to cross over a tiered pricing boundary. This will help you manage your costs because you will know when your usage is triggering higher prices.","ImageUrl":null,"ImageTitle":null,"Icon":null,"Link":"","LinkText":"","LinkType":"","Threshold":20,"IsSelected":false,"Level":"service","AccountId":"1218337093","PremiseId":"1218337000","ServiceId":"SP_M_1218337000_gas","AllowedChannels":"file,email,sms,emailandsms","ThresholdType":"percent","ThresholdMin":1,"ThresholdMax":100,"UomKey":null,"UomText":null,"CurrencySymbol":null,"Channel":"","Channels":"email,sms,emailandsms","EmailSelected":false,"SmsSelected":false,"EmailEnabled":true,"SmsEnabled":true,"ThresholdDisabled":false,"CommodityKey":"electric","ServiceLabel":"Electric Usage (Service SP_M_1218337000_gas)","SingleServiceLabel":"Electric Usage","InvalidRange":false,"InvalidRequired":false,"RangeErrorMessage":null,"ShowLabel":false}]}');
        
        sub = new ReportList(subService, appService, elementRef);
        sub.fromUnitTs = "unittest";
        sub.disableLoader();
        let loader = sub.enableLoader;
        expect(loader).toBe(false);
        sub.loadReports();
        expect(sub.enableLoader).toBe(false);
    });

    it('ReportList downloadReport().  Download single Report.', () => {
        let http: Http;
        const appService: AppService = new AppService(http);
        const reportlistService: MockReportListService = new MockReportListService(http);
        let elementRef: ElementRef;
        appService.params = [{ tabKey: "tab.reportcsr", parameters: "WWJVR1lFTjFPaE9BTnRiMXpqa2Z5L2hxL2NJK3JRNEhRbExJZ1pmcVpMN09kZ3NGOFpLcXpBdzNlcU8wUHRNcVNFVzJDaU5hOWlPb2I3MVhjOHJFVEJaMGIwZ3A1TjltY3pzdFBTSW44azQxak1VRW8vS1VISE92UU1UODlvbXNsRDhaeHJqM0NTQTVDSVlDRWtNcWlSRnZ0VHF1Q3ZHaTBHUVRjWjE5cnF0MVdBaXN1Z3FXQVhxakNLREVnd0VBdFR4S3RBQURPUzFHS0ZBT2M4Ty92QW42UEswSWNmUk1tUTU4MElLRVZoQW5JRTJVSTJFOEIzVGpCUjQyNk9idCtOTTRwcFU0NmhjcGJ3WTBJVWlsMXJNVFl5TlcvQmd2RUp4R2JMOEFXc00vTW1jb0VCMmVveWliSHg5bnpKVkNYaXZ2aHBVVmE0ZHFZbk01MTVQejE0Tml0VWhrR1FwQWVsY3FjcVJGOXpFYUlpaUgxL210NU9VZjZtMng4ZE9jVkl0Y1lOdGxhcHB3dWZUMHA1Q3ZxYnNQWk1uKzEzY1dJMEN1dU5VWTk2Z2JHdmFaWGlkdjJYZkR3N3dpZ2pEcXlHanRIRXdsNTRhbFhnWVByb254ZU9Yc3hHeVdjNlpQeGV5OXVjb092ZW5tYU9naWtLb3NtYVpjYjVlckMvUGU5RXhQZnJkN3ROUkF4K2tmYlEzZWgrWUxRcDN3VHh4UWwzRkNxd2o0YVlRRm15bzhlbVJYRGxDOHE0MU9Icjdh", errorMessage: "" }];
        appService.deepLinkVariables = [{ deepLinkWidget: "", deepLinkWidgetTab: "", deepLinkWidgetSection: "" }];
        //greenButtonService.res = JSON.parse('{"Title":"Green Button Download","SubTitle":"Download your Smart Meter Data","IntroText":"The Green Button download gives utility customers easy and secure access to their energy usage information.","FromDate":"From:","ToDate":"To:","DownloadAmiTitle":"","DownloadBillsTitle":"","DownloadUsage":"Download my usage for specific days","DownloadBills":"Download my bills","DownloadingMessage":"","Footer":"Sample footer","FooterLinkText":"Next steps","NoBillData":"No bill data is available.","NoUsageData":"No usage data is available.","DateError":"Please enter an end date that is after the start date.","ShowTitle":"true","ShowIntro":"true","ShowSubTitle":"false","FooterLink":"https://www.aclara.com","ShowFooterLink":"false","ShowFooterText":"false","NoBill":false,"NoAmi":false,"Response":null}');
        const gb = new ReportList(reportlistService, appService, elementRef);
        gb.fromUnitTs = "unittest";
        gb.downloadReport(0,"","user");
        expect(gb.noReport).toBe(true);
    });

    it('ReportList deleteReport().  Delete Report.', () => {
        let http: Http;
        const appService: AppService = new AppService(http);
        const reportlistService: MockReportListService = new MockReportListService(http);
        let elementRef: ElementRef;
        appService.params = [{ tabKey: "tab.reportcsr", parameters: "WWJVR1lFTjFPaE9BTnRiMXpqa2Z5L2hxL2NJK3JRNEhRbExJZ1pmcVpMN09kZ3NGOFpLcXpBdzNlcU8wUHRNcVNFVzJDaU5hOWlPb2I3MVhjOHJFVEJaMGIwZ3A1TjltY3pzdFBTSW44azQxak1VRW8vS1VISE92UU1UODlvbXNsRDhaeHJqM0NTQTVDSVlDRWtNcWlSRnZ0VHF1Q3ZHaTBHUVRjWjE5cnF0MVdBaXN1Z3FXQVhxakNLREVnd0VBdFR4S3RBQURPUzFHS0ZBT2M4Ty92QW42UEswSWNmUk1tUTU4MElLRVZoQW5JRTJVSTJFOEIzVGpCUjQyNk9idCtOTTRwcFU0NmhjcGJ3WTBJVWlsMXJNVFl5TlcvQmd2RUp4R2JMOEFXc00vTW1jb0VCMmVveWliSHg5bnpKVkNYaXZ2aHBVVmE0ZHFZbk01MTVQejE0Tml0VWhrR1FwQWVsY3FjcVJGOXpFYUlpaUgxL210NU9VZjZtMng4ZE9jVkl0Y1lOdGxhcHB3dWZUMHA1Q3ZxYnNQWk1uKzEzY1dJMEN1dU5VWTk2Z2JHdmFaWGlkdjJYZkR3N3dpZ2pEcXlHanRIRXdsNTRhbFhnWVByb254ZU9Yc3hHeVdjNlpQeGV5OXVjb092ZW5tYU9naWtLb3NtYVpjYjVlckMvUGU5RXhQZnJkN3ROUkF4K2tmYlEzZWgrWUxRcDN3VHh4UWwzRkNxd2o0YVlRRm15bzhlbVJYRGxDOHE0MU9Icjdh", errorMessage: "" }];
        appService.deepLinkVariables = [{ deepLinkWidget: "", deepLinkWidgetTab: "", deepLinkWidgetSection: "" }];
        const gb = new ReportList(reportlistService, appService, elementRef);
        gb.fromUnitTs = "unittest";
        gb.deleteReport(0,"user");
        expect(gb.deleteReportResult).toBe('unittest');
    });

    it('ReportList deleteReportValidation().  Delete Report Validation.', () => {
        let http: Http;
        const appService: AppService = new AppService(http);
        const reportlistService: MockReportListService = new MockReportListService(http);
        let elementRef: ElementRef;
        appService.params = [{ tabKey: "tab.reportcsr", parameters: "WWJVR1lFTjFPaE9BTnRiMXpqa2Z5L2hxL2NJK3JRNEhRbExJZ1pmcVpMN09kZ3NGOFpLcXpBdzNlcU8wUHRNcVNFVzJDaU5hOWlPb2I3MVhjOHJFVEJaMGIwZ3A1TjltY3pzdFBTSW44azQxak1VRW8vS1VISE92UU1UODlvbXNsRDhaeHJqM0NTQTVDSVlDRWtNcWlSRnZ0VHF1Q3ZHaTBHUVRjWjE5cnF0MVdBaXN1Z3FXQVhxakNLREVnd0VBdFR4S3RBQURPUzFHS0ZBT2M4Ty92QW42UEswSWNmUk1tUTU4MElLRVZoQW5JRTJVSTJFOEIzVGpCUjQyNk9idCtOTTRwcFU0NmhjcGJ3WTBJVWlsMXJNVFl5TlcvQmd2RUp4R2JMOEFXc00vTW1jb0VCMmVveWliSHg5bnpKVkNYaXZ2aHBVVmE0ZHFZbk01MTVQejE0Tml0VWhrR1FwQWVsY3FjcVJGOXpFYUlpaUgxL210NU9VZjZtMng4ZE9jVkl0Y1lOdGxhcHB3dWZUMHA1Q3ZxYnNQWk1uKzEzY1dJMEN1dU5VWTk2Z2JHdmFaWGlkdjJYZkR3N3dpZ2pEcXlHanRIRXdsNTRhbFhnWVByb254ZU9Yc3hHeVdjNlpQeGV5OXVjb092ZW5tYU9naWtLb3NtYVpjYjVlckMvUGU5RXhQZnJkN3ROUkF4K2tmYlEzZWgrWUxRcDN3VHh4UWwzRkNxd2o0YVlRRm15bzhlbVJYRGxDOHE0MU9Icjdh", errorMessage: "" }];
        appService.deepLinkVariables = [{ deepLinkWidget: "", deepLinkWidgetTab: "", deepLinkWidgetSection: "" }];
        const gb = new ReportList(reportlistService, appService, elementRef);
        gb.fromUnitTs = "unittest";
        gb.deleteReportResult = { Content: {} };
        gb.result = { ReportsUser: [{ Id: 0, ErrorStatus: true }, 1, 2], ReportsCSR: [{ Id: 0, ErrorStatus: true }, 1, 2] };
        gb.deleteReportValidation(0, "user");
        gb.deleteReportValidation(0, "");
        //with content
        gb.deleteReportResult = [{ Content: JSON.parse('{"Title":"Green Button Download","SubTitle":"Download your Smart Meter Data","IntroText":"The Green Button download gives utility customers easy and secure access to their energy usage information.","FromDate":"From:","ToDate":"To:","DownloadAmiTitle":"","DownloadBillsTitle":"","DownloadUsage":"Download my usage for specific days","DownloadBills":"Download my bills","DownloadingMessage":"","Footer":"Sample footer","FooterLinkText":"Next steps","NoBillData":"No bill data is available.","NoUsageData":"No usage data is available.","DateError":"Please enter an end date that is after the start date.","ShowTitle":"true","ShowIntro":"true","ShowSubTitle":"false","FooterLink":"https://www.aclara.com","ShowFooterLink":"false","ShowFooterText":"false","NoBill":false,"NoAmi":false,"Response":null}')}] ;
        gb.deleteReportValidation(0, "user");
        gb.deleteReportValidation(0, "user");
        expect(gb.deleteErrorOccurred).toBe(true);
    });

    it('ReportList parseReportList().  Parse Reportlist and return index of array.', () => {
        let http: Http;
        const appService: AppService = new AppService(http);
        const reportlistService: MockReportListService = new MockReportListService(http);
        let elementRef: ElementRef;
        let list: any = { Id: 1};
        appService.params = [{ tabKey: "tab.reportcsr", parameters: "WWJVR1lFTjFPaE9BTnRiMXpqa2Z5L2hxL2NJK3JRNEhRbExJZ1pmcVpMN09kZ3NGOFpLcXpBdzNlcU8wUHRNcVNFVzJDaU5hOWlPb2I3MVhjOHJFVEJaMGIwZ3A1TjltY3pzdFBTSW44azQxak1VRW8vS1VISE92UU1UODlvbXNsRDhaeHJqM0NTQTVDSVlDRWtNcWlSRnZ0VHF1Q3ZHaTBHUVRjWjE5cnF0MVdBaXN1Z3FXQVhxakNLREVnd0VBdFR4S3RBQURPUzFHS0ZBT2M4Ty92QW42UEswSWNmUk1tUTU4MElLRVZoQW5JRTJVSTJFOEIzVGpCUjQyNk9idCtOTTRwcFU0NmhjcGJ3WTBJVWlsMXJNVFl5TlcvQmd2RUp4R2JMOEFXc00vTW1jb0VCMmVveWliSHg5bnpKVkNYaXZ2aHBVVmE0ZHFZbk01MTVQejE0Tml0VWhrR1FwQWVsY3FjcVJGOXpFYUlpaUgxL210NU9VZjZtMng4ZE9jVkl0Y1lOdGxhcHB3dWZUMHA1Q3ZxYnNQWk1uKzEzY1dJMEN1dU5VWTk2Z2JHdmFaWGlkdjJYZkR3N3dpZ2pEcXlHanRIRXdsNTRhbFhnWVByb254ZU9Yc3hHeVdjNlpQeGV5OXVjb092ZW5tYU9naWtLb3NtYVpjYjVlckMvUGU5RXhQZnJkN3ROUkF4K2tmYlEzZWgrWUxRcDN3VHh4UWwzRkNxd2o0YVlRRm15bzhlbVJYRGxDOHE0MU9Icjdh", errorMessage: "" }];
        appService.deepLinkVariables = [{ deepLinkWidget: "", deepLinkWidgetTab: "", deepLinkWidgetSection: "" }];
        const gb = new ReportList(reportlistService, appService, elementRef);
        gb.fromUnitTs = "unittest";
        let res = gb.parseReportList(list, 0);
        expect(res).toBe(-1);
        list = [{Id:1}];
        res = gb.parseReportList(list, 1);
        expect(res).toBe(0);
    });
});
