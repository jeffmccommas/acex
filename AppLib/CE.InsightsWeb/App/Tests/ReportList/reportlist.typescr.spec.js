"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var reportlist_1 = require("../../ReportList/reportlist");
var reportlist_service_1 = require("../../ReportList/reportlist.service");
var app_service_1 = require("../../app.service");
var MockAppService = (function (_super) {
    __extends(MockAppService, _super);
    function MockAppService() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.params = [{ tabKey: "tab.reportcsr", parameters: "WWJVR1lFTjFPaE9BTnRiMXpqa2Z5L2hxL2NJK3JRNEhRbExJZ1pmcVpMN09kZ3NGOFpLcXpBdzNlcU8wUHRNcVNFVzJDaU5hOWlPb2I3MVhjOHJFVEJaMGIwZ3A1TjltY3pzdFBTSW44azQxak1VRW8vS1VISE92UU1UODlvbXNsRDhaeHJqM0NTQTVDSVlDRWtNcWlSRnZ0VHF1Q3ZHaTBHUVRjWjE5cnF0MVdBaXN1Z3FXQVhxakNLREVnd0VBdFR4S3RBQURPUzFHS0ZBT2M4Ty92QW42UEswSWNmUk1tUTU4MElLRVZoQW5JRTJVSTJFOEIzVGpCUjQyNk9idCtOTTRwcFU0NmhjcGJ3WTBJVWlsMXJNVFl5TlcvQmd2RUp4R2JMOEFXc00vTW1jb0VCMmVveWliSHg5bnpKVkNYaXZ2aHBVVmE0ZHFZbk01MTVQejE0Tml0VWhrR1FwQWVsY3FjcVJGOXpFYUlpaUgxL210NU9VZjZtMng4ZE9jVkl0Y1lOdGxhcHB3dWZUMHA1Q3ZxYnNQWk1uKzEzY1dJMEN1dU5VWTk2Z2JHdmFaWGlkdjJYZkR3N3dpZ2pEcXlHanRIRXdsNTRhbFhnWVByb254ZU9Yc3hHeVdjNlpQeGV5OXVjb092ZW5tYU9naWtLb3NtYVpjYjVlckMvUGU5RXhQZnJkN3ROUkF4K2tmYlEzZWgrWUxRcDN3VHh4UWwzRkNxd2o0YVlRRm15bzhlbVJYRGxDOHE0MU9Icjdh", errorMessage: "" }];
        _this.deepLinkVariables = [{ deepLinkWidget: "", deepLinkWidgetTab: "", deepLinkWidgetSection: "" }];
        return _this;
    }
    MockAppService.prototype.getParams = function () {
        return this.params;
    };
    MockAppService.prototype.getDeepLinkVariables = function () {
        return this.deepLinkVariables;
    };
    return MockAppService;
}(app_service_1.AppService));
var MockReportListService = (function (_super) {
    __extends(MockReportListService, _super);
    function MockReportListService() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    MockReportListService.prototype.loadReports = function () {
        return "unittest";
    };
    return MockReportListService;
}(reportlist_service_1.ReportListService));
describe('ReportList Typescript Tests', function () {
    it('ReportList disableLoader().  Component main entry point.', function () {
        var http;
        var sub;
        var appService = new app_service_1.AppService(http);
        var subService = new reportlist_service_1.ReportListService(http);
        var elementRef;
        appService.params = [{ tabKey: "tab.reportcsr", parameters: "WWJVR1lFTjFPaE9BTnRiMXpqa2Z5L2hxL2NJK3JRNEhRbExJZ1pmcVpMN09kZ3NGOFpLcXpBdzNlcU8wUHRNcVNFVzJDaU5hOWlPb2I3MVhjOHJFVEJaMGIwZ3A1TjltY3pzdFBTSW44azQxak1VRW8vS1VISE92UU1UODlvbXNsRDhaeHJqM0NTQTVDSVlDRWtNcWlSRnZ0VHF1Q3ZHaTBHUVRjWjE5cnF0MVdBaXN1Z3FXQVhxakNLREVnd0VBdFR4S3RBQURPUzFHS0ZBT2M4Ty92QW42UEswSWNmUk1tUTU4MElLRVZoQW5JRTJVSTJFOEIzVGpCUjQyNk9idCtOTTRwcFU0NmhjcGJ3WTBJVWlsMXJNVFl5TlcvQmd2RUp4R2JMOEFXc00vTW1jb0VCMmVveWliSHg5bnpKVkNYaXZ2aHBVVmE0ZHFZbk01MTVQejE0Tml0VWhrR1FwQWVsY3FjcVJGOXpFYUlpaUgxL210NU9VZjZtMng4ZE9jVkl0Y1lOdGxhcHB3dWZUMHA1Q3ZxYnNQWk1uKzEzY1dJMEN1dU5VWTk2Z2JHdmFaWGlkdjJYZkR3N3dpZ2pEcXlHanRIRXdsNTRhbFhnWVByb254ZU9Yc3hHeVdjNlpQeGV5OXVjb092ZW5tYU9naWtLb3NtYVpjYjVlckMvUGU5RXhQZnJkN3ROUkF4K2tmYlEzZWgrWUxRcDN3VHh4UWwzRkNxd2o0YVlRRm15bzhlbVJYRGxDOHE0MU9Icjdh", errorMessage: "" }];
        appService.deepLinkVariables = [{ deepLinkWidget: "", deepLinkWidgetTab: "", deepLinkWidgetSection: "" }];
        sub = new reportlist_1.ReportList(subService, appService, elementRef);
        sub.fromUnitTs = "unittest";
        sub.disableLoader();
        var loader = sub.enableLoader;
        expect(loader).toBe(false);
        sub.loadReports();
        expect(sub.enableLoader).toBe(false);
    });
    it('ReportList downloadReport().  Download single Report.', function () {
        var http;
        var appService = new app_service_1.AppService(http);
        var reportlistService = new MockReportListService(http);
        var elementRef;
        appService.params = [{ tabKey: "tab.reportcsr", parameters: "WWJVR1lFTjFPaE9BTnRiMXpqa2Z5L2hxL2NJK3JRNEhRbExJZ1pmcVpMN09kZ3NGOFpLcXpBdzNlcU8wUHRNcVNFVzJDaU5hOWlPb2I3MVhjOHJFVEJaMGIwZ3A1TjltY3pzdFBTSW44azQxak1VRW8vS1VISE92UU1UODlvbXNsRDhaeHJqM0NTQTVDSVlDRWtNcWlSRnZ0VHF1Q3ZHaTBHUVRjWjE5cnF0MVdBaXN1Z3FXQVhxakNLREVnd0VBdFR4S3RBQURPUzFHS0ZBT2M4Ty92QW42UEswSWNmUk1tUTU4MElLRVZoQW5JRTJVSTJFOEIzVGpCUjQyNk9idCtOTTRwcFU0NmhjcGJ3WTBJVWlsMXJNVFl5TlcvQmd2RUp4R2JMOEFXc00vTW1jb0VCMmVveWliSHg5bnpKVkNYaXZ2aHBVVmE0ZHFZbk01MTVQejE0Tml0VWhrR1FwQWVsY3FjcVJGOXpFYUlpaUgxL210NU9VZjZtMng4ZE9jVkl0Y1lOdGxhcHB3dWZUMHA1Q3ZxYnNQWk1uKzEzY1dJMEN1dU5VWTk2Z2JHdmFaWGlkdjJYZkR3N3dpZ2pEcXlHanRIRXdsNTRhbFhnWVByb254ZU9Yc3hHeVdjNlpQeGV5OXVjb092ZW5tYU9naWtLb3NtYVpjYjVlckMvUGU5RXhQZnJkN3ROUkF4K2tmYlEzZWgrWUxRcDN3VHh4UWwzRkNxd2o0YVlRRm15bzhlbVJYRGxDOHE0MU9Icjdh", errorMessage: "" }];
        appService.deepLinkVariables = [{ deepLinkWidget: "", deepLinkWidgetTab: "", deepLinkWidgetSection: "" }];
        var gb = new reportlist_1.ReportList(reportlistService, appService, elementRef);
        gb.fromUnitTs = "unittest";
        gb.downloadReport(0, "", "user");
        expect(gb.noReport).toBe(true);
    });
    it('ReportList deleteReport().  Delete Report.', function () {
        var http;
        var appService = new app_service_1.AppService(http);
        var reportlistService = new MockReportListService(http);
        var elementRef;
        appService.params = [{ tabKey: "tab.reportcsr", parameters: "WWJVR1lFTjFPaE9BTnRiMXpqa2Z5L2hxL2NJK3JRNEhRbExJZ1pmcVpMN09kZ3NGOFpLcXpBdzNlcU8wUHRNcVNFVzJDaU5hOWlPb2I3MVhjOHJFVEJaMGIwZ3A1TjltY3pzdFBTSW44azQxak1VRW8vS1VISE92UU1UODlvbXNsRDhaeHJqM0NTQTVDSVlDRWtNcWlSRnZ0VHF1Q3ZHaTBHUVRjWjE5cnF0MVdBaXN1Z3FXQVhxakNLREVnd0VBdFR4S3RBQURPUzFHS0ZBT2M4Ty92QW42UEswSWNmUk1tUTU4MElLRVZoQW5JRTJVSTJFOEIzVGpCUjQyNk9idCtOTTRwcFU0NmhjcGJ3WTBJVWlsMXJNVFl5TlcvQmd2RUp4R2JMOEFXc00vTW1jb0VCMmVveWliSHg5bnpKVkNYaXZ2aHBVVmE0ZHFZbk01MTVQejE0Tml0VWhrR1FwQWVsY3FjcVJGOXpFYUlpaUgxL210NU9VZjZtMng4ZE9jVkl0Y1lOdGxhcHB3dWZUMHA1Q3ZxYnNQWk1uKzEzY1dJMEN1dU5VWTk2Z2JHdmFaWGlkdjJYZkR3N3dpZ2pEcXlHanRIRXdsNTRhbFhnWVByb254ZU9Yc3hHeVdjNlpQeGV5OXVjb092ZW5tYU9naWtLb3NtYVpjYjVlckMvUGU5RXhQZnJkN3ROUkF4K2tmYlEzZWgrWUxRcDN3VHh4UWwzRkNxd2o0YVlRRm15bzhlbVJYRGxDOHE0MU9Icjdh", errorMessage: "" }];
        appService.deepLinkVariables = [{ deepLinkWidget: "", deepLinkWidgetTab: "", deepLinkWidgetSection: "" }];
        var gb = new reportlist_1.ReportList(reportlistService, appService, elementRef);
        gb.fromUnitTs = "unittest";
        gb.deleteReport(0, "user");
        expect(gb.deleteReportResult).toBe('unittest');
    });
    it('ReportList deleteReportValidation().  Delete Report Validation.', function () {
        var http;
        var appService = new app_service_1.AppService(http);
        var reportlistService = new MockReportListService(http);
        var elementRef;
        appService.params = [{ tabKey: "tab.reportcsr", parameters: "WWJVR1lFTjFPaE9BTnRiMXpqa2Z5L2hxL2NJK3JRNEhRbExJZ1pmcVpMN09kZ3NGOFpLcXpBdzNlcU8wUHRNcVNFVzJDaU5hOWlPb2I3MVhjOHJFVEJaMGIwZ3A1TjltY3pzdFBTSW44azQxak1VRW8vS1VISE92UU1UODlvbXNsRDhaeHJqM0NTQTVDSVlDRWtNcWlSRnZ0VHF1Q3ZHaTBHUVRjWjE5cnF0MVdBaXN1Z3FXQVhxakNLREVnd0VBdFR4S3RBQURPUzFHS0ZBT2M4Ty92QW42UEswSWNmUk1tUTU4MElLRVZoQW5JRTJVSTJFOEIzVGpCUjQyNk9idCtOTTRwcFU0NmhjcGJ3WTBJVWlsMXJNVFl5TlcvQmd2RUp4R2JMOEFXc00vTW1jb0VCMmVveWliSHg5bnpKVkNYaXZ2aHBVVmE0ZHFZbk01MTVQejE0Tml0VWhrR1FwQWVsY3FjcVJGOXpFYUlpaUgxL210NU9VZjZtMng4ZE9jVkl0Y1lOdGxhcHB3dWZUMHA1Q3ZxYnNQWk1uKzEzY1dJMEN1dU5VWTk2Z2JHdmFaWGlkdjJYZkR3N3dpZ2pEcXlHanRIRXdsNTRhbFhnWVByb254ZU9Yc3hHeVdjNlpQeGV5OXVjb092ZW5tYU9naWtLb3NtYVpjYjVlckMvUGU5RXhQZnJkN3ROUkF4K2tmYlEzZWgrWUxRcDN3VHh4UWwzRkNxd2o0YVlRRm15bzhlbVJYRGxDOHE0MU9Icjdh", errorMessage: "" }];
        appService.deepLinkVariables = [{ deepLinkWidget: "", deepLinkWidgetTab: "", deepLinkWidgetSection: "" }];
        var gb = new reportlist_1.ReportList(reportlistService, appService, elementRef);
        gb.fromUnitTs = "unittest";
        gb.deleteReportResult = { Content: {} };
        gb.result = { ReportsUser: [{ Id: 0, ErrorStatus: true }, 1, 2], ReportsCSR: [{ Id: 0, ErrorStatus: true }, 1, 2] };
        gb.deleteReportValidation(0, "user");
        gb.deleteReportValidation(0, "");
        gb.deleteReportResult = [{ Content: JSON.parse('{"Title":"Green Button Download","SubTitle":"Download your Smart Meter Data","IntroText":"The Green Button download gives utility customers easy and secure access to their energy usage information.","FromDate":"From:","ToDate":"To:","DownloadAmiTitle":"","DownloadBillsTitle":"","DownloadUsage":"Download my usage for specific days","DownloadBills":"Download my bills","DownloadingMessage":"","Footer":"Sample footer","FooterLinkText":"Next steps","NoBillData":"No bill data is available.","NoUsageData":"No usage data is available.","DateError":"Please enter an end date that is after the start date.","ShowTitle":"true","ShowIntro":"true","ShowSubTitle":"false","FooterLink":"https://www.aclara.com","ShowFooterLink":"false","ShowFooterText":"false","NoBill":false,"NoAmi":false,"Response":null}') }];
        gb.deleteReportValidation(0, "user");
        gb.deleteReportValidation(0, "user");
        expect(gb.deleteErrorOccurred).toBe(true);
    });
    it('ReportList parseReportList().  Parse Reportlist and return index of array.', function () {
        var http;
        var appService = new app_service_1.AppService(http);
        var reportlistService = new MockReportListService(http);
        var elementRef;
        var list = { Id: 1 };
        appService.params = [{ tabKey: "tab.reportcsr", parameters: "WWJVR1lFTjFPaE9BTnRiMXpqa2Z5L2hxL2NJK3JRNEhRbExJZ1pmcVpMN09kZ3NGOFpLcXpBdzNlcU8wUHRNcVNFVzJDaU5hOWlPb2I3MVhjOHJFVEJaMGIwZ3A1TjltY3pzdFBTSW44azQxak1VRW8vS1VISE92UU1UODlvbXNsRDhaeHJqM0NTQTVDSVlDRWtNcWlSRnZ0VHF1Q3ZHaTBHUVRjWjE5cnF0MVdBaXN1Z3FXQVhxakNLREVnd0VBdFR4S3RBQURPUzFHS0ZBT2M4Ty92QW42UEswSWNmUk1tUTU4MElLRVZoQW5JRTJVSTJFOEIzVGpCUjQyNk9idCtOTTRwcFU0NmhjcGJ3WTBJVWlsMXJNVFl5TlcvQmd2RUp4R2JMOEFXc00vTW1jb0VCMmVveWliSHg5bnpKVkNYaXZ2aHBVVmE0ZHFZbk01MTVQejE0Tml0VWhrR1FwQWVsY3FjcVJGOXpFYUlpaUgxL210NU9VZjZtMng4ZE9jVkl0Y1lOdGxhcHB3dWZUMHA1Q3ZxYnNQWk1uKzEzY1dJMEN1dU5VWTk2Z2JHdmFaWGlkdjJYZkR3N3dpZ2pEcXlHanRIRXdsNTRhbFhnWVByb254ZU9Yc3hHeVdjNlpQeGV5OXVjb092ZW5tYU9naWtLb3NtYVpjYjVlckMvUGU5RXhQZnJkN3ROUkF4K2tmYlEzZWgrWUxRcDN3VHh4UWwzRkNxd2o0YVlRRm15bzhlbVJYRGxDOHE0MU9Icjdh", errorMessage: "" }];
        appService.deepLinkVariables = [{ deepLinkWidget: "", deepLinkWidgetTab: "", deepLinkWidgetSection: "" }];
        var gb = new reportlist_1.ReportList(reportlistService, appService, elementRef);
        gb.fromUnitTs = "unittest";
        var res = gb.parseReportList(list, 0);
        expect(res).toBe(-1);
        list = [{ Id: 1 }];
        res = gb.parseReportList(list, 1);
        expect(res).toBe(0);
    });
});
//# sourceMappingURL=reportlist.typescr.spec.js.map