﻿import { async, TestBed } from "@angular/core/testing";
import { ElementRef } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { HttpModule } from "@angular/http";
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from "@angular/platform-browser-dynamic/testing";
import { ReportList } from "../../ReportList/reportlist";
import { ReportListService } from "../../ReportList/reportlist.service";
import { AppService, IAppParams, IDeepLinkVariables } from "../../app.service";
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import { Loader } from "../../Loader/loader";

//mock report list and app services
class MockAppService extends AppService {
    params: Array<IAppParams> = [{
        tabKey: "tab.reportcsr", parameters: "YbUGYEN1OhOANtb1zjkfy/hq/cI+rQ4HQlLIgZfqZL7OdgsF8ZKqzAw3eqO0PtMqSEW2CiNa9iOob71Xc8rETBZ0b0gp5N9mczstPSIn8k41jMUEo/KUHHOvQMT89omslD8Zxrj3CSA5CIYCEkMqiRFvtTquCvGi0GQTcZ19rqt1WAisugqWAXqjCKDEgwEAtTxKtAADOS1GKFAOc8O/vAn6PK0IcfRMmQ580IKEVhAnIE2UI2E8B3TjBR426Obt+NM4ppU46hcpbwY0IUil1mo7P+ycNA09qgGMo1XRsTKNQ+278W8S/UFIfQ269pvH0OQxh9J4q5deCmt35nFMe7z7r4MTnZOUuDV5tk1MYF9ewfY/++5Q8qbe5RRzHFGQGcoGmJTGGUuSd6imQB3Xs1JffVwJgZawDyx+6WmG2DBbTMzBkYqCt8/wQh6YBYOxeRkKs/PVXGmRIMtPt4kJbF2v2M026u2kwtYrwDcXb5HM6EfmkVDpCzX5tbkaB0xiT3J7J/AsiEn3LMRAt3bYwMGp1iGklCocNV5kwcDHph4YuaxzGpcx8GTS+Svm3MdK", errorMessage: "We’re sorry… There was a problem loading your information." }];
    getParams(): Array<IAppParams> {
        return this.params;
    }

    getDeepLinkVariables(): Array<IDeepLinkVariables> {
        return this.deepLinkVariables;
    }

}

class MockReportListService extends ReportListService {
    public res: any;
    public unit: any = "unittest";
    getReportList(tabkey, params) {
        return this.unit;
    }
}

export class MockElementRef extends ElementRef {
    constructor() { super(null); }
}

describe('ReportList Dom Tests', () => {
    beforeEach(async(() => {
        TestBed.resetTestEnvironment();
        TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting());
       
        TestBed.configureTestingModule({
            imports: [BrowserModule, FormsModule, HttpModule, ConfirmationPopoverModule.forRoot()],
            declarations: [ReportList, Loader]
        })
            .overrideComponent(ReportList,
            {
                remove: { providers: [AppService, ReportListService] },
                add: {
                    providers: [
                        { provide: AppService, useClass: MockAppService },
                        { provide: ReportListService, useClass: MockReportListService },
                        { provide: ElementRef, useClass: MockElementRef }
                    ]
                }
            }).compileComponents();

    }));

    it('should get customer and csr report types', async(() => {
        const fixture = TestBed.createComponent(ReportList);
        const app = fixture.componentInstance;
        var json = JSON.parse('{"Content": { "NoData": "There are no reports available.", "OpenLink": "Open", "DeleteLink": "Delete", "DeletConfirm": "Are you sure you want to delete this report?", "CustomerSectionTitle": "Customer Reports", "CsrSectionTitle": "CSR Reports", "DeleteError": "An error occurred when we were deleting  this report. If this problem persists, please contact your system administrator.", "ViewError": "An error occurred when we were retrieving this report. If this problem persists, please contact your system administrator.", "ShowTitle": "FALSE", "ShowIntro": "FALSE", "FooterLink": "https:///www.aclara.com", "ShowFooterLink": "FALSE", "ShowFooterText": "FALSE", "Footer": "Sample footer", "FooterLinkText": "Next steps", "IntroText": "Sample introtext", "Title": "View Reports", "ShowSubTitle": "FALSE", "FooterLinkType": "external", "SubTitle": "These are the PDF reports for the customer" }, "ReportsCSR": [{ "Id": "615", "CustomerId": "993774751", "AccountId": "1218337093", "PremiseId": "1218337000", "RoleId": "2", "UserId": "CSR13", "TypeId": "1", "Title": "Test CSR Report", "Description": "This report helps explain your energy usage and costs.", "Name": "Your Energy Report.pdf", "FileSourceId": "1", "CreateDate": "2016-10-24T18:05:57.03", "RelativeTime": "14 days ago", "ErrorStatus": false }, ' +
            '{ "Id": "577", "CustomerId": "993774751", "AccountId": "1218337093", "PremiseId": "1218337000", "RoleId": "2", "UserId": "CSR13", "TypeId": "1", "Title": "Your Energy Report", "Description": "This report helps explain your energy usage and costs. testevp", "Name": "Your Energy Report.pdf", "FileSourceId": "1", "CreateDate": "2016-10-19T17:28:02.733", "RelativeTime": "19 days ago", "ErrorStatus": false }], ' +
            '"ReportsUser": [{ "Id": "502", "CustomerId": "993774751", "AccountId": "1218337093", "PremiseId": "1218337000", "RoleId": "0", "UserId": null, "TypeId": "1", "Title": "CustRept", "Description": "this is test my energy report sample", "Name": "1.pdf", "FileSourceId": "1", "CreateDate": "2016-11-07T20:06:42.113", "RelativeTime": "1 day ago", "ErrorStatus": false }, ' +
            '{ "Id": "667", "CustomerId": "993774751", "AccountId": "1218337093", "PremiseId": "1218337000", "RoleId": "0", "UserId": null, "TypeId": "1", "Title": "CustRept2", "Description": "this is test my energy report sample", "Name": "1.pdf", "FileSourceId": "1", "CreateDate": "2016-09-27T20:06:42.113", "RelativeTime": "one month ago", "ErrorStatus": false }], "isCSR": true }');
        app.result = json;
        app.enableLoader = false;
        fixture.detectChanges();
         
        const pdfrptType = fixture.nativeElement.querySelectorAll("#iws_ql_subtitle");
        expect(pdfrptType.length).toBe(2);
    }));

    it('should get four pdf reports', async(() => {
        const fixture = TestBed.createComponent(ReportList);
        const app = fixture.componentInstance;
        var json = JSON.parse('{"Content": { "NoData": "There are no reports available.", "OpenLink": "Open", "DeleteLink": "Delete", "DeletConfirm": "Are you sure you want to delete this report?", "CustomerSectionTitle": "Customer Reports", "CsrSectionTitle": "CSR Reports", "DeleteError": "An error occurred when we were deleting  this report. If this problem persists, please contact your system administrator.", "ViewError": "An error occurred when we were retrieving this report. If this problem persists, please contact your system administrator.", "ShowTitle": "FALSE", "ShowIntro": "FALSE", "FooterLink": "https:///www.aclara.com", "ShowFooterLink": "FALSE", "ShowFooterText": "FALSE", "Footer": "Sample footer", "FooterLinkText": "Next steps", "IntroText": "Sample introtext", "Title": "View Reports", "ShowSubTitle": "FALSE", "FooterLinkType": "external", "SubTitle": "These are the PDF reports for the customer" }, ' +
            '"ReportsCSR": [{ "Id": "615", "CustomerId": "993774751", "AccountId": "1218337093", "PremiseId": "1218337000", "RoleId": "2", "UserId": "CSR13", "TypeId": "1", "Title": "Test CSR Report", "Description": "This report helps explain your energy usage and costs.", "Name": "Your Energy Report.pdf", "FileSourceId": "1", "CreateDate": "2016-10-24T18:05:57.03", "RelativeTime": "14 days ago", "ErrorStatus": false }, ' +
            '{ "Id": "577", "CustomerId": "993774751", "AccountId": "1218337093", "PremiseId": "1218337000", "RoleId": "2", "UserId": "CSR13", "TypeId": "1", "Title": "Your Energy Report", "Description": "This report helps explain your energy usage and costs. testevp", "Name": "Your Energy Report.pdf", "FileSourceId": "1", "CreateDate": "2016-10-19T17:28:02.733", "RelativeTime": "19 days ago", "ErrorStatus": false }], ' +
            '"ReportsUser": [{ "Id": "502", "CustomerId": "993774751", "AccountId": "1218337093", "PremiseId": "1218337000", "RoleId": "0", "UserId": null, "TypeId": "1", "Title": "CustRept", "Description": "this is test my energy report sample", "Name": "1.pdf", "FileSourceId": "1", "CreateDate": "2016-11-07T20:06:42.113", "RelativeTime": "1 day ago", "ErrorStatus": false }, ' +
            '{ "Id": "667", "CustomerId": "993774751", "AccountId": "1218337093", "PremiseId": "1218337000", "RoleId": "0", "UserId": null, "TypeId": "1", "Title": "CustRept2", "Description": "this is test my energy report sample", "Name": "1.pdf", "FileSourceId": "1", "CreateDate": "2016-09-27T20:06:42.113", "RelativeTime": "one month ago", "ErrorStatus": false }], "isCSR": true }');
        app.result = json;
        app.enableLoader = false;
        fixture.detectChanges();
        const ps = fixture.nativeElement.querySelectorAll("p");
        expect(ps.length).toBe(4);
    }));

    it("should have buttons and report links when customer pdf reports found", () => {
        const fixture = TestBed.createComponent(ReportList);
        const app = fixture.componentInstance;
        
        var json = JSON.parse('{"Content": { "NoData": "There are no reports available.", "OpenLink": "Open", "DeleteLink": "Delete", "DeletConfirm": "Are you sure you want to delete this report?", "CustomerSectionTitle": "Customer Reports", "CsrSectionTitle": "CSR Reports", "DeleteError": "An error occurred when we were deleting  this report. If this problem persists, please contact your system administrator.", "ViewError": "An error occurred when we were retrieving this report. If this problem persists, please contact your system administrator.", "ShowTitle": "FALSE", "ShowIntro": "FALSE", "FooterLink": "https:///www.aclara.com", "ShowFooterLink": "FALSE", "ShowFooterText": "FALSE", "Footer": "Sample footer", "FooterLinkText": "Next steps", "IntroText": "Sample introtext", "Title": "View Reports", "ShowSubTitle": "FALSE", "FooterLinkType": "external", "SubTitle": "These are the PDF reports for the customer" }, ' +
            '"ReportsCSR": [{ "Id": "615", "CustomerId": "993774751", "AccountId": "1218337093", "PremiseId": "1218337000", "RoleId": "2", "UserId": "CSR13", "TypeId": "1", "Title": "Test CSR Report", "Description": "This report helps explain your energy usage and costs.", "Name": "Your Energy Report.pdf", "FileSourceId": "1", "CreateDate": "2016-10-24T18:05:57.03", "RelativeTime": "14 days ago", "ErrorStatus": false }, ' +
            '{ "Id": "577", "CustomerId": "993774751", "AccountId": "1218337093", "PremiseId": "1218337000", "RoleId": "2", "UserId": "CSR13", "TypeId": "1", "Title": "Your Energy Report", "Description": "This report helps explain your energy usage and costs. testevp", "Name": "Your Energy Report.pdf", "FileSourceId": "1", "CreateDate": "2016-10-19T17:28:02.733", "RelativeTime": "19 days ago", "ErrorStatus": false }], ' +
            '"ReportsUser": [{ "Id": "502", "CustomerId": "993774751", "AccountId": "1218337093", "PremiseId": "1218337000", "RoleId": "0", "UserId": null, "TypeId": "1", "Title": "CustRept", "Description": "this is test my energy report sample", "Name": "1.pdf", "FileSourceId": "1", "CreateDate": "2016-11-07T20:06:42.113", "RelativeTime": "1 day ago", "ErrorStatus": false }, ' +
            '{ "Id": "667", "CustomerId": "993774751", "AccountId": "1218337093", "PremiseId": "1218337000", "RoleId": "0", "UserId": null, "TypeId": "1", "Title": "CustRept2", "Description": "this is test my energy report sample", "Name": "1.pdf", "FileSourceId": "1", "CreateDate": "2016-09-27T20:06:42.113", "RelativeTime": "one month ago", "ErrorStatus": false }], "isCSR": true }');

        app.result = json;
        app.enableLoader = false;
        fixture.detectChanges();
        const linkCnt = fixture.nativeElement.querySelectorAll("a");
        expect(linkCnt.length).toBe(12);
    });

    it('should find button with open on reportlist inner text', async(() => {
        const fixture = TestBed.createComponent(ReportList);
        const app = fixture.componentInstance;

        var json = JSON.parse('{"Content": { "NoData": "There are no reports available.", "OpenLink": "Open", "DeleteLink": "Delete", "DeletConfirm": "Are you sure you want to delete this report?", "CustomerSectionTitle": "Customer Reports", "CsrSectionTitle": "CSR Reports", "DeleteError": "An error occurred when we were deleting  this report. If this problem persists, please contact your system administrator.", "ViewError": "An error occurred when we were retrieving this report. If this problem persists, please contact your system administrator.", "ShowTitle": "FALSE", "ShowIntro": "FALSE", "FooterLink": "https:///www.aclara.com", "ShowFooterLink": "FALSE", "ShowFooterText": "FALSE", "Footer": "Sample footer", "FooterLinkText": "Next steps", "IntroText": "Sample introtext", "Title": "View Reports", "ShowSubTitle": "FALSE", "FooterLinkType": "external", "SubTitle": "These are the PDF reports for the customer" }, ' +
            '"ReportsCSR": [{ "Id": "615", "CustomerId": "993774751", "AccountId": "1218337093", "PremiseId": "1218337000", "RoleId": "2", "UserId": "CSR13", "TypeId": "1", "Title": "Test CSR Report", "Description": "This report helps explain your energy usage and costs.", "Name": "Your Energy Report.pdf", "FileSourceId": "1", "CreateDate": "2016-10-24T18:05:57.03", "RelativeTime": "14 days ago", "ErrorStatus": false }, ' +
            '{ "Id": "577", "CustomerId": "993774751", "AccountId": "1218337093", "PremiseId": "1218337000", "RoleId": "2", "UserId": "CSR13", "TypeId": "1", "Title": "Your Energy Report", "Description": "This report helps explain your energy usage and costs. testevp", "Name": "Your Energy Report.pdf", "FileSourceId": "1", "CreateDate": "2016-10-19T17:28:02.733", "RelativeTime": "19 days ago", "ErrorStatus": false }], ' +
            '"ReportsUser": [{ "Id": "502", "CustomerId": "993774751", "AccountId": "1218337093", "PremiseId": "1218337000", "RoleId": "0", "UserId": null, "TypeId": "1", "Title": "CustRept", "Description": "this is test my energy report sample", "Name": "1.pdf", "FileSourceId": "1", "CreateDate": "2016-11-07T20:06:42.113", "RelativeTime": "1 day ago", "ErrorStatus": false }, ' +
            '{ "Id": "667", "CustomerId": "993774751", "AccountId": "1218337093", "PremiseId": "1218337000", "RoleId": "0", "UserId": null, "TypeId": "1", "Title": "CustRept2", "Description": "this is test my energy report sample", "Name": "1.pdf", "FileSourceId": "1", "CreateDate": "2016-09-27T20:06:42.113", "RelativeTime": "one month ago", "ErrorStatus": false }], "isCSR": true }');
        app.result = json;
        app.enableLoader = false;
        fixture.detectChanges();

        const txtDeleteRept = fixture.nativeElement.querySelectorAll("a");
        expect(txtDeleteRept[1].innerText).toBe("Open");
    }));

    it('should find button with delete on reportlist inner text', async(() => {
        const fixture = TestBed.createComponent(ReportList);
        const app = fixture.componentInstance;
        
        var json = JSON.parse('{"Content": { "NoData": "There are no reports available.", "OpenLink": "Open", "DeleteLink": "Delete", "DeletConfirm": "Are you sure you want to delete this report?", "CustomerSectionTitle": "Customer Reports", "CsrSectionTitle": "CSR Reports", "DeleteError": "An error occurred when we were deleting  this report. If this problem persists, please contact your system administrator.", "ViewError": "An error occurred when we were retrieving this report. If this problem persists, please contact your system administrator.", "ShowTitle": "FALSE", "ShowIntro": "FALSE", "FooterLink": "https:///www.aclara.com", "ShowFooterLink": "FALSE", "ShowFooterText": "FALSE", "Footer": "Sample footer", "FooterLinkText": "Next steps", "IntroText": "Sample introtext", "Title": "View Reports", "ShowSubTitle": "FALSE", "FooterLinkType": "external", "SubTitle": "These are the PDF reports for the customer" }, ' +
            '"ReportsCSR": [{ "Id": "615", "CustomerId": "993774751", "AccountId": "1218337093", "PremiseId": "1218337000", "RoleId": "2", "UserId": "CSR13", "TypeId": "1", "Title": "Test CSR Report", "Description": "This report helps explain your energy usage and costs.", "Name": "Your Energy Report.pdf", "FileSourceId": "1", "CreateDate": "2016-10-24T18:05:57.03", "RelativeTime": "14 days ago", "ErrorStatus": false }, ' +
            '{ "Id": "577", "CustomerId": "993774751", "AccountId": "1218337093", "PremiseId": "1218337000", "RoleId": "2", "UserId": "CSR13", "TypeId": "1", "Title": "Your Energy Report", "Description": "This report helps explain your energy usage and costs. testevp", "Name": "Your Energy Report.pdf", "FileSourceId": "1", "CreateDate": "2016-10-19T17:28:02.733", "RelativeTime": "19 days ago", "ErrorStatus": false }], ' +
            '"ReportsUser": [{ "Id": "502", "CustomerId": "993774751", "AccountId": "1218337093", "PremiseId": "1218337000", "RoleId": "0", "UserId": null, "TypeId": "1", "Title": "CustRept", "Description": "this is test my energy report sample", "Name": "1.pdf", "FileSourceId": "1", "CreateDate": "2016-11-07T20:06:42.113", "RelativeTime": "1 day ago", "ErrorStatus": false }, ' +
            '{ "Id": "667", "CustomerId": "993774751", "AccountId": "1218337093", "PremiseId": "1218337000", "RoleId": "0", "UserId": null, "TypeId": "1", "Title": "CustRept2", "Description": "this is test my energy report sample", "Name": "1.pdf", "FileSourceId": "1", "CreateDate": "2016-09-27T20:06:42.113", "RelativeTime": "one month ago", "ErrorStatus": false }], "isCSR": true }');
        app.result = json;
        app.enableLoader = false; 
        fixture.detectChanges();
 
        const txtDeleteRept = fixture.nativeElement.querySelectorAll("a.xs-ml-0.xs-mr-0.xs-pl-0.xs-pr-0.button-small");
        expect(txtDeleteRept[2].innerText).toBe("Delete");
    }));

    it('should show popover content Confirm when Delete button is clicked', async(() => {
        const fixture = TestBed.createComponent(ReportList);

        const app = fixture.componentInstance;

        var json = JSON.parse('{"Content": { "NoData": "There are no reports available.", "OpenLink": "Open", "DeleteLink": "Delete", "DeletConfirm": "Are you sure you want to delete this report?", "CustomerSectionTitle": "Customer Reports", "CsrSectionTitle": "CSR Reports", "DeleteError": "An error occurred when we were deleting  this report. If this problem persists, please contact your system administrator.", "ViewError": "An error occurred when we were retrieving this report. If this problem persists, please contact your system administrator.", "ShowTitle": "FALSE", "ShowIntro": "FALSE", "FooterLink": "https:///www.aclara.com", "ShowFooterLink": "FALSE", "ShowFooterText": "FALSE", "Footer": "Sample footer", "FooterLinkText": "Next steps", "IntroText": "Sample introtext", "Title": "View Reports", "ShowSubTitle": "FALSE", "FooterLinkType": "external", "SubTitle": "These are the PDF reports for the customer" }, ' +
            '"ReportsCSR": [{ "Id": "615", "CustomerId": "993774751", "AccountId": "1218337093", "PremiseId": "1218337000", "RoleId": "2", "UserId": "CSR13", "TypeId": "1", "Title": "Test CSR Report", "Description": "This report helps explain your energy usage and costs.", "Name": "Your Energy Report.pdf", "FileSourceId": "1", "CreateDate": "2016-10-24T18:05:57.03", "RelativeTime": "14 days ago", "ErrorStatus": false }, ' +
            '{ "Id": "577", "CustomerId": "993774751", "AccountId": "1218337093", "PremiseId": "1218337000", "RoleId": "2", "UserId": "CSR13", "TypeId": "1", "Title": "Your Energy Report", "Description": "This report helps explain your energy usage and costs. testevp", "Name": "Your Energy Report.pdf", "FileSourceId": "1", "CreateDate": "2016-10-19T17:28:02.733", "RelativeTime": "19 days ago", "ErrorStatus": false }], ' +
            '"ReportsUser": [{ "Id": "502", "CustomerId": "993774751", "AccountId": "1218337093", "PremiseId": "1218337000", "RoleId": "0", "UserId": null, "TypeId": "1", "Title": "CustRept", "Description": "this is test my energy report sample", "Name": "1.pdf", "FileSourceId": "1", "CreateDate": "2016-11-07T20:06:42.113", "RelativeTime": "1 day ago", "ErrorStatus": false }, ' +
            '{ "Id": "667", "CustomerId": "993774751", "AccountId": "1218337093", "PremiseId": "1218337000", "RoleId": "0", "UserId": null, "TypeId": "1", "Title": "CustRept2", "Description": "this is test my energy report sample", "Name": "1.pdf", "FileSourceId": "1", "CreateDate": "2016-09-27T20:06:42.113", "RelativeTime": "one month ago", "ErrorStatus": false }], "isCSR": true }');
        app.result = json;
        app.enableLoader = false;
        fixture.detectChanges();

        var link = fixture.nativeElement.querySelectorAll("a.xs-ml-0.xs-mr-0.xs-pl-0.xs-pr-0.button-small");
        link[0].click(); 
        fixture.detectChanges();
        expect(fixture.elementRef.nativeElement.nextSibling.textContent).toContain('Confirm');
    }));
});