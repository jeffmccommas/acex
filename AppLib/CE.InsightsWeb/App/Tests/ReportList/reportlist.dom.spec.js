"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var platform_browser_1 = require("@angular/platform-browser");
var http_1 = require("@angular/http");
var testing_2 = require("@angular/platform-browser-dynamic/testing");
var reportlist_1 = require("../../ReportList/reportlist");
var reportlist_service_1 = require("../../ReportList/reportlist.service");
var app_service_1 = require("../../app.service");
var angular_confirmation_popover_1 = require("angular-confirmation-popover");
var loader_1 = require("../../Loader/loader");
var MockAppService = (function (_super) {
    __extends(MockAppService, _super);
    function MockAppService() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.params = [{
                tabKey: "tab.reportcsr", parameters: "YbUGYEN1OhOANtb1zjkfy/hq/cI+rQ4HQlLIgZfqZL7OdgsF8ZKqzAw3eqO0PtMqSEW2CiNa9iOob71Xc8rETBZ0b0gp5N9mczstPSIn8k41jMUEo/KUHHOvQMT89omslD8Zxrj3CSA5CIYCEkMqiRFvtTquCvGi0GQTcZ19rqt1WAisugqWAXqjCKDEgwEAtTxKtAADOS1GKFAOc8O/vAn6PK0IcfRMmQ580IKEVhAnIE2UI2E8B3TjBR426Obt+NM4ppU46hcpbwY0IUil1mo7P+ycNA09qgGMo1XRsTKNQ+278W8S/UFIfQ269pvH0OQxh9J4q5deCmt35nFMe7z7r4MTnZOUuDV5tk1MYF9ewfY/++5Q8qbe5RRzHFGQGcoGmJTGGUuSd6imQB3Xs1JffVwJgZawDyx+6WmG2DBbTMzBkYqCt8/wQh6YBYOxeRkKs/PVXGmRIMtPt4kJbF2v2M026u2kwtYrwDcXb5HM6EfmkVDpCzX5tbkaB0xiT3J7J/AsiEn3LMRAt3bYwMGp1iGklCocNV5kwcDHph4YuaxzGpcx8GTS+Svm3MdK", errorMessage: "We’re sorry… There was a problem loading your information."
            }];
        return _this;
    }
    MockAppService.prototype.getParams = function () {
        return this.params;
    };
    MockAppService.prototype.getDeepLinkVariables = function () {
        return this.deepLinkVariables;
    };
    return MockAppService;
}(app_service_1.AppService));
var MockReportListService = (function (_super) {
    __extends(MockReportListService, _super);
    function MockReportListService() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.unit = "unittest";
        return _this;
    }
    MockReportListService.prototype.getReportList = function (tabkey, params) {
        return this.unit;
    };
    return MockReportListService;
}(reportlist_service_1.ReportListService));
var MockElementRef = (function (_super) {
    __extends(MockElementRef, _super);
    function MockElementRef() {
        return _super.call(this, null) || this;
    }
    return MockElementRef;
}(core_1.ElementRef));
exports.MockElementRef = MockElementRef;
describe('ReportList Dom Tests', function () {
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.resetTestEnvironment();
        testing_1.TestBed.initTestEnvironment(testing_2.BrowserDynamicTestingModule, testing_2.platformBrowserDynamicTesting());
        testing_1.TestBed.configureTestingModule({
            imports: [platform_browser_1.BrowserModule, forms_1.FormsModule, http_1.HttpModule, angular_confirmation_popover_1.ConfirmationPopoverModule.forRoot()],
            declarations: [reportlist_1.ReportList, loader_1.Loader]
        })
            .overrideComponent(reportlist_1.ReportList, {
            remove: { providers: [app_service_1.AppService, reportlist_service_1.ReportListService] },
            add: {
                providers: [
                    { provide: app_service_1.AppService, useClass: MockAppService },
                    { provide: reportlist_service_1.ReportListService, useClass: MockReportListService },
                    { provide: core_1.ElementRef, useClass: MockElementRef }
                ]
            }
        }).compileComponents();
    }));
    it('should get customer and csr report types', testing_1.async(function () {
        var fixture = testing_1.TestBed.createComponent(reportlist_1.ReportList);
        var app = fixture.componentInstance;
        var json = JSON.parse('{"Content": { "NoData": "There are no reports available.", "OpenLink": "Open", "DeleteLink": "Delete", "DeletConfirm": "Are you sure you want to delete this report?", "CustomerSectionTitle": "Customer Reports", "CsrSectionTitle": "CSR Reports", "DeleteError": "An error occurred when we were deleting  this report. If this problem persists, please contact your system administrator.", "ViewError": "An error occurred when we were retrieving this report. If this problem persists, please contact your system administrator.", "ShowTitle": "FALSE", "ShowIntro": "FALSE", "FooterLink": "https:///www.aclara.com", "ShowFooterLink": "FALSE", "ShowFooterText": "FALSE", "Footer": "Sample footer", "FooterLinkText": "Next steps", "IntroText": "Sample introtext", "Title": "View Reports", "ShowSubTitle": "FALSE", "FooterLinkType": "external", "SubTitle": "These are the PDF reports for the customer" }, "ReportsCSR": [{ "Id": "615", "CustomerId": "993774751", "AccountId": "1218337093", "PremiseId": "1218337000", "RoleId": "2", "UserId": "CSR13", "TypeId": "1", "Title": "Test CSR Report", "Description": "This report helps explain your energy usage and costs.", "Name": "Your Energy Report.pdf", "FileSourceId": "1", "CreateDate": "2016-10-24T18:05:57.03", "RelativeTime": "14 days ago", "ErrorStatus": false }, ' +
            '{ "Id": "577", "CustomerId": "993774751", "AccountId": "1218337093", "PremiseId": "1218337000", "RoleId": "2", "UserId": "CSR13", "TypeId": "1", "Title": "Your Energy Report", "Description": "This report helps explain your energy usage and costs. testevp", "Name": "Your Energy Report.pdf", "FileSourceId": "1", "CreateDate": "2016-10-19T17:28:02.733", "RelativeTime": "19 days ago", "ErrorStatus": false }], ' +
            '"ReportsUser": [{ "Id": "502", "CustomerId": "993774751", "AccountId": "1218337093", "PremiseId": "1218337000", "RoleId": "0", "UserId": null, "TypeId": "1", "Title": "CustRept", "Description": "this is test my energy report sample", "Name": "1.pdf", "FileSourceId": "1", "CreateDate": "2016-11-07T20:06:42.113", "RelativeTime": "1 day ago", "ErrorStatus": false }, ' +
            '{ "Id": "667", "CustomerId": "993774751", "AccountId": "1218337093", "PremiseId": "1218337000", "RoleId": "0", "UserId": null, "TypeId": "1", "Title": "CustRept2", "Description": "this is test my energy report sample", "Name": "1.pdf", "FileSourceId": "1", "CreateDate": "2016-09-27T20:06:42.113", "RelativeTime": "one month ago", "ErrorStatus": false }], "isCSR": true }');
        app.result = json;
        app.enableLoader = false;
        fixture.detectChanges();
        var pdfrptType = fixture.nativeElement.querySelectorAll("#iws_ql_subtitle");
        expect(pdfrptType.length).toBe(2);
    }));
    it('should get four pdf reports', testing_1.async(function () {
        var fixture = testing_1.TestBed.createComponent(reportlist_1.ReportList);
        var app = fixture.componentInstance;
        var json = JSON.parse('{"Content": { "NoData": "There are no reports available.", "OpenLink": "Open", "DeleteLink": "Delete", "DeletConfirm": "Are you sure you want to delete this report?", "CustomerSectionTitle": "Customer Reports", "CsrSectionTitle": "CSR Reports", "DeleteError": "An error occurred when we were deleting  this report. If this problem persists, please contact your system administrator.", "ViewError": "An error occurred when we were retrieving this report. If this problem persists, please contact your system administrator.", "ShowTitle": "FALSE", "ShowIntro": "FALSE", "FooterLink": "https:///www.aclara.com", "ShowFooterLink": "FALSE", "ShowFooterText": "FALSE", "Footer": "Sample footer", "FooterLinkText": "Next steps", "IntroText": "Sample introtext", "Title": "View Reports", "ShowSubTitle": "FALSE", "FooterLinkType": "external", "SubTitle": "These are the PDF reports for the customer" }, ' +
            '"ReportsCSR": [{ "Id": "615", "CustomerId": "993774751", "AccountId": "1218337093", "PremiseId": "1218337000", "RoleId": "2", "UserId": "CSR13", "TypeId": "1", "Title": "Test CSR Report", "Description": "This report helps explain your energy usage and costs.", "Name": "Your Energy Report.pdf", "FileSourceId": "1", "CreateDate": "2016-10-24T18:05:57.03", "RelativeTime": "14 days ago", "ErrorStatus": false }, ' +
            '{ "Id": "577", "CustomerId": "993774751", "AccountId": "1218337093", "PremiseId": "1218337000", "RoleId": "2", "UserId": "CSR13", "TypeId": "1", "Title": "Your Energy Report", "Description": "This report helps explain your energy usage and costs. testevp", "Name": "Your Energy Report.pdf", "FileSourceId": "1", "CreateDate": "2016-10-19T17:28:02.733", "RelativeTime": "19 days ago", "ErrorStatus": false }], ' +
            '"ReportsUser": [{ "Id": "502", "CustomerId": "993774751", "AccountId": "1218337093", "PremiseId": "1218337000", "RoleId": "0", "UserId": null, "TypeId": "1", "Title": "CustRept", "Description": "this is test my energy report sample", "Name": "1.pdf", "FileSourceId": "1", "CreateDate": "2016-11-07T20:06:42.113", "RelativeTime": "1 day ago", "ErrorStatus": false }, ' +
            '{ "Id": "667", "CustomerId": "993774751", "AccountId": "1218337093", "PremiseId": "1218337000", "RoleId": "0", "UserId": null, "TypeId": "1", "Title": "CustRept2", "Description": "this is test my energy report sample", "Name": "1.pdf", "FileSourceId": "1", "CreateDate": "2016-09-27T20:06:42.113", "RelativeTime": "one month ago", "ErrorStatus": false }], "isCSR": true }');
        app.result = json;
        app.enableLoader = false;
        fixture.detectChanges();
        var ps = fixture.nativeElement.querySelectorAll("p");
        expect(ps.length).toBe(4);
    }));
    it("should have buttons and report links when customer pdf reports found", function () {
        var fixture = testing_1.TestBed.createComponent(reportlist_1.ReportList);
        var app = fixture.componentInstance;
        var json = JSON.parse('{"Content": { "NoData": "There are no reports available.", "OpenLink": "Open", "DeleteLink": "Delete", "DeletConfirm": "Are you sure you want to delete this report?", "CustomerSectionTitle": "Customer Reports", "CsrSectionTitle": "CSR Reports", "DeleteError": "An error occurred when we were deleting  this report. If this problem persists, please contact your system administrator.", "ViewError": "An error occurred when we were retrieving this report. If this problem persists, please contact your system administrator.", "ShowTitle": "FALSE", "ShowIntro": "FALSE", "FooterLink": "https:///www.aclara.com", "ShowFooterLink": "FALSE", "ShowFooterText": "FALSE", "Footer": "Sample footer", "FooterLinkText": "Next steps", "IntroText": "Sample introtext", "Title": "View Reports", "ShowSubTitle": "FALSE", "FooterLinkType": "external", "SubTitle": "These are the PDF reports for the customer" }, ' +
            '"ReportsCSR": [{ "Id": "615", "CustomerId": "993774751", "AccountId": "1218337093", "PremiseId": "1218337000", "RoleId": "2", "UserId": "CSR13", "TypeId": "1", "Title": "Test CSR Report", "Description": "This report helps explain your energy usage and costs.", "Name": "Your Energy Report.pdf", "FileSourceId": "1", "CreateDate": "2016-10-24T18:05:57.03", "RelativeTime": "14 days ago", "ErrorStatus": false }, ' +
            '{ "Id": "577", "CustomerId": "993774751", "AccountId": "1218337093", "PremiseId": "1218337000", "RoleId": "2", "UserId": "CSR13", "TypeId": "1", "Title": "Your Energy Report", "Description": "This report helps explain your energy usage and costs. testevp", "Name": "Your Energy Report.pdf", "FileSourceId": "1", "CreateDate": "2016-10-19T17:28:02.733", "RelativeTime": "19 days ago", "ErrorStatus": false }], ' +
            '"ReportsUser": [{ "Id": "502", "CustomerId": "993774751", "AccountId": "1218337093", "PremiseId": "1218337000", "RoleId": "0", "UserId": null, "TypeId": "1", "Title": "CustRept", "Description": "this is test my energy report sample", "Name": "1.pdf", "FileSourceId": "1", "CreateDate": "2016-11-07T20:06:42.113", "RelativeTime": "1 day ago", "ErrorStatus": false }, ' +
            '{ "Id": "667", "CustomerId": "993774751", "AccountId": "1218337093", "PremiseId": "1218337000", "RoleId": "0", "UserId": null, "TypeId": "1", "Title": "CustRept2", "Description": "this is test my energy report sample", "Name": "1.pdf", "FileSourceId": "1", "CreateDate": "2016-09-27T20:06:42.113", "RelativeTime": "one month ago", "ErrorStatus": false }], "isCSR": true }');
        app.result = json;
        app.enableLoader = false;
        fixture.detectChanges();
        var linkCnt = fixture.nativeElement.querySelectorAll("a");
        expect(linkCnt.length).toBe(12);
    });
    it('should find button with open on reportlist inner text', testing_1.async(function () {
        var fixture = testing_1.TestBed.createComponent(reportlist_1.ReportList);
        var app = fixture.componentInstance;
        var json = JSON.parse('{"Content": { "NoData": "There are no reports available.", "OpenLink": "Open", "DeleteLink": "Delete", "DeletConfirm": "Are you sure you want to delete this report?", "CustomerSectionTitle": "Customer Reports", "CsrSectionTitle": "CSR Reports", "DeleteError": "An error occurred when we were deleting  this report. If this problem persists, please contact your system administrator.", "ViewError": "An error occurred when we were retrieving this report. If this problem persists, please contact your system administrator.", "ShowTitle": "FALSE", "ShowIntro": "FALSE", "FooterLink": "https:///www.aclara.com", "ShowFooterLink": "FALSE", "ShowFooterText": "FALSE", "Footer": "Sample footer", "FooterLinkText": "Next steps", "IntroText": "Sample introtext", "Title": "View Reports", "ShowSubTitle": "FALSE", "FooterLinkType": "external", "SubTitle": "These are the PDF reports for the customer" }, ' +
            '"ReportsCSR": [{ "Id": "615", "CustomerId": "993774751", "AccountId": "1218337093", "PremiseId": "1218337000", "RoleId": "2", "UserId": "CSR13", "TypeId": "1", "Title": "Test CSR Report", "Description": "This report helps explain your energy usage and costs.", "Name": "Your Energy Report.pdf", "FileSourceId": "1", "CreateDate": "2016-10-24T18:05:57.03", "RelativeTime": "14 days ago", "ErrorStatus": false }, ' +
            '{ "Id": "577", "CustomerId": "993774751", "AccountId": "1218337093", "PremiseId": "1218337000", "RoleId": "2", "UserId": "CSR13", "TypeId": "1", "Title": "Your Energy Report", "Description": "This report helps explain your energy usage and costs. testevp", "Name": "Your Energy Report.pdf", "FileSourceId": "1", "CreateDate": "2016-10-19T17:28:02.733", "RelativeTime": "19 days ago", "ErrorStatus": false }], ' +
            '"ReportsUser": [{ "Id": "502", "CustomerId": "993774751", "AccountId": "1218337093", "PremiseId": "1218337000", "RoleId": "0", "UserId": null, "TypeId": "1", "Title": "CustRept", "Description": "this is test my energy report sample", "Name": "1.pdf", "FileSourceId": "1", "CreateDate": "2016-11-07T20:06:42.113", "RelativeTime": "1 day ago", "ErrorStatus": false }, ' +
            '{ "Id": "667", "CustomerId": "993774751", "AccountId": "1218337093", "PremiseId": "1218337000", "RoleId": "0", "UserId": null, "TypeId": "1", "Title": "CustRept2", "Description": "this is test my energy report sample", "Name": "1.pdf", "FileSourceId": "1", "CreateDate": "2016-09-27T20:06:42.113", "RelativeTime": "one month ago", "ErrorStatus": false }], "isCSR": true }');
        app.result = json;
        app.enableLoader = false;
        fixture.detectChanges();
        var txtDeleteRept = fixture.nativeElement.querySelectorAll("a");
        expect(txtDeleteRept[1].innerText).toBe("Open");
    }));
    it('should find button with delete on reportlist inner text', testing_1.async(function () {
        var fixture = testing_1.TestBed.createComponent(reportlist_1.ReportList);
        var app = fixture.componentInstance;
        var json = JSON.parse('{"Content": { "NoData": "There are no reports available.", "OpenLink": "Open", "DeleteLink": "Delete", "DeletConfirm": "Are you sure you want to delete this report?", "CustomerSectionTitle": "Customer Reports", "CsrSectionTitle": "CSR Reports", "DeleteError": "An error occurred when we were deleting  this report. If this problem persists, please contact your system administrator.", "ViewError": "An error occurred when we were retrieving this report. If this problem persists, please contact your system administrator.", "ShowTitle": "FALSE", "ShowIntro": "FALSE", "FooterLink": "https:///www.aclara.com", "ShowFooterLink": "FALSE", "ShowFooterText": "FALSE", "Footer": "Sample footer", "FooterLinkText": "Next steps", "IntroText": "Sample introtext", "Title": "View Reports", "ShowSubTitle": "FALSE", "FooterLinkType": "external", "SubTitle": "These are the PDF reports for the customer" }, ' +
            '"ReportsCSR": [{ "Id": "615", "CustomerId": "993774751", "AccountId": "1218337093", "PremiseId": "1218337000", "RoleId": "2", "UserId": "CSR13", "TypeId": "1", "Title": "Test CSR Report", "Description": "This report helps explain your energy usage and costs.", "Name": "Your Energy Report.pdf", "FileSourceId": "1", "CreateDate": "2016-10-24T18:05:57.03", "RelativeTime": "14 days ago", "ErrorStatus": false }, ' +
            '{ "Id": "577", "CustomerId": "993774751", "AccountId": "1218337093", "PremiseId": "1218337000", "RoleId": "2", "UserId": "CSR13", "TypeId": "1", "Title": "Your Energy Report", "Description": "This report helps explain your energy usage and costs. testevp", "Name": "Your Energy Report.pdf", "FileSourceId": "1", "CreateDate": "2016-10-19T17:28:02.733", "RelativeTime": "19 days ago", "ErrorStatus": false }], ' +
            '"ReportsUser": [{ "Id": "502", "CustomerId": "993774751", "AccountId": "1218337093", "PremiseId": "1218337000", "RoleId": "0", "UserId": null, "TypeId": "1", "Title": "CustRept", "Description": "this is test my energy report sample", "Name": "1.pdf", "FileSourceId": "1", "CreateDate": "2016-11-07T20:06:42.113", "RelativeTime": "1 day ago", "ErrorStatus": false }, ' +
            '{ "Id": "667", "CustomerId": "993774751", "AccountId": "1218337093", "PremiseId": "1218337000", "RoleId": "0", "UserId": null, "TypeId": "1", "Title": "CustRept2", "Description": "this is test my energy report sample", "Name": "1.pdf", "FileSourceId": "1", "CreateDate": "2016-09-27T20:06:42.113", "RelativeTime": "one month ago", "ErrorStatus": false }], "isCSR": true }');
        app.result = json;
        app.enableLoader = false;
        fixture.detectChanges();
        var txtDeleteRept = fixture.nativeElement.querySelectorAll("a.xs-ml-0.xs-mr-0.xs-pl-0.xs-pr-0.button-small");
        expect(txtDeleteRept[2].innerText).toBe("Delete");
    }));
    it('should show popover content Confirm when Delete button is clicked', testing_1.async(function () {
        var fixture = testing_1.TestBed.createComponent(reportlist_1.ReportList);
        var app = fixture.componentInstance;
        var json = JSON.parse('{"Content": { "NoData": "There are no reports available.", "OpenLink": "Open", "DeleteLink": "Delete", "DeletConfirm": "Are you sure you want to delete this report?", "CustomerSectionTitle": "Customer Reports", "CsrSectionTitle": "CSR Reports", "DeleteError": "An error occurred when we were deleting  this report. If this problem persists, please contact your system administrator.", "ViewError": "An error occurred when we were retrieving this report. If this problem persists, please contact your system administrator.", "ShowTitle": "FALSE", "ShowIntro": "FALSE", "FooterLink": "https:///www.aclara.com", "ShowFooterLink": "FALSE", "ShowFooterText": "FALSE", "Footer": "Sample footer", "FooterLinkText": "Next steps", "IntroText": "Sample introtext", "Title": "View Reports", "ShowSubTitle": "FALSE", "FooterLinkType": "external", "SubTitle": "These are the PDF reports for the customer" }, ' +
            '"ReportsCSR": [{ "Id": "615", "CustomerId": "993774751", "AccountId": "1218337093", "PremiseId": "1218337000", "RoleId": "2", "UserId": "CSR13", "TypeId": "1", "Title": "Test CSR Report", "Description": "This report helps explain your energy usage and costs.", "Name": "Your Energy Report.pdf", "FileSourceId": "1", "CreateDate": "2016-10-24T18:05:57.03", "RelativeTime": "14 days ago", "ErrorStatus": false }, ' +
            '{ "Id": "577", "CustomerId": "993774751", "AccountId": "1218337093", "PremiseId": "1218337000", "RoleId": "2", "UserId": "CSR13", "TypeId": "1", "Title": "Your Energy Report", "Description": "This report helps explain your energy usage and costs. testevp", "Name": "Your Energy Report.pdf", "FileSourceId": "1", "CreateDate": "2016-10-19T17:28:02.733", "RelativeTime": "19 days ago", "ErrorStatus": false }], ' +
            '"ReportsUser": [{ "Id": "502", "CustomerId": "993774751", "AccountId": "1218337093", "PremiseId": "1218337000", "RoleId": "0", "UserId": null, "TypeId": "1", "Title": "CustRept", "Description": "this is test my energy report sample", "Name": "1.pdf", "FileSourceId": "1", "CreateDate": "2016-11-07T20:06:42.113", "RelativeTime": "1 day ago", "ErrorStatus": false }, ' +
            '{ "Id": "667", "CustomerId": "993774751", "AccountId": "1218337093", "PremiseId": "1218337000", "RoleId": "0", "UserId": null, "TypeId": "1", "Title": "CustRept2", "Description": "this is test my energy report sample", "Name": "1.pdf", "FileSourceId": "1", "CreateDate": "2016-09-27T20:06:42.113", "RelativeTime": "one month ago", "ErrorStatus": false }], "isCSR": true }');
        app.result = json;
        app.enableLoader = false;
        fixture.detectChanges();
        var link = fixture.nativeElement.querySelectorAll("a.xs-ml-0.xs-mr-0.xs-pl-0.xs-pr-0.button-small");
        link[0].click();
        fixture.detectChanges();
        expect(fixture.elementRef.nativeElement.nextSibling.textContent).toContain('Confirm');
    }));
});
//# sourceMappingURL=reportlist.dom.spec.js.map