"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var platform_browser_1 = require("@angular/platform-browser");
var http_1 = require("@angular/http");
var testing_2 = require("@angular/platform-browser-dynamic/testing");
var subscription_1 = require("../../Subscription/subscription");
var subscription_service_1 = require("../../Subscription/subscription.service");
var app_service_1 = require("../../app.service");
var loader_1 = require("../../Loader/loader");
var pipe_1 = require("../../Helper/pipe");
var MockAppService = (function (_super) {
    __extends(MockAppService, _super);
    function MockAppService() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.params = [{ tabKey: "tab.waterportal", parameters: "YbUGYEN1OhOANtb1zjkfy/hq/cI+rQ4HQlLIgZfqZL7OdgsF8ZKqzAw3eqO0PtMqSEW2CiNa9iOob71Xc8rETBZ0b0gp5N9mczstPSIn8k41jMUEo/KUHHOvQMT89omslD8Zxrj3CSA5CIYCEkMqiRFvtTquCvGi0GQTcZ19rqt1WAisugqWAXqjCKDEgwEAtTxKtAADOS1GKFAOc8O/vAn6PK0IcfRMmQ580IKEVhAnIE2UI2E8B3TjBR426Obt+NM4ppU46hcpbwY0IUil1l8S7I0ffzpnpfuWCDxLP5p3CKQpGI/ha6jU1ounQFk1LDYSoC/j083c3i8OHlB4zy5Z44Pz+xfq4GU4zqXNZoIfipeRuVHB1pIC/dqQ3UX4kCxUJhFCZgjYozjpi2KUYYiOYV8WQQkeTybUkk+cf/UW4kOfoICCU4cVjSafdJUifaRAj+RaS1HTGjns8E4xrUYF4nGPg7wOFTnOxfdtJap5GCg9GZKPf7KBDzfDdV2d8Uyb1DwBPDMUhix9VTHKvPEjq3Bzl5zHUmaA2goT6QEtBT+x6by41BU2txhbyKY1", errorMessage: "" }];
        _this.deepLinkVariables = [{ deepLinkWidget: "", deepLinkWidgetTab: "", deepLinkWidgetSection: "" }];
        return _this;
    }
    MockAppService.prototype.getParams = function () {
        return this.params;
    };
    MockAppService.prototype.getDeepLinkVariables = function () {
        return this.deepLinkVariables;
    };
    return MockAppService;
}(app_service_1.AppService));
var MockSubscriptionService = (function (_super) {
    __extends(MockSubscriptionService, _super);
    function MockSubscriptionService() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.unit = "unittest";
        return _this;
    }
    MockSubscriptionService.prototype.getSubscriptions = function (tabKey, params) {
        return this.unit;
    };
    return MockSubscriptionService;
}(subscription_service_1.SubscriptionService));
describe('Subscription DOM Tests', function () {
    var originalTimeout;
    beforeEach(testing_1.async(function () {
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 20000;
        testing_1.TestBed.resetTestEnvironment();
        testing_1.TestBed.initTestEnvironment(testing_2.BrowserDynamicTestingModule, testing_2.platformBrowserDynamicTesting());
        core_1.ElementRef;
        testing_1.TestBed.configureTestingModule({
            imports: [platform_browser_1.BrowserModule, forms_1.FormsModule, http_1.HttpModule],
            declarations: [subscription_1.Subscription, loader_1.Loader, pipe_1.ObjToArr]
        })
            .overrideComponent(subscription_1.Subscription, {
            remove: { providers: [app_service_1.AppService, subscription_service_1.SubscriptionService] },
            add: {
                providers: [
                    { provide: app_service_1.AppService, useClass: MockAppService },
                    { provide: subscription_service_1.SubscriptionService, useClass: MockSubscriptionService }
                ]
            }
        })
            .compileComponents();
    }));
    it('should get Notification settings', testing_1.async(function () {
        var fixture = testing_1.TestBed.createComponent(subscription_1.Subscription);
        var app = fixture.componentInstance;
        var json = JSON.parse('{"Title":"Notification Settings","IntroText":"Alert options.","SubTitle":"Bill to Date Program","Footer":"Sample footer","FooterLinkText":"Next steps","ShowTitle":"true","ShowIntro":"true","ShowSubTitle":"true","FooterLink":"https://www.aclara.com","ShowFooterLink":"true","ShowFooterText":"true","SaveButtonLabel":"Save","SaveSuccess":"Notification settings saved.","SaveFailed":"Settings not saved.","NoData":null,"NoSubscription":false,"NotCompleteMessage":"Please answer this question","Disclaimer":"  ","CancelButtonLabel":"","Insights":"program1.billtodate,program1.accountprojectedcost,program1.serviceleveltieredthresholdapproaching,program1.servicelevelcostthreshold,program1.servicelevelusagethreshold","Channels":"email,sms","ChannelsText":"Email,Text","ServiceCount":3,"Notifications":[{"Name":"program1.billtodate","Title":"Send me a daily alert showing my bill-to-date.","Description":"Send me a daily alert showing my bill-to-date.","ImageUrl":null,"ImageTitle":null,"Icon":null,"Link":"","LinkText":"","LinkType":"","Threshold":0,"IsSelected":true,"Level":"account","AccountId":"1218337093","PremiseId":null,"ServiceId":null,"AllowedChannels":"file,email,sms,emailandsms","ThresholdType":null,"ThresholdMin":null,"ThresholdMax":null,"UomKey":null,"UomText":null,"CurrencySymbol":null,"Channel":"email","Channels":"email,sms,emailandsms","EmailSelected":true,"SmsSelected":false,"EmailEnabled":true,"SmsEnabled":true,"ThresholdDisabled":false,"CommodityKey":null,"ServiceLabel":" Usage (Service )","SingleServiceLabel":" Usage","InvalidRange":false,"InvalidRequired":false,"RangeErrorMessage":null,"ShowLabel":true},{"Name":"program1.accountprojectedcost","Title":"Send me a daily alert showing my projected bill amount.","Description":"Send me a daily alert showing my projected bill amount.","ImageUrl":null,"ImageTitle":null,"Icon":null,"Link":"","LinkText":"","LinkType":"","Threshold":0,"IsSelected":false,"Level":"account","AccountId":"1218337093","PremiseId":null,"ServiceId":null,"AllowedChannels":"file,email,sms,emailandsms","ThresholdType":null,"ThresholdMin":null,"ThresholdMax":null,"UomKey":null,"UomText":null,"CurrencySymbol":null,"Channel":"","Channels":"email,sms,emailandsms","EmailSelected":false,"SmsSelected":false,"EmailEnabled":true,"SmsEnabled":true,"ThresholdDisabled":false,"CommodityKey":null,"ServiceLabel":" Usage (Service )","SingleServiceLabel":" Usage","InvalidRange":false,"InvalidRequired":false,"RangeErrorMessage":null,"ShowLabel":false},{"Name":"program1.servicelevelcostthreshold","Title":"Notify me if my costs exceed this amount.","Description":"Cost","ImageUrl":null,"ImageTitle":null,"Icon":null,"Link":"","LinkText":"","LinkType":"","Threshold":25,"IsSelected":false,"Level":"service","AccountId":"1218337093","PremiseId":"1218337000","ServiceId":"SP_189354704","AllowedChannels":"file,email,sms,emailandsms","ThresholdType":"cost","ThresholdMin":1,"ThresholdMax":1000000,"UomKey":null,"UomText":null,"CurrencySymbol":"$","Channel":"","Channels":"email,sms,emailandsms","EmailSelected":false,"SmsSelected":false,"EmailEnabled":true,"SmsEnabled":true,"ThresholdDisabled":false,"CommodityKey":"electric","ServiceLabel":"Electric Usage (Service SP_189354704)","SingleServiceLabel":"Electric Usage","InvalidRange":false,"InvalidRequired":false,"RangeErrorMessage":"Please enter a value between 1.0 – 1000000.0.","ShowLabel":true},{"Name":"program1.servicelevelusagethreshold","Title":"Notify me if my usage exceeds this quantity ","Description":"Notify me if my usage exceeds this quantity.","ImageUrl":null,"ImageTitle":null,"Icon":null,"Link":"","LinkText":"","LinkType":"","Threshold":500,"IsSelected":false,"Level":"service","AccountId":"1218337093","PremiseId":"1218337000","ServiceId":"SP_189354704","AllowedChannels":"file,email,sms,emailandsms","ThresholdType":"usage","ThresholdMin":0.1,"ThresholdMax":1000000,"UomKey":"kwh","UomText":"kWh","CurrencySymbol":null,"Channel":"","Channels":"email,sms,emailandsms","EmailSelected":false,"SmsSelected":false,"EmailEnabled":true,"SmsEnabled":true,"ThresholdDisabled":false,"CommodityKey":"electric","ServiceLabel":"Electric Usage (Service SP_189354704)","SingleServiceLabel":"Electric Usage","InvalidRange":false,"InvalidRequired":false,"RangeErrorMessage":"Please enter a value between 0.1 – 1000000.0.","ShowLabel":false},{"Name":"program1.serviceleveltieredthresholdapproaching","Title":"Notify me when I am approaching a tier pricing limit.","Description":"This notification will let you know when your total usage to-date for the bill period is about to cross over a tiered pricing boundary. This will help you manage your costs because you will know when your usage is triggering higher prices.","ImageUrl":null,"ImageTitle":null,"Icon":null,"Link":"","LinkText":"","LinkType":"","Threshold":20,"IsSelected":false,"Level":"service","AccountId":"1218337093","PremiseId":"1218337000","ServiceId":"SP_189354704","AllowedChannels":"file,email,sms,emailandsms","ThresholdType":"percent","ThresholdMin":1,"ThresholdMax":100,"UomKey":null,"UomText":null,"CurrencySymbol":null,"Channel":"","Channels":"email,sms,emailandsms","EmailSelected":false,"SmsSelected":false,"EmailEnabled":true,"SmsEnabled":true,"ThresholdDisabled":false,"CommodityKey":"electric","ServiceLabel":"Electric Usage (Service SP_189354704)","SingleServiceLabel":"Electric Usage","InvalidRange":false,"InvalidRequired":false,"RangeErrorMessage":null,"ShowLabel":false},{"Name":"program1.servicelevelcostthreshold","Title":"Notify me if my costs exceed this amount.","Description":"Cost","ImageUrl":null,"ImageTitle":null,"Icon":null,"Link":"","LinkText":"","LinkType":"","Threshold":25,"IsSelected":true,"Level":"service","AccountId":"1218337093","PremiseId":"1218337000","ServiceId":"SP_M_1218337000_gas","AllowedChannels":"file,email,sms,emailandsms","ThresholdType":"cost","ThresholdMin":1,"ThresholdMax":1000000,"UomKey":null,"UomText":null,"CurrencySymbol":"$","Channel":"email","Channels":"email,sms,emailandsms","EmailSelected":true,"SmsSelected":false,"EmailEnabled":true,"SmsEnabled":true,"ThresholdDisabled":false,"CommodityKey":"electric","ServiceLabel":"Electric Usage (Service SP_M_1218337000_gas)","SingleServiceLabel":"Electric Usage","InvalidRange":false,"InvalidRequired":false,"RangeErrorMessage":"Please enter a value between 1.0 – 1000000.0.","ShowLabel":true},{"Name":"program1.servicelevelusagethreshold","Title":"Notify me if my usage exceeds this quantity ","Description":"Notify me if my usage exceeds this quantity.","ImageUrl":null,"ImageTitle":null,"Icon":null,"Link":"","LinkText":"","LinkType":"","Threshold":500,"IsSelected":false,"Level":"service","AccountId":"1218337093","PremiseId":"1218337000","ServiceId":"SP_M_1218337000_gas","AllowedChannels":"file,email,sms,emailandsms","ThresholdType":"usage","ThresholdMin":0.1,"ThresholdMax":1000000,"UomKey":"ccf","UomText":"CCF","CurrencySymbol":null,"Channel":"","Channels":"email,sms,emailandsms","EmailSelected":false,"SmsSelected":false,"EmailEnabled":true,"SmsEnabled":true,"ThresholdDisabled":false,"CommodityKey":"electric","ServiceLabel":"Electric Usage (Service SP_M_1218337000_gas)","SingleServiceLabel":"Electric Usage","InvalidRange":false,"InvalidRequired":false,"RangeErrorMessage":"Please enter a value between 0.1 – 1000000.0.","ShowLabel":false},{"Name":"program1.serviceleveltieredthresholdapproaching","Title":"Notify me when I am approaching a tier pricing limit.","Description":"This notification will let you know when your total usage to-date for the bill period is about to cross over a tiered pricing boundary. This will help you manage your costs because you will know when your usage is triggering higher prices.","ImageUrl":null,"ImageTitle":null,"Icon":null,"Link":"","LinkText":"","LinkType":"","Threshold":20,"IsSelected":false,"Level":"service","AccountId":"1218337093","PremiseId":"1218337000","ServiceId":"SP_M_1218337000_gas","AllowedChannels":"file,email,sms,emailandsms","ThresholdType":"percent","ThresholdMin":1,"ThresholdMax":100,"UomKey":null,"UomText":null,"CurrencySymbol":null,"Channel":"","Channels":"email,sms,emailandsms","EmailSelected":false,"SmsSelected":false,"EmailEnabled":true,"SmsEnabled":true,"ThresholdDisabled":false,"CommodityKey":"electric","ServiceLabel":"Electric Usage (Service SP_M_1218337000_gas)","SingleServiceLabel":"Electric Usage","InvalidRange":false,"InvalidRequired":false,"RangeErrorMessage":null,"ShowLabel":false}]}');
        app.result = json;
        fixture.detectChanges();
        expect(true).toBe(true);
    }));
});
//# sourceMappingURL=subscription.dom.spec.js.map