﻿import { inject, async, TestBed } from "@angular/core/testing";
import { Http, HttpModule, XHRBackend, Response, ResponseOptions } from "@angular/http";
import { MockBackend, MockConnection } from "@angular/http/testing";
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { SubscriptionService } from "../../Subscription/subscription.service";

describe('TestSubscriptionService -- Mock', () => {
    beforeEach(async(() => {
        TestBed.resetTestEnvironment();

        TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting());
        TestBed.configureTestingModule({
            imports: [HttpModule],
            providers: [
                SubscriptionService,
                { provide: XHRBackend, useClass: MockBackend }
            ]
        }).compileComponents();;
    }));

    it('can instantiate service when inject service',
        inject([SubscriptionService], (service: SubscriptionService) => {
            expect(service instanceof SubscriptionService).toBe(true);
        }));

    it('can provide the mockBackend as XHRBackend',
        inject([XHRBackend], (backend: MockBackend) => {
            expect(backend).not.toBeNull('backend should be provided');
        }));


    let backend: MockBackend;
    let service: SubscriptionService;
    let response: Response;

    beforeEach(inject([Http, XHRBackend], (http: Http, be: MockBackend) => {
        backend = be;
        service = new SubscriptionService(http);
        let options = new ResponseOptions({
            body: [
                {
                    id: "QA87HomeProf",
                    contentRendered: "<p><b>TestSubscription</b></p>",
                    contentMarkdown: "*Hi there*"
                }
            ]
        });
        response = new Response(options);
    }));

    it('should get subscription', async(inject([], () => {
        backend.connections.subscribe((c: MockConnection) => c.mockRespond(response));

        service.getSubscriptions('', '').subscribe((blogs) => {
            expect(blogs.length).toBe(1);
            expect(blogs[0].id).toBe("QA87HomeProf");
            expect(blogs[0].contentRendered).toBe("<p><b>TestSubscription</b></p>");
        });
    })));

    it('should post profile', async(inject([], () => {
        backend.connections.subscribe((c: MockConnection) => c.mockRespond(response));

        service.postSubscriptions('').subscribe((blogs) => {
            expect(blogs.length).toBe(1);
            expect(blogs[0].id).toBe("QA87HomeProf");
            expect(blogs[0].contentRendered).toBe("<p><b>TestSubscription</b></p>");
        });
    })));
});