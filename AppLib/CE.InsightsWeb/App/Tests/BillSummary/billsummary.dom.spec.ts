﻿import { async, TestBed } from "@angular/core/testing";
import { ElementRef } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { BrowserModule, By } from "@angular/platform-browser";
import { HttpModule } from "@angular/http";
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from "@angular/platform-browser-dynamic/testing";
import { BillSummary } from "../../BillSummary/billsummary";
import { BillSummaryService } from "../../BillSummary/billsummary.service";
import { AppService, IAppParams } from "../../app.service";
import { Loader } from "../../Loader/loader";
import { ObjToArr } from "../../Helper/pipe";

//mock bill history app services 
class MockAppService extends AppService {
    public params: Array<IAppParams> = [{ tabKey: "tab.billstatement", parameters: "YbUGYEN1OhOANtb1zjkfy/hq/cI+rQ4HQlLIgZfqZL7OdgsF8ZKqzAw3eqO0PtMqSEW2CiNa9iOob71Xc8rETBZ0b0gp5N9mczstPSIn8k41jMUEo/KUHHOvQMT89omslD8Zxrj3CSA5CIYCEkMqiRFvtTquCvGi0GQTcZ19rqt1WAisugqWAXqjCKDEgwEAtTxKtAADOS1GKFAOc8O/vAn6PK0IcfRMmQ580IKEVhAnIE2UI2E8B3TjBR426Obt+NM4ppU46hcpbwY0IUil1ojq/lN/d8AzTriNnHjy3eVb2OyYDuFYOFopUUpX/v1qUrVgV7VD96UW2zEGS5zRb4N0/RHYCSwUE3qgwkzyOjUaOX3L9Jrwk9e4tNouzbEbjbXPjTwDcqbqUBk/Wn5eik5/64JISCsuqUNSz5GbjMN4F4WYoFT37PtjCq7KsV16YccmF3NObF9MfU45pgPyRI8Qas6g2fG/XdChanb7sRPGDj0C0do7/86T3PyOYJoV4v3+afpi7+iSGcfnYR7bhg==", errorMessage: "" }];

    getParams(): Array<IAppParams> {
        return this.params;
    }
}

class MockBillSummaryService extends BillSummaryService {
    public res: any;
    public unit: any = "unittest";
    getBillSummary(tabKey, params) {
        return this.unit;
    }
}

describe('Bill Summary DOM Tests',
() => {
    let el: HTMLElement;
    beforeEach(async(() => {
        TestBed.resetTestEnvironment();
        TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting());
        ElementRef;
        TestBed.configureTestingModule({
                imports: [BrowserModule, FormsModule, HttpModule],
                declarations: [BillSummary, Loader, ObjToArr]
            })
            .overrideComponent(BillSummary,
            {
                remove: { providers: [AppService, BillSummaryService] },
                add: {
                    providers: [
                        { provide: AppService, useClass: MockAppService },
                        { provide: BillSummaryService, useClass: MockBillSummaryService }
                    ]
                }
            })
            .compileComponents();
    }));

    it('No Bills should get generic message',
        async(() => {
            const fixture = TestBed.createComponent(BillSummary);
            const app = fixture.componentInstance;
            app.enableLoader = false;
            var json = JSON
                .parse('{"TotalGasUse":null,"TotalElectricUse":null,"TotalWaterUse":null,"AvgDailyCostText":null,"NumberOfDaysText":null, "AvgTempText":null, "AmountDueText":null, "ViewBillHistory":null, "ShowBillHistoryLink":null, "Commodities":null, "BillHistoryLink":null, "CurrencyFormat":null, "NoBillsText":"No bills are available.", "TotalGasUsed":0, "TotalElectricityUsed":0, "TotalWaterUsed":0, "GasUOM":null, "ElectricUOM":null, "WaterUOM":null, "AvgDailyCost":null, "NumberOfDays":0, "AvgTemp":0, "AmountDue":null, "BillStartDateGas":null, "BillEndDateGas":null, "BillStartDateElectric":null, "BillEndDateElectric":null, "BillStartDateWater":null, "BillEndDateWater":null, "NoBills":true, "DueDate":null, "TempFormat":null, "RateClassGas":null, "RateClassElectric":null, "RateClassWater":null, "RateClassSewer":null, "RateMismatchGas":false, "RateMismatchElectric":false, "RateMismatchWater":false, "RateMismatchSewer":false, "Title":"", "IntroText":"", "SubTitle":"", "ShowTitle":"false", "ShowIntro":"", "ShowSubTitle":"", "Footer":"Sample footer text.", "FooterLink":"Page?enc=Lbe", "FooterLinkText":"View Full Billing History", "FooterLinkType":null, "ShowFooterText":"false", "ShowFooterLink":"false" }');
            app.result = json;
            fixture.detectChanges();
            const txtNoBills = fixture.nativeElement.querySelectorAll("strong");
            expect(txtNoBills[0].innerText).toBe("No bills are available.");
        }));

    it('should get Bill Summary all commodities Totals Text',
        async(() => {
            const fixture = TestBed.createComponent(BillSummary);
            const app = fixture.componentInstance;
            app.enableLoader = false;
            var json = JSON
                .parse('{"TotalGasUse":"Total Gas Used", "TotalElectricUse":"Total Electricity Used", "TotalWaterUse":"Total Water Used","AvgDailyCostText":"Average Daily Cost","NumberOfDaysText":"Number of Days","AvgTempText":"Average Temp","AmountDueText":"Amount Due: ","ViewBillHistory":"View Full Billing History","ShowBillHistoryLink":"true", "Commodities":"gas, electric,water", "BillHistoryLink":"Px","CurrencyFormat":null,"NoBillsText":null,"TotalGasUsed":767,"TotalElectricityUsed":152, "TotalWaterUsed":2250, "GasUOM":"Therms", "ElectricUOM":"kWh", "WaterUOM":"gal", "AvgDailyCost":"$13.63","NumberOfDays":30,"AvgTemp":65,"AmountDue":"$408.92","BillStartDateGas":"11/ 01 / 2016","BillEndDateGas":"12/01/2016", "BillStartDateElectric":"11/01/2016", "BillEndDateElectric":"12/01/2016", "BillStartDateWater":"11/01/2016", "BillEndDateWater":"12/01/2016", "NoBills":false, "DueDate":null, "TempFormat":"F", "RateClassGas":"BAMI-G", "RateClassElectric":"binTierM", "RateClassWater":"binWater", "RateClassSewer":null, "RateMismatchGas":false, "RateMismatchElectric":false, "RateMismatchWater":false, "RateMismatchSewer":false, "Title":"", "IntroText":"", "SubTitle":"", "ShowTitle":"false", "ShowIntro":"", "ShowSubTitle":"", "Footer":"Sample footer text.", "FooterLink":"Page?enc=LbeNmuyExXI1tZ4xHiibWB2y32qq8NpGuV0GoKvqvDwlAMJVuHaT3jW3/E8APZFrEOm3eoRMB8Yp4fKYEKA10yTBr8IMDkfwoFfArFhGOAKG4tK3Ro7BaYKZCmTgSHdhNNvFSO","FooterLinkText":"View Full Billing History","FooterLinkType":null,"ShowFooterText":"false", "ShowFooterLink":"false" }');

            app.result = json;
            fixture.detectChanges();
            const rowComs = fixture.nativeElement.querySelectorAll("li");
            expect(rowComs["0"].innerText).toContain("Total Gas Used");
            expect(rowComs["1"].innerText).toContain("Total Electricity Used");
            expect(rowComs["2"].innerText).toContain("Total Water Used");
            const totalsCom = fixture.nativeElement.querySelectorAll("p");
            expect(totalsCom.length).toBe(7);;
        }));


    it('should get Bill Summary Gas Total Text with No Other Commodity totals',
        async(() => {
            const fixture = TestBed.createComponent(BillSummary);
            const app = fixture.componentInstance;
            app.enableLoader = false;
            var json = JSON
                .parse('{"TotalGasUse":"Total Gas Used","TotalElectricUse":"Total Electricity Used","TotalWaterUse":"Total WaterUsed","AvgDailyCostText":"Average Daily Cost","NumberOfDaysText":"Number of Days","AvgTempText":"AverageTemp","AmountDueText":"Amount Due: ","ViewBillHistory":"View Full Billing History","ShowBillHistoryLink":"true", "Commodities":"gas, electric,water", "BillHistoryLink":"Page?enc=LbeNmuyExXI1tZ4xHiibWB2y32qq8NpGuV0GoKvtE","CurrencyFormat":null, "NoBillsText":null, "TotalGasUsed":20, "TotalElectricityUsed":0, "TotalWaterUsed":0, "GasUOM":"Therms", "ElectricUOM":"", "WaterUOM":"", "AvgDailyCost":"$1.58", "NumberOfDays":31, "AvgTemp":53, "AmountDue":"$48.94","BillStartDateGas":"12/ 20 / 2015","BillEndDateGas":"01/ 20 / 2016","BillStartDateElectric":null,"BillEndDateElectric":null, "BillStartDateWater":null, "BillEndDateWater":null, "NoBills":false, "DueDate":null, "TempFormat":"F", "RateClassGas":"BAMI-G", "RateClassElectric":null, "RateClassWater":null, "RateClassSewer":null, "RateMismatchGas":false, "RateMismatchElectric":false, "RateMismatchWater":false, "RateMismatchSewer":false, "Title":"", "IntroText":"", "SubTitle":"", "ShowTitle":"false", "ShowIntro":"", "ShowSubTitle":"", "Footer":"Sample footer text.", "FooterLink":"Page?enc=LbeNmuyExXI1tZ4xHiibWB2y32qq8NpGuV0GoKvqvDwlAMJVuHaT3jW3/E8APZFrEOm3eoRMB8YBFgnIWcQ=","FooterLinkText":"View Full Billing History", "FooterLinkType":null, "ShowFooterText":"false", "ShowFooterLink":"false"}');
            app.result = json;
            fixture.detectChanges();
            const rowComs = fixture.nativeElement.querySelectorAll("li");
            expect(rowComs["0"].innerText).toContain("Total Gas Used");
            const totalsCom = fixture.nativeElement.querySelectorAll("p");
            expect(totalsCom.length).toBe(5);
        }));

    it('should get View Full Bill History Link when enabled',
        async(() => {
            const fixture = TestBed.createComponent(BillSummary);
            const app = fixture.componentInstance;
            app.enableLoader = false;
            var json = JSON
                .parse('{"TotalGasUse":"Total Gas Used", "TotalElectricUse":"Total Electricity Used", "TotalWaterUse":"Total Water Used","AvgDailyCostText":"Average Daily Cost","NumberOfDaysText":"Number of Days","AvgTempText":"Average Temp","AmountDueText":"Amount Due: ","ViewBillHistory":"View Full Billing History","ShowBillHistoryLink":"true", "Commodities":"gas, electric,water", "BillHistoryLink":"Px","CurrencyFormat":null,"NoBillsText":null,"TotalGasUsed":767,"TotalElectricityUsed":152, "TotalWaterUsed":2250, "GasUOM":"Therms", "ElectricUOM":"kWh", "WaterUOM":"gal", "AvgDailyCost":"$13.63","NumberOfDays":30,"AvgTemp":65,"AmountDue":"$408.92","BillStartDateGas":"11/ 01 / 2016","BillEndDateGas":"12/01/2016", "BillStartDateElectric":"11/01/2016", "BillEndDateElectric":"12/01/2016", "BillStartDateWater":"11/01/2016", "BillEndDateWater":"12/01/2016", "NoBills":false, "DueDate":null, "TempFormat":"F", "RateClassGas":"BAMI-G", "RateClassElectric":"binTierM", "RateClassWater":"binWater", "RateClassSewer":null, "RateMismatchGas":false, "RateMismatchElectric":false, "RateMismatchWater":false, "RateMismatchSewer":false, "Title":"", "IntroText":"", "SubTitle":"", "ShowTitle":"false", "ShowIntro":"", "ShowSubTitle":"", "Footer":"Sample footer text.", "FooterLink":"Page?enc=LbeNmuyExXI1tZ4xHiibWB2y32qq8NpGuV0GoKvqvDwlAMJVuHaT3jW3/E8APZFrEOm3eoRMB8Yp4fKYEKA10yTBr8IMDkfwoFfArFhGOAKG4tK3Ro7BaYKZCmTgSHdhNNvFSO","FooterLinkText":"View Full Billing History","FooterLinkType":null,"ShowFooterText":"false", "ShowFooterLink":"false" }');

            app.result = json;
            fixture.detectChanges();
            const linkBillHistory = fixture.nativeElement.querySelectorAll("a");
            expect(linkBillHistory.length).toBe(1);
        }));

    it('should not find Footer when not enabled',
        async(() => {
            const fixture = TestBed.createComponent(BillSummary);
            const app = fixture.componentInstance;
            app.enableLoader = false;
            var json = JSON
                .parse('{"TotalGasUse":"Total Gas Used", "TotalElectricUse":"Total Electricity Used", "TotalWaterUse":"Total Water Used","AvgDailyCostText":"Average Daily Cost","NumberOfDaysText":"Number of Days","AvgTempText":"Average Temp","AmountDueText":"Amount Due: ","ViewBillHistory":"View Full Billing History","ShowBillHistoryLink":"true", "Commodities":"gas, electric,water", "BillHistoryLink":"Px","CurrencyFormat":null,"NoBillsText":null,"TotalGasUsed":767,"TotalElectricityUsed":152, "TotalWaterUsed":2250, "GasUOM":"Therms", "ElectricUOM":"kWh", "WaterUOM":"gal", "AvgDailyCost":"$13.63","NumberOfDays":30,"AvgTemp":65,"AmountDue":"$408.92","BillStartDateGas":"11/ 01 / 2016","BillEndDateGas":"12/01/2016", "BillStartDateElectric":"11/01/2016", "BillEndDateElectric":"12/01/2016", "BillStartDateWater":"11/01/2016", "BillEndDateWater":"12/01/2016", "NoBills":false, "DueDate":null, "TempFormat":"F", "RateClassGas":"BAMI-G", "RateClassElectric":"binTierM", "RateClassWater":"binWater", "RateClassSewer":null, "RateMismatchGas":false, "RateMismatchElectric":false, "RateMismatchWater":false, "RateMismatchSewer":false, "Title":"", "IntroText":"", "SubTitle":"", "ShowTitle":"false", "ShowIntro":"", "ShowSubTitle":"", "Footer":"Sample footer text.", "FooterLink":"Page?enc=LbeNmuyExXI1tZ4xHiibWB2y32qq8NpGuV0GoKvqvDwlAMJVuHaT3jW3/E8APZFrEOm3eoRMB8Yp4fKYEKA10yTBr8IMDkfwoFfArFhGOAKG4tK3Ro7BaYKZCmTgSHdhNNvFSO","FooterLinkText":"View Full Billing History","FooterLinkType":null,"ShowFooterText":"false", "ShowFooterLink":"false" }');
            app.result = json;
            fixture.detectChanges();
            // query for the footer <footerlink> by CSS element selector
            var lnkFooter = fixture.debugElement.query(By.css(".footerlink"));
            expect(lnkFooter).toBe(null);

        }));

    it('should get Footer when enabled',
        async(() => {
            const fixture = TestBed.createComponent(BillSummary);
            const app = fixture.componentInstance;
            app.enableLoader = false;
            var json = JSON
                .parse('{"TotalGasUse":"Total Gas Used", "TotalElectricUse":"Total Electricity Used", "TotalWaterUse":"Total Water Used","AvgDailyCostText":"Average Daily Cost","NumberOfDaysText":"Number of Days","AvgTempText":"AverageTemp","AmountDueText":"Amount Due: ","ViewBillHistory":"View Full Billing History","ShowBillHistoryLink":"false", "Commodities":"gas, electric,water", "BillHistoryLink":"Page?enc=LbeNmuyExXI1tZ4xHiibWB2ZbQ == ","CurrencyFormat":null,"NoBillsText":null,"TotalGasUsed":0,"TotalElectricityUsed":550, "TotalWaterUsed":4500, "GasUOM":"", "ElectricUOM":"kWh", "WaterUOM":"gal", "AvgDailyCost":"$7.18", "NumberOfDays":30, "AvgTemp":72, "AmountDue":"$215.50", "BillStartDateGas":null, "BillEndDateGas":null, "BillStartDateElectric":"06/01/2015", "BillEndDateElectric":"07/01/2015", "BillStartDateWater":"06/01/2015", "BillEndDateWater":"07/01/2015", "NoBills":false, "DueDate":null, "TempFormat":"F", "RateClassGas":null, "RateClassElectric":"GR", "RateClassWater":"GR", "RateClassSewer":null, "RateMismatchGas":false, "RateMismatchElectric":false, "RateMismatchWater":false, "RateMismatchSewer":false, "Title":"", "IntroText":"", "SubTitle":"", "ShowTitle":"false", "ShowIntro":"", "ShowSubTitle":"", "Footer":"Sample footer.", "FooterLink":"Page?enc=LbeNmu8","FooterLinkText":"Footer Text","FooterLinkType":null,"ShowFooterText":"true", "ShowFooterLink":"true" }');
            app.result = json;
            fixture.detectChanges();
            // query for the footer <footerlink> by CSS element selector
            var lnkFooter = fixture.debugElement.query(By.css(".footerlink"));
            expect(lnkFooter.nativeElement.innerText).toBe("Footer Text");
        }));


    it('should find Amount Due in Strong format with dollar amount',
        async(() => {
            const fixture = TestBed.createComponent(BillSummary);
            const app = fixture.componentInstance;
            app.enableLoader = false;
            var json = JSON
                .parse('{"TotalGasUse":"Total Gas Used", "TotalElectricUse":"Total Electricity Used", "TotalWaterUse":"Total Water Used","AvgDailyCostText":"Average Daily Cost","NumberOfDaysText":"Number of Days","AvgTempText":"AverageTemp","AmountDueText":"Amount Due: ","ViewBillHistory":"View Full Billing History","ShowBillHistoryLink":"false", "Commodities":"gas, electric,water", "BillHistoryLink":"Page?enc=LbeNmuyExXI1tZ4xHiibWB2ZbQ == ","CurrencyFormat":null,"NoBillsText":null,"TotalGasUsed":0,"TotalElectricityUsed":550, "TotalWaterUsed":4500, "GasUOM":"", "ElectricUOM":"kWh", "WaterUOM":"gal", "AvgDailyCost":"$7.18", "NumberOfDays":30, "AvgTemp":72, "AmountDue":"$215.50", "BillStartDateGas":null, "BillEndDateGas":null, "BillStartDateElectric":"06/01/2015", "BillEndDateElectric":"07/01/2015", "BillStartDateWater":"06/01/2015", "BillEndDateWater":"07/01/2015", "NoBills":false, "DueDate":null, "TempFormat":"F", "RateClassGas":null, "RateClassElectric":"GR", "RateClassWater":"GR", "RateClassSewer":null, "RateMismatchGas":false, "RateMismatchElectric":false, "RateMismatchWater":false, "RateMismatchSewer":false, "Title":"", "IntroText":"", "SubTitle":"", "ShowTitle":"false", "ShowIntro":"", "ShowSubTitle":"", "Footer":"Sample footer.", "FooterLink":"Page?enc=LbeNmu8","FooterLinkText":"Footer Text","FooterLinkType":null,"ShowFooterText":"true", "ShowFooterLink":"true" }');
            app.result = json;
            fixture.detectChanges();
            // query for the footer <footerlink> by CSS element selector
            const txtAmtDue = fixture.nativeElement.querySelectorAll("strong");
            expect(txtAmtDue.length).toBe(2);
            expect(txtAmtDue[1].innerText).toBe("$215.50");
        }));


    });