"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var platform_browser_1 = require("@angular/platform-browser");
var http_1 = require("@angular/http");
var testing_2 = require("@angular/platform-browser-dynamic/testing");
var billsummary_1 = require("../../BillSummary/billsummary");
var billsummary_service_1 = require("../../BillSummary/billsummary.service");
var app_service_1 = require("../../app.service");
var loader_1 = require("../../Loader/loader");
var pipe_1 = require("../../Helper/pipe");
var MockAppService = (function (_super) {
    __extends(MockAppService, _super);
    function MockAppService() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.params = [{ tabKey: "tab.billstatement", parameters: "YbUGYEN1OhOANtb1zjkfy/hq/cI+rQ4HQlLIgZfqZL7OdgsF8ZKqzAw3eqO0PtMqSEW2CiNa9iOob71Xc8rETBZ0b0gp5N9mczstPSIn8k41jMUEo/KUHHOvQMT89omslD8Zxrj3CSA5CIYCEkMqiRFvtTquCvGi0GQTcZ19rqt1WAisugqWAXqjCKDEgwEAtTxKtAADOS1GKFAOc8O/vAn6PK0IcfRMmQ580IKEVhAnIE2UI2E8B3TjBR426Obt+NM4ppU46hcpbwY0IUil1ojq/lN/d8AzTriNnHjy3eVb2OyYDuFYOFopUUpX/v1qUrVgV7VD96UW2zEGS5zRb4N0/RHYCSwUE3qgwkzyOjUaOX3L9Jrwk9e4tNouzbEbjbXPjTwDcqbqUBk/Wn5eik5/64JISCsuqUNSz5GbjMN4F4WYoFT37PtjCq7KsV16YccmF3NObF9MfU45pgPyRI8Qas6g2fG/XdChanb7sRPGDj0C0do7/86T3PyOYJoV4v3+afpi7+iSGcfnYR7bhg==", errorMessage: "" }];
        return _this;
    }
    MockAppService.prototype.getParams = function () {
        return this.params;
    };
    return MockAppService;
}(app_service_1.AppService));
var MockBillSummaryService = (function (_super) {
    __extends(MockBillSummaryService, _super);
    function MockBillSummaryService() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.unit = "unittest";
        return _this;
    }
    MockBillSummaryService.prototype.getBillSummary = function (tabKey, params) {
        return this.unit;
    };
    return MockBillSummaryService;
}(billsummary_service_1.BillSummaryService));
describe('Bill Summary DOM Tests', function () {
    var el;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.resetTestEnvironment();
        testing_1.TestBed.initTestEnvironment(testing_2.BrowserDynamicTestingModule, testing_2.platformBrowserDynamicTesting());
        core_1.ElementRef;
        testing_1.TestBed.configureTestingModule({
            imports: [platform_browser_1.BrowserModule, forms_1.FormsModule, http_1.HttpModule],
            declarations: [billsummary_1.BillSummary, loader_1.Loader, pipe_1.ObjToArr]
        })
            .overrideComponent(billsummary_1.BillSummary, {
            remove: { providers: [app_service_1.AppService, billsummary_service_1.BillSummaryService] },
            add: {
                providers: [
                    { provide: app_service_1.AppService, useClass: MockAppService },
                    { provide: billsummary_service_1.BillSummaryService, useClass: MockBillSummaryService }
                ]
            }
        })
            .compileComponents();
    }));
    it('No Bills should get generic message', testing_1.async(function () {
        var fixture = testing_1.TestBed.createComponent(billsummary_1.BillSummary);
        var app = fixture.componentInstance;
        app.enableLoader = false;
        var json = JSON
            .parse('{"TotalGasUse":null,"TotalElectricUse":null,"TotalWaterUse":null,"AvgDailyCostText":null,"NumberOfDaysText":null, "AvgTempText":null, "AmountDueText":null, "ViewBillHistory":null, "ShowBillHistoryLink":null, "Commodities":null, "BillHistoryLink":null, "CurrencyFormat":null, "NoBillsText":"No bills are available.", "TotalGasUsed":0, "TotalElectricityUsed":0, "TotalWaterUsed":0, "GasUOM":null, "ElectricUOM":null, "WaterUOM":null, "AvgDailyCost":null, "NumberOfDays":0, "AvgTemp":0, "AmountDue":null, "BillStartDateGas":null, "BillEndDateGas":null, "BillStartDateElectric":null, "BillEndDateElectric":null, "BillStartDateWater":null, "BillEndDateWater":null, "NoBills":true, "DueDate":null, "TempFormat":null, "RateClassGas":null, "RateClassElectric":null, "RateClassWater":null, "RateClassSewer":null, "RateMismatchGas":false, "RateMismatchElectric":false, "RateMismatchWater":false, "RateMismatchSewer":false, "Title":"", "IntroText":"", "SubTitle":"", "ShowTitle":"false", "ShowIntro":"", "ShowSubTitle":"", "Footer":"Sample footer text.", "FooterLink":"Page?enc=Lbe", "FooterLinkText":"View Full Billing History", "FooterLinkType":null, "ShowFooterText":"false", "ShowFooterLink":"false" }');
        app.result = json;
        fixture.detectChanges();
        var txtNoBills = fixture.nativeElement.querySelectorAll("strong");
        expect(txtNoBills[0].innerText).toBe("No bills are available.");
    }));
    it('should get Bill Summary all commodities Totals Text', testing_1.async(function () {
        var fixture = testing_1.TestBed.createComponent(billsummary_1.BillSummary);
        var app = fixture.componentInstance;
        app.enableLoader = false;
        var json = JSON
            .parse('{"TotalGasUse":"Total Gas Used", "TotalElectricUse":"Total Electricity Used", "TotalWaterUse":"Total Water Used","AvgDailyCostText":"Average Daily Cost","NumberOfDaysText":"Number of Days","AvgTempText":"Average Temp","AmountDueText":"Amount Due: ","ViewBillHistory":"View Full Billing History","ShowBillHistoryLink":"true", "Commodities":"gas, electric,water", "BillHistoryLink":"Px","CurrencyFormat":null,"NoBillsText":null,"TotalGasUsed":767,"TotalElectricityUsed":152, "TotalWaterUsed":2250, "GasUOM":"Therms", "ElectricUOM":"kWh", "WaterUOM":"gal", "AvgDailyCost":"$13.63","NumberOfDays":30,"AvgTemp":65,"AmountDue":"$408.92","BillStartDateGas":"11/ 01 / 2016","BillEndDateGas":"12/01/2016", "BillStartDateElectric":"11/01/2016", "BillEndDateElectric":"12/01/2016", "BillStartDateWater":"11/01/2016", "BillEndDateWater":"12/01/2016", "NoBills":false, "DueDate":null, "TempFormat":"F", "RateClassGas":"BAMI-G", "RateClassElectric":"binTierM", "RateClassWater":"binWater", "RateClassSewer":null, "RateMismatchGas":false, "RateMismatchElectric":false, "RateMismatchWater":false, "RateMismatchSewer":false, "Title":"", "IntroText":"", "SubTitle":"", "ShowTitle":"false", "ShowIntro":"", "ShowSubTitle":"", "Footer":"Sample footer text.", "FooterLink":"Page?enc=LbeNmuyExXI1tZ4xHiibWB2y32qq8NpGuV0GoKvqvDwlAMJVuHaT3jW3/E8APZFrEOm3eoRMB8Yp4fKYEKA10yTBr8IMDkfwoFfArFhGOAKG4tK3Ro7BaYKZCmTgSHdhNNvFSO","FooterLinkText":"View Full Billing History","FooterLinkType":null,"ShowFooterText":"false", "ShowFooterLink":"false" }');
        app.result = json;
        fixture.detectChanges();
        var rowComs = fixture.nativeElement.querySelectorAll("li");
        expect(rowComs["0"].innerText).toContain("Total Gas Used");
        expect(rowComs["1"].innerText).toContain("Total Electricity Used");
        expect(rowComs["2"].innerText).toContain("Total Water Used");
        var totalsCom = fixture.nativeElement.querySelectorAll("p");
        expect(totalsCom.length).toBe(7);
        ;
    }));
    it('should get Bill Summary Gas Total Text with No Other Commodity totals', testing_1.async(function () {
        var fixture = testing_1.TestBed.createComponent(billsummary_1.BillSummary);
        var app = fixture.componentInstance;
        app.enableLoader = false;
        var json = JSON
            .parse('{"TotalGasUse":"Total Gas Used","TotalElectricUse":"Total Electricity Used","TotalWaterUse":"Total WaterUsed","AvgDailyCostText":"Average Daily Cost","NumberOfDaysText":"Number of Days","AvgTempText":"AverageTemp","AmountDueText":"Amount Due: ","ViewBillHistory":"View Full Billing History","ShowBillHistoryLink":"true", "Commodities":"gas, electric,water", "BillHistoryLink":"Page?enc=LbeNmuyExXI1tZ4xHiibWB2y32qq8NpGuV0GoKvtE","CurrencyFormat":null, "NoBillsText":null, "TotalGasUsed":20, "TotalElectricityUsed":0, "TotalWaterUsed":0, "GasUOM":"Therms", "ElectricUOM":"", "WaterUOM":"", "AvgDailyCost":"$1.58", "NumberOfDays":31, "AvgTemp":53, "AmountDue":"$48.94","BillStartDateGas":"12/ 20 / 2015","BillEndDateGas":"01/ 20 / 2016","BillStartDateElectric":null,"BillEndDateElectric":null, "BillStartDateWater":null, "BillEndDateWater":null, "NoBills":false, "DueDate":null, "TempFormat":"F", "RateClassGas":"BAMI-G", "RateClassElectric":null, "RateClassWater":null, "RateClassSewer":null, "RateMismatchGas":false, "RateMismatchElectric":false, "RateMismatchWater":false, "RateMismatchSewer":false, "Title":"", "IntroText":"", "SubTitle":"", "ShowTitle":"false", "ShowIntro":"", "ShowSubTitle":"", "Footer":"Sample footer text.", "FooterLink":"Page?enc=LbeNmuyExXI1tZ4xHiibWB2y32qq8NpGuV0GoKvqvDwlAMJVuHaT3jW3/E8APZFrEOm3eoRMB8YBFgnIWcQ=","FooterLinkText":"View Full Billing History", "FooterLinkType":null, "ShowFooterText":"false", "ShowFooterLink":"false"}');
        app.result = json;
        fixture.detectChanges();
        var rowComs = fixture.nativeElement.querySelectorAll("li");
        expect(rowComs["0"].innerText).toContain("Total Gas Used");
        var totalsCom = fixture.nativeElement.querySelectorAll("p");
        expect(totalsCom.length).toBe(5);
    }));
    it('should get View Full Bill History Link when enabled', testing_1.async(function () {
        var fixture = testing_1.TestBed.createComponent(billsummary_1.BillSummary);
        var app = fixture.componentInstance;
        app.enableLoader = false;
        var json = JSON
            .parse('{"TotalGasUse":"Total Gas Used", "TotalElectricUse":"Total Electricity Used", "TotalWaterUse":"Total Water Used","AvgDailyCostText":"Average Daily Cost","NumberOfDaysText":"Number of Days","AvgTempText":"Average Temp","AmountDueText":"Amount Due: ","ViewBillHistory":"View Full Billing History","ShowBillHistoryLink":"true", "Commodities":"gas, electric,water", "BillHistoryLink":"Px","CurrencyFormat":null,"NoBillsText":null,"TotalGasUsed":767,"TotalElectricityUsed":152, "TotalWaterUsed":2250, "GasUOM":"Therms", "ElectricUOM":"kWh", "WaterUOM":"gal", "AvgDailyCost":"$13.63","NumberOfDays":30,"AvgTemp":65,"AmountDue":"$408.92","BillStartDateGas":"11/ 01 / 2016","BillEndDateGas":"12/01/2016", "BillStartDateElectric":"11/01/2016", "BillEndDateElectric":"12/01/2016", "BillStartDateWater":"11/01/2016", "BillEndDateWater":"12/01/2016", "NoBills":false, "DueDate":null, "TempFormat":"F", "RateClassGas":"BAMI-G", "RateClassElectric":"binTierM", "RateClassWater":"binWater", "RateClassSewer":null, "RateMismatchGas":false, "RateMismatchElectric":false, "RateMismatchWater":false, "RateMismatchSewer":false, "Title":"", "IntroText":"", "SubTitle":"", "ShowTitle":"false", "ShowIntro":"", "ShowSubTitle":"", "Footer":"Sample footer text.", "FooterLink":"Page?enc=LbeNmuyExXI1tZ4xHiibWB2y32qq8NpGuV0GoKvqvDwlAMJVuHaT3jW3/E8APZFrEOm3eoRMB8Yp4fKYEKA10yTBr8IMDkfwoFfArFhGOAKG4tK3Ro7BaYKZCmTgSHdhNNvFSO","FooterLinkText":"View Full Billing History","FooterLinkType":null,"ShowFooterText":"false", "ShowFooterLink":"false" }');
        app.result = json;
        fixture.detectChanges();
        var linkBillHistory = fixture.nativeElement.querySelectorAll("a");
        expect(linkBillHistory.length).toBe(1);
    }));
    it('should not find Footer when not enabled', testing_1.async(function () {
        var fixture = testing_1.TestBed.createComponent(billsummary_1.BillSummary);
        var app = fixture.componentInstance;
        app.enableLoader = false;
        var json = JSON
            .parse('{"TotalGasUse":"Total Gas Used", "TotalElectricUse":"Total Electricity Used", "TotalWaterUse":"Total Water Used","AvgDailyCostText":"Average Daily Cost","NumberOfDaysText":"Number of Days","AvgTempText":"Average Temp","AmountDueText":"Amount Due: ","ViewBillHistory":"View Full Billing History","ShowBillHistoryLink":"true", "Commodities":"gas, electric,water", "BillHistoryLink":"Px","CurrencyFormat":null,"NoBillsText":null,"TotalGasUsed":767,"TotalElectricityUsed":152, "TotalWaterUsed":2250, "GasUOM":"Therms", "ElectricUOM":"kWh", "WaterUOM":"gal", "AvgDailyCost":"$13.63","NumberOfDays":30,"AvgTemp":65,"AmountDue":"$408.92","BillStartDateGas":"11/ 01 / 2016","BillEndDateGas":"12/01/2016", "BillStartDateElectric":"11/01/2016", "BillEndDateElectric":"12/01/2016", "BillStartDateWater":"11/01/2016", "BillEndDateWater":"12/01/2016", "NoBills":false, "DueDate":null, "TempFormat":"F", "RateClassGas":"BAMI-G", "RateClassElectric":"binTierM", "RateClassWater":"binWater", "RateClassSewer":null, "RateMismatchGas":false, "RateMismatchElectric":false, "RateMismatchWater":false, "RateMismatchSewer":false, "Title":"", "IntroText":"", "SubTitle":"", "ShowTitle":"false", "ShowIntro":"", "ShowSubTitle":"", "Footer":"Sample footer text.", "FooterLink":"Page?enc=LbeNmuyExXI1tZ4xHiibWB2y32qq8NpGuV0GoKvqvDwlAMJVuHaT3jW3/E8APZFrEOm3eoRMB8Yp4fKYEKA10yTBr8IMDkfwoFfArFhGOAKG4tK3Ro7BaYKZCmTgSHdhNNvFSO","FooterLinkText":"View Full Billing History","FooterLinkType":null,"ShowFooterText":"false", "ShowFooterLink":"false" }');
        app.result = json;
        fixture.detectChanges();
        var lnkFooter = fixture.debugElement.query(platform_browser_1.By.css(".footerlink"));
        expect(lnkFooter).toBe(null);
    }));
    it('should get Footer when enabled', testing_1.async(function () {
        var fixture = testing_1.TestBed.createComponent(billsummary_1.BillSummary);
        var app = fixture.componentInstance;
        app.enableLoader = false;
        var json = JSON
            .parse('{"TotalGasUse":"Total Gas Used", "TotalElectricUse":"Total Electricity Used", "TotalWaterUse":"Total Water Used","AvgDailyCostText":"Average Daily Cost","NumberOfDaysText":"Number of Days","AvgTempText":"AverageTemp","AmountDueText":"Amount Due: ","ViewBillHistory":"View Full Billing History","ShowBillHistoryLink":"false", "Commodities":"gas, electric,water", "BillHistoryLink":"Page?enc=LbeNmuyExXI1tZ4xHiibWB2ZbQ == ","CurrencyFormat":null,"NoBillsText":null,"TotalGasUsed":0,"TotalElectricityUsed":550, "TotalWaterUsed":4500, "GasUOM":"", "ElectricUOM":"kWh", "WaterUOM":"gal", "AvgDailyCost":"$7.18", "NumberOfDays":30, "AvgTemp":72, "AmountDue":"$215.50", "BillStartDateGas":null, "BillEndDateGas":null, "BillStartDateElectric":"06/01/2015", "BillEndDateElectric":"07/01/2015", "BillStartDateWater":"06/01/2015", "BillEndDateWater":"07/01/2015", "NoBills":false, "DueDate":null, "TempFormat":"F", "RateClassGas":null, "RateClassElectric":"GR", "RateClassWater":"GR", "RateClassSewer":null, "RateMismatchGas":false, "RateMismatchElectric":false, "RateMismatchWater":false, "RateMismatchSewer":false, "Title":"", "IntroText":"", "SubTitle":"", "ShowTitle":"false", "ShowIntro":"", "ShowSubTitle":"", "Footer":"Sample footer.", "FooterLink":"Page?enc=LbeNmu8","FooterLinkText":"Footer Text","FooterLinkType":null,"ShowFooterText":"true", "ShowFooterLink":"true" }');
        app.result = json;
        fixture.detectChanges();
        var lnkFooter = fixture.debugElement.query(platform_browser_1.By.css(".footerlink"));
        expect(lnkFooter.nativeElement.innerText).toBe("Footer Text");
    }));
    it('should find Amount Due in Strong format with dollar amount', testing_1.async(function () {
        var fixture = testing_1.TestBed.createComponent(billsummary_1.BillSummary);
        var app = fixture.componentInstance;
        app.enableLoader = false;
        var json = JSON
            .parse('{"TotalGasUse":"Total Gas Used", "TotalElectricUse":"Total Electricity Used", "TotalWaterUse":"Total Water Used","AvgDailyCostText":"Average Daily Cost","NumberOfDaysText":"Number of Days","AvgTempText":"AverageTemp","AmountDueText":"Amount Due: ","ViewBillHistory":"View Full Billing History","ShowBillHistoryLink":"false", "Commodities":"gas, electric,water", "BillHistoryLink":"Page?enc=LbeNmuyExXI1tZ4xHiibWB2ZbQ == ","CurrencyFormat":null,"NoBillsText":null,"TotalGasUsed":0,"TotalElectricityUsed":550, "TotalWaterUsed":4500, "GasUOM":"", "ElectricUOM":"kWh", "WaterUOM":"gal", "AvgDailyCost":"$7.18", "NumberOfDays":30, "AvgTemp":72, "AmountDue":"$215.50", "BillStartDateGas":null, "BillEndDateGas":null, "BillStartDateElectric":"06/01/2015", "BillEndDateElectric":"07/01/2015", "BillStartDateWater":"06/01/2015", "BillEndDateWater":"07/01/2015", "NoBills":false, "DueDate":null, "TempFormat":"F", "RateClassGas":null, "RateClassElectric":"GR", "RateClassWater":"GR", "RateClassSewer":null, "RateMismatchGas":false, "RateMismatchElectric":false, "RateMismatchWater":false, "RateMismatchSewer":false, "Title":"", "IntroText":"", "SubTitle":"", "ShowTitle":"false", "ShowIntro":"", "ShowSubTitle":"", "Footer":"Sample footer.", "FooterLink":"Page?enc=LbeNmu8","FooterLinkText":"Footer Text","FooterLinkType":null,"ShowFooterText":"true", "ShowFooterLink":"true" }');
        app.result = json;
        fixture.detectChanges();
        var txtAmtDue = fixture.nativeElement.querySelectorAll("strong");
        expect(txtAmtDue.length).toBe(2);
        expect(txtAmtDue[1].innerText).toBe("$215.50");
    }));
});
//# sourceMappingURL=billsummary.dom.spec.js.map