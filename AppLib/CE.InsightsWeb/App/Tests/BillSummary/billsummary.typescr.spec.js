"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var billsummary_1 = require("../../BillSummary/billsummary");
var billsummary_service_1 = require("../../BillSummary/billsummary.service");
var app_service_1 = require("../../app.service");
var MockElementRef = (function () {
    function MockElementRef() {
        this.nativeElement = {};
    }
    return MockElementRef;
}());
describe('BillSummary Typescript Tests', function () {
    it('BillSummary loadBillSummary().  Component main entry point.', function () {
        var http;
        var appService = new app_service_1.AppService(http);
        var qlService = new billsummary_service_1.BillSummaryService(http);
        var elementRef = new MockElementRef();
        appService.params = [{ tabKey: "tab.billstatement", parameters: "YbUGYEN1OhOANtb1zjkfy/hq/cI+rQ4HQlLIgZfqZL7OdgsF8ZKqzAw3eqO0PtMqSEW2CiNa9iOob71Xc8rETBZ0b0gp5N9mczstPSIn8k41jMUEo/KUHHOvQMT89omslD8Zxrj3CSA5CIYCEkMqiRFvtTquCvGi0GQTcZ19rqt1WAisugqWAXqjCKDEgwEAtTxKtAADOS1GKFAOc8O/vAn6PK0IcfRMmQ580IKEVhAnIE2UI2E8B3TjBR426Obt+NM4ppU46hcpbwY0IUil1ojq/lN/d8AzTriNnHjy3eVb2OyYDuFYOFopUUpX/v1qUrVgV7VD96UW2zEGS5zRb4N0/RHYCSwUE3qgwkzyOjUaOX3L9Jrwk9e4tNouzbEbjbXPjTwDcqbqUBk/Wn5eik5/64JISCsuqUNSz5GbjMN4F4WYoFT37PtjCq7KsV16YccmF3NObF9MfU45pgPyRI8Qas6g2fG/XdChanb7sRPGDj0C0do7/86T3PyOYJoV4v3+afpi7+iSGcfnYR7bhg==", errorMessage: "" }];
        qlService.res = JSON.parse('{"TotalGasUse":"Total Gas Used", "TotalElectricUse":"Total Electricity Used", "TotalWaterUse":"Total Water Used","AvgDailyCostText":"Average Daily Cost","NumberOfDaysText":"Number of Days","AvgTempText":"Average Temp","AmountDueText":"Amount Due: ","ViewBillHistory":"View Full Billing History","ShowBillHistoryLink":"true", "Commodities":"gas, electric,water", "BillHistoryLink":"Px","CurrencyFormat":null,"NoBillsText":null,"TotalGasUsed":767,"TotalElectricityUsed":152, "TotalWaterUsed":2250, "GasUOM":"Therms", "ElectricUOM":"kWh", "WaterUOM":"gal", "AvgDailyCost":"$13.63","NumberOfDays":30,"AvgTemp":65,"AmountDue":"$408.92","BillStartDateGas":"11/ 01 / 2016","BillEndDateGas":"12/01/2016", "BillStartDateElectric":"11/01/2016", "BillEndDateElectric":"12/01/2016", "BillStartDateWater":"11/01/2016", "BillEndDateWater":"12/01/2016", "NoBills":false, "DueDate":null, "TempFormat":"F", "RateClassGas":"BAMI-G", "RateClassElectric":"binTierM", "RateClassWater":"binWater", "RateClassSewer":null, "RateMismatchGas":false, "RateMismatchElectric":false, "RateMismatchWater":false, "RateMismatchSewer":false, "Title":"", "IntroText":"", "SubTitle":"", "ShowTitle":"false", "ShowIntro":"", "ShowSubTitle":"", "Footer":"Sample footer text.", "FooterLink":"Page?enc=LbeNmuyExXI1tZ4xHiibWB2y32qq8NpGuV0GoKvqvDwlAMJVuHaT3jW3/E8APZFrEOm3eoRMB8Yp4fKYEKA10yTBr8IMDkfwoFfArFhGOAKG4tK3Ro7BaYKZCmTgSHdhNNvFSO","FooterLinkText":"View Full Billing History","FooterLinkType":null,"ShowFooterText":"false", "ShowFooterLink":"false" }');
        var billsumm = new billsummary_1.BillSummary(qlService, appService, elementRef);
        billsumm.fromUTest = true;
        expect(billsumm.result.AmountDue).toEqual("$408.92");
    });
});
//# sourceMappingURL=billsummary.typescr.spec.js.map