﻿import { inject, async, TestBed } from "@angular/core/testing";
import { Http, HttpModule, XHRBackend, Response, ResponseOptions } from "@angular/http";
import { MockBackend, MockConnection } from "@angular/http/testing";
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { BillDisaggService } from "../../BillDisagg/billdisagg.service";

describe('TestBillDisaggService -- Mock', () => {
    beforeEach(async(() => {
        TestBed.resetTestEnvironment();

        TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting());
        TestBed.configureTestingModule({
            imports: [HttpModule],
            providers: [
                BillDisaggService,
                { provide: XHRBackend, useClass: MockBackend }
            ]
        }).compileComponents();;
    }));

    it('can instantiate service when inject service',
        inject([BillDisaggService], (service: BillDisaggService) => {
            expect(service instanceof BillDisaggService).toBe(true);
        }));

    it('can provide the mockBackend as XHRBackend',
        inject([XHRBackend], (backend: MockBackend) => {
            expect(backend).not.toBeNull('backend should be provided');
        }));


    let backend: MockBackend;
    let service: BillDisaggService;
    let response: Response;

    beforeEach(inject([Http, XHRBackend], (http: Http, be: MockBackend) => {
        backend = be;
        service = new BillDisaggService(http);
        let options = new ResponseOptions({
            body: [
                {
                    id: "QA87Billing",
                    contentRendered: "<p><b>Test</b></p>",
                    contentMarkdown: "*Hi there*"
                }
            ]
        });
        response = new Response(options);
    }));

    it('should get bill disagg', async(inject([], () => {
        backend.connections.subscribe((c: MockConnection) => c.mockRespond(response));

        service.getBillDisagg('', '').subscribe((blogs) => {
            expect(blogs.length).toBe(1);
            expect(blogs[0].id).toBe("QA87Billing");
            expect(blogs[0].contentRendered).toBe("<p><b>Test</b></p>");
        });
    })));

});