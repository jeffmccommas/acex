﻿import { async, TestBed } from "@angular/core/testing";
import { ElementRef } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { HttpModule } from "@angular/http";
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from "@angular/platform-browser-dynamic/testing";
import { CustomerInfo } from "../../CustomerInfo/customerinfo";
import { CustomerInfoService } from "../../CustomerInfo/customerinfo.service";
import { AppService, IAppParams, IDeepLinkVariables } from "../../app.service";
import { Loader } from "../../Loader/loader";
import { ObjToArr } from "../../Helper/pipe";

//mock customer and app services
class MockAppService extends AppService {
    public params: Array<IAppParams> = [{ tabKey: "tab.csr", parameters: "YbUGYEN1OhOANtb1zjkfy/hq/cI+rQ4HQlLIgZfqZL7OdgsF8ZKqzAw3eqO0PtMqSEW2CiNa9iOob71Xc8rETBZ0b0gp5N9mczstPSIn8k41jMUEo/KUHHOvQMT89omslD8Zxrj3CSA5CIYCEkMqiRFvtTquCvGi0GQTcZ19rqt1WAisugqWAXqjCKDEgwEAtTxKtAADOS1GKFAOc8O/vAn6PK0IcfRMmQ580IKEVhAnIE2UI2E8B3TjBR426Obt+NM4ppU46hcpbwY0IUil1l8S7I0ffzpnpfuWCDxLP5p3CKQpGI/ha6jU1ounQFk1LDYSoC/j083c3i8OHlB4zy5Z44Pz+xfq4GU4zqXNZoIfipeRuVHB1pIC/dqQ3UX4kCxUJhFCZgjYozjpi2KUYYiOYV8WQQkeTybUkk+cf/UW4kOfoICCU4cVjSafdJUifaRAj+RaS1HTGjns8E4xrUYF4nGPg7wOFTnOxfdtJap5GCg9GZKPf7KBDzfDdV2d8Uyb1DwBPDMUhix9VTHKvPEjq3Bzl5zHUmaA2goT6QEtBT+x6by41BU2txhbyKY1", errorMessage: "" }];
    public deepLinkVariables: Array<IDeepLinkVariables> = [{ deepLinkWidget: "", deepLinkWidgetTab: "", deepLinkWidgetSection: "" }];

    getParams(): Array<IAppParams> {
        return this.params;
    }
    getDeepLinkVariables(): Array<IDeepLinkVariables> {
        return this.deepLinkVariables;
    }
}

class MockCustomerInfoService extends CustomerInfoService {
    public res: any;
    public unit: any = "unittest";

    getCustomerInfo(tabKey, params) {
        return this.unit;
    }
}

describe('CustomerInfo DOM Tests',
() => {
    beforeEach(async(() => {
        TestBed.resetTestEnvironment();
        TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting());
        ElementRef;
        TestBed.configureTestingModule({
                imports: [BrowserModule, FormsModule, HttpModule],
                declarations: [CustomerInfo, Loader, ObjToArr]
            })
            .overrideComponent(CustomerInfo,
            {
                remove: { providers: [AppService, CustomerInfoService] },
                add: {
                    providers: [
                        { provide: AppService, useClass: MockAppService },
                        { provide: CustomerInfoService, useClass: MockCustomerInfoService } 
                    ]
                }
            })
            .compileComponents();
        }));


    it('should get address', async(() => {
        const fixture = TestBed.createComponent(CustomerInfo);
        const app = fixture.componentInstance;
        const element = fixture.nativeElement;
        app.enableLoader = false;
        var json = JSON.parse('{"CustomerIdLabel":"Customer Id:","AccountNumberLabel":"Account Number 123:","AddressLabel":"Address","ServiceInfoLabel":"Service Information","HomeProfileLabel":"Home Profile","ProfileLink":"View","MeterLabel":" - Meter #: ","ShowProfile":"true","FirstName":"CAROLYN","LastName":"TRUMBULL","CustomerId":"993774751","AccountId":"1218337093","ViewLink":"Page?enc=4DHfV27Bb5roAmvNMy4/N00neH9vB44xvJ7zg/22vWwZwbs6ISWbEX6AJsFvxEkNTBKdD8bjgATAg4GMEhAaDro7PyVC6AWQvFCecZx8yYG2p3JnAz1R6oOxCUvKgCs8t6Y3Bq9EJGhBZjm2VtJ1QyAfUGX04ebrUme9QhdFRuF9fOaWoMBlkVF01ALDQoSPP4FGMU2aWg7/MO5zw0wXLJvRg2ZAVDwFlWNmJe+aSNfR6dLLZDy6/u+PpJuVGdNKPCyfXamO1PTKOgNGd37Er0pzxtdzLL59Ik/Rlk+KAg2Z4WKLK2E5jy4eZUWxnm77oX/Tfjia2m4tIR0O/HowlX8ZidtLYqHcBghgnrV1CVVeMN49jQqwwjsjD2L95yB6baY4sCrIZJt+osqyztwaApwdINLm7qQ19gv8tb6bbnUhA9UPouQ22RflkGmdNJeEH9lT/3dLsZAS5Y14jcSiH/qQkt5XW+bwO3Gp8wjKsu4x4kisXV+1qoALm59lXNXndnOKm2HRwU7FKtq/EUQHs0Ai6nTrsnERMdwrYh9JcL8=","Addresses":[{"Addr1":"15924 S Pannes Ave","Addr2":"","City":"COMPTON","StateProvince":"CA","PostalCode":"902214523","Country":"US","Selected":true,"Url":"Page?enc=o7FI+Wz+RYUjbcnOjKjEqLry5lf7U302ozMzaWuUsrYmL3er7HjaT/22LTj5iDWREVP770SG847oIxx5eiqLGulBoLXyxLfRCajOK6+yOVIsBmQlrMlmuDHv6tEjn/iVVw+F+Zsik87nOfqs+YGyUcopy31kHpsEvtaz/kssbDsVQP19K8MHZZ2M7J4PxyGSUe7fQUqO9YajalynWPcTQTYHqxvOHxf2bOk1YK3pZriH2Tvz5c0jAmM4VIUHx7tZ4HNQ2EMX+aze+P1ohRZXufUyYoEdOzvNVBrrhMY+b9s9k2Ib0ScK4NHU6tMMdxRFGtRtq3IpBxPY5aMXDtiBtD9/5AzlPSL7uaqoLK1DONaa70w0OGhKl1xzSo9be02WvoPX4mJHfDa1jI9AHVmf0AL792lMMFwyYSstsueTZj6A3AhuPnwYJKqy2l+i7D9zBOKuxZYqPu8l/wDdDgMrlHpZPBUKXVQytTB+zPqZxccN6Taz+KgIAsj9asJ+mlRhvuye23ALN+dtmz83WbFVzJGke5cXTcxRNIl8SUpNS7cVYmMQppB9mkxC8clZDgxCmnRQoN41f0Swlzd5Vkp0G3bLMFGrlIr0LI5F3sAirBE="}],"ServiceInfo":[{"Id":"SC_1218337000_electric","MeterId":"m189354704","CommodityKey":"electric","CommodityName":"Electric"},{"Id":"SC_1218337000_gas","MeterId":"m1218337000","CommodityKey":"gas","CommodityName":"Gas"},{"Id":"SC_1218337000_water","MeterId":"m189354705","CommodityKey":"water","CommodityName":"Water"}],"QuestionsAnswered":3,"QuestionAnsweredText":"3 Questions Answered"}');
        app.result = json;
        fixture.detectChanges();
        expect(element.textContent).toContain('Address');
    }));


    it('should find create report link', async(() => {
        const fixture = TestBed.createComponent(CustomerInfo);
        const app = fixture.componentInstance;
        const element = fixture.nativeElement;
        app.enableLoader = false;
        var json = JSON.parse('{"CustomerIdLabel":"Customer Id:","AccountNumberLabel":"Account Number 123:","AddressLabel":"Address","ServiceInfoLabel":"Service Information","HomeProfileLabel":"Home Profile","ProfileLink":"View","MeterLabel":" - Meter #: ","ShowProfile":"true","FirstName":"CAROLYN","LastName":"TRUMBULL","CustomerId":"993774751","AccountId":"1218337093","ViewLink":"Page?enc=4DHfV27Bb5roAmvNMy4/N00neH9vB44xvJ7zg/22vWwZwbs6ISWbEX6AJsFvxEkNTBKdD8bjgATAg4GMEhAaDro7PyVC6AWQvFCecZx8yYG2p3JnAz1R6oOxCUvKgCs8t6Y3Bq9EJGhBZjm2VtJ1QyAfUGX04ebrUme9QhdFRuF9fOaWoMBlkVF01ALDQoSPP4FGMU2aWg7/MO5zw0wXLJvRg2ZAVDwFlWNmJe+aSNfR6dLLZDy6/u+PpJuVGdNKPCyfXamO1PTKOgNGd37Er0pzxtdzLL59Ik/Rlk+KAg2Z4WKLK2E5jy4eZUWxnm77oX/Tfjia2m4tIR0O/HowlX8ZidtLYqHcBghgnrV1CVVeMN49jQqwwjsjD2L95yB6baY4sCrIZJt+osqyztwaApwdINLm7qQ19gv8tb6bbnUhA9UPouQ22RflkGmdNJeEH9lT/3dLsZAS5Y14jcSiH/qQkt5XW+bwO3Gp8wjKsu4x4kisXV+1qoALm59lXNXndnOKm2HRwU7FKtq/EUQHs0Ai6nTrsnERMdwrYh9JcL8=","Addresses":[{"Addr1":"15924 S Pannes Ave","Addr2":"","City":"COMPTON","StateProvince":"CA","PostalCode":"902214523","Country":"US","Selected":true,"Url":"Page?enc=o7FI+Wz+RYUjbcnOjKjEqLry5lf7U302ozMzaWuUsrYmL3er7HjaT/22LTj5iDWREVP770SG847oIxx5eiqLGulBoLXyxLfRCajOK6+yOVIsBmQlrMlmuDHv6tEjn/iVVw+F+Zsik87nOfqs+YGyUcopy31kHpsEvtaz/kssbDsVQP19K8MHZZ2M7J4PxyGSUe7fQUqO9YajalynWPcTQTYHqxvOHxf2bOk1YK3pZriH2Tvz5c0jAmM4VIUHx7tZ4HNQ2EMX+aze+P1ohRZXufUyYoEdOzvNVBrrhMY+b9s9k2Ib0ScK4NHU6tMMdxRFGtRtq3IpBxPY5aMXDtiBtD9/5AzlPSL7uaqoLK1DONaa70w0OGhKl1xzSo9be02WvoPX4mJHfDa1jI9AHVmf0AL792lMMFwyYSstsueTZj6A3AhuPnwYJKqy2l+i7D9zBOKuxZYqPu8l/wDdDgMrlHpZPBUKXVQytTB+zPqZxccN6Taz+KgIAsj9asJ+mlRhvuye23ALN+dtmz83WbFVzJGke5cXTcxRNIl8SUpNS7cVYmMQppB9mkxC8clZDgxCmnRQoN41f0Swlzd5Vkp0G3bLMFGrlIr0LI5F3sAirBE="}],"ServiceInfo":[{"Id":"SC_1218337000_electric","MeterId":"m189354704","CommodityKey":"electric","CommodityName":"Electric"},{"Id":"SC_1218337000_gas","MeterId":"m1218337000","CommodityKey":"gas","CommodityName":"Gas"},{"Id":"SC_1218337000_water","MeterId":"m189354705","CommodityKey":"water","CommodityName":"Water"}],"QuestionsAnswered":3,"QuestionAnsweredText":"3 Questions Answered"}');
        app.result = json;
        fixture.detectChanges();
        const sec = fixture.nativeElement.querySelectorAll("#create-report-csr");
        expect(element.textContent).toContain('Create Report');
    }));

    });