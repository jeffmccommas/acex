"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var platform_browser_1 = require("@angular/platform-browser");
var http_1 = require("@angular/http");
var testing_2 = require("@angular/platform-browser-dynamic/testing");
var customerinfo_1 = require("../../CustomerInfo/customerinfo");
var customerinfo_service_1 = require("../../CustomerInfo/customerinfo.service");
var app_service_1 = require("../../app.service");
var loader_1 = require("../../Loader/loader");
var pipe_1 = require("../../Helper/pipe");
var MockAppService = (function (_super) {
    __extends(MockAppService, _super);
    function MockAppService() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.params = [{ tabKey: "tab.csr", parameters: "YbUGYEN1OhOANtb1zjkfy/hq/cI+rQ4HQlLIgZfqZL7OdgsF8ZKqzAw3eqO0PtMqSEW2CiNa9iOob71Xc8rETBZ0b0gp5N9mczstPSIn8k41jMUEo/KUHHOvQMT89omslD8Zxrj3CSA5CIYCEkMqiRFvtTquCvGi0GQTcZ19rqt1WAisugqWAXqjCKDEgwEAtTxKtAADOS1GKFAOc8O/vAn6PK0IcfRMmQ580IKEVhAnIE2UI2E8B3TjBR426Obt+NM4ppU46hcpbwY0IUil1l8S7I0ffzpnpfuWCDxLP5p3CKQpGI/ha6jU1ounQFk1LDYSoC/j083c3i8OHlB4zy5Z44Pz+xfq4GU4zqXNZoIfipeRuVHB1pIC/dqQ3UX4kCxUJhFCZgjYozjpi2KUYYiOYV8WQQkeTybUkk+cf/UW4kOfoICCU4cVjSafdJUifaRAj+RaS1HTGjns8E4xrUYF4nGPg7wOFTnOxfdtJap5GCg9GZKPf7KBDzfDdV2d8Uyb1DwBPDMUhix9VTHKvPEjq3Bzl5zHUmaA2goT6QEtBT+x6by41BU2txhbyKY1", errorMessage: "" }];
        _this.deepLinkVariables = [{ deepLinkWidget: "", deepLinkWidgetTab: "", deepLinkWidgetSection: "" }];
        return _this;
    }
    MockAppService.prototype.getParams = function () {
        return this.params;
    };
    MockAppService.prototype.getDeepLinkVariables = function () {
        return this.deepLinkVariables;
    };
    return MockAppService;
}(app_service_1.AppService));
var MockCustomerInfoService = (function (_super) {
    __extends(MockCustomerInfoService, _super);
    function MockCustomerInfoService() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.unit = "unittest";
        return _this;
    }
    MockCustomerInfoService.prototype.getCustomerInfo = function (tabKey, params) {
        return this.unit;
    };
    return MockCustomerInfoService;
}(customerinfo_service_1.CustomerInfoService));
describe('CustomerInfo DOM Tests', function () {
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.resetTestEnvironment();
        testing_1.TestBed.initTestEnvironment(testing_2.BrowserDynamicTestingModule, testing_2.platformBrowserDynamicTesting());
        core_1.ElementRef;
        testing_1.TestBed.configureTestingModule({
            imports: [platform_browser_1.BrowserModule, forms_1.FormsModule, http_1.HttpModule],
            declarations: [customerinfo_1.CustomerInfo, loader_1.Loader, pipe_1.ObjToArr]
        })
            .overrideComponent(customerinfo_1.CustomerInfo, {
            remove: { providers: [app_service_1.AppService, customerinfo_service_1.CustomerInfoService] },
            add: {
                providers: [
                    { provide: app_service_1.AppService, useClass: MockAppService },
                    { provide: customerinfo_service_1.CustomerInfoService, useClass: MockCustomerInfoService }
                ]
            }
        })
            .compileComponents();
    }));
    it('should get address', testing_1.async(function () {
        var fixture = testing_1.TestBed.createComponent(customerinfo_1.CustomerInfo);
        var app = fixture.componentInstance;
        var element = fixture.nativeElement;
        app.enableLoader = false;
        var json = JSON.parse('{"CustomerIdLabel":"Customer Id:","AccountNumberLabel":"Account Number 123:","AddressLabel":"Address","ServiceInfoLabel":"Service Information","HomeProfileLabel":"Home Profile","ProfileLink":"View","MeterLabel":" - Meter #: ","ShowProfile":"true","FirstName":"CAROLYN","LastName":"TRUMBULL","CustomerId":"993774751","AccountId":"1218337093","ViewLink":"Page?enc=4DHfV27Bb5roAmvNMy4/N00neH9vB44xvJ7zg/22vWwZwbs6ISWbEX6AJsFvxEkNTBKdD8bjgATAg4GMEhAaDro7PyVC6AWQvFCecZx8yYG2p3JnAz1R6oOxCUvKgCs8t6Y3Bq9EJGhBZjm2VtJ1QyAfUGX04ebrUme9QhdFRuF9fOaWoMBlkVF01ALDQoSPP4FGMU2aWg7/MO5zw0wXLJvRg2ZAVDwFlWNmJe+aSNfR6dLLZDy6/u+PpJuVGdNKPCyfXamO1PTKOgNGd37Er0pzxtdzLL59Ik/Rlk+KAg2Z4WKLK2E5jy4eZUWxnm77oX/Tfjia2m4tIR0O/HowlX8ZidtLYqHcBghgnrV1CVVeMN49jQqwwjsjD2L95yB6baY4sCrIZJt+osqyztwaApwdINLm7qQ19gv8tb6bbnUhA9UPouQ22RflkGmdNJeEH9lT/3dLsZAS5Y14jcSiH/qQkt5XW+bwO3Gp8wjKsu4x4kisXV+1qoALm59lXNXndnOKm2HRwU7FKtq/EUQHs0Ai6nTrsnERMdwrYh9JcL8=","Addresses":[{"Addr1":"15924 S Pannes Ave","Addr2":"","City":"COMPTON","StateProvince":"CA","PostalCode":"902214523","Country":"US","Selected":true,"Url":"Page?enc=o7FI+Wz+RYUjbcnOjKjEqLry5lf7U302ozMzaWuUsrYmL3er7HjaT/22LTj5iDWREVP770SG847oIxx5eiqLGulBoLXyxLfRCajOK6+yOVIsBmQlrMlmuDHv6tEjn/iVVw+F+Zsik87nOfqs+YGyUcopy31kHpsEvtaz/kssbDsVQP19K8MHZZ2M7J4PxyGSUe7fQUqO9YajalynWPcTQTYHqxvOHxf2bOk1YK3pZriH2Tvz5c0jAmM4VIUHx7tZ4HNQ2EMX+aze+P1ohRZXufUyYoEdOzvNVBrrhMY+b9s9k2Ib0ScK4NHU6tMMdxRFGtRtq3IpBxPY5aMXDtiBtD9/5AzlPSL7uaqoLK1DONaa70w0OGhKl1xzSo9be02WvoPX4mJHfDa1jI9AHVmf0AL792lMMFwyYSstsueTZj6A3AhuPnwYJKqy2l+i7D9zBOKuxZYqPu8l/wDdDgMrlHpZPBUKXVQytTB+zPqZxccN6Taz+KgIAsj9asJ+mlRhvuye23ALN+dtmz83WbFVzJGke5cXTcxRNIl8SUpNS7cVYmMQppB9mkxC8clZDgxCmnRQoN41f0Swlzd5Vkp0G3bLMFGrlIr0LI5F3sAirBE="}],"ServiceInfo":[{"Id":"SC_1218337000_electric","MeterId":"m189354704","CommodityKey":"electric","CommodityName":"Electric"},{"Id":"SC_1218337000_gas","MeterId":"m1218337000","CommodityKey":"gas","CommodityName":"Gas"},{"Id":"SC_1218337000_water","MeterId":"m189354705","CommodityKey":"water","CommodityName":"Water"}],"QuestionsAnswered":3,"QuestionAnsweredText":"3 Questions Answered"}');
        app.result = json;
        fixture.detectChanges();
        expect(element.textContent).toContain('Address');
    }));
    it('should find create report link', testing_1.async(function () {
        var fixture = testing_1.TestBed.createComponent(customerinfo_1.CustomerInfo);
        var app = fixture.componentInstance;
        var element = fixture.nativeElement;
        app.enableLoader = false;
        var json = JSON.parse('{"CustomerIdLabel":"Customer Id:","AccountNumberLabel":"Account Number 123:","AddressLabel":"Address","ServiceInfoLabel":"Service Information","HomeProfileLabel":"Home Profile","ProfileLink":"View","MeterLabel":" - Meter #: ","ShowProfile":"true","FirstName":"CAROLYN","LastName":"TRUMBULL","CustomerId":"993774751","AccountId":"1218337093","ViewLink":"Page?enc=4DHfV27Bb5roAmvNMy4/N00neH9vB44xvJ7zg/22vWwZwbs6ISWbEX6AJsFvxEkNTBKdD8bjgATAg4GMEhAaDro7PyVC6AWQvFCecZx8yYG2p3JnAz1R6oOxCUvKgCs8t6Y3Bq9EJGhBZjm2VtJ1QyAfUGX04ebrUme9QhdFRuF9fOaWoMBlkVF01ALDQoSPP4FGMU2aWg7/MO5zw0wXLJvRg2ZAVDwFlWNmJe+aSNfR6dLLZDy6/u+PpJuVGdNKPCyfXamO1PTKOgNGd37Er0pzxtdzLL59Ik/Rlk+KAg2Z4WKLK2E5jy4eZUWxnm77oX/Tfjia2m4tIR0O/HowlX8ZidtLYqHcBghgnrV1CVVeMN49jQqwwjsjD2L95yB6baY4sCrIZJt+osqyztwaApwdINLm7qQ19gv8tb6bbnUhA9UPouQ22RflkGmdNJeEH9lT/3dLsZAS5Y14jcSiH/qQkt5XW+bwO3Gp8wjKsu4x4kisXV+1qoALm59lXNXndnOKm2HRwU7FKtq/EUQHs0Ai6nTrsnERMdwrYh9JcL8=","Addresses":[{"Addr1":"15924 S Pannes Ave","Addr2":"","City":"COMPTON","StateProvince":"CA","PostalCode":"902214523","Country":"US","Selected":true,"Url":"Page?enc=o7FI+Wz+RYUjbcnOjKjEqLry5lf7U302ozMzaWuUsrYmL3er7HjaT/22LTj5iDWREVP770SG847oIxx5eiqLGulBoLXyxLfRCajOK6+yOVIsBmQlrMlmuDHv6tEjn/iVVw+F+Zsik87nOfqs+YGyUcopy31kHpsEvtaz/kssbDsVQP19K8MHZZ2M7J4PxyGSUe7fQUqO9YajalynWPcTQTYHqxvOHxf2bOk1YK3pZriH2Tvz5c0jAmM4VIUHx7tZ4HNQ2EMX+aze+P1ohRZXufUyYoEdOzvNVBrrhMY+b9s9k2Ib0ScK4NHU6tMMdxRFGtRtq3IpBxPY5aMXDtiBtD9/5AzlPSL7uaqoLK1DONaa70w0OGhKl1xzSo9be02WvoPX4mJHfDa1jI9AHVmf0AL792lMMFwyYSstsueTZj6A3AhuPnwYJKqy2l+i7D9zBOKuxZYqPu8l/wDdDgMrlHpZPBUKXVQytTB+zPqZxccN6Taz+KgIAsj9asJ+mlRhvuye23ALN+dtmz83WbFVzJGke5cXTcxRNIl8SUpNS7cVYmMQppB9mkxC8clZDgxCmnRQoN41f0Swlzd5Vkp0G3bLMFGrlIr0LI5F3sAirBE="}],"ServiceInfo":[{"Id":"SC_1218337000_electric","MeterId":"m189354704","CommodityKey":"electric","CommodityName":"Electric"},{"Id":"SC_1218337000_gas","MeterId":"m1218337000","CommodityKey":"gas","CommodityName":"Gas"},{"Id":"SC_1218337000_water","MeterId":"m189354705","CommodityKey":"water","CommodityName":"Water"}],"QuestionsAnswered":3,"QuestionAnsweredText":"3 Questions Answered"}');
        app.result = json;
        fixture.detectChanges();
        var sec = fixture.nativeElement.querySelectorAll("#create-report-csr");
        expect(element.textContent).toContain('Create Report');
    }));
});
//# sourceMappingURL=customerinfo.dom.spec.js.map