"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var customerinfo_1 = require("../../CustomerInfo/customerinfo");
var customerinfo_service_1 = require("../../CustomerInfo/customerinfo.service");
var app_service_1 = require("../../app.service");
describe('CustomerInfo Typescript Tests', function () {
    it('CustomerInfo disableLoader().  Component main entry point.', function () {
        var http;
        var appService = new app_service_1.AppService(http);
        var custinfoService = new customerinfo_service_1.CustomerInfoService(http);
        var elementRef;
        appService.params = [{ tabKey: "tab.waterportal", parameters: "YbUGYEN1OhOANtb1zjkfy/hq/cI+rQ4HQlLIgZfqZL7OdgsF8ZKqzAw3eqO0PtMqSEW2CiNa9iOob71Xc8rETBZ0b0gp5N9mczstPSIn8k41jMUEo/KUHHOvQMT89omslD8Zxrj3CSA5CIYCEkMqiRFvtTquCvGi0GQTcZ19rqt1WAisugqWAXqjCKDEgwEASuOTT5lYb5Zeaiq/uvBlY9iRFrK+c98iNuYmYNt7bo3aP/l17bzmWnKtytoTOOZiloaVfxEYRnPxVp4qvkav13vVbOe3Q6XjYo84y6fLYm6sZnkvIOzkNDxoUA9iLv1dow6BfTi2jRrGsU+uFNlW+1pVnvFIDY4nFruTPKkJ4KkO4djJI4VQDRubA9U4exxllTJJJz4BQXJ98xTr+/we3w==", errorMessage: "" }];
        appService.deepLinkVariables = [{ deepLinkWidget: "", deepLinkWidgetTab: "", deepLinkWidgetSection: "" }];
        custinfoService.res = JSON.parse('{"CustomerIdLabel":"Customer Id:","AccountNumberLabel":"Account Number:","AddressLabel":"Address","ServiceInfoLabel":"Service Information","HomeProfileLabel":"Home Profile","ProfileLink":"View","MeterLabel":" - Meter #: ","ShowProfile":"true","FirstName":"CAROLYN","LastName":"TRUMBULL","CustomerId":"993774751","AccountId":"1218337093","ViewLink":"Page?enc=4DHfV27Bb5roAmvNMy4/N00neH9vB44xvJ7zg/22vWwZwbs6ISWbEX6AJsFvxEkNTBKdD8bjgATAg4GMEhAaDro7PyVC6AWQvFCecZx8yYG2p3JnAz1R6oOxCUvKgCs8t6Y3Bq9EJGhBZjm2VtJ1QyAfUGX04ebrUme9QhdFRuF9fOaWoMBlkVF01ALDQoSPP4FGMU2aWg7/MO5zw0wXLJvRg2ZAVDwFlWNmJe+aSNfR6dLLZDy6/u+PpJuVGdNKPCyfXamO1PTKOgNGd37Er0pzxtdzLL59Ik/Rlk+KAg2Z4WKLK2E5jy4eZUWxnm77oX/Tfjia2m4tIR0O/HowlX8ZidtLYqHcBghgnrV1CVVeMN49jQqwwjsjD2L95yB6baY4sCrIZJt+osqyztwaApwdINLm7qQ19gv8tb6bbnUhA9UPouQ22RflkGmdNJeEH9lT/3dLsZAS5Y14jcSiH/qQkt5XW+bwO3Gp8wjKsu4x4kisXV+1qoALm59lXNXndnOKm2HRwU7FKtq/EUQHs0Ai6nTrsnERMdwrYh9JcL8=","Addresses":[{"Addr1":"15924 S Pannes Ave","Addr2":"","City":"COMPTON","StateProvince":"CA","PostalCode":"902214523","Country":"US","Selected":true,"Url":"Page?enc=o7FI+Wz+RYUjbcnOjKjEqLry5lf7U302ozMzaWuUsrYmL3er7HjaT/22LTj5iDWREVP770SG847oIxx5eiqLGulBoLXyxLfRCajOK6+yOVIsBmQlrMlmuDHv6tEjn/iVVw+F+Zsik87nOfqs+YGyUcopy31kHpsEvtaz/kssbDsVQP19K8MHZZ2M7J4PxyGSUe7fQUqO9YajalynWPcTQTYHqxvOHxf2bOk1YK3pZriH2Tvz5c0jAmM4VIUHx7tZ4HNQ2EMX+aze+P1ohRZXufUyYoEdOzvNVBrrhMY+b9s9k2Ib0ScK4NHU6tMMdxRFGtRtq3IpBxPY5aMXDtiBtD9/5AzlPSL7uaqoLK1DONaa70w0OGhKl1xzSo9be02WvoPX4mJHfDa1jI9AHVmf0AL792lMMFwyYSstsueTZj6A3AhuPnwYJKqy2l+i7D9zBOKuxZYqPu8l/wDdDgMrlHpZPBUKXVQytTB+zPqZxccN6Taz+KgIAsj9asJ+mlRhvuye23ALN+dtmz83WbFVzJGke5cXTcxRNIl8SUpNS7cVYmMQppB9mkxC8clZDgxCmnRQoN41f0Swlzd5Vkp0G3bLMFGrlIr0LI5F3sAirBE="}],"ServiceInfo":[{"Id":"SC_1218337000_electric","MeterId":"m189354704","CommodityKey":"electric","CommodityName":"Electric"},{"Id":"SC_1218337000_gas","MeterId":"m1218337000","CommodityKey":"gas","CommodityName":"Gas"},{"Id":"SC_1218337000_water","MeterId":"m189354705","CommodityKey":"water","CommodityName":"Water"}],"QuestionsAnswered":3,"QuestionAnsweredText":"3 Questions Answered"}');
        custinfoService.fromUnitTest = true;
        var custInfo = new customerinfo_1.CustomerInfo(custinfoService, appService, elementRef);
        custInfo.disableLoader();
        var loader = custInfo.enableLoader;
        expect(loader).toBe(false);
    });
});
//# sourceMappingURL=customerinfo.typescr.spec.js.map