"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var compiler_1 = require("@angular/compiler");
var http_1 = require("@angular/http");
var forms_1 = require("@angular/forms");
var platform_browser_dynamic_1 = require("@angular/platform-browser-dynamic");
var platform_browser_1 = require("@angular/platform-browser");
var loader_1 = require("./Loader/loader");
var ng_recaptcha_1 = require("ng-recaptcha");
var forms_2 = require("ng-recaptcha/forms");
var pipe_1 = require("./Helper/pipe");
var defaultrequestoptions_1 = require("./Helper/defaultrequestoptions");
var longprofile_1 = require("./Profile/longprofile");
var premiseselect_1 = require("./PremiseSelect/premiseselect");
var subscription_1 = require("./Subscription/subscription");
var unauthenticated_1 = require("./Unauthenticated/unauthenticated");
var promo_1 = require("./Promo/promo");
var peercomparison_1 = require("./PeerComparison/peercomparison");
var profile_1 = require("./Profile/profile");
var customerinfo_1 = require("./CustomerInfo/customerinfo");
var billhistory_1 = require("./BillHistory/billhistory");
var reportcreate_1 = require("./ReportCreate/reportcreate");
var billedusagechart_1 = require("./BilledUsageChart/billedusagechart");
var greenbutton_1 = require("./GreenButton/greenbutton");
var gbcadmin_1 = require("./GreenButton/gbcadmin");
var billdisagg_1 = require("./BillDisagg/billdisagg");
var ngx_bootstrap_1 = require("ngx-bootstrap");
var reportlist_1 = require("./ReportList/reportlist");
var angular_confirmation_popover_1 = require("angular-confirmation-popover");
var quicklinks_1 = require("./QuickLinks/quicklinks");
var billsummary_1 = require("./BillSummary/billsummary");
var billtodate_1 = require("./BillToDate/billtodate");
var threshold_1 = require("./Threshold/threshold");
var thirdpartyselection_1 = require("./ThirdPartySelection/thirdpartyselection");
var thirdpartyscopeselection_1 = require("./ThirdPartyScopeSelection/thirdpartyscopeselection");
var consumption_1 = require("./Consumption/consumption");
var app_1 = require("./app");
var AceViewResolver = (function (_super) {
    __extends(AceViewResolver, _super);
    function AceViewResolver() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    AceViewResolver.prototype.resolve = function (type, throwIfNotFound) {
        var view = _super.prototype.resolve.call(this, type, throwIfNotFound);
        if (view.selector === "app") {
            var output_1 = "";
            var containerClass = "container";
            if (typeof enableRemote != 'undefined') {
                if (enableRemote === 'True') {
                    containerClass = "";
                }
            }
            if (typeof angWidgetsLayout != 'undefined') {
                if (angWidgetsLayout !== null) {
                    var w = angWidgetsLayout.split(",");
                    for (var i = 0; i < w.length; i++) {
                        if (w[i] !== null && w[i].length !== 0) {
                            if (w[i] === "customerinfo" || w[i] === "premiseselect") {
                                output_1 = '<div class="' + containerClass + '" id="PageWidget_3" name="customerinfo" ><' + w[i] + '></' + w[i] + '> </div>';
                            }
                        }
                    }
                    if (typeof angWidgetsHtml != 'undefined') {
                        if (angWidgetsHtml !== null) {
                            output_1 = output_1 + aclaraJQuery("<div>").html(angWidgetsHtml).text();
                        }
                    }
                    for (var k = 0; k < w.length; k++) {
                        if (w[k] !== null && w[k].length !== 0) {
                            if (w[k] === "reportcreate") {
                                output_1 = output_1 + '<div class="' + containerClass + '"><' + w[k] + '></' + w[k] + '> </div>';
                            }
                        }
                    }
                    if (typeof nonAngWidgets != 'undefined') {
                        if (nonAngWidgets !== null) {
                            var comhtml;
                            var nw_1 = nonAngWidgets.split(",");
                            var _loop_1 = function (j) {
                                if (nw_1[j] !== null && nw_1[j].length !== 0) {
                                    aclaraJQuery("#" + nw_1[j]).each(function () {
                                        var cnt = aclaraJQuery(this).children().length;
                                        if (cnt > 1) {
                                            comhtml = aclaraJQuery(this).html();
                                            output_1 = output_1.replace(nw_1[j] + "_html", "<div id='" + nw_1[j] + "_html'></div>");
                                        }
                                    });
                                }
                            };
                            for (var j = 0; j < nw_1.length; j++) {
                                _loop_1(j);
                            }
                        }
                    }
                }
            }
            else {
                if (typeof angWidgetsHtml != 'undefined') {
                    if (angWidgetsHtml !== null) {
                        output_1 = output_1 + aclaraJQuery("<div>").html(angWidgetsHtml).text();
                    }
                }
            }
            view.template = output_1;
        }
        return view;
    };
    return AceViewResolver;
}(compiler_1.DirectiveResolver));
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [platform_browser_1.BrowserModule, forms_1.FormsModule, http_1.HttpModule, forms_2.RecaptchaFormsModule, ngx_bootstrap_1.DatepickerModule.forRoot(), angular_confirmation_popover_1.ConfirmationPopoverModule.forRoot(), ngx_bootstrap_1.ModalModule.forRoot(), ng_recaptcha_1.RecaptchaModule.forRoot()],
            declarations: [app_1.App, longprofile_1.LongProfile, subscription_1.Subscription, promo_1.Promo, peercomparison_1.PeerComparison, profile_1.Profile, customerinfo_1.CustomerInfo, billhistory_1.BillHistory,
                reportcreate_1.ReportCreate, premiseselect_1.PremiseSelect, greenbutton_1.GreenButton, consumption_1.Consumption, billedusagechart_1.BilledUsageChart, billdisagg_1.BillDisagg,
                reportlist_1.ReportList, quicklinks_1.QuickLinks, billsummary_1.BillSummary, billtodate_1.BillToDate, threshold_1.Threshold, thirdpartyselection_1.ThirdPartySelection, gbcadmin_1.GBCAdmin,
                thirdpartyscopeselection_1.ThirdPartyScopeSelection, unauthenticated_1.Unauthenticated, loader_1.Loader, pipe_1.ObjToArr],
            bootstrap: [app_1.App],
            providers: [
                { provide: http_1.RequestOptions, useClass: defaultrequestoptions_1.DefaultRequestOptions },
                { provide: ng_recaptcha_1.RECAPTCHA_SETTINGS, useValue: { siteKey: '6LfR_TMUAAAAAM39id5_EkA35jOc1fA_-yaDWm0Y' } }
            ]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
if (typeof isDebugEnabled != 'undefined') {
    if (isDebugEnabled != null) {
        if (isDebugEnabled === "False") {
            core_1.enableProdMode();
        }
    }
}
platform_browser_dynamic_1.platformBrowserDynamic().bootstrapModule(AppModule, {
    providers: [
        { provide: compiler_1.DirectiveResolver, useClass: AceViewResolver }
    ]
});
//# sourceMappingURL=app.module.js.map