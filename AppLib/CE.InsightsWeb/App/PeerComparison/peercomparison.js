"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var peercomparison_service_1 = require("./peercomparison.service");
var app_service_1 = require("../app.service");
var PeerComparison = (function () {
    function PeerComparison(peerComparisonService, appService, elementRef) {
        this.elementRef = elementRef;
        this.enableLoader = true;
        this.peerComparisonService = peerComparisonService;
        this.params = appService.getParams();
        this.parameters = this.params[0].parameters;
        this.tabKey = this.params[0].tabKey;
        this.appService = appService;
        if (this.peerComparisonService.res !== undefined) {
            this.result = peerComparisonService.res;
            this.fromUTest = this.peerComparisonService.fromUnitTest;
        }
    }
    PeerComparison_1 = PeerComparison;
    PeerComparison.prototype.ngOnInit = function () {
        if (this.peerComparisonService.getPeerComparison("", "").toString() !== PeerComparison_1.fromUnitTest) {
            this.loadBenchmarks();
        }
    };
    PeerComparison.prototype.loadBenchmarks = function () {
        var _this = this;
        this.peerComparisonService.getPeerComparison(this.tabKey, this.parameters)
            .subscribe(function (res) { return _this.result = res; }, function (err) { return _this.appService.logError(_this, err); }, function () { return _this.disableLoader(); });
    };
    PeerComparison.prototype.disableLoader = function () {
        if (this.result.NoBenchmarks === false) {
            this.setupCommodities();
            this.getBenchmark(this.selectedCommodity);
        }
        this.enableLoader = false;
    };
    PeerComparison.prototype.setupCommodities = function () {
        var coms = this.result.Commodities.split(",");
        var labels = this.result.CommoditiesText.split(",");
        var icons = this.result.CommoditiesIconFonts.split(",");
        if (coms.length > 1) {
            this.commodities = [];
            for (var i = 0; i < coms.length; i++) {
                var com = new Commodity();
                com.key = coms[i];
                com.iconFont = icons[i];
                com.showLabel = this.result.ShowCommodityLabels;
                com.label = labels[i];
                if (com.key === "gas") {
                    if (this.result.GasServiceCount === 0) {
                        continue;
                    }
                }
                if (com.key === "electric") {
                    if (this.result.ElectricServiceCount === 0) {
                        continue;
                    }
                }
                if (com.key === "water") {
                    if (this.result.WaterServiceCount === 0) {
                        continue;
                    }
                }
                if (this.selectedCommodity !== null) {
                    this.selectedCommodity = coms[i];
                }
                this.commodities.push(com);
            }
        }
    };
    PeerComparison.prototype.getBenchmark = function (commodityFilter) {
        var data = {
            MyUnits: "",
            MyUnitsPercentage: 0,
            AvgUnits: "",
            AvgUnitsPercentage: 0,
            EfficientUnits: "",
            EfficientUnitsPercentage: 0,
            MyUnitsFormatted: "",
            AvgUnitsFormatted: "",
            EfficientUnitsFormatted: "",
            YourQtyColor: "",
            AverageQtyColor: "",
            EfficientQtyColor: "",
            YourQty: "",
            AverageQty: "",
            EfficientQty: ""
        };
        var commodity;
        var benchmarks = this.result.Benchmarks;
        this.selectedCommodity = commodityFilter;
        for (var b in benchmarks) {
            if (benchmarks.hasOwnProperty(b)) {
                if (benchmarks[b].CommodityKey === commodityFilter) {
                    commodity = benchmarks[b];
                }
            }
        }
        if (commodity.Measurements != null && commodity.Measurements.length > 0) {
            var measurement;
            measurement = commodity.Measurements[0];
            data.MyUnits = measurement.MyQuantity;
            data.AvgUnits = measurement.AverageQuantity;
            data.EfficientUnits = measurement.EfficientQuantity;
            data.MyUnitsFormatted = measurement.MyQuantityFormatted + " " + measurement.UOMText;
            data.AvgUnitsFormatted = measurement.AverageQuantityFormatted + " " + measurement.UOMText;
            data.EfficientUnitsFormatted = measurement.EfficientQuantityFormatted + " " + measurement.UOMText;
        }
        else if (this.result.CostOrUsage === "cost") {
            data.MyUnits = commodity.MyCost;
            data.AvgUnits = commodity.AverageCost;
            data.EfficientUnits = commodity.EfficientCost;
            data.MyUnitsFormatted = commodity.MyCostFormatted;
            data.AvgUnitsFormatted = commodity.AverageCostFormatted;
            data.EfficientUnitsFormatted = commodity.EfficientCostFormatted;
        }
        else if (this.result.CostOrUsage === "usage") {
            data.MyUnits = commodity.MyUsage;
            data.AvgUnits = commodity.AverageUsage;
            data.EfficientUnits = commodity.EfficientUsage;
            data.MyUnitsFormatted = commodity.MyUsageFormatted + " " + commodity.UOMText;
            data.AvgUnitsFormatted = commodity.AverageUsageFormatted + " " + commodity.UOMText;
            data.EfficientUnitsFormatted = commodity.EfficientUsageFormatted + " " + commodity.UOMText;
        }
        var dividend = Math.max(parseInt(data.MyUnits), parseInt(data.AvgUnits), parseInt(data.EfficientUnits));
        data.MyUnitsPercentage = (parseInt(data.MyUnits) / dividend) * 100;
        data.AvgUnitsPercentage = (parseInt(data.AvgUnits) / dividend) * 100;
        data.EfficientUnitsPercentage = (parseInt(data.EfficientUnits) / dividend) * 100;
        data.YourQtyColor = this.result.YourQtyColor;
        data.AverageQtyColor = this.result.AverageQtyColor;
        data.EfficientQtyColor = this.result.EfficientQtyColor;
        data.YourQty = this.result.YourQty;
        data.AverageQty = this.result.AverageQty;
        data.EfficientQty = this.result.EfficientQty;
        this.result.data = data;
    };
    PeerComparison.fromUnitTest = "unittest";
    PeerComparison = PeerComparison_1 = __decorate([
        core_1.Component({
            selector: 'peercomparison',
            template: "<Loader *ngIf=\"enableLoader\"></Loader><Loader *ngIf=\"enableLoader\"></Loader><div *ngIf=\"!enableLoader\"><div *ngIf=\"errorOccurred\"><div class=\"panel panel-default\"><div class=\"alert alert-danger\"><span class=\"glyphicon glyphicon-exclamation-sign xs-mr-10\" aria-hidden=\"true\"></span><strong>{{errorMessage}}</strong> <span class=\"sr-only\">Error:</span></div></div></div><div *ngIf=\"!errorOccurred\" class=\"panel panel-default usage-summary\"><div *ngIf=\"result.ShowTitle == 'true' && result.Title !== ''\" class=\"panel-heading\"><h1 class=\"panel-title\" role=\"presentation\">{{result.Title}}</h1><div *ngIf=\"result.ShowIntro == 'true' && result.ShowIntro !== ''\" class=\"intro-text\" style=\"white-space: pre-line\">{{result.IntroText}}</div></div><div *ngIf=\"result.NoBenchmarks == true\" class=\"alert alert-warning\"><span class=\"glyphicon glyphicon-alert xs-mr-10\"></span> <strong>{{result.NoBenchmarksText}}</strong></div><h2 *ngIf=\"result.NoBenchmarks !== true && result.ShowSubTitle == 'true' && result.SubTitle !== ''\" role=\"presentation\" class=\"subtitle\">{{result.SubTitle}}</h2><div *ngIf=\"result.NoBenchmarks !== true && commodities.length > 1\" class=\"panel-body utility\"><div class=\"utility-icons pull-right\"><div class=\"u-icon-focus\"><a *ngFor=\"let c of commodities;\" href=\"#\" (click)=\"getBenchmark(c.key)\" title=\"{{c.label}}\" onclick=\"return false;\" class=\"btn btn-default icon__{{c.key}} xs-ml-5\" [ngClass]=\"c.key === selectedCommodity ? 'icon_focus' : ''\" role=\"button\"><span class=\"sr-only\">{{c.label}} commodity</span> <span class=\"icon {{c.iconFont}}\"></span> <span *ngIf=\"c.ShowCommodityLabels == 'true'\" class=\"hidden-xs\">{{c.label}}</span> <span class=\"print-only\">{{c.label}}</span></a></div></div></div><div *ngIf=\"result.NoBenchmarks !== true\" class=\"pc-bar-chart clearfix xs-p-15\"><div class=\"pc-bar-chart__item\"><div class=\"pc-bar-chart__bar\"><span class=\"pc-bar-chart__chart-title\">{{result.YourQty}}</span><div class=\"pc-bar-chart__item-progress pc-bar-chart__lastmonth\"><div class=\"pc-bar-chart__bar-lastmonth\" [style.width]=\"result.data.MyUnitsPercentage + '%'\"><span class=\"pc-bar-chart__value pull-right\" *ngIf=\"result.data.AvgUnitsPercentage > 30\">{{result.data.MyUnitsFormatted}}</span></div><div class=\"pc-bar-chart__filler text-left\"><span class=\"pc-bar-chart__value\" *ngIf=\"result.data.AvgUnitsPercentage <= 30\">{{result.data.MyUnitsFormatted}}</span></div></div></div></div><div class=\"pc-bar-chart__item\"><div class=\"pc-bar-chart__bar\"><span class=\"pc-bar-chart__chart-title\">{{result.AverageQty}}</span><div class=\"pc-bar-chart__item-progress pc-bar-chart__similarhomes\"><div class=\"pc-bar-chart__bar-similarhomes\" [style.width]=\"result.data.AvgUnitsPercentage + '%'\"><span *ngIf=\"result.data.AvgUnitsPercentage > 30\" class=\"pc-bar-chart__value pull-right\">{{result.data.AvgUnitsFormatted}}</span></div><div class=\"pc-bar-chart__filler text-left\"><span *ngIf=\"result.data.AvgUnitsPercentage <= 30\" class=\"pc-bar-chart__value\">{{result.data.AvgUnitsFormatted}}</span></div></div></div></div><div class=\"pc-bar-chart__item\"><div class=\"pc-bar-chart__bar\"><span class=\"pc-bar-chart__chart-title\">{{result.EfficientQty}}</span><div class=\"pc-bar-chart__item-progress pc-bar-chart__efficienthomes\"><div class=\"pc-bar-chart__bar-efficienthomes\" [style.width]=\"result.data.EfficientUnitsPercentage + '%'\"><span class=\"pc-bar-chart__value pull-right\" *ngIf=\"result.data.AvgUnitsPercentage > 30\">{{result.data.EfficientUnitsFormatted}}</span></div><div class=\"pc-bar-chart__filler text-left\"><span class=\"pc-bar-chart__value\" *ngIf=\"result.data.AvgUnitsPercentage <= 30\">{{result.data.EfficientUnitsFormatted}}</span></div></div></div></div></div><div *ngIf=\"result.ShowFooterText === 'true' || result.ShowFooterLink === 'true'\" class=\"panel-footer\"><div *ngIf=\"result.ShowFooterText === 'true'\" class=\"footertext\">{{result.Footer}}</div><div *ngIf=\"result.ShowFooterLink === 'true'\" class=\"footerlink\"><a href=\"{{result.FooterLink}}\">{{result.FooterLinkText}}</a></div></div></div></div>",
            providers: [app_service_1.AppService, peercomparison_service_1.PeerComparisonService]
        }),
        __metadata("design:paramtypes", [peercomparison_service_1.PeerComparisonService, app_service_1.AppService, core_1.ElementRef])
    ], PeerComparison);
    return PeerComparison;
    var PeerComparison_1;
}());
exports.PeerComparison = PeerComparison;
var Commodity = (function () {
    function Commodity() {
    }
    return Commodity;
}());
exports.Commodity = Commodity;

//# sourceMappingURL=peercomparison.js.map
