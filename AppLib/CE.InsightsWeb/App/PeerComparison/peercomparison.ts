﻿import { Component, ElementRef, OnInit, } from "@angular/core";
import { PeerComparisonService } from "./peercomparison.service";
import { AppService, IAppParams, } from "../app.service";

@Component({
    selector: 'peercomparison',
    templateUrl: 'App/PeerComparison/peercomparison.html',
    providers: [AppService, PeerComparisonService]
})

export class PeerComparison implements OnInit {
    static fromUnitTest = "unittest";
    elementRef: ElementRef;
    result: any;
    error: Object;
    enableLoader: Boolean;
    data: Array<Number>;
    selectedCommodity: string;
    comList: Array<String>;
    options: Object;
    peerComparisonService: PeerComparisonService;
    appService: AppService;
    params: Array<IAppParams>;
    parameters: string;
    tabKey: string;
    showCommodityLabels: Boolean;
    commodities: Array<Commodity>;
    fromUTest: Boolean;

    constructor(peerComparisonService: PeerComparisonService, appService: AppService, elementRef: ElementRef) {
        this.elementRef = elementRef;
        this.enableLoader = true;
        this.peerComparisonService = peerComparisonService;
        this.params = appService.getParams();
        this.parameters = this.params[0].parameters;
        this.tabKey = this.params[0].tabKey;
        this.appService = appService;
        if (this.peerComparisonService.res !== undefined) {
            this.result = peerComparisonService.res;
            this.fromUTest = this.peerComparisonService.fromUnitTest;
        }
    }

    ngOnInit() {
        if (this.peerComparisonService.getPeerComparison("", "").toString() !== PeerComparison.fromUnitTest) {
            this.loadBenchmarks();
        }
    }

    loadBenchmarks() {   
        this.peerComparisonService.getPeerComparison(this.tabKey, this.parameters)
            .subscribe(res => this.result = res,
            err => this.appService.logError(this, err),
            () => this.disableLoader()
            );
    }

    disableLoader() {
        if (this.result.NoBenchmarks === false) {
            this.setupCommodities();
            this.getBenchmark(this.selectedCommodity);
        }
        this.enableLoader = false;
    }

    setupCommodities() {
        const coms = this.result.Commodities.split(",");
        const labels = this.result.CommoditiesText.split(",");
        const icons = this.result.CommoditiesIconFonts.split(",");
        if (coms.length > 1) {
            this.commodities = [];
            for (let i = 0; i < coms.length; i++) {
                const com = new Commodity();
                com.key = coms[i];
                com.iconFont = icons[i];
                com.showLabel = this.result.ShowCommodityLabels;
                com.label = labels[i];
                if (com.key === "gas") {
                    if (this.result.GasServiceCount === 0) {
                        continue;
                    }
                }
                if (com.key === "electric") {
                    if (this.result.ElectricServiceCount === 0) {
                        continue;
                    }
                }
                if (com.key === "water") {
                    if (this.result.WaterServiceCount === 0) {
                        continue;
                    }
                }
                if (this.selectedCommodity !== null) {
                    this.selectedCommodity = coms[i];
                }
                this.commodities.push(com);
            }
        }
    }

    getBenchmark(commodityFilter) {

        var data = {
            MyUnits: "",
            MyUnitsPercentage: 0,
            AvgUnits: "",
            AvgUnitsPercentage: 0,
            EfficientUnits: "",
            EfficientUnitsPercentage: 0,
            MyUnitsFormatted: "",
            AvgUnitsFormatted: "",
            EfficientUnitsFormatted: "",
            YourQtyColor: "",
            AverageQtyColor: "",
            EfficientQtyColor: "",
            YourQty: "",
            AverageQty: "",
            EfficientQty: ""
        }

        var commodity;
        var benchmarks = this.result.Benchmarks;
        this.selectedCommodity = commodityFilter;
        for (var b in benchmarks) {
            if (benchmarks.hasOwnProperty(b)) {
                if (benchmarks[b].CommodityKey === commodityFilter) {
                    commodity = benchmarks[b];
                }
            }
        }

        if (commodity.Measurements != null && commodity.Measurements.length > 0) {
            var measurement;
            measurement = commodity.Measurements[0];

            data.MyUnits = measurement.MyQuantity;
            data.AvgUnits = measurement.AverageQuantity;
            data.EfficientUnits = measurement.EfficientQuantity;
            data.MyUnitsFormatted = measurement.MyQuantityFormatted + " " + measurement.UOMText;
            data.AvgUnitsFormatted = measurement.AverageQuantityFormatted + " " + measurement.UOMText;
            data.EfficientUnitsFormatted = measurement.EfficientQuantityFormatted + " " + measurement.UOMText;
        }
        else if (this.result.CostOrUsage === "cost") {
            data.MyUnits = commodity.MyCost;
            data.AvgUnits = commodity.AverageCost;
            data.EfficientUnits = commodity.EfficientCost;
            data.MyUnitsFormatted = commodity.MyCostFormatted;
            data.AvgUnitsFormatted = commodity.AverageCostFormatted;
            data.EfficientUnitsFormatted = commodity.EfficientCostFormatted;
        }
        else if (this.result.CostOrUsage === "usage") {
            data.MyUnits = commodity.MyUsage;
            data.AvgUnits = commodity.AverageUsage;
            data.EfficientUnits = commodity.EfficientUsage;
            data.MyUnitsFormatted = commodity.MyUsageFormatted + " " + commodity.UOMText;
            data.AvgUnitsFormatted = commodity.AverageUsageFormatted + " " + commodity.UOMText;
            data.EfficientUnitsFormatted = commodity.EfficientUsageFormatted + " " + commodity.UOMText;
        }

        var dividend = Math.max(parseInt(data.MyUnits), parseInt(data.AvgUnits), parseInt(data.EfficientUnits));

        data.MyUnitsPercentage = (parseInt(data.MyUnits) / dividend) * 100;
        data.AvgUnitsPercentage = (parseInt(data.AvgUnits) / dividend) * 100;
        data.EfficientUnitsPercentage = (parseInt(data.EfficientUnits) / dividend) * 100;

        data.YourQtyColor = this.result.YourQtyColor;
        data.AverageQtyColor = this.result.AverageQtyColor;
        data.EfficientQtyColor = this.result.EfficientQtyColor;
        data.YourQty = this.result.YourQty;
        data.AverageQty = this.result.AverageQty;
        data.EfficientQty = this.result.EfficientQty;

        this.result.data = data;
    }
}


export class Commodity {
    key: string;
    iconFont: string;
    showLabel: Boolean;
    label: string;
}
