﻿import "rxjs/add/operator/map";
import { Injectable, Input } from "@angular/core"
import { Http } from "@angular/http";

@Injectable()
export class PeerComparisonService {
    @Input() res: Object;
    @Input() fromUnitTest: Boolean;
    http: Http;
    constructor(http: Http) {
        this.http = http;
    }

    getPeerComparison(tabKey, params) {
        return this.http.get(`./PeerComparison/Get?tabkey=${tabKey}&parameters=${window.btoa(params)}`).map(res => res.json());
    }
}



