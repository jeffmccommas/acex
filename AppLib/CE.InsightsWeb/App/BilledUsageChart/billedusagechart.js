"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var billedusagechart_service_1 = require("./billedusagechart.service");
var app_service_1 = require("../app.service");
var BilledUsageChart = (function () {
    function BilledUsageChart(billedusagechartService, appService, elementRef) {
        this.enableLoader = true;
        this.elementRef = elementRef;
        this.params = appService.getParams();
        this.parameters = this.params[0].parameters;
        this.tabKey = this.params[0].tabKey;
        this.errorMessage = this.params[0].errorMessage;
        this.billedusagechartService = billedusagechartService;
        this.appService = appService;
        this.showChart = true;
        this.commodityCount = 0;
        if (this.billedusagechartService.res !== undefined) {
            this.result = billedusagechartService.res;
            this.fromUTest = this.billedusagechartService.fromUnitTest;
        }
    }
    BilledUsageChart_1 = BilledUsageChart;
    BilledUsageChart.prototype.ngOnInit = function () {
        if (this.billedusagechartService.getBilledUsageChart("", "").toString() !== BilledUsageChart_1.fromUnitTest) {
            this.loadusagechart();
        }
    };
    BilledUsageChart.prototype.loadusagechart = function () {
        var _this = this;
        this.result = {};
        if (!this.fromUTest) {
            this.billedusagechartService.getBilledUsageChart(this.tabKey, this.parameters)
                .subscribe(function (res) { return _this.result = res; }, function (err) { return _this.appService.logError(_this, err); }, function () { return _this.disableLoader(); });
        }
    };
    BilledUsageChart.prototype.disableLoader = function () {
        var _this = this;
        if (!this.fromUTest) {
            setTimeout(function () { return _this.appService.setHtml(_this.elementRef); }, 1);
            setTimeout(function () { return _this.appService.setIframeExternalClass(_this.elementRef); }, 1);
        }
        this.enableLoader = false;
        if (this.result.CommoditiesText !== null) {
            var comsText = this.result.CommoditiesText.split(",");
            var comsVal = this.result.Commodities.split(",");
            var commodityfound = false;
            this.result.ShowCommodities = true;
            if (comsVal.length > 1) {
                for (var i = 0; i < comsVal.length; i++) {
                    if (comsVal[i] === "gas") {
                        if (this.result.GasServiceCount > 0) {
                            this.result.ShowGas = true;
                            this.result.GasLabel = comsText[i];
                            this.commodityCount++;
                            if (commodityfound === false) {
                                commodityfound = true;
                                this.selectedCommodity = "gas";
                            }
                        }
                    }
                    if (comsVal[i] === "electric") {
                        if (this.result.ElectricServiceCount > 0) {
                            this.result.ShowElectric = true;
                            this.result.ElectricLabel = comsText[i];
                            this.commodityCount++;
                            if (commodityfound === false) {
                                commodityfound = true;
                                this.selectedCommodity = "electric";
                            }
                        }
                    }
                    if (comsVal[i] === "water") {
                        if (this.result.WaterServiceCount > 0) {
                            this.result.ShowWater = true;
                            this.result.WaterLabel = comsText[i];
                            this.commodityCount++;
                            if (commodityfound === false) {
                                commodityfound = true;
                                this.selectedCommodity = "water";
                            }
                        }
                    }
                }
            }
            if (this.commodityCount < 2) {
                this.result.ShowCommodities = false;
            }
            setTimeout(function () { return _this.getUsage(_this.selectedCommodity); });
        }
    };
    BilledUsageChart.prototype.getUsage = function (commodity) {
        var _this = this;
        if (commodity !== '') {
            this.selectedCommodity = commodity;
        }
        else {
            this.showChart = !this.showChart;
        }
        var options;
        if (this.table !== undefined) {
            this.table.length = 0;
        }
        if (!this.fromUTest) {
            if (this.showChart) {
                setTimeout(function () { return _this.appService.buildUsageChart(_this.result, options, _this.selectedCommodity, _this.commodityCount); }, 0);
            }
            else {
                this.drawTable();
            }
        }
    };
    BilledUsageChart.prototype.drawTable = function () {
        var count = this.commodityCount * 12;
        var col1, col2, col3;
        this.table = [{}];
        this.table.length = 0;
        for (var i = 0; i < count; i++) {
            if (this.selectedCommodity === this.result.BilledUsageLastYear[i]["Commodity"]) {
                col1 = this.result.BilledUsageLastYear[i]["XAxis"];
                var usageLastYear = this.result.BilledUsageLastYear[i]["YAxis"];
                var usagePreviousYear = this.result.BilledUsagePreviousYear[i]["YAxis"];
                var lastYrServices = this.result.BilledUsageLastYear[i]["Tip"].Services;
                var previousYrServices = this.result.BilledUsagePreviousYear[i]["Tip"].Services;
                var maxService = 0;
                if (usageLastYear === 0 && usagePreviousYear !== undefined && usagePreviousYear !== 0) {
                    maxService = previousYrServices.length;
                }
                else if (usageLastYear !== 0 && lastYrServices !== undefined && usagePreviousYear === 0) {
                    maxService = lastYrServices.length;
                }
                else {
                    maxService = usagePreviousYear.length > usageLastYear.length
                        ? previousYrServices.length
                        : lastYrServices.length;
                }
                for (var j = 0; j < maxService; j++) {
                    if (usageLastYear === "" || lastYrServices === undefined || (lastYrServices !== undefined && j >= lastYrServices.length)) {
                        col2 = "none";
                    }
                    else {
                        if (lastYrServices[j].ServiceAndCost === "") {
                            col2 = "none";
                        }
                        else {
                            col2 = lastYrServices[j].ServiceAndCost +
                                " | " +
                                this.result.WeatherLabel +
                                " " +
                                this.result.BilledUsageLastYear[i]["Tip"].AverageTemperature +
                                "°" +
                                " | Billed on " +
                                lastYrServices[j].XAxis;
                        }
                    }
                    if (usagePreviousYear === "" || previousYrServices === undefined || (previousYrServices !== undefined && j >= previousYrServices.length)) {
                        col3 = "none";
                    }
                    else {
                        if (previousYrServices[j].ServiceAndCost === "") {
                            col3 = "none";
                        }
                        else {
                            col3 = previousYrServices[j].ServiceAndCost +
                                " | " +
                                this.result.WeatherLabel +
                                " " +
                                this.result.BilledUsagePreviousYear[i]["Tip"].AverageTemperature +
                                "°" +
                                " | Billed on " +
                                previousYrServices[j].XAxis;
                        }
                    }
                    this.table.push({ "col1": col1, "col2": col2, "col3": col3 });
                }
            }
        }
    };
    BilledUsageChart.fromUnitTest = "unittest";
    BilledUsageChart = BilledUsageChart_1 = __decorate([
        core_1.Component({
            selector: 'billedusagechart',
            template: "<Loader *ngIf=\"enableLoader\"></Loader><div *ngIf=\"!enableLoader\"><div *ngIf=\"errorOccurred\"><div class=\"panel panel-default\"><div class=\"alert alert-danger\"><span class=\"glyphicon glyphicon-exclamation-sign xs-mr-10\" aria-hidden=\"true\"></span><strong>{{errorMessage}}</strong> <span class=\"sr-only\">Error:</span></div></div></div><div *ngIf=\"!errorOccurred\"><div *ngIf=\"result.NoBills == true\" class=\"panel panel-default\"><div *ngIf=\"result.ShowTitle == 'true' && result.Title !== ''\" class=\"panel-heading\"><h1 class=\"panel-title\" role=\"presentation\">{{result.Title}}</h1></div><div class=\"alert alert-warning\"><span class=\"glyphicon glyphicon-alert xs-mr-10\"></span> <strong>{{result.NoBillsText}}</strong></div></div><div *ngIf=\"result.NoBills !== true && result.BilledUsageLastYear !== null && result.BilledUsagePreviousYear!==null && result.CommoditiesText !== null\" class=\"panel panel-default panel-bill-usage\"><div *ngIf=\"result.ShowTitle == 'true' && result.Title !== ''\" class=\"panel-heading\"><h1 class=\"panel-title\" role=\"presentation\">{{result.Title}}</h1></div><div *ngIf=\"result.ShowSubTitle === 'true' || result.ShowIntro === 'true'\"><div class=\"panel-body xs-pt-0 xs-pb-0 xs-mb-0\"><div *ngIf=\"result.ShowSubTitle == 'true' && result.SubTitle !== ''\" class=\"sub-title xs-mb-10\">{{result.SubTitle}}</div><div *ngIf=\"result.ShowIntro == 'true' && result.ShowIntro !== ''\" class=\"intro-text\" style=\"white-space: pre-line\">{{result.IntroText}}</div></div></div><div class=\"panel-body utility\"><div class=\"utility-icons pull-right\"><div class=\"u-icon-focus\"><a *ngIf=\"result.ShowCommodities === true && result.ShowGas === true\" aria-label=\"Gas Commodity\" (click)=\"getUsage('gas')\" href=\"#\" onclick=\"return false;\" class=\"btn btn-default xs-ml-5 icon__gas\" [ngClass]=\"selectedCommodity==='gas' ? 'icon_focus' : ''\" role=\"button\"><span class=\"sr-only\">{{result.GasLabel}} commodity</span><span class=\"icon icon-flame\"></span> <span *ngIf=\"result.ShowCommodityLabels === 'true'\" class=\"hidden-xs\">{{result.GasLabel}}</span> <span class=\"print-only\">{{result.GasLabel}}</span> </a><a *ngIf=\"result.ShowCommodities === true && result.ShowElectric === true\" aria-label=\"Electric Commodity\" (click)=\"getUsage('electric')\" href=\"#\" onclick=\"return false;\" class=\"btn btn-default xs-ml-5 icon__electric\" [ngClass]=\"selectedCommodity==='electric' ? 'icon_focus' : ''\" role=\"button\"><span class=\"sr-only\">{{result.ElectricLabel}} commodity</span><span class=\"icon icon-bolt\"></span> <span *ngIf=\"result.ShowCommodityLabels === 'true'\" class=\"hidden-xs\">{{result.ElectricLabel}}</span> <span class=\"print-only\">{{result.ElectricLabel}}</span> </a><a *ngIf=\"result.ShowCommodities === true && result.ShowWater === true\" aria-label=\"Water Commodity\" (click)=\"getUsage('water')\" href=\"#\" onclick=\"return false;\" class=\"btn btn-default xs-ml-5 icon__water\" [ngClass]=\"selectedCommodity==='water' ? 'icon_focus' : ''\" role=\"button\"><span class=\"sr-only\">{{result.WaterLabel}} commodity</span><span class=\"icon icon-drop\"></span> <span *ngIf=\"result.ShowCommodityLabels === 'true'\" class=\"hidden-xs\">{{result.WaterLabel}}</span> <span class=\"print-only\">{{result.WaterLabel}}</span> </a><a href=\"#\" onclick=\"return false;\" (click)=\"getUsage('')\" class=\"btn btn-default chart-table-spacer\" aria-label=\"Toggle chart table data\" [ngClass]=\"showChart ? 'icon__table' : 'icon__graph'\" role=\"button\"><span class=\"sr-only\">Toggle chart table data</span> <span class=\"icon\" [ngClass]=\"showChart ? 'icon-table' : 'icon-graph'\"></span></a></div></div></div><div class=\"panel-body\"><div [style.display]=\"showChart ? 'block': 'none'\"><div id=\"usage_chart\" style=\"height: 300px\"></div></div><div [style.display]=\"showChart ? 'none': 'block'\"><div class=\"table-responsive\" role=\"grid\" aria-readonly=\"true\"><table class=\"table\" role=\"presentation\"><thead><tr role=\"row\"><th role=\"columnheader\">Month</th><th role=\"columnheader\">{{result.CurrentYearLegend}}</th><th role=\"columnheader\">{{result.PrevYearLegend}}</th></tr></thead><tbody><tr *ngFor=\"let l of table\" role=\"row\"><td role=\"rowheader\"><a href=\"#\" onclick=\"return false;\">{{l.col1}}</a></td><td role=\"gridcell\"><a href=\"#\" onclick=\"return false;\">{{l.col2}}</a></td><td role=\"gridcell\"><a href=\"#\" onclick=\"return false;\">{{l.col3}}</a></td></tr></tbody></table></div></div></div><div *ngIf=\"result.ShowFooterText === 'true' || result.ShowFooterLink === 'true'\" class=\"panel-footer\"><div *ngIf=\"result.ShowFooterText === 'true'\" class=\"footertext\">{{result.Footer}}</div><div *ngIf=\"result.ShowFooterLink === 'true'\" class=\"footerlink dv-external-links\"><a href=\"{{result.FooterLink}}\">{{result.FooterLinkText}}</a></div></div></div></div></div>",
            providers: [billedusagechart_service_1.BilledUsageChartService, app_service_1.AppService]
        }),
        __metadata("design:paramtypes", [billedusagechart_service_1.BilledUsageChartService, app_service_1.AppService, core_1.ElementRef])
    ], BilledUsageChart);
    return BilledUsageChart;
    var BilledUsageChart_1;
}());
exports.BilledUsageChart = BilledUsageChart;

//# sourceMappingURL=billedusagechart.js.map
