﻿import { Component, ElementRef, OnInit } from "@angular/core";
import { BilledUsageChartService } from "./billedusagechart.service";
import { AppService, IAppParams } from "../app.service";

@Component({
    selector: 'billedusagechart',
    templateUrl: 'App/BilledUsageChart/billedusagechart.html',
    providers: [BilledUsageChartService, AppService]
})
export class BilledUsageChart implements OnInit {
    static fromUnitTest = "unittest";
    elementRef: ElementRef;
    result: any;
    table: any;
    error: Object;
    enableLoader: Boolean;
    errorOccurred: Boolean;
    errorMessage: string;
    billedusagechartService: BilledUsageChartService;
    appService: AppService;
    params: Array<IAppParams>;
    parameters: string;
    tabKey: string;
    fromUTest: Boolean;
    selectedCommodity: string;
    showChart: Boolean;
    commodityCount: number;

    constructor(billedusagechartService: BilledUsageChartService, appService: AppService, elementRef: ElementRef) {
        this.enableLoader = true;
        this.elementRef = elementRef;
        this.params = appService.getParams();
        this.parameters = this.params[0].parameters;
        this.tabKey = this.params[0].tabKey;
        this.errorMessage = this.params[0].errorMessage;
        this.billedusagechartService = billedusagechartService;
        this.appService = appService;
        this.showChart = true;
        this.commodityCount = 0;
        if (this.billedusagechartService.res !== undefined) {
            this.result = billedusagechartService.res;
            this.fromUTest = this.billedusagechartService.fromUnitTest;
        }
    }

    ngOnInit() {
        if (this.billedusagechartService.getBilledUsageChart("", "").toString() !== BilledUsageChart.fromUnitTest) {
            this.loadusagechart();
        }
    }

    loadusagechart() {
        this.result = {};
        if (!this.fromUTest) {
            this.billedusagechartService.getBilledUsageChart(this.tabKey, this.parameters)
                .subscribe(res => this.result = res,
                err => this.appService.logError(this, err),
                () => this.disableLoader()
                );                         
        }
    }

    disableLoader() {
        if (!this.fromUTest) {
            setTimeout(() => this.appService.setHtml(this.elementRef), 1);
            setTimeout(() => this.appService.setIframeExternalClass(this.elementRef), 1);                        
        }
        this.enableLoader = false;
        //check for commoditites here
        if (this.result.CommoditiesText !== null) {
            var comsText = this.result.CommoditiesText.split(",");
            var comsVal = this.result.Commodities.split(",");
            var commodityfound = false;
            this.result.ShowCommodities = true;
            if (comsVal.length > 1) {
                for (var i = 0; i < comsVal.length; i++) {
                    if (comsVal[i] === "gas") {
                        if (this.result.GasServiceCount > 0) {
                            this.result.ShowGas = true;
                            this.result.GasLabel = comsText[i];
                            this.commodityCount++;
                            if (commodityfound === false) {
                                commodityfound = true;
                                this.selectedCommodity = "gas";
                            }
                        }
                    }
                    if (comsVal[i] === "electric") {
                        if (this.result.ElectricServiceCount > 0) {
                            this.result.ShowElectric = true;
                            this.result.ElectricLabel = comsText[i];
                            this.commodityCount++;
                            if (commodityfound === false) {
                                commodityfound = true;
                                this.selectedCommodity = "electric";
                            }
                        }
                    }
                    if (comsVal[i] === "water") {
                        if (this.result.WaterServiceCount > 0) {
                            this.result.ShowWater = true;
                            this.result.WaterLabel = comsText[i];
                            this.commodityCount++;
                            if (commodityfound === false) {
                                commodityfound = true;
                                this.selectedCommodity = "water";
                            }
                        }
                    }
                }
            }
            if (this.commodityCount < 2) {
                this.result.ShowCommodities = false;
            }
            setTimeout(() => this.getUsage(this.selectedCommodity));
        }
    } 

    getUsage(commodity) {      
        if (commodity !== '')
        {
            this.selectedCommodity = commodity;
        }
        else
        {
            this.showChart = !this.showChart;
        }
        var options;
            if (this.table !== undefined) {
                this.table.length = 0;
            }
            if (!this.fromUTest) { 
                if (this.showChart)
                {
                    setTimeout(() => this.appService.buildUsageChart(this.result, options, this.selectedCommodity, this.commodityCount), 0);
                }
                else
                {
                    this.drawTable();
                }
            }
    }

    drawTable() {
        var count = this.commodityCount * 12;
        var col1, col2, col3;
        this.table = [{}];
        this.table.length = 0;
        for (var i = 0; i < count; i++) {
            if (this.selectedCommodity === this.result.BilledUsageLastYear[i]["Commodity"]) {
                col1 = this.result.BilledUsageLastYear[i]["XAxis"];
                var usageLastYear = this.result.BilledUsageLastYear[i]["YAxis"];
                var usagePreviousYear = this.result.BilledUsagePreviousYear[i]["YAxis"];
                var lastYrServices = this.result.BilledUsageLastYear[i]["Tip"].Services;
                var previousYrServices = this.result.BilledUsagePreviousYear[i]["Tip"].Services;
                    
                var maxService = 0;
                if (usageLastYear === 0 && usagePreviousYear !== undefined && usagePreviousYear !== 0) {
                    maxService = previousYrServices.length;
                } else if (usageLastYear !== 0 && lastYrServices !== undefined && usagePreviousYear === 0) {
                    maxService = lastYrServices.length;
                } else {
                    maxService = usagePreviousYear.length > usageLastYear.length
                        ? previousYrServices.length
                        : lastYrServices.length;
                }

                for (var j = 0; j < maxService; j++) {
                    if (usageLastYear === "" || lastYrServices === undefined || (lastYrServices !== undefined && j >= lastYrServices.length)) {
                        col2 = "none";
                    } else {
                        if (lastYrServices[j].ServiceAndCost === "") {
                            col2 = "none";
                        } else {
                            col2 = lastYrServices[j].ServiceAndCost +
                                " | " +
                                this.result.WeatherLabel +
                                " " +
                                this.result.BilledUsageLastYear[i]["Tip"].AverageTemperature +
                                "°" +
                                " | Billed on " +
                                lastYrServices[j].XAxis;
                        }
                    }

                    if (usagePreviousYear === "" || previousYrServices === undefined || (previousYrServices !== undefined &&j >= previousYrServices.length)) {
                        col3 = "none";
                    } else {
                        if (previousYrServices[j].ServiceAndCost === "") {
                            col3 = "none";
                        } else {
                            col3 = previousYrServices[j].ServiceAndCost +
                                " | " +
                                this.result.WeatherLabel +
                                " " +
                                this.result.BilledUsagePreviousYear[i]["Tip"].AverageTemperature +
                                "°" +
                                " | Billed on " +
                                previousYrServices[j].XAxis;
                        }
                    }
                    this.table.push({ "col1": col1, "col2": col2, "col3": col3 });
                }
            }      
        }
    }
}