﻿import { Component, ElementRef, OnInit } from "@angular/core";
import { QuickLinksService } from "./quicklinks.service";
import { AppService, IAppParams } from "../app.service";

@Component({
    selector: 'quicklinks',
    templateUrl: 'App/QuickLinks/quicklinks.html',
    providers: [QuickLinksService, AppService]
})
export class QuickLinks implements OnInit {
    elementRef: ElementRef;
    result: any;
    error: Object;
    postEvent: Object;
    enableLoader: Boolean;
    errorOccurred: Boolean;
    errorMessage: string;
    quicklinksService: QuickLinksService;
    appService: AppService;
    params: Array<IAppParams>;
    parameters: string;
    tabKey: string;
    fromUTest: Boolean;
    
    constructor(quicklinksService: QuickLinksService, appService: AppService, elementRef: ElementRef) {
        this.enableLoader = true;
        this.elementRef = elementRef;
        this.params = appService.getParams();
        this.parameters = this.params[0].parameters;
        this.tabKey = this.params[0].tabKey;
        this.errorMessage = this.params[0].errorMessage;
        this.quicklinksService = quicklinksService;
        this.appService = appService;
        this.fromUTest = false;
        if (this.quicklinksService.res !== undefined) {
            this.result = quicklinksService.res;
            this.fromUTest = this.quicklinksService.fromUnitTest;
        }
    }

    ngOnInit() {
        this.loadLinks();
    }

    loadLinks() {
        this.result = {};
        if (!this.fromUTest) {
            this.quicklinksService.getQuickLinks(this.tabKey, this.parameters)
                .subscribe(res => this.result = res,
                err => this.appService.logError(this, err)
                );
            
        }
        this.enableLoader = false;
    }

    postEvents(url, key){
        const ei = [];
        ei.push({ key: "ActionId", value: key });
        ei.push({ key: "TabId", value: this.tabKey });
        ei.push({ key: "Link", value: url });
        if (!this.fromUTest) {
            this.appService.postEvents(ei, this.parameters, "Click Quicklink")
                .subscribe(res => this.postEvent = res,
                    err => this.appService.logError(this, err)
                );
        }
    }
}