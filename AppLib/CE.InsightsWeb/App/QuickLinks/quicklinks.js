"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var quicklinks_service_1 = require("./quicklinks.service");
var app_service_1 = require("../app.service");
var QuickLinks = (function () {
    function QuickLinks(quicklinksService, appService, elementRef) {
        this.enableLoader = true;
        this.elementRef = elementRef;
        this.params = appService.getParams();
        this.parameters = this.params[0].parameters;
        this.tabKey = this.params[0].tabKey;
        this.errorMessage = this.params[0].errorMessage;
        this.quicklinksService = quicklinksService;
        this.appService = appService;
        this.fromUTest = false;
        if (this.quicklinksService.res !== undefined) {
            this.result = quicklinksService.res;
            this.fromUTest = this.quicklinksService.fromUnitTest;
        }
    }
    QuickLinks.prototype.ngOnInit = function () {
        this.loadLinks();
    };
    QuickLinks.prototype.loadLinks = function () {
        var _this = this;
        this.result = {};
        if (!this.fromUTest) {
            this.quicklinksService.getQuickLinks(this.tabKey, this.parameters)
                .subscribe(function (res) { return _this.result = res; }, function (err) { return _this.appService.logError(_this, err); });
        }
        this.enableLoader = false;
    };
    QuickLinks.prototype.postEvents = function (url, key) {
        var _this = this;
        var ei = [];
        ei.push({ key: "ActionId", value: key });
        ei.push({ key: "TabId", value: this.tabKey });
        ei.push({ key: "Link", value: url });
        if (!this.fromUTest) {
            this.appService.postEvents(ei, this.parameters, "Click Quicklink")
                .subscribe(function (res) { return _this.postEvent = res; }, function (err) { return _this.appService.logError(_this, err); });
        }
    };
    QuickLinks = __decorate([
        core_1.Component({
            selector: 'quicklinks',
            template: "<Loader *ngIf=\"enableLoader\"></Loader><div *ngIf=\"!enableLoader\"><div *ngIf=\"errorOccurred\"><div class=\"panel panel-default\"><div *ngIf=\"result.ShowTitle == 'true' && result.Title !== ''\" class=\"panel-heading\"><h1 role=\"presentation\" class=\"panel-title\">{{result.Title}}</h1></div><div class=\"alert alert-danger\"><span class=\"glyphicon glyphicon-exclamation-sign xs-mr-10\" aria-hidden=\"true\"></span><strong>{{errorMessage}}</strong> <span class=\"sr-only\">Error:</span></div></div></div><div *ngIf=\"!errorOccurred\" class=\"panel panel-default\"><div *ngIf=\"result.ShowTitle == 'true'\" class=\"panel-heading\"><h1 role=\"presentation\" class=\"panel-title\">{{result.Title}}</h1></div><div *ngIf=\"result.ShowSubTitle === 'true' || result.ShowIntro === 'true'\"><div class=\"panel-body intro-text xs-pt-0 xs-pb-0 xs-pr-0 xs-mb-0\"><div *ngIf=\"result.ShowSubTitle == 'true' && result.SubTitle !== ''\" role=\"presentation\" class=\"subtitle\">{{result.SubTitle}}</div><div *ngIf=\"result.ShowIntro == 'true' && result.ShowIntro !== ''\" class=\"markdown\">{{result.IntroText}}</div></div></div><div class=\"quick-links xs-p-15\" role=\"complementary\"><a *ngFor=\"let l of result.LinkList | objToArr\" class=\"xs-mb-5 quick\" [href]=\"l.Url\" [name]=\"l.Key\" (onclick)=\"postEvents(l.Url,l.Key)\"><span [ngClass]=\"'icon '+l.Icon\" aria-hidden=\"true\"></span>{{l.LinkText}}</a></div><div *ngIf=\"result.ShowFooterText === 'true' || result.ShowFooterLink === 'true'\"><div class=\"panel-footer\"><div *ngIf=\"result.ShowFooterText === 'true'\" class=\"footertext\">{{result.Footer}}</div><div *ngIf=\"result.ShowFooterLink === 'true'\" class=\"footerlink\"><a href=\"{{result.FooterLink}}\">{{result.FooterLinkText}}</a></div></div></div></div></div>",
            providers: [quicklinks_service_1.QuickLinksService, app_service_1.AppService]
        }),
        __metadata("design:paramtypes", [quicklinks_service_1.QuickLinksService, app_service_1.AppService, core_1.ElementRef])
    ], QuickLinks);
    return QuickLinks;
}());
exports.QuickLinks = QuickLinks;

//# sourceMappingURL=quicklinks.js.map
