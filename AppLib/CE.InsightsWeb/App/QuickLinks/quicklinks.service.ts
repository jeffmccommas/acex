﻿import "rxjs/add/operator/map";
import { Injectable, Input } from "@angular/core"
import { Http } from "@angular/http";

@Injectable()
export class QuickLinksService {
    @Input() res: Object;
    @Input() fromUnitTest: Boolean;
    http: Http;
    constructor(http: Http) {
        this.http = http;
    }

    getQuickLinks(tabKey, params) {
        return this.http.get(`./QuickLinks/Get?tabKey=${tabKey}&parameters=${window.btoa(params)}`).map(res => res.json());
    }
    
}

