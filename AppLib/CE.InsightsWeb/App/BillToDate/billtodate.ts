﻿import { Component, ElementRef, OnInit } from "@angular/core";
import { BillToDateService } from "./billtodate.service";
import { AppService, IAppParams } from "../app.service";

@Component({
    selector: 'billtodate',
    templateUrl: 'App/BillToDate/billtodate.html',
    providers: [BillToDateService, AppService]
})
export class BillToDate implements OnInit {
    static fromUnitTest = "unittest";
    elementRef: ElementRef;
    result: any;
    error: Object;
    enableLoader: Boolean;
    errorOccurred: Boolean;
    errorMessage: string;
    billtodateService: BillToDateService;
    appService: AppService;
    params: Array<IAppParams>;
    parameters: string;
    tabKey: string;
    fromUTest: Boolean;
    setupClicks = true;
    nextBill = true;
    lastBill: Boolean;

    constructor(billtodateService: BillToDateService, appService: AppService, elementRef: ElementRef) {
        this.enableLoader = true;
        this.elementRef = elementRef;
        this.params = appService.getParams();
        this.parameters = this.params[0].parameters;
        this.tabKey = this.params[0].tabKey;
        this.errorMessage = this.params[0].errorMessage;
        this.billtodateService = billtodateService;
        this.appService = appService;

        if (this.billtodateService.res !== undefined) {
            this.result = billtodateService.res;
            this.fromUTest = this.billtodateService.fromUnitTest;
        }
    }

    ngOnInit() {
        if (this.billtodateService.getBillToDate("", "").toString() !== BillToDate.fromUnitTest) {
            this.loadBillToDate();
        }
    }

    loadBillToDate() {
        this.result = {};

        this.billtodateService.getBillToDate(this.tabKey, this.parameters)
            .subscribe(res => this.result = res,
                err => this.appService.logError(this, err),
                () => this.disableLoader()
            );

    }

    disableLoader() {

        this.enableLoader = false;

        if (this.result.NoBillToDate === true || this.result.BillSummary.NoBills === true) {
            this.setupClicks = false;
        }

        if (this.result === null || this.result === undefined || this.result.Commodities == null) {
            this.errorOccurred = true;
            this.result.ShowTitle = "false";
            this.result.ShowIntro = "false";
        } else if (this.result.NoBillToDate && this.result.BillSummary.NoBills) {
            this.result.ShowTitle = "false";
            this.result.ShowIntro = "false";
            this.errorOccurred = true;
        } else {
            this.result.ShowTitle = "true";
            this.result.ShowIntro = "true";
            this.errorOccurred = false;
        }

        if (this.setupClicks) {
            this.nextBill = true;
        }

    }

    getBill(bill) {

        if (bill === "lastbill") {
            this.lastBill = true;
            this.nextBill = false;
        } else {
            this.lastBill = false;
            this.nextBill = true;
        }
    }

}