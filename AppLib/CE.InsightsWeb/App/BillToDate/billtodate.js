"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var billtodate_service_1 = require("./billtodate.service");
var app_service_1 = require("../app.service");
var BillToDate = (function () {
    function BillToDate(billtodateService, appService, elementRef) {
        this.setupClicks = true;
        this.nextBill = true;
        this.enableLoader = true;
        this.elementRef = elementRef;
        this.params = appService.getParams();
        this.parameters = this.params[0].parameters;
        this.tabKey = this.params[0].tabKey;
        this.errorMessage = this.params[0].errorMessage;
        this.billtodateService = billtodateService;
        this.appService = appService;
        if (this.billtodateService.res !== undefined) {
            this.result = billtodateService.res;
            this.fromUTest = this.billtodateService.fromUnitTest;
        }
    }
    BillToDate_1 = BillToDate;
    BillToDate.prototype.ngOnInit = function () {
        if (this.billtodateService.getBillToDate("", "").toString() !== BillToDate_1.fromUnitTest) {
            this.loadBillToDate();
        }
    };
    BillToDate.prototype.loadBillToDate = function () {
        var _this = this;
        this.result = {};
        this.billtodateService.getBillToDate(this.tabKey, this.parameters)
            .subscribe(function (res) { return _this.result = res; }, function (err) { return _this.appService.logError(_this, err); }, function () { return _this.disableLoader(); });
    };
    BillToDate.prototype.disableLoader = function () {
        this.enableLoader = false;
        if (this.result.NoBillToDate === true || this.result.BillSummary.NoBills === true) {
            this.setupClicks = false;
        }
        if (this.result === null || this.result === undefined || this.result.Commodities == null) {
            this.errorOccurred = true;
            this.result.ShowTitle = "false";
            this.result.ShowIntro = "false";
        }
        else if (this.result.NoBillToDate && this.result.BillSummary.NoBills) {
            this.result.ShowTitle = "false";
            this.result.ShowIntro = "false";
            this.errorOccurred = true;
        }
        else {
            this.result.ShowTitle = "true";
            this.result.ShowIntro = "true";
            this.errorOccurred = false;
        }
        if (this.setupClicks) {
            this.nextBill = true;
        }
    };
    BillToDate.prototype.getBill = function (bill) {
        if (bill === "lastbill") {
            this.lastBill = true;
            this.nextBill = false;
        }
        else {
            this.lastBill = false;
            this.nextBill = true;
        }
    };
    BillToDate.fromUnitTest = "unittest";
    BillToDate = BillToDate_1 = __decorate([
        core_1.Component({
            selector: 'billtodate',
            template: "<Loader *ngIf=\"enableLoader\"></Loader><div *ngIf=\"!enableLoader\"><div *ngIf=\"errorOccurred\"><div class=\"panel panel-default\"><div class=\"alert alert-danger\"><span class=\"glyphicon glyphicon-exclamation-sign xs-mr-10\" aria-hidden=\"true\"></span><strong>{{errorMessage}}</strong> <span class=\"sr-only\">Error:</span></div></div></div><div id=\"iws_btd_wrapper\" *ngIf=\"!errorOccurred\" class=\"panel panel-default panel-bill-summary\"><div *ngIf=\"result.ShowTitle == 'true' && result.Title != ''\" class=\"panel-heading\"><h1 id=\"iws_bs_title\" class=\"panel-title\" role=\"presentation\">{{result.Title}}</h1><div *ngIf=\"result.ShowIntro == 'true' && result.ShowIntro != ''\" id=\"iws_btd_intro_text\" class=\"intro-text xs-mb-0\"><p>{{result.IntroText}}</p></div></div><div class=\"panel-body xs-pt-0\"><div class=\"row\"><div role=\"tabpanel\" class=\"full-width-tabs\"><ul class=\"nav nav-tabs\" role=\"tablist\"><li *ngIf=\"result.NoBillToDate == false\" id=\"iws_btd_li_next_bill\" class=\"nav-tab-title\" [class.fullwidth]=\"result.BillSummary.NoBills\" [class.active]=\"nextBill || result.BillSummary.NoBills\" role=\"presentation\"><a id=\"iws_btd_next_bill\" [attr.href]=\"setupClicks ? '#' : null\" (click)=\"getBill('nextbill')\" data-toggle=\"tab\" role=\"tab\">{{result.BTDTitle}}<span class=\"sr-only\">Next Bill</span></a></li><li *ngIf=\"result.BillSummary.NoBills == false\" id=\"iws_btd_li_last_bill\" class=\"nav-tab-title\" [class.fullwidth]=\"result.NoBillToDate\" [class.active]=\"lastBill || result.NoBillToDate\" role=\"presentation\"><a id=\"iws_btd_last_bill\" [attr.href]=\"setupClicks ? '#' : null\" (click)=\"getBill('lastbill')\" data-toggle=\"tab\" role=\"tab\">{{result.BillStatementTitle}}<span class=\"sr-only\">Last Bill</span></a></li></ul></div></div><ul *ngIf=\"result.NoBillToDate == false && nextBill == true\" id=\"iws_btd_ul_next_bill\" class=\"list-group list-bill-to-date\" role=\"list\"><li *ngIf=\"result.ShowBTDBillPeriod == 'true'\" class=\"list-group-item row\" role=\"listitem\"><div class=\"col-xs-6 xs-pl-0\">{{result.BillPeriod}}</div><div class=\"col-xs-6 text-right list-value xs-pr-0\">{{result.NumberOfDays}} {{result.NumberOfDaysText}}</div></li><li *ngIf=\"result.ShowBTDElectricUseToDate == 'true' && result.Commodities.indexOf(('electric')) !== -1 && result.ElectricUseToDate.replace(',', '') > 0\" class=\"list-group-item row\" role=\"listitem\"><div class=\"col-xs-6 xs-pl-0\">{{result.ElectricUseToDateText}}</div><div class=\"col-xs-6 text-right list-value xs-pr-0\">{{result.ElectricUseToDate}} {{result.ElectricUOM}}</div></li><li *ngIf=\"result.ShowBTDGasUseToDate == 'true'  && result.Commodities.indexOf(('gas')) !== -1 && result.GasUseToDate.replace(',', '') > 0\" class=\"list-group-item row\" role=\"listitem\"><div class=\"col-xs-6 xs-pl-0\">{{result.GasUseToDateText}}</div><div class=\"col-xs-6 text-right list-value xs-pr-0\">{{result.GasUseToDate}} {{result.GasUOM}}</div></li><li *ngIf=\"result.ShowBTDWaterUseToDate == 'true'  && result.Commodities.indexOf(('water')) !== -1 && result.WaterUseToDate.replace(',', '') > 0\" class=\"list-group-item row\" role=\"listitem\"><div class=\"col-xs-6 xs-pl-0\">{{result.WaterUseToDateText}}</div><div class=\"col-xs-6 text-right list-value xs-pr-0\">{{result.WaterUseToDate}} {{result.WaterUOM}}</div></li><li *ngIf=\"result.ShowBTDCostToDate == 'true'\" class=\"list-group-item row\" role=\"listitem\"><div class=\"col-xs-6 xs-pl-0\" id=\"projected_bill\"><strong>{{result.CostToDateText}}</strong></div><div class=\"col-xs-6 text-right list-value xs-pr-0\"><strong>{{result.CostToDate}}</strong></div></li><li *ngIf=\"result.ShowBTDAverageDailyCost == 'true'\" class=\"list-group-item row\" role=\"listitem\"><div class=\"col-xs-7 xs-pl-0\" id=\"projected_bill\"><strong>{{result.AvgDailyCostText}}</strong></div><div class=\"col-xs-5 text-right list-value xs-pr-0 xs-pl-0\"><strong>{{result.AvgDailyCost}}</strong></div></li><li *ngIf=\"result.ShowBTDProjectedCost == 'true'\" class=\"list-group-item row\" role=\"listitem\"><div class=\"col-xs-6 xs-pl-0\" id=\"projected_bill\"><strong>{{result.ProjectedCostText}}</strong></div><div class=\"col-xs-6 text-right list-value xs-pr-0\"><strong>{{result.ProjectedCost}}</strong></div></li></ul><ul *ngIf=\"lastBill == true || result.NoBillToDate == true\" id=\"iws_btd_ul_last_bill\" class=\"list-group list-bill-to-date\" role=\"list\"><li *ngIf=\"result.BillSummary.TotalElectricityUsed > 0\" class=\"list-group-item row\" role=\"listitem\"><div class=\"col-xs-6 xs-pl-0\" id=\"days\">{{result.BillSummary.TotalElectricUse}}</div><div class=\"col-xs-6 text-right list-value xs-pr-0\" aria-labelledby=\"days\">{{result.BillSummary.TotalElectricityUsed}} {{result.BillSummary.ElectricUOM}}</div></li><li *ngIf=\"result.BillSummary.TotalGasUsed > 0\" class=\"list-group-item row\" role=\"listitem\"><div class=\"col-xs-6 xs-pl-0\">{{result.BillSummary.TotalGasUse}}</div><div class=\"col-xs-6 text-right list-value xs-pr-0\">{{result.BillSummary.TotalGasUsed}} {{result.BillSummary.GasUOM}}</div></li><li *ngIf=\"result.BillSummary.TotalWaterUsed > 0\" class=\"list-group-item row\" role=\"listitem\"><div class=\"col-xs-6 xs-pl-0\">{{result.BillSummary.TotalWaterUse}}</div><div class=\"col-xs-6 text-right list-value xs-pr-0\">{{result.BillSummary.TotalWaterUsed}} {{result.BillSummary.WaterUOM}}</div></li><li class=\"list-group-item row\" role=\"listitem\"><div class=\"col-xs-7 xs-pl-0\" id=\"days\">{{result.BillSummary.AvgDailyCostText}}</div><div class=\"col-xs-5 text-right list-value xs-pr-0 xs-pl-0\">{{result.BillSummary.AvgDailyCost}}</div></li><li class=\"list-group-item row\" role=\"listitem\"><div class=\"col-xs-6 xs-pl-0\" id=\"days\">{{result.BillSummary.NumberOfDaysText}}</div><div class=\"col-xs-6 text-right list-value xs-pr-0\">{{result.BillSummary.NumberOfDays}}</div></li><li class=\"list-group-item row\" role=\"listitem\"><div class=\"col-xs-6 xs-pl-0\">{{result.BillSummary.AvgTempText}}</div><div class=\"col-xs-6 text-right list-value xs-pr-0\">{{result.BillSummary.AvgTemp}}&deg;{{result.BillSummary.TempFormat}}</div></li><li class=\"list-group-item row\" role=\"listitem\"><div class=\"col-xs-6 xs-pl-0\"><strong>{{result.BillSummary.AmountDueText}}</strong></div><div class=\"col-xs-6 text-right list-value xs-pr-0\"><strong>{{result.BillSummary.AmountDue}}</strong></div></li></ul></div><div class=\"hideEmptyCont\"><div *ngIf=\"result.ShowFooterText === 'true' || result.ShowFooterLink === 'true'\" class=\"panel-footer\"><div *ngIf=\"result.ShowFooterText === 'true'\" id=\"iws_btd_footertext\" class=\"footertext\">{{result.Footer}}</div><div *ngIf=\"result.ShowFooterLink === 'true'\" id=\"iws_btd_footerlink\" class=\"footerlink\"><a href=\"{{result.FooterLink}}\">{{result.FooterLinkText}}</a></div></div></div></div></div>",
            providers: [billtodate_service_1.BillToDateService, app_service_1.AppService]
        }),
        __metadata("design:paramtypes", [billtodate_service_1.BillToDateService, app_service_1.AppService, core_1.ElementRef])
    ], BillToDate);
    return BillToDate;
    var BillToDate_1;
}());
exports.BillToDate = BillToDate;

//# sourceMappingURL=billtodate.js.map
