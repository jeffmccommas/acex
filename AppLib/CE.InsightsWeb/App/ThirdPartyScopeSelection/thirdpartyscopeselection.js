"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var thirdpartyscopeselection_service_1 = require("./thirdpartyscopeselection.service");
var app_service_1 = require("../app.service");
var ThirdPartyScopeSelection = (function () {
    function ThirdPartyScopeSelection(thirdpartyscopeselectionService, appService, elementRef) {
        this.enableLoader = true;
        this.elementRef = elementRef;
        this.thirdpartyscopeselectionService = thirdpartyscopeselectionService;
        this.appService = appService;
        this.fromUTest = false;
        if (this.thirdpartyscopeselectionService.res !== undefined) {
            this.result = thirdpartyscopeselectionService.res;
            this.fromUTest = this.thirdpartyscopeselectionService.fromUnitTest;
        }
    }
    ThirdPartyScopeSelection.prototype.ngOnInit = function () {
        this.loadThirdPartyScopeSelection();
    };
    ThirdPartyScopeSelection.prototype.loadThirdPartyScopeSelection = function () {
        var _this = this;
        this.result = {};
        this.thirdpartyscopeselectionService.GetThirdPartyScopeSelection()
            .subscribe(function (res) { return _this.result = res; }, function (err) { return _this.appService.logError(_this, err); }, function () { return _this.disableLoader(); });
        this.enableLoader = false;
    };
    ThirdPartyScopeSelection.prototype.disableLoader = function () {
        var _this = this;
        if (!this.fromUTest) {
            setTimeout(function () { return _this.appService.setHtml(_this.elementRef); }, 1);
        }
        this.enableLoader = false;
    };
    ThirdPartyScopeSelection = __decorate([
        core_1.Component({
            selector: 'thirdpartyscopeselection',
            template: "<h1 class=\"hidden\">Third Party Scope Selection Page</h1><p class=\"hidden\">The Data Custodian (Aclara) redirects to this Third Party Application (simulation only).</p><p class=\"hidden\">The Retail Customer would normally set up an account with the Third Party Application and select the scopes to share with the Third Party Application.</p><p class=\"hidden\">After scope selection the Third Party app would redirect to the Data Custodian oAuth page to request an auth code.</p><div class=\"container\"><div class=\"row\"><div class=\"gbc__scope-login text-center xs-pt-20 xs-pb-10\"><img class=\"img-responsive img-rounded\" src=\"{{result.LogoUri}}\"></div></div><div class=\"row\"><div class=\"panel panel-default gbc__scope-login\"><div class=\"panel-heading\"><h1 class=\"panel-title\">Register</h1></div><div class=\"panel-body\"><form role=\"form\" class=\"col-sm-12 form-horizontal\"><div class=\"form-group\"><label class=\"col-sm-3\" for=\"inputName\">Name</label><div class=\"col-sm-9\"><input class=\"form-control gbc_element\" id=\"inputName\" placeholder=\"Your Name\"></div></div><div class=\"form-group\"><label class=\"col-sm-3\" for=\"inputEmail1\">Email</label><div class=\"col-sm-9\"><input type=\"email\" class=\"form-control gbc_element\" id=\"inputEmail1\" placeholder=\"Email\"></div></div><div class=\"form-group\"><label class=\"col-sm-3\" for=\"street-address1\">Address 1</label><div class=\"col-sm-9\"><input class=\"form-control gbc_element\" id=\"street-address1\" placeholder=\"Street address, P.O. box, company name, etc.\"></div></div><div class=\"form-group\"><label class=\"col-sm-3\" for=\"street-address2\">Address 2</label><div class=\"col-sm-9\"><input class=\"form-control gbc_element\" id=\"street-address2\" placeholder=\"Apartment, suite, unit, building, floor, etc.\"></div></div><div class=\"form-group\"><label class=\"col-sm-3\" for=\"inputCity\">City</label><div class=\"col-sm-9\"><input class=\"form-control gbc_element\" id=\"inputCity\" placeholder=\"City\"></div></div><div class=\"form-group\"><label class=\"col-sm-3\" for=\"state\">State</label><div class=\"col-sm-5\"><select class=\"form-control\" id=\"state\" name=\"state\"><option value=\"\">Choose State</option><option value=\"AK\">Alaska</option><option value=\"AL\">Alabama</option><option value=\"AR\">Arkansas</option><option value=\"AZ\">Arizona</option><option value=\"CA\">California</option><option value=\"CO\">Colorado</option><option value=\"CT\">Connecticut</option><option value=\"DC\">District of Columbia</option><option value=\"DE\">Delaware</option><option value=\"FL\">Florida</option><option value=\"GA\">Georgia</option><option value=\"HI\">Hawaii</option><option value=\"IA\">Iowa</option><option value=\"ID\">Idaho</option><option value=\"IL\">Illinois</option><option value=\"IN\">Indiana</option><option value=\"KS\">Kansas</option><option value=\"KY\">Kentucky</option><option value=\"LA\">Louisiana</option><option value=\"MA\">Massachusetts</option><option value=\"MD\">Maryland</option><option value=\"ME\">Maine</option><option value=\"MI\">Michigan</option><option value=\"MN\">Minnesota</option><option value=\"MO\">Missouri</option><option value=\"MS\">Mississippi</option><option value=\"MT\">Montana</option><option value=\"NC\">North Carolina</option><option value=\"ND\">North Dakota</option><option value=\"NE\">Nebraska</option><option value=\"NH\">New Hampshire</option><option value=\"NJ\">New Jersey</option><option value=\"NM\">New Mexico</option><option value=\"NV\">Nevada</option><option value=\"NY\">New York</option><option value=\"OH\">Ohio</option><option value=\"OK\">Oklahoma</option><option value=\"OR\">Oregon</option><option value=\"PA\">Pennsylvania</option><option value=\"PR\">Puerto Rico</option><option value=\"RI\">Rhode Island</option><option value=\"SC\">South Carolina</option><option value=\"SD\">South Dakota</option><option value=\"TN\">Tennessee</option><option value=\"TX\">Texas</option><option value=\"UT\">Utah</option><option value=\"VA\">Virginia</option><option value=\"VT\">Vermont</option><option value=\"WA\">Washington</option><option value=\"WI\">Wisconsin</option><option value=\"WV\">West Virginia</option><option value=\"WY\">Wyoming</option></select></div><label class=\"col-sm-1\" for=\"zip\">Zip</label><div class=\"col-sm-3\"><input class=\"form-control gbc_element\" id=\"zip\" older=\"Zipcode\"></div></div><div class=\"form-group\"><label class=\"col-sm-3\" for=\"username\">Username</label><div class=\"col-sm-9\"><input class=\"form-control gbc_element\" id=\"username\" placeholder=\"Username\"></div></div><div class=\"form-group\"><label class=\"col-sm-3\" for=\"inputPassword\">Password</label><div class=\"col-sm-9\"><input type=\"password\" class=\"form-control gbc_element\" id=\"inputPassword\" placeholder=\"Password\"></div></div><div class=\"form-group\"><div class=\"col-sm-12\"><a [href]=\"result.AuthorizeUri\" class=\"btn btn-default pull-right\">Sign Up</a></div></div></form></div></div></div></div>",
            providers: [thirdpartyscopeselection_service_1.ThirdPartyScopeSelectionService, app_service_1.AppService]
        }),
        __metadata("design:paramtypes", [thirdpartyscopeselection_service_1.ThirdPartyScopeSelectionService, app_service_1.AppService, core_1.ElementRef])
    ], ThirdPartyScopeSelection);
    return ThirdPartyScopeSelection;
}());
exports.ThirdPartyScopeSelection = ThirdPartyScopeSelection;

//# sourceMappingURL=thirdpartyscopeselection.js.map
