﻿import { Component, ElementRef, OnInit } from "@angular/core";
import { ThirdPartyScopeSelectionService } from "./thirdpartyscopeselection.service";
import { AppService, IAppParams } from "../app.service";

@Component({
    selector: 'thirdpartyscopeselection',
    templateUrl: 'App/ThirdPartyScopeSelection/thirdpartyscopeselection.html',
    providers: [ThirdPartyScopeSelectionService, AppService]
})
export class ThirdPartyScopeSelection implements OnInit {
    elementRef: ElementRef;
    result: any;
    error: Object;
    postEvent: Object;
    enableLoader: Boolean;  
    errorOccurred: Boolean;
    errorMessage: string;
    thirdpartyscopeselectionService: ThirdPartyScopeSelectionService;
    appService: AppService; 
    params: Array<IAppParams>;
    parameters: string;
    tabKey: string; 
    fromUTest: Boolean;

    constructor(thirdpartyscopeselectionService: ThirdPartyScopeSelectionService, appService: AppService, elementRef: ElementRef) {
        this.enableLoader = true; 
        this.elementRef = elementRef;
        //this.params = appService.getParams();
        //this.parameters = this.params[0].parameters;
        //this.tabKey = this.params[0].tabKey;
        //this.errorMessage = this.params[0].errorMessage;
        this.thirdpartyscopeselectionService = thirdpartyscopeselectionService;
        this.appService = appService;
        this.fromUTest = false;

        if (this.thirdpartyscopeselectionService.res !== undefined) {
            this.result = thirdpartyscopeselectionService.res;
            this.fromUTest = this.thirdpartyscopeselectionService.fromUnitTest; 
        }

    }

    ngOnInit() {
        this.loadThirdPartyScopeSelection(); 
    }

    loadThirdPartyScopeSelection() {
        this.result = {};
        this.thirdpartyscopeselectionService.GetThirdPartyScopeSelection()
            .subscribe(res => this.result = res,
            err => this.appService.logError(this, err),
            () => this.disableLoader()
            );

        this.enableLoader = false;
    }

    disableLoader() {
        if (!this.fromUTest) {
            setTimeout(() => this.appService.setHtml(this.elementRef), 1);
        }
        this.enableLoader = false;
    }


}