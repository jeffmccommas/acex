﻿import "rxjs/add/operator/map";
import { Injectable, Input } from "@angular/core"
import { Http } from "@angular/http";

@Injectable()
export class ThirdPartyScopeSelectionService {
    @Input() res: Object;
    @Input() fromUnitTest: Boolean;
    http: Http;
    constructor(http: Http) {
        this.http = http;
    }

    GetThirdPartyScopeSelection() {
        return this.http.get(`./ThirdPartyScopeSelection/Get`).map(res => res.json());
    }

}

