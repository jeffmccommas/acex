"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var consumption_service_1 = require("./consumption.service");
var app_service_1 = require("../app.service");
var Consumption = (function () {
    function Consumption(consumptionService, appService, elementRef, renderer) {
        this.enableLoader = true;
        this.elementRef = elementRef;
        this.renderer = renderer;
        this.params = appService.getParams();
        this.parameters = this.params[0].parameters;
        this.tabKey = this.params[0].tabKey;
        this.errorMessage = this.params[0].errorMessage;
        this.consumptionService = consumptionService;
        this.appService = appService;
        this.showChart = true;
        this.paging = false;
        this.drilling = false;
        this.commodityCount = 0;
        this.dl_res = "day";
        this.dl_highlight = false;
        this.startDate = "";
        this.endDate = "";
        this.resolution = "";
        this.prevResolution = "";
        this.selectedCommodity = "";
        this.setListener = true;
        this.current_page = 0;
        if (this.consumptionService.res !== undefined) {
            this.result = consumptionService.res;
            this.fromUTest = this.consumptionService.fromUnitTest;
        }
    }
    Consumption.prototype.ngOnInit = function () {
        if (this.dl_res !== null && this.dl_res !== "") {
            this.resolution = this.dl_res;
            this.dl_highlight = true;
        }
        if (!this.fromUTest) {
            this.loadconsumption();
        }
    };
    Consumption.prototype.loadconsumption = function () {
        var _this = this;
        this.result = {};
        this.offset = "";
        if (!this.fromUTest) {
            this.consumptionService.getConsumption(this.tabKey, this.parameters, this.startDate, this.endDate, this.resolution, this.prevResolution, this.selectedCommodity, this.offset, this.paging, this.drilling)
                .subscribe(function (res) { return _this.result = res; }, function (err) { return _this.appService.logError(_this, err); }, function () { return _this.disableLoader(); });
        }
    };
    Consumption.prototype.disableLoader = function () {
        var _this = this;
        if (!this.fromUTest) {
            setTimeout(function () { return _this.appService.setHtml(_this.elementRef); }, 1);
            setTimeout(function () { return _this.appService.setIframeExternalClass(_this.elementRef); }, 0);
        }
        this.enableLoader = false;
        if (this.result.NoConsumption === true) {
            this.noConsumption = true;
        }
        else {
            if (this.result.CommoditiesText !== null) {
                this.checkCommodities();
                this.prepareResolutionData();
                this.displayRange();
                this.table = {
                    Rows: [],
                    Headers: []
                };
                setTimeout(function () { return _this.drawChart(_this.selectedCommodity); });
            }
        }
    };
    Consumption.prototype.checkCommodities = function () {
        if (this.selectedCommodity === "") {
            this.selectedCommodity = this.result.Commodities.split(",")[0];
        }
        var comsText = this.result.CommoditiesText.split(",");
        var comsVal = this.result.Commodities.split(",");
        var commodityfound = false;
        this.result.ShowCommodities = true;
        this.commodityCount = 0;
        if (comsVal.length > 1) {
            for (var i = 0; i < comsVal.length; i++) {
                if (comsVal[i] === "gas") {
                    this.result.ShowGas = true;
                    this.result.GasLabel = comsText[i];
                    this.commodityCount++;
                    if (commodityfound === false) {
                        commodityfound = true;
                        this.selectedCommodity = "gas";
                    }
                }
                if (comsVal[i] === "electric") {
                    this.result.ShowElectric = true;
                    this.result.ElectricLabel = comsText[i];
                    this.commodityCount++;
                    if (commodityfound === false) {
                        commodityfound = true;
                        this.selectedCommodity = "electric";
                    }
                }
                if (comsVal[i] === "water") {
                    this.result.ShowWater = true;
                    this.result.WaterLabel = comsText[i];
                    this.commodityCount++;
                    if (commodityfound === false) {
                        commodityfound = true;
                        this.selectedCommodity = "water";
                    }
                }
            }
        }
        if (this.commodityCount < 2) {
            this.result.ShowCommodities = false;
        }
    };
    Consumption.prototype.prepareResolutionData = function () {
        if (this.result.ResolutionKeys === null || this.result.NoConsumption === true) {
            if (this.dl_res === null || this.dl_res === "") {
                this.resolution = "month";
            }
            this.result.ShowResolutionsDropdown = "false";
        }
        else {
            var resText = this.result.ResolutionKeysText.split(",");
            var resVal = this.result.ResolutionKeys.split(",");
            var resAvail = this.result.ResolutionsAvailable.split(",");
            this.max_res = resAvail[resAvail.length - 1];
            var resolutions = [];
            if (this.resolution === "") {
                this.resolution = resAvail[0];
            }
            if (resAvail.length > 1) {
                for (var i = 0; i < resVal.length; i++) {
                    if (this.result.ResolutionsAvailable.indexOf(resVal[i]) > -1) {
                        resolutions.push({
                            Id: i,
                            Index: i,
                            Text: resText[i],
                            Value: resVal[i]
                        });
                        if (resVal[i] === this.resolution) {
                            this.result.SelectedResolutionValue = i;
                            this.result.SelectedResolutionText = resText[i];
                        }
                    }
                }
                if (resolutions.length > 1) {
                    this.result.ShowResolutionsDropdown = "true";
                    this.result.Resolutions = resolutions;
                }
                switch (this.resolution) {
                    case "month":
                        this.data_points = this.result.MonthCount;
                        break;
                    case "day":
                        this.data_points = this.result.DayCount;
                        break;
                    case "hour":
                        this.data_points = this.result.HourCount;
                        break;
                    case "halfhour":
                        this.data_points = this.result.ThirtyMinCount;
                        break;
                    case "fifteen":
                        this.data_points = this.result.FifteenMinCount;
                        break;
                    case "five":
                        this.data_points = this.result.FiveMinCount;
                        break;
                }
            }
        }
    };
    Consumption.prototype.resolutionSelected = function (Id, Val, Text) {
        this.drilling = false;
        if (this.result.SelectedResolutionValue > Id) {
            this.startDate = null;
            this.endDate = null;
        }
        if (!this.fromUTest && this.elementRef.nativeElement.querySelector("[id='iws_cn_res-" + Id + "']").getAttribute("data-drilling") !== null) {
            this.drilling = true;
        }
        if (!this.fromUTest && this.elementRef.nativeElement.querySelector("[id='iws_cn_res-" + Id + "']").getAttribute("data-enddate") !== null) {
            this.endDate = this.elementRef.nativeElement.querySelector("[id='iws_cn_res-" + Id + "']").getAttribute("data-enddate");
        }
        this.resolution = Val;
        this.current_page = 0;
        this.paging = false;
        switch (this.resolution) {
            case "month":
                this.data_points = this.result.MonthCount;
                break;
            case "day":
                this.data_points = this.result.DayCount;
                break;
            case "hour":
                this.data_points = this.result.HourCount;
                break;
            case "halfhour":
                this.data_points = this.result.ThirtyMinCount;
                break;
            case "fifteen":
                this.data_points = this.result.FifteenMinCount;
                break;
            case "five":
                this.data_points = this.result.FiveMinCount;
                break;
        }
        this.offset = this.data_points * -1;
        this.result.SelectedResolutionText = Text;
        this.result.SelectedResolutionValue = Id;
        this.getConsumption(this.selectedCommodity);
        this.displayRange();
    };
    Consumption.prototype.drawChart = function (commodity) {
        var _this = this;
        this.selectedCommodity = commodity;
        var options;
        if (this.table !== undefined) {
            this.table.length = 0;
        }
        if (this.result.Resolutions != undefined && this.result.Resolutions.length > 1) {
            for (var j = 0; j < this.result.Resolutions.length; j++) {
                var event = this.renderer.listen(this.elementRef.nativeElement.querySelector("[id='iws_cn_res-" + this.result.Resolutions[j].Id + "']"), 'click', function (event) {
                    event.preventDefault();
                    _this.resolutionSelected(event.currentTarget.id.split("-")[1], event.currentTarget.title, event.currentTarget.text);
                });
                if (!this.setListener) {
                    event();
                    var element = this.elementRef.nativeElement
                        .querySelector("[id='iws_cn_res-" + this.result.Resolutions[j].Id + "']");
                    if (element.getAttribute("data-drilling") != null) {
                        element.removeAttribute("data-drilling");
                    }
                    if (element.getAttribute("data-enddate") != null) {
                        element.removeAttribute("data-enddate");
                    }
                }
            }
        }
        if (!this.fromUTest && !this.result.NoConsumption) {
            var params = { current_page: this.current_page, paging: this.paging, drilling: this.drilling, startDate: this.startDate, endDate: this.endDate, res_selected: this.resolution, max_res: this.max_res };
            this.options = this.appService.buildConsumptionChart(this.result, options, params, this.elementRef);
            this.drawTable();
        }
        this.highLightElement();
    };
    Consumption.prototype.displayChart = function () {
        var _this = this;
        this.showChart = !this.showChart;
        if (!this.fromUTest) {
            setTimeout(function () { return _this.drawChart(_this.selectedCommodity); });
        }
    };
    Consumption.prototype.highLightElement = function () {
        if (this.dl_highlight === true) {
            this.elementRef.nativeElement.querySelector(".deep-link-highlight").focus();
            this.appService.consumptionHighLightElement(this.elementRef);
        }
    };
    Consumption.prototype.drawTable = function () {
        var series = this.options.series;
        this.table = {
            Rows: [],
            Headers: []
        };
        if (this.resolution === "month") {
            this.table.Headers.push({ Label: this.result.TableViewColumn1MonthLabel });
        }
        else {
            this.table.Headers.push({ Label: this.result.TableViewColumn1DateLabel });
        }
        for (var h = 0; h < series.length; h++) {
            this.table.Headers.push({ Label: series[h].name });
        }
        for (var d = 0; d < series[0].data.length; d++) {
            var row = { Cells: [] };
            var cells = [];
            var header;
            if (this.resolution === "month") {
                header = this.result.TableViewColumn1MonthLabel;
            }
            else {
                header = this.result.TableViewColumn1DateLabel;
            }
            cells.push({ Header: header, Value: series[0].data[d].tooltip_date });
            for (var s = 0; s < series.length; s++) {
                cells.push({ Header: series[s].name, Value: series[s].data[d].tooltip_value_formatted });
            }
            row.Cells = cells;
            this.table.Rows.push(row);
        }
    };
    Consumption.prototype.displayRange = function () {
        this.startDate = this.result.StartDate;
        this.endDate = this.result.EndDate;
    };
    Consumption.prototype.pageForward = function () {
        this.offset = this.data_points;
        this.paging = true;
        this.current_page += 1;
        this.displayRange();
        this.getConsumption(this.selectedCommodity);
    };
    Consumption.prototype.pageBack = function () {
        this.offset = this.data_points * -1;
        this.paging = true;
        this.current_page -= 1;
        this.displayRange();
        this.getConsumption(this.selectedCommodity);
    };
    Consumption.prototype.getConsumption = function (commodity) {
        var _this = this;
        this.selectedCommodity = commodity;
        this.commodities = [this.result.CommoditiesText, this.result.Commodities];
        this.resolutions = [this.result.ShowResolutionsDropdown, this.result.Resolutions, this.result.SelectedResolutionValue, this.result.SelectedResolutionText];
        if (!this.fromUTest) {
            this.consumptionService.getConsumption(this.tabKey, this.parameters, this.startDate, this.endDate, this.resolution, this.prevResolution, this.selectedCommodity, this.offset, this.paging, this.drilling)
                .subscribe(function (res) { return _this.result = res; }, function (err) { return _this.appService.logError(_this, err); }, function () { return _this.callback(_this.selectedCommodity); });
        }
    };
    Consumption.prototype.callback = function (commodity) {
        var _this = this;
        this.result.CommoditiesText = this.commodities[0];
        this.result.Commodities = this.commodities[1];
        if (this.result.CommoditiesText !== null) {
            if (!this.fromUTest) {
                this.checkCommodities();
            }
            this.selectedCommodity = commodity;
            if (!this.fromUTest) {
                this.prepareResolutionData();
            }
            this.result.ShowResolutionsDropdown = this.resolutions[0];
            this.result.Resolutions = this.resolutions[1];
            if (this.result.SelectedResolutionValue === undefined || this.result.SelectedResolutionValue === "") {
                this.result.SelectedResolutionValue = this.resolutions[2];
            }
            this.result.SelectedResolutionText = this.resolutions[3];
            this.displayRange();
            this.table = {
                Rows: [],
                Headers: []
            };
            this.setListener = false;
            if (!this.fromUTest) {
                setTimeout(function () { return _this.drawChart(_this.selectedCommodity); });
            }
        }
    };
    Consumption.prototype.commodityChange = function (commodity) {
        this.resolution = this.result.Resolutions[0].Value;
        this.result.SelectedResolutionText = this.result.Resolutions[0].Text;
        if (this.resolution === undefined) {
            this.resolution = "month";
        }
        this.result.SelectedResolutionValue = this.result.Resolutions[0].Id;
        this.startDate = "";
        this.endDate = "";
        this.current_page = 0;
        this.getConsumption(commodity);
    };
    Consumption.fromUnitTest = "unittest";
    Consumption = __decorate([
        core_1.Component({
            selector: 'consumption',
            template: "<Loader *ngIf=\"enableLoader\"></Loader><div *ngIf=\"!enableLoader\"><div *ngIf=\"errorOccurred\"><div class=\"panel panel-default\"><div class=\"alert alert-danger\"><span class=\"glyphicon glyphicon-exclamation-sign xs-mr-10\" aria-hidden=\"true\"></span><strong>{{errorMessage}}</strong> <span class=\"sr-only\">Error:</span></div></div></div><div *ngIf=\"!errorOccurred\"><div *ngIf=\"noConsumption == true && result.Commodities.indexOf(',') == -1 \" class=\"panel panel-default\"><div *ngIf=\"result.ShowTitle == 'true' && result.Title !== ''\" class=\"panel-heading\"><h1 class=\"panel-title\" role=\"presentation\">{{result.Title}}</h1></div><div class=\"alert alert-warning\"><span class=\"glyphicon glyphicon-alert xs-mr-10\"></span> <strong>{{result.NoConsumptionText}}</strong></div></div><div *ngIf=\"result.ShowTitle == 'true' || result.ShowIntro == 'true'\" class=\"panel-heading\"><h1 *ngIf=\"result.ShowTitle == 'true' && result.Title !== ''\" class=\"panel-title\" role=\"presentation\">{{result.Title}}</h1><div *ngIf=\"result.ShowIntro == 'true' && result.ShowIntro !== ''\" class=\"intro-text markdown\">{{result.IntroText}}</div></div><div *ngIf=\"noConsumption !== true\" class=\"panel panel-default\"><div class=\"panel-heading utility xs-pl-0 xs-pr-0\"><div class=\"panel-body utility xs-mr-0 xs-p-5\"><div class=\"col-sm-6 intervals text-xs-left xs-pl-0 xs-pt-10 ms-pt-0\"><div *ngIf=\"result.ShowResolutionsDropdown == 'true'\" class=\"monthsdayshours__intervals\"><div id=\"intervals\" class=\"dropdown monthsdayshours listbox\" aria-label=\"Choose an interval\" [style.display]=\"showChart ? 'block': 'none'\"><button aria-level=\"1\" class=\"btn-default btn dropdown-toggle deep-link-highlight\" type=\"button\" data-toggle=\"dropdown\" aria-expanded=\"false\"><p class=\"select\" style=\"display:inline\" val=\"result.SelectedResolutionValue\">{{result.SelectedResolutionText}}</p><span><img src=\"Content/Images/caret-darker.png\" height=\"6\" width=\"10\" alt=\"\"></span></button> <span class=\"listbox-label sr-only\">Choose an interval</span><ul class=\"dropdown-menu\" role=\"menu\"><li *ngFor=\"let r of result.Resolutions\" role=\"menuitem\"><a id=\"iws_cn_res-{{r.Id}}\" title=\"{{r.Value}}\" href=\"#\" onclick=\"return false;\">{{r.Text}}</a></li></ul></div></div><div *ngIf=\"resolution !== 'month'\" class=\"monthsdayshours__nextprev ms-mt-0\"><nav class=\"cell-inner\" aria-label=\"Primary\" [style.display]=\"showChart ? 'block': 'none'\"><ul class=\"pagination\"><li class=\"active prev\"><a class=\"xs-mr-5 xs-ml-10\" onclick=\"return false;\" (click)=\"pageBack()\" href=\"#\" tabindex=\"0\"><span aria-hidden=\"true\"><img alt=\"previous\" src=\"Content/Images/pagination-panel-left-on.png\" height=\"32\" width=\"32\"></span></a></li><li class=\"active next\"><a onclick=\"return false;\" (click)=\"pageForward()\" href=\"#\" tabindex=\"0\"><span aria-hidden=\"true\"><img alt=\"next\" src=\"Content/Images/pagination-panel-right-on.png\" height=\"32\" width=\"32\"></span></a></li></ul></nav></div><div class=\"monthsdayshours__date xs-mt-15 xs-mb-15 ms-ml-10\" style=\"display:inline-block\" aria-label=\"Date Range Interval\"><div>{{result.StartDateTitle}} - {{result.EndDateTitle}}</div></div></div><div class=\"utility-icons monthsdayshours__utility-icons col-sm-6 text-xs-left text-sm-right xs-pr-0 xs-pl-0\"><div class=\"pull-right-sm\"><div class=\"u-icon-focus\"><a *ngIf=\"result.ShowCommodities === true && result.ShowGas === true\" (click)=\"commodityChange('gas')\" href=\"#\" onclick=\"return false;\" aria-label=\"Gas Commodity\" class=\"btn btn-default xs-ml-5 icon__gas\" [ngClass]=\"selectedCommodity==='gas' ? 'icon_focus' : ''\" role=\"button\"><span class=\"icon icon-flame\" aria-hidden=\"true\"></span> <span *ngIf=\"result.ShowCommodityLabels === 'true'\" class=\"hidden-xs\">{{result.GasLabel}}</span> <span class=\"print-only\">{{result.GasLabel}}</span> </a><a *ngIf=\"result.ShowCommodities === true && result.ShowElectric === true\" (click)=\"commodityChange('electric')\" href=\"#\" onclick=\"return false;\" aria-label=\"Electric Commodity\" class=\"btn btn-default xs-ml-5 icon__electric\" [ngClass]=\"selectedCommodity==='electric' ? 'icon_focus' : ''\" role=\"button\"><span class=\"icon icon-bolt\" aria-hidden=\"true\"></span> <span *ngIf=\"result.ShowCommodityLabels === 'true'\" class=\"hidden-xs\">{{result.ElectricLabel}}</span> <span class=\"print-only\">{{result.ElectricLabel}}</span> </a><a *ngIf=\"result.ShowCommodities === true && result.ShowWater === true\" (click)=\"commodityChange('water')\" href=\"#\" onclick=\"return false;\" aria-label=\"Water Commodity\" class=\"btn btn-default xs-ml-5 icon__water\" [ngClass]=\"selectedCommodity==='water' ? 'icon_focus' : ''\" role=\"button\"><span class=\"icon icon-drop\" aria-hidden=\"true\"></span> <span *ngIf=\"result.ShowCommodityLabels === 'true'\" class=\"hidden-xs\">{{result.WaterLabel}}</span> <span class=\"print-only\">{{result.WaterLabel}}</span> </a><a href=\"#\" aria-label=\"Chart and Graph toggle\" onclick=\"return false;\" (click)=\"displayChart()\" class=\"btn btn-default chart-table-spacer\" [ngClass]=\"showChart ? 'icon__table' : 'icon__graph'\" role=\"button\"><span class=\"icon\" [ngClass]=\"showChart ? 'icon-table' : 'icon-graph'\"></span></a></div></div></div></div></div><div class=\"panel-body\"><div [style.display]=\"showChart ? 'block': 'none'\"><div id=\"iws_cn_chart\" style=\"height: 400px\"></div></div><div [style.display]=\"showChart ? 'none': 'block'\"><div role=\"grid\" aria-readonly=\"true\" aria-labelledby=\"consumption\"><table class=\"table table-hover table-striped\" role=\"presentation\"><thead><tr role=\"row\"><th *ngFor=\"let h of table.Headers\" role=\"columnheader\">{{h.Label}}</th></tr></thead><tbody><tr *ngFor=\"let l of table.Rows\" role=\"row\"><td *ngFor=\"let c of l.Cells\" role=\"gridcell\" [attr.data-th]=\"c.Header\">{{c.Value}}</td></tr></tbody></table></div></div></div><div *ngIf=\"result.ShowFooterText === 'true' || result.ShowFooterLink === 'true'\" class=\"panel-footer\"><div *ngIf=\"result.ShowFooterText === 'true'\" class=\"footertext\">{{result.Footer}}</div><div *ngIf=\"result.ShowFooterLink === 'true'\" class=\"footerlink dv-external-links\"><a href=\"{{result.FooterLink}}\">{{result.FooterLinkText}}</a></div></div></div></div></div>",
            providers: [consumption_service_1.ConsumptionService, app_service_1.AppService]
        }),
        __metadata("design:paramtypes", [consumption_service_1.ConsumptionService, app_service_1.AppService, core_1.ElementRef, core_1.Renderer])
    ], Consumption);
    return Consumption;
}());
exports.Consumption = Consumption;

//# sourceMappingURL=consumption.js.map
