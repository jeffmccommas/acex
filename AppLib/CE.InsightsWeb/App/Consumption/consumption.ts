﻿import { Component, ElementRef, OnInit, Renderer } from "@angular/core";
import { ConsumptionService } from "./consumption.service";
import { AppService, IAppParams } from "../app.service";

@Component({
    selector: 'consumption',
    templateUrl: 'App/Consumption/consumption.html',
    providers: [ConsumptionService, AppService]
})
export class Consumption implements OnInit {
    static fromUnitTest = "unittest";
    elementRef: ElementRef;
    renderer:Renderer;
    result: any;
    table: any;
    options: any;
    error: Object;
    enableLoader: Boolean;
    errorOccurred: Boolean;
    errorMessage: string;
    consumptionService: ConsumptionService;
    appService: AppService;
    params: Array<IAppParams>;
    parameters: string;
    tabKey: string;
    fromUTest: Boolean;
    selectedCommodity: string;
    showChart: Boolean;
    commodityCount: number;
    startDate: string;
    endDate: string;
    resolution: string;
    prevResolution: string;
    offset: any;
    paging: Boolean;
    drilling: Boolean;
    dl_res: string;
    dl_highlight: Boolean;
    max_res: any;
    data_points: any;
    current_page: number;
    noConsumption: Boolean;
    commodities: any;
    resolutions: any;
    setListener:Boolean;

    constructor(consumptionService: ConsumptionService, appService: AppService, elementRef: ElementRef, renderer: Renderer) {
        this.enableLoader = true;
        this.elementRef = elementRef;
        this.renderer = renderer;
        this.params = appService.getParams();
        this.parameters = this.params[0].parameters;
        this.tabKey = this.params[0].tabKey;
        this.errorMessage = this.params[0].errorMessage;
        this.consumptionService = consumptionService;
        this.appService = appService;
        this.showChart = true;
        this.paging = false;
        this.drilling = false;
        this.commodityCount = 0;
        this.dl_res = "day";
        this.dl_highlight = false;
        this.startDate = "";
        this.endDate = "";
        this.resolution = "";
        this.prevResolution = "";
        this.selectedCommodity = "";
        this.setListener = true;
        this.current_page = 0;
        if (this.consumptionService.res !== undefined) {
            this.result = consumptionService.res;
            this.fromUTest = this.consumptionService.fromUnitTest;
        }
    }

    ngOnInit() {
        if (this.dl_res !== null && this.dl_res !== "") {
            this.resolution = this.dl_res;
            this.dl_highlight = true;
        }

        if (!this.fromUTest) {
            this.loadconsumption();
        }
    }

    loadconsumption() {
        this.result = {};
        this.offset = "";
        if (!this.fromUTest) {
            this.consumptionService.getConsumption(this.tabKey,
                    this.parameters,
                    this.startDate,
                    this.endDate,
                    this.resolution,
                    this.prevResolution,
                    this.selectedCommodity,
                    this.offset,
                    this.paging,
                    this.drilling)
                .subscribe(res => this.result = res,
                    err => this.appService.logError(this, err),
                    () => this.disableLoader()
                );            
        }
    }

    disableLoader() {
        if (!this.fromUTest) {
            setTimeout(() => this.appService.setHtml(this.elementRef), 1);
            setTimeout(() => this.appService.setIframeExternalClass(this.elementRef), 0);
        }
        this.enableLoader = false;
        if (this.result.NoConsumption === true) {
            this.noConsumption = true;
        } else {
            if (this.result.CommoditiesText !== null) {
                this.checkCommodities();
                this.prepareResolutionData();
                this.displayRange();
                this.table = {
                    Rows: [],
                    Headers: []
                };
                setTimeout(() => this.drawChart(this.selectedCommodity));    
            }                 
        }
    }

    checkCommodities() {
        if (this.selectedCommodity === "") {
                this.selectedCommodity = this.result.Commodities.split(",")[0];
            }
            //check for commoditites here
            var comsText = this.result.CommoditiesText.split(",");
            var comsVal = this.result.Commodities.split(",");
            var commodityfound = false;
            this.result.ShowCommodities = true;
            this.commodityCount = 0;
            if (comsVal.length > 1) {
                for (var i = 0; i < comsVal.length; i++) {
                    if (comsVal[i] === "gas") {
                        this.result.ShowGas = true;
                        this.result.GasLabel = comsText[i];
                        this.commodityCount++;
                        if (commodityfound === false) {
                            commodityfound = true;
                            this.selectedCommodity = "gas";
                        }
                    }
                    if (comsVal[i] === "electric") {
                        this.result.ShowElectric = true;
                        this.result.ElectricLabel = comsText[i];
                        this.commodityCount++;
                        if (commodityfound === false) {
                            commodityfound = true;
                            this.selectedCommodity = "electric";
                        }
                    }
                    if (comsVal[i] === "water") {
                        this.result.ShowWater = true;
                        this.result.WaterLabel = comsText[i];
                        this.commodityCount++;
                        if (commodityfound === false) {
                            commodityfound = true;
                            this.selectedCommodity = "water";
                        }
                    }
                }
            }
            if (this.commodityCount < 2) {
                this.result.ShowCommodities = false;
            }
    }

    prepareResolutionData() {
        if (this.result.ResolutionKeys === null || this.result.NoConsumption === true) {
            if (this.dl_res === null || this.dl_res === "") {
                this.resolution = "month";
            }
            this.result.ShowResolutionsDropdown = "false";
        } else {
            var resText = this.result.ResolutionKeysText.split(",");
            var resVal = this.result.ResolutionKeys.split(",");
            var resAvail = this.result.ResolutionsAvailable.split(",");
            this.max_res = resAvail[resAvail.length - 1];
            var resolutions = [];
            if (this.resolution === "") {
                this.resolution = resAvail[0];
            }

            if (resAvail.length > 1) {

                for (var i = 0; i < resVal.length; i++) {
                    if (this.result.ResolutionsAvailable.indexOf(resVal[i]) > -1) {
                        resolutions.push({
                            Id: i,
                            Index: i,
                            Text: resText[i],
                            Value: resVal[i]
                        });

                        if (resVal[i] === this.resolution) {
                            this.result.SelectedResolutionValue = i;
                            this.result.SelectedResolutionText = resText[i];
                        }
                    }
                }

                if (resolutions.length > 1) {
                    this.result.ShowResolutionsDropdown = "true";
                    this.result.Resolutions = resolutions;
                }

                switch (this.resolution) {
                    case "month":
                        this.data_points = this.result.MonthCount;
                        break;
                    case "day":
                        this.data_points = this.result.DayCount;
                        break;
                    case "hour":
                        this.data_points = this.result.HourCount;
                        break;
                    case "halfhour":
                        this.data_points = this.result.ThirtyMinCount;
                        break;
                    case "fifteen":
                        this.data_points = this.result.FifteenMinCount;
                        break;
                    case "five":
                        this.data_points = this.result.FiveMinCount;
                        break;
                }
            }
        }
    }

    resolutionSelected(Id, Val, Text) {
        this.drilling = false;
        if (this.result.SelectedResolutionValue > Id) {
            this.startDate = null;
            this.endDate = null;
        }
        if (!this.fromUTest && this.elementRef.nativeElement.querySelector("[id='iws_cn_res-" + Id + "']").getAttribute("data-drilling")!==null) {
            this.drilling = true;
        }
        if (!this.fromUTest && this.elementRef.nativeElement.querySelector("[id='iws_cn_res-" + Id + "']").getAttribute("data-enddate") !== null) {
            this.endDate = this.elementRef.nativeElement.querySelector("[id='iws_cn_res-" + Id + "']").getAttribute("data-enddate");
        }
        this.resolution = Val;
        this.current_page = 0;
        this.paging = false;
        switch (this.resolution) {
        case "month":
            this.data_points = this.result.MonthCount;
            break;
        case "day":
                this.data_points = this.result.DayCount;
            break;
        case "hour":
                this.data_points = this.result.HourCount;
            break;
        case "halfhour":
                this.data_points = this.result.ThirtyMinCount;
            break;
        case "fifteen":
                this.data_points = this.result.FifteenMinCount;
            break;
        case "five":
                this.data_points = this.result.FiveMinCount;
            break;
        }
        this.offset = this.data_points * -1;
        this.result.SelectedResolutionText = Text;
        this.result.SelectedResolutionValue = Id;
        
        this.getConsumption(this.selectedCommodity);
        this.displayRange();
    }

    drawChart(commodity) {
        this.selectedCommodity = commodity;
        var options;
        if (this.table !== undefined) {
            this.table.length = 0;
        }
        //add click event listener for resolutions - dropdown click and chart drill down
        if (this.result.Resolutions!=undefined && this.result.Resolutions.length>1) {
            for (var j = 0; j < this.result.Resolutions.length; j++) {
                var event = this.renderer.listen(this.elementRef.nativeElement.querySelector("[id='iws_cn_res-" + this.result.Resolutions[j].Id + "']"), 'click',
                    (event) => {
                        event.preventDefault();
                        this.resolutionSelected(event.currentTarget.id.split("-")[1],
                            event.currentTarget.title,
                            event.currentTarget.text);
                    });

                

                if (!this.setListener) {
                    event();
                    var element = this.elementRef.nativeElement
                        .querySelector("[id='iws_cn_res-" + this.result.Resolutions[j].Id + "']");
                    if (element.getAttribute("data-drilling")!=null) {
                        element.removeAttribute("data-drilling");
                    }
                    if (element.getAttribute("data-enddate") != null) {
                        element.removeAttribute("data-enddate");
                    }    
                }
            }            
        }

        if (!this.fromUTest && !this.result.NoConsumption) {
            const params = { current_page: this.current_page, paging: this.paging, drilling: this.drilling, startDate: this.startDate, endDate: this.endDate, res_selected: this.resolution, max_res: this.max_res };
            this.options = this.appService.buildConsumptionChart(this.result, options, params, this.elementRef);
            this.drawTable();
        }
        this.highLightElement();
    }

    displayChart() {
        this.showChart = !this.showChart;
        if (!this.fromUTest) { setTimeout(() => this.drawChart(this.selectedCommodity)); }
    }

    highLightElement() {
        if (this.dl_highlight===true) {
            this.elementRef.nativeElement.querySelector(".deep-link-highlight").focus();
            this.appService.consumptionHighLightElement(this.elementRef);            
        }
    }

    drawTable() {
        var series = this.options.series;
        this.table = {
            Rows: [],
            Headers: []
        };
        if (this.resolution === "month") {
            this.table.Headers.push({ Label: this.result.TableViewColumn1MonthLabel });
        } else {
            this.table.Headers.push({ Label: this.result.TableViewColumn1DateLabel});
        }
        for (var h = 0; h < series.length; h++) {
            this.table.Headers.push({Label: series[h].name});
        }
        for (var d = 0; d < series[0].data.length; d++) {
            var row = { Cells: [] };
            var cells = [];
            var header;
            if (this.resolution==="month") {
                header = this.result.TableViewColumn1MonthLabel;
            } else {
                header = this.result.TableViewColumn1DateLabel;
            }
            cells.push({ Header: header, Value: series[0].data[d].tooltip_date });
            for (var s = 0; s < series.length; s++) {
                cells.push({Header: series[s].name,Value: series[s].data[d].tooltip_value_formatted});
            }
            row.Cells = cells;
            this.table.Rows.push(row);
        }
    }

    displayRange() {
    this.startDate = this.result.StartDate;
    this.endDate = this.result.EndDate;
    }

    pageForward() {
        this.offset = this.data_points;
        this.paging = true;
        this.current_page += 1;
        this.displayRange();
        this.getConsumption(this.selectedCommodity);
    }

    pageBack() {
        this.offset = this.data_points * -1;
        this.paging = true;
        this.current_page -= 1;
        this.displayRange();
        this.getConsumption(this.selectedCommodity);
    }

    getConsumption(commodity) {
        this.selectedCommodity = commodity;
        this.commodities = [this.result.CommoditiesText, this.result.Commodities];
        this.resolutions = [this.result.ShowResolutionsDropdown, this.result.Resolutions, this.result.SelectedResolutionValue, this.result.SelectedResolutionText];
        if (!this.fromUTest) {
            this.consumptionService.getConsumption(this.tabKey,
                    this.parameters,
                    this.startDate,
                    this.endDate,
                    this.resolution,
                    this.prevResolution,
                    this.selectedCommodity,
                    this.offset,
                    this.paging,
                    this.drilling)
                .subscribe(res => this.result = res,
                    err => this.appService.logError(this, err),
                    () => this.callback(this.selectedCommodity)
                );
        }
    }

    callback(commodity) {
        this.result.CommoditiesText = this.commodities[0];
        this.result.Commodities = this.commodities[1];
        if (this.result.CommoditiesText !== null) {
            if (!this.fromUTest) { this.checkCommodities();}
            this.selectedCommodity = commodity;
            if (!this.fromUTest) { this.prepareResolutionData(); }
            this.result.ShowResolutionsDropdown = this.resolutions[0];
            this.result.Resolutions = this.resolutions[1];
            if (this.result.SelectedResolutionValue === undefined || this.result.SelectedResolutionValue === "")
                {this.result.SelectedResolutionValue = this.resolutions[2];}
            this.result.SelectedResolutionText = this.resolutions[3];
            this.displayRange();
            this.table = {
                Rows: [],
                Headers: []
            };
            this.setListener = false;
            if (!this.fromUTest) {setTimeout(() => this.drawChart(this.selectedCommodity));}
            }
    }

    commodityChange(commodity) {
        this.resolution = this.result.Resolutions[0].Value;
        this.result.SelectedResolutionText = this.result.Resolutions[0].Text;
        if (this.resolution===undefined) {
            this.resolution = "month";
        }
        this.result.SelectedResolutionValue = this.result.Resolutions[0].Id;
        this.startDate = "";
        this.endDate = "";
        this.current_page = 0;
        this.getConsumption(commodity);
    }
}