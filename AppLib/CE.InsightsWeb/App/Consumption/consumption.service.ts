﻿import "rxjs/add/operator/map";
import { Injectable, Input } from "@angular/core"
import { Http } from "@angular/http";

@Injectable()
export class ConsumptionService {
    @Input() res: Object;
    @Input() fromUnitTest: Boolean;
    http: Http;
    constructor(http: Http) {
        this.http = http;
    }

    getConsumption(tabKey, params,startDate,endDate,resolution,prevResolution,commodity,offset,paging,drilling) {
       return this.http.get(`./Consumption/Get?tabKey=${tabKey}&parameters=${window.btoa(params)}&startDate=${startDate}&endDate=${endDate}&resolution=${resolution}&prevResolution=${prevResolution}&commodity=${commodity}&offset=${offset}&paging=${paging}&drilling=${drilling}`).map(res => res.json());
    }

}

