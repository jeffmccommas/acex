"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
require("rxjs/add/operator/map");
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var ConsumptionService = (function () {
    function ConsumptionService(http) {
        this.http = http;
    }
    ConsumptionService.prototype.getConsumption = function (tabKey, params, startDate, endDate, resolution, prevResolution, commodity, offset, paging, drilling) {
        return this.http.get("./Consumption/Get?tabKey=" + tabKey + "&parameters=" + window.btoa(params) + "&startDate=" + startDate + "&endDate=" + endDate + "&resolution=" + resolution + "&prevResolution=" + prevResolution + "&commodity=" + commodity + "&offset=" + offset + "&paging=" + paging + "&drilling=" + drilling).map(function (res) { return res.json(); });
    };
    return ConsumptionService;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], ConsumptionService.prototype, "res", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Boolean)
], ConsumptionService.prototype, "fromUnitTest", void 0);
ConsumptionService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], ConsumptionService);
exports.ConsumptionService = ConsumptionService;
//# sourceMappingURL=consumption.service.js.map