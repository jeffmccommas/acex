﻿import "rxjs/add/operator/map";
import {Injectable, Input} from "@angular/core"
import {Http} from "@angular/http";

@Injectable()
export class ReportListService {
    @Input() res: Object;
    @Input() fromUnitTest: Boolean;
    http: Http;
    constructor(http: Http) {
        this.http = http;
    }

    getReportList(tabKey, params) {
       return this.http.get(`./ReportList/GetReportList?tabkey=${tabKey}&parameters=${window.btoa(params)}`).map(res => res.json());

    }

    deleteReport(tabKey,params,reportId) {
        return this.http.post(`./ReportList/DeleteReport?tabkey=${tabKey}&parameters=${window.btoa(params)}&reportId=${reportId}`).map(res => res.json());
        
    }
}

