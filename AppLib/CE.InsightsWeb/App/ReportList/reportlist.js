"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var reportlist_service_1 = require("./reportlist.service");
var app_service_1 = require("../app.service");
var ReportList = (function () {
    function ReportList(reportlistService, appService, elementRef) {
        this.confirmClicked = false;
        this.cancelClicked = false;
        this.popoverTitle = "";
        this.enableLoader = true;
        this.elementRef = elementRef;
        this.params = appService.getParams();
        this.parameters = this.params[0].parameters;
        this.tabKey = this.params[0].tabKey;
        this.errorMessage = this.params[0].errorMessage;
        this.reportlistService = reportlistService;
        this.appService = appService;
        this.viewErrorOccurred = false;
        this.deleteErrorOccurred = false;
        this.noReport = true;
        if (this.reportlistService.res !== undefined) {
            this.result = reportlistService.res;
            this.fromUTest = this.reportlistService.fromUnitTest;
        }
    }
    ReportList_1 = ReportList;
    ReportList.prototype.ngOnInit = function () {
        if (this.reportlistService.getReportList("", "").toString() !== ReportList_1.fromUnitTest) {
            this.loadReports();
        }
    };
    ReportList.prototype.loadReports = function () {
        var _this = this;
        this.result = {};
        if (this.fromUnitTs !== ReportList_1.fromUnitTest) {
            this.reportlistService.getReportList(this.tabKey, this.parameters)
                .subscribe(function (res) { return _this.result = res; }, function (err) { return _this.appService.logError(_this, err); }, function () { return _this.disableLoader(); });
        }
    };
    ReportList.prototype.disableLoader = function () {
        var _this = this;
        if (this.fromUnitTs !== ReportList_1.fromUnitTest) {
            setTimeout(function () { return _this.appService.setHtml(_this.elementRef); }, 1);
        }
        if (this.result === undefined || this.result === null) {
            this.appService.logError(this, '');
        }
        this.enableLoader = false;
    };
    ReportList.prototype.downloadReport = function (reportId, filename, rtype) {
        var _this = this;
        this.viewErrorOccurred = false;
        this.deleteErrorOccurred = false;
        if (this.fromUnitTs !== ReportList_1.fromUnitTest) {
            this.download(reportId, filename, function (data) {
                if (data) {
                    _this.viewErrorOccurred = true;
                    if (rtype === 'user')
                        _this.result.ReportsUser[_this.parseReportList(_this.result.ReportsUser, reportId)].ErrorStatus = true;
                    else
                        _this.result.ReportsCSR[_this.parseReportList(_this.result.ReportsCSR, reportId)].ErrorStatus = true;
                }
            });
        }
    };
    ReportList.prototype.download = function (reportId, filename, callback) {
        var xhr = new XMLHttpRequest();
        var url = "./ReportList/DownLoadReport?tabkey=" + this.tabKey + "&parameters=" + window
            .btoa(this.parameters) + "&reportId=" + reportId;
        xhr.open('GET', url, true);
        xhr.responseType = 'blob';
        if (this.fromUnitTs !== ReportList_1.fromUnitTest) {
            xhr.onreadystatechange = (function () {
                if (xhr.readyState === 4 && xhr.status === 200) {
                    if (this.getResponseHeader("Content-Disposition").includes("filename")) {
                        var blob = new Blob([this.response], { type: "application/pdf" });
                        saveAs(blob, filename);
                    }
                    else {
                        this.viewErrorOccurred = true;
                    }
                    callback(this.viewErrorOccurred);
                }
            });
            xhr.send();
        }
    };
    ReportList.prototype.deleteReport = function (reportId, rtype) {
        var _this = this;
        this.viewErrorOccurred = false;
        this.deleteErrorOccurred = false;
        this.deleteReportResult = {};
        if (this.fromUnitTs !== ReportList_1.fromUnitTest) {
            this.reportlistService.deleteReport(this.tabKey, this.parameters, reportId)
                .subscribe(function (res) { return _this.deleteReportResult = res; }, function (err) { return _this.appService.logError(_this, err); }, function () { return _this.deleteReportValidation(reportId, rtype); });
        }
        else {
            this.deleteReportResult = 'unittest';
        }
    };
    ReportList.prototype.deleteReportValidation = function (reportId, rtype) {
        if (this.deleteReportResult.Content === "{}") {
            if (rtype === 'user') {
                this.result.ReportsUser.splice(this.parseReportList(this.result.ReportsUser, reportId), 1);
            }
            else {
                this.result.ReportsCSR.splice(this.parseReportList(this.result.ReportsCSR, reportId), 1);
            }
        }
        else {
            this.deleteErrorOccurred = true;
            if (rtype === 'user')
                this.result.ReportsUser[this.parseReportList(this.result.ReportsUser, reportId)].ErrorStatus = true;
            else
                this.result.ReportsCSR[this.parseReportList(this.result.ReportsCSR, reportId)].ErrorStatus = true;
        }
    };
    ReportList.prototype.parseReportList = function (list, reportId) {
        for (var i = 0; i < list.length; i++) {
            if (list[i].Id === reportId) {
                return i;
            }
        }
        return -1;
    };
    ReportList.widgetKey = "reportlist";
    ReportList.fromUnitTest = "unittest";
    ReportList = ReportList_1 = __decorate([
        core_1.Component({
            selector: 'reportlist',
            template: "<Loader *ngIf=\"enableLoader\"></Loader><div *ngIf=\"!enableLoader\"><div *ngIf=\"errorOccurred\"><div class=\"panel panel-default\"><div *ngIf=\"result.Content.ShowTitle == 'true' && result.Content.Title !== ''\" class=\"panel-heading\"><h1 role=\"presentation\">{{result.Content.Title}}</h1></div><div class=\"alert alert-danger\"><span class=\"glyphicon glyphicon-exclamation-sign xs-mr-10\" aria-hidden=\"true\"></span><strong>{{errorMessage}}</strong> <span class=\"sr-only\">Error:</span></div></div></div><div *ngIf=\"!errorOccurred\" class=\"container\"><div class=\"row\"><div class=\"col-ms-10 col-md-8 col-lg-6 xs-pl-0\"><div class=\"panel panel-default PDF-list\"><div class=\"panel-heading\"><h1 role=\"presentation\" class=\"panel-title PDF__title\">{{result.Content.Title}}</h1><div class=\"row no-right-gutter\"><h2 *ngIf=\"result.Content.ShowSubTitle == 'true' && result.Content.SubTitle !== ''\" role=\"presentation\" class=\"subtitle\">{{result.Content.SubTitle}}</h2><div *ngIf=\"result.Content.ShowIntro == 'true' && result.Content.ShowIntro !== ''\" class=\"markdown\">{{result.Content.IntroText}}</div></div></div><div class=\"scroll\"><div class=\"PDF__subtitle--block panel-body xs-pt-20 xs-pb-0\"><h4 id=\"iws_ql_subtitle\" role=\"presentation\" class=\"sub-title xs-mb-0\"><b>{{result.Content.CustomerSectionTitle}}</b></h4></div><div class=\"panel-body PDF__list--item\"><div *ngIf=\"result.ReportsUser.length===0\">{{result.Content.NoData}}</div><div *ngIf=\"result.ReportsUser.length > 0\"><div *ngFor=\"let n of result.ReportsUser\" class=\"container-fluid sub-line xs-mb-20\"><div class=\"col-sm-9 xs-pl-0\"><a class=\"inline-block PDF__subtitle\" (click)=\"downloadReport(n.Id,n.Name,'user')\" onclick=\"return false;\" href=\"#\"><h4>{{n.Title}}</h4></a><p class=\"inline-block ms-pl-15 PDF__time\">{{n.RelativeTime}}</p><div>{{n.Description}}</div><div *ngIf=\"n.ErrorStatus\"><p *ngIf=\"viewErrorOccurred\" class=\"error\">{{result.Content.ViewError}}</p><p *ngIf=\"deleteErrorOccurred\" class=\"error\">{{result.Content.DeleteError}}</p></div></div><div class=\"col-sm-3 xs-pl-0 xs-pr-0\"><div class=\"notify-detail__buttons\"><a class=\"xs-ml-0 xs-mr-0 xs-pl-0 xs-pr-15 button-small\" (click)=\"downloadReport(n.Id,n.Name,'user')\" onclick=\"return false;\" href=\"#\">{{result.Content.OpenLink}}</a> <a class=\"xs-ml-0 xs-mr-0 xs-pl-0 xs-pr-0 button-small\" href=\"#\" mwlConfirmationPopover [message]=\"result.Content.DeletConfirm\" placement=\"top\" [appendToBody]=\"true\" (confirm)=\"deleteReport(n.Id,'user')\" onclick=\"return false;\" [title]=\"popoverTitle\">{{result.Content.DeleteLink}}</a></div></div></div></div></div><div *ngIf=\"result.isCSR\" class=\"PDF__subtitle--block panel-body xs-pt-20 xs-pb-0\"><h4 id=\"iws_ql_subtitle\" role=\"presentation\" class=\"sub-title xs-mb-0\"><b>{{result.Content.CsrSectionTitle}}</b></h4></div><div *ngIf=\"result.isCSR\" class=\"panel-body PDF__list--item\"><div *ngIf=\"result.ReportsCSR.length===0\">{{result.Content.NoData}}</div><div *ngIf=\"result.ReportsCSR.length>0\"><div *ngFor=\"let n of result.ReportsCSR\" class=\"container-fluid sub-line xs-mb-20\"><div class=\"col-sm-9 xs-pl-0\"><a class=\"inline-block PDF__subtitle\" (click)=\"downloadReport(n.Id,n.Name,'user')\" onclick=\"return false;\" href=\"#\"><h4>{{n.Title}}</h4></a><p class=\"inline-block ms-pl-15 PDF__time\">{{n.RelativeTime}}</p><div>{{n.Description}}</div><div *ngIf=\"n.ErrorStatus\"><p *ngIf=\"viewErrorOccurred\" class=\"error\">{{result.Content.ViewError}}</p><p *ngIf=\"deleteErrorOccurred\" class=\"error\">{{result.Content.DeleteError}}</p></div></div><div class=\"col-sm-3 xs-pl-0 xs-pr-0\"><div class=\"notify-detail__buttons\"><a class=\"xs-ml-0 xs-mr-0 xs-pl-0 xs-pr-15 button-small\" (click)=\"downloadReport(n.Id,n.Name,'csr')\" href=\"#\" onclick=\"return false;\">{{result.Content.OpenLink}}</a> <a class=\"xs-ml-0 xs-mr-0 xs-pl-0 xs-pr-0 button-small\" href=\"#\" mwlConfirmationPopover [message]=\"result.Content.DeletConfirm\" placement=\"top\" (confirm)=\"deleteReport(n.Id,'csr')\" [appendToBody]=\"true\" onclick=\"return false;\" [title]=\"popoverTitle\">{{result.Content.DeleteLink}}</a></div></div></div></div></div></div></div></div></div><div *ngIf=\"result.Content.ShowFooterText === 'true' || result.Content.ShowFooterLink === 'true'\" class=\"panel-footer sub__footer container-fluid\"><div *ngIf=\"result.Content.ShowFooterText === 'true'\" class=\"footertext\">{{result.Content.Footer}}</div><div *ngIf=\"result.Content.ShowFooterLink === 'true'\" class=\"footerlink\"><a href=\"{{result.Content.FooterLink}}\">{{result.Content.FooterLinkText}}</a></div></div></div></div>",
            providers: [reportlist_service_1.ReportListService, app_service_1.AppService]
        }),
        __metadata("design:paramtypes", [reportlist_service_1.ReportListService, app_service_1.AppService, core_1.ElementRef])
    ], ReportList);
    return ReportList;
    var ReportList_1;
}());
exports.ReportList = ReportList;

//# sourceMappingURL=reportlist.js.map
