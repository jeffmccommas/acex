﻿import { Component, ElementRef, OnInit} from "@angular/core";
import {ReportListService} from "./reportlist.service";
import {AppService, IAppParams,} from "../app.service";

@Component({
    selector: 'reportlist',
    templateUrl: 'App/ReportList/reportlist.html',
    providers: [ReportListService, AppService]
})

export class ReportList implements OnInit {
    elementRef: ElementRef;
    result: any;   
    error: Object;
    static errorMessage: string;
    static widgetKey = "reportlist";
    enableLoader: Boolean;
    errorOccurred: Boolean;
    viewErrorOccurred: Boolean;
    deleteErrorOccurred: Boolean;
    noReport:Boolean;
    errorMessage: string;
    reportlistService: ReportListService;
    appService: AppService;
    params: Array<IAppParams>;
    parameters: string;
    tabKey: string;
    downloadReportDetails: any;
    deleteReportResult: any;
    fromUnitTs: string;
    static fromUnitTest = "unittest";
    fromUTest: Boolean;
    confirmClicked: boolean = false;
    cancelClicked: boolean = false;
    popoverTitle: string="";
    constructor(reportlistService: ReportListService, appService: AppService, elementRef: ElementRef ) {
        this.enableLoader = true;
        this.elementRef = elementRef;
        this.params = appService.getParams();
        this.parameters = this.params[0].parameters;
        this.tabKey = this.params[0].tabKey;
        this.errorMessage = this.params[0].errorMessage;
        this.reportlistService = reportlistService;
        this.appService = appService;
        this.viewErrorOccurred = false;
        this.deleteErrorOccurred = false;
        this.noReport = true;
        if (this.reportlistService.res !== undefined) {
            this.result = reportlistService.res;
            this.fromUTest = this.reportlistService.fromUnitTest;
        }
    }

    ngOnInit() {
        if (this.reportlistService.getReportList("", "").toString() !== ReportList.fromUnitTest) {
            this.loadReports();
        }
    }
   
    loadReports() {
        this.result = {};
        if (this.fromUnitTs !== ReportList.fromUnitTest) {
            this.reportlistService.getReportList(this.tabKey, this.parameters)
                .subscribe(res => this.result = res,
                    err => this.appService.logError(this, err),
                    () => this.disableLoader()
                );
        }
    }

    disableLoader() {
        if (this.fromUnitTs !== ReportList.fromUnitTest) {
            setTimeout(() => this.appService.setHtml(this.elementRef), 1);
        }
        if (this.result === undefined || this.result === null) {
            this.appService.logError(this, '');
        }
        this.enableLoader = false;
    }
   
    downloadReport(reportId, filename, rtype) {
        this.viewErrorOccurred = false;
        this.deleteErrorOccurred = false;
        if (this.fromUnitTs !== ReportList.fromUnitTest){
        this.download(reportId, filename,data => {
            if (data) {
                this.viewErrorOccurred = true;
                if (rtype === 'user') 
                    this.result.ReportsUser[this.parseReportList(this.result.ReportsUser, reportId)].ErrorStatus = true;
                else
                    this.result.ReportsCSR[this.parseReportList(this.result.ReportsCSR, reportId)].ErrorStatus = true;
            }
            });
        }
    }

    download(reportId, filename,callback) {
        const xhr = new XMLHttpRequest();
        const url = `./ReportList/DownLoadReport?tabkey=${this.tabKey}&parameters=${window
            .btoa(this.parameters)}&reportId=${reportId}`;
        
        xhr.open('GET', url, true);
        xhr.responseType = 'blob';
        if (this.fromUnitTs !== ReportList.fromUnitTest) {
            xhr.onreadystatechange = ((function() {
                if (xhr.readyState === 4 && xhr.status === 200) {
                    if (this.getResponseHeader("Content-Disposition").includes("filename")) {
                        const blob = new Blob([this.response], { type: "application/pdf" });
                        saveAs(blob, filename);
                    } else {
                        this.viewErrorOccurred = true;
                    }
                    
                    callback(this.viewErrorOccurred);
                }
            }
            ) as any);
            xhr.send();
        } 
    }


    deleteReport(reportId,rtype) {
        this.viewErrorOccurred = false;
        this.deleteErrorOccurred = false;
        this.deleteReportResult = {};
        if (this.fromUnitTs !== ReportList.fromUnitTest) {
            this.reportlistService.deleteReport(this.tabKey, this.parameters, reportId)
                .subscribe(res => this.deleteReportResult = res,
                    err => this.appService.logError(this, err),
                    () => this.deleteReportValidation(reportId,rtype)
                );
        } else {
            this.deleteReportResult = 'unittest';
        }               
    }

    deleteReportValidation(reportId, rtype) {
        
            if (this.deleteReportResult.Content === "{}") {
                if (rtype === 'user') {
                    this.result.ReportsUser.splice(this.parseReportList(this.result.ReportsUser, reportId), 1);
                } else {
                    this.result.ReportsCSR.splice(this.parseReportList(this.result.ReportsCSR, reportId), 1);
                }
            } else {
                this.deleteErrorOccurred = true;
                if (rtype === 'user')
                    this.result.ReportsUser[this.parseReportList(this.result.ReportsUser, reportId)].ErrorStatus = true;
                else
                    this.result.ReportsCSR[this.parseReportList(this.result.ReportsCSR, reportId)].ErrorStatus = true;
            }
        
    }

    parseReportList(list, reportId) {
       
        for (let i = 0; i < list.length; i++) {
            if (list[i].Id===reportId) {
                return i;
            }
        }
        return -1;
    }
}