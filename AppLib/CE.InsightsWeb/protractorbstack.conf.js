exports.config = {
    'specs': ['./test/e2e/**/*.spec.js'],
    'maxSessions': 2,
    'seleniumAddress': 'http://hub-cloud.browserstack.com/wd/hub',
    'jasmineNodeOpts': { 'defaultTimeoutInterval': 60000 },

    'commonCapabilities': {
        'browserstack.user': 'muazzamali3',
        'browserstack.key': 'sJxVt5NE5DjkxQsfxBiE',
        'build': '18.01or18.03',
        'project': 'acewebsite',
        'browserstack.debug': true,
        'allScriptsTimeout': 60000,
        'getPageTimeout': 60000
    },

    //nonparallel uncomment below
    //'capabilities': {
    //    'browserstack.user': 'muazzamali3',
    //    'browserstack.key': 'sJxVt5NE5DjkxQsfxBiE',
    //    'browserstack.debug': true,
    //    'browserName': 'chrome'
    //}
    'multiCapabilities': [{
        'os': 'Windows',
        'os_version': '10',
        'browserName': 'Chrome',
        'browser_version': '63.0'
    },
    {
        'os': 'OS X',
        'os_version': 'Sierra',
        'browserName': 'Safari',
        'browser_version': '10.1',
         'browserstack.safari.enablePopups': true
        },
    //exit code occurs for iphone 8 - comment out for now
     //{
     //   'device': 'iPhone 8',
     //  'realMobile': 'true',
     //  'os_version': '11.0' 
    //}
    //,
    //never renders screen on firefox - 113 out of 116 failures on last run 1/4/18 - commenting out
    //{
     //   'os': 'Windows',
     //   'os_version': '8.1',
     //   'browserName': 'Firefox',
      //  'browser_version': '57.0'
    //}, 
    {
         'os': 'Windows',
         'os_version': '10',
         'browserName': 'Edge',
         'browser_version': '16.0',
         'browserstack.ie.enablePopups': true
    }]
};

// Code to support common capabilities
exports.config.multiCapabilities.forEach(function (caps) {
    for (var i in exports.config.commonCapabilities) caps[i] = caps[i] || exports.config.commonCapabilities[i];
});