/// <binding BeforeBuild='inline-templates,buildjs,test'/>
var zip = require("gulp-zip");
var gulp = require('gulp');
var requireDir = require('require-dir');
var tasks = requireDir('./tasks');
var inlineNg2Template = require('gulp-inline-ng2-template');
var htmlMinifier = require('html-minifier');
var tsc = require('gulp-typescript');
var sourcemaps = require('gulp-sourcemaps');

var config = require('./gulp.config')();
var tscConfig = require('./tsconfig.json');

/* Default task */
//gulp.task('default', ['serve-dev']);

gulp.task('inline-templates',
    function () {
        return gulp.src(['!app/**/*.module.ts', '!app/**/*.service.ts', '!app/Helper/*.ts',
            '!app/Tests/**/*.ts', '!app/site.ts', '!app/boot.ts',
            '!app/bootprod.ts', '!test/**/*.ts', 'app/**/*.ts'])
            .pipe(inlineNg2Template({ UseRelativePaths: true, indent: 0, removeLineBreaks: true, templateProcessor: minifyTemplate }))
            .pipe(sourcemaps.init({ loadMaps: true }))
            .pipe(tsc(tscConfig.compilerOptions))
            .pipe(sourcemaps.write('', {
                includeContent: false,
                mapSources: function (sourcePath) {
                    return sourcePath.split("/")[1];
                }
            }))
            .pipe(gulp.dest('app'));
    });


gulp.task('buildjs', function () {
    var Builder = require('systemjs-builder');
    var builder = new Builder("", "./App/system.js");
    var outFile = "App/app.module.min.js";
    builder.bundle('App/app.module', outFile, { minify: true, mangle: true, rollup: true })
        .then(function() { console.log('App build complete'); })
        .catch(function (err) {
            console.log('App build error');
            console.log(err);
        });
});

function minifyTemplate(path, ext, file, cb) {
    try {
        var minifiedFile = htmlMinifier.minify(file, {
            collapseWhitespace: true,
            caseSensitive: true,
            removeComments: true,
            removeRedundantAttributes: true
        });
        cb(null, minifiedFile);
    }
    catch (err) {
        cb(err);
    }
}
