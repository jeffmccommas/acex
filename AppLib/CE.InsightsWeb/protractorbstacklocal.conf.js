var browserstack = require('browserstack-local');

exports.config = {
    'allScriptsTimeout': 30000,
    'specs': ['./test/e2e/home/testbrowsstack.js'],
    'seleniumAddress': 'http://hub-cloud.browserstack.com/wd/hub',

    'capabilities': {
            'browserstack.user': 'muazzamali3',
            'browserstack.key': 'sJxVt5NE5DjkxQsfxBiE',
            'browserstack.local': true,
            'browserName': "chrome",
            'browserstack.debug': true,
            'build': '17.09',
            'project': 'acewebsite'
        },


      // Code to start browserstack local before start of test
    beforeLaunch: function () {
        console.log("Connecting local");
        return new Promise(function (resolve, reject) {
            exports.bs_local = new browserstack.Local();
            exports.bs_local.start({ 'key': exports.config.capabilities['browserstack.key'] }, function (error) {
                if (error) return reject(error);
                console.log('Connected. Now testing...');

                resolve();
            });
        });
    },

    // Code to stop browserstack local after end of test
    afterLaunch: function () {
        return new Promise(function (resolve, reject) {
            exports.bs_local.stop(resolve);
        });
    }
};