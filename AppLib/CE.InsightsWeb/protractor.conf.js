var HtmlReporter = require('protractor-beautiful-reporter');
var path = require('path');
exports.config = {
    //seleniumAddress: 'http://localhost:4444/wd/hub',
    //let Protractor start a selenium server and handle (start and shutdown) it automatically, including stopping it once your tests are done. 
    //To do that, simply point to the selenium jar in the protractor config file instead of the address:
    seleniumServerJar: './node_modules/selenium-standalone-jar/bin/selenium-server-standalone-3.0.1.jar',

    // Capabilities to be passed to the webdriver instance.
    multiCapabilities: [
        {
            'browserName': 'chrome',
            'maxInstances': 2,
            shardTestFiles: true
        }
    ],

    allScriptsTimeout: 60000,

    specs: ['./test/e2e/**/*.spec.js'],

    // Set the framework
    framework: 'jasmine',
    onPrepare: function () {
        // Add a screenshot reporter:
        jasmine.getEnv().addReporter(new HtmlReporter({
            preserveDirectory: false,
            takeScreenShotsOnlyForFailedSpecs: true,
            screenshotsSubfolder: 'images',
            jsonsSubfolder: 'jsons',
            baseDirectory: 'reports-tmp',
            pathBuilder: function pathBuilder(spec, descriptions, results, capabilities) {
                // Return '<30-12-2016>/<browser>/<specname>' as path for screenshots:
                // Example: '30-12-2016/firefox/list-should work'.
                var currentDate = new Date(),
                    day = currentDate.getDate(),
                    month = currentDate.getMonth() + 1,
                    year = currentDate.getFullYear();

                var validDescriptions = descriptions.map(function (description) {
                    return description.replace('/', '@');
                });

                return path.join(
                    day + "-" + month + "-" + year,
                    // capabilities.get('browserName'),
                    validDescriptions.join('-'));
            }
        }).getJasmine2Reporter());
    },

    // Options to be passed to Jasmine-node.
    jasmineNodeOpts: {
        showColors: true,
        defaultTimeoutInterval: 30000
    }
};