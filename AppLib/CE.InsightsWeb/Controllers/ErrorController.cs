﻿using System;
using System.Web.Mvc;
using CE.InsightsWeb.Models;

namespace CE.InsightsWeb.Controllers
{
    public class ErrorController : Controller
    {
       
        public ActionResult Index(string errorMessage)
        {
            //Error em = new Error();
            //if (Server.GetLastError() != null)
            //{
            //    em.ErrorMessage = Server.GetLastError().Message;
            //}
            //else
            //{
            //    em.ErrorMessage = errorMessage;
            //}
            return View();
        }

        public ActionResult Error()
        {
            SetupViewData(new ErrorModel
            {
                Message = "Error"
            });
            return View();
        }

        public ActionResult Generic(ErrorModel errorModel)
        {
            SetupViewData(errorModel);
            return View("Error");
        }       

        //called from client side so we can enforce script errors to log and display error page
        [HttpPost]
        public void SubmitScriptError(string errorType, string area, string errorDetails)
        {
            // Just Log the client errors
            Exception exceptionData;
            if (errorDetails != null)
                exceptionData = new Exception(errorType, new Exception(errorDetails));
            else
                exceptionData = new Exception(errorType);

            //GeneralHelper.LogError(exceptionData, area);
        }

        [HttpPost]
        public ActionResult ClientError(string errorType, string area)
        {
            // Currently not Logging the client errors like browser cookie not supported
            ErrorModel err = new ErrorModel();
            err.Message = errorType;
            SetupViewData(err);
            return View("Error");

        }
        private void SetupViewData(ErrorModel err)
        {
            if (err == null)
            {
                err = new ErrorModel();
            }
            ViewData["ErrorMessage"] = err.GetErrorContent(err.Message);
            //Ensure no exception details are displayed on web pages.
            ViewData["ErrorMessageDetails"] = string.Empty;
            ViewData["Title"] = err.Title;

        }
    }
}
