﻿using System;
using System.Text;
using CE.InsightsWeb.Models;
using System.Web.Mvc;

namespace CE.InsightsWeb.Controllers
{
    public class GreenButtonController : Controller
    {
        // GET: Usage
        public ActionResult Index(string customerId, string webToken, string locale)
        {
            return View();
        }

        [HttpGet]
        public ActionResult Download(string tabKey, string parameters, string type, string startDate, string endDate)
        {
            var gbm = new GreenButtonModels();
            var data = Convert.FromBase64String(parameters);
            parameters = Encoding.UTF8.GetString(data);
            startDate = DateTime.Parse(startDate.Replace("?", "")).ToString("yyyy-MM-dd");
            endDate = DateTime.Parse(endDate.Replace("?", "")).ToString("yyyy-MM-dd");
            var gb = gbm.GetGreenButtonData(tabKey, parameters, startDate, endDate, type);

            var cd = new System.Net.Mime.ContentDisposition {FileName = gb.Response.FileName};
            Response.AppendHeader("Content-Disposition", cd.ToString());
            return File(gb.Response.RawBytes, System.Net.Mime.MediaTypeNames.Application.Octet);
        }

        [HttpGet]
        public JsonResult DownloadDetails(string tabKey, string parameters, string type, string startDate, string endDate)
        {
            var gbm = new GreenButtonModels();
            var data = Convert.FromBase64String(parameters);
            parameters = Encoding.UTF8.GetString(data);
            startDate = DateTime.Parse(startDate.Replace("?", "")).ToString("yyyy-MM-dd");
            endDate = DateTime.Parse(endDate.Replace("?", "")).ToString("yyyy-MM-dd");
            return Json(gbm.GetGreenButtonData(tabKey, parameters, startDate, endDate, type), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult Get(string tabKey, string parameters, string type)
        {
            var data = Convert.FromBase64String(parameters);
            parameters = Encoding.UTF8.GetString(data);
            var gbm = new GreenButtonModels();
            return Json(gbm.GetGreenButtonData(tabKey, parameters, "", "", type, true), JsonRequestBehavior.AllowGet);
        }
    }
}