﻿using System;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using CE.InsightsWeb.Models;

namespace CE.InsightsWeb.Controllers
{
    public class ThirdPartyConfirmationController : Controller
    {
        public ActionResult Index(string code = "", string error = "", string state = "")
        {
            var t = new ThirdPartyConfirmationModel();
            if (error.Length > 0)
            {
                t.Error = error;
                t.State = state;
                return View("Error", t);
            }
            else
            {

                var apps = GreenButtonConnectModels.GetRegisteredApplications("tab.thirdpartyselection", HttpContext.Request.Cookies["params"].Value, "thirdpartyselection", GreenButtonConnectModels.GetEnvironment());
                var app = apps.GreenButtonApplicationInformation.FirstOrDefault(a => a.client_id == HttpContext.Request.Cookies["tpid"].Value);

                var bytes = System.Text.ASCIIEncoding.ASCII.GetBytes(app.client_id + ":" + app.client_secret);
                var auth = Convert.ToBase64String(bytes);

                var api = new APIResponseModels();

                var response = api.GetToken(auth, code);
                t.ThirdPartyApplicationUrl = "ThirdPartyApplication";
                t.State = state;
                return View("Success", t);
            }
        }

    }

    public class ThirdPartyConfirmationModel
    {
        public string Error { get; set; }
        public string State { get; set; }
        public string ThirdPartyApplicationUrl { get; set; }
    }
  
}
