﻿using System.Web.Mvc;
using CE.InsightsWeb.Common;
using CE.InsightsWeb.Models;
using Newtonsoft.Json;

namespace CE.InsightsWeb.Controllers
{
    public class BillComparisonController : Controller
    {

        public ActionResult Index(string customerId, string webToken, string locale, string compareTo)
        {
            ViewBag.CompareTo = compareTo;
            return View();
        }

        [HttpPost]
        public JsonResult Get(string tabKey, string parameters, string fromDate, string toDate, string type)
        {
            var bm = new BillModels();

            switch (type)
            {
                case "previousbill":
                    return Json(JsonConvert.SerializeObject(bm.GetBillComparison(tabKey, parameters, null, null)), JsonRequestBehavior.AllowGet);
                case "samemonthlastyear":
                    return Json(JsonConvert.SerializeObject(bm.GetBillComparison(tabKey, parameters, null, null, Enums.BillComparisonType.SameMonthLastYear)), JsonRequestBehavior.AllowGet);
                case "mostexpensivetoleastexpensive":
                    return Json(JsonConvert.SerializeObject(bm.GetBillComparison(tabKey, parameters, null, null, Enums.BillComparisonType.MostExpensiveToLeastExpensive)), JsonRequestBehavior.AllowGet);
            }

            return Json(JsonConvert.SerializeObject(bm.GetBillComparison(tabKey, parameters, fromDate, toDate)), JsonRequestBehavior.AllowGet);
        }
    }
}