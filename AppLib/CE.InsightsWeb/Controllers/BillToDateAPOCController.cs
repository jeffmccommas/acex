﻿using System.Web.Mvc;
using CE.InsightsWeb.Models;

namespace CE.InsightsWeb.Controllers
{
    public class BillToDateAPOCController : Controller
    {
        public ActionResult Index(string customerId, string webToken, string locale)
        {
            return View();
        }

        public JsonResult Get(string tabKey, string parameters)
        {
            var btd = new BillModels();
            return Json(btd.GetBillToDate(tabKey, parameters), JsonRequestBehavior.AllowGet);
        }
    }
}