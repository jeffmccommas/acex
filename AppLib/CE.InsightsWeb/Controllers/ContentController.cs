﻿using System.Web.Mvc;
using CE.InsightsWeb.Models;
using Newtonsoft.Json;

namespace CE.InsightsWeb.Controllers
{
    public class ContentController : Controller
    {
        public ActionResult Index(string customerId, string webToken, string locale, string instance)
        {
            ViewBag.Instance = instance;
            return View();
        }

        [HttpPost]
        public JsonResult Get(string tabKey, string parameters, string instance)
        {
            var s = new ContentModels();
            return Json(JsonConvert.SerializeObject(s.GetContent(tabKey, parameters, instance)), JsonRequestBehavior.AllowGet);
        }
    }
}