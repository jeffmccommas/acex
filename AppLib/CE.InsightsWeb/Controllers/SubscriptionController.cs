﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;
using CE.InsightsWeb.Models;
using Newtonsoft.Json;

namespace CE.InsightsWeb.Controllers
{
    public class SubscriptionController : Controller
    {
        public ActionResult Index(string customerId, string webToken, string locale)
        {
            return View();
        }

        [HttpGet]
        public JsonResult Get(string tabKey, string parameters)
        {
            byte[] data = Convert.FromBase64String(parameters);
            parameters = Encoding.UTF8.GetString(data);
            var s = new SubscriptionModels();
            return Json(s.GetSubscription(tabKey, parameters), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Post(string tabKey,string parameters, List<Notification> notifications)
        {
            var s = new SubscriptionModels();
            return Json(s.PostSubscription(tabKey, parameters, notifications), JsonRequestBehavior.AllowGet);
        }
    }

}