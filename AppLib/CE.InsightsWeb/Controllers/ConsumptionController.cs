﻿using System;
using System.Text;
using System.Web.Mvc;
using CE.InsightsWeb.Models;

namespace CE.InsightsWeb.Controllers
{
    public class ConsumptionController : Controller
    {

        private const string RESOLUTIONKEY_MONTH = "month";
        private const string RESOLUTIONKEY_DAY = "day";
        private const string RESOLUTIONKEY_HOUR = "hour";
        private const string RESOLUTIONKEY_30 = "halfhour";
        private const string RESOLUTIONKEY_15 = "fifteen";
        private const string RESOLUTIONKEY_5 = "five";

        // GET: Usage
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet] 
        public JsonResult Get(string tabKey, string parameters, string startDate, string endDate, string resolution, string prevResolution, string commodity, string offset, bool paging, bool drilling)
        {
            byte[] data = Convert.FromBase64String(parameters);
            parameters = Encoding.UTF8.GetString(data);
            int os = 0;

            if (string.IsNullOrEmpty(offset))
            {
                offset = "0";
            }

            os = Convert.ToInt16(offset);

            if (resolution == "hour")
            {
                os = Convert.ToInt16(offset);
            }
            else if (resolution == "halfhour")
            {
                os = Convert.ToInt16(offset) / 2;
            }
            else if (resolution == "fifteen")
            {
                os = Convert.ToInt16(offset) / 4;
            }
            else if (resolution == "five")
            {
                os = Convert.ToInt16(offset) / 12;
            }

            if (string.IsNullOrEmpty(resolution))
            {
                resolution = "month";
            }

            if (string.IsNullOrEmpty(startDate) || string.IsNullOrEmpty(endDate) || startDate == "null" || endDate == "null" )
            {
                if (resolution == RESOLUTIONKEY_MONTH)
                {
                    startDate = DateTime.Now.AddYears(-2).AddMonths(1).ToString("yyyy-MM-dd");
                    endDate = DateTime.Now.ToString("yyyy-MM-dd");
                }
                else if (resolution == RESOLUTIONKEY_DAY)
                {
                    endDate = DateTime.Now.Date.ToString();
                    startDate = Convert.ToDateTime(endDate).AddDays(-365).ToString();
                }
                else if (resolution == RESOLUTIONKEY_HOUR)
                {
                    DateTime ed = DateTime.Now.Date;
                    TimeSpan ts = new TimeSpan(23, 00, 0);
                    endDate = (ed.Date + ts).ToString();
                    startDate = Convert.ToDateTime(endDate).AddDays(-365).ToString();
                }
                else if (resolution == RESOLUTIONKEY_30)
                {
                    DateTime ed = DateTime.Now.Date;
                    TimeSpan ts = new TimeSpan(23, 30, 0);
                    endDate = (ed.Date + ts).ToString();
                    startDate = Convert.ToDateTime(endDate).AddDays(-365).ToString();
                }
                else if (resolution == RESOLUTIONKEY_15)
                {
                    DateTime ed = DateTime.Now.Date;
                    TimeSpan ts = new TimeSpan(23, 45, 0);
                    endDate = (ed.Date + ts).ToString();
                    startDate = Convert.ToDateTime(endDate).AddDays(-365).ToString();
                }
                else if (resolution == RESOLUTIONKEY_5)
                {
                    DateTime ed = DateTime.Now.Date;
                    TimeSpan ts = new TimeSpan(23, 55, 0);
                    endDate = (ed.Date + ts).ToString();
                    startDate = Convert.ToDateTime(endDate).AddDays(-365).ToString();
                }
            }
            else //this code handles a change of resoultion while paged to an earlier time period OR a change in time period when the pager is clicked
            {
                if (resolution == RESOLUTIONKEY_MONTH)
                {
                    startDate = DateTime.Now.AddYears(-2).AddMonths(1).ToString("yyyy-MM-dd");
                    endDate = DateTime.Now.ToString("yyyy-MM-dd");
                }
                else if (resolution == RESOLUTIONKEY_DAY)
                {
                    if (paging)
                    {
                        if (os < 0) //paging backwards
                        {

                            endDate = Convert.ToDateTime(startDate).AddDays(-1).ToString();

                            var ed = Convert.ToDateTime(endDate);
                            os = DateTime.DaysInMonth(ed.Year, ed.Month);

                            startDate = Convert.ToDateTime(startDate).AddDays(-os).ToString();
                        }
                        else //paging forwards
                        {
                            startDate = Convert.ToDateTime(endDate).AddDays(1).ToString();

                            var sd = Convert.ToDateTime(startDate);
                            os = DateTime.DaysInMonth(sd.Year, sd.Month);

                            endDate = Convert.ToDateTime(endDate).AddDays(os).ToString();
                        }

                    }
                    else
                    {
                        if (drilling)
                        {
                            var ed = Convert.ToDateTime(endDate);
                            endDate = new DateTime(ed.Year, ed.Month, DateTime.DaysInMonth(ed.Year, ed.Month)).ToString();
                            startDate = new DateTime(ed.Year, ed.Month, 1).ToString();
                        }
                        else
                        {
                            DateTime ed = DateTime.Now.Date; //monthly returns date as first of month so have to set this to today to get any days after the first
                            TimeSpan ts = new TimeSpan(00, 00, 0);
                            endDate = (ed.Date + ts).ToString();
                            startDate = Convert.ToDateTime(endDate).AddDays(-365).ToString();              
                        }

                    }
                }
                else if (resolution == RESOLUTIONKEY_HOUR)
                {

                    if (paging)
                    {
                        if (os < 0) //paging backwards
                        {
                            endDate = Convert.ToDateTime(endDate).AddHours(os).ToString();
                            startDate = Convert.ToDateTime(startDate).AddHours(os).ToString();
                        }
                        else //paging forwards
                        {
                            endDate = Convert.ToDateTime(endDate).AddHours(os).ToString();
                            startDate = Convert.ToDateTime(startDate).AddHours(os).ToString();
                        }

                    }
                    else
                    {
                        DateTime ed = Convert.ToDateTime(endDate);
                        TimeSpan ts = new TimeSpan(23, 00, 0);
                        endDate = (ed + ts).ToString();
                        startDate = Convert.ToDateTime(endDate).AddHours(os + 1).ToString();
                    }
                }
                else if (resolution == RESOLUTIONKEY_30)
                {
                    if (paging)
                    {
                        if (os < 0) //paging backwards
                        {
                            endDate = Convert.ToDateTime(endDate).AddHours(os).ToString();
                            startDate = Convert.ToDateTime(startDate).AddHours(os).ToString();
                        }
                        else //paging forwards
                        {
                            endDate = Convert.ToDateTime(endDate).AddHours(os).ToString();
                            startDate = Convert.ToDateTime(startDate).AddHours(os).ToString();
                        }

                    }
                    else
                    {
                        DateTime ed = Convert.ToDateTime(endDate);
                        TimeSpan ts = new TimeSpan(23, 30, 0);
                        endDate = (ed + ts).ToString();
                        startDate = Convert.ToDateTime(endDate).AddHours(os).AddMinutes(30).ToString();
                    }
                }
                else if (resolution == RESOLUTIONKEY_15)
                {
                    if (paging)
                    {
                        if (os < 0) //paging backwards
                        {
                            endDate = Convert.ToDateTime(endDate).AddHours(os).ToString();
                            startDate = Convert.ToDateTime(startDate).AddHours(os).ToString();
                        }
                        else //paging forwards
                        {
                            endDate = Convert.ToDateTime(endDate).AddHours(os).ToString();
                            startDate = Convert.ToDateTime(startDate).AddHours(os).ToString();
                        }

                    }
                    else
                    {
                        DateTime ed = Convert.ToDateTime(endDate);
                        TimeSpan ts = new TimeSpan(23, 45, 0);
                        endDate = (ed + ts).ToString();
                        startDate = Convert.ToDateTime(endDate).AddHours(os).AddMinutes(15).ToString();
                    }
                }
                else if (resolution == RESOLUTIONKEY_5)
                {
                    if (paging)
                    {
                        if (os < 0) //paging backwards
                        {
                            endDate = Convert.ToDateTime(endDate).AddHours(os).ToString();
                            startDate = Convert.ToDateTime(startDate).AddHours(os).ToString();
                        }
                        else //paging forwards
                        {
                            endDate = Convert.ToDateTime(endDate).AddHours(os).ToString();
                            startDate = Convert.ToDateTime(startDate).AddHours(os).ToString();
                        }

                    }
                    else
                    {
                        DateTime ed = Convert.ToDateTime(endDate);
                        TimeSpan ts = new TimeSpan(23, 55, 0);
                        endDate = (ed + ts).ToString();
                        startDate = Convert.ToDateTime(endDate).AddHours(os).AddMinutes(5).ToString();
                    }
                }
            }

            var c = new ConsumptionModels();
            return Json(c.GetConsumption(tabKey, parameters, resolution, startDate, endDate, commodity, paging, drilling), JsonRequestBehavior.AllowGet);

        }
    }
}