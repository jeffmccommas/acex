﻿using System;
using System.Text;
using System.Web.Mvc;
using CE.InsightsWeb.Models;


namespace CE.InsightsWeb.Controllers
{
    /// <summary>
    /// Controller for Report List
    /// </summary>
    public class ReportListController : Controller
    {
        public ActionResult Index()
        {
            return null;
        }

        [HttpGet]
        public JsonResult GetReportList(string tabKey, string parameters)
        {
            byte[] data = Convert.FromBase64String(parameters);
            parameters = Encoding.UTF8.GetString(data);
            return Json(ReportModels.GetReportsData(tabKey,parameters), JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public ActionResult DownLoadReport(string tabKey, string parameters, int reportId)
        {
            byte[] data = Convert.FromBase64String(parameters);
            parameters = Encoding.UTF8.GetString(data);
            var response = ReportModels.GetReportData(parameters, reportId);
            var cd = new System.Net.Mime.ContentDisposition { FileName = response.FileName };
            Response.AppendHeader("Content-Disposition", cd.ToString());
            return File(response.RawBytes, System.Net.Mime.MediaTypeNames.Application.Octet);
        }

        [HttpPost]
        public JsonResult DeleteReport(string tabKey, string parameters, int reportId)
        {
            byte[] data = Convert.FromBase64String(parameters);
            parameters = Encoding.UTF8.GetString(data);
            var response=ReportModels.DeleteReport(parameters,reportId);
            //Response.AddHeader("Content-Disposition",response.Content);
            return Json(response, JsonRequestBehavior.AllowGet);

        }
    }
}