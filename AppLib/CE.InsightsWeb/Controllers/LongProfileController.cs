﻿using System;
using System.Text;
using System.Web.Mvc;
using CE.InsightsWeb.Models;

namespace CE.InsightsWeb.Controllers
{
    public class LongProfileController : Controller
    {
        [HttpGet]
        public JsonResult GetProfile(string tabKey, string profileTabKey, string parameters)
        {
            byte[] data = Convert.FromBase64String(parameters);
            parameters = Encoding.UTF8.GetString(data);
            var p = new ProfileModels();
            return Json(p.GetLongProfile(tabKey, profileTabKey, parameters.Trim()), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult PostProfile(Models.Profile profile, string parameters, string tabKey, string sectionKey)
        {
            var p = new ProfileModels();
            p.PostProfile(profile, parameters, tabKey, sectionKey);
            return Json("", JsonRequestBehavior.AllowGet);
        }
    }
}