﻿using System;
using System.Text;
using System.Web.Mvc;
using CE.InsightsWeb.Models;

namespace CE.InsightsWeb.Controllers
{
    public class ThirdPartySelectionController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult Get(string tabKey, string parameters)
        {
            var data = Convert.FromBase64String(parameters);
            parameters = Encoding.UTF8.GetString(data);
            return Json(GreenButtonConnectModels.GetRegisteredApplications(tabKey, parameters, "thirdpartyselection", GreenButtonConnectModels.GetEnvironment()), JsonRequestBehavior.AllowGet);
        }

    }
}