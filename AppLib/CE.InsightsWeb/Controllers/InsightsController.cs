﻿using System.Web.Mvc;
using CE.InsightsWeb.Models;
using Newtonsoft.Json;

namespace CE.InsightsWeb.Controllers
{
    public class InsightsController : Controller
    {
        // GET: Usage
        public ActionResult Index(string customerId, string webToken, string locale)
        {
            return View();
        }

        [HttpPost]
        public JsonResult Get(string tabKey, string parameters)
        {
            var inm = new InsightsModels();
            return Json(JsonConvert.SerializeObject(inm.GetInsights(tabKey, parameters)), JsonRequestBehavior.AllowGet);

        }

    }

}