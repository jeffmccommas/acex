﻿using System;
using System.Linq;
using System.Text;
using System.Web;
using CE.InsightsWeb.Models;
using System.Web.Mvc;
using CE.GreenButtonConnect;

namespace CE.InsightsWeb.Controllers
{
    public class GreenButtonConnectController : Controller
    {
        // GET: Usage
        public ActionResult Index(string customerId, string webToken, string locale)
        {
            return View();
        }
        public ActionResult Admin()
        {
            return View("Admin");
        }

        [HttpGet]
        public JsonResult Register(string tabKey, string parameters)
        {
            var data = Convert.FromBase64String(parameters);
            parameters = Encoding.UTF8.GetString(data);
            var success = true;
            //draw register page, get content
            var tt = GreenButtonConnectModels.RegisterApplication("87", GreenButtonConnectModels.GetEnvironment(), out success);

            var gb = new application_information
            {
                dataCustodianId = "87",
                dataCustodianApplicationStatus = 1,
                dataCustodianScopeSelectionScreenURI = "",
                dataCustodianResourceEndpoint = "",
                dataCustodianBulkRequestURI = "",
                thirdPartyNotifyUri = "",
                thirdPartyScopeSelectionScreenURI = "",
                thirdPartyApplicationStatus = (int)Helper.ThirdPartyApplicationStatus.Development,
                authorizationServerAuthorizationEndpoint = "",
                authorizationServerTokenEndpoint = "",
                client_name = "",
                redirect_uri = "",
                software_id = "",
                software_version = "",
                token_endpoint_auth_method = "",
                registration_client_uri = ""
            };

            tt.GreenButtonApplicationInformation.Clear();
            tt.GreenButtonApplicationInformation.Add(gb);

            //Create client
            //var tt1 = inw.RegisterApplication(tabKey, parameters, tt.GreenButtonApplicationInformation.FirstOrDefault());

            return Json("", JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetClientAuthCode(string tabKey, string parameters)
        {
            var data = Convert.FromBase64String(parameters);
            parameters = Encoding.UTF8.GetString(data);
            //Create auth code
            //var tt2 = gbcm.IssueClientAuthCode("3ed9e53c-acea-4553-bb3f-5d105d104b2b", parameters);

            return Json("", JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult DisconnectThirdParty(string thirdPartyId, string parameters)
        {
            var data = Convert.FromBase64String(parameters);
            parameters = Encoding.UTF8.GetString(data);
           
            return Json(GreenButtonConnectModels.DisconnectThirdpartyApp(thirdPartyId, parameters), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetRegisteredApplications(string tabKey, string parameters)
        {   
            var data = Convert.FromBase64String(parameters);
            parameters = Encoding.UTF8.GetString(data);

            //write cookie
            var c = new HttpCookie("params")
            {
                Value = parameters,
                Expires = DateTime.MaxValue
            };
            HttpContext.Response.Cookies.Add(c);
            return Json(GreenButtonConnectModels.GetRegisteredApplications(tabKey, parameters, "thirdpartyselection", GreenButtonConnectModels.GetEnvironment()), JsonRequestBehavior.AllowGet);
        }


    }
}



