﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.SessionState;
using CE.Dashboard.LayoutFramework;
using CE.InsightsWeb.Common.Base;
using CE.InsightsWeb.Common.QueryStringEncryptor;
using Newtonsoft.Json;
using CE.InsightsWeb.Common;
using CE.InsightsWeb.Models;

namespace CE.InsightsWeb.Controllers
{
    [SessionState(SessionStateBehavior.Disabled)]
    public class WidgetController : IdentifiedBase
    {

        public ActionResult Index(string id)
        {
            if (Request[Constants.kPremiseType] == null || Request[Constants.kPremiseType].Length == 0)
            {
                var pm = new ProfileModels();
                var profileResponse = pm.GetProfileAttributesFromCache(Parameters);
                var ca = Dashboard.LayoutFramework.Layout.CombineAttributes(profileResponse);

                var premType = ca.FirstOrDefault(pa => pa.Key == "premisetype");

                if (premType != null)
                {
                    Parameters.pt = premType.Answer;
                }
            }

            var wi = new WidgetInfo(Parameters.cl, id.Substring(0, id.IndexOf("-", StringComparison.Ordinal)), id.Substring(id.IndexOf("-", StringComparison.Ordinal) + 1),
                Parameters.l)
            {
                Parameters = QueryStringModule.Encrypt(JsonConvert.SerializeObject(Parameters)).Substring(5)
            };
            return View(wi);
        }

    }
}