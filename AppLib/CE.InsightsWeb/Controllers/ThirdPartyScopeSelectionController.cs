﻿using System;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CE.InsightsWeb.Models;
using CE.InsightsWeb.Common;

namespace CE.InsightsWeb.Controllers
{
    public class ThirdPartyScopeSelectionController : Controller
    {
        public ActionResult Index(string dataCustodianId, string[] scope, string tpid)
        {

            var scopes = String.Join(" ", scope);
            //write cookies, for simulation only

            var d = new HttpCookie("dataCustodianId")
            {
                Value = dataCustodianId,
                Expires = DateTime.MaxValue
            };
            HttpContext.Response.Cookies.Add(d);

            var c = new HttpCookie("scopes")
            {
                Value = HttpUtility.UrlEncode(scopes),
                Expires = DateTime.MaxValue
            };
            HttpContext.Response.Cookies.Add(c);

            var t = new HttpCookie("tpid")
            {
                Value = tpid,
                Expires = DateTime.MaxValue
            };
            HttpContext.Response.Cookies.Add(t);


            return View();
        }

        [HttpGet]
        public JsonResult Get()
        {
            //third party would look up in their own application_information table
            //the authorizeurl
            var apps = GreenButtonConnectModels.GetRegisteredApplications("tab.thirdpartyselection", HttpContext.Request.Cookies["params"].Value, "thirdpartyselection", GreenButtonConnectModels.GetEnvironment());
            var app = apps.GreenButtonApplicationInformation.Where(a => a.client_id == HttpContext.Request.Cookies["tpid"].Value).FirstOrDefault();

            var authorizeLink = "";
            if (app.environment_type.ToLower() == Enums.EnvironmentType.Internal.ToString().ToLower())
            {
                authorizeLink = ConfigurationManager.AppSettings.Get("CEInsightsWebBaseURL");
            }

            authorizeLink +=
            app.authorizationServerAuthorizationEndpoint;
            authorizeLink += "?response_type=code";
            authorizeLink += "&client_id=" + app.client_id;
            authorizeLink += "&redirect_uri=" + app.redirect_uri;
            authorizeLink += "&scope=" + HttpUtility.UrlDecode(HttpContext.Request.Cookies["scopes"].Value);
            authorizeLink += "&state=blissful";

            var tpss = new ThirdPartyScopeSelectionModel();
            tpss.AuthorizeUri = authorizeLink;
            tpss.LogoUri = app.logo_uri;
            return Json(tpss, JsonRequestBehavior.AllowGet);
        }

        //////redirect to data custodian page, this is a simulation, hardcoded url, thirdpartyid and scopes
        ////[HttpPost]
        ////public ActionResult RedirectToDataCustodianScopeSelection()
        ////{
        ////    return RedirectToAction("Index", "DataCustodianScopeSelection", new { ThirdPartyID = "tp123", scope = "99" });
        ////}

    }

    public class ThirdPartyScopeSelectionModel
    {
        public string AuthorizeUri { get; set; }
        public string LogoUri { get; set; }
    }
  
}