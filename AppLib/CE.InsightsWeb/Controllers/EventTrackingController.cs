﻿using System.Collections.Generic;
using System.Web.Mvc;
using CE.InsightsWeb.Models;

namespace CE.InsightsWeb.Controllers
{
    public class EventTrackingController : Controller
    {       
        [HttpPost]
        public JsonResult PostEvents(Dictionary<string, string> eventInfo, string parameters, string eventType)
        {
            var e = new EventTrackingModels();
            return Json(e.PostEvents(eventInfo, parameters, eventType), JsonRequestBehavior.AllowGet);
        }

    }
}