﻿using System.Web.Mvc;
using CE.InsightsWeb.Models;
using System;
using System.Text;

namespace CE.InsightsWeb.Controllers
{
    public class PeerComparisonController : Controller
    {
        public ActionResult Index(string customerId, string webToken, string locale)
        {
            return View();
        }

        [HttpGet]
        public JsonResult Get(string tabKey, string parameters)
        {
            byte[] data = Convert.FromBase64String(parameters);
            parameters = Encoding.UTF8.GetString(data);
            var bm = new BenchmarkModels();
            return Json(bm.GetBenchmark(tabKey, parameters.Trim()), JsonRequestBehavior.AllowGet);
        }
    }
}