﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;
using System.Web.SessionState;
using CE.InsightsIntegration.Common;
using CE.InsightsIntegration.Models;
using CE.InsightsWeb.Common;
using CE.InsightsWeb.Common.Base;
using CE.InsightsWeb.Common.QueryStringEncryptor;
using CE.InsightsWeb.Helpers;
using CE.InsightsWeb.Models;
using Newtonsoft.Json;
using Layout = CE.Dashboard.LayoutFramework.Layout;

namespace CE.InsightsWeb.Controllers
{
    [SessionState(SessionStateBehavior.Disabled)]
    public class PageController : IdentifiedBase
    {
        private const string AppSettingCeEnvironment = "CEEnvironment";
        private const string AppSettingCeEnvironmentLocal = "localdev";
        private const string AppSettingCeEnvironmentProd = "prod";

        [HttpGet]
        public JsonResult PageLayout(int id)
        {
            if (Request["sid"] != null)
            {
                if (Request["sid"].Length != 0)
                {
                    id = int.Parse(Request["sid"]);
                }
            }
            ViewBag.PageId = id;
            var p = Layout.GetLayout(int.Parse(Parameters.cl), id, Parameters.l, Parameters.c, Parameters.a,
                                        Parameters.p, Parameters.pt, Parameters.l, Parameters.w, Parameters.u, Parameters.g, Parameters.r);

            return Json(p);
        }

        public ActionResult Index(int id)
        {
            //Handle Offline Mode.
            var offlineHelper = new OfflineHelper();
            if (offlineHelper.ThisUserShouldBeOffline)
            {
                switch (OfflineHelper.OfflineMode.ToLower())
                {
                    case "maintenance":
                        return RedirectToAction("Maintenance", "Offline");

                    case "failover":
                        return RedirectToAction("Failover", "Offline");
                }
            }

            if (Request["sid"] != null)
            {
                if (Request["sid"].Length != 0)
                {
                    id = int.Parse(Request["sid"]);
                }
            }

            if (Request[Constants.kPremiseType] == null || Request[Constants.kPremiseType].Length == 0)
            {       
                var pm = new ProfileModels();
                var profileResponse = pm.GetProfileAttributesFromCache(Parameters);
                var ca = Layout.CombineAttributes(profileResponse);

                var premType = ca.FirstOrDefault(pa => pa.Key == "premisetype");

                if (premType != null)
                {
                    Parameters.pt = premType.Answer;
                }
            }
            
            ViewBag.PageId = id;
            var p = Layout.GetLayout(int.Parse(Parameters.cl), id, Parameters.l, Parameters.c, Parameters.a, Parameters.p, Parameters.pt, Parameters.l, Parameters.w, Parameters.u, Parameters.g, Parameters.r);
            p.ClientId = Parameters.cl;
            p.CustomerId = Parameters.c;
            p.AccountId = Parameters.a;
            p.PremiseId = Parameters.p;
            p.PremiseType = Parameters.pt;
            p.ServicePointId = Parameters.s;
            p.WebToken = Parameters.w;
            p.Locale = Parameters.l;
            p.Browser = GeneralHelper.GetBrowser();
            p.IsCsr = ViewPageExtensions.ValidateCsr(Parameters.u, Parameters.g, Parameters.r, Parameters.w);
            if (HttpRuntime.Cache[Parameters.cl + "" + Parameters.c + "" + Parameters.a + "" + Parameters.p + "" + Parameters.pt + "" + Parameters.s + "" + Parameters.l + "" + Parameters.w + "" + Parameters.id + "" + Parameters.sid] == null)
            {
                var enc = QueryStringModule.Encrypt(JsonConvert.SerializeObject(Parameters)).Substring(5);
                p.Parameters = enc;
                HttpRuntime.Cache.Insert(Parameters.cl + "" + Parameters.c + "" + Parameters.a + "" + Parameters.p + "" + Parameters.pt + "" + Parameters.s + "" + Parameters.l + "" + Parameters.w + "" + Parameters.id + "" + Parameters.sid, enc, null, DateTime.Now.AddMinutes(Constants.kCache_Expiry), Cache.NoSlidingExpiration, CacheItemPriority.NotRemovable, null);
            }
            else
            {
                p.Parameters = HttpRuntime.Cache[Parameters.cl + "" + Parameters.c + "" + Parameters.a + "" + Parameters.p + "" + Parameters.pt + "" + Parameters.s + "" + Parameters.l + "" + Parameters.w + "" + Parameters.id + "" + Parameters.sid].ToString();
            }

            p.DeepLinkData = "";
            p.DeepLinkType = "";
            p.DeepLinkWidget = "";
            p.DeepLinkWidgetState = "";
            p.DeepLinkWidgetTab = "";
            p.DeepLinkWidgetSection = "";
            p.DeepLinkEnum = "";
            p.DeepLinkActionKey = "";
            p.DeepLinkResolution = "";

            foreach (var key in Request.QueryString.AllKeys)
            {
                switch (key)
                {
                    case "dltp":
                        p.DeepLinkType = Request.QueryString[key];
                        break;
                    case "dlw":
                        p.DeepLinkWidget = Request.QueryString[key];
                        break;
                    case "dlwst":
                        p.DeepLinkWidgetState = Request.QueryString[key];
                        break;
                    case "dlwt":
                        p.DeepLinkWidgetTab = Request.QueryString[key];
                        break;
                    case "dlws":
                        p.DeepLinkWidgetSection = Request.QueryString[key];
                        break;
                    case "enum":
                        p.DeepLinkEnum = Request.QueryString[key];
                        break;
                    case "dlakey":
                        p.DeepLinkActionKey = Request.QueryString[key];
                        break;
                    case "dlres":
                        p.DeepLinkResolution = Request.QueryString[key];
                        break;
                    case "dlcmp":
                        p.DeepLinkComparison = Request.QueryString[key];
                        break;
                }
            }
           
            p.RemoteHeaderEncryptedParams = ViewPageExtensions.EncryptParams(Parameters);
            var v = View(p);
            if (Request.QueryString["tmpl"] == "gbc")
            {
                v.MasterName = "~/Views/Shared/_GreenButtonConnect.cshtml";
            }
            else if (Request.QueryString["tmpl"] == "rc")
            {
                v.MasterName = "~/Views/Shared/_LayoutRemote.cshtml";
            }
            else
            {
                v.MasterName = "~/Views/Shared/_Layout.cshtml";
            }

            return v;
        }

        [HttpGet]
        public JsonResult PostToken(string customerId, int clientId, string accountId,string premiseId, string username, string password, bool clearProfileAttr, bool csrLogin, string userId, string groupName, string roleName)
        {
            var s = ConfigurationManager.AppSettings.Get(AppSettingCeEnvironment);
            
            //declare the transaction options
            var transactionOptions = new System.Transactions.TransactionOptions
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            };
            //set it to read uncommited

            if (!string.IsNullOrEmpty(s))
            {
                if (s != AppSettingCeEnvironmentLocal)
                {
                    using (var transScope =
                            new System.Transactions.TransactionScope(
                                System.Transactions.TransactionScopeOption.Required, transactionOptions))
                    {
                        using (var ent = new InsightsEntities())
                        {
                            ent.Database.Connection.Open();

                            var client = (from ap in ent.ControlPanelLogins
                                          join cplc in ent.ControlPanelLoginClients on ap.CpID equals cplc.CpID
                                          where ap.UserName == username && cplc.ClientID == clientId && ap.EnableInd
                                          select ap).FirstOrDefault();
                            var encryptor = new Encryptor(password);

                            if (client == null ||
                                !encryptor.VerifyHash(password, client.PasswordSalt, client.PasswordHash))
                            {
                                var r = new Response
                                {
                                    StatusCode = 400,
                                    StatusDescription = "Invalid user/password or this user doesn't have access"
                                };
                                return Json(r, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {                                          
                                var c = new HttpCookie("GBC")
                                {
                                    Value = clientId + ":" + client.SuperUserInd.ToString(),
                                    Expires = DateTime.MaxValue
                                };
                                HttpContext.Response.Cookies.Add(c);
                            }
                        }
                        transScope.Complete();
                    }
                }
            }
            if (s != AppSettingCeEnvironmentProd)
            {
                if (clearProfileAttr && !string.IsNullOrEmpty(customerId) && !string.IsNullOrEmpty(accountId) &&
                    !string.IsNullOrEmpty(premiseId) && clientId > 0)
                {
                    using (var transactionScope =
                            new System.Transactions.TransactionScope(
                                System.Transactions.TransactionScopeOption.Required,
                                transactionOptions))
                    {
                        using (var ent = new InsightsEntities())
                        {
                            ent.Database.Connection.Open();
                            var dbContextTransaction = ent.Database.BeginTransaction();

                            try
                            {
                                ent.Configuration.AutoDetectChangesEnabled = false;
                                ent.Configuration.ValidateOnSaveEnabled = false;

                                const string sqlPremise = "Delete from wh.ProfilePremiseAttribute where clientid = @ClientId and accountid = @AccountId and premiseid = @PremiseId " +
                                                          "and attributekey not in ('demoscenario','premisetype','customer.segment') " +
                                                          "and attributekey not like '%enrollmentstatus' " +
                                                          "and attributekey not like '%channel' " +
                                                          "and attributekey not like '%groupnumber'";

                                var parameterPremiseList = new List<SqlParameter>
                                {
                                    new SqlParameter("@ClientId", clientId),
                                    new SqlParameter("@AccountId", accountId),
                                    new SqlParameter("@PremiseId", premiseId)
                                };

                                SqlParameter[] parametersPremise = parameterPremiseList.ToArray();
                                ent.Database.ExecuteSqlCommand(sqlPremise, parametersPremise);

                                const string sqlAccount = "Delete from wh.ProfileAccountAttribute where clientid = @ClientId and accountid = @AccountId";
                                var parameterAccountList =
                                    new List<SqlParameter>
                                    {
                                        new SqlParameter("@ClientId", clientId),
                                        new SqlParameter("@AccountId", accountId)
                                    };

                                SqlParameter[] parametersAccount = parameterAccountList.ToArray();
                                ent.Database.ExecuteSqlCommand(sqlAccount, parametersAccount);

                                // Check if attr exist for other account premise combination for that customer
                                const string sqlAcctCount = "select count(*) from wh.profileaccountattribute pa " +
                                                            "inner join wh.profilecustomeraccount pc on pa.accountid = pc.accountid and pa.clientid = pc.clientid " +
                                                            "where pc.clientid = @ClientId and pc.customerid = @CustomerId";
                                var parameterAccountCount =
                                    new List<SqlParameter>
                                    {
                                        new SqlParameter("@ClientId", clientId),
                                        new SqlParameter("@CustomerId", customerId)
                                    };

                                SqlParameter[] parameterAcctCount = parameterAccountCount.ToArray();
                                var acctCount = ent.Database.SqlQuery<int>(sqlAcctCount, parameterAcctCount).First();

                                const string sqlPremiseCount = "select count(*) from wh.profilepremiseattribute pp " +
                                                               "inner join wh.profilecustomeraccount pc on pp.accountid = pc.accountid and pp.clientid = pc.clientid " +
                                                               "where pc.clientid = @ClientId and pc.customerid = @CustomerId and pp.attributekey != 'firstlogindate'";
                                var parameterPremiseCount =
                                    new List<SqlParameter>
                                    {
                                        new SqlParameter("@ClientId", clientId),
                                        new SqlParameter("@CustomerId", customerId)
                                    };

                                SqlParameter[] parameterPremCount = parameterPremiseCount.ToArray();
                                var premCount = ent.Database.SqlQuery<int>(sqlPremiseCount, parameterPremCount).First();

                                if (acctCount == 0 && premCount == 0)
                                {
                                    const string sqlCustomer = "Delete from wh.ProfileCustomerAttribute where clientid = @ClientId and customerid = @CustomerId";
                                    var parameterCustomerList =
                                        new List<SqlParameter>
                                        {
                                            new SqlParameter("@ClientId", clientId),
                                            new SqlParameter("@CustomerId", customerId)
                                        };

                                    SqlParameter[] parameterCustomer = parameterCustomerList.ToArray();
                                    ent.Database.ExecuteSqlCommand(sqlCustomer, parameterCustomer);
                                }
                                
                                dbContextTransaction.Commit();
                            }
                            catch (Exception)
                            {
                                dbContextTransaction.Rollback();
                            }
                        }
                        transactionScope.Complete();
                    }
                }
            }

            var rm = new APIResponseModels();
            string postWebTokenStr;
            if (csrLogin)
            {
                postWebTokenStr = @"{ " +
                                     "\"CustomerId\": \"" + customerId + "\"," +
                                     "\"AdditionalInfo\": \"test\"," +
                                     "\"UserId\": \"" + userId + "\"," +
                                     "\"GroupName\": \"" + groupName + "\"," +
                                     "\"RoleName\": \"" + roleName + "\"" +
                                     "}";
            }
            else
            {
                 postWebTokenStr = @"{ " +
                                      "\"CustomerId\": \"" + customerId + "\"," +
                                      "\"AdditionalInfo\": \"test\"" +
                                      "}";
            }
           
            return Json(rm.PostWebToken(postWebTokenStr, "en-US", clientId), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult AnonymousPostToken(int clientId, string email, string zipcode)
        {
          var   postWebTokenStr = @"{ " +
                                    "\"EmailAddress\": \"" + email + "\"," +
                                    "\"ZipCode\": \"" + zipcode + "\"," +
                                    "}";
            var rm = new APIResponseModels();
            return Json(rm.PostWebToken(postWebTokenStr, "en-US", clientId), JsonRequestBehavior.AllowGet);
        }
    }
}