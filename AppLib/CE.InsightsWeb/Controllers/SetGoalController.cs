﻿using System.Web.Mvc;
using CE.InsightsWeb.Models;

namespace CE.InsightsWeb.Controllers
{
    public class SetGoalController : Controller
    {
        // GET: TestWidget
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetGoal(string tabKey, string parameters, bool getOptions)
        {
            var sgm = new SavingsGoalModels();
            return Json(sgm.GetSavingsGoal(tabKey, parameters, getOptions), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult PostGoal(Goal goal, string parameters)
        {
            var sgm = new SavingsGoalModels();
            sgm.PostSavingsGoal(goal, parameters);
            return Json("", JsonRequestBehavior.AllowGet);
        }
    }
}