﻿using CE.InsightsWeb.Common;
using CE.InsightsWeb.Models;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;

namespace CE.InsightsWeb.Controllers
{
    public class AuthorizeController : Controller
    {
        public ActionResult Index(string client_id, string redirect_uri, string scope, string response_type, string state)
        {
            //client_id is the third party clientid
            //redirect_uri is where we redirect back to on the tp side after the customer is done with accept/deny
            //response_type should be "code", validate this
            //scope, the agreed upon scopes, should not contain anything the customer did not accept on our scope selection page
            //state should be sent back to tp

            //get third party info based on client_id (thirdpartyid) 
            var apps = GreenButtonConnectModels.GetRegisteredApplications("tab.thirdpartyselection", HttpContext.Request.Cookies["params"].Value, "thirdpartyselection", GreenButtonConnectModels.GetEnvironment());
            var app = apps.GreenButtonApplicationInformation.FirstOrDefault(a => a.client_id == client_id);

            if (app != null && app.environment_type.ToLower() == Enums.EnvironmentType.Internal.ToString().ToLower())
            {
                redirect_uri = ConfigurationManager.AppSettings.Get("CEInsightsWebBaseURL") + redirect_uri;
            }

            var parameters = HttpContext.Request.Cookies["params"].Value;

            if (app!= null)
            {
                var obj = new EventTrackingModels();
                Dictionary<string, string> dictionary = new Dictionary<string, string>();
                dictionary.Add("ThirdPartyAppId", app.application_information_id.ToString());
                var res = obj.PostEvents(dictionary, parameters, "Request Auth Code");
            }

            //attempt to crate auth code
            var auth = GreenButtonConnectModels.IssueClientAuthCode(client_id, parameters, scope, redirect_uri, response_type);

            if (auth.Error != null)
            {
                //redirect to third party with error
                redirect_uri += "?error=" + auth.Error + "&state=" + state;
                return Redirect(redirect_uri);
            }
            //redirect to third party with success
            redirect_uri += "?code=" + System.Web.HttpUtility.UrlEncode(auth.code) + "&state=" + state;
            return Redirect(redirect_uri);
        }

        [Route("oauth/authorize")]
        public ActionResult ExternalAuthorize(string client_id, string redirect_uri, string scope, string response_type, string state)
        {
            return RedirectToAction("Index", "Authorize", new { client_id, redirect_uri, scope, response_type, state });
        }
    }
}