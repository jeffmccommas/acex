﻿using System;
using System.Text;
using System.Web.Mvc;
using CE.InsightsWeb.Models;

namespace CE.InsightsWeb.Controllers
{
    public class CustomerInfoController : Controller
    {
        public ActionResult Index(string customerId, string webToken, string locale)
        {
            return View();
        }

        [HttpGet]
        public JsonResult Get(string tabKey, string parameters)
        {
            var data = Convert.FromBase64String(parameters);
            parameters = Encoding.UTF8.GetString(data);
            return Json(CustomerInfoModels.GetCustomerInfo(tabKey, parameters), JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetAccountPremise(string tabKey, string parameters)
        {
            var data = Convert.FromBase64String(parameters);
            parameters = Encoding.UTF8.GetString(data);
            return Json(CustomerInfoModels.GetAccountPremise(parameters), JsonRequestBehavior.AllowGet);
        }
    }
}