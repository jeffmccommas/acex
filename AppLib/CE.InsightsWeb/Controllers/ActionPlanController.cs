﻿using System.Collections.Generic;
using System.Web.Mvc;
using CE.InsightsWeb.Models;
using Newtonsoft.Json;

namespace CE.InsightsWeb.Controllers
{
    public class ActionPlanController : Controller
    {
        // GET: Usage
        public ActionResult Index(string customerId, string webToken, string locale)
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetActions(string tabKey, string parameters)
        {
            var sgm = new WaysToSaveModels();
            return Json(JsonConvert.SerializeObject(sgm.GetWaysToSave(tabKey, parameters, true)), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult PostActions(List<WTSActionPlan> plan, string parameters)
        {
            var sgm = new WaysToSaveModels();
            return Json(sgm.PostWaysToSave(plan, parameters), JsonRequestBehavior.AllowGet);
        }

    }
  
}