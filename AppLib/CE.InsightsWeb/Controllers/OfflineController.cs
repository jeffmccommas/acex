﻿using System.Web.Mvc;

namespace CE.InsightsWeb.Controllers
{
    public class OfflineController : Controller
    {

        public ActionResult Maintenance()
        {
            return View();
        }

        public ActionResult Failover()
        {
            return View();
        }
    }
}