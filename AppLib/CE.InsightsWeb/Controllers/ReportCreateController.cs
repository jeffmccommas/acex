﻿using System;
using System.Text;
using System.Web.Mvc;
using CE.InsightsWeb.Models;

namespace CE.InsightsWeb.Controllers
{
    public class ReportCreateController : Controller
    {
        // GET: ReportCreate
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult Get(string tabKey, string parameters)
        {
            byte[] data = Convert.FromBase64String(parameters);
            parameters = Encoding.UTF8.GetString(data);
            return Json(ReportModels.GetReportCreate(tabKey, parameters), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult PostReportCreate(string tabkey, string parameters,string file, string title, string notes, string iscsr)
        {
            return Json(ReportModels.PostReport(parameters, title, notes, file, bool.Parse(iscsr)), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetIsCsr(string parameters)
        {
            byte[] data = Convert.FromBase64String(parameters);
            parameters = Encoding.UTF8.GetString(data);
            return Json(ReportModels.IsCSR(parameters), JsonRequestBehavior.AllowGet);
        }
    }
}