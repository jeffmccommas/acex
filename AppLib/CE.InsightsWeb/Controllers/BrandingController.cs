﻿using System.Web.Mvc;
using CE.InsightsWeb.Common;
using CE.InsightsWeb.Helpers;
namespace CE.InsightsWeb.Areas.CEResWeb.Controllers
{
    public class BrandingController : Controller
    {       
        public MvcHtmlString HeaderRemote()
        {
            return GeneralHelper.GetPartialViewHeader(this, Enums.PartialView.HeaderRemote, Request[Constants.kClientId].ToString(), false);
        }

        public MvcHtmlString Header()
        {
            return GeneralHelper.GetPartialView(this, Enums.PartialView.Header, Request[Constants.kClientId].ToString());
        }

        public MvcHtmlString HeaderScripts()
        {
            return GeneralHelper.GetPartialViewHeader(this, Enums.PartialView.HeaderRemote, Request[Constants.kClientId].ToString(), true);
        }

        public MvcHtmlString FooterRemote()
        {
            return GeneralHelper.GetPartialView(this, Enums.PartialView.FooterRemote, Request[Constants.kClientId].ToString());
        }

        public MvcHtmlString Footer()
        {
            return GeneralHelper.GetPartialView(this, Enums.PartialView.Footer, Request[Constants.kClientId].ToString());
        }

        public MvcHtmlString SideNavBegin()
        {
            return GeneralHelper.GetPartialView(this, Enums.PartialView.SideNavBegin, Request[Constants.kClientId].ToString());
        }

        public MvcHtmlString SideNavEnd()
        {
            return GeneralHelper.GetPartialView(this, Enums.PartialView.SideNavEnd, Request[Constants.kClientId].ToString());
        }

        public MvcHtmlString SideNavRemote()
        {
            return GeneralHelper.GetPartialView(this, Enums.PartialView.SideNavRemote, Request[Constants.kClientId].ToString());
        }

        public MvcHtmlString IFrame()
        {
            return GeneralHelper.GetPartialView(this, Enums.PartialView.Frame, Request[Constants.kClientId].ToString());
        }

        //// 13.03 bug 37726 - custom jquery script
        //public MvcHtmlString CustomJQueryScript()
        //{
        //    return GeneralHelper.RegisterAclaraJQueryFrameworkScript((UserSessionVariables)Session[Constants.USER_SESSION_VARIABLE]);
        //}
    }
}
