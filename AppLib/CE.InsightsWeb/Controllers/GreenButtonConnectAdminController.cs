﻿using CE.GreenButtonConnect;
using CE.InsightsWeb.Models;
using System;
using System.Configuration;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace CE.InsightsWeb.Controllers
{
    public class GreenButtonConnectAdminController : Controller
    {
        public ActionResult Index()
        {
            var gbcPrivs = HttpContext.Request.Cookies["GBC"].Value;
            ViewBag.SU = Convert.ToBoolean(gbcPrivs.Split(new char[] { ':' })[1]);
            return View();
        }

        [HttpGet]
        public JsonResult Get()
        {
            var gbcPrivs = HttpContext.Request.Cookies["GBC"].Value;
            var clientId = gbcPrivs.Split(new char[] { ':' })[0];
            return Json(GreenButtonConnectModels.GetAllRegisteredApplications(clientId, GreenButtonConnectModels.GetEnvironment()), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Post(application_information greenButtonApplicationInformation, string scope)
        {
            var gbcPrivs = HttpContext.Request.Cookies["GBC"].Value;
            var clientId = gbcPrivs.Split(new char[] { ':' })[0];
            bool success;
            string environment = GreenButtonConnectModels.GetEnvironment();
            var gba = GreenButtonConnectModels.RegisterApplication(clientId, environment, out success);
            gba.ScopeString = scope;
            gba.GreenButtonApplicationInformation.Clear();
            var api = ConfigurationManager.AppSettings.Get("CEInsightsAPIBaseURL");
            var web = ConfigurationManager.AppSettings.Get("CEInsightsWebBaseURL");

            greenButtonApplicationInformation.authorizationServerAuthorizationEndpoint = web + "/oauth/authorize";
            greenButtonApplicationInformation.authorizationServerTokenEndpoint = api + "/api/v1/oauth/token";
            greenButtonApplicationInformation.dataCustodianResourceEndpoint = api + "/DataCustodian/espi/1_1/resource";
            gba.GreenButtonApplicationInformation.Add(greenButtonApplicationInformation);
            GreenButtonConnectModels.RegisterApplication(clientId, GreenButtonConnectModels.GetEnvironment(),out success, gba);

            if (greenButtonApplicationInformation.dataCustodianApplicationStatus == 4)
            {
                GreenButtonConnectModels.DisconnectThirdparty(greenButtonApplicationInformation.client_id);
            }

            return Json(success, JsonRequestBehavior.AllowGet);
        }
        
        [HttpGet]
        public JsonResult GetNewApp()
        {
            var gbcPrivs = HttpContext.Request.Cookies["GBC"].Value;
            var clientId = gbcPrivs.Split(new char[] { ':' })[0];
            bool success;
            var gba = GreenButtonConnectModels.RegisterApplication(clientId, GreenButtonConnectModels.GetEnvironment(), out success);
            gba.GreenButtonApplicationInformation[0].enabled = false;
            return Json(gba.GreenButtonApplicationInformation[0], JsonRequestBehavior.AllowGet);
        }

    }
}



