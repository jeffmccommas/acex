﻿using System;
using System.Text;
using System.Web.Mvc;
using CE.InsightsWeb.Models;

namespace CE.InsightsWeb.Controllers
{
    public class BillToDateController : Controller
    {
        public ActionResult Index(string customerId, string webToken, string locale)
        {
            return View();
        }
        
        [HttpGet]
        public JsonResult Get(string tabKey, string parameters)
        {
            byte[] data = Convert.FromBase64String(parameters);
            parameters = Encoding.UTF8.GetString(data);
            var btd = new BillModels();
            return Json(btd.GetBillToDate(tabKey, parameters), JsonRequestBehavior.AllowGet);
        }
    }
}