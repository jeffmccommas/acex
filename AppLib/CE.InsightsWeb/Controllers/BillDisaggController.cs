﻿using System.Web.Mvc;
using CE.InsightsWeb.Models;
using System;
using System.Text;

namespace CE.InsightsWeb.Controllers
{
    public class BillDisaggController : Controller
    {

        public static string widgetType = "billdisagg";

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult Get(string tabKey, string parameters)
        {
            byte[] data = Convert.FromBase64String(parameters);
            parameters = Encoding.UTF8.GetString(data);
            var bdm = new BillDisaggModel();
            return Json(bdm.GetBillDisaggData(parameters, tabKey, widgetType), JsonRequestBehavior.AllowGet);

        }

    }
}