﻿using System;
using System.Text;
using System.Web.Mvc;
using CE.InsightsWeb.Models;

namespace CE.InsightsWeb.Controllers
{
    public class ProfileController : Controller
    {
        public ActionResult Index(string customerId, string webToken, string locale)
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetProfile(string tabKey, string parameters)
        {
            byte[] data = Convert.FromBase64String(parameters);
            parameters = Encoding.UTF8.GetString(data);
            var p = new ProfileModels();
            return Json(p.GetProfile(tabKey, parameters.Trim()), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult PostProfile(Profile profile,string parameters )
        {
            var p = new ProfileModels();
            p.PostProfile(profile, parameters);
            return Json("", JsonRequestBehavior.AllowGet);
        }
    }  
}