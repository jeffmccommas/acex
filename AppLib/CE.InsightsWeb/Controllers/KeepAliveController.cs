﻿using System.Web.Mvc;
using CE.InsightsWeb.Models;

namespace CE.InsightsWeb.Controllers
{
    public class KeepAliveController : Controller
    {
        // GET: KeepAlive
        public ActionResult Index()
        {
            var ka = new KeepAliveModels();
            ka.KeepAlive();
            return View(ka);
        }
    }
}