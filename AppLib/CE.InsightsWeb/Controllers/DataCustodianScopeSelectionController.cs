﻿using System;
using System.Text;
using System.Web.Mvc;
using CE.InsightsWeb.Models;

namespace CE.InsightsWeb.Controllers
{
    public class DataCustodianScopeSelectionController : Controller
    {
        public ActionResult Index(string thirdPartyId, string scope)
        {
            return View();
        }

        //call from angular service to get data/content from model
        //not sure if we are using tabs or just common content, parameters may just have thirdPartyId
        //leaving as is for now
        [HttpGet]
        public JsonResult Get(string tabKey, string parameters)
        {
            byte[] data = Convert.FromBase64String(parameters);
            parameters = Encoding.UTF8.GetString(data);
            var pm = new WaysToSaveModels();
            return Json(pm.GetPromos(tabKey, parameters), JsonRequestBehavior.AllowGet);
        }

        //this will be called from link in the page that is built up in the ts
        //need to verify this is a 302 redirect
        [HttpPost]
        public ActionResult RedirectToThirdPartyScopeSelection(string thirdPartyScopeSelectionScreen, string dataCustodianId, string scopes)
        {
            return Redirect(thirdPartyScopeSelectionScreen + "?DataCustodianID=" + "&" + dataCustodianId + "&" + "scope=" + scopes);
        }

    }
  
}