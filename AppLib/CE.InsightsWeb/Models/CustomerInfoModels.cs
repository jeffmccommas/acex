﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.WebPages;
using AutoMapper;
using CE.InsightsWeb.Common;
using CE.InsightsWeb.Common.AclaraJson;
using CE.InsightsWeb.Common.QueryStringEncryptor;
using CE.InsightsWeb.Helpers;
using Newtonsoft.Json;

namespace CE.InsightsWeb.Models
{
    public class CustomerInfoModels
    {
        private const string CustomerInfoCategory = "customerinfo";
        private const string CommonCategory = "common";
        private const string WidgetType = "customerinfo";

        public static CustomerInfo GetCustomerInfo(string tabKey, string parameters)
        {
            var c = new CustomerInfo();
            var jp = (JsonParams)JsonConvert.DeserializeObject(QueryStringModule.Decrypt(parameters), typeof(JsonParams));
            //Content
            var content = CMS.GetContent(int.Parse(jp.cl), jp.l).Content;
            var commonTextContent = content.TextContent.Where(x => x.category == CommonCategory).ToList();
            var useCommonContent = CMS.GetCommonContent(content, "customerinfo.usecommoncontent", "config");

            if (useCommonContent == "true")
            {
                c.CustomerIdLabel = CMS.GetCommonContent(content, "customerinfo.customeridlabel");
                c.AccountNumberLabel = CMS.GetCommonContent(content, "customerinfo.accountnumberlabel");
                c.AddressLabel = CMS.GetCommonContent(content, "customerinfo.addresslabel");
                c.ServiceInfoLabel = CMS.GetCommonContent(content, "customerinfo.serviceinfolabel");
                c.HomeProfileLabel = CMS.GetCommonContent(content, "customerinfo.homeprofilelabel");
                c.QuestionAnsweredText = CMS.GetCommonContent(content, "customerinfo.questionansweredtext");
                c.ProfileLink = CMS.GetCommonContent(content, "customerinfo.profilelink");
                c.MeterLabel = CMS.GetCommonContent(content, "customerinfo.meterlabel");

                c.ShowProfile = CMS.GetCommonContent(content, "customerinfo.showprofile", "config");
                c.ViewLink = GeneralHelper.CreateLink(CMS.GetCommonContent(content, "customerinfo.linktype", "config"), jp, CMS.GetCommonContent(content, "customerinfo.link", "config"));
            }
            else
            {
                var customerInfoTextContent = content.TextContent.Where(x => x.category == CustomerInfoCategory).ToList();
                var customerInfoConfig = content.Configuration.Where(x => x.category == CustomerInfoCategory).ToList();

                //Content
                c.CustomerIdLabel = CMS.GetWidgetContent(customerInfoTextContent, tabKey, WidgetType, "customeridlabel");
                c.AccountNumberLabel = CMS.GetWidgetContent(customerInfoTextContent, tabKey, WidgetType, "accountnumberlabel");
                c.AddressLabel = CMS.GetWidgetContent(customerInfoTextContent, tabKey, WidgetType, "addresslabel");
                c.ServiceInfoLabel = CMS.GetWidgetContent(customerInfoTextContent, tabKey, WidgetType, "serviceinfolabel");
                c.HomeProfileLabel = CMS.GetWidgetContent(customerInfoTextContent, tabKey, WidgetType, "homeprofilelabel");
                c.QuestionAnsweredText = CMS.GetWidgetContent(customerInfoTextContent, tabKey, WidgetType, "questionansweredtext");
                c.ProfileLink = CMS.GetWidgetContent(customerInfoTextContent, tabKey, WidgetType, "profilelink");
                c.MeterLabel = CMS.GetWidgetContent(customerInfoTextContent, tabKey, WidgetType, "meterlabel");

                //Configuration
                c.ShowProfile = CMS.GetWidgetConfig(customerInfoConfig, tabKey, WidgetType, "showprofile");
                c.ViewLink = GeneralHelper.CreateLink(CMS.GetWidgetConfig(customerInfoConfig, tabKey, WidgetType, "linktype"), jp, CMS.GetWidgetConfig(customerInfoConfig, tabKey, WidgetType, "link"));
            }

            var pm = new ProfileModels();
            var profileResponse = pm.GetProfileAttributesFromCache(jp);

            var customerInfoResponse = GetCustomerInfoFromCache(jp);

            if (c.ShowProfile == "true")
            {
                c.QuestionsAnswered = profileResponse.Customer.Accounts[0].Premises[0].Attributes.Where(x => (x.SourceKey == "customer" || x.SourceKey == "csr") && x.AttributeKey != "demoscenario" && x.AttributeKey != "premisetype").Count();
                c.QuestionAnsweredText = c.QuestionAnsweredText.Replace("{%questionanswered%}", c.QuestionsAnswered.ToString());
            }

            Mapper.CreateMap<CustomerInfoPremise, AddressList>();
            var addList = new List<AddressList>();
            var serviceList = new List<ServiceInfo>();
            if (customerInfoResponse.Customer.Accounts == null || customerInfoResponse.Customer.Accounts.Count == 0)
            {
                c.NoAccounts = true;
            }
            else if ((from b in customerInfoResponse.Customer.Accounts where b.Id == jp.a from premise in b.Premises where premise.Id == jp.p select b).ToList().Count == 0)
            {
                c.NoAccounts = true;
            }
            else
            {
                var acctFiltered = customerInfoResponse.Customer.Accounts.Where(x => x.Id == jp.a).FirstOrDefault();
                if (acctFiltered != null)
                {
                    foreach (var prem in acctFiltered.Premises)
                    {
                        var add = new AddressList();
                        Mapper.Map(prem, add);

                        if (prem.Id == jp.p)
                        {
                            add.Selected = true;
                            foreach (var s in prem.Service)
                            {
                                var service = new ServiceInfo
                                {
                                    Id = s.Id,
                                    CommodityKey = s.CommodityKey,
                                    CommodityName = commonTextContent.Where(e => e.key == "commodity." + s.CommodityKey + ".name").Select(x => x.shorttext).FirstOrDefault(),
                                    MeterId = string.Join(",", s.ServicePoints.SelectMany(x => x.Meters).Select(x => x.Id).ToList())
                                };
                                serviceList.Add(service);
                            }
                        }

                        var jp1 = new JsonParams();
                        Mapper.CreateMap<JsonParams, JsonParams>();
                        Mapper.Map(jp, jp1);
                        jp1.p = prem.Id;
                        jp1.s = prem.Service[0].Id;
                        add.Url = GeneralHelper.CreateLink("internal", jp1, "id=" + jp.id + "&sid=" + jp.sid);
                        add.Id = prem.Id;
                        addList.Add(add);
                    }
                    c.AccountId = acctFiltered.Id;
                }
            }
            c.FirstName = customerInfoResponse.Customer.FirstName;
            c.LastName = customerInfoResponse.Customer.LastName;
            c.CustomerId = customerInfoResponse.Customer.Id;
            c.ServiceInfo = serviceList;
            c.Addresses = addList;

            return c;
        }

        public static PremiseSelect GetAccountPremise(string parameters)
        {

            var jp = (JsonParams)JsonConvert.DeserializeObject(QueryStringModule.Decrypt(parameters), typeof(JsonParams));
            //Content
            var content = CMS.GetContent(int.Parse(jp.cl), jp.l);
            var commonTextContent = content.Content.TextContent.Where(x => x.category == CommonCategory).ToList();
            var c = new PremiseSelect(content, "premiseselect", jp)
            {
                AccountNumberLabel = CMS.GetCommonContent(content, "premiseselect.accountnumberlabel"),
                LoadingMessage = CMS.GetCommonContent(content, "premiseselect.loadingmessage"),
                ExpandLabel = CMS.GetCommonContent(content, "widget.expandlabel"),
                CollapseLabel = CMS.GetCommonContent(content, "widget.collapselabel"),
                ShowSinglePremise = Convert.ToBoolean(CMS.GetCommonContent(content, "premiseselect.showsinglepremise", "config"))
            };

            var customerInfoResponse = GetCustomerInfoFromCache(jp);

            if (customerInfoResponse.Customer.Accounts == null || customerInfoResponse.Customer.Accounts.Count == 0)
            {
                c.NoAccounts = true;
            }
            else if ((from b in customerInfoResponse.Customer.Accounts where b.Id == jp.a from premise in b.Premises where premise.Id == jp.p select b).ToList().Count == 0)
            {
                c.NoAccounts = true;
            }
            else
            {
                var accountList = new List<PremiseSelectAccount>();
                Mapper.CreateMap<CustomerInfoAccount, PremiseSelectAccount>();
                Mapper.CreateMap<CustomerInfoPremise, PremiseSelectPremise>()
                    .ForMember(dest => dest.Addr1, opt => opt.MapFrom(src => string.IsNullOrEmpty(src.Addr1) ? string.Empty : src.Addr1))
                    .ForMember(dest => dest.Addr2, opt => opt.MapFrom(src => string.IsNullOrEmpty(src.Addr2) ? string.Empty : src.Addr2))
                    .ForMember(dest => dest.City, opt => opt.MapFrom(src => string.IsNullOrEmpty(src.City) ? string.Empty : src.City))
                    .ForMember(dest => dest.StateProvince, opt => opt.MapFrom(src => string.IsNullOrEmpty(src.StateProvince) ? string.Empty : src.StateProvince))
                    .ForMember(dest => dest.PostalCode, opt => opt.MapFrom(src => string.IsNullOrEmpty(src.PostalCode) ? string.Empty : src.PostalCode));
                Mapper.Map(customerInfoResponse.Customer.Accounts, accountList);

                foreach (var acct in accountList)
                {
                    foreach (var prem in acct.Premises)
                    {
                        if (prem.Id == jp.p)
                        {
                            prem.Selected = true;
                            c.AccountId = acct.Id;
                        }
                        var commNames = string.Empty;
                        foreach (var s in prem.Service)
                        {
                            if (commNames.IsEmpty())
                            {
                                commNames = commonTextContent.Where(e => e.key == "commodity." + s.CommodityKey + ".name").Select(x => x.shorttext).FirstOrDefault();
                            }
                            else
                            {
                                commNames = commNames + " & " + commonTextContent.Where(e => e.key == "commodity." + s.CommodityKey + ".name").Select(x => x.shorttext).FirstOrDefault();
                            }
                        }
                        prem.Commodities = commNames;

                        var jp1 = new JsonParams();
                        Mapper.CreateMap<JsonParams, JsonParams>();
                        Mapper.Map(jp, jp1);
                        jp1.p = prem.Id;
                        jp1.s = prem.Service[0].Id;
                        prem.Url = GeneralHelper.CreateLink("internal", jp1, "id=" + jp.id + "&sid=" + jp.sid);
                    }
                }
                c.AccountList = accountList;
                c.FirstName = string.IsNullOrEmpty(customerInfoResponse.Customer.FirstName) ? string.Empty : customerInfoResponse.Customer.FirstName;
                c.LastName = string.IsNullOrEmpty(customerInfoResponse.Customer.LastName) ? string.Empty : customerInfoResponse.Customer.LastName;
                c.CustomerId = customerInfoResponse.Customer.Id;
            }

            return c;
        }

        public static CustomerInfoRoot GetCustomerInfoFromCache(JsonParams jp)
        {
            CustomerInfoRoot customerInfoResponse;
            if (HttpRuntime.Cache["CustomerInfo" + jp.cl + "" + jp.c + "" + jp.l + "" + jp.w] == null)
            {
                var rm = new APIResponseModels();
                var requestParams = "CustomerId=" + jp.c;
                customerInfoResponse = (CustomerInfoRoot)JsonConvert.DeserializeObject(rm.GetCustomerInfo(requestParams, jp.l, int.Parse(jp.cl)).Content, typeof(CustomerInfoRoot));
                HttpRuntime.Cache.Insert("CustomerInfo" + jp.cl + "" + jp.c + "" + jp.l + "" + jp.w, customerInfoResponse, null, DateTime.Now.AddMinutes(Constants.kCache_Expiry), Cache.NoSlidingExpiration,
                    CacheItemPriority.NotRemovable, null);
            }
            else
            {
                customerInfoResponse = (CustomerInfoRoot)HttpRuntime.Cache["CustomerInfo" + jp.cl + "" + jp.c + "" + jp.l + "" + jp.w];
            }
            return customerInfoResponse;
        }
    }

    public class CustomerInfo : ContentBase
    {
        public string CustomerIdLabel { get; set; }
        public string AccountNumberLabel { get; set; }
        public string AddressLabel { get; set; }
        public string ServiceInfoLabel { get; set; }
        public string HomeProfileLabel { get; set; }
        public string ProfileLink { get; set; }
        public string MeterLabel { get; set; }
        public string ShowProfile { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CustomerId { get; set; }
        public string AccountId { get; set; }
        public string ViewLink { get; set; }
        public List<AddressList> Addresses { get; set; }
        public List<ServiceInfo> ServiceInfo { get; set; }
        public int QuestionsAnswered { get; set; }
        public string QuestionAnsweredText { get; set; }
        public bool NoAccounts { get; set; }
    }

    public class AddressList
    {
        public string Addr1 { get; set; }
        public string Addr2 { get; set; }
        public string City { get; set; }
        public string StateProvince { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public bool Selected { get; set; }
        public string Url { get; set; }
        public string Id { get; set; }
    }

    public class ServiceInfo
    {
        public string Id { get; set; }
        public string MeterId { get; set; }
        public string CommodityKey { get; set; }
        public string CommodityName { get; set; }
    }

    public class PremiseSelect : ContentBase
    {
        public string AccountNumberLabel { get; set; }
        public string LoadingMessage { get; set; }
        public string ExpandLabel { get; set; }
        public string CollapseLabel { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CustomerId { get; set; }
        public string AccountId { get; set; }
        public List<PremiseSelectAccount> AccountList { get; set; }
        public bool NoAccounts { get; set; }
        public bool ShowSinglePremise { get; set; }
        public PremiseSelect(RootContentObject content, string widetType, JsonParams jp) : base(content, widetType, jp)
        { }
    }

    public class PremiseSelectPremise
    {
        public string Id { get; set; }
        public string Addr1 { get; set; }
        public string Addr2 { get; set; }
        public string City { get; set; }
        public string StateProvince { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public List<CustomerInfoService> Service { get; set; }
        public string Commodities { get; set; }
        public bool Selected { get; set; }
        public string Url { get; set; }
    }

    public class PremiseSelectAccount
    {
        public string Id { get; set; }
        public List<PremiseSelectPremise> Premises { get; set; }
    }
}

