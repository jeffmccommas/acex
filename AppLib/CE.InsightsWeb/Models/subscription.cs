//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CE.InsightsWeb.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class subscription
    {
        public long id { get; set; }
        public string uuid { get; set; }
        public string self_link_href { get; set; }
        public string self_link_rel { get; set; }
        public string up_link_href { get; set; }
        public string up_link_rel { get; set; }
        public Nullable<long> application_information_id { get; set; }
        public Nullable<long> authorization_id { get; set; }
        public Nullable<long> retail_customer_id { get; set; }
        public System.DateTime published { get; set; }
        public System.DateTime updated { get; set; }
    }
}
