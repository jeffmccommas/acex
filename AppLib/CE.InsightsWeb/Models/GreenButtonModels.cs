﻿using CE.InsightsIntegration.Models;
using CE.InsightsWeb.Common;
using CE.InsightsWeb.Common.QueryStringEncryptor;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;

namespace CE.InsightsWeb.Models
{
    public class GreenButtonModels
    {
        private const string GreenButtonCategory = "greenbutton";
        private const string WidgetType = "greenbutton";
        private const string GreenButtonBillType = "bill";
        private const string GreenButtonAmiType = "ami";
        private const string ResponseNoData = "Your request did not generate any results";
        private const string Commodities = "gas,electric";

        public GreenButton GetGreenButtonData(string tabKey, string parameters, string startDate, string endDate, string type = GreenButtonBillType, bool getContent = false)
        {
            
            var jp = (JsonParams)JsonConvert.DeserializeObject(QueryStringModule.Decrypt(parameters), typeof(JsonParams));
            var content = CMS.GetContent(int.Parse(jp.cl), jp.l).Content;
            var greenButtonTextContent = content.TextContent.Where(x => x.category == GreenButtonCategory).ToList();
            var greenButtonConfig = content.Configuration.Where(x => x.category == GreenButtonCategory).ToList();
            var gb = new GreenButton(greenButtonConfig, greenButtonTextContent, tabKey, WidgetType, jp);
            //Content
            if (getContent)
            {
                //Content
                gb.BannerText = CMS.GetWidgetContent(greenButtonTextContent, tabKey, WidgetType, "bannertext");
                gb.FromDate = CMS.GetWidgetContent(greenButtonTextContent, tabKey, WidgetType, "fromdate");
                gb.ToDate = CMS.GetWidgetContent(greenButtonTextContent, tabKey, WidgetType, "todate");
                gb.DownloadAmiTitle = CMS.GetWidgetContent(greenButtonTextContent, tabKey, WidgetType, "downloadamititle");
                gb.DownloadBillsTitle = CMS.GetWidgetContent(greenButtonTextContent, tabKey, WidgetType, "downloadbillstitle");
                gb.DownloadUsage = CMS.GetWidgetContent(greenButtonTextContent, tabKey, WidgetType, "downloadusage");
                gb.DownloadBills = CMS.GetWidgetContent(greenButtonTextContent, tabKey, WidgetType, "downloadbills");
                gb.DownloadingMessage = CMS.GetWidgetContent(greenButtonTextContent, tabKey, WidgetType, "downloadingmessage");
                gb.NoBillData = CMS.GetWidgetContent(greenButtonTextContent, tabKey, WidgetType, "nobilldata");
                gb.NoUsageData = CMS.GetWidgetContent(greenButtonTextContent, tabKey, WidgetType, "nousagedata");
                gb.DateError = CMS.GetWidgetContent(greenButtonTextContent, tabKey, WidgetType, "dateerror");
                gb.ExpandLabel = CMS.GetCommonContent(content, "widget.expandlabel");
                gb.CollapseLabel = CMS.GetCommonContent(content, "widget.collapselabel");

                //Configuration
                gb.HideBillDownload = Convert.ToBoolean(CMS.GetWidgetConfig(greenButtonConfig, tabKey, WidgetType, "hidebilldownload"));
                gb.HideUsageDownload = Convert.ToBoolean(CMS.GetWidgetConfig(greenButtonConfig, tabKey, WidgetType, "hideusagedownload"));
                return gb;
            }

            gb.Response = GetGreenButtonDataFromCache(jp, type, startDate, endDate);
            if (!gb.Response.Content.Contains(ResponseNoData) && gb.Response.StatusCode != 500) return gb;
            switch (type)
            {
                case GreenButtonBillType:
                {
                    gb.NoBill = true;
                    gb.NoBillData = CMS.GetWidgetContent(greenButtonTextContent, tabKey, WidgetType, "nobilldata");
                }
                    break;
                case GreenButtonAmiType:
                {
                    gb.NoAmi = true;
                    gb.NoUsageData = CMS.GetWidgetContent(greenButtonTextContent, tabKey, WidgetType, "nousagedata");
                }
                    break;
            }
            return gb;
        }

        private static Response GetGreenButtonDataFromCache(JsonParams jp, string type, string startDate, string endDate)
        {
            Response gbResponse = null;
            if (HttpRuntime.Cache["GreenButton" + type + "" + jp.cl + "" + jp.c + "" + jp.l + "" + jp.w] == null)
            {
                var rm = new APIResponseModels();
                switch (type)
                {
                    case GreenButtonBillType:
                        {
                            var requestParams = "CustomerId=" + jp.c + "&OutputFormat=Zip";
                            gbResponse = rm.GetGreenButtonBill(requestParams, jp.l, int.Parse(jp.cl));
                        }
                        break;
                    case GreenButtonAmiType:
                        {
                            var commKeys = Commodities.Split(',').ToList();
                            var customerInfo = CustomerInfoModels.GetCustomerInfoFromCache(jp);
                          
                            var meters = string.Join(",", customerInfo.Customer.Accounts.Where(x => x.Id == jp.a)
                            .SelectMany(c => c.Premises)
                            .Where(p => p.Id == jp.p)
                            .SelectMany(s => s.Service)
                            .Where(c => commKeys.Contains(c.CommodityKey))
                            .SelectMany(c => c.ServicePoints)
                            .SelectMany(c => c.Meters).Select(x => x.Id).ToList());

                            var requestParams = "CustomerId=" + jp.c + "&AccountId=" + jp.a + "&PremiseId=" + jp.p + "&MeterIds=" + meters + "&StartDate=" + startDate + "&EndDate=" + endDate + "&OutputFormat=Zip";
                            gbResponse = rm.GetGreenButtonAmi(requestParams, jp.l, int.Parse(jp.cl));
                            type = type + "" + startDate + "" + endDate;
                        }
                        break;
                }
                if (gbResponse != null)
                    HttpRuntime.Cache.Insert("GreenButton" + type + "" + jp.cl + "" + jp.c + "" + jp.l + "" + jp.w, gbResponse, null, DateTime.Now.AddMinutes(Constants.kCache_Expiry), Cache.NoSlidingExpiration,
                        CacheItemPriority.NotRemovable, null);
            }
            else
            {
                gbResponse = (Response)HttpRuntime.Cache["GreenButton" + type + "" + jp.cl + "" + jp.c + "" + jp.l + "" + jp.w];
            }
            return gbResponse;
        }
    }

    public class GreenButton : ContentBase
    {      
        public string BannerText { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string DownloadAmiTitle { get; set; }
        public string DownloadBillsTitle { get; set; }
        public string DownloadUsage { get; set; }
        public string DownloadBills { get; set; }
        public string DownloadingMessage { get; set; }      
        public string NoBillData { get; set; }
        public string NoUsageData { get; set; }
        public string DateError { get; set; }
        public string ExpandLabel { get; set; }
        public string CollapseLabel { get; set; }
        public bool NoBill { get; set; }
        public bool NoAmi { get; set; }
        public bool HideBillDownload { get; set; }
        public bool HideUsageDownload { get; set; }
        public Response Response { get; set; }
        public GreenButton(List<Configuration> config, List<TextContent> content, string tabKey, string widetType, JsonParams jp) : base(config, content, tabKey, widetType, jp)
        { }
    }
}

