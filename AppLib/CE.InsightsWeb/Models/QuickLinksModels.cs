﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using AutoMapper;
using CE.InsightsWeb.Common;
using CE.InsightsWeb.Common.AclaraJson;
using CE.InsightsWeb.Common.QueryStringEncryptor;
using CE.InsightsWeb.Helpers;
using Newtonsoft.Json;

namespace CE.InsightsWeb.Models
{
    public class QuickLinksModels
    {
        private const string ACTION_CATEGORY = "action";
        private const string COMMON_CATEGORY = "common";
        private const string LINKS_CATEGORY = "quicklinks";
        private const string LINKS_TYPE = "quicklink";
        private const string WIDGET_TYPE = "quicklinks";

        public QuickLinks GetQuickLinks(string tabKey, string parameters)
        {
            var jp = (JsonParams)JsonConvert.DeserializeObject(QueryStringModule.Decrypt(parameters), typeof(JsonParams));
            //Content
            var content = CMS.GetContent(int.Parse(jp.cl), jp.l).Content;
            var quickLinksTextContent =
                content.TextContent.Where(x => x.category == ACTION_CATEGORY || x.category == LINKS_CATEGORY || x.category == COMMON_CATEGORY).ToList();
            var actionContent = content.Action.Where(x => x.type == LINKS_TYPE).ToList();
            var actionFileContent = content.FileContent.Where(x => x.category == ACTION_CATEGORY).ToList();
            var quickLinksConfig = content.Configuration.Where(x => x.category == LINKS_CATEGORY).ToList();
            var q = new QuickLinks(quickLinksConfig, quickLinksTextContent, tabKey, WIDGET_TYPE, jp);
            //Get QuickLinks 
            var insightsResponse = GetQuickLinksFromCache(jp);

            var lnks = CMS.GetWidgetConfig(quickLinksConfig, tabKey, WIDGET_TYPE, "links");
            var sqSplit = new List<string>(lnks.Split(';'));

            var actions = (from pa in insightsResponse.Customer.Accounts[0].Premises[0].Actions.AsQueryable()
                             where sqSplit.Any(key => pa.Key.Equals(key))
                             select pa).OrderBy(x => sqSplit.IndexOf(x.Key)).ToList();

            var am = new ActionMerge
            {
                Actions = actions,
                ActionContent = actionContent,
                TextContent = quickLinksTextContent.Where(x => x.category == ACTION_CATEGORY).ToList(),
                FileContent = actionFileContent,
                QueryString = jp
            };
            var il = new List<Link>();
            Mapper.CreateMap<ActionMerge, List<Link>>().ConvertUsing(new QuickLinksConverter());
            q.LinkList = Mapper.Map(am, il);
           
            //Configuration  
            q.ShowIcons = CMS.GetWidgetConfig(quickLinksConfig, tabKey, WIDGET_TYPE, "showicons");
           
            return q;
        }

        private static ActionRoot GetQuickLinksFromCache(JsonParams jp)
        {
            ActionRoot insightsResponse;
            if (HttpRuntime.Cache["QuickLinksResponse" + jp.cl + "" + jp.c + "" + jp.a + "" + jp.p + "" + jp.s + "" + jp.l + "" + jp.w] ==
                null)
            {
                var rm = new APIResponseModels();
                var requestParams = "CustomerId=" + jp.c + "&AccountId=" + jp.a + "&PremiseId=" + jp.p + "&Types=quicklink";
                insightsResponse = (ActionRoot)JsonConvert.DeserializeObject(rm.GetAction(requestParams, jp.l, int.Parse(jp.cl)).Content, typeof(ActionRoot));
                HttpRuntime.Cache.Insert(
                    "QuickLinksResponse" + jp.cl + "" + jp.c + "" + jp.a + "" + jp.p + "" + jp.s + "" + jp.l + "" + jp.w, insightsResponse,
                    null, DateTime.Now.AddMinutes(Constants.kCache_Expiry), Cache.NoSlidingExpiration,
                    CacheItemPriority.NotRemovable, null);
            }
            else
            {
                insightsResponse =
                    (ActionRoot)
                        HttpRuntime.Cache[
                            "QuickLinksResponse" + jp.cl + "" + jp.c + "" + jp.a + "" + jp.p + "" + jp.s + "" + jp.l + "" + jp.w];
            }

            return insightsResponse;
        }
    }

    public class QuickLinksConverter : ITypeConverter<ActionMerge, List<Link>>
    {
        public List<Link> Convert(ResolutionContext context)
        {
            var sourceCollection = (ActionMerge)context.SourceValue;

            Mapper.CreateMap<ActionMerge, List<Link>>();
            Mapper.CreateMap<Common.AclaraJson.Action, Link>();
            var al = new List<Link>();
            foreach (var a in sourceCollection.Actions)
            {
                var i = EntityMapper.Map<Link>(a);
                var act = sourceCollection.ActionContent.Where(p => p.key == a.Key).FirstOrDefault();
                i.ActionPriority = act.actionpriority;
                sourceCollection.ImageSize = Enums.ImageSize.Small;
                SetupQuickLinks(sourceCollection, act, i, sourceCollection.QueryString);
                al.Add(i);
            }

            return al.OrderBy(x => x.ActionPriority).ToList();
        }

        private static void SetupQuickLinks(ActionMerge sourceCollection, Common.Action act, Link i, JsonParams jp)
        {
            if (!string.IsNullOrEmpty(act.imagekey))
            {
                var imgContent = sourceCollection.FileContent.Where(x => x.key == act.imagekey).FirstOrDefault();
                if (sourceCollection.ImageSize == Enums.ImageSize.Small)
                {
                    if (imgContent != null)
                    {
                        i.ImageUrl = imgContent.smallfile;
                        i.ImageTitle = imgContent.smallfiletitle;
                    }
                }
                else if (sourceCollection.ImageSize == Enums.ImageSize.Medium)
                {
                    if (imgContent != null)
                    {
                        i.ImageUrl = imgContent.mediumfile;
                        i.ImageTitle = imgContent.mediumfiletitle;
                    }
                }
                else if (sourceCollection.ImageSize == Enums.ImageSize.Large)
                {
                    if (imgContent != null)
                    {
                        i.ImageUrl = imgContent.largefile;
                        i.ImageTitle = imgContent.largefiletitle;
                    }
                }
            }
            if (!string.IsNullOrEmpty(act.iconclass))
            {
                i.Icon = act.iconclass;
            }
           
            var desc = sourceCollection.TextContent.Where(p => p.key == act.descriptionkey).FirstOrDefault();
            i.Description = (desc == null) ? "" : desc.shorttext;
            
            var lnkText = sourceCollection.TextContent.Where(p => p.key == act.nextsteplinktext).FirstOrDefault();
            i.LinkText = (lnkText == null) ? "" : lnkText.shorttext;

            i.LinkType = act.nextsteplinktype;
            i.Url = GeneralHelper.CreateLink(act.nextsteplinktype, jp, act.nextsteplink);
        }
    }
    public class QuickLinks : ContentBase
    {      
        public string ShowIcons { get; set; }      
        public List<Link> LinkList { get; set; }
        public QuickLinks(List<Common.Configuration> config, List<Common.TextContent> content, string tabKey, string widetType, JsonParams jp) : base(config, content, tabKey, widetType, jp)
        { }
    }

    public class Link
    {
        public string Key { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public string ImageTitle { get; set; }
        public string Icon { get; set; }
        public string Url { get; set; }
        public string LinkText { get; set; }
        public string LinkType { get; set; }
        public double ActionPriority { get; set; }
    }
}

