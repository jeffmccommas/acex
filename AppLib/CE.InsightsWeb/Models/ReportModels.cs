﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using CE.InsightsIntegration.Models;
using CE.InsightsWeb.Common;
using CE.InsightsWeb.Common.AclaraJson;
using CE.InsightsWeb.Common.QueryStringEncryptor;
using CE.InsightsWeb.Helpers;
using LinqKit;
using Newtonsoft.Json;
using System.IO;
using TextContent = CE.InsightsWeb.Common.TextContent;

namespace CE.InsightsWeb.Models
{
    /// <summary>
    /// Model for Report\File
    /// </summary>
    public class ReportModels
    {
        private const string WidgetType = "reportcreate";
        private const string ReportcreateCategory = "reportcreate";
        private const string ReportlistCategory = "reportlist";
        private const string ReportlistWidgetType = "reportlist";
        // Source ID for ReportCreate is 1 in table [dbo].[FileSource]
        private const string Sourceid = "1";
        // File Type ID for .pdf is 1 in the table [dbo].[FileType]
        private const string Filetypeid = "1";

        public static ReportCreate GetReportCreate(string tabKey, string parameters)
        {
            var jp = (JsonParams)JsonConvert.DeserializeObject(QueryStringModule.Decrypt(parameters), typeof(JsonParams));
            //Content
            var content = CMS.GetContent(int.Parse(jp.cl), jp.l).Content;

            var isCsr = jp.r != null && !jp.r.Equals(string.Empty);
            isCsr = isCsr && jp.u != null && !jp.u.Equals(string.Empty);
            // use common content for CSR widget
            if (isCsr)
            {
                var r = new ReportCreate
                {
                    IntroText = CMS.GetCommonContent(content, "reportcreate.introtext"),
                    Title = CMS.GetCommonContent(content, "reportcreate.title"),
                    SubTitle = CMS.GetCommonContent(content, "reportcreate.subtitle"),
                    Footer = CMS.GetCommonContent(content, "reportcreate.footer"),
                    FooterLinkText = CMS.GetCommonContent(content, "reportcreate.footerlinktext"),
                    ReportTitle = CMS.GetCommonContent(content, "reportcreate.reporttitle"),
                    ReportNotes = CMS.GetCommonContent(content, "reportcreate.reportnotes"),
                    NoTitle = CMS.GetCommonContent(content, "reportcreate.notitle"),
                    CreateReportButton = CMS.GetCommonContent(content, "reportcreate.createreportbutton"),
                    DefaultReportTitle = CMS.GetCommonContent(content, "reportcreate.defaultreporttitle"),
                    DefaultReportNotes = CMS.GetCommonContent(content, "reportcreate.defaultreportnotes"),
                    PostError = CMS.GetCommonContent(content, "reportcreate.posterror"),
                    ExcludeWidgets = CMS.GetCommonContent(content, "reportcreate.excludewidgets", "config")
                };
                r.ExcludeWidgets = r.ExcludeWidgets?.ToLower() ?? string.Empty;
                r.ShowTitle = CMS.GetCommonContent(content, "reportcreate.showtitle", "config");
                r.ShowSubTitle = CMS.GetCommonContent(content, "reportcreate.showsubtitle", "config");
                r.ShowIntro = CMS.GetCommonContent(content, "reportcreate.showintro", "config");
                r.FooterLink = GeneralHelper.CreateLink(CMS.GetCommonContent(content, "reportcreate.footerlinktype", "config"), jp, CMS.GetCommonContent(content, "reportcreate.footerlink", "config"));
                r.ShowFooterLink = CMS.GetCommonContent(content, "reportcreate.showfooterlink", "config");
                r.ShowFooterText = CMS.GetCommonContent(content, "reportcreate.showfootertext", "config");

                return r;
            }
            else
            {
                var reportcreateTextContent = content.TextContent.Where(x => x.category == ReportcreateCategory).ToList();
                var reportcreateConfig = content.Configuration.Where(x => x.category == ReportcreateCategory).ToList();
                var r = new ReportCreate(reportcreateConfig, reportcreateTextContent, tabKey, WidgetType, jp)
                {
                    ReportTitle = CMS.GetWidgetContent(reportcreateTextContent, tabKey, WidgetType, "reporttitle"),
                    ReportNotes = CMS.GetWidgetContent(reportcreateTextContent, tabKey, WidgetType, "reportnotes"),
                    NoTitle = CMS.GetWidgetContent(reportcreateTextContent, tabKey, WidgetType, "notitle"),
                    CreateReportButton = CMS.GetWidgetContent(reportcreateTextContent, tabKey, WidgetType, "createreportbutton"),
                    DefaultReportTitle = CMS.GetWidgetContent(reportcreateTextContent, tabKey, WidgetType, "defaultreporttitle"),
                    DefaultReportNotes = CMS.GetWidgetContent(reportcreateTextContent, tabKey, WidgetType, "defaultreportnotes"),
                    PostError = CMS.GetWidgetContent(reportcreateTextContent, tabKey, WidgetType, "posterror"),
                    ExcludeWidgets = CMS.GetCommonContent(content, "reportcreate.excludewidgets", "config")
                };

                r.ExcludeWidgets = r.ExcludeWidgets?.ToLower() ?? string.Empty;

                return r;
            }
        }

        public static string IsCSR(string parameters)
        {
            var jp = (JsonParams)JsonConvert.DeserializeObject(QueryStringModule.Decrypt(parameters), typeof(JsonParams));
            var isCsr = jp.r != null && !jp.r.Equals(string.Empty);
            isCsr = isCsr && jp.u != null && !jp.u.Equals(string.Empty);
            return JsonConvert.SerializeObject(isCsr);
        }

        public static ReportListWidget GetReportsData(string tabKey, string parameters)
        {
            var jp = (JsonParams)JsonConvert.DeserializeObject(QueryStringModule.Decrypt(parameters), typeof(JsonParams));
            var cr = CMS.GetContent(int.Parse(jp.cl), jp.l);

            var predicate = PredicateBuilder.True<TextContent>();
            predicate.And(m => m.category == ReportlistCategory);

            var predicateConfig = PredicateBuilder.True<Common.Configuration>();
            predicateConfig.And(m => m.category == ReportlistCategory);

            var rl = new ReportListWidget
            {
                Content = new ReportContent(cr.Content.Configuration, cr.Content.TextContent, tabKey, ReportlistWidgetType, jp)
                {
                    // Config Content
                    FooterLinkType = CMS.GetWidgetConfig(cr.Content, tabKey, ReportlistWidgetType, "footerlinktype", predicateConfig),
                    // text Content
                    OpenLink = CMS.GetWidgetContent(cr.Content, tabKey, ReportlistWidgetType, "openlink", predicate),
                    DeleteLink = CMS.GetWidgetContent(cr.Content, tabKey, ReportlistWidgetType, "deletelink", predicate),
                    NoData = CMS.GetWidgetContent(cr.Content, tabKey, ReportlistWidgetType, "nodata", predicate),
                    DeletConfirm = CMS.GetWidgetContent(cr.Content, tabKey, ReportlistWidgetType, "deleteconfirm", predicate),
                    CustomerSectionTitle = CMS.GetWidgetContent(cr.Content, tabKey, ReportlistWidgetType, "customersectiontitle", predicate),
                    CsrSectionTitle = CMS.GetWidgetContent(cr.Content, tabKey, ReportlistWidgetType, "csrsectiontitle", predicate),
                    DeleteError = CMS.GetWidgetContent(cr.Content, tabKey, ReportlistWidgetType, "deleteerror", predicate),
                    ViewError = CMS.GetWidgetContent(cr.Content, tabKey, ReportlistWidgetType, "viewerror", predicate)
                }
            };

            var rm = new APIResponseModels();
            var sb = new StringBuilder();
            var isCsr = jp.r != null && !jp.r.Equals(string.Empty);
            isCsr = isCsr && jp.u != null && !jp.u.Equals(string.Empty);

            sb.Append("CustomerId=" + jp.c + "&AccountId=" + jp.a + "&PremiseId=" + jp.p);
            if (isCsr)
            {
                sb.Append("&UserId=" + jp.u + "&UserRole=" + GetRoleId(jp.r));
            }

            var reportList = (ReportList)JsonConvert.DeserializeObject(rm.GetReportsPdf(sb.ToString(), jp.l, int.Parse(jp.cl)).Content, typeof(ReportList));
            Mapper.CreateMap<Report, ReportData>();

            var reportsUser = reportList.Files.FindAll(r => r.RoleId == "0");
            //update relativetime field of reports
            foreach (var report in reportsUser)
            {
                report.RelativeTime = CalculateRelativeTime(report.CreateDate);
            }
            rl.ReportsUser = Mapper.Map<List<Report>, List<ReportData>>(reportsUser);
            if (isCsr)
            {
                var reportsCsr = reportList.Files.FindAll(r => r.RoleId != "0");
                foreach (var report in reportsCsr)
                {
                    report.RelativeTime = CalculateRelativeTime(report.CreateDate);
                }
                rl.ReportsCSR = Mapper.Map<List<Report>, List<ReportData>>(reportsCsr);
                rl.isCSR = true;
                //Sorting the lists by Id -- descending order
                rl.ReportsCSR = rl.ReportsCSR.OrderByDescending(o => DateTime.Parse(o.CreateDate)).ToList();
            }
            rl.ReportsUser = rl.ReportsUser.OrderByDescending(o => DateTime.Parse(o.CreateDate)).ToList();
            return rl;
        }

        private static string CalculateRelativeTime(string reportdatetime)
        {
            const int SECOND = 1;
            const int MINUTE = 60 * SECOND;
            const int HOUR = 60 * MINUTE;
            const int DAY = 24 * HOUR;
            const int MONTH = 30 * DAY;

            var relativetime = DateTime.Parse(reportdatetime);
            var ts = new TimeSpan(DateTime.UtcNow.Ticks - relativetime.Ticks);
            double delta = Math.Abs(ts.TotalSeconds);
            if (delta < 1 * MINUTE)
                return ts.Seconds == 1 ? "one second ago" : ts.Seconds + " seconds ago";
            if (delta < 2 * MINUTE)
                return "a minute ago";
            if (delta < 45 * MINUTE)
                return ts.Minutes + " minutes ago";
            if (delta < 90 * MINUTE)
                return "an hour ago";
            if (delta < 24 * HOUR)
                return ts.Hours + " hours ago";
            if (delta < 48 * HOUR)
                return "yesterday";
            if (delta < 30 * DAY)
                return ts.Days + " days ago";
            if (delta < 12 * MONTH)
            {
                int months = Convert.ToInt32(Math.Floor((double)ts.Days / 30));
                return months <= 1 ? "one month ago" : months + " months ago";
            }
            int years = Convert.ToInt32(Math.Floor((double)ts.Days / 365));
            return years <= 1 ? "one year ago" : years + " years ago";
        }

        public static Response GetReportData(string parameters, int reportId)
        {
            var jp = (JsonParams)JsonConvert.DeserializeObject(QueryStringModule.Decrypt(parameters), typeof(JsonParams));
            var rm = new APIResponseModels();
            var requestParams = "FileId=" + reportId;
            var reportdata = rm.GetReportPdf(requestParams, jp.l, int.Parse(jp.cl));
            return reportdata;
        }

        public static Response DeleteReport(string parameters, int reportId)
        {
            var jp = (JsonParams)JsonConvert.DeserializeObject(QueryStringModule.Decrypt(parameters), typeof(JsonParams));
            var rm = new APIResponseModels();
            var requestParams = "FileId=" + reportId;
            return rm.DeleteReportPdf(requestParams, jp.l, int.Parse(jp.cl));

        }

        public static string PostReport(string parameters, string title, string notes, string filedata, bool isCsr)
        {

            var jp = (JsonParams)JsonConvert.DeserializeObject(QueryStringModule.Decrypt(parameters), typeof(JsonParams));
            var filename = title + ".pdf";
            var sb = new StringBuilder();
            sb.Append("CustomerId=" + jp.c + "&AccountId=" + jp.a + "&PremiseId=" + jp.p + "&SourceId=" + Sourceid);
            sb.Append("&Title=" + title + "&Description=" + notes + "&TypeId" + Filetypeid);
            if (isCsr)
            {
                sb.Append("&UserId=" + jp.u + "&RoleId=" + GetRoleId(jp.r));
            }

            filedata = filedata.Replace("data:application/pdf;base64,", "");
            byte[] reportBytes = Convert.FromBase64String(filedata);
            var ms = new MemoryStream(reportBytes, 0, reportBytes.Length);
            var rm = new APIResponseModels();
            return JsonConvert.SerializeObject(rm.PostReport(sb.ToString(), ms, filename, "en-US", Convert.ToInt32(jp.cl)));
        }
        private static string GetRoleId(string roleName)
        {
            using (var ent = new InsightsEntities())
            {
                ent.Database.Connection.Open();
                var roleId = (from r in ent.Roles
                              where r.RoleName == roleName
                              select r.RoleID).FirstOrDefault();

                return roleId.ToString();
            }

        }
    }

    public class ReportCreate : ContentBase
    {
        public string NoTitle { get; set; }
        public string CreateReportButton { get; set; }
        public string ReportNotes { get; set; }
        public string ReportTitle { get; set; }
        public string DefaultReportNotes { get; set; }
        public string DefaultReportTitle { get; set; }
        public string ExcludeWidgets { get; set; }
        public string PostError { get; set; }
        public ReportCreate(List<Common.Configuration> config, List<TextContent> content, string tabKey, string widetType, JsonParams jp) : base(config, content, tabKey, widetType, jp)
        { }
        public ReportCreate() 
        { }
    }

    public class ReportListWidget
    {
        public ReportContent Content { get; set; }
        public List<ReportData> ReportsCSR { get; set; }
        public List<ReportData> ReportsUser { get; set; }
        public bool isCSR { get; set; }
    }

    public class ReportData
    {
        public string Id { get; set; }
        public string CustomerId { get; set; }
        public string AccountId { get; set; }
        public string PremiseId { get; set; }
        public string RoleId { get; set; }
        public string UserId { get; set; }
        public string TypeId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public string FileSourceId { get; set; }
        public string CreateDate { get; set; }
        public string RelativeTime { get; set; }
        public bool ErrorStatus { get; set; }
    }

    public class ReportContent : ContentBase
    {
        public string NoData { get; set; }

        public string OpenLink { get; set; }

        public string DeleteLink { get; set; }
        public string DeletConfirm { get; set; }
        public string CustomerSectionTitle { get; set; }
        public string CsrSectionTitle { get; set; }
        public string DeleteError { get; set; }
        public string ViewError { get; set; }

        public ReportContent(List<Common.Configuration> config, List<TextContent> content, string tabKey, string widetType, JsonParams jp) : base(config, content, tabKey, widetType, jp)
        { }

    }
}