﻿using CE.GreenButtonConnect;
using CE.InsightsWeb.Common;
using CE.InsightsWeb.Common.QueryStringEncryptor;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Transactions;
using Configuration = CE.InsightsWeb.Common.Configuration;

namespace CE.InsightsWeb.Models
{
    public class GreenButtonConnectModels
    {
        private const string GreenButtonCategory = "greenbuttonconnect";
        private const string ResponseType = "code";
        private const string ErrorCodeUnsupportedResponseType = "unsupported_response_type";
        private const string ErrorDescUnsupportedResponseType = "The authorization server does not support obtaining an authorization code using this method.";
        private const string ErrorCodeInvalidClient = "invalid_clientId";
        private const string ErrorDescInvalidClient = "Client {%client_id%} is not registered in the system.";
        private const string ErrorCodeInvalidRedirectUri = "invalid_redirect_uri";
        private const string ErrorDescInvalidRedirectUri = "The redirect URI scheme is not valid.";
        private const string ErrorCodeRedirectUriMismatch = "invalid_uri_mismatch";
        private const string ErrorDescRedirectUriMismatch = "The redirect URI is missing or does not match.";
        private const string ErrorCodeNoDataFound = "no_data";
        private const string ErrorDescNoDataFound = "No data found for this customer.";
        private const string ErrorCodeInsecureRedirectUri = "insecure_redirect_uri";
        private const string ErrorDescInsecureRedirectUri = "The URI is either not a custom protocol or not HTTPS.";
        private const string ErrorCodeInvalidScope = "invalid_scope";
        private const string ErrorDescInvalidScope = "The scope string is invalid.";
        private const string AppSettingCeEnvironment = "CEEnvironment";
        private const string ThirdParty = "third_party";

        public static GreenButtonApplication RegisterApplication(string clientIdentifier, string environment, out bool success, GreenButtonApplication greenButtonApplication = null)
        {
            var gba = new GreenButtonApplication();
            var gbl = new List<application_information>();
            var gb = new application_information();
            if (greenButtonApplication == null)
            {
                gbl.Add(gb);
                gba.GreenButtonApplicationInformation = gbl;
                success = true;
                return gba;
            }
            //declare the transaction options
            var transactionOptions = new TransactionOptions
            {
                IsolationLevel = IsolationLevel.ReadUncommitted
            };

            //New
            if (greenButtonApplication.GreenButtonApplicationInformation[0].application_information_id < 1)
            {
                using (var transScope = new TransactionScope(TransactionScopeOption.Required, transactionOptions))
                {
                    using (var ent = new InsightsEntities())
                    {
                        ent.Database.Connection.Open();
                        using (var dbContextTransaction = ent.Database.BeginTransaction())
                        {
                            try
                            {
                                ent.Configuration.AutoDetectChangesEnabled = false;
                                ent.Configuration.ValidateOnSaveEnabled = false;

                                var clientId = Guid.NewGuid().ToString();
                                var secret = Helper.GenerateSecret();
                                var token = GenerateRegistrationToken(clientId, secret);

                                var ai = new application_information
                                {
                                    client_secret = secret,
                                    client_id = clientId,
                                    uuid = "",
                                    registration_access_token = token,
                                    client_id_issued_at = Helper.ConvertToUnixTimestamp(DateTime.UtcNow),
                                    client_secret_expires_at = Helper.ConvertToUnixTimestamp(DateTime.UtcNow.AddYears(2)),
                                    self_link_href = "/DataCustodian/espi/1_1/resource/ApplicationInformation",
                                    self_link_rel = "self",
                                    up_link_href = "/DataCustodian/espi/1_1/resource/ApplicationInformation",
                                    up_link_rel = "up",
                                    dataCustodianId = Helper.Base64Encode(clientIdentifier + ":" + environment),
                                    dataCustodianApplicationStatus = greenButtonApplication.GreenButtonApplicationInformation[0].dataCustodianApplicationStatus,
                                    dataCustodianScopeSelectionScreenURI = greenButtonApplication.GreenButtonApplicationInformation[0].dataCustodianScopeSelectionScreenURI,
                                    dataCustodianResourceEndpoint = greenButtonApplication.GreenButtonApplicationInformation[0].dataCustodianResourceEndpoint,
                                    dataCustodianBulkRequestURI = greenButtonApplication.GreenButtonApplicationInformation[0].dataCustodianBulkRequestURI,
                                    thirdPartyApplicationDescription = greenButtonApplication.GreenButtonApplicationInformation[0].thirdPartyApplicationDescription,
                                    thirdPartyApplicationStatus = greenButtonApplication.GreenButtonApplicationInformation[0].thirdPartyApplicationStatus,
                                    thirdPartyUserPortalScreenURI = greenButtonApplication.GreenButtonApplicationInformation[0].thirdPartyUserPortalScreenURI,
                                    thirdPartyApplicationType = greenButtonApplication.GreenButtonApplicationInformation[0].thirdPartyApplicationType,
                                    thirdPartyApplicationUse = greenButtonApplication.GreenButtonApplicationInformation[0].thirdPartyApplicationUse,
                                    thirdPartyPhone = greenButtonApplication.GreenButtonApplicationInformation[0].thirdPartyPhone,
                                    thirdPartyNotifyUri = greenButtonApplication.GreenButtonApplicationInformation[0].thirdPartyNotifyUri,
                                    thirdPartyScopeSelectionScreenURI = greenButtonApplication.GreenButtonApplicationInformation[0].thirdPartyScopeSelectionScreenURI,
                                    authorizationServerUri = greenButtonApplication.GreenButtonApplicationInformation[0].authorizationServerUri,
                                    authorizationServerAuthorizationEndpoint = greenButtonApplication.GreenButtonApplicationInformation[0].authorizationServerAuthorizationEndpoint,
                                    authorizationServerRegistrationEndpoint = greenButtonApplication.GreenButtonApplicationInformation[0].authorizationServerRegistrationEndpoint,
                                    authorizationServerTokenEndpoint = greenButtonApplication.GreenButtonApplicationInformation[0].authorizationServerTokenEndpoint,
                                    logo_uri = greenButtonApplication.GreenButtonApplicationInformation[0].logo_uri,
                                    client_name = greenButtonApplication.GreenButtonApplicationInformation[0].client_name,
                                    client_uri = greenButtonApplication.GreenButtonApplicationInformation[0].client_uri,
                                    redirect_uri = greenButtonApplication.GreenButtonApplicationInformation[0].redirect_uri,
                                    tos_uri = greenButtonApplication.GreenButtonApplicationInformation[0].tos_uri,
                                    policy_uri = greenButtonApplication.GreenButtonApplicationInformation[0].policy_uri,
                                    software_id = greenButtonApplication.GreenButtonApplicationInformation[0].software_id,
                                    software_version = greenButtonApplication.GreenButtonApplicationInformation[0].software_version,
                                    contacts = greenButtonApplication.GreenButtonApplicationInformation[0].contacts,
                                    token_endpoint_auth_method = "POST",
                                    registration_client_uri = greenButtonApplication.GreenButtonApplicationInformation[0].registration_client_uri,
                                    enabled = greenButtonApplication.GreenButtonApplicationInformation[0].enabled,
                                    environment_type = "external",
                                    published = DateTime.UtcNow,
                                    updated = DateTime.UtcNow
                                };

                                ent.Entry(ai).State = EntityState.Added;
                                ent.application_information.Add(ai);
                                ent.SaveChanges();

                                if (!string.IsNullOrEmpty(greenButtonApplication.ScopeString))
                                {
                                    var ais = new application_information_scopes
                                    {
                                        application_information_id = ai.application_information_id,
                                        scope = greenButtonApplication.ScopeString
                                    };

                                    ent.Entry(ais).State = EntityState.Added;
                                    ent.application_information_scopes.Add(ais);
                                    ent.SaveChanges();
                                }


                                var gen = new IdGenerator(Convert.ToInt32(clientIdentifier), ai.application_information_id);
                                ai.uuid = gen.ApplicationInfoId;
                                ai.self_link_href = "/DataCustodian/espi/1_1/resource/ApplicationInformation/" + ai.application_information_id;
                                ent.Entry(ai).State = EntityState.Modified;
                                ent.SaveChanges();

                                gbl.Add(ai);
                                gba.GreenButtonApplicationInformation = gbl;

                                //create a record in authorization table -- third party reg token
                                var a = new authorization
                                {
                                    published = DateTime.UtcNow,
                                    self_link_rel = "self",
                                    up_link_rel = "up",
                                    updated = DateTime.UtcNow,
                                    uuid = "",
                                    access_token = token,
                                    authorization_uri = "/Datacustodian/espi/1_1/resource/Authorization/",
                                    ap_duration = Helper.GetDuration(4),
                                    ap_start = Helper.ConvertToUnixTimestamp(DateTime.UtcNow.AddYears(-2)),
                                    expiresin = Helper.ConvertToUnixTimestamp(DateTime.UtcNow.AddYears(2)),
                                    grant_type = "registration_access_token",
                                    pp_duration = 0,
                                    pp_start = 0,
                                    resourceURI = "/Datacustodian/espi/1_1/resource/ApplicationInfo/" + ai.application_information_id,
                                    scope = "FB=36_40",
                                    application_information_id = ai.application_information_id,
                                    retail_customer_id = 0,
                                    third_party = "REGISTRATION_third_party",
                                    status = (int)Helper.AuthorizationStatus.Active
                                };

                                ent.Entry(a).State = EntityState.Added;
                                ent.authorizations.Add(a);
                                ent.SaveChanges();

                                //create a subscription record
                                var s = new subscription
                                {
                                    self_link_rel = "self",
                                    up_link_rel = "up",
                                    uuid = "",
                                    application_information_id = ai.application_information_id,
                                    retail_customer_id = 0,
                                    authorization_id = a.id,
                                    published = DateTime.UtcNow,
                                    updated = DateTime.UtcNow
                                };

                                ent.Entry(s).State = EntityState.Added;
                                ent.subscriptions.Add(s);
                                ent.SaveChanges();

                                var genSubs = new IdGenerator(Convert.ToInt32(clientIdentifier), s.id);
                                s.uuid = genSubs.SubscriptionIdClientAccess;
                                ent.Entry(s).State = EntityState.Modified;
                                ent.SaveChanges();

                                //Update record with authorization uri, subscription id
                                var genAuth = new IdGenerator(Convert.ToInt32(clientIdentifier), a.id);
                                a.uuid = genAuth.AuthorizationIdClientAccess;
                                a.authorization_uri = "/Datacustodian/espi/1_1/resource/Authorization/" + a.id;
                                a.subscription_id = s.id;
                                ent.Entry(a).State = EntityState.Modified;
                                ent.SaveChanges();

                                dbContextTransaction.Commit();
                            }
                            catch (Exception ex)
                            {
                                success = false;
                                dbContextTransaction.Rollback();
                            }
                        }
                    }
                    transScope.Complete();
                }
            }
            else//Update
            {
                using (var transScope = new TransactionScope(TransactionScopeOption.Required, transactionOptions))
                {
                    using (var ent = new InsightsEntities())
                    {
                        ent.Database.Connection.Open();
                        using (var dbContextTransaction = ent.Database.BeginTransaction())
                        {
                            try
                            {
                                ent.Configuration.AutoDetectChangesEnabled = false;
                                ent.Configuration.ValidateOnSaveEnabled = false;

                                var clientId = greenButtonApplication.GreenButtonApplicationInformation[0].client_id;
                                var ai = (from ot in ent.application_information
                                          where ot.client_id == clientId
                                          select ot).FirstOrDefault();

                                if (ai != null)
                                {
                                    ai.enabled = greenButtonApplication.GreenButtonApplicationInformation[0].enabled;
                                    ai.dataCustodianId = Helper.Base64Encode(clientIdentifier + ":" + environment);
                                    ai.dataCustodianApplicationStatus = greenButtonApplication.GreenButtonApplicationInformation[0].dataCustodianApplicationStatus;
                                    ai.dataCustodianScopeSelectionScreenURI = greenButtonApplication.GreenButtonApplicationInformation[0].dataCustodianScopeSelectionScreenURI;
                                    ai.dataCustodianResourceEndpoint = greenButtonApplication.GreenButtonApplicationInformation[0].dataCustodianResourceEndpoint;
                                    ai.dataCustodianBulkRequestURI = greenButtonApplication.GreenButtonApplicationInformation[0].dataCustodianBulkRequestURI;
                                    ai.thirdPartyApplicationDescription = greenButtonApplication.GreenButtonApplicationInformation[0].thirdPartyApplicationDescription;
                                    ai.thirdPartyApplicationStatus = greenButtonApplication.GreenButtonApplicationInformation[0].thirdPartyApplicationStatus;           
                                    ai.thirdPartyApplicationType = greenButtonApplication.GreenButtonApplicationInformation[0].thirdPartyApplicationType;
                                    ai.thirdPartyApplicationUse = greenButtonApplication.GreenButtonApplicationInformation[0].thirdPartyApplicationUse;
                                    ai.thirdPartyPhone = greenButtonApplication.GreenButtonApplicationInformation[0].thirdPartyPhone;
                                    ai.thirdPartyUserPortalScreenURI = greenButtonApplication.GreenButtonApplicationInformation[0].thirdPartyUserPortalScreenURI;
                                    ai.thirdPartyNotifyUri = greenButtonApplication.GreenButtonApplicationInformation[0].thirdPartyNotifyUri;
                                    ai.thirdPartyScopeSelectionScreenURI = greenButtonApplication.GreenButtonApplicationInformation[0].thirdPartyScopeSelectionScreenURI;
                                    ai.authorizationServerUri = greenButtonApplication.GreenButtonApplicationInformation[0].authorizationServerUri;
                                    ai.authorizationServerAuthorizationEndpoint = greenButtonApplication.GreenButtonApplicationInformation[0].authorizationServerAuthorizationEndpoint;
                                    ai.authorizationServerRegistrationEndpoint = greenButtonApplication.GreenButtonApplicationInformation[0].authorizationServerRegistrationEndpoint;
                                    ai.authorizationServerTokenEndpoint = greenButtonApplication.GreenButtonApplicationInformation[0].authorizationServerTokenEndpoint;                                 
                                    ai.client_name = greenButtonApplication.GreenButtonApplicationInformation[0].client_name;
                                    ai.client_uri = greenButtonApplication.GreenButtonApplicationInformation[0].client_uri;
                                    ai.logo_uri = greenButtonApplication.GreenButtonApplicationInformation[0].logo_uri;                                   
                                    ai.redirect_uri = greenButtonApplication.GreenButtonApplicationInformation[0].redirect_uri;
                                    ai.tos_uri = greenButtonApplication.GreenButtonApplicationInformation[0].tos_uri;
                                    ai.policy_uri = greenButtonApplication.GreenButtonApplicationInformation[0].policy_uri;
                                    ai.software_id = greenButtonApplication.GreenButtonApplicationInformation[0].software_id;
                                    ai.software_version = greenButtonApplication.GreenButtonApplicationInformation[0].software_version;
                                    ai.contacts = greenButtonApplication.GreenButtonApplicationInformation[0].contacts;
                                    ai.token_endpoint_auth_method = greenButtonApplication.GreenButtonApplicationInformation[0].token_endpoint_auth_method;
                                    ai.registration_client_uri = greenButtonApplication.GreenButtonApplicationInformation[0].registration_client_uri;
                                    
                                    ai.updated = DateTime.UtcNow;
                                    ent.Entry(ai).State = EntityState.Modified;

                                    if (!string.IsNullOrEmpty(greenButtonApplication.ScopeString))
                                    {
                                        var ais = (from i in ent.application_information_scopes
                                            where i.application_information_id == ai.application_information_id
                                            select i).FirstOrDefault();

                                        if (ais != null)
                                        {
                                            ais.scope = greenButtonApplication.ScopeString;
                                            ent.Entry(ais).State = EntityState.Modified;
                                        }
                                    }
                                }
                                ent.SaveChanges();
                                dbContextTransaction.Commit();
                            }
                            catch (Exception)
                            {
                                success = false;
                                dbContextTransaction.Rollback();
                            }
                        }
                    }
                    transScope.Complete();
                }
            }
            success = true;
            return gba;
        }

        public static GreenButtonApplication GetRegisteredApplications(string tabKey, string parameters, string widgetType, string environment)
        {
            var jp = (JsonParams)JsonConvert.DeserializeObject(QueryStringModule.Decrypt(parameters), typeof(JsonParams));
            var content = CMS.GetContent(int.Parse(jp.cl), jp.l).Content;
            var greenButtonTextContent = content.TextContent.Where(x => x.category == GreenButtonCategory).ToList();
            var greenButtonConfig = content.Configuration.Where(x => x.category == GreenButtonCategory).ToList();
            var gba = new GreenButtonApplication(greenButtonConfig, greenButtonTextContent, tabKey, widgetType, jp);

            //declare the transaction options
            var transactionOptions = new TransactionOptions
            {
                IsolationLevel = IsolationLevel.ReadUncommitted
            };

            using (var transScope = new TransactionScope(TransactionScopeOption.Required, transactionOptions))
            {
                using (var ent = new InsightsEntities())
                {
                    ent.Database.Connection.Open();
                    using (var dbContextTransaction = ent.Database.BeginTransaction())
                    {
                        try
                        {
                            ent.Configuration.AutoDetectChangesEnabled = false;
                            ent.Configuration.ValidateOnSaveEnabled = false;
                            var dcId = Helper.Base64Encode(jp.cl + ":" + environment);

                            gba.GreenButtonApplicationInformation = (from ai in ent.application_information
                                                                     where ai.dataCustodianId == dcId && ai.enabled == true
                                                                     select ai).ToList();
                            
                            var acl = new List<ApplicationsConnected>();
                            foreach (var gai in gba.GreenButtonApplicationInformation)
                            {

                                var thirdPartyAppUseText = greenButtonTextContent.Where(x => x.key == "enumeration.thirdpartyappuse" + gai.thirdPartyApplicationUse + ".name").Select(x => x.shorttext).FirstOrDefault();

                                var ac = new ApplicationsConnected();
                                var cl = Convert.ToInt32(jp.cl);
                                var auth = (from ai in ent.authorizations
                                            join ret in ent.retail_customers on ai.retail_customer_id equals ret.id
                                            where ai.application_information_id == gai.application_information_id
                                            && ret.data_custodian_client_id == cl && ret.data_custodian_customer_id == jp.c
                                            && ret.data_custodian_account_id == jp.a && ret.data_custodian_premise_id == jp.p
                                            && ai.status == (int)Helper.AuthorizationStatus.Active
                                            select ai).FirstOrDefault();
                                ac.ApplicationId = gai.application_information_id;
                                if (auth != null)
                                {
                                    if (auth.refresh_token == null)
                                    {
                                        if (auth.authCodeExpiresIn > Helper.ConvertToUnixTimestamp(DateTime.UtcNow))
                                        {
                                            ac.Connected = true;
                                        }
                                        else
                                        {
                                            auth.status = (int)Helper.AuthorizationStatus.Expired;
                                            ent.Entry(auth).State = EntityState.Modified;
                                            ent.SaveChanges();
                                        }
                                    }
                                    else
                                    {
                                        if (auth.refreshTokenExpiresIn > Helper.ConvertToUnixTimestamp(DateTime.UtcNow))
                                        {
                                            ac.Connected = true;
                                        }
                                        else
                                        {
                                            auth.status = (int)Helper.AuthorizationStatus.Expired;
                                            ent.Entry(auth).State = EntityState.Modified;
                                            ent.SaveChanges();
                                        }
                                    }
                                }

                                gai.termsAndConditions = CMS.ReplaceGlobalVariable(CMS.GetWidgetContent(greenButtonTextContent, tabKey, widgetType, "termsandconditions"), jp);
                                gai.termsAndConditions = gai.termsAndConditions.Replace("{%thirdpartyvendorname%}", gai.client_name);
                                gai.termsAndConditions = gai.termsAndConditions.Replace("{%thirdpartyapplicationuse%}", thirdPartyAppUseText);

                                acl.Add(ac);

                                var scope = (from ais in ent.application_information_scopes
                                             where ais.application_information_id == gai.application_information_id
                                             select ais).FirstOrDefault();

                                if (scope == null) continue;
                                var scopes = scope.scope.Split(' ').ToList();
                                var scopeStr = scopes.Aggregate(string.Empty, (current, s) => current + "&scope=" + s);
                                if (gai.environment_type.ToLower() == Enums.EnvironmentType.Internal.ToString().ToLower())
                                {
                                    gai.thirdPartyScopeSelectionScreenURI = ConfigurationManager.AppSettings.Get("CEInsightsWebBaseURL") + gai.thirdPartyScopeSelectionScreenURI +
                                                                            "?DataCustodianId=" + gai.dataCustodianId + scopeStr + "&tpid=" + gai.client_id;
                                }
                                else
                                {
                                    gai.thirdPartyScopeSelectionScreenURI = gai.thirdPartyScopeSelectionScreenURI + "?DataCustodianId=" + gai.dataCustodianId + scopeStr;
                                }
                            }

                            gba.GreenButtonApplicationConnected = acl;

                            if (gba.GreenButtonApplicationInformation.Count == 0)
                            {
                                gba.NoThirdParties = true;
                                gba.NoThirdPartiesMessage = CMS.GetWidgetContent(greenButtonTextContent, tabKey, widgetType, "nothirdpartiesmessage");
                            }

                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                        }
                    }
                }

                transScope.Complete();
            }

            //Content        
            gba.TermsAndConditionsTitle = CMS.GetWidgetContent(greenButtonTextContent, tabKey, widgetType, "termsandconditionstitle");
            gba.TermsAndConditionsLinkText = CMS.GetWidgetContent(greenButtonTextContent, tabKey, widgetType, "termsandconditionslinktext");
            gba.TermsAndConditionsAgreeText = CMS.GetWidgetContent(greenButtonTextContent, tabKey, widgetType, "termsandconditionsagreetext");
            gba.CookieMessage = CMS.GetWidgetContent(greenButtonTextContent, tabKey, widgetType, "cookiemessage");
            gba.ConnectButton = CMS.GetWidgetContent(greenButtonTextContent, tabKey, widgetType, "connectbutton");
            gba.DisconnectButton = CMS.GetWidgetContent(greenButtonTextContent, tabKey, widgetType, "disconnectbutton");
            gba.DisconnectMessage = CMS.GetWidgetContent(greenButtonTextContent, tabKey, widgetType, "disconnectmessage");

            gba.GreenButtonConnectLogo = CMS.GetWidgetConfig(greenButtonConfig, tabKey, widgetType, "greenbuttonconnectlogo");
            gba.ClientLogo = CMS.GetWidgetConfig(greenButtonConfig, tabKey, widgetType, "clientlogo");

            return gba;
        }

        public static List<GreenButtonAdmin> GetAllRegisteredApplications(string clientId, string environment)
        {
            var gba = new List<GreenButtonAdmin>();

            //declare the transaction options
            var transactionOptions = new TransactionOptions
            {
                IsolationLevel = IsolationLevel.ReadUncommitted
            };

            using (var transScope = new TransactionScope(TransactionScopeOption.Required, transactionOptions))
            {
                using (var ent = new InsightsEntities())
                {
                    ent.Database.Connection.Open();
                    using (var dbContextTransaction = ent.Database.BeginTransaction())
                    {
                        try
                        {
                            ent.Configuration.AutoDetectChangesEnabled = false;
                            ent.Configuration.ValidateOnSaveEnabled = false;
                            var dcId = Helper.Base64Encode(clientId + ":" + environment);

                            var gbaList = (from ai in ent.application_information
                                   where ai.dataCustodianId == dcId
                                   select ai).ToList();
                            foreach (var g in gbaList)
                            {
                                var ais = (from i in ent.application_information_scopes
                                    where i.application_information_id == g.application_information_id
                                    select i).FirstOrDefault();

                                var gb = new GreenButtonAdmin {GreenButtonApplicationInformation = g};
                                if (ais != null)
                                {
                                    gb.ScopeString = ais.scope;
                                }
                                gba.Add(gb);
                            }

                            dbContextTransaction.Commit();
                        }
                        catch (Exception)
                        {
                            dbContextTransaction.Rollback();
                        }
                    }
                }
                transScope.Complete();
            }

            return gba;
        }

        public static AuthorizePostResponse IssueClientAuthCode(string thirdPartyClientId, string parameters, string scopes, string redirectUri, string responseType = "code")
        {
            var apr = new AuthorizePostResponse();

            if (responseType != ResponseType)
            {
                apr.Error = ErrorCodeUnsupportedResponseType;
                apr.Error_Description = ErrorDescUnsupportedResponseType;
                return apr;
            }
            if (string.IsNullOrEmpty(redirectUri) || !char.IsLetter(redirectUri[0]))
            {
                apr.Error = ErrorCodeInvalidRedirectUri;
                apr.Error_Description = ErrorDescInvalidRedirectUri;
                return apr;
            }
            if (redirectUri.Substring(0, 5).ToLower() != "https")
            {
                apr.Error = ErrorCodeInsecureRedirectUri;
                apr.Error_Description = ErrorDescInsecureRedirectUri;
                return apr;
            }

            //declare the transaction options
            var transactionOptions = new TransactionOptions
            {
                IsolationLevel = IsolationLevel.ReadUncommitted
            };
            var jp = (JsonParams)JsonConvert.DeserializeObject(QueryStringModule.Decrypt(parameters), typeof(JsonParams));
            List<application_information> client;
            retail_customers customer;
            authorization authRecord = null;
            using (var transScope = new TransactionScope(TransactionScopeOption.Required, transactionOptions))
            {
                using (var ent = new InsightsEntities())
                {
                    ent.Database.Connection.Open();
                    client = (from ai in ent.application_information
                              where ai.client_id == thirdPartyClientId && ai.enabled == true
                              select ai).ToList();
                    if (client.Count == 0)
                    {
                        transScope.Complete();
                        apr.Error = ErrorCodeInvalidClient;
                        apr.Error_Description = ErrorDescInvalidClient.Replace("{%client_id%}", thirdPartyClientId);
                        return apr;
                    }
                    if (client.FirstOrDefault().environment_type.ToLower() != Enums.EnvironmentType.Internal.ToString().ToLower() && client.FirstOrDefault().redirect_uri != redirectUri)
                    {
                        transScope.Complete();
                        apr.Error = ErrorCodeRedirectUriMismatch;
                        apr.Error_Description = ErrorDescRedirectUriMismatch;
                        return apr;
                    }
                    var clId = Convert.ToInt32(jp.cl);
                    customer = (from c in ent.retail_customers
                                where c.data_custodian_client_id == clId && c.data_custodian_customer_id == jp.c
                                && c.data_custodian_account_id == jp.a && c.data_custodian_premise_id == jp.p
                                select c).FirstOrDefault();
                    var cl = client.FirstOrDefault();
                    if (customer != null)
                    {
                        authRecord = (from au in ent.authorizations
                                      where au.application_information_id == cl.application_information_id && au.third_party == ThirdParty && au.retail_customer_id == customer.id && au.status == (int)Helper.AuthorizationStatus.Active
                                      select au).FirstOrDefault();
                    }
                    var scopesInDb = (from ais in ent.application_information_scopes
                                      where ais.application_information_id == cl.application_information_id
                                      select ais).FirstOrDefault().scope;

                    if (scopesInDb != scopes)
                    {
                        var scStr = scopes.Split(' ').Where(x => !string.IsNullOrEmpty(x)).ToArray();
                        foreach (var scStr1 in scStr)
                        {
                            var scDb = scopesInDb.Split(' ').Where(x => !string.IsNullOrEmpty(x)).ToArray();
                            foreach (var scDb1 in scDb)
                            {
                                var scStr2 = scStr1.Split(';').Where(x => !string.IsNullOrEmpty(x)).ToArray();
                                foreach (var scStr3 in scStr2)
                                {
                                    var scDb2 = scDb1.Split(';').Where(x => !string.IsNullOrEmpty(x)).ToArray();
                                    foreach (var scDb3 in scDb2)
                                    {
                                        var sSpl = scDb3.Split('=')[1].Split('_').Where(x => !string.IsNullOrEmpty(x)).ToArray();
                                        string[] match = null;

                                        if (scDb3.Split('=')[0] == scStr3.Split('=')[0])
                                        {
                                            match = scStr3.Split('=')[1].Split('_').Where(x => !string.IsNullOrEmpty(x)).ToArray();
                                        }

                                        if (match == null) continue;
                                        foreach (var m in match)
                                        {
                                            var notMatched = false;
                                            foreach (var sp in sSpl)
                                            {
                                                if (m != sp) continue;
                                                notMatched = true;
                                                break;
                                            }
                                            if (notMatched) continue;
                                            transScope.Complete();
                                            apr.Error = ErrorCodeInvalidScope;
                                            apr.Error_Description = ErrorDescInvalidScope;
                                            return apr;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                transScope.Complete();
            }

            //Save auth code
            using (var transScope = new TransactionScope(TransactionScopeOption.Required, transactionOptions))
            {
                using (var ent = new InsightsEntities())
                {
                    ent.Database.Connection.Open();
                    using (var dbContextTransaction = ent.Database.BeginTransaction())
                    {
                        try
                        {
                            ent.Configuration.AutoDetectChangesEnabled = false;
                            ent.Configuration.ValidateOnSaveEnabled = false;

                            var comList = new List<string>();
                            var strSpl = scopes.Split(';');
                            foreach (var sp in strSpl)
                            {
                                if (!sp.Contains("FB")) continue;
                                var spl = sp.Split('=')[1].Split('_');
                                foreach (var sp1 in spl)
                                {
                                    if (sp1 == "05" || sp1 == "5")
                                    {
                                        comList.Add("electric");
                                    }
                                    if (sp1 == "10")
                                    {
                                        comList.Add("gas");
                                    }
                                    if (sp1 == "11")
                                    {
                                        comList.Add("water");
                                    }
                                }
                            }

                            //call customer info api to get name and meter's
                            var customerInfo = CustomerInfoModels.GetCustomerInfoFromCache(jp);
                            if (customerInfo?.Customer == null || customerInfo.Customer.Accounts.Count == 0)
                            {
                                apr.Error = ErrorCodeNoDataFound;
                                apr.Error_Description = ErrorDescNoDataFound;
                                return apr;
                            }
                            var meters = customerInfo.Customer.Accounts.Where(x => x.Id == jp.a)
                                                            .SelectMany(c => c.Premises)
                                                            .Where(p => p.Id == jp.p)
                                                            .SelectMany(s => s.Service)
                                                            .Where(c => comList.Contains(c.CommodityKey))
                                                            .SelectMany(c => c.ServicePoints)
                                                            .SelectMany(c => c.Meters).Select(x => x.Id).ToList();
                            if (meters.Count == 0)
                            {
                                apr.Error = ErrorCodeNoDataFound;
                                apr.Error_Description = ErrorDescNoDataFound;
                                return apr;
                            }

                            long retailCustId;
                            if (customer == null)
                            {
                                //create a record in retail customer table
                                var rc = new retail_customers
                                {
                                    data_custodian_client_id = Convert.ToInt32(jp.cl),
                                    data_custodian_customer_id = jp.c,
                                    data_custodian_account_id = jp.a,
                                    data_custodian_premise_id = jp.p,
                                    first_name = string.IsNullOrEmpty(customerInfo.Customer.FirstName) ? "" : customerInfo.Customer.FirstName,
                                    last_name = string.IsNullOrEmpty(customerInfo.Customer.LastName) ? "" : customerInfo.Customer.LastName,
                                    enabled = true
                                };

                                ent.Entry(rc).State = EntityState.Added;
                                ent.retail_customers.Add(rc);
                                ent.SaveChanges();
                                retailCustId = rc.id;
                            }
                            else
                            {
                                retailCustId = customer.id;
                            }

                            if (authRecord == null)
                            {
                                apr.code = Helper.GetHash(client.FirstOrDefault().client_secret + DateTime.UtcNow.ToShortDateString() + DateTime.UtcNow.ToLongTimeString());
                                //create a record in authorization table
                                var a = new authorization
                                {
                                    code = apr.code,
                                    published = DateTime.UtcNow,
                                    self_link_rel = "self",
                                    up_link_rel = "up",
                                    updated = DateTime.UtcNow,
                                    uuid = "",
                                    authorization_uri = "/Datacustodian/espi/1_1/resource/Authorization/",
                                    ap_duration = Helper.GetDuration(4),
                                    ap_start = Helper.ConvertToUnixTimestamp(DateTime.UtcNow.AddYears(-2)),
                                    expiresin = Helper.ConvertToUnixTimestamp(DateTime.UtcNow.AddYears(2)),
                                    grant_type = "authorization_code",
                                    pp_duration = 0,
                                    pp_start = 0,
                                    resourceURI = "/Datacustodian/espi/1_1/resource/ApplicationInfo/" + client.FirstOrDefault().application_information_id,
                                    scope = scopes,
                                    application_information_id = client.FirstOrDefault().application_information_id,
                                    retail_customer_id = retailCustId,
                                    third_party = ThirdParty,
                                    authCodeExpiresIn = Helper.ConvertToUnixTimestamp(DateTime.UtcNow.AddMinutes(5)),
                                    status = (int)Helper.AuthorizationStatus.Active
                                };

                                ent.Entry(a).State = EntityState.Added;
                                ent.authorizations.Add(a);
                                ent.SaveChanges();

                                //create a subscription record
                                var s = new subscription
                                {
                                    self_link_href = "",
                                    self_link_rel = "self",
                                    up_link_href = "/Datacustodian/espi/1_1/resource/Batch/Subscription",
                                    up_link_rel = "up",
                                    uuid = "",
                                    application_information_id = client.FirstOrDefault().application_information_id,
                                    retail_customer_id = retailCustId,
                                    authorization_id = a.id,
                                    published = DateTime.UtcNow,
                                    updated = DateTime.UtcNow
                                };

                                ent.Entry(s).State = EntityState.Added;
                                ent.subscriptions.Add(s);
                                ent.SaveChanges();

                                s.self_link_href = "/Datacustodian/espi/1_1/resource/Batch/Subscription/" + s.id;
                                var genSubs = new IdGenerator(Convert.ToInt32(jp.cl), jp.a, s.id);
                                s.uuid = genSubs.SubscriptionId;
                                ent.Entry(s).State = EntityState.Modified;
                                ent.SaveChanges();

                                //Update record with authorization uri, subscription id
                                var genAuth = new IdGenerator(Convert.ToInt32(jp.cl), jp.a, a.id);
                                a.uuid = genAuth.AuthorizationId;
                                a.authorization_uri = "/Datacustodian/espi/1_1/resource/Authorization/" + a.id;
                                a.resourceURI = "/Datacustodian/espi/1_1/resource/Batch/Subscription/" + s.id;
                                a.subscription_id = s.id;
                                ent.Entry(a).State = EntityState.Modified;
                                ent.SaveChanges();

                                // create entry in usage point table
                                foreach (var m in meters)
                                {
                                    var genUsage = new IdGenerator(Convert.ToInt32(jp.cl), jp.a, m);
                                    var up = new usage_points
                                    {
                                        uuid = genUsage.UsagePointId.Split(':')[2],
                                        self_link_href = "/Datacustodian/espi/1_1/resource/Subscription/" + s.id + "/UsagePoint/" + QueryStringModule.ConvertStringToHex(m, Encoding.Unicode),
                                        self_link_rel = "self",
                                        up_link_href = "/Datacustodian/espi/1_1/resource/Subscription/" + s.id + "/UsagePoint",
                                        up_link_rel = "up",
                                        meterid = m,
                                        published = DateTime.UtcNow,
                                        updated = DateTime.UtcNow
                                    };

                                    ent.Entry(up).State = EntityState.Added;
                                    ent.usage_points.Add(up);
                                    ent.SaveChanges();

                                    var sup = new subscription_usage_points
                                    {
                                        subscription_id = s.id,
                                        usage_point_id = up.id
                                    };

                                    ent.Entry(sup).State = EntityState.Added;
                                    ent.subscription_usage_points.Add(sup);
                                }
                                ent.SaveChanges();
                            }
                            else
                            {
                                apr.code = Helper.GetHash(client.FirstOrDefault().client_secret + DateTime.UtcNow.ToShortDateString() + DateTime.UtcNow.ToLongTimeString());
                                authRecord.code = apr.code;
                                authRecord.authCodeExpiresIn = Helper.ConvertToUnixTimestamp(DateTime.UtcNow.AddMinutes(5));

                                ent.Entry(authRecord).State = EntityState.Modified;
                                ent.SaveChanges();
                            }

                            dbContextTransaction.Commit();
                        }
                        catch (Exception)
                        {
                            dbContextTransaction.Rollback();
                        }
                    }
                }
                transScope.Complete();
            }

            return apr;
        }

        public static bool DisconnectThirdpartyApp(string thirdPartyClientId, string parameters)
        {
            //declare the transaction options
            var transactionOptions = new TransactionOptions
            {
                IsolationLevel = IsolationLevel.ReadUncommitted
            };
            var jp = (JsonParams)JsonConvert.DeserializeObject(QueryStringModule.Decrypt(parameters), typeof(JsonParams));
            using (var transScope = new TransactionScope(TransactionScopeOption.Required, transactionOptions))
            {
                using (var ent = new InsightsEntities())
                {
                    ent.Database.Connection.Open();
                    using (var dbContextTransaction = ent.Database.BeginTransaction())
                    {
                        try
                        {
                            ent.Configuration.AutoDetectChangesEnabled = false;
                            ent.Configuration.ValidateOnSaveEnabled = false;

                            var client = (from ai in ent.application_information
                                          where ai.client_id == thirdPartyClientId
                                          select ai).FirstOrDefault();
                            var clId = Convert.ToInt32(jp.cl);
                            var customer = (from c in ent.retail_customers
                                            where c.data_custodian_client_id == clId &&
                                                  c.data_custodian_customer_id == jp.c
                                                  && c.data_custodian_account_id == jp.a &&
                                                  c.data_custodian_premise_id == jp.p
                                            select c).FirstOrDefault();

                            var authRecord = (from au in ent.authorizations
                                              where au.application_information_id == client.application_information_id &&
                                                    au.third_party == ThirdParty && au.retail_customer_id == customer.id && au.status == (int)Helper.AuthorizationStatus.Active
                                              select au).FirstOrDefault();

                            if (authRecord != null)
                            {
                                authRecord.status = (int)Helper.AuthorizationStatus.RevokedByRetailCustomer;
                                authRecord.end_date = DateTime.UtcNow;

                                ent.Entry(authRecord).State = EntityState.Modified;
                                ent.SaveChanges();
                            }
                            dbContextTransaction.Commit();
                        }
                        catch (Exception)
                        {
                            dbContextTransaction.Rollback();
                            return false;
                        }
                    }
                }
                transScope.Complete();
            }
            return true;
        }

        public static bool DisconnectThirdparty(string thirdPartyClientId)
        {
            //declare the transaction options
            var transactionOptions = new TransactionOptions
            {
                IsolationLevel = IsolationLevel.ReadUncommitted
            };

            using (var transScope = new TransactionScope(TransactionScopeOption.Required, transactionOptions))
            {
                using (var ent = new InsightsEntities())
                {
                    ent.Database.Connection.Open();
                    using (var dbContextTransaction = ent.Database.BeginTransaction())
                    {
                        try
                        {
                            ent.Configuration.AutoDetectChangesEnabled = false;
                            ent.Configuration.ValidateOnSaveEnabled = false;

                            var client = (from ai in ent.application_information
                                          where ai.client_id == thirdPartyClientId
                                          select ai).FirstOrDefault();
                            client.enabled = false;
                            client.thirdPartyApplicationStatus = (int)Helper.ThirdPartyApplicationStatus.Retired;
                            client.updated = DateTime.UtcNow;
                            ent.Entry(client).State = EntityState.Modified;

                            var authRecords = (from au in ent.authorizations
                                               where au.application_information_id == client.application_information_id && au.end_date == null
                                               select au).ToList();

                            foreach (var ar in authRecords)
                            {
                                ar.status = (int)Helper.AuthorizationStatus.RevokedByUtility;
                                ar.end_date = DateTime.UtcNow;
                                ent.Entry(ar).State = EntityState.Modified;
                            }

                            ent.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception)
                        {
                            dbContextTransaction.Rollback();
                            return false;
                        }
                    }
                }
                transScope.Complete();
            }
            return true;
        }

        #region Helper
        private static string GenerateRegistrationToken(string clientId, string secret)
        {
            var randomNumber = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture);
            var keySource = clientId + "," + secret + "," + randomNumber;
            return Helper.Base64Encode(Helper.Encrypt(keySource));
        }

        public static string GetEnvironment() => ConfigurationManager.AppSettings.Get(AppSettingCeEnvironment) == "localdev" ? "dev" : ConfigurationManager.AppSettings.Get(AppSettingCeEnvironment);

        #endregion
    }

    public class GreenButtonApplication : ContentBase
    {
        public bool NoThirdParties { get; set; }
        public string NoThirdPartiesMessage { get; set; }
        public string TermsAndConditionsTitle { get; set; }
        public string TermsAndConditionsLinkText { get; set; }
        public string TermsAndConditionsAgreeText { get; set; }
        public bool TermsAndConditionsAccepted { get; set; }
        public string ClientLogo { get; set; }
        public string GreenButtonConnectLogo { get; set; }
        public string CookieMessage { get; set; }
        public string ConnectButton { get; set; }
        public string DisconnectButton { get; set; }
        public string DisconnectMessage { get; set; }
        public string ScopeString { get; set; }
        public List<application_information> GreenButtonApplicationInformation { get; set; }
        public List<ApplicationsConnected> GreenButtonApplicationConnected { get; set; }
        public GreenButtonApplication(List<Configuration> config, List<TextContent> content, string tabKey, string widetType, JsonParams jp) : base(config, content, tabKey, widetType, jp)
        { }
        public GreenButtonApplication() { }
    }

    public class GreenButtonAdmin
    {
        public application_information GreenButtonApplicationInformation { get; set; }
        public string ScopeString { get; set; }
    }

    public class AuthorizePostResponse : StatusOAuth
    {
        /// <summary>
        /// The authorization code. 
        /// </summary>
        public string code { get; set; }
        /// <summary>
        /// The same value as sent by the client in the state parameter, if any.
        /// </summary>
        public string state { get; set; }
    }

    public class StatusOAuth
    {
        /// <summary>
        ///Required. Must be one of a set of predefined error codes. See the specification for the codes and their meaning.
        /// </summary>
        public string Error { get; set; }
        /// <summary>
        ///Optional. A human-readable UTF-8 encoded text describing the error. Intended for a developer, not an end user.
        /// </summary>
        public string Error_Description { get; set; }
        /// <summary>
        ///Optional. A URI pointing to a human-readable web page with information about the error.
        /// </summary>
        public string Error_Uri { get; set; }
        /// <summary>
        /// Required, if present in authorization request. The same value as sent in the state parameter in the request.
        /// </summary>
        public string State { get; set; }
    }

    public class ApplicationsConnected
    {
        public long ApplicationId { get; set; }
        public bool Connected { get; set; }
        public bool Disconnected { get; set; }
    }
}

