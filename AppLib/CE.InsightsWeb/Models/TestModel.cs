﻿//using System;
//using System.Collections.Generic;
//using System.Globalization;
//using System.Linq;
//using AutoMapper;
//using CE.InsightsWeb.Common;
//using CE.InsightsWeb.Common.AclaraJson;
//using CE.InsightsWeb.Common.QueryStringEncryptor;
//using CE.InsightsWeb.Helpers;
//using Newtonsoft.Json;
//using System.Web;
//using System.Web.Caching;
//using CE.InsightsWeb.Models;

//namespace CE.InsightsWeb.Models
//{
//    public class TestModels
//    {
//        private const string BENCHMARK_CATEGORY = "benchmark";
//        private const string COMMON_CATEGORY = "common";
//        private const string ELECTRIC_COMMODITY_KEY = "electric";
//        private const string GAS_COMMODITY_KEY = "gas";
//        private const string WATER_COMMODITY_KEY = "water";
//        private const string WIDGET_TYPE = "peercomparison";

        //public Benchmark GetBenchmark(string tabKey, string parameters)
        //{
        //    var bm = new Benchmark();
        //    var jp =
        //        (JsonParams)JsonConvert.DeserializeObject(QueryStringModule.Decrypt(parameters), typeof(JsonParams));
        //    //Content
        //    var content = CMS.GetContent(int.Parse(jp.cl), jp.l).Content;

        //    var benchmarkTextContent =
        //        content.TextContent.Where(x => x.category == BENCHMARK_CATEGORY || x.category == COMMON_CATEGORY)
        //            .ToList();
        //    var benchmarkConfig = content.Configuration.Where(x => x.category == BENCHMARK_CATEGORY).ToList();
        //    var benchmarkUOMContent = content.UOM.ToList();
        //    var benchmarkCurrencyContent = content.Currency.ToList();

        //    //Get Benchmarks
        //    var gKey = CMS.GetWidgetConfig(benchmarkConfig, tabKey, WIDGET_TYPE, "groupkey");
        //    var mKey = CMS.GetWidgetConfig(benchmarkConfig, tabKey, WIDGET_TYPE, "measurementkey");
        //    var benchmarkResponse = GetBenchmark(jp, gKey, mKey);

        //    Mapper.CreateMap<List<Common.AclaraJson.Benchmark>, Benchmark>()
        //        .ForMember(dest => dest.Benchmarks,
        //            opt => opt.MapFrom(
        //                src => Mapper.Map<List<Common.AclaraJson.Benchmark>, List<BenchmarkList>>(src)));
        //    Mapper.CreateMap<Common.AclaraJson.Benchmark, BenchmarkList>();
        //    Mapper.CreateMap<Common.AclaraJson.BenchmarkMeasurement, BenchmarkMeasurement>();

        //    if (benchmarkResponse.Customer == null)
        //    {
        //        bm.NoBenchmarks = true;
        //        bm.NoBenchmarksText = CMS.GetWidgetContent(benchmarkTextContent, tabKey, WIDGET_TYPE, "nobenchmark");
        //        return bm;
        //    }

        //    Mapper.Map(benchmarkResponse.Customer.Accounts[0].Premises[0].Benchmarks, bm);

        //    bm.ElectricServiceCount =
        //        benchmarkResponse.Customer.Accounts.SelectMany(x => x.Premises).SelectMany(x => x.Benchmarks)
        //            .Where(t => t.CommodityKey == ELECTRIC_COMMODITY_KEY)
        //            .Count();
        //    bm.GasServiceCount =
        //        benchmarkResponse.Customer.Accounts.SelectMany(x => x.Premises)
        //            .SelectMany(x => x.Benchmarks)
        //            .Where(t => t.CommodityKey == GAS_COMMODITY_KEY)
        //            .Count();
        //    bm.WaterServiceCount =
        //        benchmarkResponse.Customer.Accounts.SelectMany(x => x.Premises)
        //            .SelectMany(x => x.Benchmarks)
        //            .Where(t => t.CommodityKey == WATER_COMMODITY_KEY)
        //            .Count();
        //    bm.Commodities = CMS.GetWidgetConfig(benchmarkConfig, tabKey, WIDGET_TYPE, "commodities");

        //    var cFormat = CMS.GetWidgetConfig(benchmarkConfig, tabKey, WIDGET_TYPE, "currencyformat");
        //    var ci = new CultureInfo(jp.l);

        //    var uFormat = CMS.GetWidgetConfig(benchmarkConfig, tabKey, WIDGET_TYPE, "usageformat");

        //    foreach (var c in bm.Commodities.Replace(" ", "").Split(','))
        //    {
        //        var comNameKey = "commodity." + c + ".name";
        //        if (bm.CommoditiesText == null)
        //        {
        //            bm.CommoditiesText =
        //                benchmarkTextContent.Where(e => e.key == comNameKey).Select(s => s.shorttext).FirstOrDefault();
        //        }
        //        else
        //        {
        //            bm.CommoditiesText += "," +
        //                                  benchmarkTextContent.Where(e => e.key == comNameKey)
        //                                      .Select(s => s.shorttext)
        //                                      .FirstOrDefault();
        //        }

        //        var benchMark = bm.Benchmarks.Where(t => t.CommodityKey == c).FirstOrDefault();

        //        if (benchMark != null)
        //        {
        //            var unit = benchMark.UOMKey;
        //            var ckey = benchMark.CostCurrencyKey;

        //            benchMark.UOMText = benchmarkTextContent.Where(
        //                z => z.key == benchmarkUOMContent.Where(x => x.key == unit).FirstOrDefault().namekey)
        //                .FirstOrDefault()
        //                .shorttext;

        //            benchMark.CostCurrencySymbol = benchmarkTextContent.Where(
        //                z => z.key == benchmarkCurrencyContent.Where(x => x.key == ckey).FirstOrDefault().symbolkey)
        //                .FirstOrDefault()
        //                .shorttext;

        //            benchMark.MyUsageFormatted = benchMark.MyUsage.ToString(uFormat, ci);
        //            benchMark.AverageUsageFormatted = benchMark.AverageUsage.ToString(uFormat, ci);
        //            benchMark.EfficientUsageFormatted = benchMark.EfficientUsage.ToString(uFormat, ci);

        //            benchMark.MyCostFormatted = benchMark.MyCost.ToString(cFormat, ci);
        //            benchMark.AverageCostFormatted = benchMark.AverageCost.ToString(cFormat, ci);
        //            benchMark.EfficientCostFormatted = benchMark.EfficientCost.ToString(cFormat, ci);

        //            if (benchMark.Measurements != null && benchMark.Measurements.Count > 0)
        //            {
        //                benchMark.Measurements.FirstOrDefault().MyQuantityFormatted =
        //                    benchMark.Measurements.FirstOrDefault().MyQuantity.ToString(uFormat, ci);
        //                benchMark.Measurements.FirstOrDefault().AverageQuantityFormatted =
        //                    benchMark.Measurements.FirstOrDefault().AverageQuantity.ToString(uFormat, ci);
        //                benchMark.Measurements.FirstOrDefault().EfficientQuantityFormatted =
        //                    benchMark.Measurements.FirstOrDefault().EfficientQuantity.ToString(uFormat, ci);
        //                benchMark.Measurements.FirstOrDefault().UOMText = benchmarkTextContent.Where(
        //                    z =>
        //                        z.key ==
        //                        benchmarkUOMContent.Where(x => x.key == benchMark.Measurements.FirstOrDefault().UOMKey)
        //                            .FirstOrDefault()
        //                            .namekey)
        //                    .FirstOrDefault()
        //                    .shorttext;
        //            }
        //        }
        //    }

        //    //Content
        //    bm.IntroText = CMS.GetWidgetContent(benchmarkTextContent, tabKey, WIDGET_TYPE, "introtext");
        //    bm.Title = CMS.GetWidgetContent(benchmarkTextContent, tabKey, WIDGET_TYPE, "title");
        //    bm.SubTitle = CMS.GetWidgetContent(benchmarkTextContent, tabKey, WIDGET_TYPE, "subtitle");
        //    bm.YourQty = CMS.GetWidgetContent(benchmarkTextContent, tabKey, WIDGET_TYPE, "yourqty");
        //    bm.AverageQty = CMS.GetWidgetContent(benchmarkTextContent, tabKey, WIDGET_TYPE, "averageqty");
        //    bm.EfficientQty = CMS.GetWidgetContent(benchmarkTextContent, tabKey, WIDGET_TYPE, "efficientqty");
        //    bm.Footer = CMS.GetWidgetContent(benchmarkTextContent, tabKey, WIDGET_TYPE, "footer");
        //    bm.FooterLinkText = CMS.GetWidgetContent(benchmarkTextContent, tabKey, WIDGET_TYPE, "footerlinktext");
        //    //Configuration  
        //    bm.ShowTitle = CMS.GetWidgetConfig(benchmarkConfig, tabKey, WIDGET_TYPE, "showtitle");
        //    bm.ShowIntro = CMS.GetWidgetConfig(benchmarkConfig, tabKey, WIDGET_TYPE, "showintro");
        //    bm.CostOrUsage = CMS.GetWidgetConfig(benchmarkConfig, tabKey, WIDGET_TYPE, "costorusage");
        //    bm.YourQtyColor = CMS.GetWidgetConfig(benchmarkConfig, tabKey, WIDGET_TYPE, "yourqtycolor");
        //    bm.AverageQtyColor = CMS.GetWidgetConfig(benchmarkConfig, tabKey, WIDGET_TYPE, "averageqtycolor");
        //    bm.EfficientQtyColor = CMS.GetWidgetConfig(benchmarkConfig, tabKey, WIDGET_TYPE, "efficientqtycolor");
        //    bm.FooterLink =
        //        GeneralHelper.CreateLink(CMS.GetWidgetConfig(benchmarkConfig, tabKey, WIDGET_TYPE, "footerlinktype"), jp,
        //            CMS.GetWidgetConfig(benchmarkConfig, tabKey, WIDGET_TYPE, "footerlink"));
        //    bm.ShowFooterLink = CMS.GetWidgetConfig(benchmarkConfig, tabKey, WIDGET_TYPE, "showfooterlink");
        //    bm.ShowFooterText = CMS.GetWidgetConfig(benchmarkConfig, tabKey, WIDGET_TYPE, "showfootertext");

        //    return bm;
        //}

        //private BenchmarkRoot GetBenchmark(JsonParams jp, string groupKeys, string measurementKeys)
        //{
        //    var benchmarkResponse = new BenchmarkRoot();
        //    var rm = new APIResponseModels();
        //    var sdate = DateTime.UtcNow.AddYears(-1).ToString("yyyy-MM-dd");
        //    var edate = DateTime.UtcNow.ToString("yyyy-MM-dd");
        //    var requestParams = "CustomerId=" + jp.c + "&AccountId=" + jp.a + "&PremiseId=" + jp.p + "&StartDate=" +
        //                        sdate + "&EndDate=" +
        //                        edate + "&Count=1&GroupKeys=" + groupKeys + "&MeasurementKeys=" + measurementKeys +
        //                        "&IncludeContent=false";
        //    benchmarkResponse =
        //        (BenchmarkRoot)
        //            JsonConvert.DeserializeObject(rm.GetBenchmark(requestParams, jp.l, int.Parse(jp.cl)).Content,
        //                typeof(BenchmarkRoot));
        //    return benchmarkResponse;
        //}

        //private const string ACTION_CATEGORY = "action";
        //private const string LINKS_CATEGORY = "quicklinks";
        //private const string LINKS_TYPE = "quicklink";
        //private const string WIDGET_TYPEQ = "quicklinks";

        //public QuickLinks GetQuickLinks(string tabKey, string parameters)
        //{
        //    var q = new QuickLinks();
        //    var jp =
        //        (JsonParams)JsonConvert.DeserializeObject(QueryStringModule.Decrypt(parameters), typeof(JsonParams));
        //    //Content
        //    var content = CMS.GetContent(int.Parse(jp.cl), jp.l).Content;
        //    var quickLinksTextContent =
        //        content.TextContent.Where(
        //            x => x.category == ACTION_CATEGORY || x.category == LINKS_CATEGORY || x.category == COMMON_CATEGORY)
        //            .ToList();
        //    var actionContent = content.Action.Where(x => x.type == LINKS_TYPE).ToList();
        //    var actionFileContent = content.FileContent.Where(x => x.category == ACTION_CATEGORY).ToList();
        //    var quickLinksConfig = content.Configuration.Where(x => x.category == LINKS_CATEGORY).ToList();

        //    //Get QuickLinks 
        //    var insightsResponse = GetQuickLinksFromCache(jp);

        //    var lnks = CMS.GetWidgetConfig(quickLinksConfig, tabKey, WIDGET_TYPEQ, "links");
        //    var sqSplit = new List<string>(lnks.Split(';'));

        //    var actions = (from pa in insightsResponse.Customer.Accounts[0].Premises[0].Actions.AsQueryable()
        //                   where sqSplit.Any(key => pa.Key.Equals(key))
        //                   select pa).OrderBy(x => sqSplit.IndexOf(x.Key)).ToList();

        //    var am = new ActionMerge();
        //    am.Actions = actions;
        //    am.ActionContent = actionContent;
        //    am.TextContent = quickLinksTextContent.Where(x => x.category == ACTION_CATEGORY).ToList();
        //    am.FileContent = actionFileContent;
        //    am.QueryString = jp;
        //    var il = new List<Link>();
        //    Mapper.CreateMap<ActionMerge, List<Link>>().ConvertUsing(new QuickLinksConverter());
        //    q.LinkList = Mapper.Map(am, il);

        //    //Content
        //    q.IntroText = CMS.GetWidgetContent(quickLinksTextContent, tabKey, WIDGET_TYPEQ, "introtext");
        //    q.Title = CMS.GetWidgetContent(quickLinksTextContent, tabKey, WIDGET_TYPEQ, "title");
        //    q.SubTitle = CMS.GetWidgetContent(quickLinksTextContent, tabKey, WIDGET_TYPEQ, "subtitle");
        //    q.Footer = CMS.GetWidgetContent(quickLinksTextContent, tabKey, WIDGET_TYPEQ, "footer");
        //    q.FooterLinkText = CMS.GetWidgetContent(quickLinksTextContent, tabKey, WIDGET_TYPEQ, "footerlinktext");
        //    //Configuration  
        //    q.ShowTitle = CMS.GetWidgetConfig(quickLinksConfig, tabKey, WIDGET_TYPEQ, "showtitle");
        //    q.ShowIntro = CMS.GetWidgetConfig(quickLinksConfig, tabKey, WIDGET_TYPEQ, "showintro");
        //    q.ShowSubTitle = CMS.GetWidgetConfig(quickLinksConfig, tabKey, WIDGET_TYPEQ, "showsubtitle");
        //    q.ShowIcons = CMS.GetWidgetConfig(quickLinksConfig, tabKey, WIDGET_TYPEQ, "showicons");
        //    q.FooterLink =
        //        GeneralHelper.CreateLink(CMS.GetWidgetConfig(quickLinksConfig, tabKey, WIDGET_TYPEQ, "footerlinktype"),
        //            jp, CMS.GetWidgetConfig(quickLinksConfig, tabKey, WIDGET_TYPEQ, "footerlink"));
        //    q.ShowFooterLink = CMS.GetWidgetConfig(quickLinksConfig, tabKey, WIDGET_TYPEQ, "showfooterlink");
        //    q.ShowFooterText = CMS.GetWidgetConfig(quickLinksConfig, tabKey, WIDGET_TYPEQ, "showfootertext");

        //    return q;
        //}

        //private ActionRoot GetQuickLinksFromCache(JsonParams jp)
        //{
        //    var insightsResponse = new ActionRoot();
        //    if (
        //        HttpRuntime.Cache[
        //            "QuickLinksResponse" + jp.cl + "" + jp.c + "" + jp.a + "" + jp.p + "" + jp.s + "" + jp.l + "" + jp.w
        //            ] ==
        //        null)
        //    {
        //        var rm = new APIResponseModels();
        //        var requestParams = "CustomerId=" + jp.c + "&AccountId=" + jp.a + "&PremiseId=" + jp.p +
        //                            "&Types=quicklink";
        //        insightsResponse =
        //            (ActionRoot)
        //                JsonConvert.DeserializeObject(rm.GetAction(requestParams, jp.l, int.Parse(jp.cl)).Content,
        //                    typeof(ActionRoot));
        //        HttpRuntime.Cache.Insert(
        //            "QuickLinksResponse" + jp.cl + "" + jp.c + "" + jp.a + "" + jp.p + "" + jp.s + "" + jp.l + "" + jp.w,
        //            insightsResponse,
        //            null, DateTime.Now.AddMinutes(Constants.kCache_Expiry), Cache.NoSlidingExpiration,
        //            CacheItemPriority.NotRemovable, null);
        //    }
        //    else
        //    {
        //        insightsResponse =
        //            (ActionRoot)
        //                HttpRuntime.Cache[
        //                    "QuickLinksResponse" + jp.cl + "" + jp.c + "" + jp.a + "" + jp.p + "" + jp.s + "" + jp.l +
        //                    "" + jp.w];
        //    }

        //    return insightsResponse;
        //}

        //private const string WIDGET_TYPEP = "profile";
        //private const string WIDGET_TYPE_LONG = "longprofile";

        //public Profile1 GetProfile(string tabKey, string parameters)
        //{
        //    var prof = new Profile1();
        //    var jp =
        //        (JsonParams)JsonConvert.DeserializeObject(QueryStringModule.Decrypt(parameters), typeof(JsonParams));
        //    var count = 0;
        //    var countLock = 0;
        //    var sectionList = new List<ProfileSection>();
        //    var content = CMS.GetContent(int.Parse(jp.cl), jp.l).Content;
        //    var profTextContent = content.TextContent.Where(x => x.category == WIDGET_TYPEP).ToList();
        //    var profConfig = content.Configuration.Where(x => x.category == WIDGET_TYPEP).ToList();
        //    var profFileContent = content.FileContent.Where(x => x.category == "action").ToList();
        //    var widgetKey =
        //        content.Widget.Where(x => x.widgettype == WIDGET_TYPEP && x.tabkey == tabKey).FirstOrDefault().key;
        //    //Get sections from tab configured for this widget
        //    var profSec =
        //        content.Tab.Where(x => x.tabtype == "widget" && x.widgetkey == widgetKey)
        //            .FirstOrDefault()
        //            .profilesections.Split(';');
        //    foreach (var s in profSec)
        //    {
        //        if (s.Length != 0)
        //        {
        //            sectionList.Add(content.ProfileSection.Where(x => x.key == s).FirstOrDefault());
        //        }
        //    }

        //    sectionList = sectionList.OrderBy(x => x.rank).ToList();
        //    var countSection = sectionList.Count();

        //    var profileResponse = GetProfileAttributesFromCache(jp);

        //    List<string> conditions = new List<string>();
        //    foreach (var sq in sectionList)
        //    {
        //        count += 1;

        //        var conditionList = new List<Condition>();
        //        // Condition from section
        //        if (sq.conditions.Length > 0)
        //        {
        //            var s = sq.conditions.Split(';');
        //            foreach (var i in s)
        //            {
        //                conditionList.Add(content.Condition.Where(x => x.key == i).FirstOrDefault());
        //            }
        //        }
        //        if (prof.Tabs == null || conditionList.Count > 0)
        //        {
        //            var sqSplit = new List<string>(sq.profileattributes.Split(';'));
        //            var missingAttr = new List<string>();
        //            //get attributes that don't exist in insights db-- Customer, Account and Premise
        //            if (profileResponse.Customer != null)
        //            {
        //                if (profileResponse.Customer.Accounts[0].Premises.Count > 0)
        //                {
        //                    missingAttr =
        //                        sqSplit.Where(
        //                            z => !profileResponse.Customer.Attributes.ToList().Any(z2 => z2.AttributeKey == z))
        //                            .ToList()
        //                            .Where(
        //                                z =>
        //                                    !profileResponse.Customer.Accounts[0].Attributes.ToList()
        //                                        .Any(z2 => z2.AttributeKey == z)).ToList()
        //                            .Where(
        //                                z =>
        //                                    !profileResponse.Customer.Accounts[0].Premises[0].Attributes.ToList()
        //                                        .Any(z2 => z2.AttributeKey == z)).ToList();
        //                }
        //                else
        //                {
        //                    missingAttr =
        //                        sqSplit.Where(
        //                            z => !profileResponse.Customer.Attributes.ToList().Any(z2 => z2.AttributeKey == z))
        //                            .ToList()
        //                            .Where(
        //                                z =>
        //                                    !profileResponse.Customer.Accounts[0].Attributes.ToList()
        //                                        .Any(z2 => z2.AttributeKey == z)).ToList();
        //                }
        //            }
        //            else
        //            {
        //                missingAttr = sqSplit;
        //            }

        //            //Create question list                  
        //            var qm = new QuestionMerge();
        //            qm.Attributes = (from pa in content.ProfileAttribute.AsQueryable()
        //                             where sqSplit.Any(key => pa.key.Equals(key))
        //                             select pa).OrderBy(x => sqSplit.IndexOf(x.key)).ToList();
        //            // Add condition attribute to get answer.
        //            foreach (var c in conditionList)
        //            {
        //                if (c.key != null)
        //                {
        //                    qm.Attributes.Add((from pa in content.ProfileAttribute.AsQueryable()
        //                                       where pa.key == c.profileattributekey
        //                                       select pa).FirstOrDefault());
        //                }
        //            }
        //            qm.Options = content.ProfileOption;
        //            qm.Text = profTextContent;
        //            qm.Profile = profileResponse;
        //            qm.TabKey = tabKey;
        //            qm.IsLongProfile = false;

        //            var q = new List<Question1>();
        //            var s = new List<Section1>();
        //            var t = new List<ProfileTab1>();

        //            Mapper.CreateMap<QuestionMerge, List<Question1>>().ConvertUsing(new QuestionConverter1());
        //            var mappedProf = Mapper.Map(qm, q);

        //            if (missingAttr.Count > 0 && prof.Tabs == null)
        //            {
        //                //Evaluate condition for this section   
        //                if (ValidateConditions(conditionList, mappedProf))
        //                {
        //                    var sec = new Section1();
        //                    sec.Questions = (mappedProf);
        //                    var sectTitle = profTextContent.Where(x => x.key == sq.titlekey).FirstOrDefault();
        //                    sec.SectionTitle = (sectTitle == null) ? "" : sectTitle.shorttext;
        //                    var sectSubTitle = profTextContent.Where(x => x.key == sq.subtitlekey).FirstOrDefault();
        //                    sec.SectionSubtitle = (sectSubTitle == null) ? "" : sectSubTitle.shorttext;
        //                    var sectDesc = profTextContent.Where(x => x.key == sq.descriptionkey).FirstOrDefault();
        //                    sec.SectionDescription = (sectDesc == null) ? "" : sectDesc.longtext;
        //                    var sectMark = profTextContent.Where(x => x.key == sq.introtextkey).FirstOrDefault();
        //                    sec.SectionMarkdown = (sectMark == null) ? "" : sectMark.longtext;
        //                    var imgContent = profFileContent.Where(x => x.key == sq.images).FirstOrDefault();
        //                    if (imgContent != null)
        //                    {
        //                        sec.ImageUrl = imgContent.mediumfile;
        //                        sec.ImageTitle = imgContent.mediumfiletitle;
        //                    }
        //                    s.Add(sec);

        //                    var tab = new ProfileTab1();
        //                    tab.Sections = s;
        //                    t.Add(tab);


        //                    prof.Tabs = t;
        //                    countLock = count - 1 - (sectionList.Count() - countSection);

        //                    foreach (var c in conditionList)
        //                    {
        //                        conditions.Add(c.profileattributekey);
        //                    }
        //                }
        //            }
        //            //Evaluate condition
        //            if (!ValidateConditions(conditionList, mappedProf))
        //            {
        //                countSection -= 1;
        //            }
        //        }
        //    }

        //    //All sections complete-- no questions selected
        //    if (countLock == 0 && prof.Tabs == null && sectionList.Count > 0)
        //    {
        //        prof.Completeness = 100;
        //    }
        //    else
        //    {
        //        if (countSection != 0)
        //        {
        //            prof.Completeness = (countLock * 100) / countSection;
        //        }
        //    }

        //    if (prof.Completeness == 100)
        //    {
        //        prof.Tabs = null;
        //        var eventAttribute = new Dictionary<string, string>();
        //        eventAttribute.Add("TabId", tabKey);
        //        var etm = new EventTrackingModels();
        //        JsonConvert.DeserializeObject(etm.PostEvents(eventAttribute, parameters, "Finish Profile"));
        //    }
        //    prof.CompletenessTitle = CMS.GetWidgetContent(profTextContent, tabKey, WIDGET_TYPEP, "completetitle");
        //    prof.CompletenessSubtitle = CMS.GetWidgetContent(profTextContent, tabKey, WIDGET_TYPEP, "completesubtitle");
        //    prof.CompletenessDescription = CMS.GetWidgetContent(profTextContent, tabKey, WIDGET_TYPEP, "completemessage");
        //    prof.CompletenessLabel = CMS.GetWidgetContent(profTextContent, tabKey, WIDGET_TYPEP, "completenesslabel");
        //    prof.NextButtonLabel = CMS.GetWidgetContent(profTextContent, tabKey, WIDGET_TYPEP, "nextbuttonlabel");
        //    prof.NotCompleteMessage = CMS.GetWidgetContent(profTextContent, tabKey, WIDGET_TYPEP, "notcompletemessage");
        //    prof.Title = CMS.GetWidgetContent(profTextContent, tabKey, WIDGET_TYPEP, "title");
        //    prof.IntroText = CMS.GetWidgetContent(profTextContent, tabKey, WIDGET_TYPEP, "introtext");
        //    prof.LoadingMessage =
        //        content.TextContent.Where(x => x.category == "common" && x.key == "common.loadingmessage")
        //            .FirstOrDefault()
        //            .shorttext;
        //    prof.DropDownSelectText =
        //        content.TextContent.Where(x => x.category == "common" && x.key == "common.selectfordropdown")
        //            .FirstOrDefault()
        //            .shorttext;
        //    prof.Footer = CMS.GetWidgetContent(profTextContent, tabKey, WIDGET_TYPEP, "footer");
        //    prof.FooterLinkText = CMS.GetWidgetContent(profTextContent, tabKey, WIDGET_TYPEP, "footerlinktext");
        //    prof.RequiredKey = CMS.GetWidgetContent(profTextContent, tabKey, WIDGET_TYPEP, "requiredkey");
        //    prof.RequiredSymbol = CMS.GetWidgetContent(profTextContent, tabKey, WIDGET_TYPEP, "requiredsymbol");
        //    prof.LongProfileLinkText = CMS.GetWidgetContent(profTextContent, tabKey, WIDGET_TYPEP, "longprofilelinktext");

        //    prof.ShowTitle = CMS.GetWidgetConfig(profConfig, tabKey, WIDGET_TYPEP, "showtitle");
        //    prof.ShowIntro = CMS.GetWidgetConfig(profConfig, tabKey, WIDGET_TYPEP, "showintro");
        //    prof.ShowSectionIntro = CMS.GetWidgetConfig(profConfig, tabKey, WIDGET_TYPEP, "showsectionintro");
        //    prof.FooterLink =
        //        GeneralHelper.CreateLink(CMS.GetWidgetConfig(profConfig, tabKey, WIDGET_TYPEP, "footerlinktype"), jp,
        //            CMS.GetWidgetConfig(profConfig, tabKey, WIDGET_TYPEP, "footerlink"));
        //    prof.ShowFooterLink = CMS.GetWidgetConfig(profConfig, tabKey, WIDGET_TYPEP, "showfooterlink");
        //    prof.ShowFooterText = CMS.GetWidgetConfig(profConfig, tabKey, WIDGET_TYPEP, "showfootertext");
        //    prof.ShowRequiredKey = CMS.GetWidgetConfig(profConfig, tabKey, WIDGET_TYPEP, "showrequiredkey");
        //    prof.ShowLongProfileLink = CMS.GetWidgetConfig(profConfig, tabKey, WIDGET_TYPEP, "showlongprofilelink");
        //    prof.LongProfileLink = GeneralHelper.CreateLink("internal", jp,
        //        CMS.GetWidgetConfig(profConfig, tabKey, WIDGET_TYPEP, "longprofilelink"));

        //    // remove the condition attribute from the final list.
        //    if (prof.Tabs != null)
        //    {
        //        foreach (var c in conditions)
        //        {
        //            prof.Tabs[0].Sections[0].Questions.Remove(
        //                prof.Tabs[0].Sections[0].Questions.Where(r => r.Key == c).FirstOrDefault());
        //        }
        //    }

        //    return prof;
        //}


//        public Profile1 GetLongProfile(string tabKey, string profileTabKey, string parameters)
//        {
//            var prof = new Profile1();
//            var jp = (JsonParams)JsonConvert.DeserializeObject(QueryStringModule.Decrypt(parameters), typeof(JsonParams));
//            var content = CMS.GetContent(int.Parse(jp.cl), jp.l).Content;
//            var widgetKey = content.Widget.Where(x => x.widgettype == WIDGET_TYPE_LONG && x.tabkey == tabKey).FirstOrDefault().key;
//            var tabs = content.Tab.Where(x => x.tabtype == "widget" && x.widgetkey == widgetKey).OrderBy(x => x.menuorder).ToList();
//            var profTextContent = content.TextContent.Where(x => x.category == WIDGET_TYPEP).ToList();
//            var profFileContent = content.FileContent.Where(x => x.category == "action").ToList();
//            var profConfig = content.Configuration.Where(x => x.category == WIDGET_TYPEP).ToList();

//            var profileResponse = GetProfileAttributesFromCache(jp);

//            //Check in tablist if profileTabkey exists, if not pick the first tab key and assign it to profileTabkey
//            if (tabs.Where(x => x.key == profileTabKey).FirstOrDefault() == null)
//            {
//                if (tabs.Count > 0)
//                {
//                    profileTabKey = tabs.First().key;
//                }
//            }
//            prof.OverviewTabId = CMS.GetWidgetConfig(profConfig, tabKey, WIDGET_TYPE_LONG, "overviewtabid");
//            List<string> conditions = new List<string>();
//            var t = new List<ProfileTab1>();
//            foreach (var ta in tabs)
//            {
//                var tab = new ProfileTab1();
//                tab.Key = ta.key;
//                var name = content.TextContent.Where(x => x.category == "common" && x.key == ta.namekey).FirstOrDefault();
//                tab.Name = (name == null) ? "" : name.shorttext;
//                tab.IconClass = ta.tabiconclass;
//                t.Add(tab);
//                prof.Tabs = t;

//                if (profileTabKey == tab.Key || profileTabKey == prof.OverviewTabId)
//                {
//                    // loop through tabs and get sections
//                    var sectionList = new List<ProfileSection>();
//                    //Get section from content
//                    var profSec = ta.profilesections.Split(';');
//                    foreach (var s in profSec)
//                    {
//                        if (!string.IsNullOrEmpty(s))
//                        {
//                            sectionList.Add(content.ProfileSection.Where(x => x.key == s).FirstOrDefault());
//                        }
//                    }

//                    sectionList = sectionList.OrderBy(x => x.rank).ToList();

//                    foreach (var sq in sectionList)
//                    {
//                        var conditionList = new List<Condition>();
//                        // Condition from section
//                        if (sq.conditions.Length > 0)
//                        {
//                            var seq = sq.conditions.Split(';');
//                            foreach (var i in seq)
//                            {
//                                conditionList.Add(content.Condition.Where(x => x.key == i).FirstOrDefault());
//                            }
//                        }

//                        var sqSplit = new List<string>(sq.profileattributes.Split(';'));
//                        //Create question list                  
//                        var qm = new QuestionMerge();
//                        qm.Attributes = (from pa in content.ProfileAttribute.AsQueryable()
//                                         where sqSplit.Any(key => pa.key.Equals(key))
//                                         select pa).OrderBy(x => sqSplit.IndexOf(x.key)).ToList();

//                        // Add condition attribute to get answer.
//                        foreach (var c in conditionList)
//                        {
//                            if (c.key != null)
//                            {
//                                qm.Attributes.Add((from pa in content.ProfileAttribute.AsQueryable()
//                                                   where pa.key == c.profileattributekey
//                                                   select pa).FirstOrDefault());
//                            }
//                        }
//                        qm.Options = content.ProfileOption;
//                        qm.Text = profTextContent;
//                        qm.Profile = profileResponse;
//                        qm.TabKey = tabKey;
//                        qm.IsLongProfile = true;

//                        var q = new List<Question1>();
//                        var s = new List<Section1>();
//                        Mapper.CreateMap<QuestionMerge, List<Question1>>().ConvertUsing(new QuestionConverter1());
//                        var mappedProf = Mapper.Map(qm, q);

//                        var sec = new Section1();
//                        var sectTitle = profTextContent.Where(x => x.key == sq.titlekey).FirstOrDefault();
//                        sec.SectionTitle = (sectTitle == null) ? "" : sectTitle.shorttext;
//                        var sectSubtitle = profTextContent.Where(x => x.key == sq.subtitlekey).FirstOrDefault();
//                        sec.SectionSubtitle = (sectSubtitle == null) ? "" : sectSubtitle.shorttext;
//                        var sectDesc = profTextContent.Where(x => x.key == sq.descriptionkey).FirstOrDefault();
//                        sec.SectionDescription = (sectDesc == null) ? "" : sectDesc.longtext;
//                        var sectMark = profTextContent.Where(x => x.key == sq.introtextkey).FirstOrDefault();
//                        sec.SectionMarkdown = (sectMark == null) ? "" : sectMark.longtext;
//                        sec.Key = sq.key;

//                        var imgContent = profFileContent.Where(x => x.key == sq.images).FirstOrDefault();
//                        if (imgContent != null)
//                        {
//                            sec.ImageUrl = imgContent.mediumfile;
//                            sec.ImageTitle = imgContent.mediumfiletitle;
//                        }

//                        foreach (var c in conditionList)
//                        {
//                            conditions.Add(c.profileattributekey);
//                        }

//                        if (ValidateConditions(conditionList, mappedProf))
//                        {
//                            sec.Questions = (mappedProf);
//                            foreach (var c in conditions)
//                            {
//                                sec.Questions.Remove(sec.Questions.Where(r => r.Key == c).FirstOrDefault());
//                            }

//                            if (prof.Tabs.Where(x => x.Key == ta.key).FirstOrDefault().Sections == null)
//                            {
//                                s.Add(sec);
//                                prof.Tabs.Where(x => x.Key == ta.key).FirstOrDefault().Sections = s;
//                            }
//                            else
//                            {
//                                prof.Tabs.Where(x => x.Key == ta.key).FirstOrDefault().Sections.Add(sec);
//                            }
//                        }
//                    }
//                }
//            }
//            prof.NextButtonLabel = CMS.GetWidgetContent(profTextContent, tabKey, WIDGET_TYPE_LONG, "nextbuttonlabel");
//            prof.NotCompleteMessage = CMS.GetWidgetContent(profTextContent, tabKey, WIDGET_TYPE_LONG, "notcompletemessage");
//            prof.BackButtonLabel = CMS.GetWidgetContent(profTextContent, tabKey, WIDGET_TYPE_LONG, "backbuttonlabel");
//            prof.LoadingMessage = content.TextContent.Where(x => x.category == "common" && x.key == "common.loadingmessage").FirstOrDefault().shorttext;
//            prof.DropDownSelectText = content.TextContent.Where(x => x.category == "common" && x.key == "common.selectfordropdown").FirstOrDefault().shorttext;
//            prof.Title = CMS.GetWidgetContent(profTextContent, tabKey, WIDGET_TYPE_LONG, "title");
//            prof.IntroText = CMS.GetWidgetContent(profTextContent, tabKey, WIDGET_TYPE_LONG, "introtext");
//            prof.Footer = CMS.GetWidgetContent(profTextContent, tabKey, WIDGET_TYPE_LONG, "footer");
//            prof.FooterLinkText = CMS.GetWidgetContent(profTextContent, tabKey, WIDGET_TYPE_LONG, "footerlinktext");
//            prof.OverviewText = CMS.GetWidgetContent(profTextContent, tabKey, WIDGET_TYPE_LONG, "overviewtextkey");
//            prof.SaveButtonLabel = CMS.GetWidgetContent(profTextContent, tabKey, WIDGET_TYPE_LONG, "savebuttonlabel");
//            prof.PageOf = CMS.GetWidgetContent(profTextContent, tabKey, WIDGET_TYPE_LONG, "pageof");
//            prof.SavedMessage = CMS.GetWidgetContent(profTextContent, tabKey, WIDGET_TYPE_LONG, "savedmessage");

//            prof.ShowTitle = CMS.GetWidgetConfig(profConfig, tabKey, WIDGET_TYPE_LONG, "showtitle");
//            prof.ShowIntro = CMS.GetWidgetConfig(profConfig, tabKey, WIDGET_TYPE_LONG, "showintro");
//            prof.ShowSectionIntro = CMS.GetWidgetConfig(profConfig, tabKey, WIDGET_TYPE_LONG, "showsectionintro");
//            prof.FooterLink = GeneralHelper.CreateLink(CMS.GetWidgetConfig(profConfig, tabKey, WIDGET_TYPE_LONG, "footerlinktype"), jp, CMS.GetWidgetConfig(profConfig, tabKey, WIDGET_TYPE_LONG, "footerlink"));
//            prof.ShowFooterLink = CMS.GetWidgetConfig(profConfig, tabKey, WIDGET_TYPE_LONG, "showfooterlink");
//            prof.ShowFooterText = CMS.GetWidgetConfig(profConfig, tabKey, WIDGET_TYPE_LONG, "showfootertext");
//            prof.ShowOverviewTab = CMS.GetWidgetConfig(profConfig, tabKey, WIDGET_TYPE_LONG, "showoverviewtab");

//            if (!string.IsNullOrEmpty(prof.ShowOverviewTab) && prof.ShowOverviewTab == "false")
//            {
//                prof.Tabs.Remove(prof.Tabs.Where(r => r.Key == prof.OverviewTabId).FirstOrDefault());
//            }

//            return prof;
//        }

//        public bool ValidateConditions(List<Condition> conditionList, List<Question1> mappedProf,
//            bool fromLayout = false)
//        {
//            if (conditionList.Count > 0)
//            {
//                foreach (var c in conditionList)
//                {
//                    var quest = mappedProf.Where(x => x.Key == c.profileattributekey).FirstOrDefault();
//                    if (quest != null && quest.Answer.Length > 0)
//                    {
//                        if (c.value.Length == 0)
//                        {
//                            if (c.@operator == "=")
//                            {
//                                if (quest.Answer != c.profileoptionkey)
//                                {
//                                    return false;
//                                }
//                            }
//                            else
//                            {
//                                if (
//                                    Convert.ToBoolean(
//                                        new NCalc.Expression(Convert.ToDecimal(quest.Answer) + c.@operator +
//                                                             Convert.ToDecimal(c.value)).Evaluate()) == false)
//                                {
//                                    return false;
//                                }
//                            }
//                        }
//                        else
//                        {
//                            if (c.@operator == "IN")
//                            {
//                                if (
//                                    Convert.ToBoolean(
//                                        new NCalc.Expression("in(" + quest.Answer + "," + c.value + ")").Evaluate()) ==
//                                    false)
//                                {
//                                    return false;
//                                }
//                            }
//                            else if (c.@operator == "NOTIN")
//                            {
//                                if (
//                                    Convert.ToBoolean(
//                                        new NCalc.Expression("not in(" + quest.Answer + "," + c.value + ")").Evaluate()) ==
//                                    false)
//                                {
//                                    return false;
//                                }
//                            }
//                            else
//                            {
//                                if (
//                                    Convert.ToBoolean(
//                                        new NCalc.Expression(Convert.ToDecimal(quest.Answer) + c.@operator +
//                                                             Convert.ToDecimal(c.value)).Evaluate()) == false)
//                                {
//                                    return false;
//                                }
//                            }
//                        }
//                    }
//                    else
//                    {
//                        return fromLayout;
//                    }
//                }
//            }
//            return true;
//        }



//        public ProfileRoot GetProfileAttributesFromCache(JsonParams jp)
//        {
//            var profileResponse = new ProfileRoot();
//            if (
//                HttpRuntime.Cache[
//                    "ProfileAttributes" + jp.cl + "" + jp.c + "" + jp.a + "" + jp.p + "" + jp.l + "" + jp.w] == null)
//            {
//                var rm = new APIResponseModels();
//                var requestParams = "CustomerId=" + jp.c + "&AccountId=" + jp.a + "&PremiseId=" + jp.p +
//                                    "&IncludeContent=false&IncludeMissing=false";
//                profileResponse =
//                    (ProfileRoot)
//                        JsonConvert.DeserializeObject(rm.GetProfile(requestParams, jp.l, int.Parse(jp.cl)).Content,
//                            typeof(ProfileRoot));
//                HttpRuntime.Cache.Insert(
//                    "ProfileAttributes" + jp.cl + "" + jp.c + "" + jp.a + "" + jp.p + "" + jp.l + "" + jp.w,
//                    profileResponse,
//                    null, DateTime.Now.AddMinutes(Constants.kCache_Expiry), Cache.NoSlidingExpiration,
//                    CacheItemPriority.NotRemovable, null);
//            }
//            else
//            {
//                profileResponse =
//                    (ProfileRoot)
//                        HttpRuntime.Cache[
//                            "ProfileAttributes" + jp.cl + "" + jp.c + "" + jp.a + "" + jp.p + "" + jp.l + "" + jp.w];
//            }

//            return profileResponse;
//        }

//    }

//    public class QuestionConverter1 : ITypeConverter<QuestionMerge, List<Question1>>
//    {
//        public List<Question1> Convert(ResolutionContext context)
//        {
//            var sourceCollection = (QuestionMerge)context.SourceValue;

//            Mapper.CreateMap<QuestionMerge, List<Question>>();
//            Mapper.CreateMap<ProfileAttribute, Question1>()
//                .ForMember(dest => dest.OptionValue, opt => opt.MapFrom(src => src.optionkeys))
//                .ForMember(dest => dest.Min, opt => opt.MapFrom(src => src.minvalue))
//                .ForMember(dest => dest.Max, opt => opt.MapFrom(src => src.maxvalue))
//                .ForMember(dest => dest.Key, opt => opt.MapFrom(src => src.key))
//                .ForMember(dest => dest.Level, opt => opt.MapFrom(src => src.entitylevel));

//            var ql = new List<Question1>();
//            foreach (var a in sourceCollection.Attributes)
//            {
//                var q = new Question1();
//                q = EntityMapper.Map<Question1>(a);
//                if (a.optionkeys != null)
//                {
//                    var opt = new Options();
//                    string[] options = a.optionkeys.Split(',');
//                    var optionText = string.Empty;
//                    var oVal = new List<string>();
//                    var oTxt = new List<string>();
//                    foreach (var o in options)
//                    {
//                        var option = sourceCollection.Options.Where(x => x.key == o).FirstOrDefault();
//                        if (option != null)
//                        {
//                            var text = sourceCollection.Text.Where(x => x.key == option.namekey).FirstOrDefault();
//                            optionText += (text == null) ? "" : text.shorttext + ",";
//                            oTxt.Add((text == null) ? "" : text.shorttext);
//                            oVal.Add(o);
//                        }
//                    }
//                    q.OptionText = optionText;
//                    opt.Value = oVal;
//                    opt.Text = oTxt;
//                    q.Options = opt;
//                }

//                if (a.type == "decimal" || a.type == "integer")
//                {
//                    var replaceText = new Common.TextContent();
//                    if (sourceCollection.IsLongProfile)
//                    {
//                        replaceText =
//                            sourceCollection.Text.Where(
//                                x => x.key == sourceCollection.TabKey + ".longprofile.outofrangemessage")
//                                .FirstOrDefault();
//                    }
//                    else
//                    {
//                        replaceText =
//                            sourceCollection.Text.Where(
//                                x => x.key == sourceCollection.TabKey + ".profile.outofrangemessage").FirstOrDefault();
//                    }
//                    q.RangeErrorMessage = replaceText == null
//                        ? ""
//                        : replaceText.shorttext.Replace("{%minvalue%}", a.minvalue.ToString())
//                            .Replace("{%maxvalue%}", a.maxvalue.ToString());
//                    var opt = new Options();
//                    var oVal = new List<string>();
//                    var oTxt = new List<string>();
//                    if (Int32.Parse(q.Max) > 1 && Int32.Parse(q.Max) < 51)
//                    {
//                        for (var e = Int32.Parse(q.Min); e <= Int32.Parse(q.Max); e++)
//                        {
//                            oTxt.Add(e.ToString());
//                            oVal.Add(e.ToString());
//                        }
//                    }

//                    opt.Value = oVal;
//                    opt.Text = oTxt;
//                    q.Options = opt;
//                }

//                if ((a.type == "decimal" && a.maxvalue == 1 && q.OptionText.Length == 0))
//                {
//                    var textYes = sourceCollection.Text.Where(x => x.key == "profile.option.yes.name").FirstOrDefault();
//                    q.OptionText += (textYes == null) ? "" : textYes.shorttext + ",";
//                    var textNo = sourceCollection.Text.Where(x => x.key == "profile.option.no.name").FirstOrDefault();
//                    q.OptionText += (textNo == null) ? "" : textNo.shorttext;

//                    q.OptionValue = "1,0";

//                    var opt = new Options();
//                    var oVal = new List<string>();
//                    var oTxt = new List<string>();
//                    oTxt.Add((textYes == null) ? "" : textYes.shorttext);
//                    oVal.Add("1");
//                    oTxt.Add((textNo == null) ? "" : textNo.shorttext);
//                    oVal.Add("0");
//                    opt.Value = oVal;
//                    opt.Text = oTxt;
//                    q.Options = opt;
//                }
//                if (sourceCollection.Profile.Customer != null)
//                {
//                    var answer =
//                        sourceCollection.Profile.Customer.Attributes.Where(c => c.AttributeKey == a.key)
//                            .FirstOrDefault();
//                    if (answer == null)
//                    {
//                        answer =
//                            sourceCollection.Profile.Customer.Accounts[0].Attributes.Where(c => c.AttributeKey == a.key)
//                                .FirstOrDefault();
//                        if (answer == null)
//                        {
//                            if (sourceCollection.Profile.Customer.Accounts[0].Premises.Count > 0)
//                            {
//                                answer =
//                                    sourceCollection.Profile.Customer.Accounts[0].Premises[0].Attributes.Where(
//                                        c => c.AttributeKey == a.key).FirstOrDefault();
//                            }
//                        }
//                    }

//                    if (answer == null)
//                    {
//                        q.Answer = "";
//                    }
//                    else
//                    {
//                        q.Answer = answer.AttributeValue ?? "";
//                    }
//                }
//                else
//                {
//                    q.Answer = "";
//                }
//                q.Label = sourceCollection.Text.Where(x => x.key == a.questiontextkey).FirstOrDefault().shorttext;
//                ql.Add(q);
//            }
//            return ql;
//        }
//    }

//    public class Question1
//    {
//        public string Key { get; set; }
//        public string Label { get; set; }
//        public string Tooltip { get; set; }
//        public string Type { get; set; }
//        public string OptionText { get; set; }
//        public string OptionValue { get; set; }
//        public string Min { get; set; }
//        public string Max { get; set; }
//        public string Answer { get; set; }
//        public string Level { get; set; }
//        public string RangeErrorMessage { get; set; }
//        public bool InvalidRequired { get; set; }
//        public bool InvalidRange { get; set; }
//        public Options Options { get; set; }
//    }

//    public class Section1
//    {
//        public string Key { get; set; }
//        public string SectionTitle { get; set; }
//        public string SectionSubtitle { get; set; }
//        public string SectionDescription { get; set; }
//        public string SectionMarkdown { get; set; }
//        public string ImageUrl { get; set; }
//        public string ImageTitle { get; set; }
//        public List<Question1> Questions { get; set; }
//    }

//    public class ProfileTab1
//    {
//        public string Key { get; set; }
//        public string Name { get; set; }
//        public string IconClass { get; set; }
//        public List<Section1> Sections { get; set; }
//    }

//    public class Profile1
//    {
//        public int Completeness { get; set; }
//        public string CompletenessTitle { get; set; }
//        public string CompletenessSubtitle { get; set; }
//        public string CompletenessDescription { get; set; }
//        public string CompletenessLabel { get; set; }
//        public string NextButtonLabel { get; set; }
//        public string NotCompleteMessage { get; set; }
//        public string BackButtonLabel { get; set; }
//        public string Title { get; set; }
//        public string IntroText { get; set; }
//        public string DropDownSelectText { get; set; }
//        public string ShowTitle { get; set; }
//        public string ShowIntro { get; set; }
//        public string ShowSectionIntro { get; set; }
//        public string LoadingMessage { get; set; }
//        public string OverviewTabId { get; set; }
//        public string Footer { get; set; }
//        public string FooterLinkText { get; set; }
//        public string FooterLink { get; set; }
//        public string ShowFooterLink { get; set; }
//        public string ShowFooterText { get; set; }
//        public string RequiredKey { get; set; }
//        public string RequiredSymbol { get; set; }
//        public string LongProfileLinkText { get; set; }
//        public string ShowRequiredKey { get; set; }
//        public string ShowLongProfileLink { get; set; }
//        public string LongProfileLink { get; set; }
//        public string ShowOverviewTab { get; set; }
//        public string OverviewText { get; set; }
//        public string SaveButtonLabel { get; set; }
//        public string PageOf { get; set; }

//        public string SavedMessage { get; set; }
//        public List<ProfileTab1> Tabs { get; set; }
//    }

//    public class Options
//    {
//        public List<string> Value { get; set; }
//        public List<string> Text { get; set; }
//    }
//}
