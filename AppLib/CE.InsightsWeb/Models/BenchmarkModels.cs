﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using AutoMapper;
using CE.InsightsWeb.Common;
using CE.InsightsWeb.Common.AclaraJson;
using CE.InsightsWeb.Common.QueryStringEncryptor;
using Newtonsoft.Json;

namespace CE.InsightsWeb.Models
{
    public class BenchmarkModels
    {
        private const string BENCHMARK_CATEGORY = "benchmark";
        private const string COMMON_CATEGORY = "common";
        private const string ELECTRIC_COMMODITY_KEY = "electric";
        private const string GAS_COMMODITY_KEY = "gas";
        private const string WATER_COMMODITY_KEY = "water";
        private const string WIDGET_TYPE = "peercomparison";

        public Benchmark GetBenchmark(string tabKey, string parameters)
        {
            var jp = (JsonParams)JsonConvert.DeserializeObject(QueryStringModule.Decrypt(parameters), typeof(JsonParams));
            //Content
            var content = CMS.GetContent(int.Parse(jp.cl), jp.l).Content;

            var benchmarkTextContent = content.TextContent.Where(x => x.category == BENCHMARK_CATEGORY || x.category == COMMON_CATEGORY).ToList();
            var benchmarkConfig = content.Configuration.Where(x => x.category == BENCHMARK_CATEGORY).ToList();
            var benchmarkUOMContent = content.UOM.ToList();
            var benchmarkCurrencyContent = content.Currency.ToList();
            var bm = new Benchmark(benchmarkConfig, benchmarkTextContent, tabKey, WIDGET_TYPE, jp)
            {
                Commodities = CMS.GetWidgetConfig(benchmarkConfig, tabKey, WIDGET_TYPE, "commodities")
            };

            if (jp.pt == Constants.kPremiseTypeResidential)
            {
                //Get Benchmarks
                var gKey = CMS.GetWidgetConfig(benchmarkConfig, tabKey, WIDGET_TYPE, "groupkey");
                var mKey = CMS.GetWidgetConfig(benchmarkConfig, tabKey, WIDGET_TYPE, "measurementkey");
                var benchmarkResponse = GetBenchmark(jp, gKey, mKey);

                Mapper.CreateMap<List<Common.AclaraJson.Benchmark>, Benchmark>()
                    .ForMember(dest => dest.Benchmarks,
                        opt => opt.MapFrom(
                            src => Mapper.Map<List<Common.AclaraJson.Benchmark>, List<BenchmarkList>>(src)));
                Mapper.CreateMap<Common.AclaraJson.Benchmark, BenchmarkList>();
                Mapper.CreateMap<Common.AclaraJson.BenchmarkMeasurement, BenchmarkMeasurement>();

                if (benchmarkResponse.Customer == null)
                {
                    bm.NoBenchmarks = true;
                    bm.NoBenchmarksText = CMS.GetWidgetContent(benchmarkTextContent, tabKey, WIDGET_TYPE, "nobenchmark");
                    return bm;
                }

                Mapper.Map(benchmarkResponse.Customer.Accounts[0].Premises[0].Benchmarks, bm);

                bm.ElectricServiceCount =
                    benchmarkResponse.Customer.Accounts.SelectMany(x => x.Premises).SelectMany(x => x.Benchmarks)
                        .Where(t => t.CommodityKey == ELECTRIC_COMMODITY_KEY)
                        .Count();
                bm.GasServiceCount =
                    benchmarkResponse.Customer.Accounts.SelectMany(x => x.Premises)
                        .SelectMany(x => x.Benchmarks)
                        .Where(t => t.CommodityKey == GAS_COMMODITY_KEY)
                        .Count();
                bm.WaterServiceCount =
                    benchmarkResponse.Customer.Accounts.SelectMany(x => x.Premises)
                        .SelectMany(x => x.Benchmarks)
                        .Where(t => t.CommodityKey == WATER_COMMODITY_KEY)
                        .Count();
            }
            else
            {
                //Billdisagg
                var disaggResponse = BillDisaggModel.GetBillDisagg(jp);
                //Model
                var modelResponse = GetModelData(jp);

                var sqSplit = new List<string>(CMS.GetWidgetConfig(benchmarkConfig, tabKey, WIDGET_TYPE, "commodities").Replace(" ", "").Split(','));

                if (disaggResponse.Customer == null || modelResponse.Data == null)
                {
                    bm.NoBenchmarks = true;
                    bm.NoBenchmarksText = CMS.GetWidgetContent(benchmarkTextContent, tabKey, WIDGET_TYPE, "businessnobenchmark");
                    return bm;
                }

                var electricStatus = Enums.DisaggStatusType.Unspecified;
                var gasStatus = Enums.DisaggStatusType.Unspecified;
                var waterStatus = Enums.DisaggStatusType.Unspecified;
                var serviceCount = 0;
                var bmlCol = new List<BenchmarkList>();
                if (disaggResponse.Customer != null)
                {
                    var endUses = disaggResponse.Customer.Accounts[0].Premises[0].EndUses.ToList();
                    var totalEnergyUses = modelResponse.Data.TotalEnergyUses.ToList();
                    //end use logic
                    foreach (var com in bm.Commodities.Replace(" ", "").Split(','))
                    {
                        //Check for disagg status
                        foreach (var ds in disaggResponse.Customer.Accounts[0].Premises[0].DisaggStatuses)
                        {
                            if (!sqSplit.Exists(x => x.Equals(ds.CommodityKey.ToString().ToLower()))) continue;
                            if (ds.Status != Enums.DisaggStatusType.Failed && ds.Status != Enums.DisaggStatusType.ModelOnly) continue;
                            switch (ds.CommodityKey.ToString().ToLower())
                            {
                                case ELECTRIC_COMMODITY_KEY:
                                    electricStatus = Enums.DisaggStatusType.Failed;
                                    break;
                                case GAS_COMMODITY_KEY:
                                    gasStatus = Enums.DisaggStatusType.Failed;
                                    break;
                                case WATER_COMMODITY_KEY:
                                    waterStatus = Enums.DisaggStatusType.Failed;
                                    break;
                            }
                        }

                        var bml = new BenchmarkList { CommodityKey = com };
                        var endUsesWithCom = endUses.Where(c => c.DisaggDetails.Any(x => x.CommodityKey == com)).ToList();
                        if (endUsesWithCom.Count > 0)
                        {
                            var amt = (from d in endUsesWithCom.Select(x => x.DisaggDetails)
                                       select d.Where(y => y.CommodityKey == com).Sum(z => z.CostAmount)).ToList().Sum();
                            bml.MyCost = amt;

                            var usage = (from d in endUsesWithCom.Select(x => x.DisaggDetails)
                                         select d.Where(y => y.CommodityKey == com).Sum(z => z.UsageQuantity)).ToList().Sum();
                            bml.MyUsage = usage;

                            bml.UOMKey = endUsesWithCom.FirstOrDefault().DisaggDetails[0].UsageUOMKey;
                            bml.CostCurrencyKey = endUsesWithCom.FirstOrDefault().DisaggDetails[0].CostCurrencyKey;
                            switch (com)
                            {
                                case ELECTRIC_COMMODITY_KEY:
                                    if (electricStatus != Enums.DisaggStatusType.Failed)
                                    {
                                        bm.ElectricServiceCount = (from d in endUsesWithCom.Select(x => x.DisaggDetails)
                                                                   select d.Where(y => y.CommodityKey == com)).ToList().Count;
                                        serviceCount += 1;
                                    }
                                    break;
                                case GAS_COMMODITY_KEY:
                                    if (gasStatus != Enums.DisaggStatusType.Failed)
                                    {
                                        bm.GasServiceCount = (from d in endUsesWithCom.Select(x => x.DisaggDetails)
                                                              select d.Where(y => y.CommodityKey == com)).ToList().Count;
                                        serviceCount += 1;
                                    }
                                    break;
                                case WATER_COMMODITY_KEY:
                                    if (waterStatus != Enums.DisaggStatusType.Failed)
                                    {
                                        bm.WaterServiceCount = (from d in endUsesWithCom.Select(x => x.DisaggDetails)
                                                                select d.Where(y => y.CommodityKey == com)).ToList().Count;
                                        serviceCount += 1;
                                    }
                                    break;
                            }
                        }

                        var energyUsesWithCom = totalEnergyUses.Where(c => c.CommodityName.ToLower() == com).FirstOrDefault();
                        if (energyUsesWithCom != null)
                        {
                            bml.AverageCost = energyUsesWithCom.Cost;
                            bml.AverageUsage = energyUsesWithCom.Quantity;

                            var percent = Convert.ToDouble(CMS.GetWidgetConfig(benchmarkConfig, tabKey, WIDGET_TYPE, "efficientpct"));
                            bml.EfficientCost = energyUsesWithCom.Cost * percent;
                            bml.EfficientUsage = energyUsesWithCom.Quantity * percent;
                        }
                        bmlCol.Add(bml);
                    }
                }
                bm.Benchmarks = bmlCol;
                if (serviceCount == 0)
                {
                    bm.NoBenchmarks = true;
                    bm.NoBenchmarksText = CMS.GetWidgetContent(benchmarkTextContent, tabKey, WIDGET_TYPE, "businessnobenchmark");
                    return bm;
                }
            }

            var cFormat = CMS.GetWidgetConfig(benchmarkConfig, tabKey, WIDGET_TYPE, "currencyformat");
            var ci = new CultureInfo(jp.l);

            var uFormat = CMS.GetWidgetConfig(benchmarkConfig, tabKey, WIDGET_TYPE, "usageformat");

            foreach (var c in bm.Commodities.Replace(" ", "").Split(','))
            {
                var comNameKey = "commodity." + c + ".name";
                if (bm.CommoditiesText == null)
                {
                    bm.CommoditiesText = benchmarkTextContent.Where(e => e.key == comNameKey).Select(s => s.shorttext).FirstOrDefault();
                }
                else
                {
                    bm.CommoditiesText += "," + benchmarkTextContent.Where(e => e.key == comNameKey).Select(s => s.shorttext).FirstOrDefault();
                }

                var comIcondFontKey = "commodity." + c + ".iconfont";
                if (bm.CommoditiesIconFonts == null)
                {
                    bm.CommoditiesIconFonts = benchmarkTextContent.Where(e => e.key == comIcondFontKey).Select(s => s.shorttext).FirstOrDefault();
                }
                else
                {
                    bm.CommoditiesIconFonts += "," + benchmarkTextContent.Where(e => e.key == comIcondFontKey).Select(s => s.shorttext).FirstOrDefault();
                }

                var benchMark = bm.Benchmarks.Where(t => t.CommodityKey == c).FirstOrDefault();

                if (benchMark != null)
                {
                    var unit = benchMark.UOMKey;
                    var ckey = benchMark.CostCurrencyKey;

                    benchMark.UOMText = benchmarkTextContent.Where(z => z.key == benchmarkUOMContent.Where(x => x.key == unit).FirstOrDefault()?.namekey).FirstOrDefault()?.shorttext;
                    benchMark.CostCurrencySymbol = benchmarkTextContent.Where(z => z.key == benchmarkCurrencyContent.Where(x => x.key == ckey).FirstOrDefault()?.symbolkey).FirstOrDefault()?.shorttext;

                    benchMark.MyUsageFormatted = benchMark.MyUsage.ToString(uFormat, ci);
                    benchMark.AverageUsageFormatted = benchMark.AverageUsage.ToString(uFormat, ci);
                    benchMark.EfficientUsageFormatted = benchMark.EfficientUsage.ToString(uFormat, ci);

                    benchMark.MyCostFormatted = benchMark.MyCost.ToString(cFormat, ci);
                    benchMark.AverageCostFormatted = benchMark.AverageCost.ToString(cFormat, ci);
                    benchMark.EfficientCostFormatted = benchMark.EfficientCost.ToString(cFormat, ci);

                    if (benchMark.Measurements != null && benchMark.Measurements.Count > 0)
                    {
                        benchMark.Measurements.FirstOrDefault().MyQuantityFormatted =
                            benchMark.Measurements.FirstOrDefault().MyQuantity.ToString(uFormat, ci);
                        benchMark.Measurements.FirstOrDefault().AverageQuantityFormatted =
                            benchMark.Measurements.FirstOrDefault().AverageQuantity.ToString(uFormat, ci);
                        benchMark.Measurements.FirstOrDefault().EfficientQuantityFormatted =
                            benchMark.Measurements.FirstOrDefault().EfficientQuantity.ToString(uFormat, ci);
                        benchMark.Measurements.FirstOrDefault().UOMText = benchmarkTextContent.Where(
                            z =>
                                z.key ==
                                benchmarkUOMContent.Where(
                                    x => x.key == benchMark.Measurements.FirstOrDefault().UOMKey)
                                    .FirstOrDefault()
                                    .namekey)
                            .FirstOrDefault()
                            .shorttext;
                    }
                }
            }

            //Content
            if (jp.pt == Constants.kPremiseTypeResidential)
            {
                bm.YourQty = CMS.GetWidgetContent(benchmarkTextContent, tabKey, WIDGET_TYPE, "yourqty");
                bm.AverageQty = CMS.GetWidgetContent(benchmarkTextContent, tabKey, WIDGET_TYPE, "averageqty");
                bm.EfficientQty = CMS.GetWidgetContent(benchmarkTextContent, tabKey, WIDGET_TYPE, "efficientqty");
                bm.Footer = CMS.GetWidgetContent(benchmarkTextContent, tabKey, WIDGET_TYPE, "footer");
            }
            else
            {
                bm.YourQty = CMS.GetWidgetContent(benchmarkTextContent, tabKey, WIDGET_TYPE, "businessyourqty");
                bm.AverageQty = CMS.GetWidgetContent(benchmarkTextContent, tabKey, WIDGET_TYPE, "businessaverageqty");
                bm.EfficientQty = CMS.GetWidgetContent(benchmarkTextContent, tabKey, WIDGET_TYPE, "businessefficientqty");
                bm.Footer = CMS.GetWidgetContent(benchmarkTextContent, tabKey, WIDGET_TYPE, "businessfooter");
            }
            //Configuration  
            bm.ShowCommodityLabels = CMS.GetWidgetConfig(benchmarkConfig, tabKey, WIDGET_TYPE, "showcommoditylabels");
            bm.CostOrUsage = jp.pt == Constants.kPremiseTypeResidential ? CMS.GetWidgetConfig(benchmarkConfig, tabKey, WIDGET_TYPE, "costorusage") : "usage";
            bm.YourQtyColor = CMS.GetWidgetConfig(benchmarkConfig, tabKey, WIDGET_TYPE, "yourqtycolor");
            bm.AverageQtyColor = CMS.GetWidgetConfig(benchmarkConfig, tabKey, WIDGET_TYPE, "averageqtycolor");
            bm.EfficientQtyColor = CMS.GetWidgetConfig(benchmarkConfig, tabKey, WIDGET_TYPE, "efficientqtycolor");

            return bm;
        }

        private static BenchmarkRoot GetBenchmark(JsonParams jp, string groupKeys, string measurementKeys)
        {
            var rm = new APIResponseModels();
            var sdate = DateTime.UtcNow.AddYears(-1).ToString("yyyy-MM-dd");
            var edate = DateTime.UtcNow.ToString("yyyy-MM-dd");
            var requestParams = "CustomerId=" + jp.c + "&AccountId=" + jp.a + "&PremiseId=" + jp.p + "&StartDate=" + sdate + "&EndDate=" +
                                edate + "&Count=1&GroupKeys=" + groupKeys + "&MeasurementKeys=" + measurementKeys + "&IncludeContent=false";
            return (BenchmarkRoot)JsonConvert.DeserializeObject(rm.GetBenchmark(requestParams, jp.l, int.Parse(jp.cl)).Content, typeof(BenchmarkRoot));
        }

        private static ModelRoot GetModelData(JsonParams jp)
        {
            var rm = new APIResponseModels();
            var cp = new CustomerParameters
            {
                AccountId = jp.a,
                CustomerId = jp.c,
                PremiseId = jp.p
            };
            var postStr1 = @"{ " + "\"Data\": { CustomerParameters: " + JsonConvert.SerializeObject(cp) + "}}";
            return (ModelRoot)JsonConvert.DeserializeObject(rm.PostModel(postStr1, "en-US", Convert.ToInt32(jp.cl)).Content, typeof(ModelRoot));
        }
    }

    public class Benchmark : ContentBase
    {
        public string YourQty { get; set; }
        public string AverageQty { get; set; }
        public string EfficientQty { get; set; }
        public string NoBenchmarksText { get; set; }
        public string Commodities { get; set; }
        public string ShowCommodityLabels { get; set; }
        public bool NoBenchmarks { get; set; }
        public int ElectricServiceCount { get; set; }
        public int WaterServiceCount { get; set; }
        public int GasServiceCount { get; set; }
        public string CommoditiesText { get; set; }
        public string CommoditiesIconFonts { get; set; }
        public string CostOrUsage { get; set; }
        public string YourQtyColor { get; set; }
        public string AverageQtyColor { get; set; }
        public string EfficientQtyColor { get; set; }
        public List<BenchmarkList> Benchmarks { get; set; }
        public Benchmark(List<Common.Configuration> config, List<Common.TextContent> content, string tabKey, string widetType, JsonParams jp) : base(config, content, tabKey, widetType, jp)
        { }
    }

    public class BenchmarkMeasurement
    {
        public string Key { get; set; }
        public double MyQuantity { get; set; }
        public double AverageQuantity { get; set; }
        public double EfficientQuantity { get; set; }
        public string MyQuantityFormatted { get; set; }
        public string AverageQuantityFormatted { get; set; }
        public string EfficientQuantityFormatted { get; set; }
        public string UOMKey { get; set; }
        public string UOMText { get; set; }
    }
    public class BenchmarkList
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string CommodityKey { get; set; }
        public string GroupKey { get; set; }
        public int GroupCount { get; set; }
        public double MyUsage { get; set; }
        public double AverageUsage { get; set; }
        public double EfficientUsage { get; set; }
        public string MyUsageFormatted { get; set; }
        public string AverageUsageFormatted { get; set; }
        public string EfficientUsageFormatted { get; set; }
        public string UOMKey { get; set; }
        public double MyCost { get; set; }
        public double AverageCost { get; set; }
        public double EfficientCost { get; set; }
        public string MyCostFormatted { get; set; }
        public string AverageCostFormatted { get; set; }
        public string EfficientCostFormatted { get; set; }
        public string CostCurrencyKey { get; set; }
        public string UOMText { get; set; }
        public string CostCurrencySymbol { get; set; }
        public List<BenchmarkMeasurement> Measurements { get; set; }
    }



   

}

