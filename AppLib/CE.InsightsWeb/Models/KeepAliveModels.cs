﻿using System.Diagnostics;
using System.Linq;

namespace CE.InsightsWeb.Models
{
    public class KeepAliveModels
    {
        public int StatusCode { get; set; }
        public string StatusDesc { get; set; }
        public string ElapsedTime { get; set; }
        public string ElapsedTime2 { get; set; }

        /// <summary>
        /// Simple Model that calls the API Keep Alive
        /// </summary>
        public void KeepAlive()
        {
            StatusCode = 0;
            StatusDesc = string.Empty;

            // DIRECT AZURE SQL EF CALL (remove this in future)
            var stopWatch = new Stopwatch();
            stopWatch.Start();

            //declare the transaction options
            var transactionOptions = new System.Transactions.TransactionOptions
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            };
            //set it to read uncommited

            using (var transScope = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required, transactionOptions))
            {
                using (var ent = new InsightsEntities())
                {
                    ent.Database.Connection.Open();
                    var envList = (from e in ent.UserEnvironments
                                   join ue in ent.Users on e.UserID equals ue.UserID
                                   select new { ue.CEAccessKeyID, e.BasicKey }).Take(10).ToList();

                }
                transScope.Complete();
            }

            stopWatch.Stop();
            var ts = stopWatch.Elapsed;
            ElapsedTime = $"{ts.Hours:00}:{ts.Minutes:00}:{ts.Seconds:00}.{ts.Milliseconds/10:00}";

            // API CALL TO ECHO FOR KEEPALIVE
            var stopWatch2 = new Stopwatch();
            stopWatch2.Start();

            var rm = new APIResponseModels();
            var echo = rm.GetEcho("Say=HelloFromWebsite256&Name=123&Number=1&KeepAlive=true", "en-US", 256);
            StatusCode = echo.StatusCode;
            StatusDesc = echo.StatusDescription;

            stopWatch2.Stop();
            var ts2 = stopWatch2.Elapsed;
            ElapsedTime2 = $"{ts2.Hours:00}:{ts2.Minutes:00}:{ts2.Seconds:00}.{ts2.Milliseconds/10:00}";
        }
    }
}