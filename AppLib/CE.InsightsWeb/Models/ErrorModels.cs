﻿using System;
using System.Web;
using CE.InsightsWeb.Common;

namespace CE.InsightsWeb.Models
{
    [Serializable()]
    public class ErrorModel
    {
        public string MessageDetails;
        public string Title;
        public string Message;

        public ErrorModel()
        {
            //Default Global Error Message
            Message = "Error";
            Title = "Error";
        }

        public string GetErrorContent(string errorMessage)
        {
            int clientId = Convert.ToInt32(HttpContext.Current.Request.QueryString[Constants.kClientId]);
            var locale = Convert.ToString(HttpContext.Current.Request.QueryString[Constants.kLocale]);
            var rc = CMS.GetContent(clientId, locale);
            if (errorMessage == "Un-Supported Browser")
            {
                return CMS.GetCommonContent(rc, "common.unsupportedbrowsermessage");
            }
            return CMS.GetCommonContent(rc, "common.fatalerrormessage");
        }
    }
}
