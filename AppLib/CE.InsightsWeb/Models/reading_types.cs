//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CE.InsightsWeb.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class reading_types
    {
        public long id { get; set; }
        public string uuid { get; set; }
        public System.DateTime published { get; set; }
        public System.DateTime updated { get; set; }
        public string self_link_href { get; set; }
        public string self_link_rel { get; set; }
        public string up_link_href { get; set; }
        public string up_link_ref { get; set; }
        public string accumulationBehaviour { get; set; }
        public string aggregate { get; set; }
        public Nullable<decimal> rational_denomiator { get; set; }
        public Nullable<decimal> rational_numerator { get; set; }
        public string commodity { get; set; }
        public string consumptionsTier { get; set; }
        public string cpp { get; set; }
        public string currency { get; set; }
        public string dateQualifier { get; set; }
        public string flowDirection { get; set; }
        public Nullable<decimal> interharmonic_denominator { get; set; }
        public Nullable<decimal> interharmonic_numerator { get; set; }
        public long intervalLength { get; set; }
        public string kind { get; set; }
        public string measuringPeriod { get; set; }
        public string phase { get; set; }
        public string powerOfTenMultiplier { get; set; }
        public string timeAttribute { get; set; }
        public string tou { get; set; }
        public string uom { get; set; }
        public string meterid { get; set; }
    }
}
