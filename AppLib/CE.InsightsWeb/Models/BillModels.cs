﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Caching;
using AutoMapper;
using CE.InsightsWeb.Common;
using CE.InsightsWeb.Common.AclaraJson;
using CE.InsightsWeb.Common.QueryStringEncryptor;
using CE.InsightsWeb.Helpers;
using Newtonsoft.Json;

namespace CE.InsightsWeb.Models
{
    public class BillModels
    {
        private const string BILL_CATEGORY = "bill";
        private const string COMMON_CATEGORY = "common";
        private const string ELECTRIC_COMMODITY_KEY = "electric";
        private const string GAS_COMMODITY_KEY = "gas";
        private const string WATER_COMMODITY_KEY = "water";
        private const string SEWER_COMMODITY_KEY = "sewer";
        private const string BILLTODATE_CATEGORY = "billtodate";
        private const string WIDGET_TYPE_BILL_SUMMARY = "billsummary";
        private const string WIDGET_TYPE_BILL_HISTORY = "billhistory";
        private const string WIDGET_TYPE_BILLED_USAGE_CHART = "billedusagechart";
        private const string CharTypeColumn = "column";
        private const string WIDGET_TYPE_BILL_COMPARISON = "billcomparison";
        private const string WIDGET_TYPE_BILL_TO_DATE = "billtodate";

        public BillSummary GetBillSummary(string tabKey, string parameters, bool billToDate = false, string version = "v1")
        {
            var jp = (JsonParams)JsonConvert.DeserializeObject(QueryStringModule.Decrypt(parameters), typeof(JsonParams));
            //Content
            var content = CMS.GetContent(int.Parse(jp.cl), jp.l).Content;
            var billTextContent = content.TextContent.Where(x => x.category == BILL_CATEGORY || x.category == COMMON_CATEGORY).ToList();
            var billUomContent = content.UOM.ToList();
            var billConfig = content.Configuration.Where(x => x.category == BILL_CATEGORY || x.category == COMMON_CATEGORY).ToList();

            var billResponse = GetBillsFromCache(jp, version);
            var bs = new BillSummary(billConfig, billTextContent, tabKey, WIDGET_TYPE_BILL_SUMMARY, jp);
            if (billResponse.Customer == null || billResponse.Customer.Accounts.Count == 0 || billResponse.Customer.Accounts[0].Bills.Count == 0)
            {
                bs.NoBills = true;
                bs.NoBillsText = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_SUMMARY, "nobills");
                return bs;
            }
            if ((from b in billResponse.Customer.Accounts[0].Bills from premise in b.Premises where premise.Id == jp.p select b).ToList().Count == 0)
            {
                bs.NoBills = true;
                bs.NoBillsText = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_SUMMARY, "nobills");
                return bs;
            }
            var ci = new CultureInfo(jp.l);
            var bill = GetBillsFromCache(jp, version).Customer.Accounts[0].Bills;
            bill = (from b in bill from premise in b.Premises where premise.Id == jp.p select b).Take(1).ToList();
            var dFormat = CMS.GetWidgetConfig(billConfig, tabKey, WIDGET_TYPE_BILL_SUMMARY, "dateformat");
            var gasBills =
                bill.SelectMany(c => c.Premises)
                    .SelectMany(x => x.Service)
                    .Where(t => t.CommodityKey == GAS_COMMODITY_KEY).ToList();
            var totalGasUsed = gasBills.Sum(r => r.TotalServiceUse);
            var gUom = string.Empty;
            var gBill = gasBills.FirstOrDefault();
            if (gBill != null)
            {
                var gUnit = gBill.UOMKey;
                var uomBillTextContent = billTextContent
                    .Where(z =>
                    {
                        var uomFirstOrDefault = billUomContent.FirstOrDefault(x => x.key == gUnit);
                        return uomFirstOrDefault != null && z.key == uomFirstOrDefault.namekey;
                    })
                    .FirstOrDefault();
                if (uomBillTextContent != null)
                    gUom = uomBillTextContent.shorttext;
                bs.BillStartDateGas = Convert.ToDateTime(gBill.BillStartDate).ToString(dFormat, ci);
                bs.BillEndDateGas = Convert.ToDateTime(gBill.BillEndDate).ToString(dFormat, ci);
            }
            bs.TotalGasUsed = totalGasUsed;
            bs.GasUOM = gUom;
            if (gasBills.GroupBy(x => x.RateClass).Count() > 1)
            {
                bs.RateMismatchGas = true;
            }
            else
            {
                if (gasBills.FirstOrDefault() != null)
                {
                    var firstOrDefault = gasBills.FirstOrDefault();
                    if (firstOrDefault != null) bs.RateClassGas = firstOrDefault.RateClass;
                }
            }

            var electricBills =
                bill.SelectMany(c => c.Premises)
                    .SelectMany(x => x.Service)
                    .Where(t => t.CommodityKey == ELECTRIC_COMMODITY_KEY).ToList();
            var totalElectricityUsed = electricBills.Sum(r => r.TotalServiceUse);
            var eUom = string.Empty;
            var eBill = electricBills.FirstOrDefault();
            if (eBill != null)
            {
                var eUnit = eBill.UOMKey;
                var uomBillTextContent = billTextContent
                    .Where(z =>
                    {
                        var uomFirstOrDefault = billUomContent.FirstOrDefault(x => x.key == eUnit);
                        return uomFirstOrDefault != null && z.key == uomFirstOrDefault.namekey;
                    })
                    .FirstOrDefault();
                if (uomBillTextContent != null)
                    eUom = uomBillTextContent.shorttext;
                bs.BillStartDateElectric = Convert.ToDateTime(eBill.BillStartDate).ToString(dFormat, ci);
                bs.BillEndDateElectric = Convert.ToDateTime(eBill.BillEndDate).ToString(dFormat, ci);
            }
            bs.TotalElectricityUsed = totalElectricityUsed;
            bs.ElectricUOM = eUom;
            if (electricBills.GroupBy(x => x.RateClass).Count() > 1)
            {
                bs.RateMismatchElectric = true;
            }
            else
            {
                if (electricBills.FirstOrDefault() != null)
                {
                    var electricBill = electricBills.FirstOrDefault();
                    if (electricBill != null)
                        bs.RateClassElectric = electricBill.RateClass;
                }
            }

            var waterBills =
                bill.SelectMany(c => c.Premises)
                    .SelectMany(x => x.Service)
                    .Where(t => t.CommodityKey == WATER_COMMODITY_KEY).ToList();
            var totalWaterUsed = waterBills.Sum(r => r.TotalServiceUse);
            var wUom = string.Empty;
            var wBill = waterBills.FirstOrDefault();
            if (wBill != null)
            {
                var wUnit = wBill.UOMKey;
                var uomBillTextContent = billTextContent
                    .Where(z =>
                    {
                        var uomFirstOrDefault = billUomContent.FirstOrDefault(x => x.key == wUnit);
                        return uomFirstOrDefault != null && z.key == uomFirstOrDefault.namekey;
                    })
                    .FirstOrDefault();
                if (uomBillTextContent != null)
                    wUom = uomBillTextContent.shorttext;
                bs.BillStartDateWater = Convert.ToDateTime(wBill.BillStartDate).ToString(dFormat, ci);
                bs.BillEndDateWater = Convert.ToDateTime(wBill.BillEndDate).ToString(dFormat, ci);
            }
            bs.TotalWaterUsed = totalWaterUsed;
            bs.WaterUOM = wUom;
            if (waterBills.GroupBy(x => x.RateClass).Count() > 1)
            {
                bs.RateMismatchWater = true;
            }
            else
            {
                if (waterBills.FirstOrDefault() != null)
                {
                    var waterBill = waterBills.FirstOrDefault();
                    if (waterBill != null) bs.RateClassWater = waterBill.RateClass;
                }
            }

            var maxBillDays =
                bill.SelectMany(c => c.Premises)
                    .SelectMany(x => x.Service)
                    .OrderByDescending(o => o.BillDays)
                    .FirstOrDefault();
            if (maxBillDays != null)
            {
                bs.NumberOfDays = maxBillDays.BillDays;
                var amountDue = bill.Sum(c => c.TotalAmount);
                var avgDailyCost = Math.Round(amountDue / bs.NumberOfDays, 2);
                bs.AvgTemp = maxBillDays.AverageTemperature;

                var cFormat = CMS.GetWidgetConfig(billConfig, tabKey, WIDGET_TYPE_BILL_SUMMARY, "currencyformat");

                bs.AvgDailyCost = avgDailyCost.ToString(cFormat, ci);
                bs.AmountDue = amountDue.ToString(cFormat, ci);
            }
            bs.DueDate = bill.First().BillDueDate;

            if (billToDate) return bs;

            //Content
            bs.TotalGasUse = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_SUMMARY, "totalgasuse");
            bs.TotalElectricUse = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_SUMMARY, "totalelectricuse");
            bs.TotalWaterUse = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_SUMMARY, "totalwateruse");
            bs.AvgDailyCostText = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_SUMMARY, "avgdailycost");
            bs.NumberOfDaysText = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_SUMMARY, "numberofdays");
            bs.AvgTempText = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_SUMMARY, "avgtemp");
            bs.AmountDueText = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_SUMMARY, "amountdue");
            bs.ViewBillHistory = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_SUMMARY, "viewbillhistory");
            //Configuration
            var tempFormat = billConfig.Where(x => x.category == COMMON_CATEGORY && x.key == "format.temperature").FirstOrDefault();

            bs.BillHistoryLink = GeneralHelper.CreateLink("internal", jp, CMS.GetWidgetConfig(billConfig, tabKey, WIDGET_TYPE_BILL_SUMMARY, "billhistorylink"));
            bs.Commodities = CMS.GetWidgetConfig(billConfig, tabKey, WIDGET_TYPE_BILL_SUMMARY, "commodities");
            bs.ShowBillHistoryLink = CMS.GetWidgetConfig(billConfig, tabKey, WIDGET_TYPE_BILL_SUMMARY, "showbillhistorylink");

            bs.TempFormat = tempFormat == null ? "" : tempFormat.value;

            return bs;
        }

        public BillHistory GetBillHistory(string tabKey, string parameters, string version = "v1")
        {
            var jp = (JsonParams)JsonConvert.DeserializeObject(QueryStringModule.Decrypt(parameters), typeof(JsonParams));
            //Content
            var content = CMS.GetContent(int.Parse(jp.cl), jp.l).Content;
            var billTextContent =
                content.TextContent.Where(x => x.category == BILL_CATEGORY || x.category == COMMON_CATEGORY).ToList();
            var billConfig = content.Configuration.Where(x => x.category == BILL_CATEGORY).ToList();

            //Get Bills 
            var billResponse = GetBillsFromCache(jp, version);
            var bh = new BillHistory(billConfig, billTextContent, tabKey, WIDGET_TYPE_BILL_HISTORY, jp);

            if (billResponse.Customer == null || billResponse.Customer.Accounts.Count == 0 || billResponse.Customer.Accounts[0].Bills.Count == 0)
            {
                bh.NoBills = true;
                bh.NoBillsText = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_HISTORY, "nobills");
                return bh;
            }
            if ((from b in billResponse.Customer.Accounts[0].Bills from premise in b.Premises where premise.Id == jp.p select b).ToList().Count == 0)
            {
                bh.NoBills = true;
                bh.NoBillsText = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_HISTORY, "nobills");
                return bh;
            }

            var cFormat = CMS.GetWidgetConfig(billConfig, tabKey, WIDGET_TYPE_BILL_HISTORY, "currencyformat");
            var ci = new CultureInfo(jp.l);

            var dFormat = CMS.GetWidgetConfig(billConfig, tabKey, WIDGET_TYPE_BILL_HISTORY, "dateformat");

            var service = billResponse.Customer.Accounts[0].Bills.SelectMany(x => x.Premises).SelectMany(x => x.Service).ToList();
            bh.ElectricServiceCount = service.Count(t => t.CommodityKey == ELECTRIC_COMMODITY_KEY);
            bh.GasServiceCount = service.Count(t => t.CommodityKey == GAS_COMMODITY_KEY);
            bh.WaterServiceCount = service.Count(t => t.CommodityKey == WATER_COMMODITY_KEY);

            var bills = new List<BillHistoryList>();
            foreach (var b in billResponse.Customer.Accounts[0].Bills)
            {
                var bhl = new BillHistoryList
                {
                    BillDate = Convert.ToDateTime(b.BillDate).ToString(dFormat, ci),
                    ElectricCost = b.Premises.SelectMany(x => x.Service)
                        .Where(t => t.CommodityKey == ELECTRIC_COMMODITY_KEY)
                        .Sum(x => x.CostofUsage)
                        .ToString(cFormat, ci),
                    WaterCost = b.Premises.SelectMany(x => x.Service)
                        .Where(t => t.CommodityKey == WATER_COMMODITY_KEY)
                        .Sum(x => x.CostofUsage)
                        .ToString(cFormat, ci),
                    GasCost = b.Premises.SelectMany(x => x.Service)
                        .Where(t => t.CommodityKey == GAS_COMMODITY_KEY)
                        .Sum(x => x.CostofUsage)
                        .ToString(cFormat, ci),
                    TotalCost = b.TotalAmount.ToString(cFormat, ci)
                };
                var additionalParam = new[,] { { "compareto", b.BillDate } };
                bhl.CompareLink = GeneralHelper.CreateLink("internal", jp, CMS.GetWidgetConfig(billConfig, tabKey, WIDGET_TYPE_BILL_HISTORY, "billcomparelink"), additionalParam);
                bills.Add(bhl);
            }
            bh.Bills = bills;

            //Content
            bh.BillDate = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_HISTORY, "billdate");
            bh.ElectricAmountDue = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_HISTORY, "electricamountdue");
            bh.WaterAmountDue = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_HISTORY, "wateramountdue");
            bh.GasAmountDue = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_HISTORY, "gasamountdue");
            bh.TotalAmountDue = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_HISTORY, "totalamountdue");
            bh.Actions = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_HISTORY, "actions");
            bh.CompareLink = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_HISTORY, "comparelink");
            //Configuration   
            bh.Commodities = CMS.GetWidgetConfig(billConfig, tabKey, WIDGET_TYPE_BILL_HISTORY, "commodities");
            bh.ShowActions = CMS.GetWidgetConfig(billConfig, tabKey, WIDGET_TYPE_BILL_HISTORY, "showactions");
            bh.RowCount = CMS.GetWidgetConfig(billConfig, tabKey, WIDGET_TYPE_BILL_HISTORY, "rowcount");

            return bh;
        }

        public BillUsage GetBilledUsageChart(string tabKey, string parameters, string version = "v1")
        {
            var jp = (JsonParams)JsonConvert.DeserializeObject(QueryStringModule.Decrypt(parameters), typeof(JsonParams));
            //Content
            var content = CMS.GetContent(int.Parse(jp.cl), jp.l).Content;
            var billTextContent =
                content.TextContent.Where(x => x.category == BILL_CATEGORY || x.category == COMMON_CATEGORY).ToList();
            var billUomContent = content.UOM.ToList();
            var billConfig = content.Configuration.Where(x => x.category == BILL_CATEGORY).ToList();
            var billDetailforTotalCost = CMS.GetWidgetConfig(billConfig, tabKey, WIDGET_TYPE_BILLED_USAGE_CHART, "addtototalcost");
            var billDetailforTotalCostList = billDetailforTotalCost.Replace(" ", "").Split(',');

            //Get Bills            
            var billResponse = GetBillsFromCache(jp, version);
            var bu = new BillUsage(billConfig, billTextContent, tabKey, WIDGET_TYPE_BILLED_USAGE_CHART, jp);
            if (billResponse.Customer == null || billResponse.Customer.Accounts.Count == 0 || billResponse.Customer.Accounts[0].Bills.Count == 0)
            {
                bu.NoBills = true;
                bu.NoBillsText = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILLED_USAGE_CHART, "nobills");
                return bu;
            }
            if ((from b in billResponse.Customer.Accounts[0].Bills from premise in b.Premises where premise.Id == jp.p select b).ToList().Count == 0)
            {
                bu.NoBills = true;
                bu.NoBillsText = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILLED_USAGE_CHART, "nobills");
                return bu;
            }

            var cFormat = CMS.GetWidgetConfig(billConfig, tabKey, WIDGET_TYPE_BILLED_USAGE_CHART, "currencyformat");
            var ci = new CultureInfo(jp.l);

            bu.Commodities = CMS.GetWidgetConfig(billConfig, tabKey, WIDGET_TYPE_BILLED_USAGE_CHART, "commodities");

            var billUsage = new List<BillUsageByMonth>();
            var billUsagePreviousYear = new List<BillUsageByMonth>();
            foreach (var c in bu.Commodities.Replace(" ", "").Split(','))
            {
                var commodityBillUsage = new List<BillUsageByMonth>();
                var commodityBillUsagePreviousYear = new List<BillUsageByMonth>();
                var comNameKey = "commodity." + c + ".name";
                if (bu.CommoditiesText == null)
                {
                    bu.CommoditiesText =
                        billTextContent.Where(e => e.key == comNameKey).Select(s => s.shorttext).FirstOrDefault();
                }
                else
                {
                    bu.CommoditiesText += "," +
                                          billTextContent.Where(e => e.key == comNameKey)
                                              .Select(s => s.shorttext)
                                              .FirstOrDefault();
                }

                var bills = billResponse.Customer.Accounts[0].Bills
                    .Where(b => b.Premises.Exists(p => p.Id == jp.p && p.Service.Exists(s => s.CommodityKey == c)))
                    .ToList();

                foreach(var x in bills)
                {
                    foreach(var y in x.Premises)
                    {
                        y.LatestBillEndDate = y.Service.Where(z => z.CommodityKey == c).Max(z => z.BillEndDate);
                    }
                    x.LatestBillEndDate = x.Premises.Max(a => a.LatestBillEndDate);
                }

                bills = bills.OrderByDescending(b => b.LatestBillEndDate).ToList();
                //bills.ForEach(x => x.Premises.ForEach(y => y.Service.OrderByDescending(z => z.BillEndDate);
                //bills.ForEach(x => x.LatestBillEndDate = x.Premises.Where(y => y.Service.Find(z => z.BillEndDate.Max()

                var serviceBills = bills.SelectMany(b => b.Premises).SelectMany(p => p.Service).Where(s => s.CommodityKey == c).ToList();
                var bill = serviceBills.FirstOrDefault();
                if (bill != null)
                {
                    var unit = bill.UOMKey;
                    var uom = billTextContent.FirstOrDefault(z =>
                    {
                        var firstOrDefault = billUomContent.FirstOrDefault(x => x.key == unit);
                        return firstOrDefault != null && z.key == firstOrDefault.namekey;
                    }).shorttext;

                    var latestBillDate = DateTime.Now.AddMonths(-1).ToString(CultureInfo.CurrentCulture);
                    if (bill.GetType().GetProperty("BillEndDate") != null)
                    {
                        latestBillDate = serviceBills.OrderByDescending(x => x.BillEndDate).FirstOrDefault()?.BillEndDate;
                    }


                    var cmf = CMS.GetWidgetConfig(billConfig, tabKey, WIDGET_TYPE_BILLED_USAGE_CHART, "chartmonthformat");
                    var hdf = CMS.GetWidgetConfig(billConfig, tabKey, WIDGET_TYPE_BILLED_USAGE_CHART, "hoverdateformat");

                    var huf = string.Empty;
                    switch (c)
                    {
                        case ELECTRIC_COMMODITY_KEY:
                            huf = CMS.GetWidgetConfig(billConfig, tabKey, WIDGET_TYPE_BILLED_USAGE_CHART, "hoverelectricusageformat");
                            bu.ElectricServiceCount = serviceBills.Count;
                            break;
                        case GAS_COMMODITY_KEY:
                            huf = CMS.GetWidgetConfig(billConfig, tabKey, WIDGET_TYPE_BILLED_USAGE_CHART, "hovergasusageformat");
                            bu.GasServiceCount = serviceBills.Count;
                            break;
                        case WATER_COMMODITY_KEY:
                            huf = CMS.GetWidgetConfig(billConfig, tabKey, WIDGET_TYPE_BILLED_USAGE_CHART, "hoverwaterusageformat");
                            bu.WaterServiceCount = serviceBills.Count;
                            break;
                    }

                    // use most bill days logic to get usage for charted month
                    var chartedMonth = DateTime.MinValue;
                    var duplicateChartedMonth = false;
                    
                    for (var i = 0; i < 24; i++)
                    {
                        if (i < 12)
                        {

                            // get the bill usage.  if there's duplicate charted month, usage will be null
                            var usage = BillUsageByMostBillDaysMonth(i, bills, ci, cFormat, uom,
                                latestBillDate,
                                cmf, hdf, huf, c, billDetailforTotalCostList, ref chartedMonth);

                            if (usage == null)
                            {
                                duplicateChartedMonth = true;
                                break;
                            }

                            commodityBillUsage.Add(usage);
                            chartedMonth = chartedMonth.AddMonths(-1);
                        }
                        else
                        {
                            // get the bill usage.  if there's duplicate charted month, usage will be null
                            var usage = BillUsageByMostBillDaysMonth(i, bills, ci, cFormat, uom,
                                latestBillDate,
                                cmf, hdf, huf, c, billDetailforTotalCostList, ref chartedMonth);
                            if (usage == null)
                            {
                                duplicateChartedMonth = true;
                                break;
                            }

                            commodityBillUsagePreviousYear.Add(usage);
                            chartedMonth = chartedMonth.AddMonths(-1);

                        }
                    }

                    // if there's duplicate charted month using most bill days logic, then fall back to use bill end date logic
                    if(duplicateChartedMonth)
                    {
                        commodityBillUsage = new List<BillUsageByMonth>();
                        commodityBillUsagePreviousYear = new List<BillUsageByMonth>();
                        for (var i = 0; i < 24; i++)
                        {
                            if (i < 12)
                            {
                                commodityBillUsage.Add(BillUsageByMonth(i, bills, ci, cFormat, uom, latestBillDate, cmf, hdf, huf, c, billDetailforTotalCostList));
                            }
                            else
                            {
                                billUsagePreviousYear.Add(BillUsageByMonth(i, bills, ci, cFormat, uom, latestBillDate, cmf, hdf, huf, c, billDetailforTotalCostList));
                            }
                        }
                    }
                    
                    billUsage.AddRange(commodityBillUsage);
                    billUsagePreviousYear.AddRange(commodityBillUsagePreviousYear);
                }
            }

            if (billUsage.Sum(r => r.YAxis) + billUsagePreviousYear.Sum(r => r.YAxis) <= 0)
            {
                bu.NoBills = true;
                bu.NoBillsText = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILLED_USAGE_CHART, "nobills");
                return bu;
            }

            bu.BilledUsageLastYear = billUsage;
            bu.BilledUsagePreviousYear = billUsagePreviousYear;
            if (billUsage.Count > 0)
                bu.LastYear = Convert.ToDateTime(billUsage.FirstOrDefault().Tip.Services.FirstOrDefault().XAxis).Year.ToString();
            if (billUsagePreviousYear.Count > 0)
                bu.PreviousYear = Convert.ToDateTime(billUsagePreviousYear.FirstOrDefault().Tip.Services.FirstOrDefault().XAxis).Year.ToString();

            //Content
            bu.CompareTo = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILLED_USAGE_CHART, "compareto");
            bu.AvgTemp = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILLED_USAGE_CHART, "avgtemp");
            bu.WeatherLabel = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILLED_USAGE_CHART, "weatherlabel");
            bu.BillDate = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILLED_USAGE_CHART, "billdate");
            bu.CurrentYearLegend = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILLED_USAGE_CHART, "currentyearlegend");
            bu.PrevYearLegend = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILLED_USAGE_CHART, "prevyearlegend");
            //Configuration   
            bu.ShowCommodityLabels = CMS.GetWidgetConfig(billConfig, tabKey, WIDGET_TYPE_BILLED_USAGE_CHART, "showcommoditylabels");
            bu.ChartType = CMS.GetWidgetConfig(billConfig, tabKey, WIDGET_TYPE_BILLED_USAGE_CHART, "charttype");
            bu.ChartType = bu.ChartType == "" ? CharTypeColumn : bu.ChartType;
            bu.ChartLineColor1 = CMS.GetWidgetConfig(billConfig, tabKey, WIDGET_TYPE_BILLED_USAGE_CHART, "chartlinecolor1");
            bu.ChartLineColor2 = CMS.GetWidgetConfig(billConfig, tabKey, WIDGET_TYPE_BILLED_USAGE_CHART, "chartlinecolor2");
            bu.ChartLineColor3 = CMS.GetWidgetConfig(billConfig, tabKey, WIDGET_TYPE_BILLED_USAGE_CHART, "chartlinecolor3");
            bu.ChartAnchorType = CMS.GetWidgetConfig(billConfig, tabKey, WIDGET_TYPE_BILLED_USAGE_CHART, "chartanchortype");
            bu.LineThickness = CMS.GetWidgetConfig(billConfig, tabKey, WIDGET_TYPE_BILLED_USAGE_CHART, "linethickness");
            bu.EnableWeather = CMS.GetWidgetConfig(billConfig, tabKey, WIDGET_TYPE_BILLED_USAGE_CHART, "enableweather");
            bu.ShowWeatherByDefault = Convert.ToBoolean(CMS.GetWidgetConfig(billConfig, tabKey, WIDGET_TYPE_BILLED_USAGE_CHART, "showweatherbydefault"));

            return bu;
        }

        /// <summary>
        /// set up tool tip info for each service
        /// </summary>
        /// <param name="bills"></param>
        /// <param name="services"></param>
        /// <param name="billEndDate"></param>
        /// <param name="billDetailforTotalCostList"></param>
        /// <param name="ci"></param>
        /// <param name="cFormat"></param>
        /// <param name="hoverDateFormat"></param>
        /// <param name="hoverUsageFormat"></param>
        /// <param name="uom"></param>
        /// <returns></returns>
        private List<ServiceTooltip> GetBilledUsageToolTipForService(IEnumerable<Bill> bills, List<Service> services, DateTime billEndDate, string[] billDetailforTotalCostList, CultureInfo ci, string cFormat, string hoverDateFormat, string hoverUsageFormat, string uom)
        {
            var serviceToolTips = new List<ServiceTooltip>();
            var billList = bills as IList<Bill> ?? bills.ToList();
            if (services.Any())
            {
                var serviceBillDate = DateTime.MinValue;
                var serviceToolTip = new ServiceTooltip();
                double serviceTotalCost = 0, serviceTotalUse = 0;
                foreach (var service in services)
                {

                    var bill = billList.FirstOrDefault(b => b.Premises.SelectMany(p => p.Service).ToList().Exists(
                        s => s.BillEndDate == service.BillEndDate &&
                             s.BillStartDate == service.BillStartDate &&
                             s.CommodityKey == service.CommodityKey));

                    var billDate = bill != null ? Convert.ToDateTime(bill.BillDate) : billEndDate;
                    if (billDate != serviceBillDate)
                    {
                        if(serviceBillDate != DateTime.MinValue)
                            serviceToolTips.Add(serviceToolTip);
                        serviceBillDate = billDate;
                        serviceTotalCost = 0;
                        serviceTotalUse = 0;
                        serviceToolTip = new ServiceTooltip();

                    }
                    serviceTotalCost = serviceTotalCost + services.Sum(x => x.CostofUsage);
                    if (billDetailforTotalCostList.Any())
                    {
                        double numeric;
                        var detailsFromTotal = service.CostDetails
                            .Where(d => billDetailforTotalCostList.Contains(d.Name) &&
                                        double.TryParse(d.Value, out numeric))
                            .Sum(d => Convert.ToDouble(d.Value));
                        serviceTotalCost = serviceTotalCost + detailsFromTotal;
                    }
                    serviceTotalUse = serviceTotalUse + service.TotalServiceUse;
                    serviceToolTip.XAxis = billDate.ToString(hoverDateFormat, ci);
                    serviceToolTip.ServiceAndCost =
                        serviceTotalUse.ToString(hoverUsageFormat, ci) + " " + uom +
                        " | " + serviceTotalCost.ToString(cFormat, ci);
                }
                serviceToolTips.Add(serviceToolTip);
            }
            else
            {
                serviceToolTips.Add(new ServiceTooltip
                {
                    ServiceAndCost = string.Empty,
                    XAxis = billEndDate.ToString(hoverDateFormat, ci)
                });
            }
            return serviceToolTips;

        }
        private BillUsageByMonth BillUsageByMostBillDaysMonth(int i, List<Bill> bills, CultureInfo ci, string cFormat,
            string uom, string latestBillEnddate, string monthFormat,
            string hoverDateFormat, string hoverUsageFormat, string commodity, string[] billDetailforTotalCostList, ref DateTime chartedMonth)
        {
            DateTime billDate;

            bool duplicateChartedMonth;
            // get all ther services for charted month based on most bill days logic within the same bill
            var services =
                GetChartedServicesForMostBillDaysMonth(i, bills, latestBillEnddate, commodity, ref chartedMonth,
                    out billDate, out duplicateChartedMonth);
            
            // if no duplicate, billed usage charted model
            if (!duplicateChartedMonth)
            {
                var dt = Convert.ToDateTime(latestBillEnddate).AddMonths(-1 * i);
                var bubm = new BillUsageByMonth();
                var tt = new Tooltip();
                bubm.XAxis = new DateTime(chartedMonth.Year,chartedMonth.Month,1).ToString(monthFormat, ci);
                bubm.YAxis = services.Sum(r => r.TotalServiceUse);
                bubm.UOM = uom;
                bubm.Commodity = commodity;
                var totalCost = services.Sum(x => x.CostofUsage);

                if (billDetailforTotalCostList.Any())
                {
                    var detailsFrom = services.SelectMany(b => b.CostDetails).ToList();
                    double numeric;
                    var detailsFromTotal = detailsFrom
                        .Where(d => billDetailforTotalCostList.Contains(d.Name) && double.TryParse(d.Value, out numeric))
                        .Sum(d => Convert.ToDouble(d.Value));
                    totalCost = totalCost + detailsFromTotal;
                }
                bubm.Cost = totalCost.ToString(cFormat, ci);

                // get tool tip for each services to cover all the services in the same bills
                var serviceTt = GetBilledUsageToolTipForService(bills, services, billDate, billDetailforTotalCostList, ci,
                    cFormat, hoverDateFormat, hoverUsageFormat, uom);
                tt.Services = serviceTt;

                if (services.Count > 1)
                {
                    tt.AverageTemperature = CalcAverageTemperature(services);
                }
                else if (services.Count == 1)
                {
                    var firstOrDefault = services.FirstOrDefault();
                    if (firstOrDefault != null)
                        tt.AverageTemperature = firstOrDefault.AverageTemperature;
                }
                else
                {
                    var serviceBills = bills.SelectMany(b => b.Premises).SelectMany(p => p.Service).Where(s => s.CommodityKey == commodity).ToList();

                    var prevbilldate = dt.AddMonths(-1);
                    var nextbilldate = dt.AddMonths(1);
                    var prevbill = serviceBills.Where(x => (Convert.ToDateTime(x.BillEndDate).Month >= prevbilldate.Month &&
                                                            Convert.ToDateTime(x.BillEndDate).Year == prevbilldate.Year || 
                                                           Convert.ToDateTime(x.BillEndDate).Month <= prevbilldate.Month &&
                                                           Convert.ToDateTime(x.BillEndDate).Year > prevbilldate.Year) &&
                                                           (Convert.ToDateTime(x.BillStartDate).Month <= prevbilldate.Month &&
                                                            Convert.ToDateTime(x.BillStartDate).Year == prevbilldate.Year ||
                                                            Convert.ToDateTime(x.BillStartDate).Month >= prevbilldate.Month &&
                                                            Convert.ToDateTime(x.BillStartDate).Year < prevbilldate.Year)).ToList();
                    var nextbill = serviceBills.Where(x => (Convert.ToDateTime(x.BillEndDate).Month >= nextbilldate.Month &&
                                                            Convert.ToDateTime(x.BillEndDate).Year == nextbilldate.Year ||
                                                            Convert.ToDateTime(x.BillEndDate).Month <= nextbilldate.Month &&
                                                            Convert.ToDateTime(x.BillEndDate).Year > nextbilldate.Year) &&
                                                           (Convert.ToDateTime(x.BillStartDate).Month <= nextbilldate.Month &&
                                                            Convert.ToDateTime(x.BillStartDate).Year == nextbilldate.Year ||
                                                            Convert.ToDateTime(x.BillStartDate).Month >= nextbilldate.Month &&
                                                            Convert.ToDateTime(x.BillStartDate).Year < nextbilldate.Year)).ToList();
                    if (prevbill.Count > 0 && nextbill.Count > 0)
                    {
                        tt.AverageTemperature = (CalcAverageTemperature(prevbill) + CalcAverageTemperature(nextbill)) / 2;
                    }
                    else
                    {
                        if (prevbill.Count == 0 && nextbill.Count > 0)
                        {
                            var prev2Billdate = dt.AddMonths(-2);
                            var prev2Bill = serviceBills.Where(x => (Convert.ToDateTime(x.BillEndDate).Month >= prev2Billdate.Month &&
                                                                     Convert.ToDateTime(x.BillEndDate).Year == prev2Billdate.Year ||
                                                                     Convert.ToDateTime(x.BillEndDate).Month <= prev2Billdate.Month &&
                                                                     Convert.ToDateTime(x.BillEndDate).Year > prev2Billdate.Year) &&
                                                                    (Convert.ToDateTime(x.BillStartDate).Month <= prev2Billdate.Month &&
                                                                     Convert.ToDateTime(x.BillStartDate).Year == prev2Billdate.Year ||
                                                                     Convert.ToDateTime(x.BillStartDate).Month >= prev2Billdate.Month &&
                                                                     Convert.ToDateTime(x.BillStartDate).Year < prev2Billdate.Year)).ToList();
                            tt.AverageTemperature = prev2Bill.Count == 0 ? 0 : CalcAverageTemperature(nextbill);
                        }
                        else if (prevbill.Count > 0)
                        {
                            var next2Billdate = dt.AddMonths(2);
                            var next2Bill = serviceBills.Where(x => (Convert.ToDateTime(x.BillEndDate).Month >= next2Billdate.Month &&
                                                                     Convert.ToDateTime(x.BillEndDate).Year == next2Billdate.Year ||
                                                                     Convert.ToDateTime(x.BillEndDate).Month <= next2Billdate.Month &&
                                                                     Convert.ToDateTime(x.BillEndDate).Year > next2Billdate.Year) &&
                                                                    (Convert.ToDateTime(x.BillStartDate).Month <= next2Billdate.Month &&
                                                                     Convert.ToDateTime(x.BillStartDate).Year == next2Billdate.Year ||
                                                                     Convert.ToDateTime(x.BillStartDate).Month >= next2Billdate.Month &&
                                                                     Convert.ToDateTime(x.BillStartDate).Year < next2Billdate.Year)).ToList();
                            tt.AverageTemperature = next2Bill.Count == 0 ? 0 : CalcAverageTemperature(prevbill);
                        }
                        else tt.AverageTemperature = 0;
                    }
                }
                bubm.Tip = tt;

                return bubm;
            }

            return null;
        }

        private List<Service> GetChartedServicesForMostBillDaysMonth(int i, List<Bill> bills, string latestBillEndDate, string commodity, ref DateTime chartedMonthYear, out DateTime billDate, out bool duplicateChartedMonth)
        {
            var date = Convert.ToDateTime(latestBillEndDate).AddMonths(-1 * i);

            var startMonth = Convert.ToInt32(date.AddMonths(-1).Month);
            var startYear = Convert.ToInt32(date.AddMonths(-1).Year);

            var endMonth = Convert.ToInt32(date.Month);
            var endYear = Convert.ToInt32(date.Year);

            billDate = DateTime.MinValue;
            var chartedServices = new List<Service>();

            // get all the bills for the inital chart period
            var billMonthYear = bills.Where(b => b.Premises.SelectMany(p => p.Service).ToList()
                .Exists(s => (Convert.ToDateTime(s.BillEndDate).Month >= endMonth &&
                              Convert.ToDateTime(s.BillEndDate).Year == endYear ||
                              Convert.ToDateTime(s.BillEndDate).Month <= endMonth &&
                              Convert.ToDateTime(s.BillEndDate).Year > endYear ||
                             Convert.ToDateTime(s.BillEndDate).Month == startMonth &&
                             Convert.ToDateTime(s.BillEndDate).Year == startYear) &&
                             (Convert.ToDateTime(s.BillStartDate).Month <= startMonth &&
                              Convert.ToDateTime(s.BillStartDate).Year == startYear ||
                              Convert.ToDateTime(s.BillStartDate).Month >= startMonth &&
                              Convert.ToDateTime(s.BillStartDate).Year < startYear ||
                              Convert.ToDateTime(s.BillStartDate).Month == endMonth &&
                              Convert.ToDateTime(s.BillStartDate).Year == endYear))).ToList();

            duplicateChartedMonth = false;
            DateTime chartedBillMonth = DateTime.MinValue;
            // determine which one has the most day on charted month
            foreach (var bill in billMonthYear)
            {
                var services = bill.Premises.SelectMany(p => p.Service).Where(s => s.CommodityKey == commodity)
                    .ToList();
                // get all the services for each bill for the inital chart period
                // then determine which bill should be used for charted month, and if there's duplicate
                var serviceMonthYear = services
                    .Where(x => (Convert.ToDateTime(x.BillEndDate).Month >= endMonth &&
                                 Convert.ToDateTime(x.BillEndDate).Year == endYear ||
                                       Convert.ToDateTime(x.BillEndDate).Month <= endMonth &&
                                       Convert.ToDateTime(x.BillEndDate).Year > endYear ||
                                Convert.ToDateTime(x.BillEndDate).Month == startMonth &&
                                Convert.ToDateTime(x.BillEndDate).Year == startYear) &&
                                (Convert.ToDateTime(x.BillStartDate).Month <= startMonth &&
                                 Convert.ToDateTime(x.BillStartDate).Year == startYear ||
                                 Convert.ToDateTime(x.BillStartDate).Month >= startMonth &&
                                 Convert.ToDateTime(x.BillStartDate).Year < startYear ||
                                 Convert.ToDateTime(x.BillStartDate).Month == endMonth &&
                                 Convert.ToDateTime(x.BillStartDate).Year == endYear)).ToList();

                var serviceIds = serviceMonthYear.OrderByDescending(a => a.BillEndDate).Select(s => s.Id).Distinct();
                var serviceStartDate = DateTime.MaxValue;
                var serviceEndDate = DateTime.MinValue;

                DateTime billMonth = DateTime.MinValue;
                foreach (var id in serviceIds)
                {
                    var serviceList = serviceMonthYear.Where(s => s.Id == id).OrderByDescending(a => a.BillEndDate).ToList();
                    DateTime serviceMonth = DateTime.MinValue;
                    foreach (var service in serviceList)

                    {
                        var startDate = Convert.ToDateTime(service.BillStartDate);
                        var endDate = Convert.ToDateTime(service.BillEndDate);

                        // if the start and end date are on the same month, 
                        // it will be the serviceMonth if there's not already a duplicate charted month
                        if (startDate.Month == endDate.Month && startDate.Year == endDate.Year)
                        {
                            if (serviceMonth != DateTime.MinValue && serviceMonth.Month == endDate.Month)
                            {
                                duplicateChartedMonth = true;
                                break;
                            }
                            serviceMonth = endDate;
                        }
                        else
                        {
                            var numOfMonths = (endDate.Year - startDate.Year) * 12 + endDate.Month - startDate.Month;
                            // the bill period cover more than 2 months, 
                            // then find the month with the most days beside the start and end for serviceMonth if there's not already a duplicate charted month
                            if (numOfMonths > 1)
                            {
                                var maxNumOfDays = 0; //(serviceEndDate.Date - serviceStartDate.Date).TotalDays;
                                for (var j = 0; j <= numOfMonths; j++)
                                {
                                    var dt = endDate.AddMonths(j * -1);
                                    var numOfDaysInMonth = DateTime.DaysInMonth(dt.Year, dt.Month);

                                    if (j == 0)
                                    {
                                        numOfDaysInMonth = dt.Day;
                                    }
                                    else if (j == numOfMonths)
                                    {
                                        numOfDaysInMonth = DateTime.DaysInMonth(startDate.Year, startDate.Month) -
                                                           (startDate.Day - 1);
                                    }
                                    //var serviceMonth = date.ToString(monthFormat, ci);
                                    if (numOfDaysInMonth > maxNumOfDays)
                                    {
                                        maxNumOfDays = numOfDaysInMonth;
                                        if (serviceMonth != DateTime.MinValue && serviceMonth.Month == dt.Month)
                                        {
                                            duplicateChartedMonth = true;
                                            break;
                                        }
                                        serviceMonth = dt;
                                    }
                                }
                            }
                            else
                            {
                                var numOfDaysInStart = DateTime.DaysInMonth(startDate.Year, startDate.Month) -
                                                       (startDate.Day - 1);
                                var numOfDaysInEnd = endDate.Day;
                                if (numOfDaysInStart > numOfDaysInEnd)
                                {
                                    if (serviceMonth != DateTime.MinValue && serviceMonth.Month == startDate.Month)
                                    {
                                        duplicateChartedMonth = true;
                                        break;
                                    }
                                    serviceMonth = startDate;

                                }
                                else
                                {
                                    if (serviceMonth != DateTime.MinValue && serviceMonth.Month == endDate.Month)
                                    {
                                        duplicateChartedMonth = true;
                                        break;
                                    }
                                    serviceMonth = endDate;
                                }
                            }
                        }

                        if (chartedMonthYear == DateTime.MinValue || (chartedMonthYear != DateTime.MinValue && serviceMonth.Month == chartedMonthYear.Month))
                        {
                            billDate = Convert.ToDateTime(bill.BillDate);
                            chartedMonthYear = serviceMonth;
                            serviceStartDate = startDate < serviceStartDate ? startDate : serviceStartDate;
                            serviceEndDate = endDate > serviceEndDate ? endDate : serviceEndDate;
                            chartedServices.Add(service);
                        }

                    }
                    
                    if (duplicateChartedMonth)
                        break;

                    if(serviceMonth != DateTime.MinValue)
                        billMonth = serviceMonth;
                }


                if (duplicateChartedMonth)
                    break;

                if (billMonth != DateTime.MinValue && chartedBillMonth != DateTime.MinValue && chartedBillMonth.Month == billMonth.Month)
                {
                    duplicateChartedMonth = true;
                    break;
                }
                chartedBillMonth = billMonth;
            }

            if (duplicateChartedMonth)
                return new List<Service>();
            

            return chartedServices;
        }


        private BillUsageByMonth BillUsageByMonth(int i, List<Bill> bills, CultureInfo ci, string cFormat,
            string uom, string latestBillEnddate, string monthFormat,
            string hoverDateFormat, string hoverUsageFormat, string commodity, string[] billDetailforTotalCostList)
        {
            var dt = Convert.ToDateTime(latestBillEnddate).AddMonths(-1 * i);
            var month = Convert.ToInt32(dt.Month);
            var year = Convert.ToInt32(dt.Year);
            var bubm = new BillUsageByMonth();
            var tt = new Tooltip();
            var serviceBills = bills.SelectMany(b => b.Premises).SelectMany(p => p.Service).Where(s => s.CommodityKey == commodity).ToList();

            var billMonthYear = bills.Where(b => b.Premises.SelectMany(p => p.Service).ToList()
                .Exists(s => Convert.ToDateTime(s.BillEndDate).Month == month &&
                             Convert.ToDateTime(s.BillEndDate).Year == year)).ToList();

            var serviceBillMonthYear = billMonthYear.SelectMany(b => b.Premises).SelectMany(p => p.Service)
                .Where(s => s.CommodityKey == commodity && Convert.ToDateTime(s.BillEndDate).Month == month &&
                            Convert.ToDateTime(s.BillEndDate).Year == year).ToList();

            bubm.XAxis = dt.ToString(monthFormat, ci);
            bubm.YAxis = serviceBillMonthYear.Sum(r => r.TotalServiceUse);
            bubm.UOM = uom;
            bubm.Commodity = commodity;
            var totalCost = serviceBillMonthYear.Sum(x => x.CostofUsage);

            if (billDetailforTotalCostList.Any())
            {
                var detailsFrom = serviceBillMonthYear.SelectMany(b => b.CostDetails).ToList();
                double numeric;
                var detailsFromTotal = detailsFrom
                    .Where(d => billDetailforTotalCostList.Contains(d.Name) && double.TryParse(d.Value, out numeric))
                    .Sum(d => Convert.ToDouble(d.Value));
                totalCost = totalCost + detailsFromTotal;
            }
            bubm.Cost = totalCost.ToString(cFormat, ci);

            var billDate = Convert.ToDateTime(billMonthYear.FirstOrDefault()?.BillDate);
            
            // get tool tip info for each servcie in the charted month
            var serviceTt = GetBilledUsageToolTipForService(bills, serviceBillMonthYear, billDate, billDetailforTotalCostList, ci,
                cFormat, hoverDateFormat, hoverUsageFormat, uom);
            tt.Services = serviceTt;

            if (serviceBillMonthYear.Count > 1)
            {
                tt.AverageTemperature = CalcAverageTemperature(serviceBillMonthYear);
            }
            else if (serviceBillMonthYear.Count == 1)
            {
                var firstServiceBillMonthYear = serviceBillMonthYear.FirstOrDefault();
                if (firstServiceBillMonthYear != null)
                    tt.AverageTemperature = firstServiceBillMonthYear.AverageTemperature;
            }
            else
            {
                var prevbilldate = dt.AddMonths(-1);
                var nextbilldate = dt.AddMonths(1);
                var prevbill = serviceBills.Where(x => Convert.ToDateTime(x.BillEndDate).Month == prevbilldate.Month &&
                                                       Convert.ToDateTime(x.BillEndDate).Year == prevbilldate.Year).ToList();
                var nextbill = serviceBills.Where(x => Convert.ToDateTime(x.BillEndDate).Month == nextbilldate.Month &&
                                                       Convert.ToDateTime(x.BillEndDate).Year == nextbilldate.Year).ToList();
                if (prevbill.Count > 0 && nextbill.Count > 0)
                {
                    tt.AverageTemperature = (CalcAverageTemperature(prevbill) + CalcAverageTemperature(nextbill)) / 2;
                }
                else
                {
                    if (prevbill.Count == 0 && nextbill.Count > 0)
                    {
                        var prev2Billdate = dt.AddMonths(-2);
                        var prev2Bill = serviceBills.Where(x => Convert.ToDateTime(x.BillEndDate).Month == prev2Billdate.Month &&
                                                                Convert.ToDateTime(x.BillEndDate).Year == prev2Billdate.Year).ToList();
                        tt.AverageTemperature = prev2Bill.Count == 0 ? 0 : CalcAverageTemperature(nextbill);
                    }
                    else if (prevbill.Count > 0)
                    {
                        var next2Billdate = dt.AddMonths(2);
                        var next2Bill = serviceBills.Where(x => Convert.ToDateTime(x.BillEndDate).Month == next2Billdate.Month &&
                                                                Convert.ToDateTime(x.BillEndDate).Year == next2Billdate.Year).ToList();
                        tt.AverageTemperature = next2Bill.Count == 0 ? 0 : CalcAverageTemperature(prevbill);
                    }
                    else tt.AverageTemperature = 0;
                }
            }
            bubm.Tip = tt;

            return bubm;
        }

        private static int CalcAverageTemperature(List<Service> bills)
        {
            int avgtemp = 0, billdays = 0;
            foreach (var bmy in bills)
            {
                avgtemp += bmy.AverageTemperature * bmy.BillDays;
                billdays += bmy.BillDays;
            }
            if (avgtemp == 0 && billdays == 0) return 0;

            return (avgtemp / billdays);
        }

        public BillComparison GetBillComparison(string tabKey, string parameters, string compareDate, string compareToDate,
            Enums.BillComparisonType type = Enums.BillComparisonType.PreviousBill, string version = "v1")
        {
            var jp = (JsonParams)JsonConvert.DeserializeObject(QueryStringModule.Decrypt(parameters), typeof(JsonParams));
            //Content
            var content = CMS.GetContent(int.Parse(jp.cl), jp.l).Content;
            var billTextContent =
                content.TextContent.Where(x => x.category == BILL_CATEGORY || x.category == COMMON_CATEGORY).ToList();
            var billUomContent = content.UOM.ToList();
            var billConfig =
                content.Configuration.Where(x => x.category == BILL_CATEGORY || x.category == COMMON_CATEGORY).ToList();
            var billDetailsConfig = CMS.GetWidgetConfigBulk(content, tabKey, WIDGET_TYPE_BILL_COMPARISON, "billdetails");

            var billDetailSettings =
                billDetailsConfig != null && !string.IsNullOrEmpty(billDetailsConfig)
                    ? JsonConvert.DeserializeObject<BillDetailSettings>(billDetailsConfig).BillDetails
                    : null;

            billDetailSettings = billDetailSettings?.OrderBy(d => d.Order).ToList();


            //Get Bills            
            var billResponse = GetBillsFromCache(jp, version);
            var bc = new BillComparison(billConfig, billTextContent, tabKey, WIDGET_TYPE_BILL_COMPARISON, jp);
            if (billResponse.Customer == null || billResponse.Customer.Accounts.Count == 0 || billResponse.Customer.Accounts[0].Bills.Count == 0)
            {
                bc.NoBills = true;
                bc.NoBillsText = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_COMPARISON, "nobills");
                return bc;
            }
            if ((from b in billResponse.Customer.Accounts[0].Bills from premise in b.Premises where premise.Id == jp.p select b).ToList().Count == 0)
            {
                bc.NoBills = true;
                bc.NoBillsText = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_COMPARISON, "nobills");
                return bc;
            }

            if (billResponse.Customer != null && billResponse.Customer.Accounts[0].Bills.Where(b => b.Premises.Exists(p => p.Id == jp.p)).ToList().Count == 1)
            {
                bc.OneBill = true;
                bc.OneBillText = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_COMPARISON, "onebill");
                return bc;
            }
            
            var service = billResponse.Customer.Accounts[0].Bills.SelectMany(x => x.Premises).SelectMany(x => x.Service).ToList();
            bc.ElectricServiceCount = service.Count(t => t.CommodityKey == ELECTRIC_COMMODITY_KEY);
            bc.GasServiceCount = service.Count(t => t.CommodityKey == GAS_COMMODITY_KEY);
            bc.WaterServiceCount = service.Count(t => t.CommodityKey == WATER_COMMODITY_KEY);

            var keys = CMS.GetWidgetConfig(billConfig, tabKey, WIDGET_TYPE_BILL_COMPARISON, "comparisonkeys");
            var values = string.Empty;
            foreach (var c in keys.Replace(" ", "").Split(','))
            {
                if (string.IsNullOrEmpty(values))
                {
                    values = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_COMPARISON, c);
                }
                else
                {
                    values += "," + CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_COMPARISON, c);
                }
            }

            var k = keys.Split(',').ToList();
            var v = values.Split(',').ToList();

            var tempValues = new List<string>();
            foreach (var i in Mapper.Map(k, tempValues))
            {
                Bill compDate = null, compToDate = null;
                switch (i)
                {
                    case "previousbill":
                        var dates = (from b in billResponse.Customer.Accounts[0].Bills.OrderByDescending(x => x.BillDate) from premise in b.Premises where premise.Id == jp.p select b).ToList();
                        compDate = dates.ElementAt(1);
                        compToDate = dates.FirstOrDefault();
                        break;
                    case "samemonthlastyear":
                        compDate = (from b in billResponse.Customer.Accounts[0].Bills.OrderByDescending(x => x.BillDate) from premise in b.Premises where premise.Id == jp.p select b).FirstOrDefault();
                        var dt = DateTime.Parse(compDate.BillDate).AddMonths(-12).ToString("yyyy-MM-dd");
                        compToDate = (from b in billResponse.Customer.Accounts[0].Bills from premise in b.Premises where premise.Id == jp.p select b).FirstOrDefault(x => x.BillDate == dt);
                        break;
                    case "mostexpensivetoleastexpensive":
                        compToDate = (from b in billResponse.Customer.Accounts[0].Bills.OrderByDescending(x => x.TotalAmount) from premise in b.Premises where premise.Id == jp.p select b).FirstOrDefault();
                        compDate = (from b in billResponse.Customer.Accounts[0].Bills.OrderBy(x => x.TotalAmount) from premise in b.Premises where premise.Id == jp.p select b).FirstOrDefault();
                        break;
                }
                if (i != "custom" && (compDate == null || compToDate == null))
                {
                    var index = k.IndexOf(i);
                    k.Remove(i);
                    v.RemoveAt(index);
                }
            }

            bc.CompareDropDownKeys = string.Join(",", k);
            bc.CompareDropDownValues = string.Join(",", v);

            if (string.IsNullOrEmpty(compareDate) && !string.IsNullOrEmpty(compareToDate))
            {
                var dates = (from b in billResponse.Customer.Accounts[0].Bills.OrderByDescending(x => x.BillDate) from premise in b.Premises where premise.Id == jp.p select b).ToList();
                var index = dates.ToList().FindIndex(x => x.BillDate == compareToDate);
                compareDate = dates.ElementAt(index + 1).BillDate;
                compareToDate = dates.ElementAt(index).BillDate;
            }
            else if (string.IsNullOrEmpty(compareDate) && string.IsNullOrEmpty(compareToDate))
            {
                switch (type)
                {
                    case Enums.BillComparisonType.PreviousBill:
                    {
                        var dates = (from b in billResponse.Customer.Accounts[0].Bills.OrderByDescending(x => x.BillDate) from premise in b.Premises where premise.Id == jp.p select b).ToList().Select(x => x.BillDate).Distinct().ToList();
                        compareDate = dates.ElementAt(1);
                        compareToDate = dates.FirstOrDefault();
                    }
                        break;
                    case Enums.BillComparisonType.MostExpensiveToLeastExpensive:
                        compareToDate = (from b in billResponse.Customer.Accounts[0].Bills.OrderByDescending(x => x.TotalAmount) from premise in b.Premises where premise.Id == jp.p select b).ToList().FirstOrDefault().BillDate;
                        compareDate = (from b in billResponse.Customer.Accounts[0].Bills.OrderBy(x => x.TotalAmount) from premise in b.Premises where premise.Id == jp.p select b).ToList().FirstOrDefault().BillDate;
                        break;
                    case Enums.BillComparisonType.PeakSummerToPeakWinter:
                    {
                        var dates = (from b in billResponse.Customer.Accounts[0].Bills.OrderByDescending(x => x.BillDate) from premise in b.Premises where premise.Id == jp.p select b).ToList();
                        compareDate = dates.ElementAt(1).BillDate;
                        compareToDate = dates.FirstOrDefault().BillDate;
                    }
                        break;
                    case Enums.BillComparisonType.SameMonthLastYear:
                        compareToDate = (from b in billResponse.Customer.Accounts[0].Bills.OrderByDescending(x => x.BillDate) from premise in b.Premises where premise.Id == jp.p select b).ToList().FirstOrDefault().BillDate;
                        compareDate = DateTime.Parse(compareToDate).AddMonths(-12).ToString("yyyy-MM-dd");
                        break;
                }
            }

            var cFormatTable = CMS.GetWidgetConfig(billConfig, tabKey, WIDGET_TYPE_BILL_COMPARISON, "tablecurrencyformat");
            var cFormatGraph = CMS.GetWidgetConfig(billConfig, tabKey, WIDGET_TYPE_BILL_COMPARISON, "graphcurrencyformat");
            var dFormat = CMS.GetWidgetConfig(billConfig, tabKey, WIDGET_TYPE_BILL_COMPARISON, "dateformat");

            var showDetails = CMS.GetWidgetConfig(billConfig, tabKey, WIDGET_TYPE_BILL_COMPARISON, "showdetails");
            var bShowDetails = !string.IsNullOrEmpty(showDetails) && Convert.ToBoolean(showDetails);
            var billDetailforTotalCost = CMS.GetWidgetConfig(billConfig, tabKey, WIDGET_TYPE_BILL_COMPARISON, "totalcost");
            var billDetailforTotalCostList = billDetailforTotalCost.Replace(" ", "").Split(',');

            var ci = new CultureInfo(jp.l);
            var noParens = (NumberFormatInfo)CultureInfo.CurrentCulture.NumberFormat.Clone();
            noParens.CurrencyNegativePattern = 1;
            bc.Commodities = CMS.GetWidgetConfig(billConfig, tabKey, WIDGET_TYPE_BILL_COMPARISON, "commodities");

            bc.Dates = (from b in billResponse.Customer.Accounts[0].Bills.OrderByDescending(x => x.BillDate) from premise in b.Premises where premise.Id == jp.p select b)
                .Select(x => new { x.BillDate, BillDateFormatted = DateTime.Parse(x.BillDate).ToString(dFormat) }).Distinct()
                .ToDictionary(x => x.BillDate, x => x.BillDateFormatted);

            //Compare From Date
            var bills = (from b in billResponse.Customer.Accounts[0].Bills from premise in b.Premises where premise.Id == jp.p select b).Where(x => x.BillDate == compareDate);
            var comms = bc.Commodities.Replace(" ", "").Split(',');
            var billList = bills as List<Bill> ?? bills.ToList();
            var compareFrom = PopulateCompareAttributes(billList, cFormatTable, jp.p);
            compareFrom.CompareDate = compareDate;
            compareFrom.CompareDateFormatted = DateTime.Parse(compareDate).ToString(dFormat, ci);
            compareFrom.Commodities = PopulateCommodity(billList, cFormatTable, billTextContent, billUomContent, billDetailSettings, bShowDetails, comms, jp.p, jp);
            if (compareFrom.Commodities.Count > 1)
            {
                compareFrom.TotalCost = compareFrom.Commodities.Sum(c => c.TotalCost);
            }

            if (bShowDetails)
            {
                var detailsFrom = compareFrom.Commodities.SelectMany(c => c.AdditionalDetails).ToList();
                double numeric;
                var detailsFromTotal = detailsFrom
                    .Where(d => billDetailforTotalCostList.Contains(d.Name) && double.TryParse(d.Value, out numeric))
                    .Sum(d => Convert.ToDouble(d.Value));
                compareFrom.TotalCost = compareFrom.TotalCost + detailsFromTotal;
            }

            compareFrom.TotalCostFormatted = compareFrom.TotalCost.ToString(cFormatTable);

            var bct = new BillCompareType();
            bct.CompareFrom = compareFrom;

            //Compare To Date
            bills = (from b in billResponse.Customer.Accounts[0].Bills from premise in b.Premises where premise.Id == jp.p select b).Where(x => x.BillDate == compareToDate);
            var bList = bills as List<Bill> ?? bills.ToList();
            var compareTo = PopulateCompareAttributes(bList, cFormatTable, jp.p);
            compareTo.CompareDate = compareToDate;
            compareTo.CompareDateFormatted = DateTime.Parse(compareToDate).ToString(dFormat, ci);
            compareTo.Commodities = PopulateCommodity(bList, cFormatTable, billTextContent, billUomContent, billDetailSettings, bShowDetails, comms, jp.p, jp);
            if (compareTo.Commodities.Count > 1)
            {
                compareTo.TotalCost = compareTo.Commodities.Sum(c => c.TotalCost);
            }
            if (bShowDetails)
            {
                var detailsTo = compareTo.Commodities.SelectMany(c => c.AdditionalDetails).ToList();
                double numeric;
                var detailsToTotal = detailsTo
                    .Where(d => billDetailforTotalCostList.Contains(d.Name) && double.TryParse(d.Value, out numeric))
                    .Sum(d => Convert.ToDouble(d.Value));
                compareTo.TotalCost = compareTo.TotalCost + detailsToTotal;
            }
            compareTo.TotalCostFormatted = compareTo.TotalCost.ToString(cFormatTable);
            bct.CompareTo = compareTo;

            var multiplier = 1;
            if (DateTime.Parse(compareDate) > DateTime.Parse(compareToDate))
            {
                multiplier = -1;
                bct.DifferenceElectricUse =
                    DifferenceCommodityUse(compareFrom.Commodities.Where(x => x.Commodity == ELECTRIC_COMMODITY_KEY),
                        compareTo.Commodities.Where(x => x.Commodity == ELECTRIC_COMMODITY_KEY), ELECTRIC_COMMODITY_KEY,
                        multiplier, jp);
                bct.DifferenceGasUse =
                    DifferenceCommodityUse(compareFrom.Commodities.Where(x => x.Commodity == GAS_COMMODITY_KEY),
                        compareTo.Commodities.Where(x => x.Commodity == GAS_COMMODITY_KEY), GAS_COMMODITY_KEY,
                        multiplier, jp);
                bct.DifferenceWaterUse =
                    DifferenceCommodityUse(compareFrom.Commodities.Where(x => x.Commodity == WATER_COMMODITY_KEY),
                        compareTo.Commodities.Where(x => x.Commodity == WATER_COMMODITY_KEY), WATER_COMMODITY_KEY,
                        multiplier, jp);
                bct.DifferenceElectricCost =
                    DifferenceCommodityCost(compareFrom.Commodities.Where(x => x.Commodity == ELECTRIC_COMMODITY_KEY),
                        compareTo.Commodities.Where(x => x.Commodity == ELECTRIC_COMMODITY_KEY), ELECTRIC_COMMODITY_KEY,
                        multiplier);
                bct.DifferenceGasCost =
                    DifferenceCommodityCost(compareFrom.Commodities.Where(x => x.Commodity == GAS_COMMODITY_KEY),
                        compareTo.Commodities.Where(x => x.Commodity == GAS_COMMODITY_KEY), GAS_COMMODITY_KEY,
                        multiplier);
                bct.DifferenceWaterCost =
                    DifferenceCommodityCost(compareFrom.Commodities.Where(x => x.Commodity == WATER_COMMODITY_KEY),
                        compareTo.Commodities.Where(x => x.Commodity == WATER_COMMODITY_KEY), WATER_COMMODITY_KEY,
                        multiplier);
                bct.DifferenceNumberOfDays = (bct.CompareFrom.NumberOfDays - bct.CompareTo.NumberOfDays) * multiplier;
                bct.DifferenceAvgTemp = (bct.CompareFrom.AvgTemp - bct.CompareTo.AvgTemp) * multiplier;
                bct.DifferenceUtilityCost = (bct.CompareFrom.UtilityCost - bct.CompareTo.UtilityCost) * multiplier;
                bct.DifferenceTotalCost = (bct.CompareFrom.TotalCost - bct.CompareTo.TotalCost) * multiplier;
            }
            else
            {
                bct.DifferenceElectricUse =
                    DifferenceCommodityUse(compareTo.Commodities.Where(x => x.Commodity == ELECTRIC_COMMODITY_KEY),
                        compareFrom.Commodities.Where(x => x.Commodity == ELECTRIC_COMMODITY_KEY), ELECTRIC_COMMODITY_KEY,
                        multiplier, jp);
                bct.DifferenceGasUse =
                    DifferenceCommodityUse(compareTo.Commodities.Where(x => x.Commodity == GAS_COMMODITY_KEY),
                        compareFrom.Commodities.Where(x => x.Commodity == GAS_COMMODITY_KEY), GAS_COMMODITY_KEY,
                        multiplier, jp);
                bct.DifferenceWaterUse =
                    DifferenceCommodityUse(compareTo.Commodities.Where(x => x.Commodity == WATER_COMMODITY_KEY),
                        compareFrom.Commodities.Where(x => x.Commodity == WATER_COMMODITY_KEY), WATER_COMMODITY_KEY,
                        multiplier, jp);
                bct.DifferenceElectricCost =
                    DifferenceCommodityCost(compareTo.Commodities.Where(x => x.Commodity == ELECTRIC_COMMODITY_KEY),
                        compareFrom.Commodities.Where(x => x.Commodity == ELECTRIC_COMMODITY_KEY), ELECTRIC_COMMODITY_KEY,
                        multiplier);
                bct.DifferenceGasCost =
                    DifferenceCommodityCost(compareTo.Commodities.Where(x => x.Commodity == GAS_COMMODITY_KEY),
                        compareFrom.Commodities.Where(x => x.Commodity == GAS_COMMODITY_KEY), GAS_COMMODITY_KEY,
                        multiplier);
                bct.DifferenceWaterCost =
                    DifferenceCommodityCost(compareTo.Commodities.Where(x => x.Commodity == WATER_COMMODITY_KEY),
                        compareFrom.Commodities.Where(x => x.Commodity == WATER_COMMODITY_KEY), WATER_COMMODITY_KEY,
                        multiplier);
                bct.DifferenceNumberOfDays = (bct.CompareTo.NumberOfDays - bct.CompareFrom.NumberOfDays) * multiplier;
                bct.DifferenceAvgTemp = (bct.CompareTo.AvgTemp - bct.CompareFrom.AvgTemp) * multiplier;
                bct.DifferenceUtilityCost = (bct.CompareTo.UtilityCost - bct.CompareFrom.UtilityCost) * multiplier;
                bct.DifferenceTotalCost = (bct.CompareTo.TotalCost - bct.CompareFrom.TotalCost) * multiplier;
            }
            bct.DifferenceUtilityCostFormatted = bct.DifferenceUtilityCost.ToString(cFormatTable, noParens);
            bct.DifferenceTotalCostFormatted = bct.DifferenceTotalCost.ToString(CultureInfo.InvariantCulture) == "0" ? "-" : bct.DifferenceTotalCost.ToString(cFormatTable, noParens);
            bct.DifferenceElectricCostFormatted = bct.DifferenceElectricCost.ToString(cFormatTable, noParens);
            bct.DifferenceGasCostFormatted = bct.DifferenceGasCost.ToString(cFormatTable, noParens);
            bct.DifferenceWaterCostFormatted = bct.DifferenceWaterCost.ToString(cFormatTable, noParens);
            // bill detials
            if (bShowDetails)
                bct.AdditionalDetails = AdditionalDetails(billTextContent, billDetailSettings, compareFrom, compareTo,
                    cFormatTable, noParens, multiplier, tabKey, jp);

            bc.TotalCostFromFormatted = bct.CompareFrom.TotalCost.ToString(cFormatGraph, noParens);
            bc.TotalCostToFormatted = bct.CompareTo.TotalCost.ToString(cFormatGraph, noParens);
            bc.Data = bct;

            //Content
            bc.CompareLabel = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_COMPARISON, "comparelabel");
            bc.DifferenceLabel = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_COMPARISON, "differencelabel");
            bc.GasUsage = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_COMPARISON, "gasusage");
            bc.ElectricUsage = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_COMPARISON, "electricusage");
            bc.WaterUsage = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_COMPARISON, "waterusage");
            bc.BillDays = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_COMPARISON, "billdays");
            bc.AverageTemperature = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_COMPARISON, "averagetemperature");
            bc.AdditionalCosts = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_COMPARISON, "additionalcosts");
            bc.TotalAmountDue = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_COMPARISON, "totalamountdue");
            bc.NoBillsText = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_COMPARISON, "nobills");
            bc.OneBillText = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_COMPARISON, "onebill");
            bc.ChooseDate = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_COMPARISON, "choosedate");
            bc.CompareDatePrompt = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_COMPARISON, "comparedateprompt");
            bc.GasCost = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_COMPARISON, "gascost");
            bc.ElectricCost = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_COMPARISON, "electriccost");
            bc.WaterCost = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_COMPARISON, "watercost");
            //Configuration
            var tempFormat = billConfig.FirstOrDefault(x => x.category == COMMON_CATEGORY && x.key == "format.temperature");
            bc.ShowInsights = CMS.GetWidgetConfig(billConfig, tabKey, WIDGET_TYPE_BILL_COMPARISON, "showinsights");
            bc.ShowTable = CMS.GetWidgetConfig(billConfig, tabKey, WIDGET_TYPE_BILL_COMPARISON, "showtable");
            bc.InsightsCount = CMS.GetWidgetConfig(billConfig, tabKey, WIDGET_TYPE_BILL_COMPARISON, "insightscount");
            bc.TempFormat = (tempFormat == null) ? "" : tempFormat.value;
            return bc;
        }

        public BillToDate GetBillToDate(string tabKey, string parameters, string version = "v1", bool includeBillSummary = true)
        {
            var jp = (JsonParams)JsonConvert.DeserializeObject(QueryStringModule.Decrypt(parameters), typeof(JsonParams));
            //Content
            var content = CMS.GetContent(int.Parse(jp.cl), jp.l).Content;
            var billTextContent =
                content.TextContent.Where(x => x.category == BILLTODATE_CATEGORY || x.category == COMMON_CATEGORY).ToList();
            var billUomContent = content.UOM.ToList();
            var billConfig =
                content.Configuration.Where(x => x.category == BILLTODATE_CATEGORY || x.category == COMMON_CATEGORY).ToList();

            var cFormat = CMS.GetWidgetConfig(billConfig, tabKey, WIDGET_TYPE_BILL_TO_DATE, "currencyformat");
            //Get BillToDate   
            var billToDateResponse = GetBillToDateFromCache(jp, version);
            var ci = new CultureInfo(jp.l);
            var btd = new BillToDate(billConfig, billTextContent, tabKey, WIDGET_TYPE_BILL_TO_DATE, jp);
            if (billToDateResponse.BillToDate?.Services == null || billToDateResponse.BillToDate?.HasError == true || billToDateResponse.BillToDate.Services.Count == 0)
            {
                btd.NoBillToDate = true;
            }
            else
            {
                var ngFormat = CMS.GetCommonContent(CMS.GetContent(int.Parse(jp.cl), jp.l), "format.gasquantity", "config");
                var neFormat = CMS.GetCommonContent(CMS.GetContent(int.Parse(jp.cl), jp.l), "format.electricquantity", "config");
                var nwFormat = CMS.GetCommonContent(CMS.GetContent(int.Parse(jp.cl), jp.l), "format.waterquantity", "config");

                var dFormat = CMS.GetWidgetConfig(billConfig, tabKey, WIDGET_TYPE_BILL_TO_DATE, "dateformat");

                var startDate = Convert.ToDateTime(billToDateResponse.BillToDate.BillStartDate).ToString(dFormat, ci);
                var endDate = Convert.ToDateTime(billToDateResponse.BillToDate.BillEndDate).ToString(dFormat, ci);

                btd.BillPeriod = startDate + "-" + endDate;
                btd.NumberOfDays = billToDateResponse.BillToDate.BillDays;
                btd.DaysLeftInBillPeriod =
                    (Convert.ToDateTime(billToDateResponse.BillToDate.BillEndDate) - DateTime.Now).Days;
                double avgDailyUse = 0;
                var daysIntoCycle = 0;

                var gasBills = billToDateResponse.BillToDate.Services.Where(t => t.CommodityKey == GAS_COMMODITY_KEY).ToList();
                var rg = billToDateResponse.BillToDate.Services.Where(t => t.CommodityKey == GAS_COMMODITY_KEY).ToList();
                if (rg.GroupBy(x => x.RateClass).Count() > 1)
                {
                    btd.RateMismatchGas = true;
                }
                else
                {
                    if (rg.FirstOrDefault() != null)
                    {
                        var billToDateService = rg.FirstOrDefault();
                        if (billToDateService != null) btd.RateClassGas = billToDateService.RateClass;
                    }
                }
                foreach (var b in gasBills)
                {
                    avgDailyUse += (b.UseToDate / b.DaysIntoCycle) * b.DaysIntoCycle;
                    daysIntoCycle += b.DaysIntoCycle;
                }
                if (avgDailyUse > 0 && daysIntoCycle > 0)
                {
                    btd.AverageDailyUseGas = avgDailyUse / daysIntoCycle;
                }
                var totalGasUsed = gasBills.Sum(r => r.UseToDate);
                var totalGasUseProjected = gasBills.Sum(r => r.UseProjected);
                var gUom = string.Empty;
                var gBill = gasBills.FirstOrDefault();
                if (gBill != null)
                {
                    var gUnit = gBill.UomKey;
                    var uombillTexContent = billTextContent
                        .FirstOrDefault(z =>
                        {
                            var orDefault = billUomContent.FirstOrDefault(x => x.key == gUnit);
                            return orDefault != null && z.key == orDefault.namekey;
                        });
                    if (uombillTexContent != null)
                        gUom = uombillTexContent.shorttext;
                }
                btd.GUseToDate = totalGasUsed;
                btd.GasUseToDate = totalGasUsed.ToString(ngFormat, ci);
                btd.GasUseProjected = totalGasUseProjected.ToString(ngFormat, ci);
                btd.GasUOM = gUom;

                var electricBills = billToDateResponse.BillToDate.Services.Where(t => t.CommodityKey == ELECTRIC_COMMODITY_KEY).ToList();
                var re = billToDateResponse.BillToDate.Services.Where(t => t.CommodityKey == ELECTRIC_COMMODITY_KEY).ToList();
                if (re.GroupBy(x => x.RateClass).Count() > 1)
                {
                    btd.RateMismatchElectric = true;
                }
                else
                {
                    if (re.FirstOrDefault() != null)
                    {
                        var billToDateService = re.FirstOrDefault();
                        if (billToDateService != null) btd.RateClassElectric = billToDateService.RateClass;
                    }
                }
                avgDailyUse = 0; daysIntoCycle = 0;
                foreach (var e in electricBills)
                {
                    avgDailyUse += (e.UseToDate / e.DaysIntoCycle) * e.DaysIntoCycle;
                    daysIntoCycle += e.DaysIntoCycle;
                }
                if (avgDailyUse > 0 && daysIntoCycle > 0)
                {
                    btd.AverageDailyUseElectric = avgDailyUse / daysIntoCycle;
                }
                var totalElectricUsed = electricBills.Sum(r => r.UseToDate);
                var totalElectricUseProjected = electricBills.Sum(r => r.UseProjected);
                var eUom = string.Empty;
                var eBill = electricBills.FirstOrDefault();
                if (eBill != null)
                {
                    var eUnit = eBill.UomKey;
                    var uomBillTextContent = billTextContent
                        .FirstOrDefault(z =>
                        {
                            var uomFirstOrDefault = billUomContent.FirstOrDefault(x => x.key == eUnit);
                            return uomFirstOrDefault != null && z.key == uomFirstOrDefault.namekey;
                        });
                    if (uomBillTextContent != null)
                        eUom = uomBillTextContent.shorttext;
                }
                btd.EUseToDate = totalElectricUsed;
                btd.ElectricUseToDate = totalElectricUsed.ToString(neFormat, ci);
                btd.ElectricUseProjected = totalElectricUseProjected.ToString(neFormat, ci);
                btd.ElectricUOM = eUom;

                var waterBills = billToDateResponse.BillToDate.Services.Where(t => t.CommodityKey == WATER_COMMODITY_KEY).ToList();
                var rw = billToDateResponse.BillToDate.Services.Where(t => t.CommodityKey == WATER_COMMODITY_KEY).ToList();
                if (rw.GroupBy(x => x.RateClass).Count() > 1)
                {
                    btd.RateMismatchWater = true;
                }
                else
                {
                    if (rw.FirstOrDefault() != null)
                    {
                        var billToDateService = rw.FirstOrDefault();
                        if (billToDateService != null) btd.RateClassWater = billToDateService.RateClass;
                    }
                }
                avgDailyUse = 0; daysIntoCycle = 0;
                foreach (var w in waterBills)
                {
                    avgDailyUse += (w.UseToDate / w.DaysIntoCycle) * w.DaysIntoCycle;
                    daysIntoCycle += w.DaysIntoCycle;
                }
                if (avgDailyUse > 0 && daysIntoCycle > 0)
                {
                    btd.AverageDailyUseWater = avgDailyUse / daysIntoCycle;
                }
                var totalWaterUsed = waterBills.Sum(r => r.UseToDate);
                var totalWaterUseProjected = waterBills.Sum(r => r.UseProjected);
                var wUom = string.Empty;
                var wBill = waterBills.FirstOrDefault();
                if (wBill != null)
                {
                    var wUnit = wBill.UomKey;
                    var uomBillTextContent = billTextContent
                        .FirstOrDefault(z =>
                        {
                            var uomFirstOrDefault = billUomContent.FirstOrDefault(x => x.key == wUnit);
                            return uomFirstOrDefault != null && z.key == uomFirstOrDefault.namekey;
                        });
                    if (uomBillTextContent != null)
                        wUom = uomBillTextContent.shorttext;
                }
                btd.WUseToDate = totalWaterUsed;
                btd.WaterUseToDate = totalWaterUsed.ToString(nwFormat, ci);
                btd.WaterUseProjected = totalWaterUseProjected.ToString(nwFormat, ci);
                btd.WaterUOM = wUom;

                var sewerBills = billToDateResponse.BillToDate.Services.Where(t => t.CommodityKey == SEWER_COMMODITY_KEY).ToList();
                var rs = billToDateResponse.BillToDate.Services.Where(t => t.CommodityKey == SEWER_COMMODITY_KEY).ToList();
                if (rs.GroupBy(x => x.RateClass).Count() > 1)
                {
                    btd.RateMismatchSewer = true;
                }
                else
                {
                    if (rs.FirstOrDefault() != null)
                    {
                        var billToDateService = rs.FirstOrDefault();
                        if (billToDateService != null) btd.RateClassSewer = billToDateService.RateClass;
                    }
                }
                avgDailyUse = 0; daysIntoCycle = 0;
                foreach (var s in sewerBills)
                {
                    avgDailyUse += (s.UseToDate / s.DaysIntoCycle) * s.DaysIntoCycle;
                    daysIntoCycle += s.DaysIntoCycle;
                }
                if (avgDailyUse > 0 && daysIntoCycle > 0)
                {
                    btd.AverageDailyUseSewer = avgDailyUse / daysIntoCycle;
                }
                var totalSewerUsed = sewerBills.Sum(r => r.UseToDate);
                var totalSewerUseProjected = sewerBills.Sum(r => r.UseProjected);
                var sUom = string.Empty;
                var sBill = sewerBills.FirstOrDefault();
                if (sBill != null)
                {
                    var sUnit = sBill.UomKey;
                    var uomBillTextContent = billTextContent
                        .Where(z =>
                        {
                            var uomfirFirstOrDefault = billUomContent.FirstOrDefault(x => x.key == sUnit);
                            return uomfirFirstOrDefault != null && z.key == uomfirFirstOrDefault.namekey;
                        })
                        .FirstOrDefault();
                    if (uomBillTextContent != null)
                        sUom = uomBillTextContent.shorttext;
                }
                btd.SUseToDate = totalSewerUsed;
                btd.SewerUseToDate = totalSewerUsed.ToString(nwFormat, ci);
                btd.SewerUseProjected = totalSewerUseProjected.ToString(nwFormat, ci);
                btd.SewerUOM = sUom;

                btd.AvgDailyCost = billToDateResponse.BillToDate.AverageDailyCost.ToString(cFormat, ci);
                btd.CostToDate = billToDateResponse.BillToDate.TotalCost.ToString(cFormat, ci);
                btd.ProjectedCost = billToDateResponse.BillToDate.TotalProjectedCost.ToString(cFormat, ci);
            }

            //Content
            // Bill Summary -- Tab
            if (includeBillSummary)
            {
                btd.BillSummary = GetBillSummary(tabKey, parameters);
                btd.BillSummary.AvgDailyCost = Convert.ToDouble(btd.BillSummary.AvgDailyCost).ToString(cFormat, ci);
                btd.BillSummary.AmountDue = Convert.ToDouble(btd.BillSummary.AmountDue).ToString(cFormat, ci);

                if (btd.BillSummary.NoBills)
                {
                    btd.BillSummary.NoBillsText = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_TO_DATE, "nobills");
                }
                else
                {
                    btd.BillSummary.Title = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_TO_DATE, "billtitle");
                    btd.BillSummary.TotalGasUse = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_TO_DATE, "totalgasuse");
                    btd.BillSummary.TotalElectricUse = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_TO_DATE, "totalelectricuse");
                    btd.BillSummary.TotalWaterUse = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_TO_DATE, "totalwateruse");
                    btd.BillSummary.AvgDailyCostText = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_TO_DATE, "avgdailycost");
                    btd.BillSummary.NumberOfDaysText = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_TO_DATE, "numberofdays");
                    btd.BillSummary.AvgTempText = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_TO_DATE, "avgtemp");
                    btd.BillSummary.AmountDueText = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_TO_DATE, "amountdue");
                    btd.BillSummary.ViewBillHistory = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_TO_DATE, "viewbillhistory");
                }
            }
            //Widget 
            btd.BillStatementTitle = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_TO_DATE, "billstatementtitle");
            btd.BTDTitle = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_TO_DATE, "btdtitle");

            // BTD -- Tab
            btd.ProjectedCostText = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_TO_DATE, "btdprojectedcost");
            btd.CostToDateText = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_TO_DATE, "btdcosttodate");
            btd.ElectricUseToDateText = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_TO_DATE, "btdelectricusetodate");
            btd.GasUseToDateText = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_TO_DATE, "btdgasusetodate");
            btd.WaterUseToDateText = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_TO_DATE, "btdwaterusetodate");
            btd.NumberOfDaysText = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_TO_DATE, "btddays");
            btd.AvgDailyCostText = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_TO_DATE, "btdaveragedailycost");
            btd.BTDFooter = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_TO_DATE, "btdfooter");

            //Configuration
            btd.Commodities = CMS.GetWidgetConfig(billConfig, tabKey, WIDGET_TYPE_BILL_TO_DATE, "commodities");
            btd.ShowBTDBillPeriod = CMS.GetWidgetConfig(billConfig, tabKey, WIDGET_TYPE_BILL_TO_DATE, "showbtdbillperiod");
            btd.ShowBTDCostToDate = CMS.GetWidgetConfig(billConfig, tabKey, WIDGET_TYPE_BILL_TO_DATE, "showbtdcosttodate");
            btd.ShowBTDProjectedCost = CMS.GetWidgetConfig(billConfig, tabKey, WIDGET_TYPE_BILL_TO_DATE, "showbtdprojectedcost");
            btd.ShowBTDElectricUseToDate = CMS.GetWidgetConfig(billConfig, tabKey, WIDGET_TYPE_BILL_TO_DATE, "showbtdelectricusetodate");
            btd.ShowBTDGasUseToDate = CMS.GetWidgetConfig(billConfig, tabKey, WIDGET_TYPE_BILL_TO_DATE, "showbtdgasusetodate");
            btd.ShowBTDWaterUseToDate = CMS.GetWidgetConfig(billConfig, tabKey, WIDGET_TYPE_BILL_TO_DATE, "showbtdwaterusetodate");
            btd.ShowBTDAverageDailyCost = CMS.GetWidgetConfig(billConfig, tabKey, WIDGET_TYPE_BILL_TO_DATE, "showbtdaveragedailycost");
            btd.ShowBTDSewerUseToDate = CMS.GetWidgetConfig(billConfig, tabKey, WIDGET_TYPE_BILL_TO_DATE, "showbtdsewerusetodate");

            return btd;
        }

        private static List<BillCompareCommodity> PopulateCommodity(List<Bill> bills, string cFormat,
            List<Common.TextContent> billTextContent, List<Common.UOM> billUomContent, List<BillDetailSetting> billDetailSettings, bool bShowDetails, string[] commodities, string premiseId, JsonParams jp)
        {
            var bccl = new List<BillCompareCommodity>();
            foreach (var c in commodities)
            {
                var billComm = bills.SelectMany(b => b.Premises).Where(p => p.Id == premiseId).SelectMany(x => x.Service).Where(t => t.CommodityKey == c).ToList();

                var billCommFirstOrDefault = billComm.FirstOrDefault();
                if (billCommFirstOrDefault != null)
                {
                    var bcc = new BillCompareCommodity();
                    var comNameKey = "commodity." + c + ".name";
                    bcc.Label = billTextContent.Where(e => e.key == comNameKey).Select(s => s.shorttext).FirstOrDefault();
                    bcc.Commodity = c;
                    var unit = billCommFirstOrDefault.UOMKey;
                    var uomBillTextContent = billTextContent.FirstOrDefault(z =>
                    {
                        var uomFirstOrDefault = billUomContent.FirstOrDefault(x => x.key == unit);
                        return uomFirstOrDefault != null && z.key == uomFirstOrDefault.namekey;
                    });
                    if (uomBillTextContent != null)
                    {
                        var uom = uomBillTextContent.shorttext;
                        bcc.UOM = uom;
                    }
                    bcc.TotalUse = Convert.ToDouble(billComm.Sum(r => r.TotalServiceUse).ToString(GetUsageFormat(c, jp), CultureInfo.CurrentCulture));
                    bcc.TotalCost = billComm.Sum(r => r.CostofUsage);
                    bcc.TotalCostFormatted = bcc.TotalCost.ToString(cFormat);

                    // Commodity Details
                    if (bShowDetails)
                    {
                        var commodityBillDetailSettings = billDetailSettings
                            .Where(b => b.Commodity == c || b.Commodity == "all").ToList();
                        bcc.AdditionalDetails = PopulateCommodityDetail(billComm, commodityBillDetailSettings);
                    }
                    bccl.Add(bcc);
                }
            }
            return bccl;
        }

        private static List<AdditionalCommodityDetail> PopulateCommodityDetail(List<Service> billCommodityServices, List<BillDetailSetting> billDetailSettings)
        {
            var acdl = new List<AdditionalCommodityDetail>();
            foreach (var billDetailSetting in billDetailSettings)
            {
                var costDetails = billCommodityServices.SelectMany(s => s.CostDetails).ToList();

                if (costDetails.Exists(c => c.Name == billDetailSetting.DetailKey))
                {
                    var acd = new AdditionalCommodityDetail
                    {
                        Name = billDetailSetting.DetailKey
                    };
                    var details = costDetails.Where(c => c.Name == billDetailSetting.DetailKey).ToList();
                    if (details.Count == 1)
                        acd.Value = details[0].Value;
                    else
                    {
                        double numeric;
                        acd.Value = details.Where(d => double.TryParse(d.Value, out numeric)).Sum(c => Convert.ToDouble(c.Value)).ToString(CultureInfo.InvariantCulture);
                    }
                    acdl.Add(acd);
                }
            }

            return acdl;
        }

        private static BillCompareTable PopulateCompareAttributes(List<Bill> bills, string cFormat, string premiseId)
        {
            var bct = new BillCompareTable();
            var billAvg = bills.SelectMany(b => b.Premises).Where(p => p.Id == premiseId).SelectMany(x => x.Service).ToList();

            if (billAvg.Count > 1)
            {
                int avgtemp = 0, billdays = 0;

                foreach (var bmy in billAvg)
                {
                    avgtemp += bmy.AverageTemperature * bmy.BillDays;
                    billdays += bmy.BillDays;
                }
                if (avgtemp == 0 && billdays == 0) { bct.AvgTemp = 0; }
                else
                {
                    bct.AvgTemp = (avgtemp / billdays);
                }
                var billAvgFirstOrDefault = billAvg.OrderByDescending(o => o.BillDays).FirstOrDefault();
                if (billAvgFirstOrDefault != null)
                    bct.NumberOfDays = billAvgFirstOrDefault.BillDays;
            }
            else if (billAvg.Count == 1)
            {
                var billAvgFirstOrDefault = billAvg.FirstOrDefault();
                if (billAvgFirstOrDefault != null) bct.AvgTemp = billAvgFirstOrDefault.AverageTemperature;
                var billAvgSorted = billAvg.OrderByDescending(o => o.BillDays).FirstOrDefault();
                if (billAvgSorted != null)
                    bct.NumberOfDays = billAvgSorted.BillDays;
            }
            else
            {
                bct.AvgTemp = 0;
            }

            double addBillCost = 0;
            double totalAmount = 0;
            foreach (var b in bills)
            {
                foreach (var premise in b.Premises)
                {
                    if (premise.Id != premiseId) continue;
                    addBillCost += b.AdditionalBillCost;
                    totalAmount += b.TotalAmount;
                }
            }

            bct.UtilityCost = addBillCost + billAvg.Sum(r => r.AdditionalServiceCost);
            bct.TotalCost = totalAmount;
            bct.TotalCostFormatted = bct.TotalCost.ToString(cFormat);
            bct.UtilityCostFormatted = bct.UtilityCost.ToString(cFormat);
            return bct;
        }

        private static List<BillCompareAdditionalDetail> AdditionalDetails(List<Common.TextContent> billTextContent, List<BillDetailSetting> billDetailSettings, BillCompareTable compareFrom, BillCompareTable compareTo, string cFormat, NumberFormatInfo noParens, int multiplier, string tabKey, JsonParams jp)
        {
            var additionalDetails = new List<BillCompareAdditionalDetail>();
            foreach (var billDetail in billDetailSettings)
            {
                List<AdditionalCommodityDetail> additionalDetailsFrom;
                List<AdditionalCommodityDetail> additionalDetailsTo;

                if (billDetail.Commodity == "all")
                {
                    // get from and to details for ALL commodities
                    additionalDetailsFrom = compareFrom.Commodities.SelectMany(c => c.AdditionalDetails).Where(c => c.Name == billDetail.DetailKey).ToList();
                    additionalDetailsTo = compareTo.Commodities.SelectMany(c => c.AdditionalDetails).Where(c => c.Name == billDetail.DetailKey).ToList();
                }
                else
                {
                    // get from and to details for the requested commodity
                    additionalDetailsFrom = compareFrom.Commodities.Where(c => c.Commodity == billDetail.Commodity).SelectMany(c => c.AdditionalDetails).Where(c => c.Name == billDetail.DetailKey).ToList();
                    additionalDetailsTo = compareTo.Commodities.Where(c => c.Commodity == billDetail.Commodity).SelectMany(c => c.AdditionalDetails).Where(c => c.Name == billDetail.DetailKey).ToList();
                }

                if (additionalDetailsTo.Any() || additionalDetailsFrom.Any())
                {
                    var bcad = new BillCompareAdditionalDetail();
                    string valueFromFormatted, valueToFormatted;
                    decimal numeric, valueFrom = 0m, valueTo = 0m;

                    if (additionalDetailsFrom.Count == 1 && !decimal.TryParse(additionalDetailsFrom[0].Value, out numeric))
                    {
                        valueFromFormatted = additionalDetailsFrom[0].Value;
                    }
                    else
                    {
                        // filter out the non-numeric details
                        var filteredFrom = additionalDetailsFrom.Where(c => decimal.TryParse(c.Value, out numeric)).ToList();

                        if (filteredFrom.Any() && billDetail.Type != "other")
                        {
                            valueFrom = filteredFrom.Sum(d => Convert.ToDecimal(d.Value));
                            if (billDetail.Type == "cost")
                            {
                                valueFromFormatted = valueFrom.ToString(cFormat);
                            }
                            else
                            {
                                valueFromFormatted = valueFrom.ToString(
                                    GetUsageFormat(
                                        billDetail.Commodity == "all" ? ELECTRIC_COMMODITY_KEY : billDetail.Commodity,
                                        jp),
                                    CultureInfo.CurrentCulture);
                            }
                        }
                        else
                            valueFromFormatted = additionalDetailsFrom.Any() ? additionalDetailsFrom.FirstOrDefault()?.Value : "-";

                    }
                    bcad.AdditionalValueFromFormatted = valueFromFormatted;
                    if (additionalDetailsTo.Count == 1 && !decimal.TryParse(additionalDetailsTo[0].Value, out numeric))
                    {
                        valueToFormatted = additionalDetailsTo[0].Value;
                    }
                    else
                    {
                        // filter out the non-numeric details
                        var filteredTo = additionalDetailsTo.Where(c => decimal.TryParse(c.Value, out numeric)).ToList();//.Sum(d => Convert.ToDouble(d.Value));
                        if (filteredTo.Any() && billDetail.Type != "other")
                        {
                            valueTo = filteredTo.Sum(d => Convert.ToDecimal(d.Value));
                            if (billDetail.Type == "cost")
                            {
                                valueToFormatted = valueTo.ToString(cFormat);
                            }
                            else
                            {
                                valueToFormatted = valueTo.ToString(
                                    GetUsageFormat(
                                        billDetail.Commodity == "all" ? ELECTRIC_COMMODITY_KEY : billDetail.Commodity, jp),
                                    CultureInfo.CurrentCulture);
                            }
                        }
                        else
                        {
                            valueToFormatted = additionalDetailsTo.Any() ? additionalDetailsTo.FirstOrDefault()?.Value : "-";
                        }

                    }
                    bcad.AdditionalValueToFormatted = valueToFormatted;

                    // only add row if from/to column contain non-zero/non dash value
                    if (valueFrom != 0m || valueTo != 0m || valueFromFormatted != "-" || valueToFormatted != "-")
                    {
                        if (multiplier == -1)
                            bcad.DifferenceAdditionalValue = Convert.ToDouble(valueFrom - valueTo) * multiplier;
                        else
                            bcad.DifferenceAdditionalValue = Convert.ToDouble(valueTo - valueFrom) * multiplier;

                        if ((valueFrom == 0m && valueFromFormatted != "-") || (valueTo == 0m && valueToFormatted != "-"))
                        {
                            bcad.DifferenceAdditionalValueFormatted = "-";
                        }
                        else
                        {
                            if (billDetail.Type == "cost")
                            {
                                bcad.DifferenceAdditionalValueFormatted =
                                    bcad.DifferenceAdditionalValue.ToString(CultureInfo.InvariantCulture) == "0"
                                        ? "-"
                                        : bcad.DifferenceAdditionalValue.ToString(cFormat, noParens);
                            }
                            else
                            {
                                bcad.DifferenceAdditionalValueFormatted = bcad.DifferenceAdditionalValue.ToString(CultureInfo.InvariantCulture) == "0" ? "-" :
                                    bcad.DifferenceAdditionalValue.ToString(
                                        GetUsageFormat(
                                            billDetail.Commodity == "all" ? ELECTRIC_COMMODITY_KEY : billDetail.Commodity, jp),
                                        CultureInfo.CurrentCulture);
                            }
                        }
                        bcad.Label = CMS.GetWidgetContent(billTextContent, tabKey, WIDGET_TYPE_BILL_COMPARISON, billDetail.TextContentKey);
                        additionalDetails.Add(bcad);
                    }
                }
            }

            return additionalDetails;
        }

        private static string DifferenceCommodityUse(IEnumerable<BillCompareCommodity> compareFromList, IEnumerable<BillCompareCommodity> compareToList, string commodity, int multipier, JsonParams jp)
        {
            double compareFrom = 0;
            double compareTo = 0;
            var uom = string.Empty;
            var cfl = compareFromList.FirstOrDefault(x => x.Commodity == commodity);
            var ctl = compareToList.FirstOrDefault(x => x.Commodity == commodity);
            if (cfl != null)
            {
                compareFrom = cfl.TotalUse;
                uom = cfl.UOM;
            }
            if (ctl != null)
            {
                compareTo = ctl.TotalUse;
                uom = ctl.UOM;
            }
            var calc = (compareFrom - compareTo) * multipier;
            return calc.ToString(GetUsageFormat(commodity, jp), CultureInfo.CurrentCulture) + " " + uom;
        }

        private static double DifferenceCommodityCost(IEnumerable<BillCompareCommodity> compareFromList, IEnumerable<BillCompareCommodity> compareToList, string commodity, int multipier)
        {
            double compareFrom = 0;
            double compareTo = 0;
            var cfl = compareFromList.FirstOrDefault(x => x.Commodity == commodity);
            var ctl = compareToList.FirstOrDefault(x => x.Commodity == commodity);
            if (cfl != null)
            {
                compareFrom = cfl.TotalCost;
            }
            if (ctl != null)
            {
                compareTo = ctl.TotalCost;
            }
            return (compareFrom - compareTo) * multipier;
        }

        private static string GetUsageFormat(string commodity, JsonParams jp)
        {
            switch (commodity)
            {
                case ELECTRIC_COMMODITY_KEY:
                    return CMS.GetCommonContent(CMS.GetContent(int.Parse(jp.cl), jp.l), "format.electricquantity", "config");

                case GAS_COMMODITY_KEY:
                    return CMS.GetCommonContent(CMS.GetContent(int.Parse(jp.cl), jp.l), "format.gasquantity", "config");

                case WATER_COMMODITY_KEY:
                    return CMS.GetCommonContent(CMS.GetContent(int.Parse(jp.cl), jp.l), "format.waterquantity", "config");
            }
            return "";
        }

        private static BillRoot GetBillsFromCache(JsonParams jp, string version)
        {
            BillRoot billResponse;
            if (HttpRuntime.Cache["BillResponse" + jp.cl + "" + jp.c + "" + jp.a + "" + jp.p + "" + jp.l + "" + jp.w] == null)
            {
                var rm = new APIResponseModels();
                var edate = DateTime.UtcNow.ToString("yyyy-MM-dd");
                var requestParams = "CustomerId=" + jp.c + "&AccountId=" + jp.a + "&StartDate=2000-01-01&EndDate=" + edate + "&Count=24&IncludeContent=false";
                billResponse = (BillRoot)JsonConvert.DeserializeObject(rm.GetBill(requestParams, jp.l, int.Parse(jp.cl), version).Content, typeof(BillRoot));
                HttpRuntime.Cache.Insert("BillResponse" + jp.cl + "" + jp.c + "" + jp.a + "" + jp.p + "" + jp.l + "" + jp.w, billResponse, null, DateTime.Now.AddMinutes(Constants.kCache_Expiry), Cache.NoSlidingExpiration,
                    CacheItemPriority.NotRemovable, null);
            }
            else
            {
                billResponse = (BillRoot)HttpRuntime.Cache["BillResponse" + jp.cl + "" + jp.c + "" + jp.a + "" + jp.p + "" + jp.l + "" + jp.w];
            }
            return billResponse;
        }

        private static BillToDateRoot GetBillToDateFromCache(JsonParams jp, string version)
        {
            BillToDateRoot billToDateResponse;
            if (HttpRuntime.Cache["BillToDateResponse" + jp.cl + "" + jp.c + "" + jp.a + "" + jp.p + "" + jp.l + "" + jp.w] == null)
            {
                var rm = new APIResponseModels();
                var requestParams = "CustomerId=" + jp.c + "&AccountId=" + jp.a + "&PremiseId=" + jp.p + "&IncludeContent=false";
                billToDateResponse = (BillToDateRoot)JsonConvert.DeserializeObject(rm.GetBillToDate(requestParams, jp.l, int.Parse(jp.cl), version).Content, typeof(BillToDateRoot));
                HttpRuntime.Cache.Insert("BillToDateResponse" + jp.cl + "" + jp.c + "" + jp.a + "" + jp.p + "" + jp.l + "" + jp.w, billToDateResponse, null, DateTime.Now.AddMinutes(Constants.kCache_Expiry), Cache.NoSlidingExpiration,
                    CacheItemPriority.NotRemovable, null);
            }
            else
            {
                billToDateResponse = (BillToDateRoot)HttpRuntime.Cache["BillToDateResponse" + jp.cl + "" + jp.c + "" + jp.a + "" + jp.p + "" + jp.l + "" + jp.w];
            }
            return billToDateResponse;
        }
    }

    public class BillSummary : ContentBase
    {
        public string TotalGasUse { get; set; }
        public string TotalElectricUse { get; set; }
        public string TotalWaterUse { get; set; }
        public string AvgDailyCostText { get; set; }
        public string NumberOfDaysText { get; set; }
        public string AvgTempText { get; set; }
        public string AmountDueText { get; set; }
        public string ViewBillHistory { get; set; }
        public string ShowBillHistoryLink { get; set; }
        public string Commodities { get; set; }
        public string BillHistoryLink { get; set; }
        public string CurrencyFormat { get; set; }
        public string NoBillsText { get; set; }
        public double TotalGasUsed { get; set; }
        public double TotalElectricityUsed { get; set; }
        public double TotalWaterUsed { get; set; }
        public string GasUOM { get; set; }
        public string ElectricUOM { get; set; }
        public string WaterUOM { get; set; }
        public string AvgDailyCost { get; set; }
        public double NumberOfDays { get; set; }
        public int AvgTemp { get; set; }
        public string AmountDue { get; set; }
        public string BillStartDateGas { get; set; }
        public string BillEndDateGas { get; set; }
        public string BillStartDateElectric { get; set; }
        public string BillEndDateElectric { get; set; }
        public string BillStartDateWater { get; set; }
        public string BillEndDateWater { get; set; }
        public bool NoBills { get; set; }
        public string DueDate { get; set; }
        public string TempFormat { get; set; }
        public string RateClassGas { get; set; }
        public string RateClassElectric { get; set; }
        public string RateClassWater { get; set; }
        public string RateClassSewer { get; set; }
        public bool RateMismatchGas { get; set; }
        public bool RateMismatchElectric { get; set; }
        public bool RateMismatchWater { get; set; }
        public bool RateMismatchSewer { get; set; }
        public BillSummary()
        { }
        public BillSummary(List<Common.Configuration> config, List<Common.TextContent> content, string tabKey, string widetType, JsonParams jp) : base(config, content, tabKey, widetType, jp)
        { }
    }

    public class BillHistory : ContentBase
    {
        public string BillDate { get; set; }
        public string ElectricAmountDue { get; set; }
        public string WaterAmountDue { get; set; }
        public string GasAmountDue { get; set; }
        public string TotalAmountDue { get; set; }
        public string Actions { get; set; }
        public string CompareLink { get; set; }
        public string NoBillsText { get; set; }
        public string Commodities { get; set; }
        public string ShowActions { get; set; }
        public string BillCompareLink { get; set; }
        public bool NoBills { get; set; }
        public int ElectricServiceCount { get; set; }
        public int WaterServiceCount { get; set; }
        public int GasServiceCount { get; set; }
        public string RowCount { get; set; }
        public List<BillHistoryList> Bills { get; set; }
        public BillHistory(List<Common.Configuration> config, List<Common.TextContent> content, string tabKey, string widetType, JsonParams jp) : base(config, content, tabKey, widetType, jp)
        { }
    }

    public class BillHistoryList
    {
        public string BillDate { get; set; }
        public string ElectricCost { get; set; }
        public string WaterCost { get; set; }
        public string GasCost { get; set; }
        public string TotalCost { get; set; }
        public string CompareLink { get; set; }
    }

    public class BillUsage : ContentBase
    {
        public string Subtitle { get; set; }
        public string CompareTo { get; set; }
        public string AvgTemp { get; set; }
        public string CurrentYearLegend { get; set; }
        public string PrevYearLegend { get; set; }
        public string NoBillsText { get; set; }
        public string Commodities { get; set; }
        public string CommoditiesText { get; set; }
        public string ShowCommodityLabels { get; set; }
        public string ChartType { get; set; }
        public string ChartLineColor1 { get; set; }
        public string ChartLineColor2 { get; set; }
        public string ChartLineColor3 { get; set; }
        public string ChartAnchorType { get; set; }
        public string LineThickness { get; set; }
        public string WeatherLabel { get; set; }
        public string BillDate { get; set; }
        public string EnableWeather { get; set; }
        public bool ShowWeatherByDefault { get; set; }
        public bool NoBills { get; set; }
        public string LastYear { get; set; }
        public string PreviousYear { get; set; }
        public int ElectricServiceCount { get; set; }
        public int WaterServiceCount { get; set; }
        public int GasServiceCount { get; set; }
        public List<BillUsageByMonth> BilledUsageLastYear { get; set; }
        public List<BillUsageByMonth> BilledUsagePreviousYear { get; set; }
        public BillUsage(List<Common.Configuration> config, List<Common.TextContent> content, string tabKey, string widetType, JsonParams jp) : base(config, content, tabKey, widetType, jp)
        { }
    }

    public class BillUsageByMonth
    {
        public string XAxis { get; set; }
        public double YAxis { get; set; }
        public string UOM { get; set; }
        public string Commodity { get; set; }
        public string Cost { get; set; }
        public Tooltip Tip { get; set; }
    }

    public class Tooltip
    {
        public List<ServiceTooltip> Services { get; set; }
        public int AverageTemperature { get; set; }
    }

    public class ServiceTooltip
    {
        public string XAxis { get; set; }
        public string ServiceAndCost { get; set; }
    }

    public class BillComparison : ContentBase
    {
        public string CompareLabel { get; set; }
        public string DifferenceLabel { get; set; }
        public string GasUsage { get; set; }
        public string ElectricUsage { get; set; }
        public string WaterUsage { get; set; }
        public string GasCost { get; set; }
        public string ElectricCost { get; set; }
        public string WaterCost { get; set; }
        public string BillDays { get; set; }
        public string AverageTemperature { get; set; }
        public string AdditionalCosts { get; set; }
        public string TotalAmountDue { get; set; }
        public bool NoBills { get; set; }
        public bool OneBill { get; set; }
        public string NoBillsText { get; set; }
        public string OneBillText { get; set; }
        public string Commodities { get; set; }
        public string ShowInsights { get; set; }
        public string ShowTable { get; set; }
        public string InsightsCount { get; set; }
        public string TempFormat { get; set; }
        public int ElectricServiceCount { get; set; }
        public int WaterServiceCount { get; set; }
        public int GasServiceCount { get; set; }
        public string TotalCostFromFormatted { get; set; }
        public string TotalCostToFormatted { get; set; }
        public string ChooseDate { get; set; }
        public string CompareDatePrompt { get; set; }
        public string CompareDropDownKeys { get; set; }
        public string CompareDropDownValues { get; set; }
        public Dictionary<string, string> Dates { get; set; }
        public BillCompareType Data { get; set; }
        public BillComparison(List<Common.Configuration> config, List<Common.TextContent> content, string tabKey, string widetType, JsonParams jp) : base(config, content, tabKey, widetType, jp)
        { }
    }

    public class BillCompareType
    {
        public string DifferenceGasUse { get; set; }
        public string DifferenceElectricUse { get; set; }
        public string DifferenceWaterUse { get; set; }
        public double DifferenceNumberOfDays { get; set; }
        public int DifferenceAvgTemp { get; set; }
        public double DifferenceUtilityCost { get; set; }
        public double DifferenceTotalCost { get; set; }
        public string DifferenceUtilityCostFormatted { get; set; }
        public string DifferenceTotalCostFormatted { get; set; }
        public double DifferenceElectricCost { get; set; }
        public string DifferenceElectricCostFormatted { get; set; }
        public double DifferenceGasCost { get; set; }
        public string DifferenceGasCostFormatted { get; set; }
        public double DifferenceWaterCost { get; set; }
        public string DifferenceWaterCostFormatted { get; set; }
        public BillCompareTable CompareFrom { get; set; }
        public BillCompareTable CompareTo { get; set; }
        public List<BillCompareAdditionalDetail> AdditionalDetails { get; set; }
    }

    public class BillCompareAdditionalDetail
    {
        public string Label { get; set; }
        public double DifferenceAdditionalValue { get; set; }
        public string DifferenceAdditionalValueFormatted { get; set; }
        public string AdditionalValueFromFormatted { get; set; }
        public string AdditionalValueToFormatted { get; set; }
    }

    public class BillCompareTable
    {
        public string CompareDate { get; set; }
        public string CompareDateFormatted { get; set; }
        public double NumberOfDays { get; set; }
        public int AvgTemp { get; set; }
        public double UtilityCost { get; set; }
        public double TotalCost { get; set; }
        public string UtilityCostFormatted { get; set; }
        public string TotalCostFormatted { get; set; }
        public List<BillCompareCommodity> Commodities { get; set; }
    }

    public class BillCompareCommodity
    {
        public string Label { get; set; }
        public double TotalUse { get; set; }
        public string UOM { get; set; }
        public string Commodity { get; set; }
        public double TotalCost { get; set; }
        public string TotalCostFormatted { get; set; }
        public List<AdditionalCommodityDetail> AdditionalDetails { get; set; }
    }

    public class AdditionalCommodityDetail
    {
        public string Name { get; set; }
        public string Value { get; set; }

    }

    public class BillDetailSettings
    {
        public List<BillDetailSetting> BillDetails { get; set; }
    }

    public class BillDetailSetting
    {
        public string DetailKey { get; set; }
        public string Commodity { get; set; }
        public int Order { get; set; }
        public string Type { get; set; }
        public string TextContentKey { get; set; }
    }

    public class BillToDate : ContentBase
    {
        public string BillPeriod { get; set; }
        public string ElectricUseToDateText { get; set; }
        public string GasUseToDateText { get; set; }
        public string WaterUseToDateText { get; set; }
        public string AvgDailyCostText { get; set; }
        public string NumberOfDaysText { get; set; }
        public string ProjectedCostText { get; set; }
        public string CostToDateText { get; set; }
        public string ElectricUseToDate { get; set; }
        public string GasUseToDate { get; set; }
        public string WaterUseToDate { get; set; }
        public string SewerUseToDate { get; set; }
        public string ElectricUseProjected { get; set; }
        public string GasUseProjected { get; set; }
        public string WaterUseProjected { get; set; }
        public string SewerUseProjected { get; set; }
        public string GasUOM { get; set; }
        public string ElectricUOM { get; set; }
        public string WaterUOM { get; set; }
        public string SewerUOM { get; set; }
        public string AvgDailyCost { get; set; }
        public double NumberOfDays { get; set; }
        public int DaysLeftInBillPeriod { get; set; }
        public int AvgTemp { get; set; }
        public double AverageDailyUseGas { get; set; }
        public double AverageDailyUseElectric { get; set; }
        public double AverageDailyUseWater { get; set; }
        public double AverageDailyUseSewer { get; set; }
        public bool RateMismatchGas { get; set; }
        public bool RateMismatchElectric { get; set; }
        public bool RateMismatchWater { get; set; }
        public bool RateMismatchSewer { get; set; }
        public string RateClassGas { get; set; }
        public string RateClassElectric { get; set; }
        public string RateClassWater { get; set; }
        public string RateClassSewer { get; set; }
        public string CostToDate { get; set; }
        public string ProjectedCost { get; set; }
        public string BillStatementTitle { get; set; }
        public string BTDTitle { get; set; }
        public bool NoBillToDate { get; set; }
        public string Commodities { get; set; }
        public string BTDFooter { get; set; }
        public string ShowBTDBillPeriod { get; set; }
        public string ShowBTDCostToDate { get; set; }
        public string ShowBTDProjectedCost { get; set; }
        public string ShowBTDElectricUseToDate { get; set; }
        public string ShowBTDGasUseToDate { get; set; }
        public string ShowBTDWaterUseToDate { get; set; }
        public string ShowBTDAverageDailyCost { get; set; }
        public string ShowBTDSewerUseToDate { get; set; }
        public double GUseToDate { get; set; }
        public double EUseToDate { get; set; }
        public double WUseToDate { get; set; }
        public double SUseToDate { get; set; }
        public BillSummary BillSummary { get; set; }
        public BillToDate()
        { }
        public BillToDate(List<Common.Configuration> config, List<Common.TextContent> content, string tabKey, string widetType, JsonParams jp) : base(config, content, tabKey, widetType, jp)
        { }
    }
}

