﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Caching;
using AutoMapper;
using CE.InsightsWeb.Common;
using CE.InsightsWeb.Common.AclaraJson;
using CE.InsightsWeb.Common.QueryStringEncryptor;
using CE.InsightsWeb.Helpers;
using Newtonsoft.Json;

namespace CE.InsightsWeb.Models
{
    public class InsightsModels
    {
        private const string ActionCategory = "action";
        private const string InsightCategory = "insights";
        private const string CommonCategory = "common";
        private const string WidgetType = "insights";
        private const string BillHighlight = "billhighlight";
        private const string AmiHighlight = "amihighlight";

        public Insights GetInsights(string tabKey, string parameters)
        {
            var jp = (JsonParams)JsonConvert.DeserializeObject(QueryStringModule.Decrypt(parameters), typeof(JsonParams));
            //Content
            var content = CMS.GetContent(int.Parse(jp.cl), jp.l).Content;

            var insightsTextContent = content.TextContent.Where(x => x.category == ActionCategory || x.category == InsightCategory || x.category == CommonCategory).ToList();
            var actionContent = content.Action.Where(x => x.type == InsightCategory || x.type == BillHighlight || x.type == AmiHighlight).ToList();
            var actionFileContent = content.FileContent.Where(x => x.category == ActionCategory).ToList();
            var insightsConfig = content.Configuration.Where(x => x.category == InsightCategory).ToList();

            //Get Insights 
            var com = CMS.GetWidgetConfig(insightsConfig, tabKey, WidgetType, "commodities");
            var insightsResponse = GetInsightsFromCache(jp, com, CMS.GetWidgetConfig(insightsConfig, tabKey, WidgetType, "types"));
            var i = new Insights(insightsConfig, insightsTextContent, tabKey, WidgetType, jp);

            if (insightsResponse.Customer == null)
            {
                i.NoInsights = true;
                i.NoInsightsText = CMS.GetWidgetContent(insightsTextContent, tabKey, WidgetType, "noinsightsmessage");
                return i;
            }

            var am = new ActionMerge
            {
                Actions = insightsResponse.Customer.Accounts[0].Premises[0].Actions,
                BillHighlights = insightsResponse.Customer.Accounts[0].Premises[0].BillHighlights,
                AmiHighlights = insightsResponse.Customer.Accounts[0].Premises[0].AmiHighlights,
                ActionContent = actionContent,
                Config = insightsConfig,
                TextContent = insightsTextContent.Where(x => x.category == ActionCategory || x.category == CommonCategory).ToList(),
                FileContent = actionFileContent,
                QueryString = jp,
                CurrencyFormat = CMS.GetWidgetConfig(insightsConfig, tabKey, WidgetType, "currencyformat"),
                PercentFormat = CMS.GetWidgetConfig(insightsConfig, tabKey, WidgetType, "percentageformat"),
                TabKey = tabKey,
                WidgetType = WidgetType
            };

            //Content
            i.NoInsightsText = CMS.GetWidgetContent(insightsTextContent, tabKey, WidgetType, "noinsightsmessage");
            i.ExpandLabel = CMS.GetCommonContent(content, "widget.expandlabel");
            i.CollapseLabel = CMS.GetCommonContent(content, "widget.collapselabel");
            
            //Configuration  
            i.Commodities = com;
            i.ShowRecentTitle = CMS.GetWidgetConfig(insightsConfig, tabKey, WidgetType, "showrecenttitle");
            i.ShowExpandCollapse = CMS.GetWidgetConfig(insightsConfig, tabKey, WidgetType, "showexpandcollapse");
            i.RowCount = CMS.GetWidgetConfig(insightsConfig, tabKey, WidgetType, "rowcount");
            i.InsightsToShow = CMS.GetWidgetConfig(insightsConfig, tabKey, WidgetType, "insightstoshow");
            i.HideImage = CMS.GetWidgetConfig(insightsConfig, tabKey, WidgetType, "hideimage");

            if (i.InsightsToShow.Trim().Length > 0)
            {
                var insightsToShow = i.InsightsToShow.Split(',');
                am.Actions = am.Actions.Where(a => insightsToShow.Contains(a.Key)).ToList();
                am.BillHighlights = am.BillHighlights.Where(a => insightsToShow.Contains(a.ActionKey)).ToList();
                am.AmiHighlights = am.AmiHighlights.Where(a => insightsToShow.Contains(a.ActionKey)).ToList();
            }
          
            var il = new List<Insight>();
            Mapper.CreateMap<ActionMerge, List<Insight>>().ConvertUsing(new InsightConverter());
            i.InsightList = Mapper.Map(am, il);

            i.MaxInsightsToShow = CMS.GetWidgetConfig(insightsConfig, tabKey, WidgetType, "maxinsightstoshow");
            i.MaxInsightsToShow = Convert.ToString(Math.Min(Convert.ToInt16(i.MaxInsightsToShow == "" ? "0" : i.MaxInsightsToShow), i.InsightList.Count));
            i.RowCount = CMS.GetWidgetConfig(insightsConfig, tabKey, WidgetType, "rowcount");
            i.RowCount = Convert.ToString(Math.Min(Convert.ToInt16(i.RowCount), i.InsightList.Count));
            i.RecentTitle = CMS.GetWidgetContent(insightsTextContent, tabKey, WidgetType, "recenttitle");
            i.RecentTitle = i.RecentTitle.Replace("{%rowcount%}", i.RowCount);

            return i;
        }

        private static ActionRoot GetInsightsFromCache(JsonParams jp, string com, string types)
        {
            ActionRoot insightsResponse;
            if (HttpRuntime.Cache["InsightsResponse" + types + "" + jp.cl + "" + jp.c + "" + jp.a + "" + jp.p + "" + jp.s + "" + jp.l + "" + jp.w] == null)
            {
                var rm = new APIResponseModels();
                var requestParams = "CustomerId=" + jp.c + "&AccountId=" + jp.a + "&PremiseId=" + jp.p + "&CommodityKeys=" + com + "&Types=" + types;
                insightsResponse = (ActionRoot)JsonConvert.DeserializeObject(rm.GetAction(requestParams, jp.l, int.Parse(jp.cl)).Content, typeof(ActionRoot));
                HttpRuntime.Cache.Insert("InsightsResponse" + types + "" + jp.cl + "" + jp.c + "" + jp.a + "" + jp.p + "" + jp.s + "" + jp.l + "" + jp.w, insightsResponse,
                    null, DateTime.Now.AddMinutes(Constants.kCache_Expiry), Cache.NoSlidingExpiration, CacheItemPriority.NotRemovable, null);
            }
            else
            {
                insightsResponse = (ActionRoot)HttpRuntime.Cache["InsightsResponse" + types + "" + jp.cl + "" + jp.c + "" + jp.a + "" + jp.p + "" + jp.s + "" + jp.l + "" + jp.w];
            }

            return insightsResponse;
        }
    }

    public class InsightConverter : ITypeConverter<ActionMerge, List<Insight>>
    {
        private const string ReplacementCommodity = "{%commodity%}";
        private const string ReplacementCostImpact = "{%costimpact%}";
        private const string ReplacementPercentageImpact = "{%percentageimpact%}";
        private const string ReplacementDateTime = "{%datetime%}";

        public List<Insight> Convert(ResolutionContext context)
        {
            var sourceCollection = (ActionMerge)context.SourceValue;

            Mapper.CreateMap<ActionMerge, List<Insight>>();
            Mapper.CreateMap<Common.AclaraJson.Action, Insight>();
            Mapper.CreateMap<BillHighlight, Insight>()
                .ForMember(dest => dest.Key, opt => opt.MapFrom(src => src.ActionKey));
            Mapper.CreateMap<AmiHighlight, Insight>()
                .ForMember(dest => dest.Key, opt => opt.MapFrom(src => src.ActionKey));

            var al = new List<Insight>();
            foreach (var a in sourceCollection.Actions)
            {
                if (a == null) continue;
                var i = EntityMapper.Map<Insight>(a);
                var act = sourceCollection.ActionContent.Where(p => p.key == a.Key).FirstOrDefault();
                if (act == null) continue;
                i.ActionPriority = act.actionpriority;
                sourceCollection.ImageSize = Enums.ImageSize.Small;
                SetupInsight(sourceCollection, act, i, sourceCollection.QueryString);
                al.Add(i);
            }

            var noParens = (NumberFormatInfo)CultureInfo.CurrentCulture.NumberFormat.Clone();
            noParens.CurrencyNegativePattern = 1;
            noParens.PercentPositivePattern = 1;

            if (sourceCollection.BillHighlights != null)
            {
                foreach (var a in sourceCollection.BillHighlights)
                {
                    if (a == null) continue;
                    var minBillCostImpact = double.Parse(CMS.GetWidgetConfig(sourceCollection.Config, sourceCollection.TabKey,sourceCollection.WidgetType, "minbillcostimpact"));
                    var minBillPercentImpact = double.Parse(CMS.GetWidgetConfig(sourceCollection.Config, sourceCollection.TabKey,sourceCollection.WidgetType, "minbillpercentimpact"));

                    if (!(Math.Abs(a.CostImpact) > minBillCostImpact) || !(Math.Abs(a.PercentageImpact*100) > minBillPercentImpact)) continue;
                    var i = EntityMapper.Map<Insight>(a);
                    var act = sourceCollection.ActionContent.Where(p => p.key == a.ActionKey).FirstOrDefault();
                    if (act == null) continue;
                    i.ActionPriority = act.actionpriority;
                    sourceCollection.ImageSize = Enums.ImageSize.Small;
                    SetupInsight(sourceCollection, act, i, sourceCollection.QueryString);

                    var name = CMS.GetCommoditiesName(sourceCollection.TextContent, a.Commoditykey);
                    var costImpact = a.CostImpact.ToString(sourceCollection.CurrencyFormat, noParens);
                    var percentImpact = a.PercentageImpact.ToString(sourceCollection.PercentFormat, noParens);
                    i.Title = i.Title.Replace(ReplacementCommodity, name).Replace(ReplacementCostImpact, costImpact).Replace(ReplacementPercentageImpact, percentImpact);
                    al.Add(i);
                }
            }

            if (sourceCollection.AmiHighlights == null) return al.OrderBy(x => x.ActionPriority).ToList();
            {
                foreach (var a in sourceCollection.AmiHighlights)
                {
                    if (a == null) continue;
                    var minAmiCostImpact = double.Parse(CMS.GetWidgetConfig(sourceCollection.Config, sourceCollection.TabKey,sourceCollection.WidgetType, "minamicostimpact"));
                    var minAmiPercentImpact = double.Parse(CMS.GetWidgetConfig(sourceCollection.Config, sourceCollection.TabKey,sourceCollection.WidgetType, "minamipercentimpact"));

                    var dFormat = string.Empty;
                    var byPassChecks = CMS.GetWidgetConfig(sourceCollection.Config, sourceCollection.TabKey, sourceCollection.WidgetType, "excludehighlightsfromimpactcheck");
                   
                    var found = byPassChecks.Split(',').ToList().Where(x=>x.Contains(a.ActionKey)).FirstOrDefault();
                    if (found == null)
                    {
                        if (!(Math.Abs(a.CostImpact) > minAmiCostImpact) || !(Math.Abs(a.PercentageImpact*100) > minAmiPercentImpact)) continue;
                    }
                    else
                    {
                        if (!(Math.Abs(a.PercentageImpact * 100) > minAmiPercentImpact)) continue;
                        dFormat = CMS.GetWidgetConfig(sourceCollection.Config, sourceCollection.TabKey, sourceCollection.WidgetType, "dateformat");
                    }
                    var i = EntityMapper.Map<Insight>(a);
                    var act = sourceCollection.ActionContent.Where(p => p.key == a.ActionKey).FirstOrDefault();
                    if (act == null) continue;
                    i.ActionPriority = act.actionpriority;
                    sourceCollection.ImageSize = Enums.ImageSize.Small;
                    SetupInsight(sourceCollection, act, i, sourceCollection.QueryString);

                    var name = CMS.GetCommoditiesName(sourceCollection.TextContent, a.Commoditykey);
                    var costImpact = a.CostImpact.ToString(sourceCollection.CurrencyFormat, noParens);
                    var percentImpact = a.PercentageImpact.ToString(sourceCollection.PercentFormat, noParens);
                    var dateTime = a.TimeStamp.ToString(dFormat);
                    
                    i.Title = i.Title.Replace(ReplacementCommodity, name).Replace(ReplacementCostImpact, costImpact).Replace(ReplacementPercentageImpact, percentImpact).Replace(ReplacementDateTime, dateTime);
                    al.Add(i);
                }
            }

            return al.OrderBy(x => x.ActionPriority).ToList();
        }

        private static void SetupInsight(ActionMerge sourceCollection, Common.Action act, Insight i, JsonParams jp)
        {
            if (!string.IsNullOrEmpty(act.imagekey))
            {
                var imgContent = sourceCollection.FileContent.Where(x => x.key == act.imagekey).FirstOrDefault();
                switch (sourceCollection.ImageSize)
                {
                    case Enums.ImageSize.Small:
                        if (imgContent != null)
                        {
                            i.ImageUrl = imgContent.smallfile;
                            i.ImageTitle = imgContent.smallfiletitle;
                        }
                        break;
                    case Enums.ImageSize.Medium:
                        if (imgContent != null)
                        {
                            i.ImageUrl = imgContent.mediumfile;
                            i.ImageTitle = imgContent.mediumfiletitle;
                        }
                        break;
                    case Enums.ImageSize.Large:
                        if (imgContent != null)
                        {
                            i.ImageUrl = imgContent.largefile;
                            i.ImageTitle = imgContent.largefiletitle;
                        }
                        break;
                }
            }
            if (!string.IsNullOrEmpty(act.iconclass))
            {
                i.Icon = act.iconclass;
            }

            i.Title = act.namekey == "common.undefined" ? "" : sourceCollection.TextContent.Where(p => p.key == act.namekey).FirstOrDefault().shorttext;
            i.Title = CMS.ReplaceGlobalVariable(i.Title, jp);

            var desc = sourceCollection.TextContent.Where(p => p.key == act.descriptionkey).FirstOrDefault();
            i.Description = desc == null ? "" : desc.shorttext;

            var lnkText = sourceCollection.TextContent.Where(p => p.key == act.nextsteplinktext).FirstOrDefault();
            i.LinkText = lnkText == null ? "" : lnkText.shorttext;

            i.LinkType = act.nextsteplinktype;
            i.Link = GeneralHelper.CreateLink(act.nextsteplinktype, jp, act.nextsteplink);
        }
    }
    public class Insights : ContentBase
    {      
        public string RecentTitle { get; set; }
        public string NoInsightsText { get; set; }
        public string Commodities { get; set; }      
        public string ShowRecentTitle { get; set; }
        public bool NoInsights { get; set; }
        public string ShowExpandCollapse { get; set; }
        public string ExpandLabel { get; set; }
        public string CollapseLabel { get; set; }
        public string RowCount { get; set; }
        public string MaxInsightsToShow { get; set; }
        public string InsightsToShow { get; set; }       
        public string HideImage { get; set; }
        public List<Insight> InsightList { get; set; }
        public Insights(List<Common.Configuration> config, List<Common.TextContent> content, string tabKey, string widetType, JsonParams jp) : base(config, content, tabKey, widetType, jp)
        { }
    }

    public class Insight
    {
        public string Key { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public string ImageTitle { get; set; }
        public string Icon { get; set; }
        public string Link { get; set; }
        public string LinkText { get; set; }
        public string LinkType { get; set; }
        public double ActionPriority { get; set; }
    }
}

