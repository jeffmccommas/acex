﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using CE.InsightsWeb.Common;
using CE.InsightsWeb.Common.AclaraJson;
using CE.InsightsWeb.Common.QueryStringEncryptor;
using Newtonsoft.Json;

namespace CE.InsightsWeb.Models
{
    public class EventTrackingModels
    {
        private const string AppSetting_CEEnvironment = "CEEnvironment";
        private const string kLink = "Link";
        private const string kPage = "Page?enc=";
        public string PostEvents(Dictionary<string, string> eventInfo, string parameters, string eventType)
        {
            var jp = (JsonParams)JsonConvert.DeserializeObject(QueryStringModule.Decrypt(parameters), typeof(JsonParams));
            if (eventInfo.ContainsKey(kLink) && eventInfo[kLink].Contains(kPage))
            {
                  eventInfo[kLink] = kPage + QueryStringModule.Decrypt(eventInfo[kLink].Substring(9));
            }
            var rm = new APIResponseModels();
            eventInfo.Add("EventType", eventType);
            eventInfo.Add("PremiseId", jp.p);
            eventInfo.Add("ClientId", jp.cl);
            eventInfo.Add("CustomerId", jp.c);
            eventInfo.Add("AccountId", jp.a);
            if (ViewPageExtensions.ValidateCsr(jp.u, jp.g, jp.r, jp.w))
            {
                eventInfo.Add("UserId", jp.u);
                eventInfo.Add("GroupName", jp.g);
                eventInfo.Add("RoleName", jp.r);
            }
            eventInfo.Add("PartitionKey", jp.cl + "_" + eventType + "_" + DateTime.UtcNow.Date.ToString("MM/dd/yyyy").Replace("/", "-"));
            eventInfo.Add("RowKey", jp.c + "_" + jp.a + "_" + jp.p + "_" + DateTime.UtcNow.Ticks);
            eventInfo.Add("DateCreated", DateTime.UtcNow.Date.ToString("MM/dd/yyyy"));
            eventInfo.Add("CEEnvironment", ConfigurationManager.AppSettings.Get(AppSetting_CEEnvironment));
            eventInfo.Add("ProfileAttributes", JsonConvert.SerializeObject(((ProfileRoot)HttpRuntime.Cache["ProfileAttributes" + jp.cl + "" + jp.c + "" + jp.a + "" + jp.p + "" + jp.l + "" + jp.w])));
            return JsonConvert.SerializeObject(rm.PostEventsAsync(JsonConvert.SerializeObject(eventInfo), jp.l, int.Parse(jp.cl)).Result);
        }
    }
}
