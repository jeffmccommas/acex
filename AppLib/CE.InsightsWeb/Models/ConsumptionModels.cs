﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using AutoMapper;
using CE.InsightsWeb.Common;
using CE.InsightsWeb.Common.AclaraJson;
using CE.InsightsWeb.Common.QueryStringEncryptor;
using Newtonsoft.Json;

namespace CE.InsightsWeb.Models
{
    public class ConsumptionModels
    {
        private const string ConsumptionCategory = "consumption";
        private const string CommonCategory = "common";
        private const string AclaraOneClientSettings = "aclaraone.clientsettings";
        private const string ResolutionKeyMonth = "month";
        private const string ResolutionKeyDay = "day";
        private const string ResolutionKeyHour = "hour";
        private const string ResolutionKey30 = "halfhour";
        private const string ResolutionKey15 = "fifteen";
        private const string ResolutionKey5 = "five";
        private const string WidgetType = "consumption";
        private const string CharTypeColumn = "column";
        private const string NoTou = "notou";
        private const string OnPeak = "onpeak";
        private const string OffPeak = "offpeak";
        private const string Shoulder1 = "shoulder1";
        private const string Shoulder2 = "shoulder2";
        private const string CriticalPeak = "criticalpeak";

        public Consumption GetConsumption(string tabKey, string parameters, string resolutionKey, string startDate, string endDate, string commodityKey, bool paging, bool drilling)
        {
            var jp = (JsonParams)JsonConvert.DeserializeObject(QueryStringModule.Decrypt(parameters), typeof(JsonParams));
            //Content
            var content = CMS.GetContent(int.Parse(jp.cl), jp.l).Content;
            var consumptionTextContent = content.TextContent.Where(x => x.category == ConsumptionCategory || x.category == CommonCategory).ToList();
            var consumptionConfig = content.Configuration.Where(x => x.category == ConsumptionCategory || x.category == CommonCategory).ToList();
            var consumptionUomContent = content.UOM.ToList();
            var c = new Consumption(consumptionConfig, consumptionTextContent, tabKey, WidgetType, jp)
            {
                ResolutionKeys = CMS.GetWidgetConfig(consumptionConfig, tabKey, WidgetType, "resolutionlist"),
                Commodities = CMS.GetWidgetConfig(consumptionConfig, tabKey, WidgetType, "commodities")
            };
            var hUsageFormat = CMS.GetWidgetConfig(consumptionConfig, tabKey, WidgetType, "hoverusageformat");

            //Content
            c.YourUse = CMS.GetWidgetContent(consumptionTextContent, tabKey, WidgetType, "youruse");
            c.CompareTo = CMS.GetWidgetContent(consumptionTextContent, tabKey, WidgetType, "compareto");
            c.Previous12Months = CMS.GetWidgetContent(consumptionTextContent, tabKey, WidgetType, "previous12months");
            c.NoDataForTimePeriod = CMS.GetWidgetContent(consumptionTextContent, tabKey, WidgetType, "nodatafortimeperiod");
            c.WeatherLabel = CMS.GetWidgetContent(consumptionTextContent, tabKey, WidgetType, "weatherlabel");
            c.AverageUsageLabel = CMS.GetWidgetContent(consumptionTextContent, tabKey, WidgetType, "averageusagelabel");
            c.TableViewColumn1DateLabel = CMS.GetWidgetContent(consumptionTextContent, tabKey, WidgetType, "tableviewcolumn1datelabel");
            c.TableViewColumn1MonthLabel = CMS.GetWidgetContent(consumptionTextContent, tabKey, WidgetType, "tableviewcolumn1monthlabel");

            //Configuration  
            c.MonthCount = CMS.GetWidgetConfig(consumptionConfig, tabKey, WidgetType, "monthcount");

            var ed = Convert.ToDateTime(endDate);
            c.DayCount = DateTime.DaysInMonth(ed.Year, ed.Month).ToString();
            c.HourCount = CMS.GetWidgetConfig(consumptionConfig, tabKey, WidgetType, "hourcount");
            c.ThirtyMinCount = CMS.GetWidgetConfig(consumptionConfig, tabKey, WidgetType, "30mincount");
            c.FifteenMinCount = CMS.GetWidgetConfig(consumptionConfig, tabKey, WidgetType, "15mincount");
            c.FiveMinCount = CMS.GetWidgetConfig(consumptionConfig, tabKey, WidgetType, "5mincount");
            c.ChartType = CMS.GetWidgetConfig(consumptionConfig, tabKey, WidgetType, "charttype");
            c.ChartType = c.ChartType == "" ? CharTypeColumn : c.ChartType;
            c.ChartLineColor1 = CMS.GetWidgetConfig(consumptionConfig, tabKey, WidgetType, "chartlinecolor1");
            c.ChartLineColor2 = CMS.GetWidgetConfig(consumptionConfig, tabKey, WidgetType, "chartlinecolor2");
            c.ChartLineColor3 = CMS.GetWidgetConfig(consumptionConfig, tabKey, WidgetType, "chartlinecolor3");
            c.ChartAnchortype = CMS.GetWidgetConfig(consumptionConfig, tabKey, WidgetType, "chartanchortype");
            c.LineThickness = CMS.GetWidgetConfig(consumptionConfig, tabKey, WidgetType, "linethickness");
            var tempFormat = consumptionConfig.Where(x => x.category == CommonCategory && x.key == "format.temperature").FirstOrDefault();
            c.TempFormat = tempFormat == null ? "" : tempFormat.value;
            c.EnableWeather = CMS.GetWidgetConfig(consumptionConfig, tabKey, WidgetType, "enableweather");
            c.ShowWeatherByDefault = Convert.ToBoolean(CMS.GetWidgetConfig(consumptionConfig, tabKey, WidgetType, "showweatherbydefault"));
            c.ShowCommodityLabels = CMS.GetWidgetConfig(consumptionConfig, tabKey, WidgetType, "showcommoditylabels");
            c.EnableAverageUsage = CMS.GetWidgetConfig(consumptionConfig, tabKey, WidgetType, "enableaverageusage");
            c.ShowAverageUsageByDefault = Convert.ToBoolean(CMS.GetWidgetConfig(consumptionConfig, tabKey, WidgetType, "showaverageusagebydefault"));
            c.AverageUsageLineColor = CMS.GetWidgetConfig(consumptionConfig, tabKey, WidgetType, "averageusagelinecolor");

            // Get client's local time zone from aclaraone.clientsettings
            var aclaraOneClientSettingConfigBulk = content.ConfigurationBulk.Where(x => x.key == AclaraOneClientSettings).Select(x => x.jsonconfiguration).FirstOrDefault();
            var aclaraOneClientSettings =
                aclaraOneClientSettingConfigBulk != null && !string.IsNullOrEmpty(aclaraOneClientSettingConfigBulk)
                    ? JsonConvert.DeserializeObject<ClientSettings>(aclaraOneClientSettingConfigBulk)
                    : null;
            var clientTimezone = (Enums.Timezone)Enum.Parse(typeof(Enums.Timezone), Convert.ToString(aclaraOneClientSettings.TimeZone));
            var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(clientTimezone.GetDescriptionOfEnum());
            // convert from client's local time zone to utc
            startDate = TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(startDate), timeZoneInfo).ToString();
            // consumption api's end date is not inclusive
            endDate = TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(endDate).AddMinutes(1), timeZoneInfo).ToString();


            var titleFormat = string.Empty;
            var chartFormat = string.Empty;

            var resolutionOffset = "";
            var min = 5;
            switch (resolutionKey)
            {
                case ResolutionKeyMonth:
                    titleFormat = CMS.GetWidgetConfig(consumptionConfig, tabKey, WidgetType, "titlemonthformat");
                    chartFormat = CMS.GetWidgetConfig(consumptionConfig, tabKey, WidgetType, "chartmonthformat");
                    resolutionOffset = c.MonthCount;
                    break;
                case ResolutionKeyDay:
                    titleFormat = CMS.GetWidgetConfig(consumptionConfig, tabKey, WidgetType, "titledayformat");
                    chartFormat = CMS.GetWidgetConfig(consumptionConfig, tabKey, WidgetType, "chartdayformat");
                    resolutionOffset = c.DayCount;
                    break;
                case ResolutionKeyHour:
                    titleFormat = CMS.GetWidgetConfig(consumptionConfig, tabKey, WidgetType, "titletimeformat");
                    chartFormat = CMS.GetWidgetConfig(consumptionConfig, tabKey, WidgetType, "charthourformat");
                    resolutionOffset = c.HourCount;
                    break;
                case ResolutionKey30:
                    titleFormat = CMS.GetWidgetConfig(consumptionConfig, tabKey, WidgetType, "titletimeformat");
                    chartFormat = CMS.GetWidgetConfig(consumptionConfig, tabKey, WidgetType, "chart30minformat");
                    resolutionOffset = c.ThirtyMinCount;
                    min = 30;
                    break;
                case ResolutionKey15:
                    titleFormat = CMS.GetWidgetConfig(consumptionConfig, tabKey, WidgetType, "titletimeformat");
                    chartFormat = CMS.GetWidgetConfig(consumptionConfig, tabKey, WidgetType, "chart15minformat");
                    resolutionOffset = c.FifteenMinCount;
                    min = 15;
                    break;
                case ResolutionKey5:
                    titleFormat = CMS.GetWidgetConfig(consumptionConfig, tabKey, WidgetType, "titletimeformat");
                    chartFormat = CMS.GetWidgetConfig(consumptionConfig, tabKey, WidgetType, "chart5minformat");
                    resolutionOffset = c.FiveMinCount;
                    min = 5;
                    break;
            }

            var consumptionResponse = new ConsumptionRoot();
            var commodities = string.Empty;
            var cnt = 0;

            if (drilling)
            {
                consumptionResponse = GetConsumptionFromCache(jp, resolutionKey, startDate, endDate, commodityKey, c.EnableWeather, c.EnableAverageUsage);
            }
            else if (paging)
            {
                consumptionResponse = GetConsumptionFromCache(jp, resolutionKey, startDate, endDate, commodityKey, c.EnableWeather, c.EnableAverageUsage);
            }
            else
            {
                if (string.IsNullOrEmpty(commodityKey))
                {
                    foreach (var com in c.Commodities.Replace(" ", "").Split(','))
                    {
                        var consumptionResp = GetConsumptionFromCache(jp, resolutionKey, startDate, endDate, com, c.EnableWeather, c.EnableAverageUsage, resolutionOffset);
                        if (consumptionResp.Data != null)
                        {
                            if (string.IsNullOrEmpty(commodities))
                            {
                                commodities += com;
                            }
                            else
                            {
                                commodities += "," + com;
                            }
                            if (cnt == 0)
                            {
                                consumptionResponse = consumptionResp;
                            }
                            cnt += 1;
                        }
                    }
                }
                else
                {
                    consumptionResponse = GetConsumptionFromCache(jp, resolutionKey, startDate, endDate, commodityKey, c.EnableWeather, c.EnableAverageUsage, resolutionOffset);
                }
            }
            if ((resolutionKey== ResolutionKeyHour || resolutionKey == ResolutionKey30 || resolutionKey == ResolutionKey15|| resolutionKey == ResolutionKey5) || (paging || drilling)) 
            {

                // convert back to client's local time zone from utc
                if (!string.IsNullOrEmpty(startDate))
                {
                    startDate = TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(startDate), timeZoneInfo).ToString();
                }
                if (!string.IsNullOrEmpty(endDate))
                {
                    endDate = TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(endDate).AddMinutes(-1), timeZoneInfo).ToString();
                }
            }
            if (consumptionResponse.Data == null || paging || drilling)
            {
                c.StartDate = startDate;
                c.EndDate = endDate;
                c.StartDateTitle = DateTime.Parse(startDate).ToString(titleFormat);
                c.EndDateTitle = DateTime.Parse(endDate).ToString(titleFormat);
            }
            else
            {
                c.StartDate = consumptionResponse.Data.First().DateTime;
                c.EndDate = consumptionResponse.Data.Last().DateTime;
                c.StartDateTitle = DateTime.Parse(c.StartDate).ToString(titleFormat);
                c.EndDateTitle = DateTime.Parse(c.EndDate).ToString(titleFormat);
            }

            Mapper.Reset();
            Mapper.CreateMap<List<Datum>, Consumption>()
                    .ForMember(dest => dest.Data,
                                  opt => opt.MapFrom(
                                      src => Mapper.Map<List<Datum>, List<DatumFormatted>>(src)));
            Mapper.CreateMap<string, DateTime>().ConvertUsing(Convert.ToDateTime);
            Mapper.CreateMap<Datum, DatumFormatted>()
                 .ForMember(dest => dest.ValueFormatted, opt => opt.MapFrom(src => src.Value.ToString(hUsageFormat)))
                 .ForMember(dest => dest.DateTimeLabel, opt => opt.MapFrom(src => DateTime.Parse(src.DateTime).ToString(titleFormat)))
                 .ForMember(dest => dest.DateTimeChart, opt => opt.MapFrom(src => DateTime.Parse(src.DateTime).ToString(chartFormat)));

            Mapper.CreateMap<List<TouConsumption>, DatumFormatted>()
                   .ForMember(dest => dest.TouConsumptions,
                                 opt => opt.MapFrom(
                                     src => Mapper.Map<List<TouConsumption>, List<TouFormatted>>(src)));
            Mapper.CreateMap<TouConsumption, TouFormatted>()
                .ForMember(dest => dest.ValueFormatted, opt => opt.MapFrom(src => src.Value.ToString(hUsageFormat)));

            Mapper.CreateMap<List<ConsumptionWeather>, Consumption>()
                    .ForMember(dest => dest.Weather,
                                  opt => opt.MapFrom(
                                      src => Mapper.Map<List<ConsumptionWeather>, List<WeatherFormatted>>(src)));
            Mapper.CreateMap<ConsumptionWeather, WeatherFormatted>()
                 .ForMember(dest => dest.ValueFormatted, opt => opt.MapFrom(src => src.AvgTemp))
                 .ForMember(dest => dest.DateTimeLabel, opt => opt.MapFrom(src => DateTime.Parse(src.Date).ToString(titleFormat)))
                 .ForMember(dest => dest.DateTimeChart, opt => opt.MapFrom(src => DateTime.Parse(src.Date).ToString(chartFormat)));

            Mapper.CreateMap<ConsumptionRoot, Consumption>();

            c.Commodities = commodities;
            c.CommoditiesText = CMS.GetCommoditiesName(consumptionTextContent, commodities);

            if (consumptionResponse.CustomerId == null || consumptionResponse.Data == null)
            {
                c.NoConsumption = true;
                c.ResolutionKeys = null;
                c.NoConsumptionText = CMS.GetWidgetContent(consumptionTextContent, tabKey, WidgetType, "nodata");
                return c;
            }

            var sqSplit = new List<string>(c.ResolutionKeys.Replace(" ", "").Split(','));
            c.ResolutionKeys = string.Join(",",
                                                (from r in consumptionResponse.ResolutionsAvailable.Split(',').AsQueryable()
                                                 where sqSplit.Any(key => r.ToString().ToLower().Equals(key))
                                                 select r).ToList());

            foreach (var resNameKey in c.ResolutionKeys.Replace(" ", "").Split(',').Select(rk => "enumeration." + rk + ".name"))
            {
                if (c.ResolutionKeysText == null)
                {
                    c.ResolutionKeysText =
                        consumptionTextContent.Where(e => e.key == resNameKey).Select(s => s.shorttext).FirstOrDefault();
                }
                else
                {
                    c.ResolutionKeysText += "," + consumptionTextContent.Where(e => e.key == resNameKey).Select(s => s.shorttext).FirstOrDefault();
                }
            }

            Mapper.Map(consumptionResponse, c);

            var dtl = new List<DatumFormatted>();
            var wl = new List<WeatherFormatted>();
            switch (resolutionKey)
            {
                case ResolutionKeyMonth:
                    for (var i = 0; i < 24; i++)
                    {
                        var date = Convert.ToDateTime(c.EndDate).AddMonths(i * -1);
                        var dt = c.Data.Where(x => Convert.ToInt32(Convert.ToDateTime(x.DateTime).Month) == Convert.ToInt32(date.Month)
                                                   && Convert.ToInt32(Convert.ToDateTime(x.DateTime).Year) == Convert.ToInt32(date.Year)).FirstOrDefault();

                        var w = c.Weather.Where(x => Convert.ToInt32(Convert.ToDateTime(x.Date).Month) == Convert.ToInt32(date.Month)
                                                   && Convert.ToInt32(Convert.ToDateTime(x.Date).Year) == Convert.ToInt32(date.Year)).FirstOrDefault();
                        dtl.Add(DataFormatted(date, dt, chartFormat, titleFormat));
                        wl.Add(WeatherFormatted(date, w, chartFormat, titleFormat));
                    }
                    break;
                case ResolutionKeyDay:
                    for (var i = 0; i < Convert.ToInt32(resolutionOffset); i++)
                    {
                        var date = Convert.ToDateTime(c.EndDate).AddDays(i * -1);
                        var dt = c.Data.Where(x => Convert.ToInt32(Convert.ToDateTime(x.DateTime).Month) == Convert.ToInt32(date.Month)
                                                    && Convert.ToInt32(Convert.ToDateTime(x.DateTime).Year) == Convert.ToInt32(date.Year)
                                                    && Convert.ToInt32(Convert.ToDateTime(x.DateTime).Day) == Convert.ToInt32(date.Day)).FirstOrDefault();
                        var w = c.Weather.Where(x => Convert.ToInt32(Convert.ToDateTime(x.Date).Month) == Convert.ToInt32(date.Month)
                                                    && Convert.ToInt32(Convert.ToDateTime(x.Date).Year) == Convert.ToInt32(date.Year)
                                                    && Convert.ToInt32(Convert.ToDateTime(x.Date).Day) == Convert.ToInt32(date.Day)).FirstOrDefault();
                        dtl.Add(DataFormatted(date, dt, chartFormat, titleFormat));
                        wl.Add(WeatherFormatted(date, w, chartFormat, titleFormat));
                    }
                    break;
                case ResolutionKeyHour:
                    for (var i = 0; i < Convert.ToInt32(resolutionOffset); i++)
                    {
                        var date = Convert.ToDateTime(c.EndDate).AddHours(i * -1);
                        var dt = c.Data.Where(x => Convert.ToInt32(Convert.ToDateTime(x.DateTime).Month) == Convert.ToInt32(date.Month)
                                                   && Convert.ToInt32(Convert.ToDateTime(x.DateTime).Year) == Convert.ToInt32(date.Year)
                                                   && Convert.ToInt32(Convert.ToDateTime(x.DateTime).Day) == Convert.ToInt32(date.Day)
                                                   && Convert.ToInt32(Convert.ToDateTime(x.DateTime).Hour) == Convert.ToInt32(date.Hour)).FirstOrDefault();

                        dtl.Add(DataFormatted(date, dt, chartFormat, titleFormat));
                    }
                    break;
                default:
                    for (var i = 0; i < Convert.ToInt32(resolutionOffset); i++)
                    {
                        var date = Convert.ToDateTime(c.EndDate).AddMinutes(min * i * -1);
                        var dt = c.Data.Where(x => Convert.ToInt32(Convert.ToDateTime(x.DateTime).Month) == Convert.ToInt32(date.Month)
                                                   && Convert.ToInt32(Convert.ToDateTime(x.DateTime).Year) == Convert.ToInt32(date.Year)
                                                   && Convert.ToInt32(Convert.ToDateTime(x.DateTime).Day) == Convert.ToInt32(date.Day)
                                                   && Convert.ToInt32(Convert.ToDateTime(x.DateTime).Hour) == Convert.ToInt32(date.Hour)
                                                   && Convert.ToInt32(Convert.ToDateTime(x.DateTime).Minute) == Convert.ToInt32(date.Minute)).FirstOrDefault();

                        dtl.Add(DataFormatted(date, dt, chartFormat, titleFormat));
                    }
                    break;
            }

            c.Data = dtl.OrderBy(x => Convert.ToDateTime(x.DateTime)).ToList();
            c.Weather = wl.OrderBy(x => Convert.ToDateTime(x.Date)).ToList();

            c.UOMText = consumptionTextContent.Where(z => z.key == consumptionUomContent.Where(x => x.key == c.UOMKey).FirstOrDefault().namekey).FirstOrDefault().shorttext;

            //Create tou series
            var tList = c.Data.Where(x => x.TouConsumptions != null).SelectMany(x => x.TouConsumptions).Select(x => x.TouKey).Distinct().Where(x => x != NoTou).ToList();
            if (tList != null && tList.Count > 0)
            {
                c.IsTou = true;
            }
           
            var tsl = new List<TouSeriesList>();

            foreach (var t in tList)
            {
                var ts = new TouSeriesList();
                switch (t)
                {
                    case OnPeak:
                        ts.Color = CMS.GetWidgetConfig(consumptionConfig, tabKey, WidgetType, "onpeakcolor");
                        ts.Label = CMS.GetWidgetContent(consumptionTextContent, tabKey, WidgetType, "onpeaklabel");
                        break;
                    case OffPeak:
                        ts.Color = CMS.GetWidgetConfig(consumptionConfig, tabKey, WidgetType, "offpeakcolor");
                        ts.Label = CMS.GetWidgetContent(consumptionTextContent, tabKey, WidgetType, "offpeaklabel");
                        break;
                    case Shoulder1:
                        ts.Color = CMS.GetWidgetConfig(consumptionConfig, tabKey, WidgetType, "shoulder1color");
                        ts.Label = CMS.GetWidgetContent(consumptionTextContent, tabKey, WidgetType, "shoulder1label");
                        break;
                    case Shoulder2:
                        ts.Color = CMS.GetWidgetConfig(consumptionConfig, tabKey, WidgetType, "shoulder2color");
                        ts.Label = CMS.GetWidgetContent(consumptionTextContent, tabKey, WidgetType, "shoulder2label");
                        break;
                    case CriticalPeak:
                        ts.Color = CMS.GetWidgetConfig(consumptionConfig, tabKey, WidgetType, "criticalpeakcolor");
                        ts.Label = CMS.GetWidgetContent(consumptionTextContent, tabKey, WidgetType, "criticalpeaklabel");
                        break;
                }

                var touList = new List<Tou>();
                foreach (var dt in c.Data)
                {
                    if (dt.TouConsumptions == null)
                    {
                        var tou = new Tou
                        {
                            DateTime = dt.DateTime,
                            DateTimeChart = dt.DateTimeChart,
                            DateTimeLabel = dt.DateTimeLabel,
                            TouKey = null,
                            Value = null,
                            ValueFormatted = null
                        };
                        touList.Add(tou);
                    }
                    else
                    {
                        var tt = dt.TouConsumptions.Where(x => x.TouKey == t).FirstOrDefault();

                        var tou = new Tou
                        {
                            DateTime = dt.DateTime,
                            DateTimeChart = dt.DateTimeChart,
                            DateTimeLabel = dt.DateTimeLabel,
                            TouKey = tt?.TouKey,
                            Value = tt?.Value,
                            ValueFormatted = tt?.ValueFormatted
                        };
                        touList.Add(tou);
                    }
                }
                ts.TouSeries = touList;
                ts.TouKey = t;
                tsl.Add(ts);
            }
            c.TouSeriesList = tsl;

            if (c.EnableAverageUsage == "true")
            {
                c.AverageUsageFormatted = c.AverageUsage.ToString(hUsageFormat);
            }

            if (c.IsTou)
            {
               var ts = c.TouSeriesList.FirstOrDefault().TouSeries;
                c.StartDate = ts.First().DateTime;
                c.EndDate = ts.Last().DateTime;
                c.StartDateTitle = DateTime.Parse(c.StartDate).ToString(titleFormat);
                c.EndDateTitle = DateTime.Parse(c.EndDate).ToString(titleFormat);
            }
            else
            {
                c.StartDate = c.Data.First().DateTime;
                c.EndDate = c.Data.Last().DateTime;
                c.StartDateTitle = DateTime.Parse(c.StartDate).ToString(titleFormat);
                c.EndDateTitle = DateTime.Parse(c.EndDate).ToString(titleFormat);
            }
            return c;
        }

        private static ConsumptionRoot GetConsumptionFromCache(JsonParams jp, string rKey, string startDate, string endDate, string commodityKey, string enableWeather, string enableAverageUsage, string count = null)
        {
            var rm = new APIResponseModels();
            var customerInfo = CustomerInfoModels.GetCustomerInfoFromCache(jp);
            var meters="";
            if (customerInfo.Customer!=null)
            {
                meters = string.Join(",", customerInfo.Customer.Accounts.Where(x => x.Id == jp.a)
                                            .SelectMany(c => c.Premises)
                                            .Where(p => p.Id == jp.p)
                                            .SelectMany(s => s.Service)
                                            .Where(c => c.CommodityKey == commodityKey)
                                            .SelectMany(c => c.ServicePoints)
                                            .SelectMany(c => c.Meters).Select(x => x.Id).ToList());
            }
            string requestParams;
            if (count == null)
            {
                requestParams = "CustomerId=" + jp.c + "&AccountId=" + jp.a + "&PremiseId=" + jp.p + "&MeterIds=" + meters + "&StartDate=" + startDate + "&EndDate=" +
                                    endDate + "&ResolutionKey=" + rKey + "&CommodityKey=" + commodityKey + "&IncludeResolutions=true&IncludeContent=false";
            }
            else
            {
                requestParams = "CustomerId=" + jp.c + "&AccountId=" + jp.a + "&PremiseId=" + jp.p + "&MeterIds=" + meters + "&StartDate=" + startDate + "&EndDate=" +
                                   endDate + "&ResolutionKey=" + rKey + "&CommodityKey=" + commodityKey + "&Count=" + count + "&IncludeResolutions=true&IncludeContent=false";
            }
            if (enableWeather == "true")
            {
                if (customerInfo.Customer != null)
                    requestParams += "&includeweather=true&zipcode=" + customerInfo.Customer.PostalCode + "&country=" + customerInfo.Customer.Country;
            }
            if (enableAverageUsage == "true")
            {
                requestParams += "&includeaverageusage=true";
            }
            var consumptionResponse = (ConsumptionRoot)JsonConvert.DeserializeObject(rm.GetConsumption(requestParams, jp.l, int.Parse(jp.cl)).Content, typeof(ConsumptionRoot));
            return consumptionResponse;
        }

        private static DatumFormatted DataFormatted(DateTime date, DatumFormatted dt, string chartFormat, string titleFormat)
        {
            if (dt != null) return dt;
            var dt1 = new DatumFormatted
            {
                DateTime = date.ToString(CultureInfo.CurrentCulture),
                DateTimeChart = date.ToString(chartFormat),
                DateTimeLabel = date.ToString(titleFormat)
            };
            return dt1;
        }

        private static WeatherFormatted WeatherFormatted(DateTime date, WeatherFormatted dt, string chartFormat, string titleFormat)
        {
            if (dt != null) return dt;
            var dt1 = new WeatherFormatted
            {
                Date = date.ToString(CultureInfo.CurrentCulture),
                DateTimeChart = date.ToString(chartFormat),
                DateTimeLabel = date.ToString(titleFormat)
            };
            return dt1;
        }
    }

    public class Consumption : ContentBase
    {      
        public string YourUse { get; set; }
        public string CompareTo { get; set; }
        public string Previous12Months { get; set; }
        public string NoConsumptionText { get; set; }
        public string NoDataForTimePeriod { get; set; }
        public string Commodities { get; set; }       
        public string ShowCommodityLabels { get; set; }
        public bool NoConsumption { get; set; }
        public string CommoditiesText { get; set; }
        public string MonthCount { get; set; }
        public string DayCount { get; set; }
        public string HourCount { get; set; }
        public string ThirtyMinCount { get; set; }
        public string FifteenMinCount { get; set; }
        public string FiveMinCount { get; set; }
        public string ResolutionKey { get; set; }
        public double AverageUsage { get; set; }
        public string AverageUsageFormatted { get; set; }
        public string ResolutionsAvailable { get; set; }
        public string CommodityKey { get; set; }
        public string ResolutionKeys { get; set; }
        public string ResolutionKeysText { get; set; }
        public string ChartLineColor1 { get; set; }
        public string ChartLineColor2 { get; set; }
        public string ChartLineColor3 { get; set; }
        public string ChartType { get; set; }
        public string ChartAnchortype { get; set; }
        public string LineThickness { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string StartDateTitle { get; set; }
        public string EndDateTitle { get; set; }
        public string UOMKey { get; set; }
        public string UOMText { get; set; }      
        public string TempFormat { get; set; }
        public string WeatherLabel { get; set; }
        public string TableViewColumn1DateLabel { get; set; }
        public string TableViewColumn1MonthLabel { get; set; }
        public string EnableWeather { get; set; }
        public bool ShowWeatherByDefault { get; set; }
        public bool IsTou { get; set; }
        public string EnableAverageUsage { get; set; }
        public bool ShowAverageUsageByDefault { get; set; }
        public string AverageUsageLineColor { get; set; }
        public string AverageUsageLabel { get; set; }
        public List<DatumFormatted> Data { get; set; }
        public List<WeatherFormatted> Weather { get; set; }
        public List<TouSeriesList> TouSeriesList { get; set; }
        public Consumption(List<Common.Configuration> config, List<Common.TextContent> content, string tabKey, string widetType, JsonParams jp) : base(config, content, tabKey, widetType, jp)
        { }
    }

    public class DatumFormatted
    {
        public string DateTime { get; set; }
        public double? Value { get; set; }
        public string ValueFormatted { get; set; }
        public string DateTimeLabel { get; set; }
        public string DateTimeChart { get; set; }
        public List<TouFormatted> TouConsumptions { get; set; }
    }

    public class WeatherFormatted
    {
        public string Date { get; set; }
        public int AvgTemp { get; set; }
        public string ValueFormatted { get; set; }
        public string DateTimeLabel { get; set; }
        public string DateTimeChart { get; set; }
    }

    public class TouFormatted
    {
        public string TouKey { get; set; }
        public double? Value { get; set; }
        public string ValueFormatted { get; set; }
    }

    public class TouSeriesList
    {
        public List<Tou> TouSeries { get; set; }
        public string TouKey { get; set; }
        public string Color { get; set; }
        public string Label { get; set; }
    }

    public class Tou
    {
        public string DateTime { get; set; }
        public string DateTimeLabel { get; set; }
        public string DateTimeChart { get; set; }
        public string TouKey { get; set; }
        public double? Value { get; set; }
        public string ValueFormatted { get; set; }
    }
}

