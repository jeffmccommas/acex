﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CE.InsightsWeb.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class InsightsEntities : DbContext
    {
        public InsightsEntities()
            : base("name=InsightsEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserEnvironment> UserEnvironments { get; set; }
        public virtual DbSet<ControlPanelLogin> ControlPanelLogins { get; set; }
        public virtual DbSet<ControlPanelLoginClient> ControlPanelLoginClients { get; set; }
        public virtual DbSet<Group> Groups { get; set; }
        public virtual DbSet<ClientTabCondition> ClientTabConditions { get; set; }
        public virtual DbSet<GroupRole> GroupRoles { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<application_information_grant_types> application_information_grant_types { get; set; }
        public virtual DbSet<application_information_scopes> application_information_scopes { get; set; }
        public virtual DbSet<subscription_usage_points> subscription_usage_points { get; set; }
        public virtual DbSet<time_configurations> time_configurations { get; set; }
        public virtual DbSet<usage_point_related_links> usage_point_related_links { get; set; }
        public virtual DbSet<reading_types> reading_types { get; set; }
        public virtual DbSet<subscription> subscriptions { get; set; }
        public virtual DbSet<usage_points> usage_points { get; set; }
        public virtual DbSet<retail_customers> retail_customers { get; set; }
        public virtual DbSet<authorization> authorizations { get; set; }
        public virtual DbSet<application_information> application_information { get; set; }
    }
}
