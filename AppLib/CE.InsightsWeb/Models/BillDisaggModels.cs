﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using CE.InsightsWeb.Common;
using CE.InsightsWeb.Common.AclaraJson;
using CE.InsightsWeb.Common.QueryStringEncryptor;
using LinqKit;
using Newtonsoft.Json;

namespace CE.InsightsWeb.Models
{
    public class BillDisaggModel
    {
        public BillDisaggWidget GetBillDisaggData(string parameters, string tabKey, string widgetType)
        {
            var jp = (JsonParams)JsonConvert.DeserializeObject(QueryStringModule.Decrypt(parameters), typeof(JsonParams));
            var cr = CMS.GetContent(int.Parse(jp.cl), jp.l);

            var predicate = PredicateBuilder.True<Common.TextContent>();
            predicate = predicate.And(m => m.category == "billdisagg");

            var predicateConfig = PredicateBuilder.True<Common.Configuration>();
            predicateConfig = predicateConfig.And(m => m.category == "billdisagg");

            var configuredCommodities = CMS.GetWidgetConfig(cr.Content, tabKey, widgetType, "commodities", predicateConfig).Replace(" ", "").Split(',').ToList();

            var disaggResponse = GetBillDisagg(jp);
            var billDisagg = new BillDisaggWidget(cr.Content.Configuration, cr.Content.TextContent, tabKey, widgetType, jp)
            {
                ShowCommodityLabels = CMS.GetWidgetConfig(cr.Content, tabKey, widgetType, "showcommoditylabels", predicateConfig),
                ShowCombinedView = CMS.GetWidgetConfig(cr.Content, tabKey, widgetType, "combinedview", predicateConfig)
            };

            var legendData = new List<LegendData>();
            var endUses = disaggResponse.Customer.Accounts[0].Premises[0].EndUses.ToList();
            var disaggStatus = disaggResponse.Customer.Accounts[0].Premises[0].DisaggStatuses;

            billDisagg.Dropdown = new List<DropdownItem>();

            var combinedView = false;
            if (billDisagg.ShowCombinedView != string.Empty)
            {
                combinedView = Convert.ToBoolean(billDisagg.ShowCombinedView);
            }

            double totalCost = 0;
            if (combinedView)
            {
                //end use logic
                var pieDataAll = new List<string>();
                var legendDataAll = new List<LegendData>();
                var pieData = new List<string>();

                var combinedViewlist = disaggStatus.Where(x => x.Status == Enums.DisaggStatusType.ComputedBills || x.Status == Enums.DisaggStatusType.ActualBills).Select(x => x.CommodityKey).ToList();
                var comlst = configuredCommodities.Where(c => combinedViewlist.Any(key => c.Equals(key.ToString().ToLower()))).ToList();
                var useConfComm = false;
                if (comlst.Count == 0)
                {
                    comlst = configuredCommodities;
                    useConfComm = true;
                }
                var endUseConfiguredCom = endUses.Where(c => c.DisaggDetails.Any(x => comlst.Any(key => x.CommodityKey.Equals(key)))).ToList();

                var totalCostAll = (from d in endUses.Select(x => x.DisaggDetails)
                                    select d.Where(y => comlst.Any(key => y.CommodityKey.Equals(key))).Sum(z => z.CostAmount)).ToList().Sum();

                //Model only or computed bills for more than one commodity
                if (useConfComm || comlst.Count > 1)
                {
                    foreach (var endUse in endUseConfiguredCom)
                    {
                        var leg = new LegendData();

                        if (endUse.Key == "enduse.other")
                        {
                            endUse.Key = "enduse.miscellaneous";
                        }

                        leg.Icon = "legend " + endUse.Key.Replace("enduse.", "");

                        var endUseNameKey = endUse.Key + ".name";
                        var endUseText = CMS.GetCommonContent(cr, endUseNameKey);
                        leg.Text = endUseText;

                        var ci = new CultureInfo(jp.l);
                        var cost = endUse.DisaggDetails.Sum(dd => dd.CostAmount);
                        leg.Dollars = cost.ToString("C0", ci);

                        //Model Only
                        leg.Percent = (cost / totalCostAll).ToString(CMS.GetCommonContent(cr, "format.percent", "config"), ci);
                        legendDataAll.Add(leg);
                        pieDataAll.Add(endUseText + "|" + leg.Percent + "|" + CMS.GetWidgetConfig(cr.Content, tabKey, widgetType, "color." + endUse.Key.Replace("enduse.", ""), predicateConfig) + "|0|" + cost.ToString("C0", ci) + "|");
                    }
                }
                else if (comlst.Count == 1)
                {
                    var i = 0;
                    foreach (var com in comlst)
                    {
                        i++;
                        var totalUsage = (from d in endUseConfiguredCom.Select(x => x.DisaggDetails)
                                          select d.Where(y => y.CommodityKey == com).Sum(z => z.UsageQuantity)).ToList().Sum();
                        var comNameKey = "commodity." + com + ".name";
                        var comFontKey = "commodity." + com + ".iconfont";
                        i++;
                        billDisagg.Dropdown.Add(new DropdownItem(i,
                                                                com,
                                                                cr.Content.TextContent.Where(e => e.key == comNameKey).Select(s => s.shorttext).FirstOrDefault(),
                                                                cr.Content.TextContent.Where(e => e.key == comFontKey).Select(s => s.shorttext).FirstOrDefault()
                                                                ));

                        foreach (var endUse in endUseConfiguredCom)
                        {
                            foreach (var dd in endUse.DisaggDetails)
                            {
                                if (dd.CommodityKey != com) continue;
                                if (!(dd.CostAmount > 0)) continue;
                                SetupPieData(billDisagg, cr, endUse, dd, jp, com, legendData, pieData, tabKey, widgetType, totalUsage, predicateConfig);
                            }
                        }
                    }
                }

                if (useConfComm || comlst.Count > 1)
                {
                    ShowHidePercentage(billDisagg, pieDataAll, legendDataAll, disaggStatus, totalCostAll, "combined");
                    billDisagg.All.ShowUsage = false;
                    billDisagg.All.ShowPercentageOnly = true;
                }
                else
                {
                    if (comlst.Count == 1)
                    {
                        ShowHidePercentage(billDisagg, pieData, legendData, disaggStatus, totalCostAll, comlst.FirstOrDefault());
                        billDisagg.All.ShowUsage = true;
                    }
                }
            }
            else
            {
                //end use logic
                var i = 0;
                foreach (var com in configuredCommodities)
                {
                    var pieData = new List<string>();
                    var endUsesWithCom = endUses.Where(c => c.DisaggDetails.Any(x => x.CommodityKey == com)).ToList();

                    //add the commodity text to the property used to build the dropdown
                    if (endUsesWithCom.Count > 0)
                    {
                        legendData = new List<LegendData>();
                        var comNameKey = "commodity." + com + ".name";
                        var comFontKey = "commodity." + com + ".iconfont";
                        i++;
                        billDisagg.Dropdown.Add(new DropdownItem(i, 
                                                                com,
                                                                cr.Content.TextContent.Where(e => e.key == comNameKey).Select(s => s.shorttext).FirstOrDefault(),
                                                                cr.Content.TextContent.Where(e => e.key == comFontKey).Select(s => s.shorttext).FirstOrDefault()
                                                                ));

                        var totalUsage = (from d in endUsesWithCom.Select(x => x.DisaggDetails)
                                          select d.Where(y => y.CommodityKey == com).Sum(z => z.UsageQuantity)).ToList().Sum();

                        foreach (var endUse in endUsesWithCom)
                        {
                            totalCost = 0;
                            foreach (var dd in endUse.DisaggDetails)
                            {
                                if (dd.CommodityKey != com || !(dd.UsageQuantity > 0)) continue;
                                totalCost += dd.CostAmount;
                                SetupPieData(billDisagg, cr, endUse, dd, jp, com, legendData, pieData, tabKey, widgetType, totalUsage, predicateConfig);
                            }
                        }
                    }
                    ShowHidePercentage(billDisagg, pieData, legendData, disaggStatus, totalCost, com);
                }
            }

            //widget content/configuration need by the ui
            billDisagg.CostColumnHeader = CMS.GetWidgetContent(cr.Content, tabKey, widgetType, "costcolumnheader", predicate);
            billDisagg.UsageColumnHeader = CMS.GetWidgetContent(cr.Content, tabKey, widgetType, "usagecolumnheader", predicate);
            billDisagg.PercentColumnHeader = CMS.GetWidgetContent(cr.Content, tabKey, widgetType, "percentcolumnheader", predicate);
            billDisagg.NoModelledUsage = CMS.GetWidgetContent(cr.Content, tabKey, widgetType, "nomodelledusage", predicate);

            return billDisagg;
        }

        private static void SetupPieData(BillDisaggWidget billDisagg, RootContentObject cr, EndUs endUse,
                                            DisaggDetail dd, JsonParams jp, string com, List<LegendData> legendData, List<string> pieData,
                                            string tabKey, string widgetType, double totalUsage, System.Linq.Expressions.Expression<Func<Common.Configuration, bool>> predicateConfig)
        {
            var leg = new LegendData();

            if (endUse.Key == "enduse.other")
            {
                endUse.Key = "enduse.miscellaneous";
            }

            leg.Icon = "legend " + endUse.Key.Replace("enduse.", "");

            var endUseNameKey = endUse.Key + ".name";
            var endUseText = CMS.GetCommonContent(cr, endUseNameKey);
            leg.Text = endUseText;

            var uomNameKey = "uom." + dd.UsageUOMKey + ".name";

            var quantityFormat = "";
            switch (com)
            {
                case "gas":
                    leg.UOM = billDisagg.Gas.UOM = CMS.GetCommonContent(cr, uomNameKey);
                    quantityFormat = CMS.GetCommonContent(cr, "format.gasquantity", "config");
                    break;
                case "electric":
                    leg.UOM = billDisagg.Electric.UOM = CMS.GetCommonContent(cr, uomNameKey);
                    quantityFormat = CMS.GetCommonContent(cr, "format.electricquantity",
                        "config");
                    break;
                case "water":
                    leg.UOM = billDisagg.Water.UOM = CMS.GetCommonContent(cr, uomNameKey);
                    quantityFormat = CMS.GetCommonContent(cr, "format.waterquantity", "config");
                    break;
            }

            var ci = new CultureInfo(jp.l);
            leg.Dollars = dd.CostAmount.ToString("C0", ci);
            leg.Usage = dd.UsageQuantity.ToString(quantityFormat, ci);
            leg.Percent = (dd.UsageQuantity / totalUsage).ToString(CMS.GetCommonContent(cr, "format.percent", "config"), ci);
            legendData.Add(leg);
            pieData.Add(endUseText + "|" + leg.Percent + "|" +
                        CMS.GetWidgetConfig(cr.Content, tabKey, widgetType,
                            "color." + endUse.Key.Replace("enduse.", ""), predicateConfig) + "|" +
                            dd.UsageQuantity.ToString(quantityFormat, ci) + "|" +
                             dd.CostAmount.ToString("C0", ci) + "|" +
                             CMS.GetCommonContent(cr, uomNameKey));
        }

        private static void ShowHidePercentage(BillDisaggWidget billDisagg, List<string> pieData, List<LegendData> legendData, List<DisaggStatus> disaggStatus, double totalCost, string com)
        {
            Enums.DisaggStatusType status;
            DisaggStatus stat;
            switch (com)
            {
                case "gas":
                    billDisagg.Gas.PieData = JsonConvert.SerializeObject(pieData);
                    billDisagg.Gas.LegendData = JsonConvert.SerializeObject(legendData);

                    stat = disaggStatus.FirstOrDefault(x => x.CommodityKey == Enums.CommodityType.Gas);
                    status = stat?.Status ?? Enums.DisaggStatusType.Unspecified;
                    if (status == Enums.DisaggStatusType.ModelOnly)
                    {
                        billDisagg.Gas.ShowPercentageOnly = true;
                    }

                    if (!billDisagg.Gas.ShowPercentageOnly)
                    {
                        billDisagg.Gas.ShowPercentageOnly = !(totalCost > 0);
                    }

                    break;
                case "electric":
                    billDisagg.Electric.PieData = JsonConvert.SerializeObject(pieData);
                    billDisagg.Electric.LegendData = JsonConvert.SerializeObject(legendData);

                    stat = disaggStatus.FirstOrDefault(x => x.CommodityKey == Enums.CommodityType.Electric);
                    status = stat?.Status ?? Enums.DisaggStatusType.Unspecified;
                    if (status == Enums.DisaggStatusType.ModelOnly)
                    {
                        billDisagg.Electric.ShowPercentageOnly = true;
                    }

                    if (!billDisagg.Electric.ShowPercentageOnly)
                    {
                        billDisagg.Electric.ShowPercentageOnly = !(totalCost > 0);
                    }

                    break;
                case "water":
                    billDisagg.Water.PieData = JsonConvert.SerializeObject(pieData);
                    billDisagg.Water.LegendData = JsonConvert.SerializeObject(legendData);

                    stat = disaggStatus.FirstOrDefault(x => x.CommodityKey == Enums.CommodityType.Water);
                    status = stat?.Status ?? Enums.DisaggStatusType.Unspecified;
                    if (status == Enums.DisaggStatusType.ModelOnly)
                    {
                        billDisagg.Water.ShowPercentageOnly = true;
                    }

                    if (!billDisagg.Water.ShowPercentageOnly)
                    {
                        billDisagg.Water.ShowPercentageOnly = !(totalCost > 0);
                    }
                    break;
                case "combined":
                    billDisagg.All.PieData = JsonConvert.SerializeObject(pieData);
                    billDisagg.All.LegendData = JsonConvert.SerializeObject(legendData);

                    break;
            }
        }

        public static EndUseRoot GetBillDisagg(JsonParams jp)
        {
            var rm = new APIResponseModels();
            var dt = "StartDate=" + DateTime.Now.AddYears(-1).ToString("yyyy-MM-dd") + "&EndDate=" + DateTime.Now.ToString("yyyy-MM-dd");
            var requestParams = "CustomerId=" + jp.c + "&AccountId=" + jp.a + "&PremiseId=" + jp.p + "&" + dt;
            return (EndUseRoot)JsonConvert.DeserializeObject(rm.GetBillDisagg(requestParams, jp.l, int.Parse(jp.cl)).Content, typeof(EndUseRoot));
        }

        public class BillDisaggWidget : ContentBase
        {
            public string CostColumnHeader { get; set; }
            public string UsageColumnHeader { get; set; }
            public string PercentColumnHeader { get; set; }
            public string HeaderTitle { get; set; }
            public List<DropdownItem> Dropdown { get; set; }
            public string NoModelledUsage { get; set; }
            public string ShowCommodityLabels { get; set; }
            public string ShowCombinedView { get; set; }
           
            public BillDisaggCommodity Gas = new BillDisaggCommodity();
            public BillDisaggCommodity Electric = new BillDisaggCommodity();
            public BillDisaggCommodity Water = new BillDisaggCommodity();
            public BillDisaggCommodity All = new BillDisaggCommodity();

            public BillDisaggWidget(List<Common.Configuration> config, List<Common.TextContent> content, string tabKey,string widetType, JsonParams jp) : base(config, content, tabKey, widetType, jp)
            {
                
            }
        }

        public class BillDisaggCommodity
        {
            public string PieData { get; set; }
            public string LegendData { get; set; }
            public string UOM { get; set; }
            public bool ShowPercentageOnly { get; set; }
            public bool ShowUsage { get; set; }
        }
        public class DropdownItem
        {
            public int Key { get; set; }
            public string Value { get; set; }
            public string Text { get; set; }
            public string Font { get; set; }
            public DropdownItem()
            {
            }
            public DropdownItem(int key, string val, string tex, string font)
            {
                Key = key;
                Value = val;
                Text = tex;
                Font = font;
            }
        }
    }
}
