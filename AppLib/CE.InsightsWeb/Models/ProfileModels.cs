﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using AutoMapper;
using CE.InsightsWeb.Common;
using CE.InsightsWeb.Common.AclaraJson;
using CE.InsightsWeb.Common.QueryStringEncryptor;
using CE.InsightsWeb.Helpers;
using Newtonsoft.Json;
using TextContent = CE.InsightsWeb.Common.TextContent;

namespace CE.InsightsWeb.Models
{
    public class ProfileModels
    {
        private const string WidgetType = "profile";
        private const string WidgetTypeLong = "longprofile";
        private const string PremiseTypeAttributeKey = "premisetype";
        private const string PremiseTypeResidentialOption = "premisetype.residential";
        private const string PremiseTypeBusinessOption = "premisetype.business";

        public Profile GetProfile(string tabKey, string parameters)
        {
            var jp = (JsonParams)JsonConvert.DeserializeObject(QueryStringModule.Decrypt(parameters), typeof(JsonParams));
            var count = 0;
            var countLock = 0;
            var content = CMS.GetContent(int.Parse(jp.cl), jp.l).Content;
            var profTextContent = content.TextContent.Where(x => x.category == WidgetType).ToList();
            var profConfig = content.Configuration.Where(x => x.category == WidgetType).ToList();
            var profFileContent = content.FileContent.Where(x => x.category == "action").ToList();
            var widgetKey = content.Widget.FirstOrDefault(x => x.widgettype == WidgetType && x.tabkey == tabKey).key;
            var prof = new Profile(profConfig, profTextContent, tabKey, WidgetType, jp);

            //Get sections from tab configured for this widget
            var profSec = content.Tab.FirstOrDefault(x => x.tabtype == "widget" && x.widgetkey == widgetKey).profilesections.Split(';');
            var sectionList = (from s in profSec where s.Length != 0 select content.ProfileSection.FirstOrDefault(x => x.key == s)).ToList();

            sectionList = sectionList.OrderBy(x => x.rank).ToList();
            var countSection = sectionList.Count;

            var profileResponse = GetProfileAttributes(jp);

            var conditions = new List<string>();
            foreach (var sq in sectionList)
            {
                count += 1;

                var conditionList = new List<Condition>();
                // Condition from section
                if (sq.conditions.Length > 0)
                {
                    var s = sq.conditions.Split(';');
                    conditionList.AddRange(s.Select(i => content.Condition.FirstOrDefault(x => x.key == i)));
                }
                if (prof.Tabs == null || conditionList.Count > 0)
                {
                    var sqSplit = new List<string>(sq.profileattributes.Split(';'));
                    List<string> missingAttr;
                    //get attributes that don't exist in insights db-- Customer, Account and Premise
                    if (profileResponse.Customer != null)
                    {
                        if (profileResponse.Customer.Accounts.Count > 0 && profileResponse.Customer.Accounts[0].Premises.Count > 0)
                        {
                            missingAttr = sqSplit.Where(z => profileResponse.Customer.Attributes.ToList().All(z2 => z2.AttributeKey != z)).ToList()
                                       .Where(z => profileResponse.Customer.Accounts[0].Attributes.ToList().All(z2 => z2.AttributeKey != z)).ToList()
                                       .Where(z => profileResponse.Customer.Accounts[0].Premises[0].Attributes.ToList().All(z2 => z2.AttributeKey != z)).ToList();
                        }
                        else
                        {
                            missingAttr = sqSplit.Where(z => profileResponse.Customer.Attributes.ToList().All(z2 => z2.AttributeKey != z)).ToList()
                                     .Where(z => profileResponse.Customer.Accounts[0].Attributes.ToList().All(z2 => z2.AttributeKey != z)).ToList();
                        }
                    }
                    else
                    {
                        missingAttr = sqSplit;
                    }

                    //Create question list                  
                    var qm = new QuestionMerge
                    {
                        Attributes = (from pa in content.ProfileAttribute.AsQueryable()
                                      where sqSplit.Any(key => pa.key.Equals(key))
                                      select pa).OrderBy(x => sqSplit.IndexOf(x.key)).ToList()
                    };
                    // Add condition attribute to get answer.
                    foreach (var c in conditionList.Where(c => c.key != null))
                    {
                        qm.Attributes.Add((from pa in content.ProfileAttribute.AsQueryable()
                                           where pa.key == c.profileattributekey
                                           select pa).FirstOrDefault());
                    }
                    qm.Options = content.ProfileOption;
                    qm.Text = profTextContent;
                    qm.Profile = profileResponse;
                    qm.TabKey = tabKey;
                    qm.IsLongProfile = false;

                    var q = new List<Question>();
                    var s = new List<Section>();
                    var t = new List<ProfileTab>();

                    Mapper.CreateMap<QuestionMerge, List<Question>>().ConvertUsing(new QuestionConverter());
                    var mappedProf = Mapper.Map(qm, q);

                    if (missingAttr.Count > 0 && prof.Tabs == null)
                    {
                        //Evaluate condition for this section   
                        if (ValidateConditions(conditionList, mappedProf))
                        {
                            var sec = new Section { Questions = (mappedProf) };
                            var sectTitle = profTextContent.FirstOrDefault(x => x.key == sq.titlekey);
                            sec.SectionTitle = sectTitle == null ? "" : sectTitle.shorttext;
                            var sectSubTitle = profTextContent.FirstOrDefault(x => x.key == sq.subtitlekey);
                            sec.SectionSubtitle = sectSubTitle == null ? "" : sectSubTitle.shorttext;
                            var sectDesc = profTextContent.FirstOrDefault(x => x.key == sq.descriptionkey);
                            sec.SectionDescription = sectDesc == null ? "" : sectDesc.longtext;
                            var sectMark = profTextContent.FirstOrDefault(x => x.key == sq.introtextkey);
                            sec.SectionMarkdown = sectMark == null ? "" : sectMark.longtext;
                            var imgContent = profFileContent.FirstOrDefault(x => x.key == sq.images);
                            if (imgContent != null)
                            {
                                sec.ImageUrl = imgContent.mediumfile;
                                sec.ImageTitle = imgContent.mediumfiletitle;
                            }
                            s.Add(sec);

                            var tab = new ProfileTab { Sections = s };
                            t.Add(tab);


                            prof.Tabs = t;
                            countLock = count - 1 - (sectionList.Count - countSection);

                            conditions.AddRange(conditionList.Select(c => c.profileattributekey));
                        }
                    }
                    //Evaluate condition
                    if (!ValidateConditions(conditionList, mappedProf))
                    {
                        countSection -= 1;
                    }
                }
            }

            //All sections complete-- no questions selected
            if (countLock == 0 && prof.Tabs == null && sectionList.Count > 0)
            {
                prof.Completeness = 100;
            }
            else
            {
                if (countSection != 0)
                {
                    prof.Completeness = (countLock * 100) / countSection;
                }
            }

            if (prof.Completeness == 100)
            {
                var ca = Dashboard.LayoutFramework.Layout.CombineAttributes(profileResponse);
                if (ca != null)
                {
                    if (ca.FirstOrDefault(x => x.Key == "shortprofilecompleteddate") == null)
                    {
                        var quest = new Question
                        {
                            Key = "shortprofilecompleteddate",
                            Answer = DateTime.UtcNow.ToString("s"),
                            Level = "premise"
                        };
                        var pm = new ProfileModels();
                        pm.WriteProfileAttributes(parameters, quest);
                       
                        var eventAttribute = new Dictionary<string, string> { { "TabId", tabKey } };
                        var etm = new EventTrackingModels();
                        JsonConvert.DeserializeObject(etm.PostEvents(eventAttribute, parameters, "Finish Profile"));
                    }
                }
                prof.Tabs = null;
            }
            prof.CompletenessTitle = CMS.GetWidgetContent(profTextContent, tabKey, WidgetType, "completetitle");
            prof.CompletenessSubtitle = CMS.GetWidgetContent(profTextContent, tabKey, WidgetType, "completesubtitle");
            prof.CompletenessDescription = CMS.GetWidgetContent(profTextContent, tabKey, WidgetType, "completemessage");
            prof.CompletenessLabel = CMS.GetWidgetContent(profTextContent, tabKey, WidgetType, "completenesslabel");
            prof.NextButtonLabel = CMS.GetWidgetContent(profTextContent, tabKey, WidgetType, "nextbuttonlabel");
            prof.NotCompleteMessage = CMS.GetWidgetContent(profTextContent, tabKey, WidgetType, "notcompletemessage");
            prof.LoadingMessage = content.TextContent.FirstOrDefault(x => x.category == "common" && x.key == "common.loadingmessage").shorttext;
            prof.DropDownSelectText = content.TextContent.FirstOrDefault(x => x.category == "common" && x.key == "common.selectfordropdown").shorttext;
            prof.RequiredKey = CMS.GetWidgetContent(profTextContent, tabKey, WidgetType, "requiredkey");
            prof.RequiredSymbol = CMS.GetWidgetContent(profTextContent, tabKey, WidgetType, "requiredsymbol");
            prof.LongProfileLinkText = CMS.GetWidgetContent(profTextContent, tabKey, WidgetType, "longprofilelinktext");

            prof.ShowSectionIntro = CMS.GetWidgetConfig(profConfig, tabKey, WidgetType, "showsectionintro");
            prof.ShowRequiredKey = CMS.GetWidgetConfig(profConfig, tabKey, WidgetType, "showrequiredkey");
            prof.ShowLongProfileLink = CMS.GetWidgetConfig(profConfig, tabKey, WidgetType, "showlongprofilelink");
            prof.LongProfileLink = GeneralHelper.CreateLink("internal", jp, CMS.GetWidgetConfig(profConfig, tabKey, WidgetType, "longprofilelink"));

            // remove the condition attribute from the final list.
            if (prof.Tabs != null)
            {
                foreach (var c in conditions)
                {
                    prof.Tabs[0].Sections[0].Questions.Remove(prof.Tabs[0].Sections[0].Questions.FirstOrDefault(r => r.Key == c));
                }
            }

            //check if Completeness Description has links
            if (string.IsNullOrEmpty(prof.CompletenessDescription)) return prof;
            var regex = new Regex(@"\[([^\[\]]+)\]\(([^\(\)]+)\)", RegexOptions.IgnoreCase);
            var matches = regex.Matches(prof.CompletenessDescription);
            if (matches.Count <= 0) return prof;
            foreach (Match match in matches)
            {
                if (match.Groups[2].Value.Contains("id=") || match.Groups[2].Value.Contains("type=pm") || match.Groups[2].Value.Contains("type=external"))
                {
                    var link = GeneralHelper.CreateLink("", jp, match.Groups[2].Value);
                    prof.CompletenessDescription = prof.CompletenessDescription.Replace(match.Groups[2].Value,
                        link);
                }
            }

            return prof;
        }

        public Profile GetLongProfile(string tabKey, string profileTabKey, string parameters)
        {
            var jp = (JsonParams)JsonConvert.DeserializeObject(QueryStringModule.Decrypt(parameters), typeof(JsonParams));
            var content = CMS.GetContent(int.Parse(jp.cl), jp.l).Content;
            var widgetKey = content.Widget.Where(x => x.widgettype == WidgetTypeLong && x.tabkey == tabKey).FirstOrDefault().key;
            var tabs = content.Tab.Where(x => x.tabtype == "widget" && x.widgetkey == widgetKey).OrderBy(x => x.menuorder).ToList();
            var profTextContent = content.TextContent.Where(x => x.category == WidgetType).ToList();
            var profFileContent = content.FileContent.Where(x => x.category == "action").ToList();
            var profConfig = content.Configuration.Where(x => x.category == WidgetType).ToList();
            var prof = new Profile(profConfig, profTextContent, tabKey, WidgetTypeLong, jp);
            var profileResponse = GetProfileAttributesFromCache(jp);

            //Check in tablist if profileTabkey exists, if not pick the first tab key and assign it to profileTabkey
            if (tabs.FirstOrDefault(x => x.key == profileTabKey) == null)
            {
                if (tabs.Count > 0)
                {
                    profileTabKey = tabs.First().key;
                }
            }
            prof.OverviewTabId = CMS.GetWidgetConfig(profConfig, tabKey, WidgetTypeLong, "overviewtabid");
            var t = new List<ProfileTab>();
            foreach (var ta in tabs)
            {
                var tab = new ProfileTab { Key = ta.key };
                var name = content.TextContent.FirstOrDefault(x => x.category == "common" && x.key == ta.namekey);
                tab.Name = (name == null) ? "" : name.shorttext;
                tab.IconClass = ta.tabiconclass;
                t.Add(tab);
                prof.Tabs = t;

                if (profileTabKey == tab.Key || profileTabKey == prof.OverviewTabId)
                {
                    // loop through tabs and get sections
                    //Get section from content
                    var profSec = ta.profilesections.Split(';');
                    var sectionList = (from s in profSec where !string.IsNullOrEmpty(s) select content.ProfileSection.FirstOrDefault(x => x.key == s)).ToList();

                    sectionList = sectionList.OrderBy(x => x.rank).ToList();

                    foreach (var sq in sectionList)
                    {
                        var conditionList = new List<Condition>();
                        // Condition from section
                        if (sq.conditions.Length > 0)
                        {
                            var seq = sq.conditions.Split(';');
                            conditionList.AddRange(seq.Select(i => content.Condition.FirstOrDefault(x => x.key == i)));
                        }

                        var sqSplit = new List<string>(sq.profileattributes.Split(';'));
                        //Create question list                  
                        var qm = new QuestionMerge
                        {
                            Attributes = (from pa in content.ProfileAttribute.AsQueryable()
                                          where sqSplit.Any(key => pa.key.Equals(key))
                                          select pa).OrderBy(x => sqSplit.IndexOf(x.key)).ToList()
                        };

                        var conditionAttrToBeRemoved = new List<string>();
                        // Add condition attribute to get answer.
                        foreach (var c in conditionList.Where(c => c.key != null))
                        {
                            if (qm.Attributes.FirstOrDefault(x => x.key == c.profileattributekey) != null)
                                continue;
                            qm.Attributes.Add((from pa in content.ProfileAttribute.AsQueryable()
                                               where pa.key == c.profileattributekey
                                               select pa).FirstOrDefault());
                            conditionAttrToBeRemoved.Add(c.profileattributekey);
                        }
                        qm.Options = content.ProfileOption;
                        qm.Text = profTextContent;
                        qm.Profile = profileResponse;
                        qm.TabKey = tabKey;
                        qm.IsLongProfile = true;

                        var q = new List<Question>();
                        var s = new List<Section>();
                        Mapper.CreateMap<QuestionMerge, List<Question>>().ConvertUsing(new QuestionConverter());
                        var mappedProf = Mapper.Map(qm, q);

                        var sec = new Section();
                        var sectTitle = profTextContent.FirstOrDefault(x => x.key == sq.titlekey);
                        sec.SectionTitle = sectTitle == null ? "" : sectTitle.shorttext;
                        var sectSubtitle = profTextContent.FirstOrDefault(x => x.key == sq.subtitlekey);
                        sec.SectionSubtitle = sectSubtitle == null ? "" : sectSubtitle.shorttext;
                        var sectDesc = profTextContent.FirstOrDefault(x => x.key == sq.descriptionkey);
                        sec.SectionDescription = sectDesc == null ? "" : sectDesc.longtext;
                        var sectMark = profTextContent.FirstOrDefault(x => x.key == sq.introtextkey);
                        sec.SectionMarkdown = sectMark == null ? "" : sectMark.longtext;
                        sec.Key = sq.key;

                        var imgContent = profFileContent.FirstOrDefault(x => x.key == sq.images);
                        if (imgContent != null)
                        {
                            sec.ImageUrl = imgContent.mediumfile;
                            sec.ImageTitle = imgContent.mediumfiletitle;
                        }

                        if (!ValidateConditions(conditionList, mappedProf)) continue;
                        {
                            sec.Questions = mappedProf;
                            foreach (var c in conditionAttrToBeRemoved)
                            {
                                sec.Questions.Remove(sec.Questions.FirstOrDefault(r => r.Key == c));
                            }

                            if (prof.Tabs.FirstOrDefault(x => x.Key == ta.key).Sections == null)
                            {
                                s.Add(sec);
                                prof.Tabs.FirstOrDefault(x => x.Key == ta.key).Sections = s;
                            }
                            else
                            {
                                prof.Tabs.FirstOrDefault(x => x.Key == ta.key).Sections.Add(sec);
                            }
                        }
                    }
                }
            }
            prof.NextButtonLabel = CMS.GetWidgetContent(profTextContent, tabKey, WidgetTypeLong, "nextbuttonlabel");
            prof.NotCompleteMessage = CMS.GetWidgetContent(profTextContent, tabKey, WidgetTypeLong, "notcompletemessage");
            prof.BackButtonLabel = CMS.GetWidgetContent(profTextContent, tabKey, WidgetTypeLong, "backbuttonlabel");
            prof.LoadingMessage = content.TextContent.FirstOrDefault(x => x.category == "common" && x.key == "common.loadingmessage").shorttext;
            prof.DropDownSelectText = content.TextContent.FirstOrDefault(x => x.category == "common" && x.key == "common.selectfordropdown").shorttext;
            prof.OverviewText = CMS.GetWidgetContent(profTextContent, tabKey, WidgetTypeLong, "overviewtextkey");
            prof.SaveButtonLabel = CMS.GetWidgetContent(profTextContent, tabKey, WidgetTypeLong, "savebuttonlabel");
            prof.PageOf = CMS.GetWidgetContent(profTextContent, tabKey, WidgetTypeLong, "pageof");
            prof.SavedMessage = CMS.GetWidgetContent(profTextContent, tabKey, WidgetTypeLong, "savedmessage");

            prof.ShowSectionIntro = CMS.GetWidgetConfig(profConfig, tabKey, WidgetTypeLong, "showsectionintro");
            prof.ShowOverviewTab = CMS.GetWidgetConfig(profConfig, tabKey, WidgetTypeLong, "showoverviewtab");
            prof.ShowOverviewTabColumnOneCount = CMS.GetWidgetConfig(profConfig, tabKey, WidgetTypeLong, "overviewtabcolumnonecount");

            if (!string.IsNullOrEmpty(prof.ShowOverviewTab) && prof.ShowOverviewTab == "false")
            {
                prof.Tabs.Remove(prof.Tabs.FirstOrDefault(r => r.Key == prof.OverviewTabId));
            }

            return prof;
        }

        public void PostProfile(Profile profile, string parameters, string tabKey = "", string sectionKey = "")
        {
            var jp = (JsonParams)JsonConvert.DeserializeObject(QueryStringModule.Decrypt(parameters), typeof(JsonParams));
            Section sec;
            if (tabKey.Length != 0 && sectionKey.Length != 0)
            {
                sec = profile.Tabs.FirstOrDefault(x => x.Key == tabKey).Sections.FirstOrDefault(x => x.Key == sectionKey);
            }
            else
            {
                sec = profile.Tabs[0].Sections == null ? profile.Tabs[1].Sections[0] : profile.Tabs[0].Sections[0];
            }
            if (sec == null) return;

            var questCustomer = sec.Questions.Where(x => x.Level == "customer").ToList();
            var questAccount = sec.Questions.Where(x => x.Level == "account").ToList();
            var questPremise = sec.Questions.Where(x => x.Level == "premise").ToList();

            var isCsr = ViewPageExtensions.ValidateCsr(jp.u, jp.g, jp.r, jp.w);
            var author = "customer";
            if (isCsr)
            {
                author = "csr";
            }

            var custAttr = new List<Common.AclaraJson.Attribute>();
            foreach (var qc in questCustomer)
            {
                var cAttr = new Common.AclaraJson.Attribute();
                if (string.IsNullOrEmpty(qc.Answer)) continue;
                cAttr.AttributeKey = qc.Key;
                cAttr.AttributeValue = qc.Answer;
                cAttr.SourceKey = author;
                custAttr.Add(cAttr);
            }
            var newCust = new ProfileCustomer
            {
                Id = jp.c,
                Attributes = custAttr
            };

            var acctAttr = new List<Common.AclaraJson.Attribute>();
            foreach (var qa in questAccount)
            {
                var aAttr = new Common.AclaraJson.Attribute();
                if (string.IsNullOrEmpty(qa.Answer)) continue;
                aAttr.AttributeKey = qa.Key;
                aAttr.AttributeValue = qa.Answer;
                aAttr.SourceKey = author;
                acctAttr.Add(aAttr);
            }
            var newAcct = new ProfileAccount
            {
                Id = jp.a,
                Attributes = acctAttr
            };
            var newAcctList = new List<ProfileAccount> { newAcct };
            newCust.Accounts = newAcctList;

            var premAttr = new List<Common.AclaraJson.Attribute>();
            foreach (var qp in questPremise)
            {
                var pAttr = new Common.AclaraJson.Attribute();
                if (string.IsNullOrEmpty(qp.Answer)) continue;
                pAttr.AttributeKey = qp.Key;
                pAttr.AttributeValue = qp.Answer;
                pAttr.SourceKey = author;
                premAttr.Add(pAttr);
            }
            var newPrem = new ProfilePremis
            {
                Id = jp.p,
                Attributes = premAttr
            };
            var newPremList = new List<ProfilePremis> { newPrem };
            newCust.Accounts[0].Premises = newPremList;

            var postStr1 = @"{ " +
                           "\"Customer\": " + JsonConvert.SerializeObject(newCust) +
                           "}";

            var rm = new APIResponseModels();
            rm.PostProfile(postStr1, "en-US", Convert.ToInt32(jp.cl));

            UpdateProfileAttributesInCache(jp);
        }

        public static bool ValidateConditions(List<Condition> conditionList, List<Question> mappedProf)
        {
            if (conditionList.Count <= 0) return true;
            foreach (var c in conditionList)
            {
                var quest = mappedProf.FirstOrDefault(x => x.Key == c.profileattributekey);
                if (c.profileattributekey == PremiseTypeAttributeKey && quest == null && c.profileoptionkey == PremiseTypeResidentialOption)
                {
                    return true;
                }
                if (c.profileattributekey == PremiseTypeAttributeKey && quest == null && c.profileoptionkey == PremiseTypeBusinessOption)
                {
                    return false;
                }
                var answer = string.Empty;
                if (quest != null && quest.Answer.Length > 0)
                {
                    answer = quest.Answer;
                }
                if (c.value.Length == 0)
                {
                    switch (c.@operator)
                    {
                        case "=":
                            if (answer != c.profileoptionkey)
                            {
                                return false;
                            }
                            break;
                        case "!=":
                            if (answer == c.profileoptionkey)
                            {
                                return false;
                            }
                            break;
                        default:
                            if (Convert.ToBoolean(new NCalc.Expression(Convert.ToDecimal(answer) + c.@operator + Convert.ToDecimal(c.value)).Evaluate()) == false)
                            {
                                return false;
                            }
                            break;
                    }
                }
                else
                {
                    switch (c.@operator)
                    {
                        case "IN":
                            if (Convert.ToBoolean(new NCalc.Expression("in(" + answer + "," + c.value + ")").Evaluate()) == false)
                            {
                                return false;
                            }
                            break;
                        case "NOTIN":
                            if (Convert.ToBoolean(new NCalc.Expression("not in(" + answer + "," + c.value + ")").Evaluate()) == false)
                            {
                                return false;
                            }
                            break;
                        default:
                            if (answer == string.Empty)
                            {
                                var expr = new NCalc.Expression("a" + c.@operator + "b");
                                expr.EvaluateParameter += (name, args) =>
                                {
                                    if (name == "a") args.Result = answer;
                                    if (name == "b") args.Result = c.value;
                                };
                                if (Convert.ToBoolean(expr.Evaluate()) == false)
                                {
                                    return false;
                                }
                            }
                            else
                            {
                                if (Convert.ToBoolean(new NCalc.Expression(Convert.ToDecimal(answer) + c.@operator + Convert.ToDecimal(c.value)).Evaluate()) == false)
                                {
                                    return false;
                                }
                            }
                            break;
                    }
                }
            }
            return true;
        }

        public void WriteProfileAttributes(string parameters, Question q)
        {
            var prof = new Profile();
            var tabs = new List<ProfileTab>();
            var secs = new List<Section>();
            var quests = new List<Question>();

            var tab = new ProfileTab();
            var sec = new Section();

            quests.Add(q);
            sec.Questions = quests;
            secs.Add(sec);
            tab.Sections = secs;
            tabs.Add(tab);
            prof.Tabs = tabs;

            PostProfile(prof, parameters);
        }

        public ProfileRoot GetProfileAttributesFromCache(JsonParams jp)
        {
            ProfileRoot profileResponse;
            if (HttpRuntime.Cache["ProfileAttributes" + jp.cl + "" + jp.c + "" + jp.a + "" + jp.p + "" + jp.l + "" + jp.w] == null)
            {
                var rm = new APIResponseModels();
                var requestParams = "CustomerId=" + jp.c + "&AccountId=" + jp.a + "&PremiseId=" + jp.p + "&IncludeContent=false&IncludeMissing=false";
                profileResponse = (ProfileRoot)JsonConvert.DeserializeObject(rm.GetProfile(requestParams, jp.l, int.Parse(jp.cl)).Content, typeof(ProfileRoot));
                HttpRuntime.Cache.Insert("ProfileAttributes" + jp.cl + "" + jp.c + "" + jp.a + "" + jp.p + "" + jp.l + "" + jp.w, profileResponse,
                                        null, DateTime.Now.AddMinutes(Constants.kCache_Expiry), Cache.NoSlidingExpiration, CacheItemPriority.NotRemovable, null);
            }
            else
            {
                profileResponse = (ProfileRoot)HttpRuntime.Cache["ProfileAttributes" + jp.cl + "" + jp.c + "" + jp.a + "" + jp.p + "" + jp.l + "" + jp.w];
            }

            return profileResponse;
        }

        public ProfileRoot GetProfileAttributes(JsonParams jp)
        {
            var rm = new APIResponseModels();
            var requestParams = "CustomerId=" + jp.c + "&AccountId=" + jp.a + "&PremiseId=" + jp.p + "&IncludeContent=false&IncludeMissing=false";
            var profileResponse = (ProfileRoot)JsonConvert.DeserializeObject(rm.GetProfile(requestParams, jp.l, int.Parse(jp.cl)).Content, typeof(ProfileRoot));
            HttpRuntime.Cache.Insert("ProfileAttributes" + jp.cl + "" + jp.c + "" + jp.a + "" + jp.p + "" + jp.l + "" + jp.w, profileResponse, null, DateTime.Now.AddMinutes(Constants.kCache_Expiry), Cache.NoSlidingExpiration, CacheItemPriority.NotRemovable, null);

            profileResponse = (ProfileRoot)HttpRuntime.Cache["ProfileAttributes" + jp.cl + "" + jp.c + "" + jp.a + "" + jp.p + "" + jp.l + "" + jp.w];
            return profileResponse;
        }

        public void UpdateProfileAttributesInCache(JsonParams jp)
        {
            try
            {
                var requestParams = "CustomerId=" + jp.c + "&AccountId=" + jp.a + "&PremiseId=" + jp.p +
                                    "&IncludeContent=false&IncludeMissing=false";
                var rm = new APIResponseModels();
                var profileResponse =
                    (ProfileRoot)JsonConvert.DeserializeObject(
                        rm.GetProfile(requestParams, jp.l, Convert.ToInt32(jp.cl)).Content, typeof(ProfileRoot));
                HttpRuntime.Cache.Insert(
                    "ProfileAttributes" + jp.cl + "" + jp.c + "" + jp.a + "" + jp.p + "" + jp.l + "" + jp.w,
                    profileResponse, null, DateTime.Now.AddMinutes(Constants.kCache_Expiry), Cache.NoSlidingExpiration,
                    CacheItemPriority.NotRemovable, null);
            }
            catch (Exception)
            {
                // do something. 
            }
        }
    }

    public class QuestionConverter : ITypeConverter<QuestionMerge, List<Question>>
    {
        public List<Question> Convert(ResolutionContext context)
        {
            var sourceCollection = (QuestionMerge)context.SourceValue;

            Mapper.CreateMap<QuestionMerge, List<Question>>();
            Mapper.CreateMap<ProfileAttribute, Question>()
                .ForMember(dest => dest.OptionValue, opt => opt.MapFrom(src => src.optionkeys))
                .ForMember(dest => dest.Min, opt => opt.MapFrom(src => src.minvalue))
                .ForMember(dest => dest.Max, opt => opt.MapFrom(src => src.maxvalue))
                .ForMember(dest => dest.Key, opt => opt.MapFrom(src => src.key))
                .ForMember(dest => dest.Level, opt => opt.MapFrom(src => src.entitylevel));

            var ql = new List<Question>();
            foreach (var a in sourceCollection.Attributes)
            {
                var q = EntityMapper.Map<Question>(a);
                if (a.optionkeys != null)
                {
                    var opt = new Option();
                    var options = a.optionkeys.Split(',');
                    var optionText = string.Empty;
                    var oVal = new List<string>();
                    var oTxt = new List<string>();
                    foreach (var o in options)
                    {
                        var option = sourceCollection.Options.FirstOrDefault(x => x.key == o);
                        if (option == null) continue;
                        {
                            var text = sourceCollection.Text.FirstOrDefault(x => x.key == option.namekey);
                            optionText += text == null ? "" : text.shorttext + ",";
                            oTxt.Add(text == null ? "" : text.shorttext);
                            oVal.Add(o);
                        }
                    }
                    q.OptionText = optionText;
                    opt.Value = oVal;
                    opt.Text = oTxt;
                    q.Options = opt;
                }

                if (a.type == "text")
                {
                    var replaceText = sourceCollection.IsLongProfile
                        ? sourceCollection.Text.FirstOrDefault(x => x.key == sourceCollection.TabKey + ".longprofile.invalidlengthmessage")
                        : sourceCollection.Text.FirstOrDefault(x => x.key == sourceCollection.TabKey + ".profile.invalidlengthmessage");
                    q.InvalidLengthMessage =
                        replaceText?.shorttext.Replace("{%minvalue%}", a.minvalue.ToString())
                            .Replace("{%maxvalue%}", a.maxvalue.ToString()) ?? "";
                }
                else if (a.type == "decimal" || a.type == "integer")
                {
                    var replaceText = sourceCollection.IsLongProfile
                        ? sourceCollection.Text.FirstOrDefault(x => x.key == sourceCollection.TabKey + ".longprofile.outofrangemessage")
                        : sourceCollection.Text.FirstOrDefault(x => x.key == sourceCollection.TabKey + ".profile.outofrangemessage");
                    q.RangeErrorMessage =
                        replaceText?.shorttext.Replace("{%minvalue%}", a.minvalue.ToString())
                            .Replace("{%maxvalue%}", a.maxvalue.ToString()) ?? "";

                    var opt = new Option();
                    var oVal = new List<string>();
                    var oTxt = new List<string>();
                    if (int.Parse(q.Max) > 1 && int.Parse(q.Max) < 51)
                    {
                        for (var e = int.Parse(q.Min); e <= int.Parse(q.Max); e++)
                        {
                            oTxt.Add(e.ToString());
                            oVal.Add(e.ToString());
                        }
                    }

                    opt.Value = oVal;
                    opt.Text = oTxt;
                    q.Options = opt;
                }

                if (a.type == "decimal" && a.maxvalue == 1 && q.OptionText.Length == 0)
                {
                    var textYes = sourceCollection.Text.FirstOrDefault(x => x.key == "profile.option.yes.name");
                    q.OptionText += textYes == null ? "" : textYes.shorttext + ",";
                    var textNo = sourceCollection.Text.FirstOrDefault(x => x.key == "profile.option.no.name");
                    q.OptionText += textNo == null ? "" : textNo.shorttext;

                    q.OptionValue = "1,0";
                    var opt = new Option();
                    var oVal = new List<string>();
                    var oTxt = new List<string> { textYes == null ? "" : textYes.shorttext };
                    oVal.Add("1");
                    oTxt.Add(textNo == null ? "" : textNo.shorttext);
                    oVal.Add("0");
                    opt.Value = oVal;
                    opt.Text = oTxt;
                    q.Options = opt;
                }
                if (sourceCollection.Profile.Customer != null)
                {
                    var answer = sourceCollection.Profile.Customer.Attributes.FirstOrDefault(c => c.AttributeKey == a.key);
                    if (answer == null)
                    {
                        answer = sourceCollection.Profile.Customer.Accounts[0].Attributes.FirstOrDefault(c => c.AttributeKey == a.key);
                        if (answer == null)
                        {
                            if (sourceCollection.Profile.Customer.Accounts[0].Premises.Count > 0)
                            {
                                answer = sourceCollection.Profile.Customer.Accounts[0].Premises[0].Attributes.FirstOrDefault(c => c.AttributeKey == a.key);
                            }
                        }
                    }

                    if (answer == null)
                    {
                        q.Answer = "";
                    }
                    else
                    {
                        q.Answer = answer.AttributeValue ?? "";
                    }
                }
                else { q.Answer = ""; }
                q.Label = sourceCollection.Text.Where(x => x.key == a.questiontextkey).FirstOrDefault().shorttext;
                ql.Add(q);
            }
            return ql;
        }
    }

    public class QuestionMerge
    {
        public List<ProfileAttribute> Attributes { get; set; }
        public List<ProfileOption> Options { get; set; }
        public List<TextContent> Text { get; set; }
        public ProfileRoot Profile { get; set; }
        public string TabKey { get; set; }
        public bool IsLongProfile { get; set; }
    }

    public class Question
    {
        public string Key { get; set; }
        public string Label { get; set; }
        public string Tooltip { get; set; }
        public string Type { get; set; }
        public string OptionText { get; set; }
        public string OptionValue { get; set; }
        public string Min { get; set; }
        public string Max { get; set; }
        public string Answer { get; set; }
        public string Level { get; set; }
        public string RangeErrorMessage { get; set; }
        public string InvalidLengthMessage { get; set; }
        public bool InvalidRequired { get; set; }
        public bool InvalidRange { get; set; }
        public bool InvalidLength { get; set; }
        public Option Options { get; set; }
    }

    public class Profile : ContentBase
    {
        public int Completeness { get; set; }
        public string CompletenessTitle { get; set; }
        public string CompletenessSubtitle { get; set; }
        public string CompletenessDescription { get; set; }
        public string CompletenessLabel { get; set; }
        public string NextButtonLabel { get; set; }
        public string NotCompleteMessage { get; set; }
        public string BackButtonLabel { get; set; }
        public string DropDownSelectText { get; set; }
        public string ShowSectionIntro { get; set; }
        public string LoadingMessage { get; set; }
        public string OverviewTabId { get; set; }
        public string RequiredKey { get; set; }
        public string RequiredSymbol { get; set; }
        public string LongProfileLinkText { get; set; }
        public string ShowRequiredKey { get; set; }
        public string ShowLongProfileLink { get; set; }
        public string LongProfileLink { get; set; }
        public string ShowOverviewTab { get; set; }
        public string ShowOverviewTabColumnOneCount { get; set; }
        public string OverviewText { get; set; }
        public string SaveButtonLabel { get; set; }
        public string PageOf { get; set; }
        public string SavedMessage { get; set; }
        public List<ProfileTab> Tabs { get; set; }
        public Profile()
        { }
        public Profile(List<Common.Configuration> config, List<TextContent> content, string tabKey, string widetType, JsonParams jp) : base(config, content, tabKey, widetType, jp)
        { }
    }

    public class Section
    {
        public string Key { get; set; }
        public string SectionTitle { get; set; }
        public string SectionSubtitle { get; set; }
        public string SectionDescription { get; set; }
        public string SectionMarkdown { get; set; }
        public string ImageUrl { get; set; }
        public string ImageTitle { get; set; }
        public List<Question> Questions { get; set; }
    }

    public class ProfileTab
    {
        public string Key { get; set; }
        public string Name { get; set; }
        public string IconClass { get; set; }
        public List<Section> Sections { get; set; }
    }

    public class Option
    {
        public List<string> Value { get; set; }
        public List<string> Text { get; set; }
    }
}