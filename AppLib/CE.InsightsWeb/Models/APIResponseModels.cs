﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using System.Web.Caching;
using CE.InsightsIntegration;
using CE.InsightsIntegration.Models;
using CE.InsightsWeb.Common;
using RestSharp;

namespace CE.InsightsWeb.Models
{
    public class APIResponseModels
    {
        private const string AppSetting_CEEnvironment = "CEEnvironment";
        private const string AppSetting_CEEnvironmentLocal = "localdev";
        private const string AppSetting_CEEnvironmentDev = "dev";
        private const string AppSetting_CEEnvironmentQa = "qa";
        private const string AppSetting_CEEnvironmentUat = "uat";
        private const string AppSetting_CEEnvironmentProd = "prod";
        private const string POST = "POST";
        private const string GET = "GET";
        private const string DELETE = "DELETE";
        private const string AppSetting_EventTrackingKey = "SasKey";
        private const string AppSetting_EventTrackingURL = "EventTrackingUrl";

        public Response GetEcho(string parameters, string locale, int clientId)
        {
            var query = new QueryTool {LocaleHeader = locale};
            query = GetCurrentEnvironmentKeys(query, clientId);
            query.Parameters = parameters;
            var str = API.GetEchoResponse(query);
            return str;
        }

        public Response GetContent(string parameters, string locale, int clientId)
        {
            var query = new QueryTool {LocaleHeader = locale};
            query = GetCurrentEnvironmentKeys(query, clientId);
            query.Parameters = parameters;
            var str = API.GetContentResponse(query);
            return str;
        }

        public RestSharp.IRestResponse GetToken(string authorization, string code)
        {
            var client = new RestClient("https://localhost/CE.Insights/api/v1/oauth/token");
            var request = new RestRequest(Method.POST);

            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Authorization", "Basic " + authorization);

            request.AddJsonBody(new { grant_type = "authorization_code", code = code });
            request.RequestFormat = DataFormat.Json;

            return client.Execute(request);
        }

        public Response GetConsumption(string parameters, string locale, int clientId)
        {
            var query = new QueryTool {LocaleHeader = locale};
            query = GetCurrentEnvironmentKeys(query, clientId);
            query.Parameters = parameters;
            var str = API.GetConsumptionResponse(query);
            return str;
        }

        public Response GetBillDisagg(string parameters, string locale, int clientId)
        {
            var query = new QueryTool {LocaleHeader = locale};
            query = GetCurrentEnvironmentKeys(query, clientId);
            query.Parameters = parameters;
            var str = API.GetBillDisaggResponse(query);
            return str;
        }

        public Response PostProfile(string parameters, string locale, int clientId)
        {
            var query = new QueryTool {LocaleHeader = locale};
            query = GetCurrentEnvironmentKeys(query, clientId);
            query.Parameters = parameters;
            query.SelectedMethodApi = POST;
            var str = API.ProfileResponse(query);
            return str;
        }

        public Response GetProfile(string parameters, string locale, int clientId)
        {
            var query = new QueryTool {LocaleHeader = locale};
            query = GetCurrentEnvironmentKeys(query, clientId);
            query.Parameters = parameters;
            query.SelectedMethodApi = GET;
            var str = API.ProfileResponse(query);
            return str;
        }

        public Response PostWebToken(string parameters, string locale, int clientId)
        {
            var query = new QueryTool {LocaleHeader = locale};
            query = GetCurrentEnvironmentKeys(query, clientId);
            query.Parameters = parameters;
            query.SelectedMethodApi = POST;
            var rc = CMS.GetContent(clientId, locale);
            query.ApiVersion = CMS.GetCommonContent(rc, "webtokenapiversion", "config");
            var str = API.WebTokenResponse(query);
            return str;
        }

        public Response GetWebToken(string parameters, string locale, int clientId)
        {
            var query = new QueryTool {LocaleHeader = locale};
            query = GetCurrentEnvironmentKeys(query, clientId);
            query.Parameters = parameters;
            query.SelectedMethodApi = GET;
            var rc = CMS.GetContent(clientId, locale);
            query.ApiVersion = CMS.GetCommonContent(rc, "webtokenapiversion", "config");
            var str = API.WebTokenResponse(query);
            return str;
        }

        public Response DeleteWebToken(string parameters, string locale, int clientId)
        {
            var query = new QueryTool {LocaleHeader = locale};
            query = GetCurrentEnvironmentKeys(query, clientId);
            query.Parameters = parameters;
            query.SelectedMethodApi = DELETE;
            var str = API.WebTokenResponse(query);
            return str;
        }

        public Response PostActionPlan(string parameters, string locale, int clientId)
        {
            var query = new QueryTool {LocaleHeader = locale};
            query = GetCurrentEnvironmentKeys(query, clientId);
            query.Parameters = parameters;
            query.SelectedMethodApi = POST;
            var str = API.ActionPlanResponse(query);
            return str;
        }

        public Response GetActionPlan(string parameters, string locale, int clientId)
        {
            var query = new QueryTool {LocaleHeader = locale};
            query = GetCurrentEnvironmentKeys(query, clientId);
            query.Parameters = parameters;
            query.SelectedMethodApi = GET;
            var str = API.ActionPlanResponse(query);
            return str;
        }

        public Response GetAction(string parameters, string locale, int clientId)
        {
            var query = new QueryTool {LocaleHeader = locale};
            query = GetCurrentEnvironmentKeys(query, clientId);
            query.Parameters = parameters;
            var str = API.ActionResponse(query);
            return str;
        }

        public Response GetBill(string parameters, string locale, int clientId, string version)
        {
            var query = new QueryTool {LocaleHeader = locale};
            query = GetCurrentEnvironmentKeys(query, clientId);
            query.Parameters = parameters;
            query.ApiVersion = version == "" ? "v1" : version; 
            var str = API.BillResponse(query);
            return str;
        }


        public async Task<Response> PostEventsAsync(string parameters, string locale, int clientId)
        {
            var query = new QueryTool
            {
                LocaleHeader = locale,
                Parameters = parameters,
                BasicKey = ConfigurationManager.AppSettings.Get(AppSetting_EventTrackingKey),
                BaseUrl = ConfigurationManager.AppSettings.Get(AppSetting_EventTrackingURL)
            };
            var task = await Task.Factory.StartNew(() => API.EventTrackingResponse(query)).ConfigureAwait(false);
            return task;
        }

        public Response GetBenchmark(string parameters, string locale, int clientId)
        {
            var query = new QueryTool {LocaleHeader = locale};
            query = GetCurrentEnvironmentKeys(query, clientId);
            query.Parameters = parameters;
            var str = API.BenchmarkResponse(query);
            return str;
        }
   
        public Response GetBillToDate(string parameters, string locale, int clientId, string version)
        {
            var query = new QueryTool {LocaleHeader = locale};
            query = GetCurrentEnvironmentKeys(query, clientId);
            query.Parameters = parameters;
            query.ApiVersion = version == "" ? "v1" : version;
            var str = API.GetBillToDateResponse(query);
            return str;
        }

        public Response GetCustomerInfo(string parameters, string locale, int clientId)
        {
            var query = new QueryTool {LocaleHeader = locale};
            query = GetCurrentEnvironmentKeys(query, clientId);
            query.Parameters = parameters;
            var str = API.GetCustomerInfoResponse(query);
            return str;
        }

        public Response GetRate(string parameters, string locale, int clientId, string version)
        {
            var query = new QueryTool {LocaleHeader = locale};
            query = GetCurrentEnvironmentKeys(query, clientId);
            query.Parameters = parameters;
            query.ApiVersion = version == "" ? "v1" : version;
            var str = API.RateResponse(query);
            return str;
        }

        public Response GetSubscription(string parameters, string locale, int clientId, string version)
        {
            var query = new QueryTool
            {
                SelectedMethodApi = GET,
                LocaleHeader = locale
            };
            query = GetCurrentEnvironmentKeys(query, clientId);
            query.Parameters = parameters;
            query.ApiVersion = version == "" ? "v1" : version;
            var str = API.SubscriptionResponse(query);
            return str;
        }

        public Response PostSubscription(string parameters, string locale, int clientId, string version)
        {
            var query = new QueryTool
            {
                SelectedMethodApi = POST,
                LocaleHeader = locale
            };
            query = GetCurrentEnvironmentKeys(query, clientId);
            query.Parameters = parameters;
            query.ApiVersion = version == "" ? "v1" : version;
            var str = API.SubscriptionResponse(query);
            return str;
        }

        public Response PostModel(string parameters, string locale, int clientId)
        {
            var query = new QueryTool { LocaleHeader = locale };
            query = GetCurrentEnvironmentKeys(query, clientId);
            query.Parameters = parameters;
            query.SelectedMethodApi = POST;
            var str = API.ModelResponse(query);
            return str;
        }

        public Response GetGreenButtonBill(string parameters, string locale, int clientId)
        {
            var query = new QueryTool { LocaleHeader = locale };
            query = GetCurrentEnvironmentKeys(query, clientId);
            query.Parameters = parameters;
            var str = API.GreenButtonBillResponse(query);
            return str;
        }

        public Response GetGreenButtonAmi(string parameters, string locale, int clientId)
        {
            var query = new QueryTool { LocaleHeader = locale };
            query = GetCurrentEnvironmentKeys(query, clientId);
            query.Parameters = parameters;
            var str = API.GreenButtonAmiResponse(query);
            return str;
        }

        public Response GetReportsPdf(string parameters, string locale, int clientId)
        {
            var query = new QueryTool { LocaleHeader = locale };
            query = GetCurrentEnvironmentKeys(query, clientId);
            query.Parameters = parameters;
            var str = API.PdfReportsResponse(query);
            return str;
        }

        public Response GetReportPdf(string parameters, string locale, int clientId)
        {
            var query = new QueryTool { LocaleHeader = locale };
            query = GetCurrentEnvironmentKeys(query, clientId);
            query.Parameters = parameters;
            var str = API.PdfReportResponse(query);
            return str;
        }

        public Response DeleteReportPdf(string parameters, string locale, int clientId)
        {
            var query = new QueryTool { LocaleHeader = locale };
            query = GetCurrentEnvironmentKeys(query, clientId);
            query.Parameters = parameters;
            var str = API.PdfDeleteReportResponse(query);
            return str;
        }

        public Response PostReport(string parameters, Stream s,string fileName, string locale, int clientId)
        {
            var query = new QueryTool { LocaleHeader = locale };
            query = GetCurrentEnvironmentKeys(query, clientId);
            query.Parameters = parameters;
            var f = new FileDetail { ContentType = "application/pdf", FileName = fileName, FileStream = s, ParameterName = "File"};
            var fList = new List<FileDetail>() {f};
            query.FileDetailList = fList;
            var str = API.FileResponse(query);
            return str;
        }
        private static QueryTool GetCurrentEnvironmentKeys(QueryTool query, int clientId)
        {
            var s = ConfigurationManager.AppSettings.Get(AppSetting_CEEnvironment);
            if (!string.IsNullOrEmpty(s))
            {
                var env = 4;
                switch (s)
                {
                    case AppSetting_CEEnvironmentLocal:
                        env = 4;
                        break;
                    //query.Thumbprint = "6ADF4A438F28CF88DF75B20B15811F12B6D19395";
                    case AppSetting_CEEnvironmentDev:
                        env = 3;
                        break;
                    case AppSetting_CEEnvironmentQa:
                        env = 2;
                        break;
                    case AppSetting_CEEnvironmentUat:
                        env = 1;
                        break;
                    case AppSetting_CEEnvironmentProd:
                        env = 0;
                        break;
                }

                if (HttpRuntime.Cache[clientId + "env" + env] == null)
                {
                    //declare the transaction options
                    var transactionOptions = new System.Transactions.TransactionOptions
                    {
                        IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                    };
                    //set it to read uncommited

                    using (var transScope = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required, transactionOptions))
                    {
                        using (var ent = new InsightsEntities())
                        {
                            ent.Database.Connection.Open();
                            var envList = (from e in ent.UserEnvironments
                                           join ue in ent.Users on e.UserID equals ue.UserID
                                           where ue.ClientID == clientId && e.EnvID == env && ue.ActorName == "Web Users"
                                           select new { ue.CEAccessKeyID, e.BasicKey }).FirstOrDefault();

                            if (envList != null)
                            {
                                HttpRuntime.Cache.Insert(clientId + "env" + env, envList, null, DateTime.Now.AddMinutes(Constants.kCache_Expiry), Cache.NoSlidingExpiration, CacheItemPriority.NotRemovable, null);

                                query.selectedCEAccessKeyID = envList.CEAccessKeyID;
                                query.BasicKey = envList.BasicKey;
                            }
                            else
                            {
                                throw new Exception("ClientId:" + clientId + "EnvId:" + env);
                            }
                        }
                        transScope.Complete();
                    }
                }
                else
                {
                    var envList = HttpRuntime.Cache[clientId + "env" + env];
                    var type = envList.GetType();
                    IList<PropertyInfo> props = new List<PropertyInfo>(type.GetProperties());
                    query.selectedCEAccessKeyID = props[0].GetValue(envList, null).ToString();
                    query.BasicKey = props[1].GetValue(envList, null).ToString();
                }

                query.AuthType = 1;
            }
            query.BaseUrl = InsightsIntegration.Common.Helpers.GetBaseUrl_CEInsightsAPI();

            query.MessageIdHeader = "13";
            query.ChannelHeader = "CE";
            query.DateTime = DateTime.UtcNow;
            return query;
        }
    }
}