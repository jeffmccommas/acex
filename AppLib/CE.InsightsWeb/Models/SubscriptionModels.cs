﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using AutoMapper;
using CE.InsightsWeb.Common;
using CE.InsightsWeb.Common.AclaraJson;
using CE.InsightsWeb.Common.QueryStringEncryptor;
using CE.InsightsWeb.Helpers;
using Newtonsoft.Json;

namespace CE.InsightsWeb.Models
{
    public class SubscriptionModels
    {
        private const string SUBSCRIPTION_CATEGORY = "subscription";
        private const string COMMON_CATEGORY = "common";
        private const string WIDGET_TYPE_SUBSCRIPTION = "subscription";
        private const string ACTION_CATEGORY = "action";

        private const string ChannelEmailAndSms = "emailandsms";
        private const string ChannelEmail = "email";
        private const string ChannelSms = "sms";

        public Subscription GetSubscription(string tabKey, string parameters)
        {
            var jp = (JsonParams)JsonConvert.DeserializeObject(QueryStringModule.Decrypt(parameters), typeof(JsonParams));
            //Content
            var content = CMS.GetContent(int.Parse(jp.cl), jp.l).Content;
            var subscriptionTextContent = content.TextContent.Where(x => x.category == SUBSCRIPTION_CATEGORY || x.category == COMMON_CATEGORY).ToList();
            var subscriptionConfig = content.Configuration.Where(x => x.category == SUBSCRIPTION_CATEGORY || x.category == COMMON_CATEGORY).ToList();
            var actionContent = content.Action.Where(x => x.type == SUBSCRIPTION_CATEGORY).ToList();
            var actionFileContent = content.FileContent.Where(x => x.category == ACTION_CATEGORY).ToList();
            var uomContent = content.UOM.ToList();

            var subscriptionApiVersion = CMS.GetWidgetConfig(subscriptionConfig, tabKey, WIDGET_TYPE_SUBSCRIPTION, "subscriptionapiversion");
            var subscriptionResponse = GetSubscriptionFromCache(jp, subscriptionApiVersion);
            var s = new Subscription(subscriptionConfig, subscriptionTextContent, tabKey, WIDGET_TYPE_SUBSCRIPTION, jp);
            if (subscriptionResponse.Customer == null)
            {
                s.NoSubscription = true;
                s.NoData = CMS.GetWidgetContent(subscriptionTextContent, tabKey, WIDGET_TYPE_SUBSCRIPTION, "nodata");
                return s;
            }
            s.Insights = CMS.GetWidgetConfig(subscriptionConfig, tabKey, WIDGET_TYPE_SUBSCRIPTION, "insights");
            s.ServiceCount = subscriptionResponse.Customer.Accounts.SelectMany(c => c.Premises).SelectMany(p => p.Services).Count();
            var sm = new SubscrtiptionMerge
            {
                Accounts = subscriptionResponse.Customer.Accounts,
                ActionContent = actionContent,
                TextContent = subscriptionTextContent,
                FileContent = actionFileContent,
                QueryString = jp,
                Uom = uomContent,
                ServiceLabel = CMS.GetWidgetContent(subscriptionTextContent, tabKey, WIDGET_TYPE_SUBSCRIPTION, "servicelabel"),
                SingleServiceLabel = CMS.GetWidgetContent(subscriptionTextContent, tabKey, WIDGET_TYPE_SUBSCRIPTION, "singleservicelabel"),
                Channels = CMS.GetWidgetConfig(subscriptionConfig, tabKey, WIDGET_TYPE_SUBSCRIPTION, "channels"),
                InsightTypes = new List<string>(s.Insights.Split(',')),
                RangeErrorMessage = CMS.GetWidgetContent(subscriptionTextContent, tabKey, WIDGET_TYPE_SUBSCRIPTION, "outofrangemessage")
            };
            var il = new List<Notification>();
            Mapper.CreateMap<SubscrtiptionMerge, List<Notification>>().ConvertUsing(new SubscriptionConverter());
            s.Notifications = Mapper.Map(sm, il);

            if (s.Notifications == null || s.Notifications.Count == 0)
            {
                s.NoSubscription = true;
                s.NoData = CMS.GetWidgetContent(subscriptionTextContent, tabKey, WIDGET_TYPE_SUBSCRIPTION, "nodata");
                return s;
            }

            var accountId = "";
            var serviceId = "";
            foreach (var n in s.Notifications)
            {
                if (n.Level == "account" && n.AccountId != accountId)
                {
                    n.ShowLabel = true;
                    accountId = n.AccountId;
                }
                if (n.Level == "service" && n.ServiceId != serviceId)
                {
                    n.ShowLabel = true;
                    serviceId = n.ServiceId;
                }
            }

            //Content
            s.SaveButtonLabel = CMS.GetWidgetContent(subscriptionTextContent, tabKey, WIDGET_TYPE_SUBSCRIPTION, "savebuttonlabel");
            s.Disclaimer = CMS.GetWidgetContent(subscriptionTextContent, tabKey, WIDGET_TYPE_SUBSCRIPTION, "disclaimer");
            s.CancelButtonLabel = CMS.GetWidgetContent(subscriptionTextContent, tabKey, WIDGET_TYPE_SUBSCRIPTION, "cancelbuttonlabel");
            s.NotCompleteMessage = CMS.GetWidgetContent(subscriptionTextContent, tabKey, WIDGET_TYPE_SUBSCRIPTION, "notcompletemessage");
            s.SaveFailed = CMS.GetWidgetContent(subscriptionTextContent, tabKey, WIDGET_TYPE_SUBSCRIPTION, "savefailed");
            s.SaveSuccess = CMS.GetWidgetContent(subscriptionTextContent, tabKey, WIDGET_TYPE_SUBSCRIPTION, "savesuccess");
            s.SaveFailed = CMS.GetWidgetContent(subscriptionTextContent, tabKey, WIDGET_TYPE_SUBSCRIPTION, "savefailed");

            //Configuration
            s.Channels = CMS.GetWidgetConfig(subscriptionConfig, tabKey, WIDGET_TYPE_SUBSCRIPTION, "channels");
            foreach (var c in s.Channels.Replace(" ", "").Split(','))
            {
                var channelNameKey = "channel." + c + ".name";
                if (s.ChannelsText == null)
                {
                    s.ChannelsText = subscriptionTextContent.Where(e => e.key == channelNameKey).Select(q => q.shorttext).FirstOrDefault();
                }
                else
                {
                    s.ChannelsText += "," + subscriptionTextContent.Where(e => e.key == channelNameKey).Select(q => q.shorttext).FirstOrDefault();
                }
            }
            return s;
        }

        public Subscription PostSubscription(string tabKey, string parameters, List<Notification> notifications)
        {
            var s = new Subscription();
            var jp = (JsonParams)JsonConvert.DeserializeObject(QueryStringModule.Decrypt(parameters), typeof(JsonParams));
            var content = CMS.GetContent(int.Parse(jp.cl), jp.l).Content;
            var subscriptionTextContent =
                content.TextContent.Where(x => x.category == SUBSCRIPTION_CATEGORY || x.category == COMMON_CATEGORY).ToList();
            var subscriptionConfig =
               content.Configuration.Where(x => x.category == SUBSCRIPTION_CATEGORY || x.category == COMMON_CATEGORY).ToList();
            var subscriptionPostApiVersion = CMS.GetWidgetConfig(subscriptionConfig, tabKey, WIDGET_TYPE_SUBSCRIPTION, "subscriptionpostapiversion");

            if (notifications == null || notifications.Count == 0)
            {
                s.SaveFailed = CMS.GetWidgetContent(subscriptionTextContent, tabKey, WIDGET_TYPE_SUBSCRIPTION, "savefailed");
                return s;
            }

            foreach (var n in notifications)
            {
                n.Channel = "";
                if (n.EmailSelected && n.SmsSelected)
                {
                    n.Channel = ChannelEmailAndSms;
                }
                else if (n.EmailSelected)
                {
                    n.Channel = ChannelEmail;
                }
                else if (n.SmsSelected)
                {
                    n.Channel = ChannelSms;
                }
                if (n.Channel.Length > 0)
                {
                    n.IsSelected = true;
                }
                else
                {
                    n.IsSelected = false;
                }
            }

            var sci = new List<SubscribedInsight>();
            Mapper.CreateMap<Notification, SubscribedInsight>();
            Mapper.Map(notifications, sci);

            var mapCustomer = new SubscribedCustomerInsight
            {
                Id = jp.c,
                Insights = sci
            };

            var postStr1 = @"{ " +
                            "\"Customer\": " + JsonConvert.SerializeObject(mapCustomer) +
                            "}";

            var rm = new APIResponseModels();
            var st = (Status)JsonConvert.DeserializeObject(rm.PostSubscription(postStr1, "en-US", Convert.ToInt32(jp.cl), subscriptionPostApiVersion).Content, typeof(Status));
            if (st.Message != null && st.StatusType == StatusType.ModelErrors)
            {
                s.SaveFailed = CMS.GetWidgetContent(subscriptionTextContent, tabKey, WIDGET_TYPE_SUBSCRIPTION, "savefailed");
            }
            else
            {
                s.SaveSuccess = CMS.GetWidgetContent(subscriptionTextContent, tabKey, WIDGET_TYPE_SUBSCRIPTION, "savesuccess");
            }
            return s;
        }

        private static SubscriptionRoot GetSubscriptionFromCache(JsonParams jp, string version = "v1")
        {
            var rm = new APIResponseModels();
            var requestParams = "CustomerId=" + jp.c + "&AccountId=" + jp.a + "&IncludeContent=false";
            return (SubscriptionRoot)JsonConvert.DeserializeObject(rm.GetSubscription(requestParams, jp.l, int.Parse(jp.cl), version).Content, typeof(SubscriptionRoot));
        }
    }

    public class SubscriptionConverter : ITypeConverter<SubscrtiptionMerge, List<Notification>>
    {
        private const string ThresholdTypeCost = "cost";
        private const string ThresholdTypeUsage = "usage";
        private const string ThresholdTypePercent = "percent";
        private const string ChannelEmailAndSms = "emailandsms";
        private const string ChannelEmail = "email";
        private const string ChannelSms = "sms";

        public List<Notification> Convert(ResolutionContext context)
        {
            var sourceCollection = (SubscrtiptionMerge)context.SourceValue;
            Mapper.CreateMap<SubscrtiptionMerge, List<Notification>>();
            Mapper.CreateMap<SubscriptionInsight, Notification>();

            var nl = new List<Notification>();
            foreach (var a in sourceCollection.Accounts)
            {
                if (a.Insights != null)
                {
                    foreach (var insight in a.Insights)
                    {
                        if (!sourceCollection.InsightTypes.Contains(insight.Name)) continue;
                        var i = EntityMapper.Map<Notification>(insight);
                        var act = sourceCollection.ActionContent.Where(p => p.key == insight.Name).FirstOrDefault();
                        i.AccountId = a.Id;
                        sourceCollection.ImageSize = Enums.ImageSize.Small;
                        if (act == null) continue;
                            SetupSubscription(sourceCollection, act, i, sourceCollection.QueryString);
                            nl.Add(i);
                        }
                    }
                //premise
                foreach (var premise in a.Premises)
                {
                    foreach (var service in premise.Services)
                    {
                        if (service.Insights != null)
                        {
                            foreach (var insight in service.Insights)
                            {
                                if (!sourceCollection.InsightTypes.Contains(insight.Name)) continue;
                                var i = EntityMapper.Map<Notification>(insight);
                                var act = sourceCollection.ActionContent.Where(p => p.key == insight.Name).FirstOrDefault();
                                i.ServiceId = service.Id;
                                //i.ServiceContractId = service.ContractId;
                                i.AccountId = a.Id;
                                i.PremiseId = premise.Id;
                                i.CommodityKey = service.CommodityKey;
                                sourceCollection.ImageSize = Enums.ImageSize.Small;
                                if (act == null) continue;
                                    SetupSubscription(sourceCollection, act, i, sourceCollection.QueryString);
                                    nl.Add(i);
                                }
                            }
                        }
                    }
                }
            return nl;
        }

        private static void SetupSubscription(SubscrtiptionMerge sourceCollection, Common.Action act, Notification i, JsonParams jp)
        {
            if (!string.IsNullOrEmpty(act.imagekey))
            {
                var imgContent = sourceCollection.FileContent.Where(x => x.key == act.imagekey).FirstOrDefault();
                switch (sourceCollection.ImageSize)
                {
                    case Enums.ImageSize.Small:
                    if (imgContent != null)
                    {
                        i.ImageUrl = imgContent.smallfile;
                        i.ImageTitle = imgContent.smallfiletitle;
                    }
                        break;
                    case Enums.ImageSize.Medium:
                    if (imgContent != null)
                    {
                        i.ImageUrl = imgContent.mediumfile;
                        i.ImageTitle = imgContent.mediumfiletitle;
                    }
                        break;
                    case Enums.ImageSize.Large:
                    if (imgContent != null)
                    {
                        i.ImageUrl = imgContent.largefile;
                        i.ImageTitle = imgContent.largefiletitle;
                    }
                        break;
                }
            }
            if (!string.IsNullOrEmpty(act.iconclass))
            {
                i.Icon = act.iconclass;
            }

            if (i.ThresholdType == "cost" || i.ThresholdType == "usage")
            {
                i.RangeErrorMessage = sourceCollection.RangeErrorMessage?.Replace("{%minvalue%}", i.ThresholdMin.ToString()).Replace("{%maxvalue%}", i.ThresholdMax.ToString()) ?? "";
            }

            i.Title = act.namekey == "common.undefined" ? "" : sourceCollection.TextContent.Where(p => p.key == act.namekey).FirstOrDefault().shorttext;

            var desc = sourceCollection.TextContent.Where(p => p.key == act.descriptionkey).FirstOrDefault();
            i.Description = desc == null ? "" : desc.shorttext;

            var lnkText = sourceCollection.TextContent.Where(p => p.key == act.nextsteplinktext).FirstOrDefault();
            i.LinkText = lnkText == null ? "" : lnkText.shorttext;

            i.LinkType = act.nextsteplinktype;
            i.Link = GeneralHelper.CreateLink(act.nextsteplinktype, jp, act.nextsteplink);

            var ci = new CultureInfo(jp.l);
            switch (i.ThresholdType)
            {
                case ThresholdTypeCost:
                    i.CurrencySymbol = ci.NumberFormat.CurrencySymbol;
                    break;
                case ThresholdTypeUsage:
                    if (i.UomKey != null)
                    {
                        i.UomText = sourceCollection.TextContent.Where(z => z.key == sourceCollection.Uom.Where(x => x.key == i.UomKey).FirstOrDefault().namekey).FirstOrDefault().shorttext;
                    }
                    break;
                case ThresholdTypePercent:
                    break;
            }

            i.EmailEnabled = i.AllowedChannels.Contains(ChannelEmail);
            i.SmsEnabled = i.AllowedChannels.Contains(ChannelSms);

            if (i.EmailEnabled && i.SmsEnabled)
            {
                i.Channels = sourceCollection.Channels + "," + ChannelEmailAndSms;
            }

            if (i.IsSelected == false)
            {
                i.Channel = "";
            }

            i.EmailSelected = i.Channel.Contains(ChannelEmail);
            i.SmsSelected = i.Channel.Contains(ChannelSms);

            var comNameKey = "commodity." + i.CommodityKey + ".name";
            var comName = sourceCollection.TextContent.Where(e => e.key == comNameKey).Select(s => s.shorttext).FirstOrDefault();
            i.ServiceLabel = sourceCollection.ServiceLabel.Replace("{%Commodity%}", comName).Replace("{%ServiceId%}", i.ServiceId);
            i.SingleServiceLabel = sourceCollection.SingleServiceLabel.Replace("{%Commodity%}", comName);
        }
    }

    public class SubscrtiptionMerge
    {
        public List<SubscriptionAccount> Accounts { get; set; }
        public List<FileContent> FileContent { get; set; }
        public List<Common.TextContent> TextContent { get; set; }
        public List<Common.Action> ActionContent { get; set; }
        public Enums.ImageSize ImageSize { get; set; }
        public JsonParams QueryString { get; set; }
        public List<Common.UOM> Uom { get; set; }
        public string ServiceLabel { get; set; }
        public string SingleServiceLabel { get; set; }
        public string Channels { get; set; }
        public List<string> InsightTypes { get; set; }
        public string RangeErrorMessage { get; set; }
    }

    public class Notification
    {
        public string Name { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public string ImageTitle { get; set; }
        public string Icon { get; set; }
        public string Link { get; set; }
        public string LinkText { get; set; }
        public string LinkType { get; set; }
        public decimal Threshold { get; set; }
        public bool IsSelected { get; set; }
        public string Level { get; set; }
        public string AccountId { get; set; }
        public string PremiseId { get; set; }
        public string ServiceId { get; set; }
        //public string ServiceContractId { get; set; }
        public string AllowedChannels { get; set; }
        public string ThresholdType { get; set; }
        public decimal? ThresholdMin { get; set; }
        public decimal? ThresholdMax { get; set; }
        public string UomKey { get; set; }
        public string UomText { get; set; }
        public string CurrencySymbol { get; set; }
        public string Channel { get; set; }
        public string Channels { get; set; }
        public bool EmailSelected { get; set; }
        public bool SmsSelected { get; set; }
        public bool EmailEnabled { get; set; }
        public bool SmsEnabled { get; set; }
        public bool ThresholdDisabled { get; set; }
        public string CommodityKey { get; set; }
        public string ServiceLabel { get; set; }
        public string SingleServiceLabel { get; set; }
        public bool InvalidRange { get; set; }
        public bool InvalidRequired { get; set; }
        public string RangeErrorMessage { get; set; }

        public bool ShowLabel { get; set; }

    }

    public class Subscription : ContentBase
    {    
        public string SaveButtonLabel { get; set; }
        public string SaveSuccess { get; set; }
        public string SaveFailed { get; set; }
        public string NoData { get; set; }
        public bool NoSubscription { get; set; }
        public string NotCompleteMessage { get; set; }
        public string Disclaimer { get; set; }
        public string CancelButtonLabel { get; set; }
        public string Insights { get; set; }
        public string Channels { get; set; }
        public string ChannelsText { get; set; }
        public int ServiceCount { get; set; }
        public List<Notification> Notifications { get; set; }
        public Subscription() 
        { }
        public Subscription(List<Common.Configuration> config, List<Common.TextContent> content, string tabKey, string widetType, JsonParams jp) : base(config, content, tabKey, widetType, jp)
        { }
    }
}

