﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Caching;
using AutoMapper;
using CE.InsightsWeb.Common;
using CE.InsightsWeb.Common.AclaraJson;
using CE.InsightsWeb.Common.QueryStringEncryptor;
using Fasterflect;
using Newtonsoft.Json;

namespace CE.InsightsWeb.Models
{
    public class ThresholdModels
    {
        private const string THRESHOLD_CATEGORY = "threshold";
        private const string COMMON_CATEGORY = "common";
        private const string ELECTRIC_COMMODITY_KEY = "electric";
        private const string GAS_COMMODITY_KEY = "gas";
        private const string WATER_COMMODITY_KEY = "water";
        private const string WIDGET_TYPE_THRESHOLD = "threshold";

        public Threshold GetThreshold(string tabKey, string parameters, string commodityKey)
        {
            var jp = (JsonParams)JsonConvert.DeserializeObject(QueryStringModule.Decrypt(parameters), typeof(JsonParams));
            //Content
            var content = CMS.GetContent(int.Parse(jp.cl), jp.l).Content;
            var thresholdTextContent = content.TextContent.Where(x => x.category == THRESHOLD_CATEGORY || x.category == COMMON_CATEGORY).ToList();
            var thresholdConfig = content.Configuration.Where(x => x.category == THRESHOLD_CATEGORY || x.category == COMMON_CATEGORY).ToList();

            var bm = new BillModels();
            var dFormat = CMS.GetWidgetConfig(thresholdConfig, tabKey, WIDGET_TYPE_THRESHOLD, "dateformat");
            var cFormat = CMS.GetWidgetConfig(thresholdConfig, tabKey, WIDGET_TYPE_THRESHOLD, "currencyformat");
            var ngFormat = CMS.GetCommonContent(CMS.GetContent(int.Parse(jp.cl), jp.l), "format.gasquantity", "config");
            var neFormat = CMS.GetCommonContent(CMS.GetContent(int.Parse(jp.cl), jp.l), "format.electricquantity", "config");
            var nwFormat = CMS.GetCommonContent(CMS.GetContent(int.Parse(jp.cl), jp.l), "format.waterquantity", "config");
            var ci = new CultureInfo(jp.l);

            var t = new Threshold(thresholdConfig, thresholdTextContent, tabKey, WIDGET_TYPE_THRESHOLD, jp);

            var rateApiVersion = CMS.GetWidgetConfig(thresholdConfig, tabKey, WIDGET_TYPE_THRESHOLD, "rateapiversion");
            var billtodateApiVersion = CMS.GetWidgetConfig(thresholdConfig, tabKey, WIDGET_TYPE_THRESHOLD, "billtodateapiversion");
            var billApiVersion = CMS.GetWidgetConfig(thresholdConfig, tabKey, WIDGET_TYPE_THRESHOLD, "billapiversion");
            t.Commodities = CMS.GetWidgetConfig(thresholdConfig, tabKey, WIDGET_TYPE_THRESHOLD, "commodities");
            t.CommoditiesText = CMS.GetCommoditiesName(thresholdTextContent, t.Commodities);
            t.CommoditiesIconFonts = CMS.GetCommoditiesIconFonts(thresholdTextContent, t.Commodities);

            var commodities = t.Commodities.Replace(" ", "").Split(',');

            if (string.IsNullOrEmpty(commodityKey))
            {
                commodityKey = commodities[0];
            }

            //Bill To Date
            var rateClass = string.Empty;
            double usage = 0;
            t.BillToDate = bm.GetBillToDate(tabKey, parameters, billtodateApiVersion, false);
            switch (commodityKey)
            {
                case GAS_COMMODITY_KEY:
                    if (t.BillToDate.RateMismatchGas)
                    {
                        t.RateMismatch = true;
                        return t;
                    }
                    rateClass = t.BillToDate.RateClassGas;
                    t.UseToDate = t.BillToDate.GasUseToDate;
                    t.ProjectedUse = t.BillToDate.GasUseProjected;
                    t.AverageDailyUse = t.BillToDate.AverageDailyUseGas.ToString(ngFormat, ci);
                    t.UOM = t.BillToDate.GasUOM;
                    usage = t.BillToDate.GUseToDate;
                    break;
                case ELECTRIC_COMMODITY_KEY:
                    if (t.BillToDate.RateMismatchElectric)
                    {
                        t.RateMismatch = true;
                        return t;
                    }
                    rateClass = t.BillToDate.RateClassElectric;
                    t.UseToDate = t.BillToDate.ElectricUseToDate;
                    t.ProjectedUse = t.BillToDate.ElectricUseProjected;
                    t.AverageDailyUse = t.BillToDate.AverageDailyUseElectric.ToString(neFormat, ci);
                    t.UOM = t.BillToDate.ElectricUOM;
                    usage = t.BillToDate.EUseToDate;
                    break;
                case WATER_COMMODITY_KEY:
                    if (t.BillToDate.RateMismatchWater)
                    {
                        t.RateMismatch = true;
                        return t;
                    }
                    rateClass = t.BillToDate.RateClassWater;
                    t.UseToDate = t.BillToDate.WaterUseToDate;
                    t.ProjectedUse = t.BillToDate.WaterUseProjected;
                    t.AverageDailyUse = t.BillToDate.AverageDailyUseWater.ToString(nwFormat, ci);
                    t.UOM = t.BillToDate.WaterUOM;
                    usage = t.BillToDate.WUseToDate;
                    break;
            }

            if (t.BillToDate.NoBillToDate)
            {
                //Bill Summary
                t.BillSummary = bm.GetBillSummary(tabKey, parameters, true, billApiVersion);
                if (!t.BillSummary.NoBills)
                {
                    switch (commodityKey)
                    {
                        case GAS_COMMODITY_KEY:
                            t.BillTotalServiceUse = t.BillSummary.TotalGasUsed.ToString(ngFormat, ci);
                            t.BillStartDate = Convert.ToDateTime(t.BillSummary.BillStartDateGas).ToString(dFormat, ci);
                            t.BillEndDate = Convert.ToDateTime(t.BillSummary.BillEndDateGas).ToString(dFormat, ci);
                            t.UOM = t.BillSummary.GasUOM;
                            rateClass = t.BillSummary.RateClassGas;
                            usage = t.BillSummary.TotalGasUsed;
                            break;
                        case ELECTRIC_COMMODITY_KEY:
                            t.BillTotalServiceUse = t.BillSummary.TotalElectricityUsed.ToString(ngFormat, ci);
                            t.BillStartDate = Convert.ToDateTime(t.BillSummary.BillStartDateElectric).ToString(dFormat, ci);
                            t.BillEndDate = Convert.ToDateTime(t.BillSummary.BillEndDateElectric).ToString(dFormat, ci);
                            t.UOM = t.BillSummary.ElectricUOM;
                            rateClass = t.BillSummary.RateClassElectric;
                            usage = t.BillSummary.TotalElectricityUsed;
                            break;
                        case WATER_COMMODITY_KEY:
                            t.BillTotalServiceUse = t.BillSummary.TotalWaterUsed.ToString(ngFormat, ci);
                            t.BillStartDate = Convert.ToDateTime(t.BillSummary.BillStartDateWater).ToString(dFormat, ci);
                            t.BillEndDate = Convert.ToDateTime(t.BillSummary.BillEndDateWater).ToString(dFormat, ci);
                            t.UOM = t.BillSummary.WaterUOM;
                            rateClass = t.BillSummary.RateClassWater;
                            usage = t.BillSummary.TotalWaterUsed;
                            break;
                    }
                }
            }

            if (rateClass == null)
            {
                t.NoBills = true;
                t.NoBillsText = CMS.GetWidgetContent(thresholdTextContent, tabKey, WIDGET_TYPE_THRESHOLD, "nobills");
                return t;
            }

            //Get Rates
            var rateResponse = GetRatesFromCache(jp, rateClass, rateApiVersion);
            //Non tier rate
            if (!rateResponse.RateClass.IsTiered)
            {
                t.NonTier = true;
                t.NonTierText = CMS.GetWidgetContent(thresholdTextContent, tabKey, WIDGET_TYPE_THRESHOLD, "nontierrate");
                return t;
            }

            var useCharges = rateResponse.Details.UseCharges;

            var ti = new List<TierInfo>();
            Mapper.CreateMap<FinalizedTierBoundary, TierInfo>();
            Mapper.Map(rateResponse.Details.FinalizedTierBoundaries, ti);

            var tt = new TierInfo();
            Mapper.CreateMap<List<TierInfo>, TierInfo>();
            Mapper.Map(ti.Last(), tt);
            tt.BaseOrTierKey = "Tier" + (ti.Count + 1);
            ti.Add(tt);
            var cnt = 0;
            double previousThreshold = 0;
            double previousRange = 0;
            var previousSeasonKey = string.Empty;
            foreach (var i in ti)
            {
                if (usage <= i.Threshold && previousThreshold < usage)
                {
                    i.InTier = true;
                    i.InTierPercentage = (usage - previousThreshold) / (i.Threshold - previousThreshold) * 100;
                }
                else if (previousThreshold < usage && cnt == ti.Count - 1)
                {
                    i.InTier = true;
                    i.InTierPercentage = 33;
                }

                if (cnt == 0)
                {
                    i.TierRange = i.Threshold - previousThreshold;
                    i.TierRangeLabel = CMS.GetWidgetContent(thresholdTextContent, tabKey, WIDGET_TYPE_THRESHOLD, "upto") + " " + i.Threshold.ToString(ci) + " " + t.UOM;
                }
                else if (cnt == ti.Count - 1)
                {
                    i.TierRangeLabel = CMS.GetWidgetContent(thresholdTextContent, tabKey, WIDGET_TYPE_THRESHOLD, "above") + " " + previousThreshold + " " + t.UOM;
                    i.Threshold = previousThreshold;
                    i.SeasonKey = previousSeasonKey;
                    i.TierRange = previousRange;
                }
                else
                {
                    i.TierRangeLabel = previousThreshold + " - " + i.Threshold.ToString(ci) + " " + t.UOM;
                    i.TierRange = i.Threshold - previousThreshold;
                }

                i.TierLabel = CMS.GetWidgetContent(thresholdTextContent, tabKey, WIDGET_TYPE_THRESHOLD, i.BaseOrTierKey.ToLower() + "label");
                i.TierColor = CMS.GetWidgetConfig(thresholdConfig, tabKey, WIDGET_TYPE_THRESHOLD, i.BaseOrTierKey.ToLower() + "color");
                i.UseCharges = useCharges.Where(x => x.BaseOrTierKey == i.BaseOrTierKey && x.SeasonKey == i.SeasonKey).Sum(x => x.ChargeValue);
                i.UseChargesLabel = i.UseCharges.ToString(cFormat) + " " + CMS.GetWidgetContent(thresholdTextContent, tabKey, WIDGET_TYPE_THRESHOLD, "peruom") + " " + t.UOM;

                previousThreshold = i.Threshold;
                previousRange = i.TierRange;
                previousSeasonKey = i.SeasonKey;
                cnt += 1;
            }
            var tierRangeTotal = ti.Sum(tr => tr.TierRange);
            var factor = 1.0 / tierRangeTotal;
            foreach (var i in ti)
            {
                i.TierPercentage = Math.Round(factor * i.TierRange * 100, 2);
            }
            t.TierBoundaries = ti;

            //Content
            t.NoBillsText = CMS.GetWidgetContent(thresholdTextContent, tabKey, WIDGET_TYPE_THRESHOLD, "nobills");
            t.UseToDateLabel = CMS.GetWidgetContent(thresholdTextContent, tabKey, WIDGET_TYPE_THRESHOLD, "usetodatelabel");
            t.DaysLeftLabel = CMS.GetWidgetContent(thresholdTextContent, tabKey, WIDGET_TYPE_THRESHOLD, "daysleftlabel");
            t.PricingTiersLabel = CMS.GetWidgetContent(thresholdTextContent, tabKey, WIDGET_TYPE_THRESHOLD, "pricingtierslabel");
            t.ProjectedUseMessage = CMS.GetWidgetContent(thresholdTextContent, tabKey, WIDGET_TYPE_THRESHOLD, "projectedusemessage");
            t.AveragedailyUseMessage = CMS.GetWidgetContent(thresholdTextContent, tabKey, WIDGET_TYPE_THRESHOLD, "averagedailyusemessage");
            t.BilledUseMessage = CMS.GetWidgetContent(thresholdTextContent, tabKey, WIDGET_TYPE_THRESHOLD, "billedusemessage");
            t.BilledUseLabel = CMS.GetWidgetContent(thresholdTextContent, tabKey, WIDGET_TYPE_THRESHOLD, "billeduselabel");
            t.DateTo = CMS.GetWidgetContent(thresholdTextContent, tabKey, WIDGET_TYPE_THRESHOLD, "dateto");
            //Configuration
            t.Commodities = CMS.GetWidgetConfig(thresholdConfig, tabKey, WIDGET_TYPE_THRESHOLD, "commodities");

            t.TierBoundariesCurrent = t.TierBoundaries.DeepClone();
          
            return t;
        }

        private static RateRoot GetRatesFromCache(JsonParams jp, string rateClass, string version = "v1")
        {
            RateRoot rateResponse;
            if (HttpRuntime.Cache["RateResponse" + jp.cl + "" + jp.c + "" + jp.a + "" + jp.p + "" + jp.l + "" + jp.w] ==
                null)
            {
                var rm = new APIResponseModels();
                var date = DateTime.UtcNow.ToString("yyyy-MM-dd");
                var requestParams = "rateclass=" + rateClass + "&date=" + date + "&IncludeRateDetails=true&IncludeFinalizedTierBoundaries=true";
                rateResponse = (RateRoot)JsonConvert.DeserializeObject(rm.GetRate(requestParams, jp.l, int.Parse(jp.cl), version).Content, typeof(RateRoot));
                HttpRuntime.Cache.Insert("RateResponse" + jp.cl + "" + jp.c + "" + jp.a + "" + jp.p + "" + jp.l + "" + jp.w, rateResponse,
                                            null, DateTime.Now.AddMinutes(Constants.kCache_Expiry), Cache.NoSlidingExpiration,
                                            CacheItemPriority.NotRemovable, null);
            }
            else
            {
                rateResponse =
                    (RateRoot)
                        HttpRuntime.Cache[
                            "RateResponse" + jp.cl + "" + jp.c + "" + jp.a + "" + jp.p + "" + jp.l + "" + jp.w];
            }

            return rateResponse;
        }
    }

    public class Threshold : ContentBase
    {   
        public string Commodities { get; set; }
        public string CommoditiesText { get; set; }
        public string CommoditiesIconFonts { get; set; }
        public string NoBillsText { get; set; }
        public bool NonTier { get; set; }
        public string NonTierText { get; set; }
        public string UseToDateLabel { get; set; }
        public string DaysLeftLabel { get; set; }
        public string PricingTiersLabel { get; set; }
        public string PerUOM { get; set; }
        public string ProjectedUseMessage { get; set; }
        public string AveragedailyUseMessage { get; set; }
        public string BilledUseMessage { get; set; }
        public string BilledUseLabel { get; set; }
        public string DateTo { get; set; }
        public string UseToDate { get; set; }
        public string ProjectedUse { get; set; }
        public string AverageDailyUse { get; set; }
        public string UOM { get; set; }
        public string BillTotalServiceUse { get; set; }
        public string BillStartDate { get; set; }
        public string BillEndDate { get; set; }
        public bool NoBills { get; set; }     
        public bool RateMismatch { get; set; }
        public BillSummary BillSummary { get; set; }
        public BillToDate BillToDate { get; set; }
        public List<TierInfo> TierBoundaries { get; set; }
        public List<TierInfo> TierBoundariesCurrent { get; set; }
        public Threshold(List<Common.Configuration> config, List<Common.TextContent> content, string tabKey, string widetType, JsonParams jp) : base(config, content, tabKey, widetType, jp)
        { }
    }

    public class TierInfo
    {
        public string BaseOrTierKey { get; set; }
        public string TimeOfUseKey { get; set; }
        public string SeasonKey { get; set; }
        public double Threshold { get; set; }
        public string TierLabel { get; set; }
        public string TierColor { get; set; }
        public string TierRangeLabel { get; set; }
        public double TierRange { get; set; }
        public bool InTier { get; set; }
        public double InTierPercentage { get; set; }
        public double TierPercentage { get; set; }
        public double UseCharges { get; set; }
        public string UseChargesLabel { get; set; }
    }
}

