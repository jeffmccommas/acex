﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using AutoMapper;
using CE.InsightsWeb.Common;
using CE.InsightsWeb.Common.AclaraJson;
using CE.InsightsWeb.Common.QueryStringEncryptor;
using CE.InsightsWeb.Helpers;
using Newtonsoft.Json;

namespace CE.InsightsWeb.Models
{
    public class SavingsGoalModels
    {
        private const string WIDGET_TYPE = "setgoal";

        public string GetSavingsGoal(string tabKey, string parameters, bool getOptions = false)
        {
            var goal = new Goal();
            var jp = (JsonParams)JsonConvert.DeserializeObject(QueryStringModule.Decrypt(parameters), typeof(JsonParams));
            var rm = new APIResponseModels();
            //Content
            var cr = CMS.GetContent(int.Parse(jp.cl), jp.l);
            var actionTextContent = cr.Content.TextContent.Where(x => x.category == "action").ToList();
            var actionConfig = cr.Content.Configuration.Where(x => x.category == "action").ToList();
            var configuredCommodities = CMS.GetWidgetConfig(actionConfig, tabKey, WIDGET_TYPE, "commodities").Replace(" ", string.Empty);
            var configuredCommoditiesBill = CMS.GetWidgetConfig(actionConfig, tabKey, WIDGET_TYPE, "billcommodities").Replace(" ", string.Empty);
            double goalAmount = 0, goalPercent = 0, planAmount = 0, billDisaggAmount = 0;
            if (getOptions == false)
            {
                // Call profile endpoint to get stored goal
                var pm = new ProfileModels();
                var profileResponse = pm.GetProfileAttributesFromCache(jp);

                if (profileResponse.Customer?.Accounts[0].Premises.Count > 0)
                {
                    var goalAttrAmount = profileResponse.Customer.Accounts[0].Premises[0].Attributes.Where(z => z.AttributeKey == "savingsgoalamount").FirstOrDefault();
                    if (goalAttrAmount?.AttributeValue != null && Convert.ToInt32(goalAttrAmount.AttributeValue) > 0)
                    {
                        goal.GoalType = "stored";
                        goalAmount = Convert.ToDouble(goalAttrAmount.AttributeValue);

                        //Action plan
                        var actionPlanResponse = WaysToSaveModels.GetActionPlanFromCache(jp);
                        if (actionPlanResponse.Customer != null && actionPlanResponse.Customer.Accounts.Count > 0 && actionPlanResponse.Customer.Accounts[0].Premises.Count > 0)
                        {
                            var actions = actionPlanResponse.Customer.Accounts[0].Premises[0].ActionStatuses.Where(x => x.Status == "completed" || x.Status == "selected").ToList();

                            //Get Actions
                            var actionTypes = CMS.GetWidgetConfig(actionConfig, tabKey, WIDGET_TYPE, "actiontypes").Replace(" ", string.Empty);
                            var requestParams = "CustomerId=" + jp.c + "&AccountId=" + jp.a + "&PremiseId=" + jp.p + "&CommodityKeys=" + configuredCommodities + "&Types="+ actionTypes;
                            var actionResponse = (ActionRoot)JsonConvert.DeserializeObject(rm.GetAction(requestParams, jp.l, int.Parse(jp.cl)).Content, typeof(ActionRoot));

                            if (actionResponse.Customer != null)
                            {
                                planAmount = actionResponse.Customer.Accounts[0].Premises[0].Actions.Where(p => actions.Any(l => p.Key == l.ActionKey) && p.AnnualSavingsEstimate > 0).ToList().Sum(x => x.AnnualSavingsEstimate);
                                goal.PlanAmount = planAmount;
                            }
                        }

                        var goalAttrPercent = profileResponse.Customer.Accounts[0].Premises[0].Attributes.Where(z => z.AttributeKey == "savingsgoalpercent").FirstOrDefault();
                        if (goalAttrPercent != null)
                        {
                            goalPercent = Convert.ToDouble(goalAttrPercent.AttributeValue);
                        }
                    }
                }

                var o = GetBillDisaggAmount(tabKey, actionConfig, jp, rm, configuredCommoditiesBill).Split(',');
                billDisaggAmount = Convert.ToDouble(o[0]);

                //if no goal call BillDisagg  
                if (goalAmount == 0)
                {
                    goal.GoalType = o[1];
                }
            }
            else
            {
                string[] o = GetBillDisaggAmount(tabKey, actionConfig, jp, rm, configuredCommoditiesBill).Split(',');
                billDisaggAmount = Convert.ToDouble(o[0]);
                goal.GoalType = o[1];
            }

            var ci = new CultureInfo(jp.l);
            var numberInfo = (NumberFormatInfo)CultureInfo.CurrentCulture.NumberFormat.Clone();
            numberInfo.PercentPositivePattern = 1;
            var gal = new List<GoalAttribute>();
            //goal amt
            var ga = new GoalAttribute
            {
                Key = "savingsgoalamount",
                Answer = goalAmount.ToString(CultureInfo.CurrentCulture),
                Level = "premise"
            };
            gal.Add(ga);
            //goal percent
            var gap = new GoalAttribute
            {
                Key = "savingsgoalpercent",
                Answer = goalPercent.ToString(CultureInfo.CurrentCulture),
                Level = "premise"
            };
            gal.Add(gap);
            goal.Attributes = gal;

            goal.GoalAmount = goalAmount;
            goal.GoalAmountFormatted = goalAmount.ToString("C0", ci);
            goal.PlanAmountFormatted = planAmount.ToString("C0", ci);
            //Content
            var recGoalPercent = actionConfig.Where(x => x.key == tabKey + ".setgoal.recgoalpercent").FirstOrDefault();
            var percentFormat = actionConfig.Where(x => x.key == tabKey + ".setgoal.format.percent").FirstOrDefault();

            var recGP = (recGoalPercent == null) ? 0 : Convert.ToDouble(recGoalPercent.value);
            var perFormat = (percentFormat == null) ? "p0" : percentFormat.value;
            //Intro page
            goal.IntroTitle = CMS.GetWidgetContent(actionTextContent, tabKey, WIDGET_TYPE, "introtitle");
            goal.IntroTitle2 = CMS.GetWidgetContent(actionTextContent, tabKey, WIDGET_TYPE, "introtitle2");
            goal.IntroSubtitle = CMS.GetWidgetContent(actionTextContent, tabKey, WIDGET_TYPE, "introsubtitle");
            if (recGP > 0)
            {
                var percent = Convert.ToDouble(recGP).ToString(perFormat, numberInfo);
                goal.IntroSetgoalButton = CMS.GetWidgetContent(actionTextContent, tabKey, WIDGET_TYPE, "introsetgoalbutton").Replace("{%recgoalpercent%}", percent);
                var amt = (int)(billDisaggAmount * (recGP * 100)) / 100;
                goal.IntroSetgoalButtonDesc = CMS.GetWidgetContent(actionTextContent, tabKey, WIDGET_TYPE, "introsetgoalbuttondesc").Replace("{%savingsamount%}", amt.ToString("C0", ci));
                goal.RecommendedAmount = amt;
                goal.RecommendedPercent = recGP;
                goal.IntroTitle2 = goal.IntroTitle2.Replace("{%recgoalpercent%}", percent);
            }
            goal.IntroMoreOptions = CMS.GetWidgetContent(actionTextContent, tabKey, WIDGET_TYPE, "intromoreoptions");
            goal.IntroOr = CMS.GetWidgetContent(actionTextContent, tabKey, WIDGET_TYPE, "introor");
            //Set Goal Options page
            goal.OptionsTitle = CMS.GetWidgetContent(actionTextContent, tabKey, WIDGET_TYPE, "optionstitle");
            goal.OptionsDesc = CMS.GetWidgetContent(actionTextContent, tabKey, WIDGET_TYPE, "optionsdesc");
            goal.OptionsText = CMS.GetWidgetContent(actionTextContent, tabKey, WIDGET_TYPE, "optionstext");
            var gp = CMS.GetWidgetConfig(actionConfig, tabKey, WIDGET_TYPE, "goalpercentages");
            var options = gp.Split(',');
            var gos = new List<GoalOption>();
            foreach (var o in options)
            {
                var go = new GoalOption();
                if ((goalPercent > 0 || billDisaggAmount > 0) && o.Length > 0)
                {
                    var amt = (int)(billDisaggAmount * (Convert.ToDouble(o) * 100)) / 100;
                    go.OptionText = goal.OptionsText.Replace("{%goalpercent%}", Convert.ToDouble(o).ToString(perFormat, numberInfo)).Replace("{%savingsamount%}", amt.ToString("C0", ci));
                    if (goalPercent > 0)
                    {
                        if (Convert.ToDouble(o) == goalPercent)
                        {
                            go.Selected = "selected";
                        }
                    }
                    else
                    {
                        if (Convert.ToDouble(o) == recGP)
                        {
                            go.Selected = "selected";
                        }
                    }
                    go.OptionValue = Convert.ToDouble(o);
                    go.OptionAmount = amt;
                    gos.Add(go);
                }
            }
            goal.GoalOptions = gos;
            goal.OptionsRecommended = CMS.GetWidgetContent(actionTextContent, tabKey, WIDGET_TYPE, "optionsrecommended");
            goal.OptionsCancelButton = CMS.GetWidgetContent(actionTextContent, tabKey, WIDGET_TYPE, "optionscancelbutton");
            goal.OptionsSaveButton = CMS.GetWidgetContent(actionTextContent, tabKey, WIDGET_TYPE, "optionssavebutton");
            //My Goal Page
            goal.MyGoalTitle = CMS.GetWidgetContent(actionTextContent, tabKey, WIDGET_TYPE, "mygoaltitle");
            goal.IntroText = CMS.GetWidgetContent(actionTextContent, tabKey, WIDGET_TYPE, "introtext");
            goal.MyGoalAmount = CMS.GetWidgetContent(actionTextContent, tabKey, WIDGET_TYPE, "mygoalamount");
            goal.MyGoalPlanAmount = CMS.GetWidgetContent(actionTextContent, tabKey, WIDGET_TYPE, "mygoalplanamount");
            goal.MyGoalAddMoreActions = CMS.GetWidgetContent(actionTextContent, tabKey, WIDGET_TYPE, "mygoaladdmoreactions").Replace("{%savingsamount%}",goal.GoalAmountFormatted);
            goal.MyGoalEditButton = CMS.GetWidgetContent(actionTextContent, tabKey, WIDGET_TYPE, "mygoaleditbutton");
            goal.MyGoalCreatePlan = CMS.GetWidgetContent(actionTextContent, tabKey, WIDGET_TYPE, "mygoalcreateplan");
            goal.ViewMoreActions = CMS.GetWidgetContent(actionTextContent, tabKey, WIDGET_TYPE, "viewmoreactions");
            goal.EnoughActions = CMS.GetWidgetContent(actionTextContent, tabKey, WIDGET_TYPE, "enoughactions");
            goal.Footer = CMS.GetWidgetContent(actionTextContent, tabKey, WIDGET_TYPE, "footer");
            goal.FooterLinkText = CMS.GetWidgetContent(actionTextContent, tabKey, WIDGET_TYPE, "footerlinktext");

            goal.HtmlTemplate = CMS.GetWidgetConfig(actionConfig, tabKey, WIDGET_TYPE, "htmltemplate");
            goal.ActionsLink = CMS.GetWidgetConfig(actionConfig, tabKey, WIDGET_TYPE, "actionslink");
            goal.ShowActionsLink = CMS.GetWidgetConfig(actionConfig, tabKey, WIDGET_TYPE, "showactionslink");
            goal.ShowTitle = CMS.GetWidgetConfig(actionConfig, tabKey, WIDGET_TYPE, "showtitle");
            goal.ShowIntro = CMS.GetWidgetConfig(actionConfig, tabKey, WIDGET_TYPE, "showintro");
            goal.ShowWaysToSaveLink = CMS.GetWidgetConfig(actionConfig, tabKey, WIDGET_TYPE, "showwaystosavelink");
            goal.OptionsShowRecommended = CMS.GetWidgetConfig(actionConfig, tabKey, WIDGET_TYPE, "optionsshowrecommended");
            goal.FooterLink = GeneralHelper.CreateLink(CMS.GetWidgetConfig(actionConfig, tabKey, WIDGET_TYPE, "footerlinktype"), jp, CMS.GetWidgetConfig(actionConfig, tabKey, WIDGET_TYPE, "footerlink"));
            goal.ShowFooterLink = CMS.GetWidgetConfig(actionConfig, tabKey, WIDGET_TYPE, "showfooterlink");
            goal.ShowFooterText = CMS.GetWidgetConfig(actionConfig, tabKey, WIDGET_TYPE, "showfootertext");

            goal.WaysToSaveLink = GeneralHelper.CreateLink("internal", jp, CMS.GetWidgetConfig(actionConfig, tabKey, WIDGET_TYPE, "waystosavelink"));

            return JsonConvert.SerializeObject(goal);
        }

        private static string GetBillDisaggAmount(string tabKey, List<Common.Configuration> config, JsonParams jp, APIResponseModels rm, string configuredCommodities)
        {
            var current = DateTime.Now;
            var dt = "StartDate=" + current.AddYears(-1).ToString("yyyy-MM-dd") + "&EndDate=" + current.ToString("yyyy-MM-dd");
            var requestParams = "CustomerId=" + jp.c + "&AccountId=" + jp.a + "&PremiseId=" + jp.p + "&" + dt;
            var disaggResponse = (EndUseRoot)JsonConvert.DeserializeObject(rm.GetBillDisagg(requestParams, jp.l, int.Parse(jp.cl)).Content, typeof(EndUseRoot));

            var sqSplit = new List<string>(configuredCommodities.Replace(" ", "").Split(','));
            var statusFailedOrModel = (from pa in disaggResponse.Customer.Accounts[0].Premises[0].DisaggStatuses.AsQueryable()
                                       where sqSplit.Any(key => pa.CommodityKey.ToString().ToLower().Equals(key))
                                       select pa).Where(x => x.Status == Enums.DisaggStatusType.Failed || x.Status == Enums.DisaggStatusType.ModelOnly).ToList();

            if (disaggResponse.Customer == null || statusFailedOrModel.Count > 0)
            {
                var defaultYearlyBill = config.Where(x => x.key == tabKey + ".setgoal.defaultyearlybill").FirstOrDefault();
                return (defaultYearlyBill == null) ? "0,authored" : defaultYearlyBill.value + ",authored";
            }

            if (disaggResponse.Customer != null)
            {
                double billDisaggAmount = 0;
                var endUses = disaggResponse.Customer.Accounts[0].Premises[0].EndUses.ToList();
                //end use logic
                foreach (string com in configuredCommodities.Replace(" ", "").Split(','))
                {
                    var endUsesWithCom = endUses.Where(c => c.DisaggDetails.Any(x => x.CommodityKey == com));
                    if (endUsesWithCom != null)
                    {
                        var amt = (from d in endUsesWithCom.Select(x => x.DisaggDetails)
                                   select d.Where(y => y.CommodityKey == com).Sum(z => z.CostAmount)).ToList().Sum();
                        billDisaggAmount += amt;
                    }
                }
                return billDisaggAmount + ",billdisagg";
            }

            return "";
        }

        public void PostSavingsGoal(Goal goal, string parameters)
        {
            if (goal == null) return;
            var jp = (JsonParams)JsonConvert.DeserializeObject(QueryStringModule.Decrypt(parameters), typeof(JsonParams));

            var questCustomer = goal.Attributes.Where(x => x.Level == "customer").ToList();
            var questAccount = goal.Attributes.Where(x => x.Level == "account").ToList();
            var questPremise = goal.Attributes.Where(x => x.Level == "premise").ToList();

            var custAttr = new List<Common.AclaraJson.Attribute>();
            foreach (var qc in questCustomer)
            {
                var cAttr = new Common.AclaraJson.Attribute();
                if (string.IsNullOrEmpty(qc.Answer)) continue;
                cAttr.AttributeKey = qc.Key;
                cAttr.AttributeValue = qc.Answer;
                cAttr.SourceKey = "customer";
                custAttr.Add(cAttr);
            }
            var newCust = new ProfileCustomer
            {
                Id = jp.c,
                Attributes = custAttr
            };

            var acctAttr = new List<Common.AclaraJson.Attribute>();
            foreach (var qa in questAccount)
            {
                var aAttr = new Common.AclaraJson.Attribute();
                if (string.IsNullOrEmpty(qa.Answer)) continue;
                aAttr.AttributeKey = qa.Key;
                aAttr.AttributeValue = qa.Answer;
                aAttr.SourceKey = "customer";
                acctAttr.Add(aAttr);
            }
            var newAcct = new ProfileAccount
            {
                Id = jp.a,
                Attributes = acctAttr
            };
            var newAcctList = new List<ProfileAccount> { newAcct };
            newCust.Accounts = newAcctList;

            var premAttr = new List<Common.AclaraJson.Attribute>();
            foreach (var qp in questPremise)
            {
                var pAttr = new Common.AclaraJson.Attribute();
                if (string.IsNullOrEmpty(qp.Answer)) continue;
                pAttr.AttributeKey = qp.Key;
                pAttr.AttributeValue = qp.Answer;
                pAttr.SourceKey = "customer";
                premAttr.Add(pAttr);
            }
            var newPrem = new ProfilePremis
            {
                Id = jp.p,
                Attributes = premAttr
            };
            var newPremList = new List<ProfilePremis> { newPrem };
            newCust.Accounts[0].Premises = newPremList;

            var postStr1 = @"{ " +
                           "\"Customer\": " + JsonConvert.SerializeObject(newCust) +
                           "}";

            var rm = new APIResponseModels();
            rm.PostProfile(postStr1, "en-US", Convert.ToInt32(jp.cl));
            var p = new ProfileModels();
            p.UpdateProfileAttributesInCache(jp);
        }
    }

    public class Goal : ContentBase
    { 
        public string IntroTitle { get; set; }
        public string IntroTitle2 { get; set; }
        public string IntroSubtitle { get; set; }
        public string IntroSetgoalButton { get; set; }
        public string IntroSetgoalButtonDesc { get; set; }
        public string IntroMoreOptions { get; set; }
        public string IntroOr { get; set; }
        public string OptionsTitle { get; set; }
        public string OptionsDesc { get; set; }
        public string OptionsText { get; set; }
        public string OptionsRecommended { get; set; }
        public string OptionsCancelButton { get; set; }
        public string OptionsSaveButton { get; set; }
        public string MyGoalTitle { get; set; }
        public string MyGoalAmount { get; set; }
        public string MyGoalPlanAmount { get; set; }
        public string MyGoalAddMoreActions { get; set; }
        public string MyGoalEditButton { get; set; }
        public string MyGoalCreatePlan { get; set; }
        public string GoalType { get; set; }
        public string ActionsLink { get; set; }   
        public string ShowActionsLink { get; set; }
        public string OptionsShowRecommended { get; set; }
        public List<GoalAttribute> Attributes { get; set; }
        public List<GoalOption> GoalOptions { get; set; }
        public double PlanAmount { get; set; }
        public double GoalAmount { get; set; }
        public string PlanAmountFormatted { get; set; }
        public string GoalAmountFormatted { get; set; }
        public double RecommendedAmount { get; set; }
        public double RecommendedPercent { get; set; }
        public string HtmlTemplate { get; set; }
        public string WaysToSaveLink { get; set; }
        public string ShowWaysToSaveLink { get; set; }
        public string ViewMoreActions { get; set; }
        public string EnoughActions { get; set; }        
    }

    public class GoalAttribute
    {
        public string Key { get; set; }
        public string Answer { get; set; }
        public string Level { get; set; }
    }

    public class GoalOption
    {
        public string OptionText { get; set; }
        public double OptionValue { get; set; }
        public double OptionAmount { get; set; }
        public string Selected { get; set; }
    }
}