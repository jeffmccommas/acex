﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Caching;
using AutoMapper;
using CE.InsightsWeb.Common;
using CE.InsightsWeb.Common.AclaraJson;
using CE.InsightsWeb.Common.QueryStringEncryptor;
using CE.InsightsWeb.Helpers;
using Newtonsoft.Json;

namespace CE.InsightsWeb.Models
{
    public class WaysToSaveModels
    {
        private const string ACTION_CATEGORY = "action";
        private const string WIDGET_TYPE_ACTIONS = "actions";
        private const string WIDGET_TYPE_ACTIONPLAN = "actionplan";
        private const string COMMON_CATEGORY = "common";
        private const string PROMO_CATEGORY = "promo";

        public WaysToSave GetWaysToSave(string tabKey, string parameters, bool inPlan = false)
        {
            var jp = (JsonParams)JsonConvert.DeserializeObject(QueryStringModule.Decrypt(parameters), typeof(JsonParams));
            var rm = new APIResponseModels();
            //Content
            var content = CMS.GetContent(int.Parse(jp.cl), jp.l).Content;
            var actionTextContent = content.TextContent.Where(x => x.category == ACTION_CATEGORY || x.category == COMMON_CATEGORY).ToList();
            var actionContent = content.Action.ToList();
            var actionConfig = content.Configuration.Where(x => x.category == ACTION_CATEGORY).ToList();
            var actionFileContent = content.FileContent.Where(x => x.category == ACTION_CATEGORY).ToList();
            var actionEnduse = content.EndUse.ToList();
            var actionAppliance = content.Appliance.ToList();

            var tabs = new List<Tab>();
            var showTabs = string.Empty;
            var widgetType = WIDGET_TYPE_ACTIONS;
            if (inPlan) { widgetType = WIDGET_TYPE_ACTIONPLAN; }
            var waysToSave = new WaysToSave(actionConfig, actionTextContent, tabKey, widgetType, jp);
            if (widgetType == WIDGET_TYPE_ACTIONS)
            {
                showTabs = CMS.GetWidgetConfig(actionConfig, tabKey, widgetType, "showtabs");
                if (Convert.ToBoolean(showTabs))
                {
                    var widgetKey = content.Widget.FirstOrDefault(x => x.widgettype == WIDGET_TYPE_ACTIONS && x.tabkey == tabKey);
                    if (widgetKey != null)
                    {
                        tabs = content.Tab.Where(x => x.tabtype == "widget" && x.widgetkey == widgetKey.key).OrderBy(x => x.menuorder).ToList();
                    }
                }
            }

            var configuredCommodities = CMS.GetWidgetConfig(actionConfig, tabKey, widgetType, "commodities").Replace(" ", string.Empty);
            var actionTypes = CMS.GetWidgetConfig(actionConfig, tabKey, widgetType, "actiontypes").Replace(" ", string.Empty);

            //Get Actions
            var requestParams = "CustomerId=" + jp.c + "&AccountId=" + jp.a + "&PremiseId=" + jp.p + "&CommodityKeys=" + configuredCommodities + "&Types=" + actionTypes;
            var actionResponse = (ActionRoot)JsonConvert.DeserializeObject(rm.GetAction(requestParams, jp.l, int.Parse(jp.cl)).Content, typeof(ActionRoot));

            //Action plan
            var actionPlanResponse = GetActionPlanFromCache(jp);
            var am = new ActionMerge();

            if (actionResponse.Customer != null)
            {
                am.Actions = actionResponse.Customer.Accounts[0].Premises[0].Actions.Where(x => x.AnnualSavingsEstimate > 0).ToList();
            }
            am.PercentFormat = CMS.GetWidgetConfig(actionConfig, tabKey, widgetType, "percentformat");
            am.CurrencyFormat = CMS.GetWidgetConfig(actionConfig, tabKey, widgetType, "currencyformat");
            am.FileContent = actionFileContent;
            am.TextContent = actionTextContent;
            am.ActionContent = actionContent;
            am.EndUseContent = actionEnduse;
            am.ApplianceContent = actionAppliance;
            am.Language = jp.l;
            am.DetailActionLinkText = CMS.GetWidgetContent(actionTextContent, tabKey, widgetType, "detailactionlinktext");
            am.PerYearLabel = CMS.GetWidgetContent(actionTextContent, tabKey, widgetType, "peryear");
            am.PerYearDetailLabel = CMS.GetWidgetContent(actionTextContent, tabKey, widgetType, "detailperyear");

            if (actionPlanResponse.Customer != null && actionPlanResponse.Customer.Accounts.Count > 0 && actionPlanResponse.Customer.Accounts[0].Premises.Count > 0)
            {
                am.ActionsInPlan = inPlan ? actionPlanResponse.Customer.Accounts[0].Premises[0].ActionStatuses.Where(x => x.Status == "completed" || x.Status == "selected").ToList() : actionPlanResponse.Customer.Accounts[0].Premises[0].ActionStatuses.Where(x => x.Status == "completed" || x.Status == "selected" || x.Status == "rejected").ToList();
                am.RecommendedActionsInPlan = actionPlanResponse.Customer.Accounts[0].Premises[0].ActionStatuses.Where(x => x.Status == "completed" || x.Status == "selected").ToList();
            }

            var a = new List<WTSAction>();
            Mapper.CreateMap<ActionMerge, List<WTSAction>>().ConvertUsing(new ActionConverter());
            var mappedActions = Mapper.Map(am, a);

            if (mappedActions.Count > 0)
            {
                waysToSave.ToDoCount = mappedActions.Select(x => x.Plan).Where(x => x != null).Count(x => x.Status == "NA" || x.Status == "selected");
                waysToSave.CompletedCount = mappedActions.Select(x => x.Plan).Where(x => x != null).Count(x => x.Status == "completed");
            }

            if (!inPlan)
            {
                var minSavings = actionConfig.FirstOrDefault(x => x.key == tabKey + "." + widgetType + ".minsavings");
                var minSav = minSavings == null ? 0 : Convert.ToDouble(minSavings.value);
                var maxpaybacktime = actionConfig.FirstOrDefault(x => x.key == tabKey + "." + widgetType + ".maxpaybacktime");
                var maxPay = maxpaybacktime == null ? 0 : Convert.ToDouble(maxpaybacktime.value);

                mappedActions = mappedActions.Where(x => x.AnnualSavingsEstimate >= minSav && (x.UpfrontCost / x.AnnualSavingsEstimate) <= maxPay).ToList();
            }

            var t = new List<ActionTab>();
            if (tabs.Count > 0)
            {
                foreach (var ta in tabs)
                {
                    var tab = new ActionTab { Key = ta.key };
                    var name = content.TextContent.FirstOrDefault(x => x.category == ACTION_CATEGORY && x.key == ta.namekey);
                    tab.Name = name == null ? "" : name.shorttext;
                    tab.IconClass = ta.tabiconclass;

                    var actionsOnTab = ta.actions.Split(';');
                    tab.Actions = mappedActions.Where(p => actionsOnTab.Any(l => p.Key == l)).ToList();
                    t.Add(tab);
                }
            }
            else
            {
                var tab = new ActionTab { Actions = mappedActions.ToList() };
                t.Add(tab);
            }
            waysToSave.Tabs = t;

            //Content
            waysToSave.CostTitle = CMS.GetWidgetContent(actionTextContent, tabKey, widgetType, "costtitle");
            waysToSave.SavingsTitle = CMS.GetWidgetContent(actionTextContent, tabKey, widgetType, "savingstitle");
            waysToSave.NoActionsMessage = CMS.GetWidgetContent(actionTextContent, tabKey, widgetType, "noactionsmessage");
            waysToSave.PerYear = CMS.GetWidgetContent(actionTextContent, tabKey, widgetType, "peryear");
            waysToSave.AddedToDo = CMS.GetWidgetContent(actionTextContent, tabKey, widgetType, "addedtodo");
            waysToSave.AddedToDoOptions = CMS.GetWidgetContent(actionTextContent, tabKey, widgetType, "addedtodooptions");
            waysToSave.Completed = CMS.GetWidgetContent(actionTextContent, tabKey, widgetType, "completed");
            waysToSave.CompletedOptions = CMS.GetWidgetContent(actionTextContent, tabKey, widgetType, "completedoptions");
            waysToSave.WaysToSaveLinkText = CMS.GetWidgetContent(actionTextContent, tabKey, widgetType, "waystosavelinktext");
            waysToSave.YourPlan = CMS.GetWidgetContent(actionTextContent, tabKey, widgetType, "yourplan");
            waysToSave.ActionPlanLinkText = CMS.GetWidgetContent(actionTextContent, tabKey, widgetType, "actionplanlinktext");
            waysToSave.ToDoList = CMS.GetWidgetContent(actionTextContent, tabKey, widgetType, "todolist");
            waysToSave.CompletedList = CMS.GetWidgetContent(actionTextContent, tabKey, widgetType, "completedlist");

            //Configuration
            waysToSave.WaysToSaveLink = GeneralHelper.CreateLink("internal", jp, CMS.GetWidgetConfig(actionConfig, tabKey, widgetType, "waystosavelink"));
            waysToSave.ShowWaysToSaveLink = CMS.GetWidgetConfig(actionConfig, tabKey, widgetType, "showwaystosavelink");
            waysToSave.ActionPlanLink = GeneralHelper.CreateLink("internal", jp, CMS.GetWidgetConfig(actionConfig, tabKey, widgetType, "actionplanlink"));

            if (widgetType == WIDGET_TYPE_ACTIONS)
            {
                //Content 
                waysToSave.SortLabel = CMS.GetWidgetContent(actionTextContent, tabKey, widgetType, "sortlabel");
                waysToSave.SortByRecommended = CMS.GetWidgetContent(actionTextContent, tabKey, widgetType, "sortbyrecommended");
                waysToSave.SortBySavings = CMS.GetWidgetContent(actionTextContent, tabKey, widgetType, "sortbysavings");
                waysToSave.SortByEasiest = CMS.GetWidgetContent(actionTextContent, tabKey, widgetType, "sortbyeasiest");
                waysToSave.AddToPlan = CMS.GetWidgetContent(actionTextContent, tabKey, widgetType, "addtoplan");
                waysToSave.AddToPlanOptions = CMS.GetWidgetContent(actionTextContent, tabKey, widgetType, "addtoplanoptions");
                waysToSave.AddToPlanValues = "selected,completed";
                waysToSave.ViewAllButton = CMS.GetWidgetContent(actionTextContent, tabKey, widgetType, "viewallbutton");
                //Configuration
                waysToSave.ShowTabs = showTabs;
                waysToSave.ShowSort = CMS.GetWidgetConfig(actionConfig, tabKey, widgetType, "showsort");
                waysToSave.ShowPagination = CMS.GetWidgetConfig(actionConfig, tabKey, widgetType, "showpagination");
                waysToSave.ShowMoreSavingsLink = CMS.GetWidgetConfig(actionConfig, tabKey, widgetType, "showmoresavingslink");
                waysToSave.RowCount = CMS.GetWidgetConfig(actionConfig, tabKey, widgetType, "rowcount");
                waysToSave.DefaultSort = CMS.GetWidgetConfig(actionConfig, tabKey, widgetType, "defaultsort");
            }
            else
            {
                //Content 
                waysToSave.ToDoTitle = CMS.GetWidgetContent(actionTextContent, tabKey, widgetType, "todotitle");
                waysToSave.CompletedTitle = CMS.GetWidgetContent(actionTextContent, tabKey, widgetType, "completedtitle");
                waysToSave.Complete = CMS.GetWidgetContent(actionTextContent, tabKey, widgetType, "complete");
                waysToSave.CompleteOptions = CMS.GetWidgetContent(actionTextContent, tabKey, widgetType, "completeoptions");
                waysToSave.Move = CMS.GetWidgetContent(actionTextContent, tabKey, widgetType, "move");
                waysToSave.MoveOptions = CMS.GetWidgetContent(actionTextContent, tabKey, widgetType, "moveoptions");
                waysToSave.NotApplicableMessage = CMS.GetWidgetContent(actionTextContent, tabKey, widgetType, "notapplicablemessage");
            }
            var tempAct = new List<WTSAction>();
            Mapper.CreateMap<WTSAction, WTSAction>();
            foreach (var tt in waysToSave.Tabs)
            {
                foreach (var act in Mapper.Map(tt.Actions, tempAct))
                {
                    if (inPlan)
                    {
                        if (act.Plan == null)
                            tt.Actions.Remove(tt.Actions.FirstOrDefault(x => x.Key == act.Key));

                    }
                    else
                    {
                        if (act.Plan != null)
                            tt.Actions.Remove(tt.Actions.FirstOrDefault(x => x.Key == act.Key));
                    }
                }
            }

            //Action Detail           
            //Content 
            waysToSave.DetailAddToPlan = CMS.GetWidgetContent(actionTextContent, tabKey, widgetType, "detailaddtoplan");
            waysToSave.DetailIntroText = CMS.GetWidgetContent(actionTextContent, tabKey, widgetType, "detailintrotext");
            waysToSave.DetailAddToPlanOptions = CMS.GetWidgetContent(actionTextContent, tabKey, widgetType, "detailaddtoplanoptions");
            waysToSave.DetailCompleted = CMS.GetWidgetContent(actionTextContent, tabKey, widgetType, "detailcompleted");
            waysToSave.DetailCompletedOptions = CMS.GetWidgetContent(actionTextContent, tabKey, widgetType, "detailcompletedoptions");
            waysToSave.DetailAddedToDo = CMS.GetWidgetContent(actionTextContent, tabKey, widgetType, "detailaddedtodo");
            waysToSave.DetailAddedToDoOptions = CMS.GetWidgetContent(actionTextContent, tabKey, widgetType, "detailaddedtodooptions");
            waysToSave.DetailCategoryTitle = CMS.GetWidgetContent(actionTextContent, tabKey, widgetType, "detailcategorytitle");
            waysToSave.DetailCostTitle = CMS.GetWidgetContent(actionTextContent, tabKey, widgetType, "detailcosttitle");
            waysToSave.DetailSavingsTitle = CMS.GetWidgetContent(actionTextContent, tabKey, widgetType, "detailsavingstitle");
            waysToSave.DetailDifficultyTitle = CMS.GetWidgetContent(actionTextContent, tabKey, widgetType, "detaildifficultytitle");
            waysToSave.DetailPerYear = CMS.GetWidgetContent(actionTextContent, tabKey, widgetType, "detailperyear");
            waysToSave.DetailAddedMessage = CMS.GetWidgetContent(actionTextContent, tabKey, widgetType, "detailaddedmessage");
            waysToSave.DetailCompletedMessage = CMS.GetWidgetContent(actionTextContent, tabKey, widgetType, "detailcompletedmessage");
            waysToSave.DetailRemovedMessage = CMS.GetWidgetContent(actionTextContent, tabKey, widgetType, "detailremovedmessage");
            waysToSave.DetailActionPlanLinkText = CMS.GetWidgetContent(actionTextContent, tabKey, widgetType, "detailactionplanlinktext");
            waysToSave.DetailBackLinkText = CMS.GetWidgetContent(actionTextContent, tabKey, widgetType, "detailbacklinktext");
            waysToSave.DetailWhileYoureAtIt = CMS.GetWidgetContent(actionTextContent, tabKey, widgetType, "detailwhileyoureatit");
            waysToSave.DetailFree = CMS.GetWidgetContent(actionTextContent, tabKey, widgetType, "detailfree");
            waysToSave.DetailAddToPlanValues = "selected,completed";
            waysToSave.DetailFooter = CMS.GetWidgetContent(actionTextContent, tabKey, widgetType, "footer");
            waysToSave.DetailFooterLinkText = CMS.GetWidgetContent(actionTextContent, tabKey, widgetType, "footerlinktext");
            //Configuration	
            waysToSave.DetailShowActionPlanLink = CMS.GetWidgetConfig(actionConfig, tabKey, widgetType, "detailshowactionplanlink");
            waysToSave.DetailActionPlanLink = GeneralHelper.CreateLink("internal", jp, CMS.GetWidgetConfig(actionConfig, tabKey, widgetType, "detailactionplanlink"));
            waysToSave.DetailShowIntro = CMS.GetWidgetConfig(actionConfig, tabKey, widgetType, "detailshowintro");
            waysToSave.DetailShowBackLink = CMS.GetWidgetConfig(actionConfig, tabKey, widgetType, "detailshowbacklink");
            waysToSave.DetailFooterLink = CMS.GetWidgetConfig(actionConfig, tabKey, widgetType, "footerlink");
            waysToSave.DetailShowFooterLink = CMS.GetWidgetConfig(actionConfig, tabKey, widgetType, "showfooterlink");
            waysToSave.DetailShowFooterText = CMS.GetWidgetConfig(actionConfig, tabKey, widgetType, "showfootertext");


            return waysToSave;
        }

        public string PostWaysToSave(List<WTSActionPlan> plan, string parameters)
        {
            var jp = (JsonParams)JsonConvert.DeserializeObject(QueryStringModule.Decrypt(parameters), typeof(JsonParams));

            if (plan == null) return "";
            var mapCustomer = new ActionPlanCustomer();
            var mapAccount = new ActionPlanAccount();
            mapCustomer.Id = jp.c;
            mapAccount.Id = jp.a;
            //Premise
            var profilePremise = new ActionPlanPremis();

            Mapper.CreateMap<List<WTSActionPlan>, ActionPlanPremis>()
                .ForMember(dest => dest.ActionStatuses,
                    opt => opt.MapFrom(
                        src => Mapper.Map<List<WTSActionPlan>, List<ActionStatus>>(src)));

            Mapper.CreateMap<List<ActionStatus>, ActionPlanPremis>()
                .ForMember(dest => dest.ActionStatuses,
                    opt => opt.MapFrom(
                        src => Mapper.Map<List<ActionStatus>, List<ActionStatus>>(src)));

            Mapper.CreateMap<WTSActionPlan, ActionStatus>()
                .ForMember(dest => dest.ActionKey, opt => opt.MapFrom(src => src.Key))
                .ForMember(dest => dest.StatusDate, opt => opt.UseValue(null))
                .ForMember(dest => dest.SourceKey, opt => opt.UseValue("customer"));

            var mapPremise = Mapper.Map(plan, profilePremise);
            mapPremise.Id = jp.p;

            //Set
            mapAccount.Premises = new List<ActionPlanPremis> { mapPremise };
            mapCustomer.Accounts = new List<ActionPlanAccount> { mapAccount };

            var postStr1 = @"{ " +
                           "\"Customer\": " + JsonConvert.SerializeObject(mapCustomer) +
                           "}";

            var rm = new APIResponseModels();
            rm.PostActionPlan(postStr1, "en-US", Convert.ToInt32(jp.cl));
            UpdateActionPlanInCache(jp);
            return "";
        }

        public List<Promo> GetPromos(string tabKey, string parameters)
        {
            var jp = (JsonParams)JsonConvert.DeserializeObject(QueryStringModule.Decrypt(parameters), typeof(JsonParams));
            var rm = new APIResponseModels();
            //Content
            var content = CMS.GetContent(int.Parse(jp.cl), jp.l).Content;
            var actionTextContent = content.TextContent.Where(x => x.category == ACTION_CATEGORY || x.category == COMMON_CATEGORY || x.category == PROMO_CATEGORY).ToList();
            var actionContent = content.Action.ToList();
            var actionConfig = content.Configuration.Where(x => x.category == PROMO_CATEGORY || x.category == COMMON_CATEGORY).ToList();
            var actionFileContent = content.FileContent.Where(x => x.category == "promotion").ToList();

            var configuredCommodities = CMS.GetWidgetConfig(actionConfig, tabKey, "promo", "commodities").Replace(" ", string.Empty);
            var actionTypes = CMS.GetWidgetConfig(actionConfig, tabKey, "promo", "actiontypes").Replace(" ", string.Empty);

            //Get Actions
            var requestParams = "CustomerId=" + jp.c + "&AccountId=" + jp.a + "&PremiseId=" + jp.p + "&CommodityKeys=" + configuredCommodities + "&Types=" + actionTypes;
            var actionResponse = (ActionRoot)JsonConvert.DeserializeObject(rm.GetAction(requestParams, jp.l, int.Parse(jp.cl)).Content, typeof(ActionRoot));

            Mapper.CreateMap<Common.AclaraJson.Action, WTSAction>();
            var al = new List<WTSAction>();
            if (actionResponse.Customer != null)
            {
                foreach (var a in actionResponse.Customer.Accounts[0].Premises[0].Actions)
                {
                    var q = EntityMapper.Map<WTSAction>(a);
                    var act = actionContent.Where(p => p.key == a.Key).FirstOrDefault(p => p.type == "promotion");

                    if (act != null)
                    {
                        var imgContent = actionFileContent.FirstOrDefault(x => x.key == act.rebateimagekey);
                        if (imgContent != null)
                        {
                            q.RebateLargeImageUrl = imgContent.largefile;
                            q.RebateMediumImageUrl = imgContent.mediumfile;
                            q.RebateSmallImageUrl = imgContent.smallfile;
                            q.RebateUrl = act.rebateurl;
                        }
                        else
                        {
                            var descContent = actionTextContent.FirstOrDefault(x => x.key == act.descriptionkey);
                            q.PromoText = (descContent == null) ? "" : descContent.shorttext;
                        }
                        q.ActionPriority = act.actionpriority;
                        al.Add(q);
                    }
                }
            }

            var rowCount = actionConfig.FirstOrDefault(x => x.key == tabKey + ".promo.rowcount");
            var rCount = (rowCount == null) ? 1 : Convert.ToInt32(rowCount.value);
            al = al.OrderBy(x => x.ActionPriority).Take(rCount).ToList();

            Mapper.CreateMap<WTSAction, Promo>();
            var promo = new List<Promo>();
            Mapper.Map(al, promo);

            foreach (var p in promo)
            {
                p.ImageLowBreakPoint = actionConfig.Where(x => x.category == COMMON_CATEGORY && x.key == "imagelowbreakpoint").FirstOrDefault().value;
                p.ImageHighBreakPoint = actionConfig.Where(x => x.category == COMMON_CATEGORY && x.key == "imagehighbreakpoint").FirstOrDefault().value;

                p.Footer = CMS.GetWidgetContent(actionTextContent, tabKey, "promo", "footer");
                p.FooterLinkText = CMS.GetWidgetContent(actionTextContent, tabKey, "promo", "footerlinktext");

                p.FooterLink = GeneralHelper.CreateLink(CMS.GetWidgetConfig(actionConfig, tabKey, "promo", "footerlinktype"), jp, CMS.GetWidgetConfig(actionConfig, tabKey, "promo", "footerlink"));
                p.ShowFooterLink = CMS.GetWidgetConfig(actionConfig, tabKey, "promo", "showfooterlink");
                p.ShowFooterText = CMS.GetWidgetConfig(actionConfig, tabKey, "promo", "showfootertext");
            }
            return promo;
        }

        public static ActionPlanRoot GetActionPlanFromCache(JsonParams jp)
        {
            ActionPlanRoot actionPlanResponse;
            if (HttpRuntime.Cache["ActionPlan" + jp.cl + "" + jp.c + "" + jp.a + "" + jp.p + "" + jp.l + "" + jp.w] == null)
            {
                var rm = new APIResponseModels();
                var requestParams = "CustomerId=" + jp.c + "&AccountId=" + jp.a + "&PremiseId=" + jp.p;
                actionPlanResponse = (ActionPlanRoot)JsonConvert.DeserializeObject(rm.GetActionPlan(requestParams, jp.l, int.Parse(jp.cl)).Content, typeof(ActionPlanRoot));
                HttpRuntime.Cache.Insert("ActionPlan" + jp.cl + "" + jp.c + "" + jp.a + "" + jp.p + "" + jp.l + "" + jp.w, actionPlanResponse,
                    null, DateTime.Now.AddMinutes(Constants.kCache_Expiry), Cache.NoSlidingExpiration, CacheItemPriority.NotRemovable, null);
            }
            else
            {
                actionPlanResponse = (ActionPlanRoot)HttpRuntime.Cache["ActionPlan" + jp.cl + "" + jp.c + "" + jp.a + "" + jp.p + "" + jp.l + "" + jp.w];
            }

            return actionPlanResponse;
        }

        private static void UpdateActionPlanInCache(JsonParams jp)
        {
            var requestParams = "CustomerId=" + jp.c + "&AccountId=" + jp.a + "&PremiseId=" + jp.p;
            var rm = new APIResponseModels();
            var actionPlanResponse = (ActionPlanRoot)JsonConvert.DeserializeObject(rm.GetActionPlan(requestParams, jp.l, Convert.ToInt32(jp.cl)).Content, typeof(ActionPlanRoot));
            HttpRuntime.Cache.Insert("ActionPlan" + jp.cl + "" + jp.c + "" + jp.a + "" + jp.p + "" + jp.l + "" + jp.w, actionPlanResponse,
                null, DateTime.Now.AddMinutes(Constants.kCache_Expiry), Cache.NoSlidingExpiration, CacheItemPriority.NotRemovable, null);
        }
    }

    public class ActionConverter : ITypeConverter<ActionMerge, List<WTSAction>>
    {
        public List<WTSAction> Convert(ResolutionContext context)
        {
            var sourceCollection = (ActionMerge)context.SourceValue;

            Mapper.CreateMap<ActionMerge, List<WTSAction>>();
            Mapper.CreateMap<Common.AclaraJson.Action, WTSAction>();
            var ci = new CultureInfo(sourceCollection.Language);
            var al = new List<WTSAction>();
            foreach (var a in sourceCollection.Actions)
            {
                var q = EntityMapper.Map<WTSAction>(a);
                var actionInPlan = sourceCollection.ActionsInPlan?.Where(p => p.ActionKey == a.Key).FirstOrDefault();
                if (actionInPlan != null)
                {
                    Mapper.CreateMap<ActionStatus, WTSActionPlan>()
                        .ForMember(dest => dest.Key, opt => opt.MapFrom(src => src.ActionKey));
                    var plan = EntityMapper.Map<WTSActionPlan>(actionInPlan);
                    q.Plan = plan;
                }
                var act = sourceCollection.ActionContent.FirstOrDefault(p => p.key == a.Key);

                RecommendedActions(sourceCollection, act, q);

                if (sourceCollection.PercentFormat.Length > 0)
                {
                    q.CostVariancePercentFormatted = a.CostVariancePercent.ToString(sourceCollection.PercentFormat, ci);
                }
                if (sourceCollection.CurrencyFormat.Length > 0)
                {
                    q.AnnualCostFormatted = a.AnnualCost.ToString(sourceCollection.CurrencyFormat, ci);
                    q.UpfrontCostFormatted = a.UpfrontCost.ToString(sourceCollection.CurrencyFormat, ci);
                    q.AnnualSavingsEstimateFormatted = a.AnnualSavingsEstimate.ToString(sourceCollection.CurrencyFormat, ci) + sourceCollection.PerYearLabel;
                }
                if (act != null)
                {
                    q.ActionPriority = act.actionpriority;
                    sourceCollection.ImageSize = Enums.ImageSize.Small;
                    SetupAction(sourceCollection, act, q);
                }
                al.Add(q);
            }

            //Actions in plan that are no longer recommended.
            if (sourceCollection.ActionsInPlan != null)
            {
                var actionsNotValid = sourceCollection.ActionsInPlan.Where(p => sourceCollection.Actions.All(p2 => p2.Key != p.ActionKey));
                foreach (var a in actionsNotValid)
                {
                    var q = new WTSAction { Key = a.ActionKey };
                    Mapper.CreateMap<ActionStatus, WTSActionPlan>()
                        .ForMember(dest => dest.Key, opt => opt.MapFrom(src => src.ActionKey));
                    var plan = EntityMapper.Map<WTSActionPlan>(a);
                    plan.Status = "NA";
                    q.Plan = plan;

                    var act = sourceCollection.ActionContent.FirstOrDefault(p => p.key == a.ActionKey);
                    sourceCollection.ImageSize = Enums.ImageSize.Small;
                    SetupAction(sourceCollection, act, q);
                    al.Add(q);
                }
            }

            return al.OrderBy(x => x.ActionPriority).ToList();
        }

        private static void RecommendedActions(ActionMerge sourceCollection, Common.Action act, WTSAction q)
        {
            var adList = new List<WTSAction>();
            if (act.appliancekeys == null) act.appliancekeys = "bogusappliance";
            var applianceKeys = act.appliancekeys.Split(',').ToList();

            string enduseNameKey;
            try
            {
                enduseNameKey = sourceCollection.EndUseContent.FirstOrDefault(x => x.appliances.Contains(applianceKeys.First())).namekey;
                q.Category = sourceCollection.TextContent.FirstOrDefault(x => x.key == enduseNameKey).shorttext;
            }
            catch
            {
                enduseNameKey = sourceCollection.ApplianceContent.FirstOrDefault(x => x.key.Contains(applianceKeys.First())).namekey;
                q.Category = sourceCollection.TextContent.FirstOrDefault(x => x.key == enduseNameKey).shorttext;
            }

            List<Common.Action> actDetail;
            if (sourceCollection.RecommendedActionsInPlan != null)
            {
                actDetail = sourceCollection.ActionContent.Where(p => sourceCollection.Actions.Any(p2 => p2.Key == p.key)  // Actions recommended
                                                                      && p.key != act.key && sourceCollection.RecommendedActionsInPlan.All(p2 => p2.ActionKey != p.key)  // Actions not in plan and not this action
                                                                      && applianceKeys.Any(t1 => p.appliancekeys != null && p.appliancekeys.Contains(t1))).OrderBy(x => x.actionpriority).ToList(); //Actions that have appliance keys that match this actions                
            }
            else
            {
                actDetail = sourceCollection.ActionContent.Where(p => sourceCollection.Actions.Any(p2 => p2.Key == p.key)  // Actions recommended
                                                                      && p.key != act.key && applianceKeys.Any(t1 => p.appliancekeys != null && p.appliancekeys.Contains(t1))).OrderBy(x => x.actionpriority).ToList(); //Actions that have appliance keys that match this actions  
            }

            PopulateDetails(adList, actDetail, 1, sourceCollection);
            //enduse
            if (adList.Count < 3)
            {
                var applianceInEndUse = sourceCollection.EndUseContent.Where(x => applianceKeys.Any(t1 => x.appliances.Contains(t1))).ToList();
                List<string> applianceKeys2 = null;
                foreach (var ap in applianceInEndUse)
                {
                    if (applianceKeys2 == null)
                    {
                        applianceKeys2 = ap.appliances.Split(';').ToList();
                    }
                    else
                    {
                        var apk = ap.appliances.Split(';').ToList();
                        applianceKeys2.AddRange(apk);
                    }
                }

                if (applianceKeys2 != null)
                {
                    applianceKeys2 = (from w in applianceKeys2
                        select w).Distinct().ToList();
                }
                else
                {
                    applianceKeys2 = "bogus".Split(';').ToList();
                }

                List<Common.Action> actDetail2;
                if (sourceCollection.RecommendedActionsInPlan != null)
                {
                    actDetail2 = sourceCollection.ActionContent.Where(p => sourceCollection.Actions.Any(p2 => p2.Key == p.key)
                                                                           && p.key != act.key && sourceCollection.RecommendedActionsInPlan.All(p2 => p2.ActionKey != p.key)
                                                                           && applianceKeys2.Any(t1 => p.appliancekeys != null && p.appliancekeys.Contains(t1))).OrderBy(x => x.actionpriority).ToList();

                }
                else
                {
                    actDetail2 = sourceCollection.ActionContent.Where(p => sourceCollection.Actions.Any(p2 => p2.Key == p.key)
                                                                           && p.key != act.key && applianceKeys2.Any(t1 => p.appliancekeys != null && p.appliancekeys.Contains(t1))).OrderBy(x => x.actionpriority).ToList();

                }

                PopulateDetails(adList, actDetail2, 2, sourceCollection);
            }
            //Select action with highest priority
            if (adList.Count < 3)
            {
                List<Common.Action> actDetail3;
                if (sourceCollection.RecommendedActionsInPlan != null)
                {
                    actDetail3 = sourceCollection.ActionContent.Where(p => sourceCollection.Actions.Any(p2 => p2.Key == p.key)
                                                                           && p.key != act.key && sourceCollection.RecommendedActionsInPlan.All(p2 => p2.ActionKey != p.key)).OrderBy(x => x.actionpriority).ToList();

                }
                else
                {
                    actDetail3 = sourceCollection.ActionContent.Where(p => sourceCollection.Actions.Any(p2 => p2.Key == p.key)
                                                                           && p.key != act.key).OrderBy(x => x.actionpriority).ToList();

                }

                PopulateDetails(adList, actDetail3, 3, sourceCollection);
            }

            q.RecommendedActions = adList.Take(3).ToList();
        }

        private static void SetupAction(ActionMerge sourceCollection, Common.Action act, WTSAction q)
        {
            var imgContent = sourceCollection.FileContent.FirstOrDefault(x => x.key == act.imagekey);
            switch (sourceCollection.ImageSize)
            {
                case Enums.ImageSize.Small:
                    if (imgContent != null)
                    {
                        q.ImageUrl = imgContent.smallfile;
                        q.ImageTitle = imgContent.smallfiletitle;
                    }
                    break;
                case Enums.ImageSize.Medium:
                    if (imgContent != null)
                    {
                        q.ImageUrl = imgContent.mediumfile;
                        q.ImageTitle = imgContent.mediumfiletitle;
                    }
                    break;
                case Enums.ImageSize.Large:
                    if (imgContent != null)
                    {
                        q.ImageUrl = imgContent.largefile;
                        q.ImageTitle = imgContent.largefiletitle;
                    }
                    break;
            }

            q.Title = (act == null) ? "" : sourceCollection.TextContent.Where(p => p.key == act.namekey).FirstOrDefault().shorttext;
            q.MarkDown = sourceCollection.DetailActionLinkText.Replace("{%actionname%}", q.Title).Replace("{%actionlink%}", "iws_ad_ra-" + q.Key);
            var desc = new Common.TextContent();
            if (act != null) { desc = sourceCollection.TextContent.FirstOrDefault(p => p.key == act.descriptionkey); }
            if (desc != null)
            {
                q.Description = act == null ? "" : desc.shorttext;
                q.DescriptionMarkDown = act == null ? "" : desc.longtext;
            }

            switch (act.difficulty)
            {
                case "low":
                    q.Difficulty = 0;
                    break;
                case "medium":
                    q.Difficulty = 1;
                    break;
                case "high":
                    q.Difficulty = 2;
                    break;
                default:
                    q.Difficulty = 3;
                    break;
            }
        }

        private static void PopulateDetails(List<WTSAction> adList, List<Common.Action> actDetail, int criteria, ActionMerge sourceCollection)
        {
            var ci = (NumberFormatInfo)CultureInfo.CurrentCulture.NumberFormat.Clone();
            ci.CurrencyNegativePattern = 1;
            ci.PercentPositivePattern = 1;

            Mapper.CreateMap<WTSAction, List<WTSAction>>();
            Mapper.CreateMap<Common.AclaraJson.Action, WTSAction>();
            if (actDetail != null)
            {
                foreach (var adetail in actDetail)
                {
                    if (adList.Count < 3)
                    {
                        if (adList.FirstOrDefault(x => x.Key == adetail.key) == null)
                        {
                            var actionDetail = sourceCollection.Actions.FirstOrDefault(x => x.Key == adetail.key);
                            var ad = EntityMapper.Map<WTSAction>(actionDetail);
                            var act = sourceCollection.ActionContent.FirstOrDefault(p => p.key == actionDetail.Key);

                            ad.Title = sourceCollection.TextContent.Where(p => p.key == adetail.namekey).FirstOrDefault().shorttext;
                            ad.Criteria = criteria;
                            ad.Key = adetail.key;
                            ad.MarkDown = sourceCollection.DetailActionLinkText.Replace("{%actionname%}", ad.Title).Replace("{%actionlink%}", "iws_ad_ra-" + adetail.key);
                            var desc = sourceCollection.TextContent.FirstOrDefault(p => p.key == adetail.descriptionkey);
                            ad.DescriptionMarkDown = (desc == null) ? "" : desc.longtext;

                            if (sourceCollection.PercentFormat.Length > 0)
                            {
                                ad.CostVariancePercentFormatted = ad.CostVariancePercent.ToString(sourceCollection.PercentFormat, ci);
                            }
                            if (sourceCollection.CurrencyFormat.Length > 0)
                            {
                                ad.AnnualCostFormatted = ad.AnnualCost.ToString(sourceCollection.CurrencyFormat, ci);
                                ad.UpfrontCostFormatted = ad.UpfrontCost.ToString(sourceCollection.CurrencyFormat, ci);
                                ad.AnnualSavingsEstimateFormatted = ad.AnnualSavingsEstimate.ToString(sourceCollection.CurrencyFormat, ci) + sourceCollection.PerYearDetailLabel;
                            }
                            ad.ActionPriority = act.actionpriority;
                            sourceCollection.ImageSize = Enums.ImageSize.Medium;
                            SetupAction(sourceCollection, act, ad);

                            adList.Add(ad);
                        }
                    }
                    else { break; }
                }
            }
        }
    }

    public class ActionMerge
    {
        public List<Common.AclaraJson.Action> Actions { get; set; }
        public List<ActionStatus> ActionsInPlan { get; set; }
        public List<ActionStatus> RecommendedActionsInPlan { get; set; }
        public string TabKey { get; set; }
        public List<FileContent> FileContent { get; set; }
        public List<Common.EndUse> EndUseContent { get; set; }
        public List<Common.TextContent> TextContent { get; set; }
        public List<Common.Appliance> ApplianceContent { get; set; }
        public List<Common.Action> ActionContent { get; set; }
        public Enums.ImageSize ImageSize { get; set; }
        public string PercentFormat { get; set; }
        public string CurrencyFormat { get; set; }
        public string Language { get; set; }
        public string DetailActionLinkText { get; set; }
        public string PerYearLabel { get; set; }
        public string PerYearDetailLabel { get; set; }
        public JsonParams QueryString { get; set; }
        public List<BillHighlight> BillHighlights { get; set; }
        public List<AmiHighlight> AmiHighlights { get; set; }
        public List<Common.Configuration> Config { get; set; }
        public string WidgetType { get; set; }

    }

    public class WaysToSave : ContentBase
    {
        public string Subtitle { get; set; }
        public string SortLabel { get; set; }
        public string SortByRecommended { get; set; }
        public string SortBySavings { get; set; }
        public string SortByEasiest { get; set; }
        public string AddToPlan { get; set; }
        public string AddToPlanOptions { get; set; }
        public string AddToPlanValues { get; set; }
        public string CostTitle { get; set; }
        public string SavingsTitle { get; set; }
        public string ViewAllButton { get; set; }
        public string ShowTabs { get; set; }
        public string ShowSort { get; set; }
        public string ShowPagination { get; set; }
        public string ShowMoreSavingsLink { get; set; }
        public string RowCount { get; set; }
        public string DefaultSort { get; set; }
        public string WaysToSaveLink { get; set; }
        public string WaysToSaveLinkText { get; set; }
        public string NoActionsMessage { get; set; }
        public string PerYear { get; set; }
        public string ToDoTitle { get; set; }
        public string CompletedTitle { get; set; }
        public int ToDoCount { get; set; }
        public int CompletedCount { get; set; }
        public string Complete { get; set; }
        public string CompleteOptions { get; set; }
        public string Move { get; set; }
        public string MoveOptions { get; set; }
        public string ShowWaysToSaveLink { get; set; }
        public string NotApplicableMessage { get; set; }
        public string AddedToDo { get; set; }
        public string AddedToDoOptions { get; set; }
        public string Completed { get; set; }
        public string CompletedOptions { get; set; }
        public string YourPlan { get; set; }
        public string ToDoList { get; set; }
        public string CompletedList { get; set; }
        public string ActionPlanLink { get; set; }
        public string DetailIntroText { get; set; }
        public string DetailAddToPlan { get; set; }
        public string DetailAddToPlanOptions { get; set; }
        public string DetailCompleted { get; set; }
        public string DetailCompletedOptions { get; set; }
        public string DetailAddedToDo { get; set; }
        public string DetailAddedToDoOptions { get; set; }
        public string DetailCategoryTitle { get; set; }
        public string DetailCostTitle { get; set; }
        public string DetailSavingsTitle { get; set; }
        public string DetailDifficultyTitle { get; set; }
        public string DetailPerYear { get; set; }
        public string DetailAddedMessage { get; set; }
        public string DetailCompletedMessage { get; set; }
        public string DetailRemovedMessage { get; set; }
        public string DetailActionPlanLinkText { get; set; }
        public string DetailWhileYoureAtIt { get; set; }
        public string DetailFree { get; set; }
        public string DetailShowActionPlanLink { get; set; }
        public string DetailActionPlanLink { get; set; }
        public string DetailAddToPlanValues { get; set; }
        public string DetailShowIntro { get; set; }
        public string DetailShowBackLink { get; set; }
        public string DetailBackLinkText { get; set; }
        public string DetailFooter { get; set; }
        public string DetailFooterLinkText { get; set; }
        public string DetailFooterLink { get; set; }
        public string DetailShowFooterLink { get; set; }
        public string DetailShowFooterText { get; set; }
        public string ActionPlanLinkText { get; set; }
        public List<ActionTab> Tabs { get; set; }
        public WaysToSave(List<Common.Configuration> config, List<Common.TextContent> content, string tabKey, string widetType, JsonParams jp) : base(config, content, tabKey, widetType, jp)
        { }
    }

    public class ActionTab
    {
        public string Key { get; set; }
        public string Name { get; set; }
        public string IconClass { get; set; }
        public List<WTSAction> Actions { get; set; }
    }

    public class WTSAction
    {
        public string Key { get; set; }
        public double AnnualCost { get; set; }
        public double UpfrontCost { get; set; }
        public double CostVariancePercent { get; set; }
        public double AnnualSavingsEstimate { get; set; }
        public double AnnualUsageSavingsEstimate { get; set; }
        public string AnnualUsageSavingsUOMKey { get; set; }
        public string SavingsEstimateQuality { get; set; }
        public double PaybackTime { get; set; }
        public double ROI { get; set; }
        public string Popularity { get; set; }
        public double ActionPriority { get; set; }
        public int Difficulty { get; set; }
        public string ImageUrl { get; set; }
        public string ImageTitle { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string DescriptionMarkDown { get; set; }
        public string AnnualCostFormatted { get; set; }
        public string UpfrontCostFormatted { get; set; }
        public string CostVariancePercentFormatted { get; set; }
        public string AnnualSavingsEstimateFormatted { get; set; }
        public string AnnualUsageSavingsEstimateFormatted { get; set; }
        public string Category { get; set; }
        public string MarkDown { get; set; }
        public int Criteria { get; set; }
        public string RebateAmount { get; set; }
        public string RebateUrl { get; set; }
        public string RebateLargeImageUrl { get; set; }
        public string RebateMediumImageUrl { get; set; }
        public string RebateSmallImageUrl { get; set; }
        public string PromoText { get; set; }
        public WTSActionPlan Plan { get; set; }
        public List<WTSAction> RecommendedActions { get; set; }
    }

    public class WTSActionPlan
    {
        public string Key { get; set; }
        public string Status { get; set; }
        public string StatusDate { get; set; }
        public string SourceKey { get; set; }
        public List<AdditionalInfo> AdditionalInfo { get; set; }
        public string SubActionKey { get; set; }
    }

    public class Promo
    {
        public string Key { get; set; }
        public string RebateLargeImageUrl { get; set; }
        public string RebateMediumImageUrl { get; set; }
        public string RebateSmallImageUrl { get; set; }
        public string RebateUrl { get; set; }
        public string ImageLowBreakPoint { get; set; }
        public string ImageHighBreakPoint { get; set; }
        public string PromoText { get; set; }
        public string Footer { get; set; }
        public string FooterLinkText { get; set; }
        public string FooterLink { get; set; }
        public string ShowFooterLink { get; set; }
        public string ShowFooterText { get; set; }
    }
}