﻿using System.Collections.Generic;
using System.Linq;
using CE.InsightsWeb.Common;
using CE.InsightsWeb.Common.QueryStringEncryptor;
using Newtonsoft.Json;

namespace CE.InsightsWeb.Models
{
    public class ContentModels
    {
        private const string CONTENT_CATEGORY = "content";
        private const string WIDGET_TYPE_CONTENT = "content";

        public Content GetContent(string tabKey, string parameters, string instance)
        {
            var jp = (JsonParams)JsonConvert.DeserializeObject(QueryStringModule.Decrypt(parameters), typeof(JsonParams));
            //Content
            var content = CMS.GetContent(int.Parse(jp.cl), jp.l).Content;
            var textContent = content.TextContent.Where(x => x.category == CONTENT_CATEGORY).ToList();
            var textConfig = content.Configuration.Where(x => x.category == CONTENT_CATEGORY).ToList();
            var c = new Content(textConfig, textContent, tabKey, WIDGET_TYPE_CONTENT, jp);
            if (string.IsNullOrEmpty(instance))
            {
                c.NoContent = true;
                return c;
            }

            var key = tabKey + "." + CONTENT_CATEGORY + ".markdown." + instance;
            var txtContent = textContent.Where(x => x.key == key).ToList();
            c.ShortText = txtContent.Select(x => x.shorttext).FirstOrDefault();
            c.MediumText = txtContent.Select(x => x.mediumtext).FirstOrDefault();
            c.LongText = txtContent.Select(x => x.longtext).FirstOrDefault();
            if (string.IsNullOrEmpty(c.ShortText) && string.IsNullOrEmpty(c.MediumText) && string.IsNullOrEmpty(c.LongText))
            {
                c.NoContent = true;
                return c;
            }
           
            //Configuration
            c.ImageLowBreakPoint = CMS.GetCommonContent(content, "imagelowbreakpoint", "config");
            c.ImageHighBreakPoint = CMS.GetCommonContent(content, "imagehighbreakpoint", "config");

            return c;
        }
    }

    public class Content : ContentBase
    {     
        public string ShortText { get; set; }
        public string MediumText { get; set; }
        public string LongText { get; set; }
        public string ImageLowBreakPoint { get; set; }
        public string ImageHighBreakPoint { get; set; }
        public bool NoContent { get; set; }
        public Content(List<Configuration> config, List<TextContent> content, string tabKey, string widetType, JsonParams jp) : base(config, content, tabKey, widetType, jp)
        { }
    }
}

