﻿
using System.Configuration;


namespace CE.InsightsWeb.Helpers
{
    public class OfflineHelper
    {
        public static string OfflineMode { get; private set; }

        /// <summary>
        /// This is true if we should redirect the user to the Offline View
        /// </summary>
        public bool ThisUserShouldBeOffline { get; private set; }

        public OfflineHelper()
        {

            var offlineMode = ConfigurationManager.AppSettings["OfflineMode"];

            //Offline mode not defined. Assume online.
            if (offlineMode == null)
            {
                OfflineMode = string.Empty;
                ThisUserShouldBeOffline = false;
                return;
            }

            switch (offlineMode.ToLower())
            {
                case "maintenance": // Offline - mainteance window.
                    OfflineMode = offlineMode;
                    ThisUserShouldBeOffline = true;
                    break;
                case "failover": // Offline - failover from primary region.
                    OfflineMode = offlineMode;
                    ThisUserShouldBeOffline = true;
                    break;
                default: // Online.
                    OfflineMode = string.Empty;
                    ThisUserShouldBeOffline = false;

                    break;
            }
        }
    }

}