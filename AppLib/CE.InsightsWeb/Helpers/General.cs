﻿using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using CE.InsightsWeb.Common;
using CE.InsightsWeb.Common.QueryStringEncryptor;

namespace CE.InsightsWeb.Helpers
{
    public static class GeneralHelper
    {
        #region Declarations
        private const string BRANDING_PATH = @"\Views\Branding\";
        private const string HEADER_VIEW = @"_{%AclaraClientId%}_Header";
        private const string HEADER_REMOTE_VIEW = @"_{%AclaraClientId%}_Header_Remote";
        private const string FOOTER_VIEW = @"_{%AclaraClientId%}_Footer";
        private const string FOOTER_REMOTE_VIEW = @"_{%AclaraClientId%}_Footer_Remote";
        private const string SIDENAV_REMOTE_VIEW = @"_{%AclaraClientId%}_SideNav_Remote";
        private const string SIDENAVBEGIN_VIEW = @"_{%AclaraClientId%}_SideNavBegin";
        private const string SIDENAVEND_VIEW = @"_{%AclaraClientId%}_SideNavEnd";
        private const string IFRAME_VIEW = @"_{%AclaraClientId%}_IFrame";
        private const string ACLARA_CLIENT_ID_REP = "{%AclaraClientId%}";
        #endregion

        public static string BuildQuerystring(HttpRequestBase request)
        {
            var qs = new StringBuilder();
            qs.Append("&" + Constants.kClientId + "=" + request[Constants.kClientId]
                      + "&" + Constants.kCustomerId + "=" + request[Constants.kCustomerId]
                      + "&" + Constants.kAccountId + "=" + request[Constants.kAccountId]
                      + "&" + Constants.kPremiseId + "=" + request[Constants.kPremiseId]
                      + "&" + Constants.kServicePointId + "=" + request[Constants.kServicePointId]
                      + "&" + Constants.kWebToken + "=" + request[Constants.kWebToken]
                      + "&" + Constants.kLocale + "=" + request[Constants.kLocale]
                      + "&" + Constants.kUserId + "=" + request[Constants.kUserId]
                      + "&" + Constants.kGroupName + "=" + request[Constants.kGroupName]
                      + "&" + Constants.kRoleName + "=" + request[Constants.kRoleName]
                      + "&" + Constants.kShowSubTabs + "=" + request[Constants.kShowSubTabs]);

            return qs.ToString();
        }

        private static string BuildQuerystring(JsonParams parameters, string[,] additionalParam = null)
        {
            var qs = new StringBuilder();
            if (additionalParam == null)
            {
                qs.Append("&" + Constants.kClientId + "=" + parameters.cl
                          + "&" + Constants.kCustomerId + "=" + parameters.c
                          + "&" + Constants.kAccountId + "=" + parameters.a
                          + "&" + Constants.kPremiseId + "=" + parameters.p
                          + "&" + Constants.kServicePointId + "=" + parameters.s
                          + "&" + Constants.kWebToken + "=" + parameters.w
                          + "&" + Constants.kLocale + "=" + parameters.l
                          + "&" + Constants.kUserId + "=" + parameters.u
                          + "&" + Constants.kGroupName + "=" + parameters.g
                          + "&" + Constants.kRoleName + "=" + parameters.r
                          + "&" + Constants.kShowSubTabs + "=" + parameters.st);
            }
            else
            {
                qs.Append("&" + Constants.kClientId + "=" + parameters.cl
                         + "&" + Constants.kCustomerId + "=" + parameters.c
                         + "&" + Constants.kAccountId + "=" + parameters.a
                         + "&" + Constants.kPremiseId + "=" + parameters.p
                         + "&" + Constants.kServicePointId + "=" + parameters.s
                         + "&" + Constants.kWebToken + "=" + parameters.w
                         + "&" + Constants.kLocale + "=" + parameters.l
                         + "&" + Constants.kUserId + "=" + parameters.u
                         + "&" + Constants.kGroupName + "=" + parameters.g
                         + "&" + Constants.kRoleName + "=" + parameters.r
                         + "&" + Constants.kShowSubTabs + "=" + parameters.st);

                for (var i = 0; i < additionalParam.GetLength(0); i++)
                {
                    qs.Append("&" + additionalParam[i, 0] + "=" + additionalParam[i, 1]);
                }
            }

            return qs.ToString();
        }

        public static string GetBrowser()
        {
            if (HttpContext.Current.Request.UserAgent.Contains("Firefox"))
            {
                // Firefox Detected
                return "-firefox";
            }
            if (HttpContext.Current.Request.UserAgent.Contains("Chrome"))
            {
                // Chrome Detected
                return "-chrome";
            }
            if (HttpContext.Current.Request.UserAgent.Contains("Edge"))
            {
                // Edge Detected
                return "-edge";
            }
            if (HttpContext.Current.Request.Browser.Browser.Equals("Safari"))
            {
                // Safari Detected
                return "-desktop";
            }
            //For IE11, the Browser is being shown as "Mozilla" and not "IE"
            //So, we can check for "Trident/7.0", which is the new render engine for IE11
            if (HttpContext.Current.Request.UserAgent.Contains("Trident/7.0"))
            {
                if (HttpContext.Current.Request.Browser.MajorVersion == 11)
                {
                    // IE11 detected 
                    return "-IE11";
                }
            }
            if (!HttpContext.Current.Request.Browser.Browser.Equals("IE") &&
                !HttpContext.Current.Request.Browser.Browser.Equals("IEMobile")) return "";
            switch (HttpContext.Current.Request.Browser.MajorVersion)
            {
                case 10:
                    // IE10 detected 
                    return "-IE10";
                    //case 9:
                    //    // IE9 detected 
                    //    return "-IE9;partial";
            }
            return "";
        }

        //private static void BuildCopyright(string copyrightText, UserSessionVariables usv)
        //{
        //    string cacheKeyCopyright = CEResidentialWeb.CACHEKEYS_COPYRIGHT + usv.AclaraClientId.ToString();
        //    if (HttpRuntime.Cache[cacheKeyCopyright] == null)
        //    {
        //        copyrightText = copyrightText.Replace("{%CurrentYear%}", DateTime.Now.Year.ToString());
        //        HttpRuntime.Cache.Insert(cacheKeyCopyright, copyrightText, null, System.DateTime.Now.AddMinutes(CEResidentialWeb.CACHE_EXPIRY), Cache.NoSlidingExpiration, CacheItemPriority.NotRemovable, null);
        //    }
        //}

        public static void Compress(ActionExecutingContext filterContext)
        {
            // - COMPRESS
            var request = filterContext.HttpContext.Request;
            var acceptEncoding = request.Headers["Accept-Encoding"];
            if (string.IsNullOrEmpty(acceptEncoding))
                return;
            acceptEncoding = acceptEncoding.ToUpperInvariant();
            var response = filterContext.HttpContext.Response;
            if (response.Filter == null) return;
            if (acceptEncoding.Contains("GZIP"))
            {
                response.AppendHeader("Content-encoding", "gzip");
                response.Filter = new GZipStream(response.Filter, CompressionMode.Compress);
            }
            else if (acceptEncoding.Contains("DEFLATE"))
            {
                response.AppendHeader("Content-encoding", "deflate");
                response.Filter = new DeflateStream(response.Filter, CompressionMode.Compress);
            }

            // - REMOVE WHITE SPACE 
            //response.Filter = new WhitespaceFilter(response.Filter);
        }

        private static MvcHtmlString RenderPartialViewToString(this Controller controller, string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = controller.ControllerContext.RouteData.GetRequiredString("action");

            controller.ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
                var viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return MvcHtmlString.Create(sw.GetStringBuilder().ToString());
            }
        }

        public static MvcHtmlString GetPartialView(this Controller controller, Enums.PartialView view, string clientId)
        {
            if (clientId == null)
            {
                return MvcHtmlString.Create("");
            }
            var viewName = string.Empty;
            switch (view)
            {
                case Enums.PartialView.Footer:
                    viewName = FOOTER_VIEW.Replace(ACLARA_CLIENT_ID_REP, clientId);
                    break;
                case Enums.PartialView.FooterRemote:
                    viewName = FOOTER_REMOTE_VIEW.Replace(ACLARA_CLIENT_ID_REP, clientId);
                    break;
                case Enums.PartialView.Header:
                    viewName = HEADER_VIEW.Replace(ACLARA_CLIENT_ID_REP, clientId);
                    break;
                case Enums.PartialView.SideNavRemote:
                    viewName = SIDENAV_REMOTE_VIEW.Replace(ACLARA_CLIENT_ID_REP, clientId);
                    break;
                case Enums.PartialView.SideNavBegin:
                    viewName = SIDENAVBEGIN_VIEW.Replace(ACLARA_CLIENT_ID_REP, clientId);
                    break;
                case Enums.PartialView.SideNavEnd:
                    viewName = SIDENAVEND_VIEW.Replace(ACLARA_CLIENT_ID_REP, clientId);
                    break;
                case Enums.PartialView.Frame:
                    viewName = IFRAME_VIEW.Replace(ACLARA_CLIENT_ID_REP, clientId);
                    break;
            }

            var area = controller.RouteData.DataTokens["area"] != null ? @"\Areas\" + controller.RouteData.DataTokens["area"] + BRANDING_PATH : BRANDING_PATH;

            if (!CheckFileExists(BRANDING_PATH, viewName) && !CheckFileExists(area, viewName))
                return MvcHtmlString.Create("");
            var partialView = RenderPartialViewToString(controller, viewName, null);
            return partialView;
        }

        //this function is required for the jquery modifications to allow the client to load their own version of jquery.
        //because the clients version needs to be loaded first then we need to load the clients header in the <head> tag instead of the body tag
        //as we were before.  This causes issues if the header contains html as well as scripts.
        //this function allows us to get only the scripts (to load in the header) or just the html content (to load in the body)
        public static MvcHtmlString GetPartialViewHeader(this Controller controller, Enums.PartialView view, string clientId, bool getScripts)
        {

            if (clientId == null)
            {
                return MvcHtmlString.Create("");
            }
            var viewName = HEADER_REMOTE_VIEW.Replace(ACLARA_CLIENT_ID_REP, clientId);

            var area = controller.RouteData.DataTokens["area"] != null ? @"\Areas\" + controller.RouteData.DataTokens["area"] + BRANDING_PATH : BRANDING_PATH;

            if (!CheckFileExists(BRANDING_PATH, viewName) && !CheckFileExists(area, viewName))
                return MvcHtmlString.Create("");
            var ret = RenderPartialViewToString(controller, viewName, null);
            var viewString = ret.ToHtmlString();
            var rRemScript = new Regex(@"<script [^>]*>[\s\S]*?</script>");

            var scripts = string.Join(" ", rRemScript.Matches(viewString).Cast<Match>().Select(m => m.Groups[0].Value).ToArray());
            var content = rRemScript.Replace(viewString, "");
            return MvcHtmlString.Create(getScripts ? scripts : content);
        }

        private static bool CheckFileExists(string viewPath, string viewName)
        {
            var path = HttpContext.Current.Server.MapPath("~" + viewPath + viewName + ".cshtml");
            return File.Exists(path);
        }

        public static string CreateLink(string linkType, JsonParams jp, string link, string[,] additionalParam = null)
        {
            var qString = additionalParam == null ? BuildQuerystring(jp) : BuildQuerystring(jp, additionalParam);
            if (!string.IsNullOrEmpty(link))
            {
                if (link.Contains("type=pm"))
                {
                    if (link.Substring(link.IndexOf("type=pm") - 1, 1) == "?" && link.Split('?')[1].Length>8)
                    {
                        link = link.Remove(link.IndexOf("type=pm&"), 8);
                    }
                    else if (link.Substring(link.IndexOf("type=pm") - 1, 1) == "?")
                    {
                        link = link.Remove(link.IndexOf("?type=pm"), 8);
                    }
                    else
                    {
                        link = link.Remove(link.IndexOf("&type=pm"), 8);
                    }                    
                    return link + "?type=pm";
                }
                if (link.Contains("type=external"))
                {
                    if (link.Substring(link.IndexOf("type=external") - 1, 1) == "?" && link.Split('?')[1].Length>13)
                    {
                        link = link.Remove(link.IndexOf("type=external&"), 14);
                    }
                    else if (link.Substring(link.IndexOf("type=external") - 1, 1) == "?")
                    {
                        link = link.Remove(link.IndexOf("?type=external"), 14);
                    }
                    else
                    {
                        link = link.Remove(link.IndexOf("&type=external"), 14);
                    }
                    return link;
                }
                return "Page" + QueryStringModule.Encrypt(link + qString);
            }
            return "";
        }

    }

    #region Stream filter

    public class WhitespaceFilter : Stream
    {

        public WhitespaceFilter(Stream sink)
        {
            _sink = sink;
        }

        private Stream _sink;
        private static Regex reg = new Regex(@"(?<=[^])\t{2,}|(?<=[>])\s{2,}(?=[<])|(?<=[>])\s{2,11}(?=[<])|(?=[\n])\s{2,}", RegexOptions.Multiline | RegexOptions.Compiled);
        //private static Regex reg = new Regex(@"^\s+", RegexOptions.Multiline | RegexOptions.Compiled); 

        #region Properites

        public override bool CanRead
        {
            get { return true; }
        }

        public override bool CanSeek
        {
            get { return true; }
        }

        public override bool CanWrite
        {
            get { return true; }
        }

        public override void Flush()
        {
            _sink.Flush();
        }

        public override long Length
        {
            get { return 0; }
        }

        private long _position;
        public override long Position
        {
            get { return _position; }
            set { _position = value; }
        }

        #endregion

        #region Methods

        public override int Read(byte[] buffer, int offset, int count)
        {
            return _sink.Read(buffer, offset, count);
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            return _sink.Seek(offset, origin);
        }

        public override void SetLength(long value)
        {
            _sink.SetLength(value);
        }

        public override void Close()
        {
            _sink.Close();
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            string html = Encoding.Default.GetString(buffer);
            html = reg.Replace(html, string.Empty);
            byte[] outdata = Encoding.Default.GetBytes(html);
            _sink.Write(outdata, 0, outdata.GetLength(0));
        }

        #endregion

    }

    #endregion

}
