var gulp = require('gulp');
var config = require('../gulp.config')();
var Server = require('karma').Server;
var gulpProtractor = require('gulp-protractor');
var remapIstanbul = require('remap-istanbul/lib/gulpRemapIstanbul');

gulp.task('test', ['clean-report', 'unit-test']);

//gulp.task('unit-test', ['tsc'], function (done) {

gulp.task('unit-test', function (done) {
    new Server({
        configFile: __dirname + '/../karma.conf.js',
        singleRun: true
    }, karmaDone).start();

    function karmaDone(exitCode) {
        //console.log('Test Done with exit code: ' + exitCode);
        //remapCoverage();
        if (exitCode === 0) {
            remapCoverage();
            done();
        } else {
            //done('Unit test failed.');
            //done();
        }
    }
});

//gulp.task('e2e', ['e2e-test']);
//gulp.task('driver-update', gulpProtractor['webdriver_update']);
//gulp.task('e2e-test', ['driver-update', 'tsc-e2e'], function () {
//	gulp.src(config.e2e + '**/*.spec.js')
//	.pipe(gulpProtractor.protractor({
//		configFile: 'protractor.conf.js',
//		args: ['--baseUrl', config.e2eConfig.seleniumTarget]
//	}))
//	.on('error', function(e) {
//		console.log('Error running E2E testing');
//		process.exit(1);
//	});
//});

// **************************************************************************************************
// This task will run e2e tests from public URL machine and using browserstack. 
// Browserstack subscription is required.
// DONE: pass the environment URL using --baseurl param
// **************************************************************************************************
gulp.task('e2etests-browserstack-uat', function (cb) {
    gulp.src(['test/e2e/**/*.spec.js'], { read: false })
        .pipe(gulpProtractor.protractor({
            configFile: 'protractorbstack.conf.js',
            args: ['--baseUrl', 'https://acewebsiteuat.aclarax.com/']
        }))
        .on('error', function (e) {
            console.log('Error running E2E testing uat');
            //process.exit(1);
            cb();
        }).on('end', function () {
            console.log("****Success****");
            cb();
        });
});

gulp.task('e2etests-browserstack-qa', function (cb) {
    gulp.src(['test/e2e/**/*.spec.js'], { read: false })
        .pipe(gulpProtractor.protractor({
            configFile: 'protractorbstack.conf.js',
            args: ['--baseUrl', 'https://acewebsiteqa.aclarax.com/']
        }))
        .on('error', function (e) {
            console.log('Error running E2E testing on qa');
            //process.exit(1);
            cb();
        }).on('end', function () {
            console.log("****Success****");
            cb();
        });
});

// **************************************************************************************************
// These set of tasks are needed to run standalone webdriver on local machine and run e2e tests on it
// Developers can run this to run e2e tests on their local machine without browserstack
// Browserstack subscription is NOT required.
// **************************************************************************************************
// Download and update the selenium driver
gulp.task('e2etests:webdriver_manager_update', gulpProtractor.webdriver_update);

 gulp.task('e2etests-local:run', ['e2etests:webdriver_manager_update'], function (cb) {

    gulp.src(['test/e2e/**/*.spec.js'], { read: false })
        .pipe(gulpProtractor.protractor({
            configFile: 'protractor.conf.js',
            args: ['--baseUrl', 'https://localhost/CE.InsightsWeb']
            

        })).on('error', function (e) {
            console.log(e);
            cb();
        }).on('end', function () {
            console.log("****Success****");
            cb();
        });
 });

gulp.task('e2etests-uat:run', ['e2etests:webdriver_manager_update'], function (cb) {

   
    gulp.src(['test/e2e/**/*.spec.js'], { read: false })
        .pipe(gulpProtractor.protractor({
            configFile: 'protractor.conf.js',
            args: ['--baseUrl', 'https://acewebsiteuat.aclarax.com/']

        })).on('error', function (e) {
            console.log(e);
            cb();
        }).on('end', function () {
            console.log("****Success****");
            cb();
        });
});

gulp.task('e2etests-qa:run', ['e2etests:webdriver_manager_update'], function (cb) {

    gulp.src(['test/e2e/**/*.spec.js'], { read: false })
        .pipe(gulpProtractor.protractor({
            configFile: 'protractor.conf.js',
            args: ['--baseUrl', 'https://acewebsiteqa.aclarax.com/']

        })).on('error', function (e) {
            console.log(e);
            cb();
        }).on('end', function () {
            console.log("****Success****");
            cb();
        });
});

gulp.task('e2etests-dev:run', ['e2etests:webdriver_manager_update'], function (cb) {

    gulp.src(['test/e2e/**/*.spec.js'], { read: false })
        .pipe(gulpProtractor.protractor({
            configFile: 'protractor.conf.js',
            args: ['--baseUrl', 'https://acewebsitedev.aclarax.com/']

        })).on('error', function (e) {
            console.log(e);
            cb();
        }).on('end', function () {
            console.log("****Success****");
            cb();
        });
});
// ###################################################################################################


function remapCoverage() {
    console.log('Remapping coverage to TypeScript format...');
    gulp.src(config.report.path + 'report-json/coverage-final.json')
        .pipe(remapIstanbul({
            reports: {
                'lcovonly': config.report.path + 'remap/lcov.info',
                'json': config.report.path + 'remap/coverage.json',
                'html': config.report.path + 'remap/html-report',
                'text-summary': config.report.path + 'remap/text-summary.txt'
            }
        }))
        .on('finish', function () {
            console.log('Remapping done! View the result in report/remap/html-report');
        });
}