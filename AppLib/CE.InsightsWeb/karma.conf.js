module.exports = function (config) {
    var dependencies = require('./package.json').dependencies;
    var excludedDependencies = [
      'systemjs', 'zone.js', 'font-awesome', 'bootswatch'
    ];
    var configuration = {
        basePath: '',

        frameworks: ['jasmine'],
        browsers: ['PhantomJS'],
        reporters: ['progress', 'coverage'],

        preprocessors: {
            'app/**/!(*.spec)+(.js)': ['coverage'],
            'app/**/*.js': ['sourcemap']
        },

        // Generate json used for remap-istanbul
        coverageReporter: {
            dir: 'report/',
            reporters: [
              { type: 'json', subdir: 'report-json' }
            ]
        },

        files: [
          'node_modules/traceur/bin/traceur-runtime.js',
          'node_modules/systemjs/dist/system.src.js',
           // Polyfills
          'node_modules/core-js/client/shim.js',
           // Reflect and Zone.js
          'node_modules/reflect-metadata/Reflect.js',
          'node_modules/zone.js/dist/zone.js',
          'node_modules/zone.js/dist/long-stack-trace-zone.js',
          'node_modules/zone.js/dist/proxy.js',
          'node_modules/zone.js/dist/sync-test.js',
          'node_modules/zone.js/dist/jasmine-patch.js',
          'node_modules/zone.js/dist/async-test.js',
          'node_modules/zone.js/dist/fake-async-test.js',

          'systemjs.conf.js',
          'karma-test-shim.js',

          { pattern: 'node_modules/ng-recaptcha/**/*.js', included: false, watched: false },
          { pattern: 'node_modules/ngx-bootstrap/**/*.js', included: false, watched: false },
          { pattern: 'node_modules/angular-confirmation-popover/**/*.js', included: false, watched: false },

          { pattern: 'app/**/*.js', included: false },
          { pattern: 'test/test-helpers/*.js', included: false },

          // paths loaded via Angular's component compiler
          // (these paths need to be rewritten, see proxies section)
          { pattern: 'app/**/*.html', included: false },
          //{ pattern: 'app/**/*.css', included: false },

          // paths to support debugging with source maps in dev tools
          { pattern: 'app/**/*.ts', included: false, watched: false },
          { pattern: 'app/**/*.js.map', included: false, watched: false }
        ],

        // proxied base paths
        proxies: {
            // required for component assests fetched by Angular's compiler
            "/app/": "/base/app/",
            "/App/": "/base/app/",
            "/test/": "/base/test/",
            "/node_modules/": "/base/node_modules/"
        },

        port: 9876,
        colors: true,
        logLevel: config.LOG_INFO,
        autoWatch: false,
        singleRun: true,
    };

    Object.keys(dependencies).forEach(function (key) {
        if (excludedDependencies.indexOf(key) >= 0) { return; }

        configuration.files.push({
            pattern: 'node_modules/' + key + '/**/*.js',
            included: false,
            watched: false
        });
    });
    
    configuration.captureTimeout = 60000;
    // to avoid DISCONNECTED messages
    configuration.browserDisconnectTimeout = 10000; // default 2000
    configuration.browserDisconnectTolerance = 3; // default 0
    configuration.browserNoActivityTimeout = 60000; //default 10000

    if (process.env.APPVEYOR) {
        configuration.browsers = ['IE'];
        configuration.singleRun = true;
        configuration.browserNoActivityTimeout = 90000; // Note: default value (10000) is not enough
    }

    config.set(configuration);
}
