﻿using System;
using System.Web.Mvc;
using System.Text;
using System.Web.ModelBinding;
using RestSharp;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CE.AceHeadersDemoWeb.Controllers
{
    public class MLGWController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel entity)
        {

            try
            {
                    //set up rest client
                    var client = new RestClient("https://aceapiqa.aclarax.com");

                    //request
                    var request = new RestRequest(Method.POST);

                    //WebToken API resource endpoint
                    request.Resource = "/api/v1/webtoken";

                    //required security headers
                    request.AddParameter("X-CE-AccessKeyId", "da7ec68a06bcf943602981fbc59dfb0a45e6", ParameterType.HttpHeader);
                    var authHeader = $"Basic {Base64Encode("da7ec68a06bcf943602981fbc59dfb0a45e6" + ":" + "MLGWBasicKey80")}";
                    request.AddParameter("Authorization", authHeader, ParameterType.HttpHeader);

                    //Pass the of customerId for which you
                    //want to grant access to the ACE Website
                    request.RequestFormat = DataFormat.Json;
                    request.AddJsonBody(new { CustomerId = "100030" });
 
                    // execute response
                    var response = client.Execute(request);

                    JToken token = JObject.Parse(response.Content);

                    var webToken = token.SelectToken("WebToken");
                     
                    var aceQueryStringParams = "&ClientId=" + "118"
                                           + "&CustomerId=" + "100030"
                                           + "&AccountId=" + "0001000301045222"
                                           + "&PremiseId=" + "1045222"
                                           + "&ServicePointId=" + ""
                                           + "&Locale=" + "en-US"
                                           + "&WebToken=" + webToken;

                    //redirect to ACE Website passing the id of the
                    //page you want to land on and the querystring params
                    //from above in order to gain access
                    var aceUrl = "https://acewebsiteqa.aclarax.com/Page?id=1&tmpl=rc&hideMenu=true" + aceQueryStringParams;
                    return Redirect(aceUrl);

            }
            catch(Exception ex)
            {
                throw ;
            }

        }
        private static string Base64Encode(string plainText)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(plainTextBytes);
        }

    }
}