﻿using System;
using System.Web.Mvc;
using System.Text;
using RestSharp;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CE.AceHeadersDemoWeb.Controllers
{
    public class DemoController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel entity)
        {

            try
            {
                // setup rest client
                var client = new RestClient("https://aceapiqa.aclarax.com");

                //request
                var request = new RestRequest(Method.POST);

                //WebToken API resource endpoint
                request.Resource = "/api/v1/webtoken";

                //required security headers
                request.AddParameter("X-CE-AccessKeyId", "7320D04481A949B8AB4BB04CDD1A307E", ParameterType.HttpHeader);
                var authHeader = $"Basic {Base64Encode("7320D04481A949B8AB4BB04CDD1A307E" + ":" + "DemoBasicKey87")}";
                request.AddParameter("Authorization", authHeader, ParameterType.HttpHeader);

                //Pass the of customerId for which you
                //want to grant access to the ACE Website
                request.RequestFormat = DataFormat.Json;
                request.AddJsonBody(new { CustomerId = "993774751" });

                // execute response
                var response = client.Execute(request);

                JToken token = JObject.Parse(response.Content);

                var webToken = token.SelectToken("WebToken");

                var aceQueryStringParams = "&ClientId=" + "87"
                            + "&CustomerId=" + "993774751"
                            + "&AccountId=" + "1218337093"
                            + "&PremiseId=" + "1218337000"
                            + "&ServicePointId=" + ""
                            + "&Locale=" + "en-US"
                            + "&WebToken=" + webToken;

                //redirect to ACE Website passing the id of the
                //page you want to land on and the querystring params
                //from above in order to gain access
                var aceUrl = "https://acewebsiteqa.aclarax.com/Page?id=1&tmpl=rc&hideMenu=true" + aceQueryStringParams;
                return Redirect(aceUrl);

            }
            catch (Exception ex)
            {
                throw;
            }

        }
        private static string Base64Encode(string plainText)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(plainTextBytes);
        }

    }
}