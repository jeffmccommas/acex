﻿using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using AO.GreenButtonConnect.IngestionUtil;
using AO.TimeSeriesProcessing;

namespace AO.GreenButtonConnect.SubscribedMeterConsole
{
    public class GreenButtonButtonMeterConsole
    {
        private static string _env;
        private static int _clientId;
        private static string _filename;
        private static bool _exportCsv;
        private static ContextData _context;
        public static void Main(string[] args)
        {
            var procArgs = new ProcessingArgs(args);
            string vsrc = procArgs.GetControlVal("reqVsrc", "evc");
            ContextData.InitContextStrings(vsrc, procArgs["aclenv"], "none", procArgs, null);
            ProcessingArgs.AccumulateRequestPars(vsrc, procArgs["aclenv"], "none", procArgs);

            _context = new ContextData(procArgs);
            _env = procArgs.GetControlVal("aclenv", "perfd");
            _clientId = Convert.ToInt32(procArgs.GetControlVal("clientid", "87"));
            _filename = procArgs.GetControlVal("gbcmeterdatafile", "c:/meters.xml");
            _exportCsv = Convert.ToBoolean(procArgs.GetControlVal("exportcsv", "false"));

            var oper = procArgs.GetControlVal("oper", "gbcmetersfile");

            switch (oper.ToLower())
            {
                case "gbcmetersfile":
                    UpdateSubscribedMeters(procArgs);
                    break;
                case "report":
                    GenerateReport(procArgs);
                    break;
                default:
                    SmokeTest(procArgs);
                    break;
            }

            _context.End();
        }

        private static void UpdateSubscribedMeters(ProcessingArgs procArgs)
        {
            _context.ContextWrite("GBCFileGenerationProc", "Begin");
            DateTime procStartTime = DateTime.Now;
            try
            {
                var dt = new DataTable();
                var data = ReadFile(dt);

                _env = procArgs.GetControlVal("aclenv", "dev");
                var insightsEntities = $"{_env}InsightsEntities";
                var insightsEntitiesConnStr = procArgs.GetControlVal(insightsEntities, "");
                var preprocess = new IngestionPreprocess(insightsEntitiesConnStr, _clientId, _filename);
                var updatedSubscribedMeters = preprocess.GenerateSubscribedMetersFile(_context, data);
                if (updatedSubscribedMeters != null && updatedSubscribedMeters.Rows.Count > 0)
                {
                    WriteFile(updatedSubscribedMeters);
                    if (_exportCsv)
                    {
                        var csvFile = _filename.Remove(_filename.LastIndexOf('.')) + "_" + DateTime.Now.ToString("MMddyyyy-HHmmss.ffff") + ".csv";
                        csvFile = procArgs.GetControlVal("csvOutFile", csvFile);
                        ExportCsv(csvFile, updatedSubscribedMeters);
                    }
                }
            }
            catch (Exception ex)
            {
                _context.ContextWrite("UpdateSubscribedMeters-ERROR", $"Exception: {ex.Message} InnerException: {ex.InnerException?.Message}");
            }

            DateTime procEndTime = DateTime.Now;
            _context.ContextWrite("GBCFileGenerationProc", "End");
            _context.Add2Counter("GBCFileGenerationProcMs", (long)procEndTime.Subtract(procStartTime).TotalMilliseconds);
            Console.WriteLine(_context.Snag());
        }

        private static void SmokeTest(ProcessingArgs procArgs)
        {

            _context.ContextWrite("SmokeTest", "Begin");
            try
            {
                var oper = procArgs.GetControlVal("smoketest", "");
                var data = new DataTable();

                data = ReadFile(data);

                switch (oper.ToLower())
                {
                    case "count":
                        var count = procArgs.GetControlVal("count", "new");
                        var i = data.AsEnumerable().Count(d => d.Field<string>("Status") == count);
                        _context.ContextWrite($"Number of meter status = {count}: {i}");
                        break;
                    case "select":
                        var selectParam = "ClientId";
                        var select = procArgs.GetControlVal("select", "clientid");
                        var id = procArgs.GetControlVal("id", "");

                        if (!string.IsNullOrEmpty(id))
                        {
                            switch (select.ToLower())
                            {
                                case "customerid":
                                    selectParam = "CustomerId";
                                    break;
                                case "accountid":
                                    selectParam = "AccountId";
                                    break;
                                case "meterid":
                                    selectParam = "MeterId";
                                    break;
                            }
                        }
                        else
                        {
                            id = _clientId.ToString();
                        }
                        
                        var result = selectParam == "ClientId"
                            ? data.AsEnumerable().Where(m => m.Field<int>(selectParam) == Convert.ToInt32(id)).Select(t => t).ToList()
                            : data.AsEnumerable().Where(m => m.Field<string>(selectParam) == id).Select(t => t).ToList();

                        if (result.Any())
                        {
                            foreach (var row in result)
                            {
                                var client = row["ClientId"].ToString();
                                var customerId = row["CustomerId"].ToString();
                                var accountId = row["AccountId"].ToString();
                                var meterId = row["MeterId"].ToString();
                                var status = row["Status"].ToString();
                                var errorCount = row["ErrorCount"].ToString();
                                var ingestedReadsCount = row["IngestedReadingsCount"].ToString();
                                var rejestedReadsCount = row["RejectedReadingsCount"].ToString();

                                _context.ContextWrite(
                                    $"ClientId: {client} customerId: {customerId} accountId: {accountId} meterId: {meterId} status: {status} error count: {errorCount} ingestedReadsCount: {ingestedReadsCount} rejestedReadsCount: {rejestedReadsCount}");
                            }
                        }
                        break;
                    case "update":
                        var update = data.AsEnumerable().Where(d => d.Field<string>("Status") == "new").ToList();
                        foreach (var dataRow in update)
                        {
                            dataRow["Status"] = "existing";
                            dataRow["Updated"] = DateTime.Now;
                        }
                        WriteFile(data);
                        break;
                }

                if (_exportCsv)
                {
                    var csvFile = _filename.Remove(_filename.LastIndexOf('.')) + "_" + DateTime.Now.ToString("MMddyyyy-HHmmss.ffff") + ".csv";
                    csvFile = procArgs.GetControlVal("csvOutFile", csvFile);
                    ExportCsv(csvFile, data);
                }
            }
            catch (Exception ex)
            {
                _context.ContextWrite("SmokeTest-ERROR",
                    $"Exception: {ex.Message} InnerException: {ex.InnerException?.Message}");
            }

            _context.ContextWrite("SmokeTest", "End");
            Console.WriteLine(_context.Snag());
            
        }

        private static void GenerateReport(ProcessingArgs procArgs)
        {
            _context.ContextWrite("AMIIngestionReport", "Begin");
            DateTime procStartTime = DateTime.Now;

            try
            {
                var dt = new DataTable();
                var data = ReadFile(dt) ?? dt;

                var localDir = Path.GetDirectoryName(_filename);
                var destPathAppSetting = $"{_env}DestinationReportpath";
                var destPath = procArgs.GetControlVal(destPathAppSetting, string.Empty);
                var report = new IngestionReport(_clientId, localDir, destPath);
                var result = report.GenerateReport(_context, ref data);
                if (result)
                {
                    if (_exportCsv)
                    {
                        var csvFile = _filename.Remove(_filename.LastIndexOf('.')) + "_" + DateTime.Now.ToString("MMddyyyy-HHmmss.ffff") + ".csv";
                        csvFile = procArgs.GetControlVal("csvOutFile", csvFile);
                        ExportCsv(csvFile, data);
                    }

                    // update file with error count reset
                    WriteFile(data);
                }
            }
            catch (Exception ex)
            {
                _context.ContextWrite("GenerateReport-ERROR",
                    $"Exception: {ex.Message} InnerException: {ex.InnerException?.Message}");
            }

            DateTime procEndTime = DateTime.Now;
            _context.ContextWrite("AMIIngestionReport", "End");
            _context.Add2Counter("AMIIngestionReportProcMs", (long)procEndTime.Subtract(procStartTime).TotalMilliseconds);
            Console.WriteLine(_context.Snag());
        }

        private static void ExportCsv(string csvfileName, DataTable inData)
        {
            if (File.Exists(csvfileName)) {
                File.Delete(csvfileName);
            }

            DataView dv = inData.DefaultView;
            dv.Sort = "MeterId,AccountId asc";
            DataTable data = dv.ToTable();


            StreamWriter sw = new StreamWriter(csvfileName, false);
            //headers  
            for (int j = 0; j < data.Columns.Count; j++)
            {
                sw.Write(data.Columns[j]);
                if (j < data.Columns.Count - 1)
                {
                    sw.Write(",");
                }
            }
            sw.Write(sw.NewLine);
            foreach (DataRow dr in data.Rows)
            {
                for (int j = 0; j < data.Columns.Count; j++)
                {
                    if (!Convert.IsDBNull(dr[j]))
                    {
                        string value = dr[j].ToString();
                        if (value.Contains(','))
                        {
                            value = $"\"{value}\"";
                            sw.Write(value);
                        }
                        else
                        {
                            sw.Write(dr[j].ToString());
                        }
                    }
                    if (j < data.Columns.Count - 1)
                    {
                        sw.Write(",");
                    }
                }
                sw.Write(sw.NewLine);
            }
            sw.Close();
            _context.ContextWrite("INFO", $"Export datatable to csv file {csvfileName} succesfully.");
        }

        public static DataTable ReadFile<T>(T data)
        {
            XmlSerializer xmlSerializer = XmlSerializer.FromTypes(new[] { data.GetType() })[0];

            //XmlSerializer xmlSerializer = new XmlSerializer(data.GetType());
            DataTable result = null;

            if (File.Exists(_filename))
            {
                var fileStream = new FileStream(_filename, FileMode.Open);
                try
                {
                    using (XmlTextReader reader = new XmlTextReader(fileStream))
                    {
                        result = (DataTable)xmlSerializer.Deserialize(reader);
                    }
                    _context.ContextWrite("INFO", $"File {_filename} is loaded");
                }
                catch (Exception ex)
                {
                    // log error tbd
                    _context.ContextWrite("ReadFile - ERROR",
                        $"Fail to open file {_filename} for clientId {_clientId}.  Exception: {ex.Message} InnerException: {ex.InnerException?.Message}");

                }
                finally
                {
                    fileStream.Close();
                }
            }
            

            return result;
        }
        public static void WriteFile<T>(T data)
        {
            XmlSerializer xmlSerializer = XmlSerializer.FromTypes(new[] { data.GetType() })[0];

            if (File.Exists(_filename))
            {
                var copy = _filename.Remove(_filename.LastIndexOf('.')) + "_Archived_" + DateTime.Now.ToString("MMddyyyy-HHmmss.ffff") + ".xml";
                File.Copy(_filename, copy);
                _context.ContextWrite("INFO", $"Previous file is archived to {copy}");
            }

            var fileStream = new FileStream(_filename, FileMode.Create);


            try
            {

                using (XmlTextWriter writer = new XmlTextWriter(fileStream, new UTF8Encoding(false)))
                {
                    writer.Formatting = Formatting.Indented;

                    xmlSerializer.Serialize(writer, data);

                    writer.Flush();
                }
                _context.ContextWrite("INFO", $"File {_filename} is updated/generated");
            }
            catch (Exception ex)
            {
                // log error 
                _context.ContextWrite("WriteFile ERROR",
                    $"Fail to write file {_filename} for clientId {_clientId}.  Exception: {ex.Message} InnerException: {ex.InnerException?.Message}");
            }
            finally
            {
                fileStream.Close();
            }
        }
        
    }
}


