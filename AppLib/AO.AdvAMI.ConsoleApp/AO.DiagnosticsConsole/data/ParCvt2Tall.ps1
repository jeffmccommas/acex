# Loop through the server list
Get-Content ".\data\Cvt2TallDirlist.txt" | %{

  # Define what each job does
  $ScriptBlock = {
    param($pparList) 
    $ppar1,$ppar2 = $pparList.split(' ');
    $root="C:\_ameren_60"
    $cmdr="C:\work3\ACEx\17.09\AppLib\AO.AdvAMI.ConsoleApp\AO.DiagnosticsConsole\bin\x64\Debug\AO.DiagnosticsConsole.exe oper=cvt2Tall p2=$ppar2 procLimit=10000000 stripCsvQ=yes overwriteDrop=yes addCsvQuotes=no retainTallSrc=yes "
    $src="$root\$ppar1\orig"
    $des="$root\$ppar1\tall"
    $command="$cmdr zipSrc=$src dropLocation=$des"
    echo $command
    Invoke-Expression -Command:$command
    #echo "$root\$ppar1\orig"
    #dir "$root\$ppar1\orig"
    #Start-Sleep 1
  }

  # Execute the jobs in parallel
  Start-Job $ScriptBlock -ArgumentList $_ 
}

Get-Job

# Wait for it all to complete
While (Get-Job -State "Running")
{
  Start-Sleep 2
}

# Getting the information back from the jobs
Get-Job | Receive-Job 
