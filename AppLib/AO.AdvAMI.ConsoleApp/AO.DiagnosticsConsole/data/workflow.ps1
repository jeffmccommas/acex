﻿Param([String[]] $args)

$DROP="G:\__Ameren_Watch"
$DROPRELOC="G:\__Ameren_Watch_Processed"
$FailOnInc=0

$SCRIPTROOT="E:\AclaraX\PaaS\AO.AdvAMI.ConsoleApp\"
$DATAFILE="G:\__Ameren_GBCMeterData\GBCMeterDataFile.xml"
$HISTORICALZIPSRCROOT="G:\__Ameren_Talls"
$ENV="perfd"
$ZIPSRC="G:\__Ameren_services_drop" 
$TEMPTALL = "G:\__Ameren_Temp"
$NEWFILES=$HISTORICALZIPSRCROOT + "\NEWFILES\Tall"
$CONFCHANGEHIS="ConfigurationChangeToHistorical.csv"
$CONFCHANGEINC="ConfigurationChangeToIncremental.csv"

ForEach($arg in $args)
{
    $command=$arg.Split("=")
    switch($command[0])
    {
        "clientId" {$CLIENTID = $command[1]}
        "subfolder" {$SUBFOLDER = $command[1]}
        "dropLocation" {$DROP = $command[1]}
        "dropRelocate" {$DROPRELOC = $command[1]}
        "workflow" {$workflow = $command[1]}
        "failOnInc" {$FailOnInc = $command[1]}        
        "gbcmeterdatafile" {$DATAFILE = $command[1]}
        "aclenv" {$ENV = $command[1]}
        "incRawZipLoc" {$ZIPSRC = $command[1]}
        "historicalRoot" {$HISTORICALZIPSRCROOT = $command[1]}
        "consoleLoc" {$SCRIPTROOT = $command[1]}
        "incTallZipLoc" {$TEMPTALL = $command[1]}
        "newFilesHisLoc" {$NEWFILES = $command[1]}
        "confChangeToHistorical" {$CONFCHANGEHIS = $command[1]}
        "confChangeToIncremental" {$CONFCHANGEINC = $command[1]}

    }
    
}

Function outputConfig
{
    Write-Host "clientId " $CLIENTID
    Write-Host "subfolder " $SUBFOLDER
    Write-Host "dropLocation " $DROP
    Write-Host "dropRelocate " $DROPRELOC
    Write-Host "workflow " $workflow
    Write-Host "failonincomplete " $FailOnInc
    Write-Host "gbcmeterdatafile " $DATAFILE
    Write-Host "aclenv " $ENV
    Write-Host "incRawZipLoc " $ZIPSRC
    Write-Host "historicalRoot " $HISTORICALZIPSRCROOT
    Write-Host "consoleLoc " $SCRIPTROOT
    Write-Host "incTallZipLoc " $TEMPTALL
    Write-Host "newFilesHisLoc " $NEWFILES
    Write-Host "confChangeToHistorical " $CONFCHANGEHIS
    Write-Host "confChangeToIncremental " $CONFCHANGEINC
}

if(!$CLIENTID)
{
    if($FailOnInc -eq 0)
    {
        do{
            $CLIENTID = read-host "enter client id:"
        }
        while(!$CLIENTID)
    }
    else{
        Write-Error "client id is missing"
        outputConfig
        exit 1
        
    }
}


Function workflow1{
    Write-Host "********** Workflow 1 *************"
    Write-Debug "******** Activate Watcher ********"
    $filePath = $SCRIPTROOT + "AO.DiagnosticsConsole.exe"
    $args = "configDataFile=.\data\config_watcher_ConfigurationChange.csv  enableProcessed=yes dropHoldCount=0 aclenv=" + $ENV + " dropLocation="+ $DROP +" dropRelocate=" +$DROPRELOC + " gbcmeterdatafile="+$DATAFILE+" modDataFileName=" + $DATAFILE
    Start-Process -FilePath $filePath -ArgumentList $args
    Write-Debug "******** Activated Watcher ********"
    Write-Host "********** Workflow 1 End *************"    
}

Function workflow2 {
	Write-Host "********** Workflow 2 *************"
	Write-Debug "******** Reset archived historical copy ********"

    $dirs = Get-ChildItem $HISTORICALZIPSRCROOT 
    ForEach($dir in $dirs)
    {
        $extracted= $dir.FullName + "\Tall\extracted"
        $dest = $dir.FullName + "\Tall"

        if(Test-Path $extracted)
        {
            $files = Get-ChildItem $extracted -Filter *.zip
            ForEach($file in $files){
                Copy-Item $file.FullName -Destination $dest
            }
        }
    }
	Write-Debug "******** Reset Done ********"
	Write-Host "********** Workflow 2 End *************"
}

Function workflow3{
	Write-Host "********** Workflow 3 *************"
	Write-Debug "******** Convert Zip to Tall and Staging Start ********"	
	if(!(test-path $TEMPTALL))
	{
		New-Item -ItemType Directory -Force -Path $TEMPTALL
	}

	Write-Debug "************ Zip 60min csv ****************************"
	$date = Get-Date -UFormat "%Y%m%d%H%M%S"
	$hourly = $ZIPSRC + "\174_ami_60min*.csv"
	$hourlyZip = $ZIPSRC + "\174_ami_60min_" + $date  +".zip"
    ForEach($hourlyFile in $hourlyCSV)
    { 
        Compress-Archive -Path $hourlyFile.FullName -DestinationPath $hourlyZip
    }
	
	Write-Debug "************ Zip Daily csv *************************"
	$dailyZip = $TEMPTALL + "\174_ami_daily_" + $date +".zip"
	$daily = $ZIPSRC + "\174_ami_daily*.csv"
	$dailyCsv = Get-ChildItem $daily
    ForEach($dailyFile in $dailyCsv)
    { 
        Compress-Archive -Path $dailyFile.FullName -DestinationPath $dailyZip
    }
	
	Write-Debug "************* move daily zip to temp location *************"
	$dailyZip = $ZIPSRC + "\174_ami_daily*.zip"
	Move-Item $dailyZip $TEMPTALL
	
	$files = Get-ChildItem $ZIPSRC -Filter *.zip 
	ForEach ($file in $files)
	{
        $command = $SCRIPTROOT + "AO.DiagnosticsConsole.exe oper=cvt2Tall zipSrc=" + $file.FullName + " dropLocation=" + $TEMPTALL + " procLimit=100000 stripCsvQ=yes overwriteDrop=yes addCsvQuotes=no retainTallSrc=yes"
		Invoke-Expression $command
	}
	Write-Debug "******** Convert Zip to Tall and Staging Done ********"
	Write-Host "********** Workflow 3 End *************"
}

Function workflow4{

    if(!$SUBFOLDER)
    {
        if($FailOnInc -eq 0)
        {
            do{
                $SUBFOLDER = read-host "enter historical subfolder(s) (seperated by comma):"
            }
            while(!$SUBFOLDER)
        }
        else{
            Write-Error "historical subfolder info is missing"
            outputConfig
            exit 1
        
        }
    }
    Write-Host "********** Workflow 4 *************"
	Write-Debug "******** GBC Meter data File Generateion Start ********"
    $gbcCommand = $SCRIPTROOT + "AO.GreenButtonConnect.SubscribedMeterConsole.exe clientId=" + $CLIENTID + " aclenv=" + $ENV + " gbcmeterdatafile="+ $DATAFILE + " oper=gbcmetersfile exportcsv=true"
	Invoke-Expression $gbcCommand
	Write-Debug "******** GBC Meter data File Generateion Done ********"
    Write-Debug "******** Historical Ingestion Start ************"	
	Write-Debug "******** Drop Historical Process Indication file to Drop Location **********"
	$copyCommand = "xcopy " + $SCRIPTROOT + $CONFCHANGEHIS + " " + $DROP + " /Y"
	Invoke-Expression $copyCommand

    $sub = $SUBFOLDER.Split(",")
    ForEach($s in $sub)
    {
        $HISTORICALZIPSRC=$HISTORICALZIPSRCROOT + "\" + $SUBFOLDER + "\Tall"
        $command= $SCRIPTROOT + "AO.DiagnosticsConsole.exe oper=ziptest dropLocation=" +$DROP + " zipSrc=" + $HISTORICALZIPSRC + " overwriteDrop=yes"
	    Invoke-Expression $command
    }
	Write-Debug "******** Historical Ingestion Done ************"
	Write-Host "********** Workflow 4 End *************"
}

Function workflow5{

    Write-Host "********** Workflow 5 *************"
	Write-Debug "******** GBC Meter data File Generateion Start ********"
    $gbcCommand = $SCRIPTROOT + "AO.GreenButtonConnect.SubscribedMeterConsole.exe clientId=" + $CLIENTID + " aclenv=" + $ENV + " gbcmeterdatafile="+ $DATAFILE + " oper=gbcmetersfile exportcsv=true"
	Invoke-Expression $gbcCommand
	Write-Debug "******** GBC Meter data File Generateion Done ********"
    Write-Debug "******** Incremental Ingestion Start ************"	
	Write-Debug "******** Drop Incremental Process Indication file to Drop Location **********"
	$copyCommand = "xcopy " + $SCRIPTROOT + $CONFCHANGEINC + " " + $DROP + " /Y"
	Invoke-Expression $copyCommand
    $command= $SCRIPTROOT + "AO.DiagnosticsConsole.exe oper=ziptest dropLocation=" +$DROP + " zipSrc=" + $TEMPTALL + " overwriteDrop=yes"
	Invoke-Expression $command
	Write-Debug "******** Daily Ingestion Done ************"
	Write-Host "********** Workflow 5 End *************"
}

Function workflow6 {
	Write-Host "********** Workflow 6 *************"
	Write-Debug "******** START AMI Ingestion Report********"
	$command= $SCRIPTROOT + "AO.GreenButtonConnect.SubscribedMeterConsole.exe clientId=" +$CLIENTID + " aclenv=" + $ENV + " gbcmeterdatafile=" + $DATAFILE + " oper=report exportcsv=true"
	Invoke-Expression $command
	Write-Debug "******** START AMI Ingestion Report Done ********"
	Write-Debug "******** Move Daily file to Historical **********	"
	if(!(test-path $NEWFILES))
	{
		New-Item -ItemType Directory -Force -Path $NEWFILES
	}
	$copyCommand = "robocopy " + $TEMPTALL + "\extracted " + $NEWFILES + " /E"
	Invoke-Expression $copyCommand
	Write-Debug "******** Move Daily file to Historical Done **********"
	Write-Host "********** Workflow 6 End *************"

}

Function workflow7{
	Write-Host "********** Workflow 7 *************"
	Write-Debug "******** Validation Start ********"
    $skipRows=1000
    $maxFiles=20
    $skipFiles=0
    $command= $SCRIPTROOT + "AO.DiagnosticsConsole.exe oper=validator procLimit=1000 skipRows=" +$skipRows + " useRefFile=" + $DROPRELOC + " maxFiles=" + $maxFiles + " justListData=yes skipFiles=" + $skipFiles
	Invoke-Expression $command	
	Write-Debug "******** Validation End ********"
	Write-Host "********** Workflow 7 End *************"
}

Function main{
    param($CLIENTID, $SUBFOLDER, $workflow)

    $interactive=$false
    if(!$workflow)
    {
        if($FailOnInc -eq 0)
            {
            $interactive=$true
            Write-Host "1 Activate Watcher"
            Write-Host "2 Reset Historical"
	        Write-Host "3 Convert zip to tall"
	        Write-Host "4 Historical Ingestion - update GBC subscribed meter and ingestion"
	        Write-Host "5 Incremental Ingestion - update GBC subscribed meter and ingestion"
	        Write-Host "6 Report and move daily to historical location"
	        Write-Host "7 Validation"
	        Write-Host "8 Exit" 
	        do{
                $workflow = read-host "Select a workflow (1, 2, 3, 4, 5, 6, 7 or 8)?"
            }
            while(!$workflow)
        }
        else{
            Write-Error "workflow is missing"
            outputConfig
            exit 1
        
        }
    } 
    

    if($workflow -eq 1)
    {
        workflow1
    }elseif($workflow -eq 2)
    {
        workflow2
    }elseif($workflow -eq 3)
    {
        workflow3
    }elseif($workflow -eq 4)
    {
        workflow4
    }elseif($workflow -eq 5)
    {
        workflow5
    }elseif($workflow -eq 6)
    {
        workflow6
    }elseif($workflow -eq 7)
    {
        workflow7
    }elseif($workflow -eq 8)
	{
        outputConfig
		exit 0
	}
	
    if($interactive){
    	main $CLIENTID $SUBFOLDER 
    }
	

    outputConfig
    exit 0
    
}



main $CLIENTID $SUBFOLDER $workflow
