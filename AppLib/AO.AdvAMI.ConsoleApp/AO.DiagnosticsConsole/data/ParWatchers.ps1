
$mainp1=$args[0]
echo "arg for watchers initialization = '$mainp1'"
# Loop through the list
Get-Content ".\data\$mainp1.txt" | %{

  # Define what each job does
  $ScriptBlock = {
    param($pparList) 
    $reppar,$watchpar,$exedir,$addlpar = $pparList.split(' ');
    echo " ****** $reppar, $watchpar, $exedir, $addlpar"
	$filePath="$exedir\AO.DiagnosticsConsole.exe"
    echo " ###### $filePath"

    if ($reppar.CompareTo("#") ) {
		$cfg = "configDataFile=$exedir\data\$watchpar";
		$repl = "repl1=$reppar";
		$gbcm = "repl9=C:\junk\t122";
		$dropl = "repl8=g:\test";
		$trackl = "repl7=G:\_Test\_altTrack";
		Set-Location $exedir
		echo Start-Process -FilePath "cmd.exe"  -ArgumentList "/K $filePath $cfg $repl $gbcm $dropl $trackl "
		Start-Process -FilePath "cmd.exe"  -ArgumentList "/K $filePath $cfg $repl"
    }
    else {
		echo "commented out $pparList"
    }
  }

  # Execute the jobs in parallel
  Start-Job $ScriptBlock -ArgumentList $_
}

Get-Job

# Wait for it all to complete
While (Get-Job -State "Running")
{
  Start-Sleep 1
}

# Getting the information back from the jobs
Get-Job | Receive-Job 
