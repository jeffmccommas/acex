delete from [SeriesMgt1].[dbo].[SeriesCollectionInfo]
GO
delete from [SeriesMgt1].[dbo].[SeriesInfo]
GO
INSERT INTO [dbo].[SeriesInfo] ([Specs],[CollectionKey],[DidSpec],[TSSpec])
VALUES ('seriesStarterSpecs',0,'yyyyMM','yyyy-MM-dd HH:mm:ssZ')
GO

INSERT INTO [dbo].SeriesCollectionInfo ([Specs],[CollectionId],state)
VALUES ('collectionStarterSpecs','collectionStarterId','active')

GO
SELECT count(*)  FROM [SeriesMgt1].[dbo].[SeriesCollectionInfo]

SELECT count(*)  FROM [SeriesMgt1].[dbo].[SeriesInfo]
GO
