﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Threading;
using AO.TimeSeriesProcessing;
using System.Xml.Serialization;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Xml;
using System.Text;

namespace AO.DiagnosticsConsole {

    public class EventsConsole {

        private static ContextData _mainContext;
        private static ProcessingArgs _mainPa;

        private const int numReplaceParams = 10;

        public static void Main(string[] args) {

            for (int i = 0; i < args.Length; i++) {
                Console.WriteLine($"arg {i} = '{args[i]}'");
            }
            Console.CancelKeyPress += myHandler;
            _mainPa = new ProcessingArgs(args);
            string vsrc = _mainPa.GetControlVal("reqVsrc", "evc");
            ContextData.InitContextStrings(vsrc, _mainPa["aclenv"], "none", _mainPa, null);
            ProcessingArgs.AccumulateRequestPars(vsrc, _mainPa["aclenv"], "none", _mainPa);
            //_mainPa.ApplyCustomConfig();
            //see if we have some replacement parameters.
            // 'repl2=foo'   leads to " blah=5 blat##repl2##"  becoming " blah=5 blatfoo" 
            Dictionary<string, string> replPars = new Dictionary<string, string>();
            for (int i = 0; i < numReplaceParams; i++) {
                string tpar = _mainPa[$"repl{i}"];
                if (tpar != null) {
                    replPars[$"##repl{i}##"] = tpar;
                }
            }
            string repCfg = "";
            if (replPars.Count > 0) {
                foreach (KeyValuePair<string, string> kvp in replPars) {
                    repCfg += $"{kvp.Key}@{kvp.Value} ";
                }
                _mainPa["replPars"]=repCfg;
            }

            string configData = "";
            _mainContext = new ContextData(_mainPa);
            string cfgUpdates = _mainPa.GetControlVal("configDataFile", "");
            Console.WriteLine($"check configDataFile='{cfgUpdates}'");
            if (!string.IsNullOrEmpty(cfgUpdates) && File.Exists(cfgUpdates)) {
                Console.WriteLine($"have configDataFile='{cfgUpdates}'");
                using (StreamReader sr = new StreamReader(cfgUpdates)) {
                    int maxCfgCnt = 100;
                    while (!sr.EndOfStream && maxCfgCnt-- > 0) {
                        string configLine = sr.ReadLine();
                        if (!string.IsNullOrEmpty(configLine)) {
                            configLine = configLine.Trim();
                            if (!string.IsNullOrEmpty(configLine)) {
                                configData += configLine + " ";
                                if (replPars.Count > 0) {
                                    foreach (KeyValuePair<string, string> kvp in replPars) {
                                        configData = configData.Replace(kvp.Key, kvp.Value);
                                    }
                                }
                            }
                            Console.WriteLine($" **** ConfigurationChange data: '{configLine}'  ");
                            _mainContext.ContextWrite("AO.DiagnosticsConsole",$" **** ConfigurationChange data: '{configLine}'  ");
                        }
                    }
                    if (!string.IsNullOrEmpty(configData)) {
                        _mainPa.AddIfMissing(configData, true);
                        List<string> nargs = configData.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries).ToList();
                        List<string> sargs = args.ToList();
                        sargs.Add($"replPars={repCfg}");
                        sargs.AddRange(nargs);
                        args = sargs.ToArray();
                        string npars = String.Join(" ", args);
                        Console.WriteLine($" **** ConfigurationChange data new: '{npars}'  ");
                        _mainContext.ContextWrite("AO.DiagnosticsConsole", $" ****  ConfigurationChange data new: '{npars}'  ");
                    }
                    else {
                        Console.WriteLine($" **** ConfigurationChange data: nada ");
                    }
                }
            }

            _mainContext.CtxArgs = _mainPa;
            MainProcess(args);
            CaptureCurrentInfo("mainExit","nada");
        }


        protected static void myHandler(object sender, ConsoleCancelEventArgs args) {
            CaptureCurrentInfo("myHandler", args.SpecialKey.ToString());
            //mainContext.ContextWrite(info);

            args.Cancel = true;  // assume we dont' want to exit
            if (args.SpecialKey == ConsoleSpecialKey.ControlBreak) {
                if (_mainPa.GetControlVal("cntlBreak", "1") == "1") { // normally do want cntlBreak to end
                    args.Cancel = false;
                }
            }
            else {
                if (_mainPa.GetControlVal("cntlCC", "0") == "1") {  // normally don't want cntlC to end
                    args.Cancel = false;
                }
            }
        }

        private static void CaptureCurrentInfo(string wherefrom, string addlInfo) {
            string snagTxt = _mainContext.Snag(true);
            string info = $"{wherefrom}  addlInfo: {addlInfo}, {snagTxt}\n {_mainContext.Counters()} ";
            Console.WriteLine($"{wherefrom} data {info}");
            //Console.WriteLine($"keyHandler pa *****mainPA  \n{_mainPa.AsJson()} \n *****watchPA {_watcherPa.AsJson()}");
            string traceFile = $".\\consTracePars_{wherefrom}.txt";
            try {
                StreamWriter sw = new StreamWriter(traceFile);
                sw.WriteLine($"{wherefrom} data {info}");
                sw.WriteLine($"{wherefrom} pa *****mainPA  \n{_mainPa.AsJson()} \n *****watchPA {_watcherPa?.AsJson()}");
                sw.Close();
                Console.WriteLine($"{wherefrom} write trace file {traceFile}");
            }
            catch (Exception exc) {
                Console.WriteLine($"{wherefrom} cannot write trace file {traceFile} {exc.Message}, {exc.InnerException?.Message}");
            }
        }


        /// <summary>
        /// Main processing entry point
        /// </summary>
        /// <param name="args"></param>
        /// <param name="context">likely null unless this is a callback from an entry point processor (like the web this guy can spin up)</param>
        /// <returns></returns>
        public static int MainProcess(string[] args, ContextData context = null) {
            ProcessingArgs pa;
            try {
                Console.Title += ": Running in '" + Environment.CurrentDirectory;
            }
            catch (Exception exc) {
                Console.WriteLine($"MainProcess:warning (background process? -> ignore) exception is {exc.Message}, inner={exc.InnerException?.Message}");
            }

            Console.WriteLine($"Starting at {DateTime.Now}");
            bool externalContext = false;
            if (context == null) {
                pa = new ProcessingArgs(args);
                pa["bodyOverride"] = "yes";
                string vsrc = pa.GetControlVal("reqVsrc", "evc");
                ContextData.InitContextStrings(vsrc, pa["aclenv"], "none", pa, null);
                ProcessingArgs.AccumulateRequestPars(vsrc, pa["aclenv"], "none", pa);
                //pa.ApplyCustomConfig();
                context = new ContextData(pa);
                context.CtxArgs = pa;
            }
            else {
                externalContext = true;
                if (context.CtxArgs != null) {
                    pa = context.CtxArgs;
                }
                else {
                    pa = new ProcessingArgs(args);
                    //pa["bodyOverride"] = "yes";
                }
            }

            if (pa["sqlMeterMgtCxnStg"] == null) {
                pa["sqlMeterMgtCxnStg"] = ConfigurationManager.AppSettings.Get("sqlMeterMgt");
            }
            DateTime dtStt = DateTime.Now;
            string todo = pa["oper"] ?? "nada";
            switch (todo) {

                case "dttest": {
                    DateTimeTest2(pa);
                }
                    break;

                case "dtoffsets": {
                    DateTimeOffsets(pa);
                }
                    break;

                case "gbcTable": {
                    MeterFilter mf = new MeterFilter();
                    mf.InitializeFiltering(context);
                }
                    break;

                case "dumpconfig": {
                    Console.WriteLine(pa.AsJson());
                }
                    break;
                case "hostservice": {
                    ServicesBroker.SetProcessDelegate(MainProcess,null);
                    SimpleWebServerLocal ws = new SimpleWebServerLocal(pa);
                    for (; ; ) {
                        Console.WriteLine(" in hostservice and waiting ....");
                        Thread.Sleep(5000);
                    }
                }
                    break;


                case "watcher": {
                    InitializeWorkerProcessors(context);
                }
                    break;
                case "limitValidate": {
                        string filename = pa.GetControlVal("useRefFile", "referenceFile.csv");
                        string clientId = pa.GetControlVal("clientId", "174");
                        string reqVsrc = pa.GetClientControlVal(clientId, "consumptionTableName", "");
                        // figure out the max number of rows to validate in a file and how many to skip.
                        long skipRows = 0;
                        long fileLength = new FileInfo(filename).Length;
                        long approxRows = fileLength / 70;
                        long procLimit = pa.GetControlVal("procLimit", 2000000);
                        long maxValRows = pa.GetControlVal("maxValRows", 5000);
                        if (maxValRows > 0) {
                            if (approxRows < maxValRows) {
                                skipRows = 0;
                            }
                            else {
                                skipRows = approxRows / maxValRows;
                            }
                        }
                        string dat = "";
                        using (StreamReader sr = new StreamReader(filename)) {
                            dat = sr.ReadToEnd();
                        }
                        string heOper0 = "{**}";
                        string cmdLine = $"oper=validator clientId={clientId} procLimit={procLimit} origSize={fileLength} approxRows={approxRows} skipRows={skipRows} useRefFile={filename} importfile={filename} maxFiles=20 justListData=no skipFiles=0 heOper0={heOper0} reqVsrc={reqVsrc}";
                        Console.WriteLine("limitValidate {0}", cmdLine);
                        context.ContextWrite("limitValidate", cmdLine);
                        string[] rVals = ServicesBroker.TestProcess(cmdLine.Split(' '), dat);
                        string cmbRval = rVals.Aggregate((current, next) => current + '\n' + next);
                        Console.WriteLine("limitValidateOut {0}", cmbRval);
                        context.ContextWrite("limitValidate", cmbRval);
                    }
                    break;

                default: {
                    try {
                        context.ContextWrite("MainProcess call ProcessOptions", "Begin");
                        ServicesBroker.ProcessOptions(pa, context, dtStt);
                        context.ContextWrite("MainProcess call ProcessOptions", "End", dtStt);
                    }
                    finally {
                        Console.WriteLine(context.Snag());
                    }
                }
                    break;
            }
            int errorCount = (int)context.Add2Counter("errors", 0);
            DateTime dtNow = DateTime.Now;
            string resetErrors = "";
            if (pa.GetControlVal("allowFailOverride", "1").Equals("1")) { // we may stop errors from propoaating so cleanup can proceed anyways
                resetErrors = "(reset on return)";
            }
            context.Add2Counter("mainProcMs", (long)dtNow.Subtract(dtStt).TotalMilliseconds);
            Console.WriteLine($"mainprocess done at {dtNow}, errorCount={errorCount} {resetErrors} delta = {dtNow.Subtract(dtStt).TotalMilliseconds} ms. counters:{context.Counters()}");
            Console.WriteLine(context.Snag());
            if (!externalContext) {
                context.End(_mainContext);
            }
            if (!string.IsNullOrEmpty(resetErrors)) {
                errorCount = 0;
            }
            return errorCount;
        }



        static void DateTimeOffsets(ProcessingArgs pa) {
            InitShiftDays();
            string destTimeZoneString = pa.GetControlVal("destTimeZoneString", "Central Standard Time");
            //dtStartStg=11/5/2017
            //dtStartStg=3/12/2017
            DTOffset("3/12/2016", destTimeZoneString);
            DTOffset("11/5/2016", destTimeZoneString);
            string dtStartStg = pa.GetControlVal("dtStartStg", "");
            if (!string.IsNullOrEmpty(dtStartStg)) {
                DTOffset(dtStartStg, destTimeZoneString);
            }
        }

        private static int[][] shiftDays = new int[6][];

        // {2017,3,11,1},{2017,11,4,-1},{2016,3,13,1},{2016,11,6,-1},{2015,3,8,1},{2015,11,1,-1},
        //
        static void InitShiftDays() {
            shiftDays[0] = new int[] { 2017, 3, 11, -1 };
            shiftDays[1] = new int[] { 2017, 11, 4, 1 };
            shiftDays[2] = new int[] { 2016, 3, 13, -1 } ;
            shiftDays[3] = new int[] { 2016,11,6,1 };
            shiftDays[4] = new int[] { 2015, 3, 8, -1 };
            shiftDays[5] = new int[] { 2015, 11, 1, 1 };
        }

        static int GetDTShift(DateTimeOffset dto) {
            int rval = 0;
            for (int i = 0; i < shiftDays.Length; i++) {
                if (
                    (dto.Year == shiftDays[i][0]) &&
                    (dto.Month == shiftDays[i][1]) &&
                    (dto.Day == shiftDays[i][2])) {
                    rval = shiftDays[i][3];
                    if (dto.Hour == 2) {
                        rval *= 2;
                    }
                }
            }
            return rval;
        }


        static List<DateTimeOffset[]> DTOffset(string dtStartStg, string destTimeZoneString) {
            DateTimeOffset dtSttq = DateTimeOffset.Parse(dtStartStg);
            TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById(destTimeZoneString);
            TimeSpan tsZero = new TimeSpan();
            DateTimeOffset dt = new DateTimeOffset(dtSttq.Year, dtSttq.Month, dtSttq.Day, dtSttq.Hour, dtSttq.Minute, dtSttq.Second, tzi.GetUtcOffset(dtSttq));
            //DateTimeOffset dt = dtSttq.DateTime;
            Console.WriteLine($"\n DateTimeOffset start {dtStartStg} in '{destTimeZoneString}' starting with {dt.ToString(StandardTimeFormat)}");

            List<DateTimeOffset[]> result = new List<DateTimeOffset[]>();
            for (int i = 0; i < 72; i++) {
                int dtShiftCode = GetDTShift(dt);
                long tval = dt.Year;
                tval = tval * 100 + dt.Month;
                tval = tval * 100 + dt.Day;
                tval = tval * 100 + dt.Hour;
                string addlInfo = $"{dtShiftCode} {tval}";
                TimeSpan tzOffset = tzi.GetUtcOffset(dt);
                DateTimeOffset[] dtosa = new DateTimeOffset[5];
                dtosa[0] = dt;
                if (dtShiftCode < -1) {
                    dtosa[0] = dt;
                    dtosa[1] = dtosa[2] = dtosa[3] = dtosa[4] = new DateTimeOffset();
                    addlInfo += "**skipping**";
                }
                else {
                    DateTimeOffset dtSttl =
                        new DateTimeOffset(dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute, dt.Second, tzOffset);
                    dtosa[1] = dtSttl;
                    DateTimeOffset dtos = dt.AddHours(-tzOffset.Hours);
                    dtosa[2] = dtos;
                    DateTimeOffset dtoUTC = new DateTimeOffset(dtos.Year, dtos.Month, dtos.Day, dtos.Hour, dtos.Minute,
                        dtos.Second, tsZero);
                    dtosa[3] = dtoUTC;
                    dtosa[4] = DTOffsetted(dt, tzi);
                    result.Add(dtosa);
                }
                WTL(i.ToString("D3"), dtosa, tzOffset, addlInfo);
                dt = dt.AddHours(1);
            }
            return result;
        }

        static TimeSpan tsZero = new TimeSpan();

        static DateTimeOffset DTOffsetted(DateTimeOffset dtSttq, TimeZoneInfo tzi) {
            DateTimeOffset dt = new DateTimeOffset(dtSttq.Year, dtSttq.Month, dtSttq.Day, dtSttq.Hour, dtSttq.Minute, dtSttq.Second, tzi.GetUtcOffset(dtSttq));
            TimeSpan tzOffset = tzi.GetUtcOffset(dt);
            DateTimeOffset dtos = dt.AddHours(-tzOffset.Hours);
            DateTimeOffset dtoUTC = new DateTimeOffset(dtos.Year, dtos.Month, dtos.Day, dtos.Hour, dtos.Minute, dtos.Second, tsZero);
            return dtoUTC;
        }



        static void WTL(string stg, DateTimeOffset[] dtal, TimeSpan tzOffset, string addlInfo = "") {
            try {
                string dtos = "";
                for (int i = 0; i < dtal.Length; i++) {
                    dtos += $"{dtal[i].ToString(StandardTimeFormat)} ";
                }
                Console.WriteLine($" {addlInfo}\t {stg}\t {tzOffset.Hours} {dtos}");
            }
            catch (Exception exc) {
                Console.WriteLine($" {stg}=\t, '{tzOffset}', {exc.Message},{exc.InnerException?.Message}");
            }
        }

        static void DateTimeTest2(ProcessingArgs pa) {
            string destTimeZoneString = pa.GetControlVal("destTimeZoneString", "Central Standard Time");
            string dtStartStg = pa.GetControlVal("dtStartStg", "3/12/2017");
            //dtStartStg=11/5/2017
            //dtStartStg=3/12/2017
            DateTimeTestf("11/5/2016", "Central Standard Time");
            DateTimeTestf("3/12/2016", "Central Standard Time");
            DateTimeTestf(dtStartStg, destTimeZoneString);
        }

        static List<DateTimeOffset> DateTimeTestf(string dtStartStg, string destTimeZoneString) {
            Console.WriteLine($"\n start {dtStartStg} in '{destTimeZoneString}'");
            DateTimeOffset dtSttq = DateTimeOffset.Parse(dtStartStg);
            TimeSpan tsOffset2 = TimeZoneInfo.FindSystemTimeZoneById(destTimeZoneString).GetUtcOffset(dtSttq);
            DateTimeOffset dtSttl = new DateTimeOffset(dtSttq.Year, dtSttq.Month, dtSttq.Day, 0, 0, 0, tsOffset2);
            DateTimeOffset dt = dtSttl.UtcDateTime;

            List<DateTimeOffset> result = new List<DateTimeOffset>();
            for (int i = 0; i < 72; i++) {
                result.Add(dt);
                WT(i.ToString(), dt, destTimeZoneString);
                dt = dt.AddHours(1);
            }
            return result;
        }


        static void WT(string stg, DateTimeOffset dt, string destTimeZoneString) {
            try {
                DateTime dtodt = dt.DateTime;
                DateTime dtodtl = TimeZoneInfo.ConvertTimeFromUtc(dtodt, TimeZoneInfo.FindSystemTimeZoneById(destTimeZoneString));
                Console.WriteLine($"O {stg}=\t dto='{dt.ToString(StandardTimeFormat)}'  dtodt='{dtodt}' \t dtodtl='{dtodtl}' ");
            }
            catch (Exception exc) {
                Console.WriteLine($" {stg}=\t, '{dt}', {exc.Message},{exc.InnerException?.Message}");
            }
        }

        static void WT(DateTimeOffset[] dts) {
            string res = "";
            for (int i = 0; i < dts.Length; i++) {
                //res += dts[i].Kind.Equals(DateTimeKind.Utc) ? "U " + dts[i].ToString() : " L " + dts[i].ToString(StandardTimeFormat);
                Console.WriteLine($"o=\t{dts[i].ToString(StandardTimeFormat)}");
            }
            Console.WriteLine($" {res}");
        }


        static void WT(string stg, DateTime dt) {
            try {
                if (dt.Kind.Equals(DateTimeKind.Utc)) {
                    Console.WriteLine($"U {stg}=\t{dt.ToString()}");
                }
                else {
                    Console.WriteLine($"L {stg}=\t{dt.ToString(StandardTimeFormat)}");
                }
            }
            catch (Exception exc) {
                Console.WriteLine($" {stg}=\t{exc.Message},{exc.InnerException?.Message}");
            }
        }
        static void WT(DateTime[] dts) {
            string res = "";
            for (int i = 0; i < dts.Length; i++) {
                res += dts[i].Kind.Equals(DateTimeKind.Utc) ? "U " + dts[i].ToString() : " L " + dts[i].ToString(StandardTimeFormat);
            }
            Console.WriteLine($" {res}");
        }


        static void DateTimeTest5(ProcessingArgs pa) {
            string destTimeZoneString = pa.GetControlVal("destTimeZoneString", "Central Standard Time");
            TimeZoneInfo desTimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(destTimeZoneString);

            string dtStartStg = pa.GetControlVal("dtStartStg", "3/12/2017");
            //dtStartStg=11/5/2017
            //dtStartStg=3/12/2017

            DateTimeOffset dtSttq = DateTimeOffset.Parse(dtStartStg);

            TimeSpan tsOffset = TimeZoneInfo.FindSystemTimeZoneById(destTimeZoneString).GetUtcOffset(DateTime.UtcNow);


            DateTimeOffset dtSttl = new DateTimeOffset(dtSttq.Year, dtSttq.Month, dtSttq.Day, 0, 0, 0, tsOffset);
            DateTimeOffset dt = dtSttl.UtcDateTime;

            for (int i = 0; i < 24; i++) {
                WT(i.ToString(), dt, destTimeZoneString);
                dt = dt.AddHours(1);
            }
        }

        static void DateTimeTest4(ProcessingArgs pa) {
            string destTimeZoneString = pa.GetControlVal("destTimeZoneString", "Central Standard Time");
            TimeZoneInfo desTimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(destTimeZoneString);

            string dtStartStg = pa.GetControlVal("dtStartStg", "3/12/2017");
            DateTime dtSttq = DateTime.Parse(dtStartStg);
            DateTime dtSttu = new DateTime(dtSttq.Year, dtSttq.Month, dtSttq.Day, 0, 0, 0, DateTimeKind.Utc);
            DateTime dtStt = TimeZoneInfo.ConvertTime(dtSttu, TimeZoneInfo.Utc, desTimeZoneInfo);

            DateTime dtSttDataU = new DateTime(dtStt.Year, dtStt.Month, dtStt.Day, dtStt.Hour, dtStt.Minute, dtStt.Second, DateTimeKind.Utc);
            DateTime dt = dtSttDataU;

            WT("dtSttq", dtSttq);
            WT("dtSttu", dtSttu);
            WT("dtStt", dtStt);
            WT("dtSttDataU", dtSttDataU);
            WT("dt", dt);

            for (int i = 0; i < 24; i++) {
                WT(i.ToString(), dt);
                dt = dt.AddHours(1);
            }
        }


        public const string StandardTimeFormat = "yyyy-MM-ddTHH:mm:ss.fffzz00";


        static void DateTimeTest(ProcessingArgs pa) {
            string destTimeZoneString = pa.GetControlVal("destTimeZoneString", "Central Standard Time");
            TimeZoneInfo desTimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(destTimeZoneString);

            string dtStartStg = pa.GetControlVal("dtStartStg", "3/12/2017");
            DateTime dtSttq = DateTime.Parse(dtStartStg);
            DateTime dtSttu = new DateTime(dtSttq.Year, dtSttq.Month, dtSttq.Day, 0, 0, 0, DateTimeKind.Utc);
            WT("dtSttq", dtSttq);
            WT("dtSttu", dtSttu);


            DateTime dtStt = TimeZoneInfo.ConvertTime(dtSttu, TimeZoneInfo.Utc, desTimeZoneInfo);
            WT("dtStt", dtStt);
            DateTime dtUse = dtStt;
            WT("dtUse", dtUse);
            for (int i = 0; i < 24; i++) {
                DateTime ndtl = TimeZoneInfo.ConvertTime(dtUse, desTimeZoneInfo, TimeZoneInfo.Utc);
                WT(i.ToString(), ndtl);
                dtUse = dtUse.AddHours(1);
            }
        }


        static void DateTimeTest3(ProcessingArgs pa) {
            string destTimeZoneString = pa.GetControlVal("destTimeZoneString", "Central Standard Time");
            TimeZoneInfo desTimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(destTimeZoneString);

            string dtStartStg = pa.GetControlVal("dtStartStg", "3/12/2017");
            DateTime dtSttq = DateTime.Parse(dtStartStg);
            DateTime dtSttu = new DateTime(dtSttq.Year, dtSttq.Month, dtSttq.Day, 0, 0, 0, DateTimeKind.Utc);
            WT("dtSttq", dtSttq);
            WT("dtSttu", dtSttu);
            DateTime dtStt = TimeZoneInfo.ConvertTime(dtSttu, TimeZoneInfo.Utc, desTimeZoneInfo);
            WT("dtStt", dtStt);

            DateTime dtSttDataU = new DateTime(dtStt.Year, dtStt.Month, dtStt.Day, dtStt.Hour, dtStt.Minute, dtStt.Second, DateTimeKind.Utc);

            WT("dtSttDataU", dtSttDataU);
            DateTime dtUse = dtSttDataU;
            WT("dtUse", dtUse);
            for (int i = 0; i < 24; i++) {
                //DateTime ndtl = TimeZoneInfo.ConvertTime(dtUse, desTimeZoneInfo, TimeZoneInfo.Utc);
                WT(i.ToString(), dtUse);
                dtUse = dtUse.AddHours(1);
            }
        }



        private static ProcessingArgs _watcherPa;

        private static void InitializeWorkerProcessors(ContextData context) {
            _watcherPa = context.CtxArgs;
            string dropSourceDir = _watcherPa.GetControlVal("dropLocation", @"C:\_Test\drop");
            string dropRelocateDir = _watcherPa.GetControlVal("dropRelocate", @"C:\_Test\drop\snagged");

            FilesProcessor inputProcessor = new FilesProcessor(context, _watcherPa, dropSourceDir, dropRelocateDir);
            if (_watcherPa.GetControlVal("enableProcessed", "no").Equals("yes")) {
                inputProcessor.SetMoveProcessor(ProcessDroppedFile);
            }

            if (_watcherPa.GetControlVal("dropFlushPendingAndExit", "no").Equals("yes")) {
                inputProcessor.FlushPending();
                inputProcessor.RelocateChanges(new List<string>(inputProcessor.WatchingDoneList));
                Console.WriteLine($" InitializeWorkerProcessors   dropFlushPendingAndExit  == bye bye {inputProcessor.Status()}.");
                return;
            }
            if (_watcherPa.GetControlVal("dropFlushPending", "no").Equals("yes")) {
                inputProcessor.FlushPending();

            }


            int mainerrorTermCount = _watcherPa.GetControlVal("mainerrorTermCount", 100);
            int mainLoopDelay = _watcherPa.GetControlVal("mainLoopDelay", 5000);
            if (mainLoopDelay < 1000) mainLoopDelay = 1000;
            int dropHoldCount = _watcherPa.GetControlVal("dropHoldCount", 0);
            int watchBatchLimit = _watcherPa.GetControlVal("watchBatchLimit", 5);
            int mainErrorCount = 0;
            DateTime dtStt = DateTime.Now;
            int cycleCounter = 0;
            for (; ; ) {
                try {
                    int batchExec = 0;
                    int idle = 0;
                    int errored = 0;
                    if (inputProcessor.RequestTerminate) {
                        dropHoldCount = 0;
                    }
                    else {
                        if (!string.IsNullOrEmpty(inputProcessor.ConfigChangeData)) {
                            try {
                                string configReq = inputProcessor.ConfigChangeData;
                                string[] configLine = configReq.Split(new[] { Environment.NewLine },
                                    StringSplitOptions.RemoveEmptyEntries);
                                bool resetWatchers = false;
                                for (int i = 0; i < configLine.Length; i++) {  // want to respect order of params
                                    try {
                                        Console.WriteLine($" **** Processing ConfigurationChange data: {configLine[i]}  ");
                                        _watcherPa.AddIfMissing(configLine[i], true);
                                        inputProcessor.FpArgs.AddIfMissing(configLine[i], true);
                                        if (configLine.Contains("dropLocation") ||
                                            configLine.Contains("dropRelocate")) {
                                            resetWatchers = true;
                                        }
                                    }
                                    catch (Exception exc) {
                                        Console.WriteLine(
                                            $" **** Processing ConfigurationChange exception '{configLine[i]}' = {exc.Message} , {exc.InnerException?.Message} ");
                                    }
                                    mainerrorTermCount = _watcherPa.GetControlVal("mainerrorTermCount", 100);
                                    mainLoopDelay = _watcherPa.GetControlVal("mainLoopDelay", 5000);
                                    if (mainLoopDelay < 1000) mainLoopDelay = 1000;
                                    dropHoldCount = _watcherPa.GetControlVal("dropHoldCount", 2);
                                    watchBatchLimit = _watcherPa.GetControlVal("watchBatchLimit", 5);
                                    if (resetWatchers) {
                                        inputProcessor.ResetWatchers(_watcherPa);
                                    }
                                }
                            }
                            catch (Exception exc) {
                                Console.WriteLine(
                                    $" **** Processing ConfigurationChange ouoter exception = {exc.Message} , {exc.InnerException?.Message} ");
                            }
                            finally {
                                inputProcessor.ConfigChangeData = "";
                            }
                        }
                        if (!inputProcessor.RequestPause) {
                            while (inputProcessor.QueuedList.Count > 0) { // allow a few to be dequed and executed.
                                int stat = inputProcessor.DoProcessOne();
                                switch (stat) {
                                    case 0:
                                        batchExec++; // a successful execution
                                        break;
                                    case -1:
                                        idle++; // a successful execution
                                        break;
                                    default:
                                        errored++;
                                        break;
                                }
                                if ((errored > 0) || (batchExec >= watchBatchLimit) ||
                                    inputProcessor.RequestTerminate || inputProcessor.RequestPause) {
                                    break;
                                }
                            }
                        }
                    }
                    string status = inputProcessor.Status();
                    int changeCount = inputProcessor.Count();
                    Console.WriteLine(
                        $" {DateTime.Now} in watcher for {(int)DateTime.Now.Subtract(dtStt).TotalMinutes} mins, cycles={cycleCounter++}   input : paused={inputProcessor.RequestPause} queued={inputProcessor.QueuedList.Count} count={changeCount} batchexec={batchExec} idle={idle} berr={errored} status={status} ....");
                    //Console.WriteLine($" in watcher and waiting process : count={processProcessor.Count()} status={processProcessor.Status()} ....");
                    if (changeCount > 0) {
                        List<string> changeList = inputProcessor.GetChangeList();
                        foreach (string change in changeList) {
                            Console.WriteLine(change);
                        }
                        if (changeCount > dropHoldCount) {
                            Console.WriteLine($" in watcher and removing  ....");
                            List<string>[] results = inputProcessor.RelocateChanges(changeList);
                            string resultsStat = "";
                            foreach (List<string> result in results) {
                                resultsStat += ((result != null) ? result.Count.ToString() : "[null]") + ",";
                            }
                            Console.WriteLine(
                                $" after remove : count={inputProcessor.Count()} status={inputProcessor.Status()} ; results status = {resultsStat} ....");
                            //foreach (string change in changeList) {
                            //    Console.WriteLine(change);
                            //}

                            List<string> errors = results[results.Length - 1];
                            foreach (string error in errors) {
                                Console.WriteLine($" notes/errors : {error}");
                            }
                        }
                    }
                }
                catch (Exception exc) {
                    mainErrorCount++;
                    Console.WriteLine($"InitializeWorkerProcessors: exception count={mainErrorCount} is {exc.Message}, inner={exc.InnerException?.Message}  {inputProcessor?.Status()}");
                }
                Thread.Sleep(mainLoopDelay);
                if (inputProcessor.RequestTerminate || (mainErrorCount > mainerrorTermCount)) {
                    Console.WriteLine($"InitializeWorkerProcessors: ending count={mainErrorCount} requestTerm={inputProcessor.RequestTerminate}  {inputProcessor?.Status()}");
                    break;
                }
            }
            context.Add2Counter("cntr_fpFileRetries", inputProcessor.FileRetries);
            context.Add2Counter("cntr_fpFileOpenRetries", inputProcessor.FileOpenRetries);
            context.Add2Counter("cntr_fpFileConfigRetries", inputProcessor.FileConfigRetries);
            context.Add2Counter("cntr_fpFilesDropped", inputProcessor.FilesDropped);
            context.Add2Counter("cntr_fpConfigDropped", inputProcessor.ConfigDropped);
            context.Add2Counter("cntr_fpFilesRelocated", inputProcessor.FilesRelocated);
            context.Add2Counter("cntr_fpFilesIgnored", inputProcessor.FilesIgnored);
        }

        public static string ProcessDroppedFile(FilesProcessor src, string pathName) {
            ProcessingArgs pa = src.FpArgs;
            string result = "0";
            int procLimit = pa.GetControlVal("procLimit", 5000000);

            // dislike this, however we are stuck with client id in start of file name.
            string fileName = Path.GetFileName(pathName);
            string[] fnameParts = fileName.Split(new[] {'_'}, StringSplitOptions.RemoveEmptyEntries);
            pa["clientId"] = fnameParts[0];

            BatchUtils.EstablishIntervalEnvironment(pa, pathName);
            string useEnv = pa.GetControlVal("aclenv", "work");


            //if (pathName.ToLower().Contains("60min")) { // BUGBUG: don't like these assumptions but for now....
            //    useEnv = pa.GetControlVal("hourlyEnv", "perfh");
            //}
            //if (pathName.ToLower().Contains("daily")) { // BUGBUG: don't like these assumptions but for now....
            //    useEnv = pa.GetControlVal("hourlyEnv", "perfd");
            //}

            string impCmd = $"{useEnv}importFileCmd";
            string processCommand = pa.GetControlVal(impCmd, "");
            if (!string.IsNullOrEmpty(processCommand)) {
                string tableName = pa.GetControlVal("reqVsrc", "consumption_by_meter");
                string execCmd = string.Format(processCommand, pathName, useEnv, procLimit, tableName);
                string addlImportPars = pa.GetControlVal("addlImportPars", "");
                if (!string.IsNullOrEmpty(addlImportPars)) { // if we want to pass along additional control parameters
                    string[] addlPars = addlImportPars.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string addlPar in addlPars) {
                        string addVal = pa.GetControlVal(addlPar, "");
                        switch ((addlPar)) {
                            case "outputPath": { // special case
                                string[] pathVals = addVal.Split('\\');
                                if (pathVals.Length > 1) {
                                    addVal = $"{pathVals[0]}\\{pathVals[1]}";
                                }
                            }
                                break;
                        }
                        string addThis = $" {addlPar}={addVal}";
                        execCmd += $" {addlPar}={addThis}";
                    }
                }

                string[] procArgs = execCmd.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                Console.WriteLine($"  wants to exec {execCmd}");
                try {
                    int rVal = MainProcess(procArgs);
                    result = rVal.ToString();
                }
                catch (Exception exc) {
                    Console.WriteLine(
                        $"MainProcess.ProcessDroppedFile: evc exception is {exc.Message}, inner={exc.InnerException?.Message} stack={exc.StackTrace}");
                }
            }
            if (!string.IsNullOrEmpty(pa.GetControlVal("allowFailuresCode", ""))) {
                result = pa.GetControlVal("allowFailuresCode", result);
            }

            return result;
        }

        public static string Wide2TallProcessZipFile(FilesProcessor src, string srcName, string destName) {
            string result = "1";
            if (File.Exists(srcName)) {
                string destPath = Path.GetDirectoryName(destName);
                if (!string.IsNullOrEmpty(destPath) && !Directory.Exists(destPath)) {
                    Directory.CreateDirectory(destPath);
                }
            }
            return result;
        }



    }


    public class SimpleWebServerLocal : SimpleWebServer {
        public static void LocalLog(string msg) {
            Console.WriteLine(msg);
        }

        public SimpleWebServerLocal(ProcessingArgs pa) : base(pa) {
            SetLogFn(LocalLog);
            CheckEndpoints(pa);
        }


        public void CheckEndpoints(ProcessingArgs pa) {
            //var epName = "AdvAmiEP";
            string hostAddr = pa.GetControlVal("hostIp", "127.0.0.1");
            string hostPort = pa.GetControlVal("hostPort", "443");
            string tmpStg = ConfigurationManager.AppSettings["AdvAmiHostAddress"];
            if (!string.IsNullOrEmpty(tmpStg)) {
                hostAddr = tmpStg;
            }
            tmpStg = ConfigurationManager.AppSettings["AdvAmiHostPort"];
            if (!string.IsNullOrEmpty(tmpStg)) {
                hostPort = tmpStg;
            }
            //bool cloudConfigSuccess = false;
            try {
                pa["hostIp"] = hostAddr;
                pa["port"] = hostPort;
                InitializeServer(pa);
            }
            catch (Exception e) {
                Trace.TraceError(
                    $"Caught exception in CheckEndpoint SimpleWebServer. {e.Message}, {e.InnerException?.Message} Details: {0}", e);
                Logit($"Event Process Worker  Caught exception in CheckEndpoint SimpleWebServer. {e.Message}, {e.InnerException?.Message}");
            }
        }


    }


    //static class Extensions {
    //    public static IList<T> Clone<T>(this IList<T> listToClone) where T : ICloneable {
    //        return listToClone.Select(item => (T)item.Clone()).ToList();
    //    }
    //}





}

