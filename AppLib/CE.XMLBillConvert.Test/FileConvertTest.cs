﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CE.XMLBillConvert.ConsoleApp;

namespace CE.XMLBillConvert.Test
{
    [TestClass]
    public class FileConvertTest
    {
        #region Constants
        private const string _accountXML = @"<Accounts xmlns=""Nexus:BillAnalyzer""><Account Account=""0002000710"" Addr1=""408 W MARKET ST"" Addr2="""" City=""CHRISTOPHER"" FirstName=""PAT"" LastName=""JURICH"" State=""IL"" Zip=""62822"" BalanceDate=""2017-06-05"" BalanceAmount=""1338.17"" LastPaymentDate=""2017-01-30"" LastPaymentAmount=""368.55"" Description=""Single Family"" >
<AccountIndicator IndicatorType=""BudgetBillingStatus"" IndicatorValue=""N"" /><Bill BillCycle=""Monthly"" BillDate=""2017-06-05"" BillMonth=""06"" LastPaymentAmount=""368.55"" LastPaymentDate=""2017-01-30"" DueDate=""2017-06-27"" PaymentType=""check/cash"" PastDue=""1339.2"" TotalAmount=""1680.23"" >
<Premise PremiseId=""200051   "" FirstName=""PAT"" LastName=""JURICH"" Addr1=""408 W MARKET ST"" Addr2="""" City=""CHRISTOPHER"" State=""IL"" Zip=""62822"" Description=""Single Family"" >
<Service AccountType=""R"" ServiceId=""63800500"" NextReadDate=""2017-07-03"" Fuel=""E"" TotalServiceAmount=""127.57"" TotalServiceUse=""1304"" Units=""kWh"" RetailSupply=""ConsolidatedBilling"" RetailSupplierName=""POST 06 - BGS1"" ReadCycle=""5"" >
<Meter Meter=""00400625"" MeterType=""Regular"" TwoWay=""0"" MeterMultiplier=""1"" MeterLocation=""Outside"" ServicePoint=""63800500"" Description=""Electric"">
<ReadInterval RateClass=""DS1 BGS RZ 1B"" RateGroup=""1"" BillDays=""30"" StartDate=""2017-05-04"" EndDate=""2017-06-02"">
<RateClassAttributes>
 <add value=""DS1 BGS RZ 1B"" key=""rc""/>
 <add value=""false"" key=""idre""/>
</RateClassAttributes>
<UseDetail Amount=""105.02"" ClientType=""TOTAL"" NexusType=""TOTAL"" Quantity=""1304"" Units=""kWh"" ReadType=""Estimated"" StartMeterRead=""47831"" EndMeterRead=""49135"">
<AmountDetail Amount=""35.72"" ClientType=""Electric Delivery"" NexusType=""DIST_ENGY""/>
<AmountDetail Amount=""69.30"" ClientType=""Electric Supply"" NexusType=""SUPP_ENGY""/>
</UseDetail>
</ReadInterval>
</Meter>
<Service_Charge Amount=""12.32"" ClientType=""Monthly Charges (2017-06-02)"" Date=""2017-06-05"" Description=""Monthly Charges (2017-06-02)"" NexusType=""STD_CUST""/>
<Service_Charge Amount=""10.23"" ClientType=""Taxes (Electric 2017-06-02)"" Date=""2017-06-05"" Description=""Taxes (Electric 2017-06-02)"" NexusType=""GOVT_OTHR""/>
</Service>
<Service AccountType=""R"" ServiceId=""83800509"" NextReadDate=""2017-07-03"" Fuel=""G"" TotalServiceAmount=""213.46"" TotalServiceUse=""281"" Units=""Therms"" RetailSupply=""ConsolidatedBilling"" RetailSupplierName=""SGS1"" ReadCycle=""5"" >
<Meter Meter=""02222227"" MeterType=""AMR"" TwoWay=""1"" MeterMultiplier=""1"" MeterLocation=""Outside"" ServicePoint=""83800509"" Description=""Gas"">
<ReadInterval RateClass=""GDS1 RZ 1B"" RateGroup=""3"" BillDays=""30"" StartDate=""2017-05-04"" EndDate=""2017-06-02"">
<UseDetail Amount=""168.65"" ClientType=""TOTAL"" NexusType=""TOTAL"" Quantity=""281"" Units=""Therms"" ReadType=""Estimated"" StartMeterRead=""1815"" EndMeterRead=""2096"">
<AmountDetail Amount=""51.46"" ClientType=""Gas Delivery"" NexusType=""DIST_ENGY""/>
<AmountDetail Amount=""117.19"" ClientType=""Gas Supply"" NexusType=""SUPP_ENGY""/>
</UseDetail>
</ReadInterval>
</Meter>
<Service_Charge Amount=""21.82"" ClientType=""Monthly Charges (2017-06-02)"" Date=""2017-06-05"" Description=""Monthly Charges (2017-06-02)"" NexusType=""STD_CUST""/>
<Service_Charge Amount=""22.99"" ClientType=""Taxes (Gas 2017-06-02)"" Date=""2017-06-05"" Description=""Taxes (Gas 2017-06-02)"" NexusType=""GOVT_OTHR""/>
</Service>
</Premise>
</Bill></Account><Account Account=""0003017915"" Addr1=""335 S 16TH ST UNIT 1/2"" Addr2="""" City=""QUINCY"" FirstName=""KATHY"" LastName=""DANCER"" State=""IL"" Zip=""62301"" BalanceDate=""2017-06-05"" BalanceAmount=""496.9"" LastPaymentDate=""2017-01-25"" LastPaymentAmount=""131.21"" Description=""Apartment"" >
<AccountIndicator IndicatorType=""BudgetBillingStatus"" IndicatorValue=""N"" /><Bill BillCycle=""Monthly"" BillDate=""2017-06-05"" BillMonth=""06"" LastPaymentAmount=""131.21"" LastPaymentDate=""2017-01-25"" DueDate=""2017-06-27"" PaymentType=""check/cash"" PastDue=""380.24"" TotalAmount=""506.05"" >
<Premise PremiseId=""301771   "" FirstName=""KATHY"" LastName=""DANCER"" Addr1=""335 S 16TH ST UNIT 1/2"" Addr2="""" City=""QUINCY"" State=""IL"" Zip=""62301"" Description=""Apartment"" >
<Service AccountType=""R"" ServiceId=""33580207"" NextReadDate=""2017-07-03"" Fuel=""E"" TotalServiceAmount=""97.06"" TotalServiceUse=""1064"" Units=""kWh"" RetailSupply=""ConsolidatedBilling"" RetailSupplierName=""First Energy Solutions"" ReadCycle=""5"" >
<Meter Meter=""39793374"" MeterType=""Regular"" TwoWay=""0"" MeterMultiplier=""1"" MeterLocation=""North"" ServicePoint=""33580207"" Description=""Electric"">
<ReadInterval RateClass=""DS1 ARES RZ 1B"" RateGroup=""1"" BillDays=""30"" StartDate=""2017-05-04"" EndDate=""2017-06-02"">
<RateClassAttributes>
 <add value=""DS1 ARES RZ 1B"" key=""rc""/>
 <add value=""false"" key=""idre""/>
</RateClassAttributes>
<UseDetail Amount=""76.40"" ClientType=""TOTAL"" NexusType=""TOTAL"" Quantity=""1064"" Units=""kWh"" ReadType=""Estimated"" StartMeterRead=""11999"" EndMeterRead=""13063"">
<AmountDetail Amount=""31.78"" ClientType=""Electric Delivery"" NexusType=""DIST_ENGY""/>
<AmountDetail Amount=""44.62"" ClientType=""Electric Supply"" NexusType=""SUPP_ENGY""/>
</UseDetail>
</ReadInterval>
</Meter>
<Service_Charge Amount=""12.32"" ClientType=""Monthly Charges (2017-06-02)"" Date=""2017-06-05"" Description=""Monthly Charges (2017-06-02)"" NexusType=""STD_CUST""/>
<Service_Charge Amount=""8.34"" ClientType=""Taxes (Electric 2017-06-02)"" Date=""2017-06-05"" Description=""Taxes (Electric 2017-06-02)"" NexusType=""GOVT_OTHR""/>
</Service>
<Service AccountType=""R"" ServiceId=""53580206"" NextReadDate=""2017-07-03"" Fuel=""G"" TotalServiceAmount=""28.75"" TotalServiceUse=""9"" Units=""Therms"" RetailSupply=""ConsolidatedBilling"" RetailSupplierName=""SGS1"" ReadCycle=""5"" >
<Meter Meter=""91910265"" MeterType=""Regular"" TwoWay=""0"" MeterMultiplier=""1"" MeterLocation=""South"" ServicePoint=""53580206"" Description=""Gas"">
<ReadInterval RateClass=""GDS1 RZ 1B"" RateGroup=""3"" BillDays=""30"" StartDate=""2017-05-04"" EndDate=""2017-06-02"">
<UseDetail Amount=""6.17"" ClientType=""TOTAL"" NexusType=""TOTAL"" Quantity=""9"" Units=""Therms"" ReadType=""Estimated"" StartMeterRead=""342"" EndMeterRead=""351"">
<AmountDetail Amount=""2.41"" ClientType=""Gas Delivery"" NexusType=""DIST_ENGY""/>
<AmountDetail Amount=""3.76"" ClientType=""Gas Supply"" NexusType=""SUPP_ENGY""/>
</UseDetail>
</ReadInterval>
</Meter>
<Service_Charge Amount=""21.82"" ClientType=""Monthly Charges (2017-06-02)"" Date=""2017-06-05"" Description=""Monthly Charges (2017-06-02)"" NexusType=""STD_CUST""/>
<Service_Charge Amount=""0.76"" ClientType=""Taxes (Gas 2017-06-02)"" Date=""2017-06-05"" Description=""Taxes (Gas 2017-06-02)"" NexusType=""GOVT_OTHR""/>
</Service>
</Premise>
</Bill></Account><Account Account=""0019021041"" Addr1=""6902 N WHITE FIR DR"" Addr2="""" City=""EDWARDS"" FirstName=""KATIE"" LastName=""MAURITZSON"" State=""IL"" Zip=""61528"" BalanceDate=""2017-06-05"" BalanceAmount=""0"" LastPaymentDate=""2016-11-29"" LastPaymentAmount=""769.23"" Description=""Single Family"" >
<AccountIndicator IndicatorType=""BudgetBillingStatus"" IndicatorValue=""N"" /><Bill BillCycle=""Monthly"" BillDate=""2017-06-05"" BillMonth=""06"" LastPaymentAmount=""769.23"" LastPaymentDate=""2016-11-29"" DueDate=""2017-06-27"" PaymentType=""direct payment"" PastDue=""0"" TotalAmount=""0"" >
<Premise PremiseId=""1902110  "" FirstName=""KATIE"" LastName=""MAURITZSON"" Addr1=""6902 N WHITE FIR DR"" Addr2="""" City=""EDWARDS"" State=""IL"" Zip=""61528"" Description=""Single Family"" >
<Service AccountType=""R"" ServiceId=""11057767"" NextReadDate=""2017-07-06"" Fuel=""G"" TotalServiceAmount=""80.11"" TotalServiceUse=""85"" Units=""Therms"" RetailSupply=""ConsolidatedBilling"" RetailSupplierName=""SGS1"" ReadCycle=""8"" >
<Meter Meter=""20927706"" MeterType=""AMR"" TwoWay=""0"" MeterMultiplier=""1"" MeterLocation=""Outside"" ServicePoint=""11057767"" Description=""Gas"">
<ReadInterval RateClass=""GDS1 RZ 2"" RateGroup=""3"" BillDays=""32"" StartDate=""2017-05-05"" EndDate=""2017-06-05"">
<UseDetail Amount=""51.40"" ClientType=""TOTAL"" NexusType=""TOTAL"" Quantity=""85"" Units=""Therms"" ReadType=""Actual"" StartMeterRead=""4206"" EndMeterRead=""4291"">
<AmountDetail Amount=""16.11"" ClientType=""Gas Delivery"" NexusType=""DIST_ENGY""/>
<AmountDetail Amount=""35.29"" ClientType=""Gas Supply"" NexusType=""SUPP_ENGY""/>
</UseDetail>
</ReadInterval>
</Meter>
<Service_Charge Amount=""21.82"" ClientType=""Monthly Charges (2017-06-05)"" Date=""2017-06-05"" Description=""Monthly Charges (2017-06-05)"" NexusType=""STD_CUST""/>
<Service_Charge Amount=""6.89"" ClientType=""Taxes (Gas 2017-06-05)"" Date=""2017-06-05"" Description=""Taxes (Gas 2017-06-05)"" NexusType=""GOVT_OTHR""/>
</Service>
<Service AccountType=""R"" ServiceId=""27171209"" NextReadDate=""2017-07-06"" Fuel=""E"" TotalServiceAmount=""73.95"" TotalServiceUse=""649"" Units=""kWh"" RetailSupply=""ConsolidatedBilling"" RetailSupplierName=""Homefield Energy 2"" ReadCycle=""8"" >
<Meter Meter=""94261832"" MeterType=""AMR"" TwoWay=""0"" MeterMultiplier=""1"" MeterLocation=""Outside"" ServicePoint=""27171209"" Description=""Electric"">
<ReadInterval RateClass=""DS1 ARES RZ 2"" RateGroup=""1"" BillDays=""32"" StartDate=""2017-05-05"" EndDate=""2017-06-05"">
<RateClassAttributes>
 <add value=""DS1 ARES RZ 2"" key=""rc""/>
 <add value=""false"" key=""idre""/>
</RateClassAttributes>
<UseDetail Amount=""55.05"" ClientType=""TOTAL"" NexusType=""TOTAL"" Quantity=""649"" Units=""kWh"" ReadType=""Actual"" StartMeterRead=""20644"" EndMeterRead=""21293"">
<AmountDetail Amount=""24.25"" ClientType=""Electric Delivery"" NexusType=""DIST_ENGY""/>
<AmountDetail Amount=""30.80"" ClientType=""Electric Supply"" NexusType=""SUPP_ENGY""/>
</UseDetail>
</ReadInterval>
</Meter>
<Service_Charge Amount=""12.32"" ClientType=""Monthly Charges (2017-06-05)"" Date=""2017-06-05"" Description=""Monthly Charges (2017-06-05)"" NexusType=""STD_CUST""/>
<Service_Charge Amount=""6.58"" ClientType=""Taxes (Electric 2017-06-05)"" Date=""2017-06-05"" Description=""Taxes (Electric 2017-06-05)"" NexusType=""GOVT_OTHR""/>
</Service>
</Premise>
</Bill></Account><Account Account=""0019118027"" Addr1=""918 N SHERIDAN RD APT 4"" Addr2="""" City=""PEORIA"" FirstName=""DAVID"" LastName=""MARTIN"" State=""IL"" Zip=""61606"" BalanceDate=""2017-06-05"" BalanceAmount=""40.26"" LastPaymentDate=""2016-11-25"" LastPaymentAmount=""201.75"" Description=""Apartment"" >
<AccountIndicator IndicatorType=""BudgetBillingStatus"" IndicatorValue=""N"" /><Bill BillCycle=""Monthly"" BillDate=""2017-06-05"" BillMonth=""06"" LastPaymentAmount=""201.75"" LastPaymentDate=""2016-11-25"" DueDate=""2017-06-27"" PaymentType=""check/cash"" PastDue=""0"" TotalAmount=""40.26"" >
<Premise PremiseId=""1911810  "" FirstName=""DAVID"" LastName=""MARTIN"" Addr1=""918 N SHERIDAN RD APT 4"" Addr2="""" City=""PEORIA"" State=""IL"" Zip=""61606"" Description=""Apartment"" >
<Service AccountType=""R"" ServiceId=""14567768"" NextReadDate=""2017-07-07"" Fuel=""E"" TotalServiceAmount=""25.59"" TotalServiceUse=""97"" Units=""kWh"" RetailSupply=""ConsolidatedBilling"" RetailSupplierName=""Homefield Energy 2"" ReadCycle=""8"" >
<Meter Meter=""94326997"" MeterType=""AMR"" TwoWay=""0"" MeterMultiplier=""1"" MeterLocation=""Outside"" ServicePoint=""14567768"" Description=""Electric"">
<ReadInterval RateClass=""DS1 ARES RZ 2"" RateGroup=""1"" BillDays=""32"" StartDate=""2017-05-05"" EndDate=""2017-06-05"">
<RateClassAttributes>
 <add value=""DS1 ARES RZ 2"" key=""rc""/>
 <add value=""false"" key=""idre""/>
</RateClassAttributes>
<UseDetail Amount=""12.29"" ClientType=""TOTAL"" NexusType=""TOTAL"" Quantity=""97"" Units=""kWh"" ReadType=""Actual"" StartMeterRead=""2256"" EndMeterRead=""2353"">
<AmountDetail Amount=""7.69"" ClientType=""Electric Delivery"" NexusType=""DIST_ENGY""/>
<AmountDetail Amount=""4.60"" ClientType=""Electric Supply"" NexusType=""SUPP_ENGY""/>
</UseDetail>
</ReadInterval>
</Meter>
<Service_Charge Amount=""12.32"" ClientType=""Monthly Charges (2017-06-05)"" Date=""2017-06-05"" Description=""Monthly Charges (2017-06-05)"" NexusType=""STD_CUST""/>
<Service_Charge Amount=""0.98"" ClientType=""Taxes (Electric 2017-06-05)"" Date=""2017-06-05"" Description=""Taxes (Electric 2017-06-05)"" NexusType=""GOVT_OTHR""/>
</Service>
<Service AccountType=""R"" ServiceId=""21377925"" NextReadDate=""2017-07-07"" Fuel=""G"" TotalServiceAmount=""24.79"" TotalServiceUse=""2"" Units=""Therms"" RetailSupply=""ConsolidatedBilling"" RetailSupplierName=""SGS1"" ReadCycle=""8"" >
<Meter Meter=""21411308"" MeterType=""AMR"" TwoWay=""0"" MeterMultiplier=""1"" MeterLocation=""Outside"" ServicePoint=""21377925"" Description=""Gas"">
<ReadInterval RateClass=""GDS1 RZ 2"" RateGroup=""3"" BillDays=""32"" StartDate=""2017-05-05"" EndDate=""2017-06-05"">
<UseDetail Amount=""1.99"" ClientType=""TOTAL"" NexusType=""TOTAL"" Quantity=""2"" Units=""Therms"" ReadType=""Actual"" StartMeterRead=""67"" EndMeterRead=""69"">
<AmountDetail Amount=""1.15"" ClientType=""Gas Delivery"" NexusType=""DIST_ENGY""/>
<AmountDetail Amount=""0.84"" ClientType=""Gas Supply"" NexusType=""SUPP_ENGY""/>
</UseDetail>
</ReadInterval>
</Meter>
<Service_Charge Amount=""21.82"" ClientType=""Monthly Charges (2017-06-05)"" Date=""2017-06-05"" Description=""Monthly Charges (2017-06-05)"" NexusType=""STD_CUST""/>
<Service_Charge Amount=""0.98"" ClientType=""Taxes (Gas 2017-06-05)"" Date=""2017-06-05"" Description=""Taxes (Gas 2017-06-05)"" NexusType=""GOVT_OTHR""/>
</Service>
</Premise>
</Bill></Account><Account Account=""0029029658"" Addr1=""6859 E 2000TH AVE"" Addr2="""" City=""ANNAPOLIS"" FirstName=""MICHAEL"" LastName=""ALSPACH"" State=""IL"" Zip=""62413"" BalanceDate=""2017-06-05"" BalanceAmount=""81.87"" LastPaymentDate=""2017-05-17"" LastPaymentAmount=""380.67"" Description=""Single Family"" >
<AccountIndicator IndicatorType=""BudgetBillingStatus"" IndicatorValue=""N"" /><Bill BillCycle=""Monthly"" BillDate=""2017-06-05"" BillMonth=""06"" LastPaymentAmount=""380.67"" LastPaymentDate=""2017-05-17"" DueDate=""2017-06-27"" PaymentType=""check/cash"" PastDue=""0"" TotalAmount=""81.87"" >
<Premise PremiseId=""2902941  "" FirstName=""MICHAEL"" LastName=""ALSPACH"" Addr1=""6859 E 2000TH AVE"" Addr2="""" City=""ANNAPOLIS"" State=""IL"" Zip=""62413"" Description=""Single Family"" >
<Service AccountType=""R"" ServiceId=""43230064"" NextReadDate=""2017-07-06"" Fuel=""E"" TotalServiceAmount=""81.87"" TotalServiceUse=""1384"" Units=""kWh"" RetailSupply=""ConsolidatedBilling"" RetailSupplierName=""Direct Energy Services"" ReadCycle=""6"" >
<Meter Meter=""71841975"" MeterType=""AMR"" TwoWay=""1"" MeterMultiplier=""1"" MeterLocation=""West"" ServicePoint=""43230064"" Description=""Electric"">
<ReadInterval RateClass=""DS1 ARES RZ 1B"" RateGroup=""5"" BillDays=""30"" StartDate=""2017-05-03"" EndDate=""2017-06-01"">
<RateClassAttributes>
 <add value=""DS1 ARES RZ 1B"" key=""rc""/>
 <add value=""false"" key=""idre""/>
</RateClassAttributes>
<UseDetail Amount=""51.59"" ClientType=""TOTAL"" NexusType=""TOTAL"" Quantity=""1384"" Units=""kWh"" ReadType=""Actual"" StartMeterRead=""55740"" EndMeterRead=""57124"">
<AmountDetail Amount=""35.91"" ClientType=""Electric Delivery"" NexusType=""DIST_ENGY""/>
<AmountDetail Amount=""15.68"" ClientType=""Electric Supply"" NexusType=""SUPP_ENGY""/>
</UseDetail>
</ReadInterval>
</Meter>
<Service_Charge Amount=""7.10"" ClientType=""Electric Lighting"" Date=""2017-06-05"" Description=""Electric Lighting"" NexusType=""OTHR_OTHR""/>
<Service_Charge Amount=""12.32"" ClientType=""Monthly Charges (2017-06-01)"" Date=""2017-06-05"" Description=""Monthly Charges (2017-06-01)"" NexusType=""STD_CUST""/>
<Service_Charge Amount=""10.86"" ClientType=""Taxes (Electric+Lighting)"" Date=""2017-06-05"" Description=""Taxes (Electric+Lighting)"" NexusType=""GOVT_OTHR""/>
</Service>
</Premise>
</Bill></Account></Accounts>";
#endregion

        [TestMethod]
        [DeploymentItem("C:\\Dev\\ACEx Branches\\17.12\\AppLib\\CE.XMLBillConvert.Test\\GoodData\\174_test.xml","GoodData")]
        public void TestFileConvertGoodFile()
        {
            FileConvert fc = new FileConvert();
            System.IO.TextReader tr = new System.IO.StreamReader("GoodData\\174_test.xml");

            List<string> results = fc.Convert(tr, "174"); // arbitrary clientid
            Assert.IsTrue(results.Count == 339); 
            
        }
        [TestMethod]
        [DeploymentItem("C:\\Dev\\ACEx Branches\\17.12\\AppLib\\CE.XMLBillConvert.Test\\BadData\\174_test_bad_xml.xml", "BadData")]
        public void TestFileConvertBadXMLFile()
        {
            FileConvert fc = new FileConvert();
            System.IO.TextReader tr = new System.IO.StreamReader("BadData\\174_test_bad_xml.xml");
            try
            {
                List<string> results = fc.Convert(tr, "174"); // arbitrary clientid
            } catch (Exception ex)
            {
                Assert.IsTrue(ex.Message == "The 'RateClassAttributes' start tag on line 7 position 2 does not match the end tag of 'ReadInterval'. Line 15, position 3.");
            }
        }

        [TestMethod]
        [DeploymentItem("C:\\Dev\\ACEx Branches\\17.12\\AppLib\\CE.XMLBillConvert.Test\\BadData\\174_test_bad_premiseId.xml", "BadData")]
        public void TestFileConvertMissingPremiseId()
        {
            FileConvert fc = new FileConvert();
            System.IO.TextReader tr = new System.IO.StreamReader("BadData\\174_test_bad_premiseId.xml");
            try
            {
                List<string> results = fc.Convert(tr, "174"); // arbitrary clientid
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex.Message == "Missing premise ID.");
            }


        }
        //TODO: create a few other tests to validate error handling
    }
}
