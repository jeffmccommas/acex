﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommandLine;
using System.IO;

namespace Aclara.AO.FileGenerator
{
    public class Program
    {
        // Main entry point
        static void Main(string[] args)
        {
            var options = new Options();
            bool parseOkay = false;

            if( !args.Contains("--optionsfile"))
            {
                if (CommandLine.Parser.Default.ParseArgumentsStrict(args, options, () => Environment.Exit(-2)))
                {
                    parseOkay = true;
                }
            }
            else
            {
                foreach (string line in File.ReadLines(args[1]))
                {
                    string[] argsFromFile = line.Split(' ');

                    if (CommandLine.Parser.Default.ParseArgumentsStrict(argsFromFile, options, () => Environment.Exit(-3)))
                    {
                        // only parse first line of the options file                         
                        parseOkay = true;
                        break;
                    }
                }
            }


            // parsed arguments from the command line or arguments from an otions file successfully
            if (parseOkay)
            {
                if (options.Verbose) Console.WriteLine("Mode: {0}", options.Mode);
                if (options.Verbose) Console.WriteLine("ClientId: {0}", options.ClientId);

                Run(options);
            }

        }


        // Mode handling begins here
        private static void Run(Options options)
        {
            var config = new Configuration(options.ClientId);
            string workingFolder = string.Empty;
            var retval = false;
            var mgr = new ProcessManager();

            try
            {

                switch( options.Mode)
                {
                    case "amirequestfile":

                        config.LoadAMIRequestFileModeConfiguration();
                        workingFolder = config.AMIRequestFileDropFolder;
                        if (options.Verbose) Console.WriteLine("Looking for AMI Request File to process at '{0}'.", workingFolder);                        
                        var dirInfo = new DirectoryInfo(workingFolder);

                        foreach (FileInfo file in dirInfo.GetFiles("*AMIRequest*", SearchOption.TopDirectoryOnly).OrderBy(f => f.Name))
                        {
                            var originalFileName = file.FullName;
                            var processingFileName = originalFileName.Replace("AMIRequest", "AMIR") + "-processing";
                            File.Move(originalFileName, processingFileName);
                            retval = mgr.ProcessAMIRequestFile(workingFolder, processingFileName, config, options);
                            File.Move(processingFileName, processingFileName.Replace("-processing","-done"));
                        }

                
                        break;

                    case "tallamifile":

                        config.LoadTallFileModeConfiguration();
                        workingFolder = config.AMITallWorkingFolder;
                        if (options.Verbose) Console.WriteLine("Working folder at '{0}'.", workingFolder);

                        retval = mgr.CreateAMITallFile(workingFolder, config, options);

                        break;

                    case "wideamifile":

                        config.LoadWideFileModeConfiguration();
                        workingFolder = config.AMIWideWorkingFolder;
                        if (options.Verbose) Console.WriteLine("Working folder at '{0}'.", workingFolder);

                        retval = mgr.CreateAMIWideFile(workingFolder, config, options);

                        break;

                    case "wideamifromfile":

                        config.LoadWideFileModeConfiguration();
                        workingFolder = config.AMIWideWorkingFolder;
 
                        if (options.Verbose) Console.WriteLine("Looking for Customer files to process at '{0}'.", workingFolder);
                        var dir = new DirectoryInfo(workingFolder);

                        foreach (FileInfo file in dir.GetFiles(options.WideAmiFromFile, SearchOption.TopDirectoryOnly).OrderBy(f => f.Name))
                        {
                            var originalFileName = file.FullName;
                            var processingFileName = originalFileName;// + "-processing";
                            File.Move(originalFileName, processingFileName);
                            retval = mgr.CreateAMIWideFileFromFile(workingFolder, processingFileName, config, options);
                            //File.Move(processingFileName, processingFileName.Replace("-processing", "-done"));
                        }


                        break;

                    default:
                        throw new Exception("unknown mode");
                }


            }
            catch (Exception e)
            {
                if (options.Verbose)
                {
                    Console.WriteLine("Exception = " + e.StackTrace);
                }
                Environment.Exit(100);
            }


            Environment.Exit(0);
        }


    }
}
