﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aclara.AO.FileGenerator
{
    public interface IConfig
    {
        void LoadAMIRequestFileModeConfiguration();
        void LoadTallFileModeConfiguration();
        void LoadWideFileModeConfiguration();


        string AMIRequestFileDropFolder { get; set; }
        string AMIWideFileDateFormat { get; set; }
        string AMITallWorkingFolder { get; set; }
        string AMIWideWorkingFolder { get; set; }

        string WaterUnitOfMeasure { get; set; }
        string ElectricUnitOfMeasure { get; set; }

        string WaterInterval { get; set; }
        string ElectricInterval { get; set; }

        string GasUnitOfMeasure { get; set; }
        string GasInterval { get; set; }

        string FtpHost { get; set; }
        string FtpPort { get; set; }
        string FtpUsername { get; set; }
        string FtpPassword { get; set; }
        string FtpControlFileDrop { get; set; }
        string FtpFileDrop { get; set; }

    }
}
