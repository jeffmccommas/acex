﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;
using System.Text.RegularExpressions;
using Renci.SshNet;

namespace Aclara.AO.FileGenerator
{
    public class ProcessManager
    {
        const string dm = ",";  //delimeter
        Random rnd = new Random();
        private const string billFileHeader = "customer_id,premise_id,mail_address_line_1,mail_address_line_2,mail_address_line_3,mail_city,mail_state,mail_zip_code,first_name,last_name,phone_1,phone_2,email,customer_type,account_id,active_date,inactive_date,read_cycle,rate_code,service_point_id,site_addressline1,site_addressline2,site_addressline3,site_city,site_state,site_zip_code,meter_type,meter_units,bldg_sq_foot,year_built,bedrooms,assess_value,usage_value,bill_enddate,bill_days,is_estimate,usage_charge,ClientId,service_commodity,account_structure_type,meter_id,billperiod_type,meter_replaces_meterid,service_read_date,Service_contract,Programs";

        /// <summary>
        /// Process an AMI Request File; read request file, create ami file with data for each input meter, and ftp to target location if appropriate
        /// debug with cmd line: -m amirequestfile -c 290 --control --ftp --verbose
        /// </summary>
        /// <param name="actionItemFile"></param>
        /// <param name="statusList"></param>
        /// <returns></returns>
        public bool ProcessAMIRequestFile(string workingFolder, string fullFileName, Configuration config, Options options)
        {
            // AMIRequestFile
            // ClientId,AccountNumber,ServiceId,PremiseId,ServicePoint,MeterId,StartDate,EndDate,BillingPeriodStartDate,FuelId

            // AMI file to create, legacy order of columns, no header
            // ClientId,AccountNumber,MeterId,ServicePointId,CommodityId,UOMId,VolumeFactor,Direction,ProjectedReadDate,TimeStamp,i0, i1, ...
            List<OutputFile> outputFiles = new List<OutputFile>();
            int interval = 0;
            int uom = 0;
            string fullControlFileName = string.Empty;
            string shortControlFileName = string.Empty;
            string envName = string.Empty;
            var todayDatetime = DateTime.Now.ToString("yyyyMdd-Hmmss");

            envName = "000";
            if (fullFileName.Contains("_dev")) envName = "_dev";
            if (fullFileName.Contains("_qa")) envName = "_qa";  // note that the uat environment has no envName

            // read each line of file
            var lineCount = 0;

            foreach (string line in File.ReadLines(fullFileName))
            {
                // assume top line is a header row, and process all lines following that line
                if (lineCount > 0)
                {
                    MatchCollection matches = new Regex("((?<=\")[^\"]*(?=\"(,|$)+)|(?<=,|^)[^,\"]*(?=,|$))").Matches(line);

                    var clientid = matches[0].Value;
                    var accountNumber = matches[1].Value;
                    var serviceId = matches[2].Value;
                    var premiseId = matches[3].Value;
                    var servicePoint = matches[4].Value;
                    var meterId = matches[5].Value;
                    var startDate = matches[6].Value;
                    var endDate = matches[7].Value;
                    var billingPeriodStartDate = matches[8].Value;
                    var fuelId = matches[9].Value;

                    switch (System.Convert.ToInt32(fuelId))
                    {
                        case 1: //electric
                            interval = System.Convert.ToInt32(config.ElectricInterval);
                            uom = System.Convert.ToInt32(config.ElectricUnitOfMeasure);
                            break;

                        case 2: //gas
                            interval = System.Convert.ToInt32(config.GasInterval);
                            uom = System.Convert.ToInt32(config.GasUnitOfMeasure);
                            break;

                        case 3: //water
                            interval = System.Convert.ToInt32(config.WaterInterval);
                            uom = System.Convert.ToInt32(config.WaterUnitOfMeasure);
                            break;
                    }

                    // List of files we must write to
                    if (!outputFiles.Exists(f => f.Commodity == System.Convert.ToInt32(fuelId)))
                    {
                        //clientid_interval_yyyyMdd-Hmmss_001.csv

                        var shortOutputFileName = options.ClientId + "_" + interval + "_" + todayDatetime + "_" + fuelId + "_" + envName + ".csv";
                        var fullOutputFileName = workingFolder + shortOutputFileName;

                        outputFiles.Add(new OutputFile()
                        {
                            Commodity = System.Convert.ToInt32(fuelId),
                            Interval = interval,
                            ShortOutputFilename = shortOutputFileName,
                            FullOutputFilename = fullOutputFileName,
                            Writer = new StreamWriter(fullOutputFileName, false)
                        });

                    }


                    if (clientid == options.ClientId.ToString())
                    {
                        DateTime sd = DateTime.Parse(startDate);
                        DateTime ed = DateTime.Parse(endDate);

                        // output a line per day to file (note that the order is ClientId, AccountNumber, meterID, ...)
                        // ClientId,AccountNumber,MeterId,ServicePointId,CommodityId,UOMId,VolumeFactor,Direction,ProjectedReadDate,TimeStamp,i0, i1, ...
                        foreach (DateTime day in EachDay(sd, ed))
                        {
                            var newLine = new List<string>();
                            newLine.Add(clientid);
                            newLine.Add(accountNumber);
                            newLine.Add(meterId);
                            newLine.Add(servicePoint);
                            newLine.Add(fuelId);
                            newLine.Add(uom.ToString());
                            newLine.Add(string.Empty); //VolumeFactor
                            newLine.Add(string.Empty); //Direction 
                            newLine.Add(string.Empty); //ProjectedEndDate
                            newLine.Add(day.ToString(config.AMIWideFileDateFormat));   //Timestamp (actually date string = 11/22/2015 )

                            switch (interval)
                            {
                                case 60:
                                    for (int i = 0; i < 24; i++)
                                    {
                                        newLine.Add(string.Format("{0:0.##}", GetUsageQuantity(Convert.ToInt32(fuelId), uom, interval, options)));
                                    }
                                    break;

                                case 30:
                                    for (int i = 0; i < 48; i++)
                                    {
                                        newLine.Add(string.Format("{0:0.##}", GetUsageQuantity(Convert.ToInt32(fuelId), uom, interval, options)));
                                    }
                                    break;

                                case 15:
                                    for (int i = 0; i < 96; i++)
                                    {
                                        newLine.Add(string.Format("{0:0.##}", GetUsageQuantity(Convert.ToInt32(fuelId), uom, interval, options)));
                                    }
                                    break;
                            }

                            string csvLine = String.Join(",", newLine);

                            // write the line to the correct output file
                            outputFiles.Find(f => f.Commodity == System.Convert.ToInt32(fuelId)).Writer.WriteLine(csvLine);

                        }

                    }


                }

                lineCount++;
            }

            // close all files when done
            foreach (var file in outputFiles)
            {
                file.Writer.Close();
            }

            if (outputFiles.Count() > 0)
            {
                var randName = RandomString(5);

                // create manifest and zip into a control file if indicated
                if (options.Control)
                {
                    // manifest file
                    var shortManifestFileName = options.ClientId + "_Manifest_" + todayDatetime + "_" + randName + "_" + envName + ".xml";
                    var fullManifestFileName = workingFolder + shortManifestFileName;

                    using (StreamWriter writeManifest = new StreamWriter(fullManifestFileName))
                    {
                        writeManifest.WriteLine("<?xml version=\"1.0\" encoding=\"UTF - 8\"?>");
                        writeManifest.WriteLine("<DataIntegrationManifest>");
                        writeManifest.WriteLine("	<Import>");
                        writeManifest.WriteLine("		<AMIFiles>");

                        foreach (var file in outputFiles)
                        {
                            writeManifest.WriteLine("           <AMIFile Interval=\"{0}Minute\">{1}</AMIFile>", file.Interval, file.ShortOutputFilename);
                        }

                        writeManifest.WriteLine("       </AMIFiles>");
                        writeManifest.WriteLine("	</Import>");
                        writeManifest.WriteLine("</DataIntegrationManifest>");
                    }

                    // control zip file with ami and manifest included
                    shortControlFileName = options.ClientId + "_Control_" + todayDatetime + "_" + randName + "_" + envName + ".zip";
                    fullControlFileName = workingFolder + shortControlFileName;

                    using (FileStream zipToOpen = new FileStream(fullControlFileName, FileMode.Create))
                    {
                        using (ZipArchive archive = new ZipArchive(zipToOpen, ZipArchiveMode.Update))
                        {
                            archive.CreateEntryFromFile(fullManifestFileName, shortManifestFileName);

                            foreach (var file in outputFiles)
                            {
                                archive.CreateEntryFromFile(file.FullOutputFilename, file.ShortOutputFilename);
                            }
                        }
                    }

                    // delete the files that were added to the archive
                    File.Delete(fullManifestFileName);

                    foreach (var file in outputFiles)
                    {
                        File.Delete(file.FullOutputFilename);
                    }

                }


                // ftp to moveit dmz
                if (options.Ftp)
                {
                    var uploadfile = fullControlFileName;
                    int port = 22;
                    string host = config.FtpHost;
                    string username = config.FtpUsername;
                    string password = config.FtpPassword;
                    string workingdirectory = config.FtpControlFileDrop;

                    using (var client = new SftpClient(host, port, username, password))
                    {
                        client.Connect();
                        if (options.Verbose) Console.WriteLine("FTP - Connected to {0}", host);

                        client.ChangeDirectory(workingdirectory);
                        if (options.Verbose) Console.WriteLine("FTP - Changed directory to {0}", workingdirectory);

                        using (var fileStream = new FileStream(uploadfile, FileMode.Open))
                        {
                            if (options.Verbose) Console.WriteLine("FTP - Uploading {0} ({1:N0} bytes)", uploadfile, fileStream.Length);
                            client.BufferSize = 4 * 1024; // bypass Payload error large files
                            client.UploadFile(fileStream, Path.GetFileName(uploadfile));
                        }

                        if (options.Verbose) Console.WriteLine("FTP - Transfer complete.");
                    }

                }

            }


            return (true);
        }


        /// <summary>
        /// Create tall AMI file
        /// debug with cmd line:  -m tallamifile -c 61 --tallenv dev --verbose --tallusageperinterval 3 --talloffsethours 4 --tallbackhours 6 --tallmetercount 10 --tallprefixname test
        /// </summary>
        /// <param name="workingFolder"></param>
        /// <param name="config"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public bool CreateAMITallFile(string workingFolder, Configuration config, Options options)
        {
            //TallFile - needs a header
            //METER_ID,TRANSPONDER_ID,TRANSPONDER_PORT,CUSTOMER_ID,READING_VALUE,UNIT_OF_MEASURE,READING_DATETIME,TIMEZONE,BATTERY_VOLTAGE,COMMODITY_ID
            DateTime rootDate = DateTime.Parse("1/1/2010");

            var todayDatetime = DateTime.Now.ToString("yyyyMdd-Hmmss");
            var shortOutputFileName = options.ClientId + "_ncc_" + todayDatetime + "_" + options.TallEnvironment + ".csv";
            var fullOutputFileName = workingFolder + shortOutputFileName;

            int usagePerInterval = 1;
            if (options.TallUsagePerInterval > 0)
            {
                usagePerInterval = options.TallUsagePerInterval;
            }

            int backHours = 4;
            if (options.TallBackHours > 0)
            {
                backHours = options.TallBackHours;
            }

            int offsetHours = 4;
            if (options.TallOffsetHours >= 0)
            {
                offsetHours = options.TallOffsetHours;
            }

            int meterCount = 1;
            if (options.TallMeterCount > 0)
            {
                meterCount = options.TallMeterCount;
            }

            var tw = new StreamWriter(fullOutputFileName, false);

            // header 
            var headerLine = new List<string>();
            headerLine.Add("METER_ID");
            headerLine.Add("TRANSPONDER_ID");
            headerLine.Add("TRANSPONDER_PORT");
            headerLine.Add("CUSTOMER_ID");
            headerLine.Add("READING_VALUE");
            headerLine.Add("UNIT_OF_MEASURE");
            headerLine.Add("READING_DATETIME");
            headerLine.Add("TIMEZONE");
            headerLine.Add("BATTERY_VOLTAGE");
            headerLine.Add("COMMODITY_ID");
            string headerCsv = String.Join(",", headerLine);
            tw.WriteLine(headerCsv);

            // it is water now, but make this configurable
            var interval = System.Convert.ToInt32(config.WaterInterval);
            var uom = config.WaterUnitOfMeasure;

            // Meter loop
            for (int i = 0; i < meterCount; i++)
            {
                // hours of reads per meter
                DateTime sd = DateTime.Now.AddHours(-(offsetHours + backHours));

                for (int h = 0; h < backHours; h++)
                {
                    var currentDate = sd.AddHours(h);
                    TimeSpan span = currentDate.Subtract(rootDate);
                    var hoursSpanned = (int)span.TotalHours;
                    var read = hoursSpanned + (h * usagePerInterval);

                    var newLine = new List<string>();
                    newLine.Add(options.TallPrefixName + "M" + i);
                    newLine.Add((i + 10).ToString());
                    newLine.Add("1");
                    newLine.Add(options.TallPrefixName + "C" + i);
                    newLine.Add(read.ToString());
                    newLine.Add(uom);
                    newLine.Add(currentDate.ToString("M-dd-yyyy H:00"));
                    newLine.Add("GMT");
                    newLine.Add("3.58997");
                    newLine.Add("3");       // 3 = water, 2 = gas, 1 = electric

                    string csvLine = String.Join(",", newLine);

                    tw.WriteLine(csvLine);

                }
            }

            tw.Close();


            // ftp to moveit dmz
            if (options.Ftp)
            {
                var uploadfile = fullOutputFileName;
                int port = 22;
                string host = config.FtpHost;
                string username = config.FtpUsername;
                string password = config.FtpPassword;
                string workingdirectory = config.FtpFileDrop;

                using (var client = new SftpClient(host, port, username, password))
                {
                    client.Connect();
                    if (options.Verbose) Console.WriteLine("FTP - Connected to {0}", host);

                    client.ChangeDirectory(workingdirectory);
                    if (options.Verbose) Console.WriteLine("FTP - Changed directory to {0}", workingdirectory);

                    using (var fileStream = new FileStream(uploadfile, FileMode.Open))
                    {
                        if (options.Verbose) Console.WriteLine("FTP - Uploading {0} ({1:N0} bytes)", uploadfile, fileStream.Length);
                        client.BufferSize = 4 * 1024; // bypass Payload error large files
                        client.UploadFile(fileStream, Path.GetFileName(uploadfile));
                    }

                    if (options.Verbose) Console.WriteLine("FTP - Transfer complete.");
                }

            }


            return (true);
        }


        /// <summary>
        /// Create Wide AMI File.
        /// denug with: 
        /// </summary> -m wideamifile -c 87 -i 60 -u 0 --widecommodity 1 --wideenv dev --verbose --wideoffsetdays 2 --widebackdays 50 --widemetercount 20 --wideprefixname testx
        /// <param name="workingFolder"></param>
        /// <param name="config"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public bool CreateAMIWideFile(string workingFolder, Configuration config, Options options)
        {
            // AMI file to create, header file included ami_60min, etc in name
            const string header24 = "ClientId,MeterId,AccountNumber,ServicePointId,CommodityId,UOMId,VolumeFactor,Direction,ProjectedReadDate,TimeStamp,IntValue0000,IntValue0100,IntValue0200,IntValue0300,IntValue0400,IntValue0500,IntValue0600,IntValue0700,IntValue0800,IntValue0900,IntValue1000,IntValue1100,IntValue1200,IntValue1300,IntValue1400,IntValue1500,IntValue1600,IntValue1700,IntValue1800,IntValue1900,IntValue2000,IntValue2100,IntValue2200,IntValue2300,Timezone";
            const string header48 = "ClientId,MeterId,AccountNumber,ServicePointId,CommodityId,UOMId,VolumeFactor,Direction,ProjectedReadDate,TimeStamp,IntValue0000,IntValue0030,IntValue0100,IntValue0130,IntValue0200,IntValue0230,IntValue0300,IntValue0330,IntValue0400,IntValue0430,IntValue0500,IntValue0530,IntValue0600,IntValue0630,IntValue0700,IntValue0730,IntValue0800,IntValue0830,IntValue0900,IntValue0930,IntValue1000,IntValue1030,IntValue1100,IntValue1130,IntValue1200,IntValue1230,IntValue1300,IntValue1330,IntValue1400,IntValue1430,IntValue1500,IntValue1530,IntValue1600,IntValue1630,IntValue1700,IntValue1730,IntValue1800,IntValue1830,IntValue1900,IntValue1930,IntValue2000,IntValue2030,IntValue2100,IntValue2130,IntValue2200,IntValue2230,IntValue2300,IntValue2330,Timezone";
            const string header96 = "ClientId,MeterId,AccountNumber,ServicePointId,CommodityId,UOMId,VolumeFactor,Direction,ProjectedReadDate,TimeStamp,IntValue0000,IntValue0015,IntValue0030,IntValue0045,IntValue0100,IntValue0115,IntValue0130,IntValue0145,IntValue0200,IntValue0215,IntValue0230,IntValue0245,IntValue0300,IntValue0315,IntValue0330,IntValue0345,IntValue0400,IntValue0415,IntValue0430,IntValue0445,IntValue0500,IntValue0515,IntValue0530,IntValue0545,IntValue0600,IntValue0615,IntValue0630,IntValue0645,IntValue0700,IntValue0715,IntValue0730,IntValue0745,IntValue0800,IntValue0815,IntValue0830,IntValue0845,IntValue0900,IntValue0915,IntValue0930,IntValue0945,IntValue1000,IntValue1015,IntValue1030,IntValue1045,IntValue1100,IntValue1115,IntValue1130,IntValue1145,IntValue1200,IntValue1215,IntValue1230,IntValue1245,IntValue1300,IntValue1315,IntValue1330,IntValue1345,IntValue1400,IntValue1415,IntValue1430,IntValue1445,IntValue1500,IntValue1515,IntValue1530,IntValue1545,IntValue1600,IntValue1615,IntValue1630,IntValue1645,IntValue1700,IntValue1715,IntValue1730,IntValue1745,IntValue1800,IntValue1815,IntValue1830,IntValue1845,IntValue1900,IntValue1915,IntValue1930,IntValue1945,IntValue2000,IntValue2015,IntValue2030,IntValue2045,IntValue2100,IntValue2115,IntValue2130,IntValue2145,IntValue2200,IntValue2215,IntValue2230,IntValue2245,IntValue2300,IntValue2315,IntValue2330,IntValue2345,Timezone";

            var todayDatetime = DateTime.Now.ToString("yyyyMdd-Hmmss");
            var shortOutputFileName = options.ClientId + "_ami_" + GetIntervalStringForFilename(options.Interval) + "_" + todayDatetime + "_" + options.WideEnvironment + ".csv";
            var fullOutputFileName = workingFolder + shortOutputFileName;
            var shortBillOutputFilename = options.ClientId + "_bill_" + GetIntervalStringForFilename(options.Interval) + "_" + todayDatetime + "_" + options.WideEnvironment + ".csv";
            var fullBillOutputFileName = workingFolder + shortBillOutputFilename;

            int meterCount = 1;
            if (options.WideMeterCount > 0)
            {
                meterCount = options.WideMeterCount;
            }

            int backDays = 5;
            if (options.WideBackDays > 0)
            {
                backDays = options.WideBackDays;
            }

            int offsetDays = 5;
            if (options.WideOffsetDays > 0)
            {
                offsetDays = options.WideOffsetDays;
            }

            var tw = new StreamWriter(fullOutputFileName, false);
            if (options.Interval == 60) tw.WriteLine(header24);
            if (options.Interval == 30) tw.WriteLine(header48);
            if (options.Interval == 15) tw.WriteLine(header96);

            StreamWriter billTextWriter = null;
            if (options.WideCreateBillFile)
            {
                billTextWriter = new StreamWriter(fullBillOutputFileName, false);
                billTextWriter.WriteLine(billFileHeader);
            }

            // Meter loop
            for (int i = 0; i < meterCount; i++)
            {

                DateTime now = DateTime.Now;
                DateTime sd = now.AddDays(-(options.WideOffsetDays + options.WideBackDays));
                DateTime ed = now.AddDays(-options.WideOffsetDays);

                string meterId = options.WidePrefixName + "M" + i;
                string accountNumber = options.WidePrefixName + "A" + i;
                string servicePointId = options.WidePrefixName + "SP" + i;


                // output a line per day to file
                // ClientId,MeterId,AccountNumber,ServicePointId,CommodityId,UOMId,VolumeFactor,Direction,ProjectedReadDate,TimeStamp,IntValue0000,IntValue0100,IntValue0200,IntValue0300,IntValue0400,IntValue0500,IntValue0600,IntValue0700,IntValue0800,IntValue0900,IntValue1000,IntValue1100,IntValue1200,IntValue1300,IntValue1400,IntValue1500,IntValue1600,IntValue1700,IntValue1800,IntValue1900,IntValue2000,IntValue2100,IntValue2200,IntValue2300,IntValue2400
                foreach (DateTime day in EachDay(sd, ed))
                {
                    var newLine = new List<string>();
                    newLine.Add(options.ClientId.ToString());
                    newLine.Add(meterId);
                    newLine.Add(accountNumber);
                    newLine.Add(servicePointId);
                    newLine.Add(options.WideCommodity.ToString());
                    newLine.Add(options.Uom.ToString());
                    newLine.Add(string.Empty); //VolumeFactor
                    newLine.Add(string.Empty); //Direction 
                    newLine.Add(string.Empty); //ProjectedEndDate
                    newLine.Add(day.ToString(config.AMIWideFileDateFormat));   //Timestamp (actually date string = 11/22/2015 )

                    switch (options.Interval)
                    {
                        case 60:
                            for (int ii = 0; ii < 24; ii++)
                            {
                                newLine.Add(string.Format("{0:0.###}", GetUsageQuantity(Convert.ToInt32(options.WideCommodity), options.Uom, options.Interval, options)));
                            }
                            break;

                        case 30:
                            for (int ii = 0; ii < 48; ii++)
                            {
                                newLine.Add(string.Format("{0:0.###}", GetUsageQuantity(Convert.ToInt32(options.WideCommodity), options.Uom, options.Interval, options)));
                            }
                            break;

                        case 15:
                            for (int ii = 0; ii < 96; ii++)
                            {
                                newLine.Add(string.Format("{0:0.###}", GetUsageQuantity(Convert.ToInt32(options.WideCommodity), options.Uom, options.Interval, options)));
                            }
                            break;
                    }

                    string csvLine = String.Join(",", newLine);

                    tw.WriteLine(csvLine);
                }


                if (options.WideCreateBillFile)
                {
                    // WRITE a bill row to the billing output file (I broke this into lines of 5 in comments for easier reading.)
                    // ** customer_id, premise_id, mail_address_line_1, mail_address_line_2, mail_address_line_3,
                    // ** mail_city, mail_state, mail_zip_code, first_name, last_name,
                    // ** phone_1, phone_2, email, customer_type, account_id,
                    // ** active_date, inactive_date, read_cycle, rate_code, service_point_id, 

                    string customerId = options.WidePrefixName + "C" + i;
                    string premiseId = options.WidePrefixName + "P" + i;
                    string serviceContractId = options.WidePrefixName + "SC" + i;

                    var billNewLine = new List<string>();

                    billNewLine.Add(customerId);
                    billNewLine.Add(premiseId);
                    billNewLine.Add(i + " Test St.");
                    billNewLine.Add("L2");
                    billNewLine.Add(string.Empty); //mail_address_line_3

                    billNewLine.Add("Compton");
                    billNewLine.Add("CA");
                    billNewLine.Add("90221");
                    billNewLine.Add("Mike" + i);
                    billNewLine.Add("Smith" + i);   //last_name

                    billNewLine.Add("617-555-1212");
                    billNewLine.Add(string.Empty);
                    billNewLine.Add("mganley@aclara.com");
                    billNewLine.Add("Residential");
                    billNewLine.Add(accountNumber); //account_id

                    billNewLine.Add(string.Empty);
                    billNewLine.Add(string.Empty);
                    billNewLine.Add("Cycle02");
                    billNewLine.Add("binTierM");
                    billNewLine.Add(servicePointId); //service_point_id

                    // ** site_addressline1, site_addressline2, site_addressline3, site_city, site_state,
                    // ** site_zip_code, meter_type, meter_units, bldg_sq_foot, year_built, bedrooms,
                    // ** assess_value, usage_value, bill_enddate, bill_days, is_estimate,
                    // ** usage_charge, ClientId, service_commodity, account_structure_type, meter_id,
                    // ** billperiod_type, meter_replaces_meterid, service_read_date, Service_contract, Programs

                    billNewLine.Add("100 Test St");
                    billNewLine.Add("Addr2");
                    billNewLine.Add(string.Empty);
                    billNewLine.Add("Compton");
                    billNewLine.Add("CA"); //site_state

                    billNewLine.Add("90221");
                    billNewLine.Add("ami");
                    billNewLine.Add(options.Uom.ToString()); //meter_units
                    billNewLine.Add(string.Empty);
                    billNewLine.Add(string.Empty);
                    billNewLine.Add(string.Empty); //bedrooms

                    billNewLine.Add(string.Empty);
                    billNewLine.Add("550");         //usage_value
                    billNewLine.Add("2016-04-30");  //bill_enddate
                    billNewLine.Add("30");
                    billNewLine.Add(string.Empty); //is_estimate

                    billNewLine.Add("99.4"); //usage_charge
                    billNewLine.Add(options.ClientId.ToString());
                    billNewLine.Add(options.WideCommodity.ToString());
                    billNewLine.Add(string.Empty);
                    billNewLine.Add(meterId); //meter_id

                    billNewLine.Add("1");   //billperiod_type
                    billNewLine.Add(string.Empty);
                    billNewLine.Add("2016-04-30T00:00:00"); //service_read_date
                    billNewLine.Add(serviceContractId);
                    billNewLine.Add(string.Empty); //Programs

                    string billCsvLine = String.Join(",", billNewLine);

                    billTextWriter.WriteLine(billCsvLine);

                }

            }

            tw.Close();


            if (options.WideCreateBillFile)
            {
                billTextWriter.Close();
            }




            // ftp to moveit dmz
            if (options.Ftp)
            {
                var uploadfile = fullOutputFileName;
                int port = 22;
                string host = config.FtpHost;
                string username = config.FtpUsername;
                string password = config.FtpPassword;
                string workingdirectory = config.FtpFileDrop;

                using (var client = new SftpClient(host, port, username, password))
                {
                    client.Connect();
                    if (options.Verbose) Console.WriteLine("FTP - Connected to {0}", host);

                    client.ChangeDirectory(workingdirectory);
                    if (options.Verbose) Console.WriteLine("FTP - Changed directory to {0}", workingdirectory);

                    using (var fileStream = new FileStream(uploadfile, FileMode.Open))
                    {
                        if (options.Verbose) Console.WriteLine("FTP - Uploading {0} ({1:N0} bytes)", uploadfile, fileStream.Length);
                        client.BufferSize = 4 * 1024; // bypass Payload error large files
                        client.UploadFile(fileStream, Path.GetFileName(uploadfile));
                    }

                    if (options.Verbose) Console.WriteLine("FTP - Transfer complete.");
                }

            }

            return (true);
        }

        /// <summary>
        /// Create Wide AMI File From File
        /// </summary>
        /// <param name="workingFolder"></param>
        /// <param name="config"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public bool CreateAMIWideFileFromFile(string workingFolder, string fullFileName, Configuration config, Options options)
        {
            //ClientId,CustomerID,AccountID,MeterID,ServicePoint,Commodity,amiuom,interval
            //87,993774751,1218337093,189354704,1,0,15
            //87,9000EG1,9000EG1,M9000EG_elec,1,0,15

            // AMI file to create, header file included ami_60min, etc in name
            const string header24 = "ClientId,MeterId,AccountNumber,ServicePointId,CommodityId,UOMId,VolumeFactor,Direction,ProjectedReadDate,TimeStamp,IntValue0000,IntValue0100,IntValue0200,IntValue0300,IntValue0400,IntValue0500,IntValue0600,IntValue0700,IntValue0800,IntValue0900,IntValue1000,IntValue1100,IntValue1200,IntValue1300,IntValue1400,IntValue1500,IntValue1600,IntValue1700,IntValue1800,IntValue1900,IntValue2000,IntValue2100,IntValue2200,IntValue2300,Timezone";
            const string header48 = "ClientId,MeterId,AccountNumber,ServicePointId,CommodityId,UOMId,VolumeFactor,Direction,ProjectedReadDate,TimeStamp,IntValue0000,IntValue0030,IntValue0100,IntValue0130,IntValue0200,IntValue0230,IntValue0300,IntValue0330,IntValue0400,IntValue0430,IntValue0500,IntValue0530,IntValue0600,IntValue0630,IntValue0700,IntValue0730,IntValue0800,IntValue0830,IntValue0900,IntValue0930,IntValue1000,IntValue1030,IntValue1100,IntValue1130,IntValue1200,IntValue1230,IntValue1300,IntValue1330,IntValue1400,IntValue1430,IntValue1500,IntValue1530,IntValue1600,IntValue1630,IntValue1700,IntValue1730,IntValue1800,IntValue1830,IntValue1900,IntValue1930,IntValue2000,IntValue2030,IntValue2100,IntValue2130,IntValue2200,IntValue2230,IntValue2300,IntValue2330,Timezone";
            const string header96 = "ClientId,MeterId,AccountNumber,ServicePointId,CommodityId,UOMId,VolumeFactor,Direction,ProjectedReadDate,TimeStamp,IntValue0000,IntValue0015,IntValue0030,IntValue0045,IntValue0100,IntValue0115,IntValue0130,IntValue0145,IntValue0200,IntValue0215,IntValue0230,IntValue0245,IntValue0300,IntValue0315,IntValue0330,IntValue0345,IntValue0400,IntValue0415,IntValue0430,IntValue0445,IntValue0500,IntValue0515,IntValue0530,IntValue0545,IntValue0600,IntValue0615,IntValue0630,IntValue0645,IntValue0700,IntValue0715,IntValue0730,IntValue0745,IntValue0800,IntValue0815,IntValue0830,IntValue0845,IntValue0900,IntValue0915,IntValue0930,IntValue0945,IntValue1000,IntValue1015,IntValue1030,IntValue1045,IntValue1100,IntValue1115,IntValue1130,IntValue1145,IntValue1200,IntValue1215,IntValue1230,IntValue1245,IntValue1300,IntValue1315,IntValue1330,IntValue1345,IntValue1400,IntValue1415,IntValue1430,IntValue1445,IntValue1500,IntValue1515,IntValue1530,IntValue1545,IntValue1600,IntValue1615,IntValue1630,IntValue1645,IntValue1700,IntValue1715,IntValue1730,IntValue1745,IntValue1800,IntValue1815,IntValue1830,IntValue1845,IntValue1900,IntValue1915,IntValue1930,IntValue1945,IntValue2000,IntValue2015,IntValue2030,IntValue2045,IntValue2100,IntValue2115,IntValue2130,IntValue2145,IntValue2200,IntValue2215,IntValue2230,IntValue2245,IntValue2300,IntValue2315,IntValue2330,IntValue2345,Timezone";

            var todayDatetime = string.Empty;
            var shortOutputFileName = string.Empty;
            var fullOutputFileName = string.Empty;

            int meterCount = 1;
            if (options.WideMeterCount > 0)
            {
                meterCount = options.WideMeterCount;
            }

            int backDays = 5;
            if (options.WideBackDays > 0)
            {
                backDays = options.WideBackDays;
            }

            int offsetDays = 5;
            if (options.WideOffsetDays > 0)
            {
                offsetDays = options.WideOffsetDays;
            }

            DateTime now = DateTime.Now;
            DateTime sd = now.AddDays(-(options.WideOffsetDays + options.WideBackDays));
            DateTime ed = now.AddDays(-options.WideOffsetDays);

            todayDatetime = DateTime.Now.ToString("yyyyMdd-Hmmss");
            shortOutputFileName = options.ClientId + "_ami_" + GetIntervalStringForFilename(options.Interval) + "_" + todayDatetime + ".csv";
            fullOutputFileName = workingFolder + shortOutputFileName;

            var tw = new StreamWriter(fullOutputFileName, false);
            if (options.Interval == 60) tw.WriteLine(header24);
            if (options.Interval == 30) tw.WriteLine(header48);
            if (options.Interval == 15) tw.WriteLine(header96);


            // read each line of file
            var lineCount = 0;

            foreach (string line in File.ReadLines(fullFileName))
            {
                //ClientId,CustomerID,AccountID,MeterID,ServicePoint,Commodity,amiuom,interval
                //87,993774751,1218337093,189354704,1,0,15
                //87,9000EG1,9000EG1,M9000EG_elec,1,0,15

                // assume top line is a header row, and process all lines following that line
                if (lineCount > 0)
                {
                    MatchCollection matches = new Regex("((?<=\")[^\"]*(?=\"(,|$)+)|(?<=,|^)[^,\"]*(?=,|$))").Matches(line);

                    var clientid = matches[0].Value;
                    var customerId = matches[1].Value;
                    var accountNumber = matches[2].Value;
                    var meterId = matches[3].Value;
                    var servicePoint = matches[4].Value;
                    var commodityId = matches[5].Value;
                    var uom = matches[6].Value;
                    var interval = matches[7].Value;

                    foreach (DateTime day in EachDay(sd, ed))
                    {
                        var newLine = new List<string>();
                        newLine.Add(clientid);
                        newLine.Add(meterId);
                        newLine.Add(accountNumber);
                        newLine.Add(servicePoint);
                        newLine.Add(commodityId);
                        newLine.Add(uom.ToString());
                        newLine.Add(string.Empty); //VolumeFactor
                        newLine.Add(string.Empty); //Direction 
                        newLine.Add(string.Empty); //ProjectedEndDate
                        newLine.Add(day.ToString(config.AMIWideFileDateFormat));   //Timestamp (actually date string = 11/22/2015 )

                        switch (Convert.ToInt32(interval))
                        {
                            case 60:
                                for (int i = 0; i < 24; i++)
                                {
                                    newLine.Add(string.Format("{0:0.##}", GetUsageQuantity(Convert.ToInt32(commodityId), Convert.ToInt32(uom), Convert.ToInt32(interval), options)));
                                }
                                break;

                            case 30:
                                for (int i = 0; i < 48; i++)
                                {
                                    newLine.Add(string.Format("{0:0.##}", GetUsageQuantity(Convert.ToInt32(commodityId), Convert.ToInt32(uom), Convert.ToInt32(interval), options)));
                                }
                                break;

                            case 15:
                                for (int i = 0; i < 96; i++)
                                {
                                    newLine.Add(string.Format("{0:0.##}", GetUsageQuantity(Convert.ToInt32(commodityId), Convert.ToInt32(uom), Convert.ToInt32(interval), options)));
                                }
                                break;
                        }

                        string csvLine = String.Join(",", newLine);

                        // write the line to the correct output file
                        tw.WriteLine(csvLine);

                    }


                }

                lineCount++;
            }


            tw.Close();


            return (true);
        }


        /// <summary>
        /// Helper.
        /// </summary>
        /// <param name="interval"></param>
        /// <returns></returns>
        private string GetIntervalStringForFilename(int interval)
        {
            var retval = string.Empty;

            switch (interval)
            {
                case 60:
                    retval = "60min";
                    break;

                case 30:
                    retval = "30min";
                    break;

                case 15:
                    retval = "15min";
                    break;
            }

            return (retval);
        }

        /// <summary>
        /// Create a usage quantity.
        /// </summary>
        /// <param name="commodityId"></param>
        /// <param name="uom"></param>
        /// <param name="interval"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        private double GetUsageQuantity(int commodityId, int uom, int interval, Options options)
        {
            double value = 0.0;

            switch (commodityId)
            {
                case 1: // electric

                    if (interval == 15)
                    {
                        value = 0.42;
                        if (options.RandUsage) value = value + rnd.NextDouble();
                    }
                    else if (interval == 30)
                    {
                        value = 0.625;
                        if (options.RandUsage) value = value + rnd.NextDouble();
                    }
                    else if (interval == 60)
                    {
                        value = 1.25;
                        if (options.RandUsage) value = value + rnd.NextDouble();
                    }

                    break;

                case 2: // gas;   Therms = 1, CCF = 2 (treat them the same since they are so close right now)

                    if (interval == 15)
                    {
                        value = 0.09;
                        if (options.RandUsage) value = value + rnd.NextDouble() / 8.0;
                    }
                    else if (interval == 30)
                    {
                        value = 0.14;
                        if (options.RandUsage) value = value + rnd.NextDouble() / 8.0;
                    }
                    else if (interval == 60)
                    {
                        value = 0.28;
                        if (options.RandUsage) value = value + rnd.NextDouble() / 8.0;
                    }


                    break;


                case 3: // water (gallons, convert at end)

                    if (interval == 15)
                    {
                        value = 2.125;
                        if (options.RandUsage) value = value + rnd.NextDouble() / 7.0;
                    }
                    else if (interval == 30)
                    {
                        value = 4.25;
                        if (options.RandUsage) value = value + rnd.NextDouble() / 5.0;
                    }
                    else if (interval == 60)
                    {
                        value = 8.5;
                        if (options.RandUsage) value = value + rnd.NextDouble();
                    }

                    //CF = 3
                    //cgal = 4
                    //hgal = 5
                    //gal = 6
                    if (uom == 3) value = value * 0.133681;
                    if (uom == 4 || uom == 5) value = value * 0.00133681;

                    break;
            }

            return (value);
        }

        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        /// <summary>
        /// Helper for looping days.
        /// </summary>
        /// <param name="from"></param>
        /// <param name="thru"></param>
        /// <returns></returns>
        public IEnumerable<DateTime> EachDay(DateTime from, DateTime thru)
        {
            for (var day = from.Date; day.Date <= thru.Date; day = day.AddDays(1))
                yield return day;
        }


    }


    public class OutputFile
    {
        public int Commodity { get; set; }
        public int Interval { get; set; }
        public string ShortOutputFilename { get; set; }
        public string FullOutputFilename { get; set; }
        public StreamWriter Writer { get; set; }
    }

}
