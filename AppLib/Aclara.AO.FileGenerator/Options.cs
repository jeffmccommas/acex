﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommandLine;
using CommandLine.Text;

namespace Aclara.AO.FileGenerator
{
    public class Options
    {

        // mode
        [Option('m', "mode", Required = true, HelpText = "The mode: amirequestfile, tallamifile, wideamifile")]
        public string Mode { get; set; }

        // clientid
        [Option('c', "clientid", Required = true, HelpText = "The clientid for use in some modes.")]
        public int ClientId { get; set; }

        // interval
        [Option('i', "interval", Required = false, HelpText = "The interval: 60, 30, 15.")]
        public int Interval { get; set; }

        // uom
        [Option('u', "uom", Required = false, HelpText = "The unit of measure: 0 (kwh), 1 (therms), 2 (ccf), 3 (cf), 4 (cgal), 5 (hgal), 6 (gal), 7 (kgal).")]
        public int Uom { get; set; }

        // verbose
        [Option('v', "verbose", Required = false, HelpText = "Print messages to standard output.")]
        public bool Verbose { get; set; }

        // randusage
        [Option("randusage", Required = false, HelpText = "Generate random usage quantities that are generally accurate for the commodity, interval, and uom.")]
        public bool RandUsage { get; set; }

        // control
        [Option("control", Required = false, HelpText = "When mode=amirequestfile, this indicates if control zip file with a manifest file is created.")]
        public bool Control { get; set; }

        // ftp
        [Option("ftp", Required = false, HelpText = "When mode=amirequestfile, the control zip file is copied to ControlFileDrop specified in app.config; when mode=tallamifile | wideamifile, the file is copied to FileDrop specified in app.config.")]
        public bool Ftp { get; set; }


        // tallenv
        [Option("tallenv", Required = false, HelpText = "When mode=tallamifile then use: dev, qa; do not specify anuthing for uat or prod.")]
        public string TallEnvironment { get; set; }

        // tallusageperinterval
        [Option("tallusageperinterval", Required = false, HelpText = "When mode=tallamifile, usage per interval.")]
        public int TallUsagePerInterval { get; set; }

        // talloffsethours
        [Option("talloffsethours", Required = false, HelpText = "When mode=tallamifile, hours of ami to offset in past before applying back hours.")]
        public int TallOffsetHours { get; set; }

        // tallbackhours
        [Option("tallbackhours", Required = false, HelpText = "When mode=tallamifile, hours of ami to provide in past.")]
        public int TallBackHours { get; set; }

        // tallmetercount
        [Option("tallmetercount", Required = false, HelpText = "When mode=tallamifile, file created will have 1 to tallmetercount meters of data.")]
        public int TallMeterCount { get; set; }

        // tallprefixname
        [Option("tallprefixname", Required = false, HelpText = "When mode=tallamifile, prefix meterid, and all ids with this string.")]
        public string TallPrefixName { get; set; }


        // widebackdays
        [Option("widebackdays", Required = false, HelpText = "When mode=wideamifile, days of ami to provide in past.")]
        public int WideBackDays { get; set; }

        // wideoffsetdays
        [Option("wideoffsetdays", Required = false, HelpText = "When mode=wideamifile, days of ami to offset in past before applying back days.")]
        public int WideOffsetDays { get; set; }

        // widemetercount
        [Option("widemetercount", Required = false, HelpText = "When mode=wideamifile, file created will have 1 to widemetercount meters of data.")]
        public int WideMeterCount { get; set; }

        // wideenv
        [Option("wideenv", Required = false, HelpText = "When mode=wideamifile then use: dev, qa; do not specify anuthing for uat or prod.")]
        public string WideEnvironment { get; set; }

        // widecommodity
        [Option("widecommodity", Required = false, HelpText = "When mode=wideamifile: 1=electric, 2=gas, 3=water.")]
        public int WideCommodity { get; set; }

        // wideprefixname
        [Option("wideprefixname", Required = false, HelpText = "When mode=wideamifile, prefix meterid, and all ids with this string.")]
        public string WidePrefixName { get; set; }

        // widecreatebillfile
        [Option("widecreatebillfile", Required = false, HelpText = "A bill file will be created with a bill per meter.  BillCycle and RateClass hard-coded for now.")]
        public bool WideCreateBillFile { get; set; }

        // wideamifromfile
        [Option("wideamifromfile", Required = false, HelpText = "When mode=wideamfilefromfile, use this file as source of identifiers and other information.")]
        public string WideAmiFromFile { get; set; }


        // widefilefromfile
        [Option("wideamifromfile", Required = false, HelpText = "When mode=wideamfilefromfile, use this file as source of identifiers and other information.")]
        public string WideAmiFromFile { get; set; }


        // optionsfile
        [Option("optionsfile", Required = false, HelpText = "Load options from the specified options file.")]
        public string OptionsFile { get; set; }


        [ParserState]
        public IParserState LastParserState { get; set; }

        [HelpOption]
        public string GetUsage()
        {
            return HelpText.AutoBuild(this,
              (HelpText current) => HelpText.DefaultParsingErrorsHandler(this, current));
        }

    }

}
