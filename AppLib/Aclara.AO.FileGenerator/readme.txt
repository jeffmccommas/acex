﻿******************
__AMI Request file__ header row and sample data.  This is inbound example data that this console app should be receiving:

ClientId,AccountNumber,ServiceId,PremiseId,ServicePoint,MeterId,StartDate,EndDate,BillingPeriodStartDate,FuelId
5300,1000011071,1000011000,1000011000,1000011000,4837030,12/24/2015,12/28/2015,12/03/2015,2
5300,100003493,0100003400,0100003400,0100003400,10025662,12/24/2015,12/28/2015,12/17/2015,2
5300,1000063385,1000063300,1000063300,1000063300,10496244,12/24/2015,12/28/2015,12/03/2015,2
5300,1000068210,1000068200,1000068200,1000068200,13668255,12/24/2015,12/28/2015,12/03/2015,2
5300,1000173426,1000173400,1000173400,1000173400,3772417,12/24/2015,12/28/2015,12/23/2015,2
5300,1000175100,1000175100,1000175100,1000175100,13659743,12/24/2015,12/28/2015,12/03/2015,2
5300,1000179807,1000179800,1000179800,1000179800,10945897,12/24/2015,12/28/2015,12/03/2015,2
5300,1000197800,1000197800,1000197800,1000197800,13842445,12/24/2015,12/28/2015,12/03/2015,2
5300,1000199602,1000199600,1000199600,1000199600,11627510,12/24/2015,12/28/2015,12/03/2015,2
5300,1000268027,1000268000,1000268000,1000268000,12095106,12/24/2015,12/28/2015,12/03/2015,2
5300,1000270800,1000270800,1000270800,1000270800,11897419,12/24/2015,12/28/2015,12/03/2015,2
5300,100066392,0100066300,0100066300,0100066300,4176779,12/24/2015,12/28/2015,12/23/2015,2
5300,100066977,0100066900,0100066900,0100066900,13587718,12/24/2015,12/28/2015,12/23/2015,2
5300,100073043,0100073000,0100073000,0100073000,13619667,12/24/2015,12/28/2015,12/03/2015,2

ClientId,AccountNumber,ServiceId,PremiseId,ServicePoint,MeterId,StartDate,EndDate,BillingPeriodStartDate,FuelId
290,mgea1,mges1,mgep1,mgesp1,mgem1,12/24/2015,12/28/2015,12/01/2015,1
290,mgea2,mges2,mgep2,mgesp2,mgem2,12/24/2015,12/28/2015,12/01/2015,1
290,mgea3,mges3,mgep3,mgesp3,mgem3,12/25/2015,12/29/2015,12/15/2015,1

ClientId,AccountNumber,ServiceId,PremiseId,ServicePoint,MeterId,StartDate,EndDate,BillingPeriodStartDate,FuelId
290,mgea1,mges1,mgep1,mgesp1,mgem1,12/24/2015,12/28/2015,12/01/2015,1
290,mgea2,mges2,mgep2,mgesp2,mgem2,12/24/2015,12/28/2015,12/01/2015,1
290,mgea3,mges3,mgep3,mgesp3,mgem3,12/25/2015,12/29/2015,12/15/2015,1
290,mgwa1,mgws1,mgwp1,mgwsp1,mgwm1,12/24/2015,12/28/2015,12/01/2015,3
290,mgwa2,mgws2,mgwp2,mgwsp2,mgwm2,12/24/2015,12/28/2015,12/01/2015,3
290,mgwa3,mgws3,mgwp3,mgwsp3,mgwm3,12/25/2015,12/29/2015,12/15/2015,3
290,mgwa4,mgws4,mgwp4,mgwsp4,mgwm4,12/25/2015,12/29/2015,12/15/2015,3


Filename sample: 122915_033322_AMIRequest.csv
{0:MMddyy_hhmmss}{1}", DateTime.Now, "_AMIRequest.csv"
Format is: mddyy_hhmmss_AMIRequest.csv
(Should we suppor a zip file for faster transfer - perhaps in future)
Look here for legacy details on creation of csv file: Aclara.UFx.ServiceLibrary.DataIntegration.AMI, AMIRequestFileUtility.cs
See AMI database, SP: [dbo].[GetServicePointsForAMIRequest]


        //Fuel.
        public enum Fuel
        {
            None = 0,
            Electric = 1,
            Gas = 2,
            Water = 3,
            Total = 4
        }


******************
__AMI wide file__ header row and some sample data:

ClientId,MeterId,AccountNumber,ServicePointId,CommodityId,UOMId,VolumeFactor,Direction,ProjectedReadDate,TimeStamp,IntValue0000,IntValue0100,IntValue0200,IntValue0300,IntValue0400,IntValue0500,IntValue0600,IntValue0700,IntValue0800,IntValue0900,IntValue1000,IntValue1100,IntValue1200,IntValue1300,IntValue1400,IntValue1500,IntValue1600,IntValue1700,IntValue1800,IntValue1900,IntValue2000,IntValue2100,IntValue2200,IntValue2300,IntValue2400
87,meter-Irina-01,AccountIrina01,SP_Irina01,1,0,3,,10/31/2015,10/1/2015,0.18,0.244,0.238,0.192,0.131,0.24,0.165,0.187,0.207,0.139,0.146,0.193,0.161,0.163,0.146,0.142,0.169,0.248,0.135,0.244,0.186,0.213,0.174,0.251,0
87,meter-Irina-01,AccountIrina01,SP_Irina01,1,0,3,,10/31/2015,10/2/2015,0.155,0.197,0.216,0.242,0.212,0.165,0.152,0.129,0.178,0.213,0.217,0.22,0.216,0.142,0.236,0.197,0.218,0.128,0.207,0.19,0.248,0.212,0.174,0.144,0
87,meter-Irina-01,AccountIrina01,SP_Irina01,1,0,3,,10/31/2015,10/3/2015,0.249,0.157,0.193,0.234,0.189,0.171,0.15,0.19,0.167,0.176,0.204,0.22,0.25,0.172,0.169,0.231,0.223,0.169,0.178,0.209,0.128,0.227,0.204,0.134,0

**legacy data is in this order**
ClientId,AccountNumber,MeterId,ServicePointId,CommodityId,UOMId,VolumeFactor,Direction,ProjectedReadDate,TimeStamp,i0, i1, ...
**and header is not supplied**

1,a1-Asimplegastest-1,GM60M1a1p1s1-Asimplegastest-1,SP_GM60M1a1p1s1-Asimplegastest-1,2,2,3,,2016/01/21,2015/11/22,0.405,0.414,0.269,0.390,0.407,0.416,0.336,0.238,0.318,0.397,0.390,0.358,0.297,0.260,0.233,0.396,0.248,0.345,0.352,0.347,0.365,0.247,0.331,0.227,
1,a1-Asimplegastest-1,GM60M1a1p1s1-Asimplegastest-1,SP_GM60M1a1p1s1-Asimplegastest-1,2,2,3,,2016/01/21,2015/11/23,0.262,0.223,0.273,0.317,0.222,0.299,0.235,0.221,0.347,0.409,0.285,0.214,0.398,0.383,0.237,0.231,0.243,0.412,0.318,0.269,0.298,0.225,0.397,0.211,
1,a1-Asimplegastest-1,GM60M1a1p1s1-Asimplegastest-1,SP_GM60M1a1p1s1-Asimplegastest-1,2,2,3,,2016/01/21,2015/11/24,0.370,0.218,0.386,0.228,0.250,0.280,0.345,0.309,0.288,0.223,0.344,0.382,0.229,0.257,0.359,0.225,0.281,0.305,0.326,0.324,0.352,0.351,0.271,0.342,

5300,1000121640,5395347,1000121600,2,2,,,,11/14/2015,0,0,0.42,0.42,0.02,0.48,0.12,0.08,0,0,0.02,0.1,0.02,0.14,0.18,0.1,0.04,0.02,0.02,0.1,0.1,0.02,0.12,0,
5300,1000132483,12973287,1000132400,2,2,,,,11/14/2015,0.42,0.4,0.42,0.36,0.24,0.36,0.54,0.54,0.56,0.48,0.38,0.38,0.4,0.18,0.06,0.04,0,0.02,0.04,0.18,0.02,0.2,0.56,0.5,

Sample file names:
5300_60_20151229_001.csv (this is from SCG from EV MDM packeaged in Control file)
clientid_interval_date_filenum.csv

LUS water ami is gal. Interval is hourly
LUS electric ami is kwh. Interval is 15 minute data.

control file name example:
5300_Control_20130517101723_001.zip
5200_Manifest_20130517.xml

<?xml version="1.0" encoding="UTF-8"?>
<DataIntegrationManifest>
	<Import>
		<AMIFiles TriggerCalculations="true">
			<AMIFile Interval="60Minute">5300_60_20130517_001.csv</AMIFile>
		</AMIFiles>
	</Import>
</DataIntegrationManifest>



Header row is optional.  Some integrations will not include the header row.


***** THESE ARE NEW ENUMS ADDED FOR 16.02 *****

        public enum UomType
        {
            kwh = 0,
            therms = 1,
            ccf = 2,
            [Description("Cu. Ft.")]
            cf = 3,
            cgal = 4,
            hgal = 5,
            [Description("GALS.")]
            gal = 6,
            kgal = 7,
            hcf = 8,
            lbs = 9,
            trees = 10,
            [Description("CU. MTR.")]
            cm = 11
        }

        public enum CommodityType
        {
            Undefined = 0,
            Electric = 1,
            Gas = 2,
            Water = 3,
            Sewer = 4,
            Refuse = 5,
            Oil = 6,
            Propane = 7,
            Tv = 8,
            Fire = 9,
            Transportation = 10,
            Other = 99
        }

***** THESE WERE OLD ENUMS PRIOR TO  JANUARY 25th *****

        public enum AmiUomType
        {
            kWh = 0,
            Therms = 1,
            CCF = 2,
            CF = 3,
            cgal = 4,
            hgal = 5,
            gal = 6,
            kgal = 7
        }

		public enum AmiCommodityType
        {
            Undefined = 0,
            Electric = 1,
            Gas = 2,
            Water = 3
        }

        public enum Direction
        {
            Forward = 0,
            Reverse = 1
        }



******************
__Readings tall file__ header row and some sample data:

METER_ID,TRANSPONDER_ID,TRANSPONDER_PORT,CUSTOMER_ID,READING_VALUE,UNIT_OF_MEASURE,READING_DATETIME,TIMEZONE,BATTERY_VOLTAGE,COMMODITY_ID
57937743,6043067,1,77210,114782,CF,10-17-2015 00:00,GMT,3.58997,3
57937743,6043067,1,77210,114783,Cu. Ft.,10-17-2015 01:00,GMT,3.58997,3
57937743,6043067,1,77210,114783,Cu. Ft.,10-17-2015 02:00,GMT,3.58997,3
57937743,6043067,1,77210,114784,Cu. Ft.,10-17-2015 03:00,GMT,3.58997,3
57937743,6043067,1,77210,114784,Cu. Ft.,10-17-2015 04:00,GMT,3.58997,3
57937743,6043067,1,77210,114785,Cu. Ft.,10-17-2015 05:00,GMT,3.58997,3
11255,6043068,1,424420,221626,Cu. Ft.,10-17-2015 00:00,GMT,3.58997,3
11255,6043068,1,424420,221625,Cu. Ft.,10-17-2015 01:00,GMT,3.58997,3
11255,6043068,1,424420,221627,Cu. Ft.,10-17-2015 02:00,GMT,3.58997,3
11255,6043068,1,424420,221627,Cu. Ft.,10-17-2015 03:00,GMT,3.58997,3
11255,6043068,1,424420,221627,Cu. Ft.,10-17-2015 04:00,GMT,3.58997,3
11255,6043068,1,424420,221627,Cu. Ft.,10-17-2015 05:00,GMT,3.58997,3

Header row should be present.  Although, it could be optional.


***** THESE WERE OLD ENUMS PRIOR TO  JANUARY 25th *****

        public enum NccAmiUomType
        {
            [Description("Cu. Ft.")]
            CF = 3,
            [Description("GALS.")]
            gal = 6,
            [Description("CU. MTR.")]
            CM = 8
        }


NCC data is water from LADWP it seems.  
So this data is in Cubic Feet (sample above.)
Rates will be authored in HCF (take note.)  This means that the AMI provided to BTD must be scaled from CF to HCF.
NOTE that I added the COMMODITY_ID column on Jan 14th, and updated logic.

**************************
FROM AO Saviant area:


namespace CE.AO.Utilities
{
    public class Enums
    {
        public enum EventType
        {
            None,
            Ami,
            Bill,
            AccountUpdate,
            Subscription,
            NccAmiReading
        }
        public enum IntervalType
        {
            None,
            Fifteen = 15,
            Thirty = 30,
            Sixty = 60
        }
        public enum AmiCommodityType
        {
            Undefined = 0,
            Electric = 1,
            Gas = 2,
            Water = 3
        }
        public enum CommodityType
        {
            Electric = 1,
            Gas = 2,
            Water = 3,
            Sewer = 4,
            Refuse = 5,
            Oil = 6,
            Propane = 7,
            Tv = 8,
            Fire = 9,
            Transportation = 10,
            Other = 99
        }
        public enum Country
        {
            // ReSharper disable once InconsistentNaming
            US
        }
        public enum Source
        {
            Utility
        }
        public enum Direction
        {
            Forward = 0,
            Reverse = 1
        }
        [SuppressMessage("ReSharper", "InconsistentNaming")]  *********** OLD USED IN BILLS **** NEWER ONE ABOVE *****
        public enum UomType
        {
            ccf = 1,
            gal = 2,
            kwh = 3,
            lbs = 4,
            therms = 5,
            trees = 6
        }
        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public enum AmiUomType
        {
            kWh = 0,
            Therms = 1,
            CCF = 2,
            CF = 3,
            cgal = 4,
            hgal = 5,
            gal = 6,
            kgal = 7
        }
        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public enum NccAmiUomType
        {
            [Description("Cu. Ft.")]
            CF = 3,
            [Description("GALS.")]
            gal = 6,
            [Description("CU. MTR.")]
            CM = 8
        }
        public enum BillPeriodType
        {
            Monthly = 1,
            BiMonthly = 2,
            Quarterly = 3
        }
        public enum CalcalutionType
        {
            None,
            CostToDate,
            BillToDate
        }
        public enum AccountUpdateType
        {
            RateClass,
            Email,
            Phone1,
            Address1,
            State,
            Zip,
            FirstName,
            LastName,
            MeterReplacement,
            NewMeter,
            BillCycle,
            RemoveMeter,
            RemoveAccount,
            RemoveService,
            RemovePremise,
            ReadStartDate,
            ReadEndDate,
            NewServiceContract,
            NewServicePoint
        }

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public enum MeterType
        {
            ami,
            regular,
            nonmetered
        }

        public enum NotificationFrequency
        {
            Daily,
            Weekly,
            Monthly,
            Threshold
        }

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public enum SubscriptionChannel
        {
            Email,
            SMS,
            EmailAndSMS,
            File
        }

        public enum InsightType
        {
            Cost,
            ProjectedCost,
            Usage,
            AccountLevelCostThreshold,
            ServiceLevelUsageThreshold,
            ServiceLevelCostThreshold,
            DayThreshold,
            ServiceLevelTieredThresholdApproaching,
            ServiceLevelTieredThresholdExceed
        }

        public enum CustomerType
        {
            Residential,
            Commercial
        }

        public enum IsEstimate : byte
        {
            False = 0,
            True = 1
        }

        public enum AccountStructureType : byte
        {
            AccountHasOnePremise = 1,
            AccountHasMultiplePremises = 2,
            PremiseHasMultipleAccounts = 3
        }

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public enum Timezone
        {
            [Description("Atlantic Standard Time")]
            ADT,
            [Description("Atlantic Standard Time")]
            AST,
            [Description("Alaskan Standard Time")]
            AKDT,
            [Description("Alaskan Standard Time")]
            AKST,
            [Description("Central Standard Time")]
            CDT,
            [Description("Central Standard Time")]
            CST,
            [Description("Eastern Standard Time")]
            EDT,
            [Description("Eastern Standard Time")]
            EST,
            [Description("Greenwich Standard Time")]
            GMT,
            [Description("Hawaiian Standard Time")]
            HADT,
            [Description("Hawaiian Standard Time")]
            HAST,
            [Description("Mountain Standard Time")]
            MDT,
            [Description("Mountain Standard Time")]
            MST,
            [Description("Pacific Standard Time")]
            PDT,
            [Description("Pacific Standard Time")]
            PST,
            [Description("UTC")]
            UTC
        }

        public static T GetValueFromDescription<T>(string description)
        {
            var type = typeof(T);
            if (!type.IsEnum) throw new InvalidOperationException();
            foreach (var field in type.GetFields())
            {
                var attribute = Attribute.GetCustomAttribute(field,
                    typeof(DescriptionAttribute)) as DescriptionAttribute;
                if (attribute != null)
                {
                    if (attribute.Description == description)
                        return (T)field.GetValue(null);
                }
                else
                {
                    if (field.Name == description)
                        return (T)field.GetValue(null);
                }
            }
            throw new ArgumentException("Not found.", nameof(description));
            //or return default(T);
        }
    }
}



******************customers file****for generating ami****electric, gas, water

ClientId,CustomerID,AccountID,MeterID,ServicePoint,Commodity,amiuom,interval
87,993774751,1218337093,189354704,SP_189354704,1,0,15
87,9000EG1,9000EG1,M9000EG_elec,SP_M9000EG_elec,1,0,15
87,9000EG2,9000EG2,M9000EG2_elec,SP_M9000EG2_elec,1,0,15
87,9000E1,9000E1,M9000E,SP_M9000E,1,0,15
87,9000E2,9000E2,M9000E2,SP_M9000E2,1,0,15
87,9000E3,9000E3,M9000E3,SP_M9000E3,1,0,15

