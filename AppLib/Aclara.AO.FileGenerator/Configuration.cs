﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace Aclara.AO.FileGenerator
{
    public class Configuration : IConfig
    {
        private int _clientid;

        public Configuration(int clientId)
        {
            _clientid = clientId;
        }

        public string AMIRequestFileDropFolder { get; set; }
        public string AMIWideFileDateFormat { get; set; }
        public string AMITallWorkingFolder { get; set; }
        public string AMIWideWorkingFolder { get; set; }

        public string WaterUnitOfMeasure { get; set; }
        public string WaterInterval { get; set; }
        public string ElectricUnitOfMeasure { get; set; }
        public string ElectricInterval { get; set; }
        public string GasUnitOfMeasure { get; set; }
        public string GasInterval { get; set; }

        public string FtpHost { get; set; }
        public string FtpPort { get; set; }
        public string FtpUsername { get; set; }
        public string FtpPassword { get; set; }
        public string FtpControlFileDrop { get; set; }
        public string FtpFileDrop { get; set; }


        public void LoadAMIRequestFileModeConfiguration()
        {

            LoadCommonConfiguration();


            if (ConfigurationManager.AppSettings[_clientid + ".FTPDirectory.ControlFileDrop"] != null)
            {
                FtpControlFileDrop = ConfigurationManager.AppSettings[_clientid + ".FTPDirectory.ControlFileDrop"];
            }
            else
            {
                FtpControlFileDrop = "unk";
            }



            if (ConfigurationManager.AppSettings["AMIRequestFileDropFolder"] != null)
            {
                AMIRequestFileDropFolder = ConfigurationManager.AppSettings["AMIRequestFileDropFolder"];
            }


        }


        public void LoadTallFileModeConfiguration()
        {

            LoadCommonConfiguration();


            if (ConfigurationManager.AppSettings["AMITallWorkingFolder"] != null)
            {
                AMITallWorkingFolder = ConfigurationManager.AppSettings["AMITallWorkingFolder"];
            }


        }

        public void LoadWideFileModeConfiguration()
        {
            LoadCommonConfiguration();

            if (ConfigurationManager.AppSettings["AMIWideWorkingFolder"] != null)
            {
                AMIWideWorkingFolder = ConfigurationManager.AppSettings["AMIWideWorkingFolder"];
            }

        }

        /// <summary>
        ///  Helper for common configuration.
        /// </summary>
        private void LoadCommonConfiguration()
        {

            if (ConfigurationManager.AppSettings["FTPHost"] != null)
            {
                FtpHost = ConfigurationManager.AppSettings["FTPHost"];
            }

            if (ConfigurationManager.AppSettings["FTPPort"] != null)
            {
                FtpPort = ConfigurationManager.AppSettings["FTPPort"];
            }

            if (ConfigurationManager.AppSettings["FTPUsername"] != null)
            {
                FtpUsername = ConfigurationManager.AppSettings["FTPUsername"];
            }

            if (ConfigurationManager.AppSettings["FTPPassword"] != null)
            {
                FtpPassword = ConfigurationManager.AppSettings["FTPPassword"];
            }


            if (ConfigurationManager.AppSettings["AMIWideFileDateFormat"] != null)
            {
                AMIWideFileDateFormat = ConfigurationManager.AppSettings["AMIWideFileDateFormat"];
            }


            if (ConfigurationManager.AppSettings[_clientid + ".FTPDirectory.FileDrop"] != null)
            {
                FtpFileDrop = ConfigurationManager.AppSettings[_clientid + ".FTPDirectory.FileDrop"];
            }
            else
            {
                FtpFileDrop = "unk";
            }



            if (ConfigurationManager.AppSettings[_clientid + ".Water.Interval"] != null)
            {
                WaterInterval = ConfigurationManager.AppSettings[_clientid + ".Water.Interval"];
            }
            else
            {
                WaterInterval = "60";
            }

            if (ConfigurationManager.AppSettings[_clientid + ".Water.Uom"] != null)
            {
                WaterUnitOfMeasure = ConfigurationManager.AppSettings[_clientid + ".Water.Uom"];
            }
            else
            {
                WaterInterval = "6";
            }

            if (ConfigurationManager.AppSettings[_clientid + ".Electric.Interval"] != null)
            {
                ElectricInterval = ConfigurationManager.AppSettings[_clientid + ".Electric.Interval"];
            }
            else
            {
                ElectricInterval = "60";
            }

            if (ConfigurationManager.AppSettings[_clientid + ".Electric.Uom"] != null)
            {
                ElectricUnitOfMeasure = ConfigurationManager.AppSettings[_clientid + ".Electric.Uom"];
            }
            else
            {
                ElectricUnitOfMeasure = "0";
            }

            if (ConfigurationManager.AppSettings[_clientid + ".Gas.Interval"] != null)
            {
                GasInterval = ConfigurationManager.AppSettings[_clientid + ".Gas.Interval"];
            }
            else
            {
                GasInterval = "60";
            }

            if (ConfigurationManager.AppSettings[_clientid + ".Gas.Uom"] != null)
            {
                GasUnitOfMeasure = ConfigurationManager.AppSettings[_clientid + ".Gas.Uom"];
            }
            else
            {
                GasUnitOfMeasure = "2";
            }

        }


    }
}
