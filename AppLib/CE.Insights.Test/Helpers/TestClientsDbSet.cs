﻿using System.Linq;
using CE.Models.Insights.EF;

namespace CE.Insights.Test.Helpers
{
    class TestClientsDbSet : TestDbSet<Client>
    {
        public override Client Find(params object[] keyValues)
        {
            return this.SingleOrDefault(Client => Client.ClientID == (int)keyValues.Single());
        }

    }
}
