﻿using CE.Models.Insights.EF;
using Moq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace CE.Insights.Test.Helpers
{
    public class TestInsightsEntitiesMock
    {

        public Mock<InsightsEntities> MockInsightsEntities;

        public TestInsightsEntitiesMock()
        {
            MockInsightsEntities = new Mock<InsightsEntities>();

            AuthorizationDbSetMock();
            ApplicationInfoDbSetMock();
            ApplicationInfoScopeDbSetMock();
            RetailCustomerDbSetMock();
            SubscriptionDbSetMock();
            SubscriptionUsagePointDbSetMock();
            UsagePointDbSetMock();
            UsagePointRelatedLinkDbSetMock();
            ReadingTypeDbSetMock();
            TimeConfigurationDbSetMock();
        }

        private void RetailCustomerDbSetMock()
        {
            var retailCustomerDbSetMock = new Mock<DbSet<retail_customers>>();
            var retailCustomerMock = MockRetailCustomerDbs().AsQueryable();

            retailCustomerDbSetMock.As<IQueryable<retail_customers>>()
                .Setup(m => m.Provider)
                .Returns(retailCustomerMock.Provider);

            retailCustomerDbSetMock.As<IQueryable<retail_customers>>()
                .Setup(m => m.Expression)
                .Returns(retailCustomerMock.Expression);

            retailCustomerDbSetMock.As<IQueryable<retail_customers>>()
                .Setup(m => m.ElementType)
                .Returns(retailCustomerMock.ElementType);

            retailCustomerDbSetMock.As<IQueryable<retail_customers>>()
                .Setup(m => m.GetEnumerator())
                .Returns(retailCustomerMock.GetEnumerator());

            MockInsightsEntities.Setup(i => i.retail_customers).Returns(retailCustomerDbSetMock.Object);
        }
        private List<retail_customers> MockRetailCustomerDbs()
        {
            var retailCustomerMock = new List<retail_customers>
            {
                new retail_customers
                {
                    id = 1,
                    data_custodian_account_id = "account1",
                    data_custodian_customer_id = "1"
                },
                new retail_customers
                {
                    id = 2,
                    data_custodian_account_id = "account2"
                },
                new retail_customers
                {
                    id = 3,
                    data_custodian_account_id = "account3"
                },
                new retail_customers
                {
                    id = 4,
                    data_custodian_account_id = "account4"
                },
                new retail_customers
                {
                    id = 5,
                    data_custodian_account_id = "account5"
                },
                new retail_customers // daily meter
                {
                    id = 6,
                    data_custodian_account_id = "account6"
                }
            };


            return retailCustomerMock;
        }

        private void SubscriptionDbSetMock()
        {
            var subscriptionDbSetMock = new Mock<DbSet<subscription>>();
            var subscriptionMock = MockSubscriptionDbs().AsQueryable();

            subscriptionDbSetMock.As<IQueryable<subscription>>()
                .Setup(m => m.Provider)
                .Returns(subscriptionMock.Provider);

            subscriptionDbSetMock.As<IQueryable<subscription>>()
                .Setup(m => m.Expression)
                .Returns(subscriptionMock.Expression);

            subscriptionDbSetMock.As<IQueryable<subscription>>()
                .Setup(m => m.ElementType)
                .Returns(subscriptionMock.ElementType);

            subscriptionDbSetMock.As<IQueryable<subscription>>()
                .Setup(m => m.GetEnumerator())
                .Returns(subscriptionMock.GetEnumerator());

            MockInsightsEntities.Setup(i => i.subscription).Returns(subscriptionDbSetMock.Object);
        }
        private List<subscription> MockSubscriptionDbs()
        {

            var subscriptiontMock = new List<subscription>();

            // customer 1 - meter 1, meter 2
            subscriptiontMock.Add(new subscription
            {
                id = 1,
                uuid = "GuidSubscription1",
                self_link_href = "selfLink",
                self_link_rel = "self",
                up_link_href = "upLink",
                up_link_rel = "up",
                retail_customer_id = 1,
                application_information_id = 1,
                authorization_id = 1,
                updated = DateTime.UtcNow,
                published = DateTime.UtcNow
            });
            // customer 1 - meter 3
            subscriptiontMock.Add(new subscription
            {
                id = 2,
                uuid = "GuidSubscription2",
                self_link_href = "selfLink",
                self_link_rel = "self",
                up_link_href = "upLink",
                up_link_rel = "up",
                retail_customer_id = 1,
                application_information_id = 1,
                authorization_id = 2,
                updated = DateTime.UtcNow,
                published = DateTime.UtcNow
            });
            // customer 2 - meter 4
            subscriptiontMock.Add(new subscription
            {
                id = 3,
                uuid = "GuidSubscription3",
                self_link_href = "selfLink",
                self_link_rel = "self",
                up_link_href = "upLink",
                up_link_rel = "up",
                retail_customer_id = 2,
                application_information_id = 2,
                authorization_id = 3,
                updated = DateTime.UtcNow,
                published = DateTime.UtcNow
            });
            // customer 3 - no usage point
            subscriptiontMock.Add(new subscription
            {
                id = 4,
                uuid = "GuidSubscription4",
                self_link_href = "selfLink",
                self_link_rel = "self",
                up_link_href = "upLink",
                up_link_rel = "up",
                retail_customer_id = 3,
                application_information_id = 3,
                authorization_id = 4,
                updated = DateTime.UtcNow,
                published = DateTime.UtcNow
            });
            // customer 4 - no reading type meter 5
            subscriptiontMock.Add(new subscription
            {
                id = 5,
                uuid = "GuidSubscription5",
                self_link_href = "selfLink",
                self_link_rel = "self",
                up_link_href = "upLink",
                up_link_rel = "up",
                retail_customer_id = 4,
                application_information_id = 4,
                authorization_id = 5,
                updated = DateTime.UtcNow,
                published = DateTime.UtcNow
            });
            // customer 5 - meter 6
            subscriptiontMock.Add(new subscription
            {
                id = 6,
                uuid = "GuidSubscription5",
                self_link_href = "selfLink",
                self_link_rel = "self",
                up_link_href = "upLink",
                up_link_rel = "up",
                retail_customer_id = 5,
                application_information_id = 24,
                authorization_id = 100,
                updated = DateTime.UtcNow,
                published = DateTime.UtcNow
            });

            // customer 6 - meter 7 - daily
            subscriptiontMock.Add(new subscription
            {
                id = 7,
                uuid = "GuidSubscription7",
                self_link_href = "selfLink",
                self_link_rel = "self",
                up_link_href = "upLink",
                up_link_rel = "up",
                retail_customer_id = 6,
                application_information_id = 32,
                authorization_id = 12,
                updated = DateTime.UtcNow,
                published = DateTime.UtcNow
            });

            return subscriptiontMock;
        }
        private void SubscriptionUsagePointDbSetMock()
        {
            var subscriptionUsagePointsDbSetMock = new Mock<DbSet<subscription_usage_points>>();
            var subscriptionUsagePointsMock = MockSubscriptionUsagePointDbs().AsQueryable();

            subscriptionUsagePointsDbSetMock.As<IQueryable<subscription_usage_points>>()
                .Setup(m => m.Provider)
                .Returns(subscriptionUsagePointsMock.Provider);

            subscriptionUsagePointsDbSetMock.As<IQueryable<subscription_usage_points>>()
                .Setup(m => m.Expression)
                .Returns(subscriptionUsagePointsMock.Expression);

            subscriptionUsagePointsDbSetMock.As<IQueryable<subscription_usage_points>>()
                .Setup(m => m.ElementType)
                .Returns(subscriptionUsagePointsMock.ElementType);

            subscriptionUsagePointsDbSetMock.As<IQueryable<subscription_usage_points>>()
                .Setup(m => m.GetEnumerator())
                .Returns(subscriptionUsagePointsMock.GetEnumerator());

            MockInsightsEntities.Setup(i => i.subscription_usage_points).Returns(subscriptionUsagePointsDbSetMock.Object);
        }
        private List<subscription_usage_points> MockSubscriptionUsagePointDbs()
        {

            var subscriptionUsagePointMock = new List<subscription_usage_points>();

            subscriptionUsagePointMock.Add(new subscription_usage_points { subscription_id = 1, usage_point_id = 1 });
            subscriptionUsagePointMock.Add(new subscription_usage_points { subscription_id = 1, usage_point_id = 2 });
            subscriptionUsagePointMock.Add(new subscription_usage_points { subscription_id = 2, usage_point_id = 3 });
            subscriptionUsagePointMock.Add(new subscription_usage_points { subscription_id = 3, usage_point_id = 4 });
            subscriptionUsagePointMock.Add(new subscription_usage_points { subscription_id = 5, usage_point_id = 5 });
            subscriptionUsagePointMock.Add(new subscription_usage_points { subscription_id = 6, usage_point_id = 6 });
            subscriptionUsagePointMock.Add(new subscription_usage_points { subscription_id = 7, usage_point_id = 7 });

            return subscriptionUsagePointMock;
        }
        private void UsagePointDbSetMock()
        {
            var usagePointsDbSetMock = new Mock<DbSet<usage_points>>();
            var usagePointsMock = MockUsagePointDbs().AsQueryable();

            usagePointsDbSetMock.As<IQueryable<usage_points>>()
                .Setup(m => m.Provider)
                .Returns(usagePointsMock.Provider);

            usagePointsDbSetMock.As<IQueryable<usage_points>>()
                .Setup(m => m.Expression)
                .Returns(usagePointsMock.Expression);

            usagePointsDbSetMock.As<IQueryable<usage_points>>()
                .Setup(m => m.ElementType)
                .Returns(usagePointsMock.ElementType);

            usagePointsDbSetMock.As<IQueryable<usage_points>>()
                .Setup(m => m.GetEnumerator())
                .Returns(usagePointsMock.GetEnumerator());
            MockInsightsEntities.Setup(i => i.usage_points).Returns(usagePointsDbSetMock.Object);

        }
        private List<usage_points> MockUsagePointDbs()
        {
            var usagePointMock = new List<usage_points>();

            usagePointMock.Add(new usage_points
            {
                id = 1,
                uuid = "23FB453B-C50C-4E2F-96E7-3D338BAAD9D8",
                self_link_href = "/Datacustodian/espi/1_1/resource/Subscription/1/UsagePoint/meter1encryted",
                self_link_rel = "self",
                up_link_href = "/Datacustodian/espi/1_1/resource/Subscription/1/UsagePoint",
                up_link_rel = "up",
                meterid = "meter1",
                local_time_parameters_id = null,
                updated = DateTime.UtcNow,
                published = DateTime.UtcNow
            });
            usagePointMock.Add(new usage_points
            {
                id = 2,
                uuid = "A02C4C34-C4E2-490A-8D20-F0780AF91587",
                self_link_href = "/Datacustodian/espi/1_1/resource/Subscription/1/UsagePoint/meter2encryted",
                self_link_rel = "self",
                up_link_href = "/Datacustodian/espi/1_1/resource/Subscription/1/UsagePoint",
                up_link_rel = "up",
                meterid = "meter2",
                local_time_parameters_id = null,
                updated = DateTime.UtcNow,
                published = DateTime.UtcNow
            });
            usagePointMock.Add(new usage_points
            {
                id = 3,
                uuid = "822062ED-9C56-4EF4-82F9-6F5815D68486",
                self_link_href = "/Datacustodian/espi/1_1/resource/Subscription/2/UsagePoint/meter3encryted",
                self_link_rel = "self",
                up_link_href = "/Datacustodian/espi/1_1/resource/Subscription/2/UsagePoint",
                up_link_rel = "up",
                meterid = "meter3",
                local_time_parameters_id = 1,
                updated = DateTime.UtcNow,
                published = DateTime.UtcNow
            });
            usagePointMock.Add(new usage_points
            {
                id = 4,
                uuid = "GuidUsagePoint4",
                self_link_href = "/Datacustodian/espi/1_1/resource/Subscription/3/UsagePoint/meter4encryted",
                self_link_rel = "self",
                up_link_href = "/Datacustodian/espi/1_1/resource/Subscription/3/UsagePoint",
                up_link_rel = "up",
                meterid = "meter4",
                local_time_parameters_id = null,
                updated = DateTime.UtcNow,
                published = DateTime.UtcNow
            });
            usagePointMock.Add(new usage_points
            {
                id = 5,
                uuid = "GuidUsagePoint5",
                self_link_href = "/Datacustodian/espi/1_1/resource/Subscription/5/UsagePoint/meter5encryted",
                self_link_rel = "self",
                up_link_href = "/Datacustodian/espi/1_1/resource/Subscription/5/UsagePoint",
                up_link_rel = "up",
                meterid = "meter5",
                local_time_parameters_id = null,
                updated = DateTime.UtcNow,
                published = DateTime.UtcNow
            });
            usagePointMock.Add(new usage_points
            {
                id = 6,
                uuid = "GuidUsagePoint6",
                self_link_href = "/Datacustodian/espi/1_1/resource/Subscription/6/UsagePoint/meter5encryted",
                self_link_rel = "self",
                up_link_href = "/Datacustodian/espi/1_1/resource/Subscription/6/UsagePoint",
                up_link_rel = "up",
                meterid = "meter6",
                local_time_parameters_id = null,
                updated = DateTime.UtcNow,
                published = DateTime.UtcNow
            });

            // meter 7 daily
            usagePointMock.Add(new usage_points
            {
                id = 7,
                uuid = "GuidUsagePoint6",
                self_link_href = "/Datacustodian/espi/1_1/resource/Subscription/7/UsagePoint/meter7encryted",
                self_link_rel = "self",
                up_link_href = "/Datacustodian/espi/1_1/resource/Subscription/7/UsagePoint",
                up_link_rel = "up",
                meterid = "meter7",
                local_time_parameters_id = null,
                updated = DateTime.UtcNow,
                published = DateTime.UtcNow
            });

            return usagePointMock;
        }

        private void UsagePointRelatedLinkDbSetMock()
        {
            var usagePointRelatedLinkDbSetMock = new Mock<DbSet<usage_point_related_links>>();
            var usagePointRelatedLinkMock = MockUasagePointRelatedLinkDbs().AsQueryable();

            usagePointRelatedLinkDbSetMock.As<IQueryable<usage_point_related_links>>()
                .Setup(m => m.Provider)
                .Returns(usagePointRelatedLinkMock.Provider);

            usagePointRelatedLinkDbSetMock.As<IQueryable<usage_point_related_links>>()
                .Setup(m => m.Expression)
                .Returns(usagePointRelatedLinkMock.Expression);

            usagePointRelatedLinkDbSetMock.As<IQueryable<usage_point_related_links>>()
                .Setup(m => m.ElementType)
                .Returns(usagePointRelatedLinkMock.ElementType);

            usagePointRelatedLinkDbSetMock.As<IQueryable<usage_point_related_links>>()
                .Setup(m => m.GetEnumerator())
                .Returns(usagePointRelatedLinkMock.GetEnumerator());
            MockInsightsEntities.Setup(i => i.usage_point_related_links).Returns(usagePointRelatedLinkDbSetMock.Object);
        }

        private List<usage_point_related_links> MockUasagePointRelatedLinkDbs()
        {
            var relatedLinks = new List<usage_point_related_links>();

            relatedLinks.Add(new usage_point_related_links
            {
                id = 1,
                href = "/Datacustodian/espi/1_1/resource/Subscription/1/UsagePoint/meter1encryted/MeterReading",
                rel = "related",
                usage_point_id = 1
            });
            relatedLinks.Add(new usage_point_related_links
            {
                id = 2,
                href = "/DataCustodian/espi/1_1/resource/LocalTimeParameters/1",
                rel = "related",
                usage_point_id = 1
            });
            return relatedLinks;
        }
        private void TimeConfigurationDbSetMock()
        {
            var timeConfiguationDbSetMock = new Mock<DbSet<time_configurations>>();

            var timeConfigurationMock = MockTimeConfigurationDbs().AsQueryable();

            timeConfiguationDbSetMock.As<IQueryable<time_configurations>>()
                .Setup(m => m.Provider)
                .Returns(timeConfigurationMock.Provider);

            timeConfiguationDbSetMock.As<IQueryable<time_configurations>>()
                .Setup(m => m.Expression)
                .Returns(timeConfigurationMock.Expression);

            timeConfiguationDbSetMock.As<IQueryable<time_configurations>>()
                .Setup(m => m.ElementType)
                .Returns(timeConfigurationMock.ElementType);

            timeConfiguationDbSetMock.As<IQueryable<time_configurations>>()
                .Setup(m => m.GetEnumerator())
                .Returns(timeConfigurationMock.GetEnumerator());

            MockInsightsEntities.Setup(i => i.time_configurations).Returns(timeConfiguationDbSetMock.Object);
        }
        private List<time_configurations> MockTimeConfigurationDbs()
        {
            var timeConfigaruationMock = new List<time_configurations>();

            timeConfigaruationMock.Add(new time_configurations
            {
                dstEndRule = "B40E2000",
                dstOffset = 3600,
                dstStartRule = "360E2000",
                id = 1,
                published = DateTime.UtcNow,
                updated = DateTime.UtcNow,
                self_link_href = "/Datacustodian/espi/1_1/resource/LocalTimeParameters/1",
                self_link_rel = "self",
                up_link_href = "/Datacustodian/espi/1_1/resource/LocalTimeParameters",
                up_link_rel = "up",
                tzOffset = -18000,
                uuid = "31F9E775-3A5F-4770-8B4C-656E549FB134"
            });

            timeConfigaruationMock.Add(new time_configurations
            {
                dstEndRule = "B40E2000",
                dstOffset = 3600,
                dstStartRule = "360E2000",
                id = 2,
                published = DateTime.UtcNow,
                updated = DateTime.UtcNow,
                self_link_href = "/Datacustodian/espi/1_1/resource/LocalTimeParameters/2",
                self_link_rel = "self",
                up_link_href = "/Datacustodian/espi/1_1/resource/LocalTimeParameters",
                up_link_rel = "up",
                tzOffset = -21600,
                uuid = "77CE5940-013B-48CE-A3AB-2398A87180A1"
            });

            timeConfigaruationMock.Add(new time_configurations
            {
                dstEndRule = "B40E2000",
                dstOffset = 3600,
                dstStartRule = "360E2000",
                id = 3,
                published = DateTime.UtcNow,
                updated = DateTime.UtcNow,
                self_link_href = "/Datacustodian/espi/1_1/resource/LocalTimeParameters/3",
                self_link_rel = "self",
                up_link_href = "/Datacustodian/espi/1_1/resource/LocalTimeParameters",
                up_link_rel = "up",
                tzOffset = -25200,
                uuid = "C30B8614-B117-43A4-AD2E-E20B7A2DE645"
            });

            timeConfigaruationMock.Add(new time_configurations
            {
                dstEndRule = "B40E2000",
                dstOffset = 3600,
                dstStartRule = "360E2000",
                id = 4,
                published = DateTime.UtcNow,
                updated = DateTime.UtcNow,
                self_link_href = "/Datacustodian/espi/1_1/resource/LocalTimeParameters/4",
                self_link_rel = "self",
                up_link_href = "/Datacustodian/espi/1_1/resource/LocalTimeParameters",
                up_link_rel = "up",
                tzOffset = -28800,
                uuid = "F2B4020F-9A0A-464A-806A-0B8302F51A7B"
            });

            timeConfigaruationMock.Add(new time_configurations
            {
                dstEndRule = "B40E2000",
                dstOffset = 3600,
                dstStartRule = "360E2000",
                id = 5,
                published = DateTime.UtcNow,
                updated = DateTime.UtcNow,
                self_link_href = "/Datacustodian/espi/1_1/resource/LocalTimeParameters/5",
                self_link_rel = "self",
                up_link_href = "/Datacustodian/espi/1_1/resource/LocalTimeParameters",
                up_link_rel = "up",
                tzOffset = -32400,
                uuid = "ED3EDE0C-6548-45DF-9F24-261C38DCD680"
            });

            timeConfigaruationMock.Add(new time_configurations
            {
                dstEndRule = "B40E2000",
                dstOffset = 3600,
                dstStartRule = "360E2000",
                id = 6,
                published = DateTime.UtcNow,
                updated = DateTime.UtcNow,
                self_link_href = "/Datacustodian/espi/1_1/resource/LocalTimeParameters/6",
                self_link_rel = "self",
                up_link_href = "/Datacustodian/espi/1_1/resource/LocalTimeParameters",
                up_link_rel = "up",
                tzOffset = -36000,
                uuid = "B4FEA3D8-196B-464E-B68B-2AECC9C3E0BC"
            });


            return timeConfigaruationMock;
        }


        private void ReadingTypeDbSetMock()
        {
            var readingTypeDbSetMock = new Mock<DbSet<reading_types>>();

            var readingTypeMock = MockReadingTypeDbs().AsQueryable();

            readingTypeDbSetMock.As<IQueryable<reading_types>>()
                .Setup(m => m.Provider)
                .Returns(readingTypeMock.Provider);

            readingTypeDbSetMock.As<IQueryable<reading_types>>()
                .Setup(m => m.Expression)
                .Returns(readingTypeMock.Expression);

            readingTypeDbSetMock.As<IQueryable<reading_types>>()
                .Setup(m => m.ElementType)
                .Returns(readingTypeMock.ElementType);

            readingTypeDbSetMock.As<IQueryable<reading_types>>()
                .Setup(m => m.GetEnumerator())
                .Returns(readingTypeMock.GetEnumerator());

            MockInsightsEntities.Setup(i => i.reading_types).Returns(readingTypeDbSetMock.Object);
        }

        private List<reading_types> MockReadingTypeDbs()
        {
            var readingTypesMock = new List<reading_types>();

            // no conversion
            readingTypesMock.Add(new reading_types
            {
                id = 1,
                self_link_href = "/selflink",
                self_link_rel = "self",
                up_link_href = "/uplink",
                up_link_ref = "up",
                accumulationBehaviour = "4",
                commodity = "0",
                dateQualifier = "0",
                flowDirection = "1",
                intervalLength = 3600,
                timeAttribute = "60",
                uom = "72",
                kind = "0",
                phase = "0",
                powerOfTenMultiplier = "0",
                published = DateTime.UtcNow.Date,
                updated = DateTime.UtcNow.Date,
                uuid = "meter1uuid",
                meterid = "meter1"  // usage point 1
            });
            readingTypesMock.Add(new reading_types
            {
                id = 2,
                self_link_href = "/selflink",
                self_link_rel = "self",
                up_link_href = "/uplink",
                up_link_ref = "up",
                accumulationBehaviour = "4",
                commodity = "1",
                dateQualifier = "0",
                flowDirection = "1",
                intervalLength = 3600,
                timeAttribute = "60",
                uom = "169", //therm
                kind = "1",
                phase = "0",
                powerOfTenMultiplier = "-3",
                published = DateTime.UtcNow.Date,
                updated = DateTime.UtcNow.Date,
                uuid = "meter2uuid",
                meterid = "meter2"  // usage point 1
            });
            readingTypesMock.Add(new reading_types
            {
                id = 3,
                self_link_href = "/selflink",
                self_link_rel = "self",
                up_link_href = "/uplink",
                up_link_ref = "up",
                accumulationBehaviour = "4",
                commodity = "0",
                dateQualifier = "0",
                flowDirection = "1",
                intervalLength = 3600,
                timeAttribute = "60",
                uom = "72",
                kind = "0",
                phase = "0",
                powerOfTenMultiplier = "0",
                published = DateTime.UtcNow.Date,
                updated = DateTime.UtcNow.Date,
                uuid = "meter3uuid1",
                meterid = "meter3"  // usage point 1
            });
            readingTypesMock.Add(new reading_types
            {
                id = 4,
                self_link_href = "/selflink",
                self_link_rel = "self",
                up_link_href = "/uplink",
                up_link_ref = "up",
                accumulationBehaviour = "4",
                commodity = "0",
                dateQualifier = "0",
                flowDirection = "1",
                intervalLength = 900,
                timeAttribute = "15",
                uom = "72",
                kind = "0",
                phase = "0",
                powerOfTenMultiplier = "0",
                published = DateTime.UtcNow.Date,
                updated = DateTime.UtcNow.Date,
                uuid = "meter3uuid2",
                meterid = "meter3"  // usage point 1
            });
            readingTypesMock.Add(new reading_types
            {
                id = 5,
                self_link_href = "/selflink",
                self_link_rel = "self",
                up_link_href = "/uplink",
                up_link_ref = "up",
                accumulationBehaviour = "4",
                commodity = "0",
                dateQualifier = "0",
                flowDirection = "1",
                intervalLength = 3600,
                timeAttribute = "60",
                uom = "72",
                kind = "0",
                phase = "0",
                powerOfTenMultiplier = "0",
                published = DateTime.UtcNow.Date,
                updated = DateTime.UtcNow.Date,
                uuid = "meter6uuid1",
                meterid = "meter6"  // usage point 6
            });

            return readingTypesMock;
        }


        private void AuthorizationDbSetMock()
        {
            var authorizationDbSetMock = new Mock<DbSet<authorizations>>();

            var authorizationMock = MockAuthorizationDbs().AsQueryable();

            authorizationDbSetMock.As<IQueryable<authorizations>>()
                .Setup(m => m.Provider)
                .Returns(authorizationMock.Provider);

            authorizationDbSetMock.As<IQueryable<authorizations>>()
                .Setup(m => m.Expression)
                .Returns(authorizationMock.Expression);

            authorizationDbSetMock.As<IQueryable<authorizations>>()
                .Setup(m => m.ElementType)
                .Returns(authorizationMock.ElementType);

            authorizationDbSetMock.As<IQueryable<authorizations>>()
                .Setup(m => m.GetEnumerator())
                .Returns(authorizationMock.GetEnumerator());

            MockInsightsEntities.Setup(i => i.authorizations).Returns(authorizationDbSetMock.Object);
        }

        private List<authorizations> MockAuthorizationDbs()
        {
            var authorizationsMock = new List<authorizations>
            {
                new authorizations
                {
                    id = 90,
                    application_information_id = 26,
                    access_token = "token1",
                    scope =
                        "FB=1_3_4_5_13_14_18_37;IntervalDuration=900_1800_3600;BlockDuration=Daily; HistoryLength=34128000;SubscriptionFrequency=Daily; AccountCollection=5;BR=1;",
                    subscription_id = 1,
                    third_party = "third_party"
                },
                new authorizations
                {
                    id = 91,
                    application_information_id = 26,
                    access_token = "token12",
                    scope =
                        "FB=1_3_4_5_13_14_18_37;IntervalDuration=900_1800_3600;BlockDuration=Daily; HistoryLength=34128000;SubscriptionFrequency=Daily; AccountCollection=5;BR=1;",
                    subscription_id = 2,
                    third_party = "third_party"
                },
                new authorizations
                {
                    id = 92,
                    application_information_id = 26,
                    access_token = "token13",
                    scope =
                        "FB=1_3_4_5_13_14_18_37;IntervalDuration=900_1800_3600;BlockDuration=Daily; HistoryLength=34128000;SubscriptionFrequency=Daily; AccountCollection=5;BR=1;",
                    subscription_id = 3,
                    third_party = "third_party"
                },
                new authorizations
                {
                    id = 90,
                    application_information_id = 26,
                    access_token = "token14",
                    scope =
                        "FB=1_3_4_5_13_14_18_37;IntervalDuration=900_1800_3600;BlockDuration=Daily; HistoryLength=34128000;SubscriptionFrequency=Daily; AccountCollection=5;BR=1;",
                    subscription_id = 4,
                    third_party = "third_party"
                },
                new authorizations
                {
                    id = 93,
                    application_information_id = 26,
                    access_token = "token15",
                    scope =
                        "FB=1_3_4_5_13_14_18_37;IntervalDuration=900_1800_3600;BlockDuration=Daily; HistoryLength=34128000;SubscriptionFrequency=Daily; AccountCollection=5;BR=1;",
                    subscription_id = 5,
                    third_party = "third_party"
                },
                new authorizations
                {
                    id = 94,
                    application_information_id = 26,
                    access_token = "token11000",
                    scope =
                        "FB=1_3_4_5_13_14_18_37;IntervalDuration=900_1800_3600;BlockDuration=Daily; HistoryLength=34128000;SubscriptionFrequency=Daily; AccountCollection=5;BR=1;",
                    subscription_id = 1000,
                    third_party = "third_party"
                },
                new authorizations
                {
                    id = 95,
                    application_information_id = 26,
                    access_token = "token2",
                    scope =
                        "FB=1_3_5_13_14_37;IntervalDuration=900_1800_3600;BlockDuration=Daily; HistoryLength=34128000;SubscriptionFrequency=Daily; AccountCollection=5;BR=1;",
                    subscription_id = 1,
                    third_party = "third_party"
                },
                new authorizations
                {
                    id = 96,
                    application_information_id = 26,
                    access_token = "token6",
                    scope =
                        "FB=1_3_5_13_14_18_37;IntervalDuration=900_1800_3600;BlockDuration=Daily; HistoryLength=34128000;SubscriptionFrequency=Daily; AccountCollection=5;BR=1;",
                    subscription_id = 1,
                    third_party = "third_party"
                },
                new authorizations
                {
                    id = 97,
                    application_information_id = 26,
                    access_token = "token7",
                    scope =
                        "FB=1_3_4_5_13_14_18;IntervalDuration=900_1800_3600;BlockDuration=Daily; HistoryLength=34128000;SubscriptionFrequency=Daily; AccountCollection=5;BR=1;",
                    subscription_id = 1,
                    third_party = "third_party"
                },
                new authorizations
                {
                    id = 98,
                    application_information_id = 25,
                    access_token = "token8",
                    scope =
                        "FB=1_3_4_5_13_14_18_37;IntervalDuration=900_1800;BlockDuration=Daily; HistoryLength=34128000;SubscriptionFrequency=Daily; AccountCollection=5;BR=1;",
                    subscription_id = 3,
                    third_party = "third_party"

                },
                new authorizations
                {
                    id = 99,
                    application_information_id = 25,
                    access_token = "token85",
                    scope =
                        "FB=1_3_4_5_13_14_18_37;IntervalDuration=900_1800;BlockDuration=Daily; HistoryLength=34128000;SubscriptionFrequency=Daily; AccountCollection=5;BR=1;",
                    subscription_id = 5,
                    third_party = "third_party"

                },new authorizations
                {
                    id = 100,
                    application_information_id = 24,
                    access_token = "token9",
                    scope =
                        "FB=1_3_4_5_13_14_18_37;IntervalDuration=900_1800_3600;BlockDuration=Daily; HistoryLength=34128000;SubscriptionFrequency=Daily; AccountCollection=5;BR=1;",
                    subscription_id = 6,
                    third_party = "third_party"
                },new authorizations // no subscription
                {
                    id = 101,
                    application_information_id = 28,
                    access_token = "token10",
                    scope =
                        "FB=1_3_4_5_13_14_18_37;IntervalDuration=900_1800_3600;BlockDuration=Daily; HistoryLength=34128000;SubscriptionFrequency=Daily; AccountCollection=5;BR=1;",
                    subscription_id = 1000,
                    third_party = "third_party"
                },
                new authorizations
                {
                    id = 103,
                    application_information_id = 27,
                    access_token = "token11",
                    scope =
                        "FB=1_3_5_13_14_37;IntervalDuration=900_1800_3600;BlockDuration=Daily; HistoryLength=34128000;SubscriptionFrequency=Daily; AccountCollection=5;BR=1;",
                    subscription_id = 1,
                    third_party = "third_party"
                },new authorizations
                {
                    id = 104,
                    application_information_id = 29,
                    access_token = GenerateAccessToken("client_id,secret_singleSubEncrypted"),
                    scope =
                        "FB=1_3_4_5_13_14_18_37;IntervalDuration=900_1800_3600;BlockDuration=Daily; HistoryLength=34128000;SubscriptionFrequency=Daily; AccountCollection=5;BR=1;",
                    subscription_id = 6,
                    third_party = "third_party"
                },new authorizations
                {
                    id = 105,
                    application_information_id = 29,
                    access_token = GenerateAccessToken("client_id,secret_singleSubEncrypted,clientToken"),
                    scope =
                        "FB=1_3_4_5_13_14_18_37;IntervalDuration=900_1800_3600;BlockDuration=Daily; HistoryLength=34128000;SubscriptionFrequency=Daily; AccountCollection=5;BR=1;",
                    subscription_id = 6,
                    third_party = "third_party_admin"
                },new authorizations
                {
                    id = 90,
                    application_information_id = 30,
                    access_token = GenerateAccessToken("client_id,secretEncrypted"),
                    scope =
                        "FB=1_3_4_5_13_14_18_37;IntervalDuration=900_1800_3600;BlockDuration=Daily; HistoryLength=34128000;SubscriptionFrequency=Daily; AccountCollection=5;BR=1;",
                    subscription_id = 1,
                    third_party = "third_party_admin"
                },new authorizations
                {
                    id = 90,
                    application_information_id = 31,
                    access_token = GenerateAccessToken("client_id,secretEncrypted2"),
                    scope =
                        "FB=1_3_4_5_13_14_18_37;IntervalDuration=900_1800_3600;BlockDuration=Daily; HistoryLength=34128000;SubscriptionFrequency=Daily; AccountCollection=5;BR=1;",
                    subscription_id = 2,
                    third_party = "third_party"
                },new authorizations
                {
                    id = 106,
                    application_information_id = 31,
                    access_token = GenerateAccessToken("client_id,secretRegistrationEncrypted"),
                    scope =
                        "FB=1_3_4_5_13_14_18_37;IntervalDuration=900_1800_3600;BlockDuration=Daily; HistoryLength=34128000;SubscriptionFrequency=Daily; AccountCollection=5;BR=1;",
                    subscription_id = 2,
                    third_party = "registration_third_party",
                    end_date = DateTime.UtcNow.AddDays(10),
                    ap_start = GreenButtonConnect.Helper.ConvertToUnixTimestamp(DateTime.UtcNow),
                    ap_duration = 10,
                    status = 1
                },new authorizations
                {
                    id = 107,
                    application_information_id = 32,
                    access_token = GenerateAccessToken("client_id,secretRegistrationEncryptedInPast"),
                    scope =
                        "FB=1_3_4_5_13_14_18_37;IntervalDuration=900_1800_3600;BlockDuration=Daily; HistoryLength=34128000;SubscriptionFrequency=Daily; AccountCollection=5;BR=1;",
                    subscription_id = 2,
                    third_party = "registration_third_party",
                    end_date = DateTime.UtcNow.AddDays(-10),
                    status = 1

                },new authorizations
                {
                    id = 108,
                    application_information_id = 33,
                    access_token = GenerateAccessToken("client_id,secretRegistrationEncryptedInvalidDuration"),
                    scope =
                        "FB=1_3_4_5_13_14_18_37;IntervalDuration=900_1800_3600;BlockDuration=Daily; HistoryLength=34128000;SubscriptionFrequency=Daily; AccountCollection=5;BR=1;",
                    subscription_id = 2,
                    third_party = "registration_third_party",
                    end_date = DateTime.UtcNow.AddDays(10),
                    ap_start = GreenButtonConnect.Helper.ConvertToUnixTimestamp(DateTime.UtcNow.Date.AddDays(-20)),
                    ap_duration = 10,
                    status = 1
                },new authorizations
                {
                    id = 109,
                    application_information_id = 34,
                    access_token = GenerateAccessToken("client_id,secretRegistrationEncryptedInactive"),
                    scope =
                        "FB=1_3_4_5_13_14_18_37;IntervalDuration=900_1800_3600;BlockDuration=Daily; HistoryLength=34128000;SubscriptionFrequency=Daily; AccountCollection=5;BR=1;",
                    subscription_id = 2,
                    third_party = "registration_third_party",
                    end_date = DateTime.UtcNow.AddDays(10),
                    ap_start = GreenButtonConnect.Helper.ConvertToUnixTimestamp(DateTime.UtcNow),
                    ap_duration = 10,
                    status = 4
                },
                new authorizations
                {
                    id = 6,
                    access_token = GenerateAccessToken("client_id,authorization1"),
                    scope =
                        "FB=1_3_4_5_13_14_18_37;IntervalDuration=900_1800_3600;BlockDuration=Daily; HistoryLength=34128000;SubscriptionFrequency=Daily; AccountCollection=5;BR=1;",
                    application_information_id = 1,
                    third_party = "third_party",
                    ap_start = GreenButtonConnect.Helper.ConvertToUnixTimestamp(DateTime.UtcNow),
                    ap_duration = 10,
                    status = 1,
                    expiresin = GreenButtonConnect.Helper.ConvertToUnixTimestamp(DateTime.UtcNow.AddDays(1)),
                    resourceURI = "/reourseUri/6",
                    authorization_uri = "/authorizationUri/6",
                    uuid = "Guid6",
                    self_link_href = "/selflink/6",
                    self_link_rel = "self",
                    up_link_href = "/uplink",
                    up_link_rel = "up"
                },
                new authorizations
                {
                    id = 7,
                    access_token = GenerateAccessToken("client_id,authorization2"),
                    scope =
                        "FB=1_3_4_5_13_14_18_37;IntervalDuration=900_1800_3600;BlockDuration=Daily; HistoryLength=34128000;SubscriptionFrequency=Daily; AccountCollection=5;BR=1;",
                    application_information_id = 1,
                    third_party = "third_party",
                    refresh_token = "refreshToken",
                    ap_start = GreenButtonConnect.Helper.ConvertToUnixTimestamp(DateTime.UtcNow),
                    ap_duration = 10,
                    status = 1,
                    expiresin = GreenButtonConnect.Helper.ConvertToUnixTimestamp(DateTime.UtcNow.AddDays(1)),
                    resourceURI = "/reourseUri/7",
                    authorization_uri = "/authorizationUri/7",
                    uuid = "Guid7",
                    self_link_href = "/selflink/7",
                    self_link_rel = "self",
                    up_link_href = "/uplink",
                    up_link_rel = "up"
                },
                new authorizations
                {
                    id = 8,
                    access_token = GenerateAccessToken("client_id,authorization3"),
                    scope =
                        "FB=1_3_4_5_13_14_18_37;IntervalDuration=900_1800_3600;BlockDuration=Daily; HistoryLength=34128000;SubscriptionFrequency=Daily; AccountCollection=5;BR=1;",
                    application_information_id = 1,
                    third_party = "third_party_admin",
                    ap_start = GreenButtonConnect.Helper.ConvertToUnixTimestamp(DateTime.UtcNow),
                    ap_duration = 10,
                    status = 1,
                    expiresin = GreenButtonConnect.Helper.ConvertToUnixTimestamp(DateTime.UtcNow.AddDays(1)),
                    resourceURI = "/reourseUri/8",
                    authorization_uri = "/authorizationUri/8",
                    uuid = "Guid8",
                    self_link_href = "/selflink/8",
                    self_link_rel = "self",
                    up_link_href = "/uplink",
                    up_link_rel = "up"
                },
                new authorizations
                {
                    id = 9,
                    access_token = GenerateAccessToken("client_id,authorization4"),
                    scope =
                        "FB=1_3_4_5_13_14_18_37;IntervalDuration=900_1800_3600;BlockDuration=Daily; HistoryLength=34128000;SubscriptionFrequency=Daily; AccountCollection=5;BR=1;",
                    application_information_id = 1,
                    third_party = "third_party",
                    refresh_token = "refreshToken",
                    ap_start = GreenButtonConnect.Helper.ConvertToUnixTimestamp(DateTime.UtcNow),
                    ap_duration = 10,
                    status = 2,
                    expiresin = GreenButtonConnect.Helper.ConvertToUnixTimestamp(DateTime.UtcNow.AddDays(1)),
                    resourceURI = "/reourseUri/9",
                    authorization_uri = "/authorizationUri/9",
                    uuid = "Guid9",
                    self_link_href = "/selflink/9",
                    self_link_rel = "self",
                    up_link_href = "/uplink",
                    up_link_rel = "up"
                },
                new authorizations
                {
                    id = 10,
                    access_token = GenerateAccessToken("client_id,authorization5"),
                    scope =
                        "FB=1_3_4_5_13_14_18_37;IntervalDuration=900_1800_3600;BlockDuration=Daily; HistoryLength=34128000;SubscriptionFrequency=Daily; AccountCollection=5;BR=1;",
                    application_information_id = 1,
                    third_party = "third_party",
                    refresh_token = "refreshToken",
                    ap_start = GreenButtonConnect.Helper.ConvertToUnixTimestamp(DateTime.UtcNow),
                    ap_duration = 10,
                    status = 3,
                    expiresin = GreenButtonConnect.Helper.ConvertToUnixTimestamp(DateTime.UtcNow.AddDays(1)),
                    resourceURI = "/reourseUri/10",
                    authorization_uri = "/authorizationUri/10",
                    uuid = "Guid10",
                    self_link_href = "/selflink/10",
                    self_link_rel = "self",
                    up_link_href = "/uplink",
                    up_link_rel = "up"
                },
                new authorizations
                {
                    id = 11,
                    access_token = GenerateAccessToken("client_id,authorization6"),
                    scope =
                        "FB=1_3_4_5_13_14_18_37;IntervalDuration=900_1800_3600;BlockDuration=Daily; HistoryLength=34128000;SubscriptionFrequency=Daily; AccountCollection=5;BR=1;",
                    application_information_id = 1,
                    third_party = "third_party",
                    refresh_token = "refreshToken",
                    ap_start = GreenButtonConnect.Helper.ConvertToUnixTimestamp(DateTime.UtcNow),
                    ap_duration = 10,
                    status = 4,
                    expiresin = GreenButtonConnect.Helper.ConvertToUnixTimestamp(DateTime.UtcNow.AddDays(1)),
                    resourceURI = "/reourseUri/11",
                    authorization_uri = "/authorizationUri/11",
                    uuid = "Guid11",
                    self_link_href = "/selflink/11",
                    self_link_rel = "self",
                    up_link_href = "/uplink",
                    up_link_rel = "up"
                },
                new authorizations
                {
                    id = 12,
                    subscription_id = 7,
                    access_token = "tokenDaily",
                    scope =
                        "FB=1_3_4_5_13_14_18_37;IntervalDuration=900_1800_3600_86400;BlockDuration=Daily; HistoryLength=34128000;SubscriptionFrequency=Daily; AccountCollection=5;BR=1;",
                    application_information_id = 35,
                    third_party = "third_party",
                    ap_start = GreenButtonConnect.Helper.ConvertToUnixTimestamp(DateTime.UtcNow),
                    ap_duration = 10,
                    status = 4,
                    expiresin = GreenButtonConnect.Helper.ConvertToUnixTimestamp(DateTime.UtcNow.AddDays(1)),
                    resourceURI = "/reourseUri/12",
                    authorization_uri = "/authorizationUri/12",
                    uuid = "Guid12",
                    self_link_href = "/selflink/12",
                    self_link_rel = "self",
                    up_link_href = "/uplink",
                    up_link_rel = "up"
                }
            };



            return authorizationsMock;
        }


        private string GenerateAccessToken(string plainText)
        {
            //var plainText = "client_id,secret";
            var enrypted = GreenButtonConnect.Helper.Encrypt(plainText);
            var plainTextBytes = Encoding.UTF8.GetBytes(enrypted);
            return Convert.ToBase64String(plainTextBytes);
        }

        private void ApplicationInfoDbSetMock()
        {
            var applicationInfoDbSetMock = new Mock<DbSet<application_information>>();

            var applicationInfoMock = MockApplicationInfoDbs().AsQueryable();

            applicationInfoDbSetMock.As<IQueryable<application_information>>()
                .Setup(m => m.Provider)
                .Returns(applicationInfoMock.Provider);

            applicationInfoDbSetMock.As<IQueryable<application_information>>()
                .Setup(m => m.Expression)
                .Returns(applicationInfoMock.Expression);

            applicationInfoDbSetMock.As<IQueryable<application_information>>()
                .Setup(m => m.ElementType)
                .Returns(applicationInfoMock.ElementType);

            applicationInfoDbSetMock.As<IQueryable<application_information>>()
                .Setup(m => m.GetEnumerator())
                .Returns(applicationInfoMock.GetEnumerator());

            MockInsightsEntities.Setup(i => i.application_information).Returns(applicationInfoDbSetMock.Object);
        }

        private List<application_information> MockApplicationInfoDbs()
        {

            var plainTextBytes = Encoding.UTF8.GetBytes("87:dev");
            var dataCustotianId = Convert.ToBase64String(plainTextBytes);
            var applicationInfosMock = new List<application_information>
            {new application_information
                {
                    application_information_id = 30,
                    client_secret = "secretEncrypted",
                    client_id = "client_id",
                    dataCustodianId = dataCustotianId,
                    dataCustodianApplicationStatus = 1,
                    client_name = "client_name",
                    thirdPartyScopeSelectionScreenURI = "/thirdPartyScopeSelectionScreenURI",
                    thirdPartyNotifyUri = "/thirdPartyNotifyUri",
                    uuid = "GuidId30",
                    self_link_href = "/selflink",
                    self_link_rel = "self",
                    up_link_href = "/uplink",
                    up_link_rel = "up",
                    dataCustodianBulkRequestURI = "/dataCustodianBulkRequestURI",
                    dataCustodianResourceEndpoint = "/dataCustodianResourceEndpoint",
                    redirect_uri = "/redirectUrl",
                    updated = DateTime.UtcNow,
                    published = DateTime.UtcNow
                },
                new application_information
                {
                    application_information_id = 26,
                    client_secret = "secret",
                    client_id = "client_id",
                    dataCustodianId = dataCustotianId,
                    dataCustodianApplicationStatus = 1,
                    client_name = "client_name",
                    thirdPartyScopeSelectionScreenURI = "/thirdPartyScopeSelectionScreenURI",
                    thirdPartyNotifyUri = "/thirdPartyNotifyUri",
                    uuid = "GuidId26",
                    self_link_href = "/selflink",
                    self_link_rel = "self",
                    up_link_href = "/uplink",
                    up_link_rel = "up",
                    dataCustodianBulkRequestURI = "/dataCustodianBulkRequestURI",
                    dataCustodianResourceEndpoint = "/dataCustodianResourceEndpoint",
                    redirect_uri = "/redirectUrl",
                    updated = DateTime.UtcNow,
                    published = DateTime.UtcNow
                },
                 new application_information
                {
                    application_information_id = 25,
                    client_secret = "secret8",
                    client_id = "client_id8",
                    dataCustodianId = dataCustotianId,
                    dataCustodianApplicationStatus = 1,
                    client_name = "client_name",
                    thirdPartyScopeSelectionScreenURI = "/thirdPartyScopeSelectionScreenURI",
                    thirdPartyNotifyUri = "/thirdPartyNotifyUri",
                    uuid = "GuidId25",
                    self_link_href = "/selflink",
                    self_link_rel = "self",
                    up_link_href = "/uplink",
                    up_link_rel = "up",
                    dataCustodianBulkRequestURI = "/dataCustodianBulkRequestURI",
                    dataCustodianResourceEndpoint = "/dataCustodianResourceEndpoint",
                    redirect_uri = "/redirectUrl",
                    updated = DateTime.UtcNow,
                    published = DateTime.UtcNow
                },
                 new application_information
                {
                    application_information_id = 24,
                    client_secret = "secret_singleSub",
                    client_id = "client_id",
                    dataCustodianId = dataCustotianId,
                    dataCustodianApplicationStatus = 1,
                    client_name = "client_name",
                    thirdPartyScopeSelectionScreenURI = "/thirdPartyScopeSelectionScreenURI",
                    thirdPartyNotifyUri = "/thirdPartyNotifyUri",
                    uuid = "GuidId24",
                    self_link_href = "/selflink",
                    self_link_rel = "self",
                    up_link_href = "/uplink",
                    up_link_rel = "up",
                    dataCustodianBulkRequestURI = "/dataCustodianBulkRequestURI",
                    dataCustodianResourceEndpoint = "/dataCustodianResourceEndpoint",
                    redirect_uri = "/redirectUrl",
                    updated = DateTime.UtcNow,
                    published = DateTime.UtcNow
                },
                 new application_information
                {
                    application_information_id = 28,
                    client_secret = "secret_noSub",
                    client_id = "client_id",
                    dataCustodianId = dataCustotianId,
                    dataCustodianApplicationStatus = 1,
                    client_name = "client_name",
                    thirdPartyScopeSelectionScreenURI = "/thirdPartyScopeSelectionScreenURI",
                    thirdPartyNotifyUri = "/thirdPartyNotifyUri",
                    uuid = "GuidId28",
                    self_link_href = "/selflink",
                    self_link_rel = "self",
                    up_link_href = "/uplink",
                    up_link_rel = "up",
                    dataCustodianBulkRequestURI = "/dataCustodianBulkRequestURI",
                    dataCustodianResourceEndpoint = "/dataCustodianResourceEndpoint",
                    redirect_uri = "/redirectUrl",
                    updated = DateTime.UtcNow,
                    published = DateTime.UtcNow
                },
                 new application_information
                {
                    application_information_id = 27,
                    client_secret = "secret_SingleUsagePoint",
                    client_id = "client_id",
                    dataCustodianId = dataCustotianId,
                    dataCustodianApplicationStatus = 1,
                    client_name = "client_name",
                    thirdPartyScopeSelectionScreenURI = "/thirdPartyScopeSelectionScreenURI",
                    thirdPartyNotifyUri = "/thirdPartyNotifyUri",
                    uuid = "GuidId27",
                    self_link_href = "/selflink",
                    self_link_rel = "self",
                    up_link_href = "/uplink",
                    up_link_rel = "up",
                    dataCustodianBulkRequestURI = "/dataCustodianBulkRequestURI",
                    dataCustodianResourceEndpoint = "/dataCustodianResourceEndpoint",
                    redirect_uri = "/redirectUrl",
                    updated = DateTime.UtcNow,
                    published = DateTime.UtcNow
                },
                 new application_information
                {
                    application_information_id = 29,
                    client_secret = "secret_singleSubEncrypted",
                    client_id = "client_id",
                    dataCustodianId = dataCustotianId,
                    dataCustodianApplicationStatus = 1,
                    client_name = "client_name",
                    thirdPartyScopeSelectionScreenURI = "/thirdPartyScopeSelectionScreenURI",
                    thirdPartyNotifyUri = "/thirdPartyNotifyUri",
                    uuid = "GuidId29",
                    self_link_href = "/selflink",
                    self_link_rel = "self",
                    up_link_href = "/uplink",
                    up_link_rel = "up",
                    dataCustodianBulkRequestURI = "/dataCustodianBulkRequestURI",
                    dataCustodianResourceEndpoint = "/dataCustodianResourceEndpoint",
                    redirect_uri = "/redirectUrl",
                    updated = DateTime.UtcNow,
                    published = DateTime.UtcNow
                },
                 new application_information
                {
                    application_information_id = 31,
                    client_secret = "secretRegistrationEncrypted",
                    client_id = "client_id",
                    dataCustodianId = dataCustotianId,
                    dataCustodianApplicationStatus = 1,
                    client_name = "client_name",
                    thirdPartyScopeSelectionScreenURI = "/thirdPartyScopeSelectionScreenURI",
                    thirdPartyNotifyUri = "/thirdPartyNotifyUri",
                    uuid = "GuidId31",
                    self_link_href = "/selflink",
                    self_link_rel = "self",
                    up_link_href = "/uplink",
                    up_link_rel = "up",
                    dataCustodianBulkRequestURI = "/dataCustodianBulkRequestURI",
                    dataCustodianResourceEndpoint = "/dataCustodianResourceEndpoint",
                    redirect_uri = "/redirectUrl",
                    updated = DateTime.UtcNow,
                    published = DateTime.UtcNow
                },
                 new application_information
                {
                    application_information_id = 31,
                    client_secret = "secretEncrypted2",
                    client_id = "client_id",
                    dataCustodianId = dataCustotianId,
                    dataCustodianApplicationStatus = 1,
                    client_name = "client_name",
                    thirdPartyScopeSelectionScreenURI = "/thirdPartyScopeSelectionScreenURI",
                    thirdPartyNotifyUri = "/thirdPartyNotifyUri",
                    uuid = "GuidId31",
                    self_link_href = "/selflink",
                    self_link_rel = "self",
                    up_link_href = "/uplink",
                    up_link_rel = "up",
                    dataCustodianBulkRequestURI = "/dataCustodianBulkRequestURI",
                    dataCustodianResourceEndpoint = "/dataCustodianResourceEndpoint",
                    redirect_uri = "/redirectUrl",
                    updated = DateTime.UtcNow,
                    published = DateTime.UtcNow
                },
                 new application_information
                {
                    application_information_id = 32,
                    client_secret = "secretRegistrationEncryptedInPast",
                    client_id = "client_id",
                    dataCustodianId = dataCustotianId,
                    dataCustodianApplicationStatus = 1,
                    client_name = "client_name",
                    thirdPartyScopeSelectionScreenURI = "/thirdPartyScopeSelectionScreenURI",
                    thirdPartyNotifyUri = "/thirdPartyNotifyUri",
                    uuid = "GuidId32",
                    self_link_href = "/selflink",
                    self_link_rel = "self",
                    up_link_href = "/uplink",
                    up_link_rel = "up",
                    dataCustodianBulkRequestURI = "/dataCustodianBulkRequestURI",
                    dataCustodianResourceEndpoint = "/dataCustodianResourceEndpoint",
                    redirect_uri = "/redirectUrl",
                    updated = DateTime.UtcNow,
                    published = DateTime.UtcNow
                },
                 new application_information
                {
                    application_information_id = 33,
                    client_secret = "secretRegistrationEncryptedInvalidDuration",
                    client_id = "client_id",
                    dataCustodianId = dataCustotianId,
                    dataCustodianApplicationStatus = 1,
                    client_name = "client_name",
                    thirdPartyScopeSelectionScreenURI = "/thirdPartyScopeSelectionScreenURI",
                    thirdPartyNotifyUri = "/thirdPartyNotifyUri",
                    uuid = "GuidId33",
                    self_link_href = "/selflink",
                    self_link_rel = "self",
                    up_link_href = "/uplink",
                    up_link_rel = "up",
                    dataCustodianBulkRequestURI = "/dataCustodianBulkRequestURI",
                    dataCustodianResourceEndpoint = "/dataCustodianResourceEndpoint",
                    redirect_uri = "/redirectUrl",
                    updated = DateTime.UtcNow,
                    published = DateTime.UtcNow
                },
                 new application_information
                {
                    application_information_id = 34,
                    client_secret = "secretRegistrationEncryptedInactive",
                    client_id = "client_id",
                    dataCustodianId = dataCustotianId,
                    dataCustodianApplicationStatus = 1,
                    client_name = "client_name",
                    thirdPartyScopeSelectionScreenURI = "/thirdPartyScopeSelectionScreenURI",
                    thirdPartyNotifyUri = "/thirdPartyNotifyUri",
                    uuid = "GuidId34",
                    self_link_href = "/selflink",
                    self_link_rel = "self",
                    up_link_href = "/uplink",
                    up_link_rel = "up",
                    dataCustodianBulkRequestURI = "/dataCustodianBulkRequestURI",
                    dataCustodianResourceEndpoint = "/dataCustodianResourceEndpoint",
                    redirect_uri = "/redirectUrl",
                    updated = DateTime.UtcNow,
                    published = DateTime.UtcNow
                },
                 new application_information
                {
                    application_information_id = 1,
                    client_secret = "authorization3",
                    client_id = "client_id",
                    dataCustodianId = dataCustotianId
                },
                 new application_information
                {
                    application_information_id = 2,
                    client_secret = "noAuthorization",
                    client_id = "client_id",
                    dataCustodianId = dataCustotianId
                },
                new application_information
                {
                    application_information_id = 35,
                    client_secret = "dailyAuthorization",
                    client_id = "client_id",
                    dataCustodianId = dataCustotianId
                }
            };



            return applicationInfosMock;
        }

        private void ApplicationInfoScopeDbSetMock()
        {
            var applicationInfoScopeDbSetMock = new Mock<DbSet<application_information_scopes>>();

            var applicationInfoScopeMock = MockApplicationInfoScopeDbs().AsQueryable();

            applicationInfoScopeDbSetMock.As<IQueryable<application_information_scopes>>()
                .Setup(m => m.Provider)
                .Returns(applicationInfoScopeMock.Provider);

            applicationInfoScopeDbSetMock.As<IQueryable<application_information_scopes>>()
                .Setup(m => m.Expression)
                .Returns(applicationInfoScopeMock.Expression);

            applicationInfoScopeDbSetMock.As<IQueryable<application_information_scopes>>()
                .Setup(m => m.ElementType)
                .Returns(applicationInfoScopeMock.ElementType);

            applicationInfoScopeDbSetMock.As<IQueryable<application_information_scopes>>()
                .Setup(m => m.GetEnumerator())
                .Returns(applicationInfoScopeMock.GetEnumerator());

            MockInsightsEntities.Setup(i => i.application_information_scopes).Returns(applicationInfoScopeDbSetMock.Object);
        }

        private List<application_information_scopes> MockApplicationInfoScopeDbs()
        {
            var applicationInfoScopesMock = new List<application_information_scopes>
            {
                new application_information_scopes
                {
                    application_information_id = 29,
                    scope = "FB=1_3_4_5_13_14_18_37;IntervalDuration=900_1800_3600;BlockDuration=Daily; HistoryLength=34128000;SubscriptionFrequency=Daily; AccountCollection=5;BR=1;"
                },new application_information_scopes
                {
                    application_information_id = 31,
                    scope = "FB=1_3_4_5_13_14_18_37;IntervalDuration=900_1800_3600;BlockDuration=Daily; HistoryLength=34128000;SubscriptionFrequency=Daily; AccountCollection=5;BR=1;"
                },new application_information_scopes
                {
                    application_information_id = 35,
                    scope = "FB=1_3_4_5_13_14_18_37;IntervalDuration=900_1800_3600_86400;BlockDuration=Daily; HistoryLength=34128000;SubscriptionFrequency=Daily; AccountCollection=5;BR=1;"
                }
            };

            return applicationInfoScopesMock;
        }
    }
    
}
