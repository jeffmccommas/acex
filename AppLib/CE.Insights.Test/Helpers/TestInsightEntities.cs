﻿using System.Data.Entity;
using CE.Models.Insights.EF;

namespace CE.Insights.Test.Helpers
{
    public partial class TestInsightEntities: InsightsEntities, IInsightsEntities
    {

         public TestInsightEntities()
        {
            this.Endpoints = new TestEndpointsDbSet();
            this.EndpointTrackings = new TestEndpointTrackingsDbSet();
            this.Clients = new TestClientsDbSet();
            this.Users = new TestUsersDbSet();
            this.ProfileCustomerAccounts = new TestProfileCustomerAccountsDbSet();
            this.ProfileCustomerAttributes = new TestProfileCustomerAttributesDbSet();
            this.ProfileAccountAttributes = new TestProfileAccountAttributesDbSet();
            this.ProfilePremiseAttributes = new TestProfilePremiseAttributesDbSet();
            this.ActionCustomerAccount = new TestActionCustomerAccountsDbSet();
            this.ActionPremisePlan = new TestActionPremisePlansDbSet();


        }
         public override DbSet<Endpoint> Endpoints { get; set; }
         public override DbSet<EndpointTracking> EndpointTrackings { get; set; }
         public override DbSet<Client> Clients { get; set; }
         public override DbSet<User> Users { get; set; }
         public override  DbSet<ProfileAccountAttribute> ProfileAccountAttributes { get; set; }
         public override DbSet<ProfilePremiseAttribute> ProfilePremiseAttributes { get; set; }
         public override DbSet<ProfileCustomerAccount> ProfileCustomerAccounts { get; set; }
         public override DbSet<ProfileCustomerAttribute> ProfileCustomerAttributes { get; set; }
         public override DbSet<ActionCustomerAccount> ActionCustomerAccount { get; set; }
         public override DbSet<ActionPremisePlan> ActionPremisePlan { get; set; }
    }
}
