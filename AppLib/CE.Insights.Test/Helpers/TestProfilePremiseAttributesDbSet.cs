﻿using System.Linq;
using CE.Models.Insights.EF;

namespace CE.Insights.Test.Helpers
{
    class TestProfilePremiseAttributesDbSet : TestDbSet<ProfilePremiseAttribute>
    {
        public override ProfilePremiseAttribute Find(params object[] keyValues)
        {
            return this.SingleOrDefault(ProfilePremiseAttribute => ProfilePremiseAttribute.AttributeKey == (string)keyValues.Single());
        }
    }
}
