﻿using System.Linq;
using CE.Models.Insights.EF;

namespace CE.Insights.Test.Helpers
{
    class TestProfileAccountAttributesDbSet : TestDbSet<ProfileAccountAttribute>
    {
        public override ProfileAccountAttribute Find(params object[] keyValues)
        {
            return this.SingleOrDefault(ProfileAccountAttribute => ProfileAccountAttribute.AttributeKey == (string)keyValues.Single());
        }
    }
}
