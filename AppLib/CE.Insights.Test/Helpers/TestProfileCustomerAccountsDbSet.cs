﻿using System.Linq;
using CE.Models.Insights.EF;

namespace CE.Insights.Test.Helpers
{
    class TestProfileCustomerAccountsDbSet : TestDbSet<ProfileCustomerAccount>
    {
        public override ProfileCustomerAccount Find(params object[] keyValues)
        {
            return this.SingleOrDefault(ProfileCustomerAccount => ProfileCustomerAccount.AccountID == (string)keyValues.Single());
        }

    }
}
