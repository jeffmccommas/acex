﻿using System.Linq;
using CE.Models.Insights.EF;

namespace CE.Insights.Test.Helpers
{
    class TestUsersDbSet : TestDbSet<User>
    {
        public override User Find(params object[] keyValues)
        {
            return this.SingleOrDefault(User => User.UserID == (int)keyValues.Single());
        }

    }
}
