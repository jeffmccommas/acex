﻿using System.Linq;
using CE.Models.Insights.EF;

namespace CE.Insights.Test.Helpers
{
    class TestProfileCustomerAttributesDbSet : TestDbSet<ProfileCustomerAttribute>
    {
        public override ProfileCustomerAttribute Find(params object[] keyValues)
        {
            return this.SingleOrDefault(ProfileCustomerAttribute => ProfileCustomerAttribute.AttributeKey == (string)keyValues.Single());
        }
    }
}
