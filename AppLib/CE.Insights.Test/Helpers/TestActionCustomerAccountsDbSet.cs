﻿using System.Linq;
using CE.Models.Insights.EF;

namespace CE.Insights.Test.Helpers
{
    class TestActionCustomerAccountsDbSet : TestDbSet<ActionCustomerAccount>
    {
        public override ActionCustomerAccount Find(params object[] keyValues)
        {
            return this.SingleOrDefault(ActionCustomerAccount => ActionCustomerAccount.AccountID == (string)keyValues.Single());
        }
    }
}