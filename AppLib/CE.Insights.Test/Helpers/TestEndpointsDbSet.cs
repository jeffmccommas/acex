﻿using System.Linq;
using CE.Models.Insights.EF;

namespace CE.Insights.Test.Helpers
{
    class TestEndpointsDbSet : TestDbSet<Endpoint>
    {
        public override Endpoint Find(params object[] keyValues)
        {
            return this.SingleOrDefault(Endpoint => Endpoint.EndpointID == (int)keyValues.Single());
        }

    }
}
