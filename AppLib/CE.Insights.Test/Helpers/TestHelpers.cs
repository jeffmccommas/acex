﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Transactions;
using AO.Entities;
using CE.AO.Models;
using CE.BillToDate;
using CE.ContentModel.Entities;
using CE.Models;
using CE.Models.Insights;
using CE.Models.Insights.EF;
using CE.Models.Insights.Types;
using CE.RateModel;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;
using CM = CE.ContentModel;
using UnitOfMeasureType = CE.Models.Insights.Types.UnitOfMeasureType;

namespace CE.Insights.Test.Helpers
{
    /// <summary>
    /// Used for controller and other helper methods on unit tests
    /// </summary>

    public static class TestHelpers
    {
        // ReSharper disable once InconsistentNaming
        public static List<CM.ClientProfileAttributeDefault> createmockProfileAttributes()
        {
            var result = new List<CM.ClientProfileAttributeDefault>();
            var profileattribute = new CM.ClientProfileAttributeDefault
            {
                AttributeKey = "pool.size",
                ConditionKeys = null,
                DoNotDefault = true,
                EntityLevel = "premise"
            };

            result.Add(profileattribute);
            var profileattribute1 = new CM.ClientProfileAttributeDefault
            {
                AttributeKey = "pool",
                ConditionKeys = null,
                DoNotDefault = true,
                EntityLevel = "premise"
            };
            result.Add(profileattribute1);
            return result;
        }
        // ReSharper disable once InconsistentNaming
        public static ProfilePostResponse createRepoProfile()
        {
            var response = new ProfilePostResponse { Message = "OK" };
            return response;
        }


        /// <summary>
        /// Inserts profile data into the insights dw for test of get profile. Assumes data does not exist in the Insights database and the data from insights dw 
        /// is pulled into (inserted) into insights database for profile attributes for that customer
        /// </summary>
        /// <param name="clientId">ClientId being used for data insert</param>
        /// <param name="seeddata"></param>
        /// <returns>false if insert has error, true if insert is successful</returns>
        // ReSharper disable once InconsistentNaming
        public static int InsertInsightsDWProfileData(int clientId, string seeddata)
        {
            var retcode = 0;

            //declare the transaction options
            var transactionOptions = new TransactionOptions
            {
                IsolationLevel = IsolationLevel.ReadUncommitted
            };

            //set it to read uncommited
            //create the transaction scope, passing our options in            
            using (var transactionScope = new TransactionScope(TransactionScopeOption.Required, transactionOptions))
            {
                using (var ent = new InsightsDWEntities(ConnStringBuilder.GetEFConnectionStringInsightsDW(clientId)))
                {
                    ent.Database.Connection.Open();
                    var dbContextTransaction = ent.Database.BeginTransaction();

                    try
                    {
                        ent.Configuration.AutoDetectChangesEnabled = false;
                        ent.Configuration.ValidateOnSaveEnabled = false;

                        //create dimpremise
                        const string sqlDimPremise =
                            "INSERT INTO dbo.DimPremise(PostalCodeKey,CityKey,SourceKey,AccountId,PremiseId,Street1,City,StateProvince,Country,PostalCode,ClientId,SourceId) VALUES(@PostalCodeKey,@CityKey,@SourceKey,@AccountId,@PremiseId,@Street1,@City,@StateProvince,@Country,@PostalCode,@ClientId,@SourceId)";
                        var parameterList = new List<object>
                        {
                            new SqlParameter("@PostalCodeKey", "-1"),
                            new SqlParameter("@CityKey", "17227"),
                            new SqlParameter("@SourceKey", "8"),
                            new SqlParameter("@AccountId", seeddata),
                            new SqlParameter("@PremiseId", seeddata),
                            new SqlParameter("@Street1", seeddata + " StreetName"),
                            new SqlParameter("@City", "BELL"),
                            new SqlParameter("@StateProvince", "CA"),
                            new SqlParameter("@Country", "US"),
                            new SqlParameter("@PostalCode", "902012514"),
                            new SqlParameter("@ClientId", clientId),
                            new SqlParameter("@SourceId", 8)
                        };
                        var parameters = parameterList.ToArray();
                        var result = ent.Database.ExecuteSqlCommand(sqlDimPremise, parameters);
                        retcode += result;

                        const string sqlDimCustomer =
                            "INSERT INTO dbo.DimCustomer(ClientKey,ClientId,PostalCodeKey,CustomerAuthenticationTypeKey,SourceKey,CityKey,CustomerId,FirstName,LastName,Street1,City,StateProvince,Country,PostalCode,SourceId) VALUES(@ClientKey,@ClientId,@PostalCodeKey,@CustomerAuthenticationTypeKey,@SourceKey,@CityKey,@CustomerId,@FirstName,@LastName,@Street1,@City,@StateProvince,@Country,@PostalCode,@SourceId)";
                        var parameterList2 = new List<object>
                        {
                            new SqlParameter("@ClientKey", 14),
                            new SqlParameter("@ClientId", clientId),
                            new SqlParameter("@PostalCodeKey", "-1"),
                            new SqlParameter("@CustomerAuthenticationTypeKey", "1"),
                            new SqlParameter("@SourceKey", "8"),
                            new SqlParameter("@CityKey", "-1"),
                            new SqlParameter("@CustomerId", seeddata),
                            new SqlParameter("@FirstName", seeddata + " Fname"),
                            new SqlParameter("@LastName", seeddata + " LName"),
                            new SqlParameter("@Street1", seeddata + " StreetName"),
                            new SqlParameter("@City", "BELL"),
                            new SqlParameter("@StateProvince", "CA"),
                            new SqlParameter("@Country", "US"),
                            new SqlParameter("@PostalCode", "902012514"),
                            new SqlParameter("@SourceId", 8)
                        };
                        var parameters2 = parameterList2.ToArray();
                        var result2 = ent.Database.ExecuteSqlCommand(sqlDimCustomer, parameters2);
                        retcode += result2;

                        const string sqlFactCustPrem =
                            "INSERT INTO dbo.FactCustomerPremise(CustomerKey,PremiseKey) (SELECT TOP 1 c.CustomerKey , p.PremiseKey FROM DimCustomer c INNER JOIN DimClient ct WITH (NOLOCK) ON ct.ClientId = c.ClientId INNER JOIN DimPremise p WITH (NOLOCK) ON ct.ClientId = p.ClientId WHERE c.CustomerID = @custid AND p.PremiseID = @premid and ct.ClientId = @clientid )";
                        var parameterList3 = new List<object>
                        {
                            new SqlParameter("@custid", seeddata),
                            new SqlParameter("@premid", seeddata),
                            new SqlParameter("@clientid", clientId)
                        };
                        var parameters3 = parameterList3.ToArray();
                        var result3 = ent.Database.ExecuteSqlCommand(sqlFactCustPrem, parameters3);
                        retcode += result3;


                        //insert one profile attribute, we will test that it pulls back into the insights database next
                        const string sqlPremiseAttribute =
                            "INSERT INTO dbo.FactPremiseAttribute(premisekey,OptionKey,SourceKey,ContentAttributeKey,ContentOptionKey,AttributeId,OptionId,Value,ClientId,PremiseId,AccountId,SourceId,ETLLogId,TrackingId,TrackingDate,EffectiveDate,EffectiveDateKey) ( SELECT (SELECT TOP 1 PremiseKey FROM InsightsDW.dbo.DimPremise dp WITH (NOLOCK) WHERE dp.clientId =  @clientid AND dp.PremiseId = @premid) AS PremiseKey, OptionKey,8 AS SourceKey, CMSAttributeKey AS ContentAttributeKey, CMSOptionKey AS ContentOptionKey, AttributeId, OptionId, 2239 AS Value, @clientid2, @premid2, @accountid, 8 AS SourceID,   2609 AS EtlLogID,'20140926120555' AS TrackingID, '2014-11-20 13:15:48.000' AS TrackingDate, '2014-11-20 18:33:13.333', '20141120' FROM InsightsDW.dbo.DimPremiseOption dpo WITH (NOLOCK) WHERE CMSAttributeKey = 'house.totalarea')";
                        var parameterList4 = new List<object>
                        {
                            new SqlParameter("@clientid", clientId),
                            new SqlParameter("@premid", seeddata),
                            new SqlParameter("@clientid2", clientId),
                            new SqlParameter("@premid2", seeddata),
                            new SqlParameter("@accountid", seeddata)
                        };
                        var parameters4 = parameterList4.ToArray();
                        var result4 = ent.Database.ExecuteSqlCommand(sqlPremiseAttribute, parameters4);
                        retcode += result4;

                        //save and commit as a whole transaction
                        ent.SaveChanges();
                        dbContextTransaction.Commit();
                    }

                    catch (Exception)
                    {
                        dbContextTransaction.Rollback();
                    }
                }
                transactionScope.Complete();
            }
            return retcode;

        }
        // ReSharper disable once InconsistentNaming
        public static int InsertInsightsDWActionPlanData(int clientId, string seeddata)
        {
            var retcode = 0;

            //declare the transaction options
            //set it to read uncommited
            var transactionOptions = new TransactionOptions
            {
                IsolationLevel = IsolationLevel.ReadUncommitted
            };

            //create the transaction scope, passing our options in            
            using (var transactionScope = new TransactionScope(TransactionScopeOption.Required, transactionOptions))
            {
                using (var ent = new InsightsDWEntities(ConnStringBuilder.GetEFConnectionStringInsightsDW(clientId)))
                {
                    ent.Database.Connection.Open();
                    var dbContextTransaction = ent.Database.BeginTransaction();

                    try
                    {
                        ent.Configuration.AutoDetectChangesEnabled = false;
                        ent.Configuration.ValidateOnSaveEnabled = false;

                        //create dimpremise
                        const string sqlDimPremise = "INSERT INTO dbo.DimPremise(PostalCodeKey,CityKey,SourceKey,AccountId,PremiseId,Street1,City,StateProvince,Country,PostalCode,ClientId,SourceId) VALUES(@PostalCodeKey,@CityKey,@SourceKey,@AccountId,@PremiseId,@Street1,@City,@StateProvince,@Country,@PostalCode,@ClientId,@SourceId)";
                        var parameterList = new List<object>
                        {
                            new SqlParameter("@PostalCodeKey", "-1"),
                            new SqlParameter("@CityKey", "17227"),
                            new SqlParameter("@SourceKey", "8"),
                            new SqlParameter("@AccountId", seeddata),
                            new SqlParameter("@PremiseId", seeddata),
                            new SqlParameter("@Street1", seeddata + " StreetName"),
                            new SqlParameter("@City", "BELL"),
                            new SqlParameter("@StateProvince", "CA"),
                            new SqlParameter("@Country", "US"),
                            new SqlParameter("@PostalCode", "902012514"),
                            new SqlParameter("@ClientId", clientId),
                            new SqlParameter("@SourceId", 8)
                        };
                        var parameters = parameterList.ToArray();
                        var result = ent.Database.ExecuteSqlCommand(sqlDimPremise, parameters);
                        retcode += result;

                        const string sqlDimCustomer = "INSERT INTO dbo.DimCustomer(ClientKey,ClientId,PostalCodeKey,CustomerAuthenticationTypeKey,SourceKey,CityKey,CustomerId,FirstName,LastName,Street1,City,StateProvince,Country,PostalCode,SourceId) VALUES(@ClientKey,@ClientId,@PostalCodeKey,@CustomerAuthenticationTypeKey,@SourceKey,@CityKey,@CustomerId,@FirstName,@LastName,@Street1,@City,@StateProvince,@Country,@PostalCode,@SourceId)";
                        var parameterList2 = new List<object>
                        {
                            new SqlParameter("@ClientKey", 14),
                            new SqlParameter("@ClientId", clientId),
                            new SqlParameter("@PostalCodeKey", "-1"),
                            new SqlParameter("@CustomerAuthenticationTypeKey", "1"),
                            new SqlParameter("@SourceKey", "8"),
                            new SqlParameter("@CityKey", "-1"),
                            new SqlParameter("@CustomerId", seeddata),
                            new SqlParameter("@FirstName", seeddata + " Fname"),
                            new SqlParameter("@LastName", seeddata + " LName"),
                            new SqlParameter("@Street1", seeddata + " StreetName"),
                            new SqlParameter("@City", "BELL"),
                            new SqlParameter("@StateProvince", "CA"),
                            new SqlParameter("@Country", "US"),
                            new SqlParameter("@PostalCode", "902012514"),
                            new SqlParameter("@SourceId", 8)
                        };
                        var parameters2 = parameterList2.ToArray();
                        var result2 = ent.Database.ExecuteSqlCommand(sqlDimCustomer, parameters2);
                        retcode += result2;

                        const string sqlFactCustPrem = "INSERT INTO dbo.FactCustomerPremise(CustomerKey,PremiseKey) (SELECT TOP 1 c.CustomerKey , p.PremiseKey FROM DimCustomer c INNER JOIN DimClient ct WITH (NOLOCK) ON ct.ClientId = c.ClientId INNER JOIN DimPremise p WITH (NOLOCK) ON ct.ClientId = p.ClientId WHERE c.CustomerID = @custid AND p.PremiseID = @premid and ct.ClientId = @clientid )";
                        var parameterList3 = new List<object>
                        {
                            new SqlParameter("@custid", seeddata),
                            new SqlParameter("@premid", seeddata),
                            new SqlParameter("@clientid", clientId)
                        };
                        var parameters3 = parameterList3.ToArray();
                        int result3 = ent.Database.ExecuteSqlCommand(sqlFactCustPrem, parameters3);
                        retcode += result3;


                        //insert one action item, we will test that it pulls back into the insights database next
                        const string sqlPremiseAttribute = "INSERT INTO dbo.FactAction(premisekey,DateKey,ActionId,ActionKey,SubActionKey,StatusKey,StatusDate,ClientId,PremiseId,AccountId,SourceKey,CreateDate,ETLLogId,TrackingId,TrackingDate) ( SELECT (SELECT TOP 1 PremiseKey FROM InsightsDW.dbo.DimPremise dp WITH (NOLOCK) WHERE dp.clientId =  @clientid AND dp.PremiseId = @premid) AS PremiseKey, OptionKey,8 AS SourceKey, CMSAttributeKey AS ContentAttributeKey, CMSOptionKey AS ContentOptionKey, AttributeId, OptionId, 2239 AS Value, @clientid2, @premid2, @accountid, 8 AS SourceID,   2609 AS EtlLogID,'20140926120555' AS TrackingID, '2014-11-20 13:15:48.000' AS TrackingDate, '2014-11-20 18:33:13.333', '20141120' FROM InsightsDW.dbo.DimPremiseOption dpo WITH (NOLOCK) WHERE CMSAttributeKey = 'house.totalarea')";
                        var parameterList4 = new List<object>
                        {
                            new SqlParameter("@clientid", clientId),
                            new SqlParameter("@premid", seeddata),
                            new SqlParameter("@clientid2", clientId),
                            new SqlParameter("@premid2", seeddata),
                            new SqlParameter("@accountid", seeddata)
                        };
                        var parameters4 = parameterList4.ToArray();
                        int result4 = ent.Database.ExecuteSqlCommand(sqlPremiseAttribute, parameters4);
                        retcode += result4;

                        //save and commit as a whole transaction
                        ent.SaveChanges();
                        dbContextTransaction.Commit();
                    }

                    catch (Exception)
                    {
                        dbContextTransaction.Rollback();
                    }
                }
                transactionScope.Complete();
            }
            return retcode;

        }

        public static BillDisaggResponse CreateRepoDataBillDisagg1()
        {
            var response = new BillDisaggResponse
            {
                ClientId = 87,
                Customer = new Customer
                {
                    Id = "QACustomer1",
                    Accounts = new List<Account>
                    {
                        new Account
                        {
                            Id = "QAAccount1",
                            BillStartDate = DateTime.Parse("2014/01/03"),
                            BillEndDate = DateTime.Parse("2014/03/05"),
                            Premises = new List<Premise>
                            {
                                new Premise
                                {
                                    Id = "QAPremise1",
                                    EndUses = new List<EndUse>
                                    {
                                        new EndUse
                                        {
                                            Key = "cooking",
                                            DisaggDetails = new List<DisaggDetail>(),
                                            Appliances = new List<Appliance>
                                            {
                                                new Appliance
                                                {
                                                    Key = ((ApplianceType) 6).ToString().ToLower(),
                                                    DisaggDetails = new List<DisaggDetail>
                                                    {
                                                        new DisaggDetail
                                                        {
                                                            CommodityKey = ((CommodityType) 1).ToString().ToLower(),
                                                            UsageQuantity = 5.20000M,
                                                            UsageUOMKey = ((UnitOfMeasureType) 1).ToString().ToLower(),
                                                            CostAmount = 31.09M,
                                                            CostCurrencyKey = ((CurrencyType) 1).ToString().ToLower(),
                                                            Measurements = null
                                                        },
                                                        new DisaggDetail
                                                        {
                                                            CommodityKey = ((CommodityType) 2).ToString().ToLower(),
                                                            UsageQuantity = 1.25000M,
                                                            UsageUOMKey = ((UnitOfMeasureType) 2).ToString().ToLower(),
                                                            CostAmount = 2.09M,
                                                            CostCurrencyKey = ((CurrencyType) 1).ToString().ToLower(),
                                                            Measurements = null
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            };

            return response;
        }

        public static List<DynamicTableEntity> CreateMockAzureAmiDailyTouData(int clientId, string accountId,
         string meterId, string servicePointId, DateTime startDate, DateTime endDate, DateTime projected,
         int commodityType = 1, int unitOfMeasurement = 0)
        {
            var readings = new List<DynamicTableEntity>();
            var daysOfData = (endDate - startDate).TotalDays + 1;

            for (var j = 0; j < daysOfData; j++)
            {
                var reading = new DynamicTableEntity();
                var date = startDate.AddDays(j);
                reading.Properties.Add("AccountNumber", new EntityProperty(accountId));
                reading.Properties.Add("AmiTimeStamp", new EntityProperty(date));
                reading.Properties.Add("ClientId", new EntityProperty(clientId));
                reading.Properties.Add("CommodityId", new EntityProperty(commodityType));
                reading.Properties.Add("IntervalType", new EntityProperty(24));
                reading.Properties.Add("MeterId", new EntityProperty(meterId));
                reading.Properties.Add("ProjectedReadDate", new EntityProperty(projected));
                reading.Properties.Add("ServicePointId", new EntityProperty(servicePointId));
                reading.Properties.Add("UOMId", new EntityProperty(unitOfMeasurement));
                reading.Properties.Add("TOU_OnPeak", new EntityProperty(15d));
                reading.Properties.Add("TOU_OffPeak", new EntityProperty(15d));
                reading.Properties.Add("TOU_CriticalPeak", new EntityProperty(15d));
                reading.Properties.Add("TOU_Regular", new EntityProperty(10d));
                reading.Properties.Add("TOU_Shoulder1", new EntityProperty(22d));
                reading.Properties.Add("TOU_Shoulder2", new EntityProperty(7d));
                reading.Timestamp = date;
                readings.Add(reading);
            }
            return readings;
        }

        public static List<DynamicTableEntity> CreateMockAzureAmiDailyNoTouData(int clientId, string accountId,
          string meterId, string servicePointId, DateTime startDate, DateTime endDate, DateTime projected,
          int commodityType = 1, int unitOfMeasurement = 0)
        {
            var readings = new List<DynamicTableEntity>();
            var daysOfData = (endDate - startDate).TotalDays + 1;

            for (var j = 0; j < daysOfData; j++)
            {
                var reading = new DynamicTableEntity();
                var date = startDate.AddDays(j);
                reading.Properties.Add("AccountNumber", new EntityProperty(accountId));
                reading.Properties.Add("AmiTimeStamp", new EntityProperty(date));
                reading.Properties.Add("ClientId", new EntityProperty(clientId));
                reading.Properties.Add("CommodityId", new EntityProperty(commodityType));
                reading.Properties.Add("IntervalType", new EntityProperty(24));
                reading.Properties.Add("MeterId", new EntityProperty(meterId));
                reading.Properties.Add("ProjectedReadDate", new EntityProperty(projected));
                reading.Properties.Add("ServicePointId", new EntityProperty(servicePointId));
                reading.Properties.Add("UOMId", new EntityProperty(unitOfMeasurement));
                reading.Properties.Add("TOU_Regular", new EntityProperty(10d));
                reading.Timestamp = date;
                readings.Add(reading);
            }
            return readings;
        }

        public static IList<ConsumptionByMeterEntity> CreateMockAzureAmiHourlyData(int clientId, string accountId,
            string meterId, string servicePointId, DateTime startDate, DateTime endDate, DateTime projected,
            int commodityType = 1, int unitOfMeasurement = 0)
        {
            IList<ConsumptionByMeterEntity> readings = new List<ConsumptionByMeterEntity>();
            var daysOfData = (endDate - startDate).TotalDays + 1;
            for (var j = 0; j < daysOfData; j++)
            {
                var date = startDate.AddDays(j);
                for (var i = 0; i < 24; i++)
                {
                    var reading = new ConsumptionByMeterEntity();
                    reading.AccountNumber = accountId;
                    reading.AmiTimeStamp = date;
                    reading.ClientId = clientId;
                    reading.CommodityId = commodityType;
                    //reading.DailyConsumption = Convert.ToDouble(24);
                    reading.IntervalType = 60;
                    reading.MeterId = meterId;
                    reading.ProjectedReadDate = projected;
                    reading.ServicePointId = servicePointId;
                    reading.UOMId = unitOfMeasurement;
                    var min = 60 * i;
                    reading.AmiTimeStamp = date.AddMinutes(min);
                    readings.Add(reading);
                }
            }
            return readings;
        }

    
        public static IList<ConsumptionByMeterEntity> CreateMockAzureAmi15DataEntities(int clientId, string accountId, string meterId, string servicePointId, DateTime startDate, DateTime endDate, DateTime projected)
        {
            IList<ConsumptionByMeterEntity> readings = new List<ConsumptionByMeterEntity>();
            var daysOfData = (endDate - startDate).TotalDays + 1;
            for (int j = 0; j < daysOfData; j++){
                var date = startDate.AddDays(j);
                for (var i = 0; i < 24; i++)
                {
                    for (var x = 0; x < 4; x++)
                    {
                        var reading = new ConsumptionByMeterEntity();
                        reading.AccountNumber = accountId;
                        reading.ClientId = clientId;
                        reading.CommodityId = 1;
                        reading.IntervalType = 15;
                        reading.MeterId = meterId;
                        reading.ProjectedReadDate = projected;
                        reading.ServicePointId = servicePointId;
                        reading.UOMId = 0;
                        var min = (15*x) + (60 *i);
                        reading.TOU_Regular = Convert.ToDouble(1);
                        reading.AmiTimeStamp = date.AddMinutes(min);
                        readings.Add(reading);
                    }
                }

            }
            return readings;
        }


        public static List<TallAmiModel> CreateMockAmi15AmiModel(int clientId, string accountId, string meterId, string servicePointId, DateTime startDate, DateTime endDate, DateTime projected)
        {
            List<TallAmiModel> readings = new List<TallAmiModel>();
            var daysOfData = (endDate - startDate).TotalDays + 1;
            for (int j = 0; j < daysOfData; j++)
            {
                var date = startDate.AddDays(j);
                for (var i = 0; i < 24; i++)
                {
                    for (var x = 0; x < 4; x++)
                    {
                        var reading = new TallAmiModel();
                        reading.AccountNumber = accountId;
                        reading.ClientId = clientId;
                        reading.CommodityId = 1;
                        reading.IntervalType = 15;
                        reading.MeterId = meterId;
                        reading.ProjectedReadDate = projected;
                        reading.ServicePointId = servicePointId;
                        reading.UOMId = 0;
                        reading.Timezone = "EST";
                        var min = (15 * x) + (60 * i);
                        reading.TOU_Regular = Convert.ToDouble(1);
                        reading.AmiTimeStamp = date.AddMinutes(min);
                        readings.Add(reading);
                    }
                }

            }

            return readings.OrderByDescending(r => r.AmiTimeStamp).ToList();
        }

        public static List<Reading> CreateMockAmi15Reading(DateTime startDate, DateTime endDate)
        {
            List<Reading> readings = new List<Reading>();
            var daysOfData = (endDate - startDate).TotalDays + 1;
            for (int j = 0; j < daysOfData; j++)
            {
                var date = startDate.AddDays(j);
                for (var i = 0; i < 24; i++)
                {
                    for (var x = 0; x < 4; x++)
                    {
                        var min = (15 * x) + (60 * i);
                        var reading = new Reading(date.AddMinutes(min), Convert.ToDouble(1));
                        readings.Add(reading);
                    }
                }

            }
            return readings.OrderByDescending(r => r.Timestamp).ToList();
        }

        public static List<TallAmiModel> CreateMockAmiHourlyModel(int clientId, string accountId,
            string meterId, string servicePointId, DateTime startDate, DateTime endDate, DateTime projected,
            int commodityType = 1, int unitOfMeasurement = 0)
        {
            List<TallAmiModel> readings = new List<TallAmiModel>();
            var daysOfData = (endDate - startDate).TotalDays + 1;
            for (var j = 0; j < daysOfData; j++)
            {
                var date = startDate.AddDays(j);
                for (var i = 0; i < 24; i++)
                {
                    var reading = new TallAmiModel();
                    reading.AccountNumber = accountId;
                    reading.AmiTimeStamp = date;
                    reading.ClientId = clientId;
                    reading.CommodityId = commodityType;
                    reading.IntervalType = 60;
                    reading.MeterId = meterId;
                    reading.ProjectedReadDate = projected;
                    reading.ServicePointId = servicePointId;
                    reading.UOMId = unitOfMeasurement;
                    reading.Timezone = "EST";
                    reading.Consumption = Convert.ToDouble(1);
                    var min = 60 * i;
                    reading.AmiTimeStamp = date.AddMinutes(min);
                    readings.Add(reading);
                }
            }
            return readings.OrderByDescending(r => r.AmiTimeStamp).ToList();
        }

        public static List<Reading> CreateMockAmiHourlyReading(DateTime startDate, DateTime endDate)
        {
            List<Reading> readings = new List<Reading>();
            var daysOfData = (endDate - startDate).TotalDays + 1;
            for (var j = 0; j < daysOfData; j++)
            {
                var date = startDate.AddDays(j);
                for (var i = 0; i < 24; i++)
                {
                    var min = 60 * i;
                    var reading = new Reading(date.AddMinutes(min), Convert.ToDouble(1));
                    readings.Add(reading);
                }
            }
            return readings.OrderByDescending(r => r.Timestamp).ToList();
        }

        public static List<Reading> CreateMockAmiDailyReading(DateTime startDate, DateTime endDate)
        {
            List<Reading> readings = new List<Reading>();
            var daysOfData = (endDate - startDate).TotalDays + 1;
            for (var j = 0; j < daysOfData; j++)
            {
                var date = startDate.AddDays(j);
                var reading = new Reading(date, Convert.ToDouble(1));
                readings.Add(reading);
            }
            return readings.OrderByDescending(r => r.Timestamp).ToList();
        }

        public static List<Reading> CreateMockAmiMonthlyReading(DateTime startDate, DateTime endDate)
        {
            List<Reading> readings = new List<Reading>();

            var numOfMonths = endDate.Month - startDate.Month + 1;
            if (endDate.Year != startDate.Year)
            {
                var numOfYears = endDate.Year - startDate.Year;
                numOfMonths = (numOfYears*12) + numOfMonths;
            }
            for (var j = 0; j < numOfMonths; j++)
            {
                var date = startDate.AddMonths(j);
                var timestamp = new DateTime(date.Year, date.Month, 1, 0, 0, 0, DateTimeKind.Local);
                var reading = new Reading(timestamp, Convert.ToDouble(30));
                readings.Add(reading);
            }

            return readings.OrderByDescending(r => r.Timestamp).ToList();
        }
        public static List<Reading> CreateMockAmi30Reading(DateTime startDate, DateTime endDate)
        {
            List<Reading> readings = new List<Reading>();
            var daysOfData = (endDate - startDate).TotalDays + 1;
            for (var j = 0; j < daysOfData; j++)
            {
                var date = startDate.AddDays(j);
                for (var i = 0; i < 48; i++)
                {
                    var min = 30 * i;
                    var reading = new Reading(date.AddMinutes(min), Convert.ToDouble(1));
                    readings.Add(reading);
                }
            }
            return readings.OrderByDescending(r => r.Timestamp).ToList();
        }

        public static List<TallAmiModel> CreateMockAmi30Model(int clientId, string accountId, string meterId, string servicePointId, DateTime startDate, DateTime endDate, DateTime projected)
        {
            List<TallAmiModel> readings = new List<TallAmiModel>();
            var daysOfData = (endDate - startDate).TotalDays + 1;
            for (int j = 0; j < daysOfData; j++)
            {
                var date = startDate.AddDays(j);
                for (var i = 0; i < 24; i++)
                {
                    for (var x = 0; x < 2; x++)
                    {
                        var reading = new TallAmiModel();
                        reading.AccountNumber = accountId;
                        reading.ClientId = clientId;
                        reading.CommodityId = 1;
                        reading.IntervalType = 30;
                        reading.MeterId = meterId;
                        reading.ProjectedReadDate = projected;
                        reading.ServicePointId = servicePointId;
                        reading.UOMId = 0;
                        reading.Timezone = "EST";
                        var min = (30 * x) + (60 * i);
                        reading.TOU_Regular = Convert.ToDouble(1);
                        reading.AmiTimeStamp = date.AddMinutes(min);
                        readings.Add(reading);
                    }
                }

            }

            return readings.OrderByDescending(r => r.AmiTimeStamp).ToList();
        }


        public static CustomerEntity CreateMockResidentialCustomerEntityData(int clientId, string customerId, string firstname, string lastname,
            string addr1, string addr2, string city, string state, string zip, string email, string phone = "781-694-3200")
        {
            var customerEntity = new CustomerEntity
            {
                CustomerId = customerId,
                FirstName = firstname,
                LastName = lastname,
                AddressLine1 = addr1,
                AddressLine2 = addr2,
                City = city,
                State = state,
                PostalCode = zip,
                ClientId = clientId,
                CustomerType = "Residential",
                EmailAddress = email,
                IsBusiness = false,
                Phone1 = phone
            };

            return customerEntity;
        }

        public static PremiseEntity CreateMockPremiseEntityData(int clientId, string customerId,
            string premiseId, string accountId, string addr1, string addr2, string city, string state, string zip)
        {
            var premiseEntity = new PremiseEntity
            {
                CustomerId = customerId,
                AddressLine1 = addr1,
                AddressLine2 = addr2,
                City = city,
                State = state,
                PostalCode = zip,
                ClientId = clientId,
                AccountId = accountId,
                Country = "US",
                PremiseId = premiseId

            };

            return premiseEntity;
        }

        public static BillingEntity CreateMockBillingEntityData(int clientId, string customerId, string accountId, string premiseId,
           string serviceContractId, string serviceId, string meterId, string billCycle, string rateClass, double usage,
           DateTime start, DateTime end, int commodityId = 1)
        {
            var days = Convert.ToInt32((end - start).TotalDays + 1);

            // billing info
            var billingEntity = new BillingEntity
            {
                ClientId = clientId,
                CustomerId = customerId,
                AccountId = accountId,
                PremiseId = premiseId,
                ServicePointId = serviceId,
                ServiceContractId = serviceContractId,
                BillCycleScheduleId = billCycle,
                RateClass = rateClass,
                BillDays = days,
                BillPeriodType = 1,
                EndDate = end,
                MeterId = meterId,
                MeterType = "AMI",
                ReadDate = end,
                ReadQuality = "Estimated",
                Source = "Utility",
                StartDate = start,
                TotalCost = 144,
                TotalUsage = usage,
                UOMId = 0,
                CommodityId = commodityId
            };

            return billingEntity;
        }

        public static SubscriptionEntity CreateMockSubscriptionEntityData(int clientId, string customerId, string accountId, string serviceContractId,
            string programName, string insightName, bool isSelected, double threshold, bool emailDoubleOptInCompleted = false, bool smsDoubleOptInCompleted = false)
        {
            if (!string.IsNullOrEmpty(programName) && !string.IsNullOrEmpty(insightName))
            {
                emailDoubleOptInCompleted = true;
                smsDoubleOptInCompleted = true;
            }

            var subscriptionEntity = new SubscriptionEntity
            {
                ClientId = clientId,
                AccountId = accountId,
                Channel = "Email",
                CustomerId = customerId,
                InsightTypeName = insightName,
                IsSelected = isSelected,
                ProgramName = programName,
                ServiceContractId = serviceContractId,
                SubscriptionDate = DateTime.Today.AddDays(-1),
                IsEmailOptInCompleted = emailDoubleOptInCompleted,
                IsSmsOptInCompleted = smsDoubleOptInCompleted,
                IsLatest =  true
            };

            if (!threshold.Equals(0))
            {
                subscriptionEntity.SubscriberThresholdValue = threshold;
            }

            return subscriptionEntity;
        }

        public static string CreateMockDefaultSubscriptionString()
        {
            const string settings = @"{    'ClientId': 87,    'TimeZone': 'EST',    'IsAmiIntervalStart': false,    'IsDstHandled': false,    'CalculationScheduleTime': '',    'NotificationReportScheduleTime': '05:00',    'InsightReportScheduleTime': '06:00',    'ForgotPasswordEmailTemplateId': 'af5c0251-486f-4100-a4cf-2d91a905f108',    'RegistrationConfirmationEmailTemplateId': '7c0657cd-1020-42a4-a281-87db82a7f6c4',    'OptInEmailConfirmationTemplateId': '777ca9f8-bb03-490b-946b-b1e6991b2b15',    'OptInSmsConfirmationTemplateId': '87_12',    'OptInEmailUserName': 'aclaradev',    'OptInEmailPassword': 'Xaclaradev2311X',    'OptInEmailFrom': 'support@aclarax.com',    'TrumpiaShortCode': '99000',    'TrumpiaApiKey': 'b4c1971153b3509b6ec0d8a24a33454c',    'TrumpiaUserName': 'aclaradev',    'TrumpiaContactList': 'AclaraDemoDev',    'UnmaskedAccountIdEndingDigit': '4',    'Programs': [      {        'ProgramName': 'Program1',        'UtilityProgramName': 'Program1',        'FileExportRequired': true,        'DoubleOptInRequired': true,        'TrumpiaKeyword': 'abagdasarian2',        'Insights': [          {            'InsightName': 'BillToDate',            'UtilityInsightName': 'BillToDate',            'AllowedCommunicationChannel': 'File,Email,SMS,EmailandSMS',            'DefaultCommunicationChannel': 'Email',            'DefaultFrequency': 'Weekly',            'DefaultValue': '',            'EmailTemplateId': 'c8d5e21f-1150-4b3d-b4b8-ad869d89a040',            'SMSTemplateId': '87_1',            'NotifyTime': '20:00',            'NotificationDay': 'Saturday',            'TrumpiaKeyword': '',            'Level': 'Account',            'CommodityType': '',            'ThresholdType': '',            'ThresholdMin': '',            'ThresholdMax': ''          },          {            'InsightName': 'AccountProjectedCost',            'UtilityInsightName': 'AccountProjectedCost',            'AllowedCommunicationChannel': 'File,Email,SMS,EmailandSMS',            'DefaultCommunicationChannel': 'Email',            'DefaultFrequency': 'Daily',            'DefaultValue': '',            'EmailTemplateId': '7367b661-0dfd-4eb2-a0d8-0735a459fcb5',            'SMSTemplateId': '87_3',            'NotifyTime': '20:00',            'NotificationDay': 'Monday',            'TrumpiaKeyword': '',            'Level': 'Account',            'CommodityType': '',            'ThresholdType': '',            'ThresholdMin': '',            'ThresholdMax': ''          },          {            'InsightName': 'AccountLevelCostThreshold',            'UtilityInsightName': 'AccountLevelCostThreshold',            'AllowedCommunicationChannel': 'File,Email,SMS,EmailandSMS',            'DefaultCommunicationChannel': 'Email',            'DefaultFrequency': 'Immediate',            'DefaultValue': '100',            'EmailTemplateId': '7b3f9981-ae52-4fd9-b223-8ae516f6f82f',            'SMSTemplateId': '87_6',            'NotifyTime': '',            'NotificationDay': '',            'TrumpiaKeyword': '',            'Level': 'Account',            'CommodityType': '',            'ThresholdType': 'Cost',            'ThresholdMin': '1',            'ThresholdMax': '1000000'          },          {            'InsightName': 'ServiceLevelCostThreshold',            'UtilityInsightName': 'ServiceLevelCostThreshold',            'AllowedCommunicationChannel': 'File,Email,SMS,EmailandSMS',            'DefaultCommunicationChannel': 'Email',            'DefaultFrequency': 'Immediate',            'DefaultValue': '25,25',            'EmailTemplateId': '87791eaa-0342-45d5-85d4-4645f7a85f7d',            'SMSTemplateId': '87_8',            'NotifyTime': '',            'NotificationDay': '',            'TrumpiaKeyword': '',            'Level': 'Service',            'CommodityType': 'Gas,Electric',            'ThresholdType': 'Cost',            'ThresholdMin': '1',            'ThresholdMax': '1000000'          },          {            'InsightName': 'ServiceLevelUsageThreshold',            'UtilityInsightName': 'ServiceLevelUsageThreshold',            'AllowedCommunicationChannel': 'File,Email,SMS,EmailandSMS',            'DefaultCommunicationChannel': 'Email',            'DefaultFrequency': 'Immediate',            'DefaultValue': '25,500',            'EmailTemplateId': 'f7791cc2-9665-4d4b-8a7f-9421daf55e29',            'SMSTemplateId': '87_7',            'NotifyTime': '',            'NotificationDay': '',            'TrumpiaKeyword': '',            'Level': 'Service',            'CommodityType': 'Gas,Electric',            'ThresholdType': 'Usage',            'ThresholdMin': '0.1',            'ThresholdMax': '1000000'          },          {            'InsightName': 'CostToDate',            'UtilityInsightName': 'CostToDate',            'AllowedCommunicationChannel': 'File,Email,SMS,EmailandSMS',            'DefaultCommunicationChannel': 'Email',            'DefaultFrequency': 'Weekly',            'DefaultValue': '',            'EmailTemplateId': 'b4425b3d-de12-4694-8751-edb41bc5cd79',            'SMSTemplateId': '87_2',            'NotifyTime': '20:00',            'NotificationDay': 'Monday',            'TrumpiaKeyword': '',            'Level': 'Service',            'CommodityType': 'Gas,Electric',            'ThresholdType': '',            'ThresholdMin': '',            'ThresholdMax': ''          },          {            'InsightName': 'ServiceProjectedCost',            'UtilityInsightName': 'ServiceProjectedCost',            'AllowedCommunicationChannel': 'File,Email,SMS,EmailandSMS',            'DefaultCommunicationChannel': 'Email',            'DefaultFrequency': 'Daily',            'DefaultValue': '',            'EmailTemplateId': '6bd40b25-fc86-480f-a1d4-0f2a22b7d270',            'SMSTemplateId': '87_4',            'NotifyTime': '20:00',            'NotificationDay': 'Monday',            'TrumpiaKeyword': '',            'Level': 'Service',            'CommodityType': 'Gas,Electric',            'ThresholdType': '',            'ThresholdMin': '',            'ThresholdMax': ''          },          {            'InsightName': 'Usage',            'UtilityInsightName': 'Usage',            'AllowedCommunicationChannel': 'File,Email,SMS,EmailandSMS',            'DefaultCommunicationChannel': 'Email',            'DefaultFrequency': 'Daily',            'DefaultValue': '',            'EmailTemplateId': '3fdb3336-0192-4bb9-bc53-8810dd110a3a',            'SMSTemplateId': '87_5',            'NotifyTime': '20:00',            'NotificationDay': 'Monday',            'TrumpiaKeyword': '',            'Level': 'Service',            'CommodityType': 'Gas,Electric',            'ThresholdType': '',            'ThresholdMin': '',            'ThresholdMax': ''          },          {            'InsightName': 'DayThreshold',            'UtilityInsightName': 'DayThreshold',            'AllowedCommunicationChannel': 'File,Email,SMS,EmailandSMS',            'DefaultCommunicationChannel': 'Email',            'DefaultFrequency': 'Immediate',            'DefaultValue': '1,16',            'EmailTemplateId': 'ce508747-444b-4e1c-9f39-53629e47260b',            'SMSTemplateId': '87_9',            'NotifyTime': '',            'NotificationDay': '',            'TrumpiaKeyword': '',            'Level': 'Service',            'CommodityType': 'Gas,Electric',            'ThresholdType': 'Usage',            'ThresholdMin': '0.1',            'ThresholdMax': '1000000'          },          {            'InsightName': 'ServiceLevelTieredThresholdApproaching',            'UtilityInsightName': 'ServiceLevelTieredThresholdApproaching',            'AllowedCommunicationChannel': 'File,Email,SMS,EmailandSMS',            'DefaultCommunicationChannel': 'Email',            'DefaultFrequency': 'Immediate',            'DefaultValue': '20,20',            'EmailTemplateId': '4962e788-fb1e-43cf-a2f6-faf713d25e0c',            'SMSTemplateId': '87_10',            'NotifyTime': '',            'NotificationDay': '',            'TrumpiaKeyword': '',            'Level': 'Service',            'CommodityType': 'Gas,Electric',            'ThresholdType': 'Percent',            'ThresholdMin': '1',            'ThresholdMax': '100'          },          {            'InsightName': 'ServiceLevelTieredThresholdExceed',            'UtilityInsightName': 'ServiceLevelTieredThresholdExceed',            'AllowedCommunicationChannel': 'File,Email,SMS,EmailandSMS',            'DefaultCommunicationChannel': 'Email',            'DefaultFrequency': 'Immediate',            'DefaultValue': '',            'EmailTemplateId': 'c1dc511c-0dd7-4208-bad7-8729fa097194',            'SMSTemplateId': '87_11',            'NotifyTime': '',            'NotificationDay': '',            'TrumpiaKeyword': '',            'Level': 'Service',            'CommodityType': 'Gas,Electric',            'ThresholdType': '',            'ThresholdMin': '',            'ThresholdMax': ''          }        ]      },      {        'ProgramName': 'Program2',        'UtilityProgramName': 'Program2',        'FileExportRequired': false,        'DoubleOptInRequired': true,        'TrumpiaKeyword': '',        'Insights': [          {            'InsightName': 'ServiceLevelCostThreshold',            'UtilityInsightName': 'ServiceLevelCostThreshold',            'AllowedCommunicationChannel': 'File,Email,SMS,EmailandSMS',            'DefaultCommunicationChannel': 'Email',            'DefaultFrequency': 'Immediate',            'DefaultValue': '25',            'EmailTemplateId': '87791eaa-0342-45d5-85d4-4645f7a85f7d',            'SMSTemplateId': '87_8',            'NotifyTime': '',            'NotificationDay': '',            'TrumpiaKeyword': '',            'Level': 'Service',            'CommodityType': 'Water',            'ThresholdType': 'Cost',            'ThresholdMin': '1',            'ThresholdMax': '1000000'          },          {            'InsightName': 'ServiceLevelUsageThreshold',            'UtilityInsightName': 'ServiceLevelUsageThreshold',            'AllowedCommunicationChannel': 'File,Email,SMS,EmailandSMS',            'DefaultCommunicationChannel': 'Email',            'DefaultFrequency': 'Immediate',            'DefaultValue': '5',            'EmailTemplateId': 'f7791cc2-9665-4d4b-8a7f-9421daf55e29',            'SMSTemplateId': '87_7',            'NotifyTime': '',            'NotificationDay': '',            'TrumpiaKeyword': '',            'Level': 'Service',            'CommodityType': 'Water',            'ThresholdType': 'Usage',            'ThresholdMin': '0.1',            'ThresholdMax': '1000000'          },          {            'InsightName': 'CostToDate',            'UtilityInsightName': 'CostToDate',            'AllowedCommunicationChannel': 'File,Email,SMS,EmailandSMS',            'DefaultCommunicationChannel': 'Email',            'DefaultFrequency': 'Weekly',            'DefaultValue': '',            'EmailTemplateId': 'b4425b3d-de12-4694-8751-edb41bc5cd79',            'SMSTemplateId': '87_2',            'NotifyTime': '20:00',            'NotificationDay': 'Monday',            'TrumpiaKeyword': '',            'Level': 'Service',            'CommodityType': 'Water',            'ThresholdType': '',            'ThresholdMin': '',            'ThresholdMax': ''          },          {            'InsightName': 'ServiceProjectedCost',            'UtilityInsightName': 'ServiceProjectedCost',            'AllowedCommunicationChannel': 'File,Email,SMS,EmailandSMS',            'DefaultCommunicationChannel': 'Email',            'DefaultFrequency': 'Daily',            'DefaultValue': '',            'EmailTemplateId': '6bd40b25-fc86-480f-a1d4-0f2a22b7d270',            'SMSTemplateId': '87_4',            'NotifyTime': '20:00',            'NotificationDay': 'Monday',            'TrumpiaKeyword': '',            'Level': 'Service',            'CommodityType': 'Water',            'ThresholdType': '',            'ThresholdMin': '',            'ThresholdMax': ''          },          {            'InsightName': 'Usage',            'UtilityInsightName': 'Usage',            'AllowedCommunicationChannel': 'File,Email,SMS,EmailandSMS',            'DefaultCommunicationChannel': 'Email',            'DefaultFrequency': 'Daily',            'DefaultValue': '',            'EmailTemplateId': '3fdb3336-0192-4bb9-bc53-8810dd110a3a',            'SMSTemplateId': '87_5',            'NotifyTime': '20:00',            'NotificationDay': 'Monday',            'TrumpiaKeyword': '',            'Level': 'Service',            'CommodityType': 'Water',            'ThresholdType': '',            'ThresholdMin': '',            'ThresholdMax': ''          },          {            'InsightName': 'DayThreshold',            'UtilityInsightName': 'DayThreshold',            'AllowedCommunicationChannel': 'File,Email,SMS,EmailandSMS',            'DefaultCommunicationChannel': 'Email',            'DefaultFrequency': 'Immediate',            'DefaultValue': '0.15',            'EmailTemplateId': 'ce508747-444b-4e1c-9f39-53629e47260b',            'SMSTemplateId': '87_9',            'NotifyTime': '',            'NotificationDay': '',            'TrumpiaKeyword': '',            'Level': 'Service',            'CommodityType': 'Water',            'ThresholdType': 'Usage',            'ThresholdMin': '0.1',            'ThresholdMax': '1000000'          },          {            'InsightName': 'ServiceLevelTieredThresholdApproaching',            'UtilityInsightName': 'ServiceLevelTieredThresholdApproaching',            'AllowedCommunicationChannel': 'File,Email,SMS,EmailandSMS',            'DefaultCommunicationChannel': 'Email',            'DefaultFrequency': 'Immediate',            'DefaultValue': '20',            'EmailTemplateId': '4962e788-fb1e-43cf-a2f6-faf713d25e0c',            'SMSTemplateId': '87_10',            'NotifyTime': '',            'NotificationDay': '',            'TrumpiaKeyword': '',            'Level': 'Service',            'CommodityType': 'Water',            'ThresholdType': 'Percent',            'ThresholdMin': '1',            'ThresholdMax': '100'          },          {            'InsightName': 'ServiceLevelTieredThresholdExceed',            'UtilityInsightName': 'ServiceLevelTieredThresholdExceed',            'AllowedCommunicationChannel': 'File,Email,SMS,EmailandSMS',            'DefaultCommunicationChannel': 'Email',            'DefaultFrequency': 'Immediate',            'DefaultValue': '',            'EmailTemplateId': 'c1dc511c-0dd7-4208-bad7-8729fa097194',            'SMSTemplateId': '87_11',            'NotifyTime': '',            'NotificationDay': '',            'TrumpiaKeyword': '',            'Level': 'Service',            'CommodityType': 'Water',            'ThresholdType': '',            'ThresholdMin': '',            'ThresholdMax': ''          }        ]      }    ],    'ExtendedAttributes': 'normalDecimalFormat=0.00,altNumericFormat=0',    'altNumericFormat': '01',    'AlterInformation': 'apply changes indicated'  }";
            return settings;
        }


        public static BillToDateSettings CreateMockBillToDateSettings(int clientId)
        {
            var settings = new List<ClientConfiguration>
            {
                new ClientConfiguration
                {
                    Key = "general.allowbaselinecalculations",
                    Category = "billtodateengine",
                    Value = "false"
                },
                new ClientConfiguration
                {
                    Key = "general.allowmultiplemetersperservice",
                    Category = "billtodateengine",
                    Value = "false"
                },
                new ClientConfiguration
                {
                    Key = "general.allowrebatecalculations",
                    Category = "billtodateengine",
                    Value = "false"
                },
                new ClientConfiguration
                {
                    Key = "general.conversionfactorforgas",
                    Category = "billtodateengine",
                    Value = "1"
                },
                new ClientConfiguration
                {
                    Key = "general.conversionfactorforwater",
                    Category = "billtodateengine",
                    Value = "1"
                },
                new ClientConfiguration
                {
                    Key = "general.dailytierbilldaystype",
                    Category = "billtodateengine",
                    Value = "0"
                },
                new ClientConfiguration
                {
                    Key = "general.dstenddate",
                    Category = "billtodateengine",
                    Value = ""
                },
                new ClientConfiguration
                {
                    Key = "general.handlemixedintervalreading",
                    Category = "billtodateengine",
                    Value = "false"
                },
                new ClientConfiguration
                {
                    Key = "general.minimumchargetype",
                    Category = "billtodateengine",
                    Value = "0"
                },
                new ClientConfiguration
                {
                    Key = "general.projectednumberofdays",
                    Category = "billtodateengine",
                    Value = "31"
                },
                new ClientConfiguration
                {
                    Key = "general.proratedemandusagedeterminants",
                    Category = "billtodateengine",
                    Value = "false"
                },
                new ClientConfiguration
                {
                    Key = "general.proratemonthlyservicecharges",
                    Category = "billtodateengine",
                    Value = "false"
                },
                new ClientConfiguration
                {
                    Key = "general.proratenonmeteredcharges",
                    Category = "billtodateengine",
                    Value = "false"
                },
                new ClientConfiguration
                {
                    Key = "general.smoothtiers",
                    Category = "billtodateengine",
                    Value = "false"
                },
                new ClientConfiguration
                {
                    Key = "general.supportdailydemand",
                    Category = "billtodateengine",
                    Value = "false"
                },
                new ClientConfiguration
                {
                    Key = "general.useconversionfactorforgas",
                    Category = "billtodateengine",
                    Value = "false"
                },
                new ClientConfiguration
                {
                    Key = "general.useconversionfactorforwater",
                    Category = "billtodateengine",
                    Value = "false"
                },
                new ClientConfiguration
                {
                    Key = "general.useprojectednumdaysforbilltodate",
                    Category = "billtodateengine",
                    Value = "false"
                },
                new ClientConfiguration
                {
                    Key = "sewer.proratesewermaxmonthsusagedeterminants",
                    Category = "billtodateengine",
                    Value = "false"
                },
                new ClientConfiguration
                {
                    Key = "sewer.sewermaxmonthsusageforcommercial",
                    Category = "billtodateengine",
                    Value = "80"
                },
                new ClientConfiguration
                {
                    Key = "sewer.sewermaxmonthsusageforresidential",
                    Category = "billtodateengine",
                    Value = "80"
                },
                new ClientConfiguration
                {
                    Key = "sewer.usesewermaxmonthsusageforcommercial",
                    Category = "billtodateengine",
                    Value = "false"
                },
                new ClientConfiguration
                {
                    Key = "sewer.usesewermaxmonthsusageforresidential",
                    Category = "billtodateengine",
                    Value = "false"
                },
                new ClientConfiguration
                {
                    Key = "validate.checkdaysinfullmonth",
                    Category = "billtodateengine",
                    Value = "false"
                },
                new ClientConfiguration
                {
                    Key = "validate.checkmaxmissingdays",
                    Category = "billtodateengine",
                    Value = "false"
                },
                new ClientConfiguration
                {
                    Key = "validate.checkminbilldays",
                    Category = "billtodateengine",
                    Value = "false"
                },
                new ClientConfiguration
                {
                    Key = "validate.checkminusage",
                    Category = "billtodateengine",
                    Value = "false"
                },
                new ClientConfiguration
                {
                    Key = "validate.daysinfullmonth",
                    Category = "billtodateengine",
                    Value = "25"
                },
                new ClientConfiguration
                {
                    Key = "validate.maxmissingdays",
                    Category = "billtodateengine",
                    Value = "8"
                },
                new ClientConfiguration
                {
                    Key = "validate.minbilldayselectric",
                    Category = "billtodateengine",
                    Value = "5"
                },
                new ClientConfiguration
                {
                    Key = "validate.minbilldaysgas",
                    Category = "billtodateengine",
                    Value = "5"
                },
                new ClientConfiguration
                {
                    Key = "validate.minbilldayswater",
                    Category = "billtodateengine",
                    Value = "5"
                },
                new ClientConfiguration
                {
                    Key = "validate.minusage",
                    Category = "billtodateengine",
                    Value = "5"
                },
                new ClientConfiguration
                {
                    Key = "ratecompanyid",
                    Category = "common",
                    Value = "100"
                }

            };

            var reader = new BillToDate.Configuration.SettingsReader(clientId);
            return reader.GetSettings(settings);
        }
        
        public static ClientSettings CreateMockDefaultSubscriptionList()
        {
            var settings = CreateMockDefaultSubscriptionString();

            return JsonConvert.DeserializeObject<ClientSettings>(settings);
        }

        public static List<WeatherDb> CreateMockWeatherData( DateTime startDate, DateTime endDate)
        {
            var weatherReadings = new List<WeatherDb>();
            var daysOfData = (endDate - startDate).TotalDays + 1;
            var avgTemp = 50;

            for (var j = 0; j < daysOfData; j++)
            {
                var weather = new WeatherDb
                {
                    WeatherReadingDate = startDate.AddDays(j),
                    AvgTemp = avgTemp + j
                };
                weatherReadings.Add(weather);
            }
            return weatherReadings;
        }

        public static List<TallAmiModel> CreateMockAmiDailyModel(int clientId, string accountId,
            string meterId, string servicePointId, DateTime startDate, DateTime endDate, DateTime projected,
            int commodityType = 1, int unitOfMeasurement = 0)
        {
            List<TallAmiModel> readings = new List<TallAmiModel>();
            var daysOfData = (endDate - startDate).TotalDays + 1;
            for (var j = 0; j < daysOfData; j++)
            {
                var date = startDate.AddDays(j);
                var reading = new TallAmiModel();
                reading.AccountNumber = accountId;
                reading.AmiTimeStamp = date;
                reading.ClientId = clientId;
                reading.CommodityId = commodityType;
                reading.IntervalType = 24;
                reading.MeterId = meterId;
                reading.ProjectedReadDate = projected;
                reading.ServicePointId = servicePointId;
                reading.UOMId = unitOfMeasurement;
                reading.Timezone = "EST";
                reading.Consumption = Convert.ToDouble(1);
                reading.AmiTimeStamp = date.AddDays(j);
                readings.Add(reading);
            }
            return readings.OrderByDescending(r => r.AmiTimeStamp).ToList();
        }

        public static List<TallAmiModel> CreateMockAmiDailyModelWithTou(int clientId, string accountId,
            string meterId, string servicePointId, DateTime startDate, DateTime endDate, DateTime projected,
            int commodityType = 1, int unitOfMeasurement = 0)
        {
            List<TallAmiModel> readings = new List<TallAmiModel>();
            var daysOfData = (endDate - startDate).TotalDays + 1;
            for (var j = 0; j < daysOfData; j++)
            {
                var date = startDate.AddDays(j);
                var reading = new TallAmiModel();
                reading.AccountNumber = accountId;
                reading.AmiTimeStamp = date;
                reading.ClientId = clientId;
                reading.CommodityId = commodityType;
                //reading.DailyConsumption = Convert.ToDouble(24);
                reading.IntervalType = 24;
                reading.MeterId = meterId;
                reading.ProjectedReadDate = projected;
                reading.ServicePointId = servicePointId;
                reading.UOMId = unitOfMeasurement;
                reading.Timezone = "EST";
                reading.Consumption = Convert.ToDouble(5);
                reading.TOU_OnPeak = Convert.ToDouble(1);
                reading.TOU_OffPeak = Convert.ToDouble(1);
                reading.TOU_CriticalPeak = Convert.ToDouble(1);
                reading.TOU_Shoulder1 = Convert.ToDouble(1);
                reading.TOU_Shoulder2 = Convert.ToDouble(1);
                reading.AmiTimeStamp = date.AddDays(j);
                readings.Add(reading);
            }
            return readings.OrderByDescending(r => r.AmiTimeStamp).ToList();
        }

        public static IEnumerable<AmiDailyIntervalModel> CreateMockEnumerableAmiDailyData(int clientId, string accountId,
           string meterId, string servicePointId, DateTime startDate, DateTime endDate, DateTime projected,
           int commodityType = 1, int unitOfMeasurement = 0)
        {
            List<AmiDailyIntervalModel> readings = new List<AmiDailyIntervalModel>();
            var daysOfData = (endDate - startDate).TotalDays + 1;

            for (var j = 0; j < daysOfData; j++)
            {
                var date = startDate.AddDays(j);

                var reading = new AmiDailyIntervalModel
                {
                    AccountNumber = accountId,
                    AmiTimeStamp = date,
                    ClientId = clientId,
                    CommodityId = commodityType,
                    IntervalType = 24,
                    MeterId = meterId,
                    ProjectedReadDate = projected,
                    ServicePointId = servicePointId,
                    UOMId = unitOfMeasurement
                };

                // notou
                reading.TOUID = 0;
                reading.TOU_Regular = 5;
                readings.Add(reading);
            }
            return readings.AsEnumerable();
        }

        public static IEnumerable<AmiDailyIntervalModel> CreateMockEnumerableAmiDailyDataTouWithTotal(int clientId, string accountId,
   string meterId, string servicePointId, DateTime startDate, DateTime endDate, DateTime projected,
   int commodityType = 1, int unitOfMeasurement = 0)
        {
            List<AmiDailyIntervalModel> readings = new List<AmiDailyIntervalModel>();
            var daysOfData = (endDate - startDate).TotalDays + 1;

            for (var j = 0; j < daysOfData; j++)
            {
                var date = startDate.AddDays(j);

                var reading = new AmiDailyIntervalModel
                {
                    AccountNumber = accountId,
                    AmiTimeStamp = date,
                    ClientId = clientId,
                    CommodityId = commodityType,
                    IntervalType = 24,
                    MeterId = meterId,
                    ProjectedReadDate = projected,
                    ServicePointId = servicePointId,
                    UOMId = unitOfMeasurement,
                    TOU_OnPeak = 1,
                    TOU_OffPeak = 1,
                    TOU_CriticalPeak = 1,
                    TOU_Shoulder1 = 1,
                    TOU_Shoulder2 = 1,
                    TOUID = 0
                };
                
                // notou
                reading.TOU_Regular = 5;
                readings.Add(reading);
            }
            return readings.AsEnumerable();
        }

        public static List<Reading> ConvertDailyToReadings(IEnumerable<AmiDailyIntervalModel> amiDailyList)
        {
            var readings = new List<Reading>();

            foreach (var row in amiDailyList)
            {
                var amiRow = row;
                var date = amiRow.AmiTimeStamp;
                var noTou = amiRow.TOU_Regular;
                if (noTou != null && noTou > 0d)
                {
                    var reading = new Reading(date, noTou.Value);
                    reading.BaseOrTier = Enums.BaseOrTier.TotalServiceUse;
                    reading.TimeOfUse = Enums.TimeOfUse.Undefined;
                    readings.Add(reading);
                }
                var onPeak = amiRow.TOU_OnPeak;
                if (onPeak != null && onPeak > 0d)
                {
                    var reading = new Reading(date, onPeak.Value);
                    reading.BaseOrTier = Enums.BaseOrTier.Undefined;
                    reading.TimeOfUse = Enums.TimeOfUse.OnPeak;
                    readings.Add(reading);
                }
                var offPeak = amiRow.TOU_OffPeak;
                if (offPeak != null && offPeak > 0d)
                {
                    var reading = new Reading(date, offPeak.Value);
                    reading.BaseOrTier = Enums.BaseOrTier.Undefined;
                    reading.TimeOfUse = Enums.TimeOfUse.OffPeak;
                    readings.Add(reading);
                }
                var shoulder1 = amiRow.TOU_Shoulder1;
                if (shoulder1 != null && shoulder1 > 0d)
                {
                    var reading = new Reading(date, shoulder1.Value);
                    reading.BaseOrTier = Enums.BaseOrTier.Undefined;
                    reading.TimeOfUse = Enums.TimeOfUse.Shoulder1;
                    readings.Add(reading);
                }
                var shoulder2 = amiRow.TOU_Shoulder2;
                if (shoulder2 != null && shoulder2 > 0d)
                {
                    var reading = new Reading(date, shoulder2.Value);
                    reading.BaseOrTier = Enums.BaseOrTier.Undefined;
                    reading.TimeOfUse = Enums.TimeOfUse.Shoulder1;
                    readings.Add(reading);
                }
                var criticalPeak = amiRow.TOU_CriticalPeak;
                if (criticalPeak != null && criticalPeak > 0d)
                {
                    var reading = new Reading(date, criticalPeak.Value);
                    reading.BaseOrTier = Enums.BaseOrTier.Undefined;
                    reading.TimeOfUse = Enums.TimeOfUse.CriticalPeak;
                    readings.Add(reading);
                }

            }

            return readings;
        }

        public static ClientSettings CreateMockClientSettings()
        {
            var clientSettingsJson = new List<ClientConfigurationBulk>
            {
                new ClientConfigurationBulk
                {
                    Category = "system",
                    Key = "aclaraone.clientsettings",
                    Value = @"{
  'ClientId': 87,
  'TimeZone': 'EST',
  'IsAmiIntervalStart': false,
  'IsDstHandled': false,
  'CalculationScheduleTime': '',
  'NotificationReportScheduleTime': '05:00',
  'InsightReportScheduleTime': '06:00',
  'ForgotPasswordEmailTemplateId': 'af5c0251-486f-4100-a4cf-2d91a905f108',
  'RegistrationConfirmationEmailTemplateId': '7c0657cd-1020-42a4-a281-87db82a7f6c4',
  'OptInEmailConfirmationTemplateId': '777ca9f8-bb03-490b-946b-b1e6991b2b15',
  'OptInSmsConfirmationTemplateId': '87_12',
  'OptInEmailUserName': 'aclaradev',
  'OptInEmailPassword': 'Xaclaradev2311X',
  'OptInEmailFrom': 'support@aclarax.com',
  'TrumpiaShortCode': '99000',
  'TrumpiaApiKey': 'b4c1971153b3509b6ec0d8a24a33454c',
  'TrumpiaUserName': 'aclaradev',
  'TrumpiaContactList': 'AclaraDemoDev',
  'UnmaskedAccountIdEndingDigit': '4',
  'Programs': [
    {
      'ProgramName': 'Program1',
      'UtilityProgramName': 'Program1',
      'FileExportRequired': true,
      'DoubleOptInRequired': true,
      'TrumpiaKeyword': 'abagdasarian2',
      'Insights': [
        {
          'InsightName': 'BillToDate',
          'UtilityInsightName': 'BillToDate',
          'AllowedCommunicationChannel': 'File,Email,SMS,EmailandSMS',
          'DefaultCommunicationChannel': 'Email',
          'DefaultFrequency': 'Daily',
          'DefaultValue': '',
          'EmailTemplateId': 'c8d5e21f-1150-4b3d-b4b8-ad869d89a040',
          'SMSTemplateId': '87_1',
          'NotifyTime': '20:00',
          'NotificationDay': 'Monday',
          'TrumpiaKeyword': '',
          'Level': 'Account',
          'CommodityType': '',
          'ThresholdType': '',
          'ThresholdMin': '',
          'ThresholdMax': ''
        },
        {
          'InsightName': 'AccountProjectedCost',
          'UtilityInsightName': 'AccountProjectedCost',
          'AllowedCommunicationChannel': 'File,Email,SMS,EmailandSMS',
          'DefaultCommunicationChannel': 'Email',
          'DefaultFrequency': 'Daily',
          'DefaultValue': '',
          'EmailTemplateId': '7367b661-0dfd-4eb2-a0d8-0735a459fcb5',
          'SMSTemplateId': '87_3',
          'NotifyTime': '20:00',
          'NotificationDay': 'Monday',
          'TrumpiaKeyword': '',
          'Level': 'Account',
          'CommodityType': '',
          'ThresholdType': '',
          'ThresholdMin': '',
          'ThresholdMax': ''
        },
        {
          'InsightName': 'AccountLevelCostThreshold',
          'UtilityInsightName': 'AccountLevelCostThreshold',
          'AllowedCommunicationChannel': 'File,Email,SMS,EmailandSMS',
          'DefaultCommunicationChannel': 'Email',
          'DefaultFrequency': 'Immediate',
          'DefaultValue': '100',
          'EmailTemplateId': '7b3f9981-ae52-4fd9-b223-8ae516f6f82f',
          'SMSTemplateId': '87_6',
          'NotifyTime': '',
          'NotificationDay': '',
          'TrumpiaKeyword': '',
          'Level': 'Account',
          'CommodityType': '',
          'ThresholdType': 'Cost',
          'ThresholdMin': '1',
          'ThresholdMax': '1000000'
        },
        {
          'InsightName': 'ServiceLevelCostThreshold',
          'UtilityInsightName': 'ServiceLevelCostThreshold',
          'AllowedCommunicationChannel': 'File,Email,SMS,EmailandSMS',
          'DefaultCommunicationChannel': 'Email',
          'DefaultFrequency': 'Immediate',
          'DefaultValue': '25,25',
          'EmailTemplateId': '87791eaa-0342-45d5-85d4-4645f7a85f7d',
          'SMSTemplateId': '87_8',
          'NotifyTime': '',
          'NotificationDay': '',
          'TrumpiaKeyword': '',
          'Level': 'Service',
          'CommodityType': 'Gas,Electric',
          'ThresholdType': 'Cost',
          'ThresholdMin': '1',
          'ThresholdMax': '1000000'
        },
        {
          'InsightName': 'ServiceLevelUsageThreshold',
          'UtilityInsightName': 'ServiceLevelUsageThreshold',
          'AllowedCommunicationChannel': 'File,Email,SMS,EmailandSMS',
          'DefaultCommunicationChannel': 'Email',
          'DefaultFrequency': 'Immediate',
          'DefaultValue': '25,500',
          'EmailTemplateId': 'f7791cc2-9665-4d4b-8a7f-9421daf55e29',
          'SMSTemplateId': '87_7',
          'NotifyTime': '',
          'NotificationDay': '',
          'TrumpiaKeyword': '',
          'Level': 'Service',
          'CommodityType': 'Gas,Electric',
          'ThresholdType': 'Usage',
          'ThresholdMin': '0.1',
          'ThresholdMax': '1000000'
        },
        {
          'InsightName': 'CostToDate',
          'UtilityInsightName': 'CostToDate',
          'AllowedCommunicationChannel': 'File,Email,SMS,EmailandSMS',
          'DefaultCommunicationChannel': 'Email',
          'DefaultFrequency': 'Weekly',
          'DefaultValue': '',
          'EmailTemplateId': 'b4425b3d-de12-4694-8751-edb41bc5cd79',
          'SMSTemplateId': '87_2',
          'NotifyTime': '20:00',
          'NotificationDay': 'Monday',
          'TrumpiaKeyword': '',
          'Level': 'Service',
          'CommodityType': 'Gas,Electric',
          'ThresholdType': '',
          'ThresholdMin': '',
          'ThresholdMax': ''
        },
        {
          'InsightName': 'ServiceProjectedCost',
          'UtilityInsightName': 'ServiceProjectedCost',
          'AllowedCommunicationChannel': 'File,Email,SMS,EmailandSMS',
          'DefaultCommunicationChannel': 'Email',
          'DefaultFrequency': 'Daily',
          'DefaultValue': '',
          'EmailTemplateId': '6bd40b25-fc86-480f-a1d4-0f2a22b7d270',
          'SMSTemplateId': '87_4',
          'NotifyTime': '20:00',
          'NotificationDay': 'Monday',
          'TrumpiaKeyword': '',
          'Level': 'Service',
          'CommodityType': 'Gas,Electric',
          'ThresholdType': '',
          'ThresholdMin': '',
          'ThresholdMax': ''
        },
        {
          'InsightName': 'Usage',
          'UtilityInsightName': 'Usage',
          'AllowedCommunicationChannel': 'File,Email,SMS,EmailandSMS',
          'DefaultCommunicationChannel': 'Email',
          'DefaultFrequency': 'Daily',
          'DefaultValue': '',
          'EmailTemplateId': '3fdb3336-0192-4bb9-bc53-8810dd110a3a',
          'SMSTemplateId': '87_5',
          'NotifyTime': '20:00',
          'NotificationDay': 'Monday',
          'TrumpiaKeyword': '',
          'Level': 'Service',
          'CommodityType': 'Gas,Electric',
          'ThresholdType': '',
          'ThresholdMin': '',
          'ThresholdMax': ''
        },
        {
          'InsightName': 'DayThreshold',
          'UtilityInsightName': 'DayThreshold',
          'AllowedCommunicationChannel': 'File,Email,SMS,EmailandSMS',
          'DefaultCommunicationChannel': 'Email',
          'DefaultFrequency': 'Immediate',
          'DefaultValue': '1,16',
          'EmailTemplateId': 'ce508747-444b-4e1c-9f39-53629e47260b',
          'SMSTemplateId': '87_9',
          'NotifyTime': '',
          'NotificationDay': '',
          'TrumpiaKeyword': '',
          'Level': 'Service',
          'CommodityType': 'Gas,Electric',
          'ThresholdType': 'Usage',
          'ThresholdMin': '0.1',
          'ThresholdMax': '1000000'
        },
        {
          'InsightName': 'ServiceLevelTieredThresholdApproaching',
          'UtilityInsightName': 'ServiceLevelTieredThresholdApproaching',
          'AllowedCommunicationChannel': 'File,Email,SMS,EmailandSMS',
          'DefaultCommunicationChannel': 'Email',
          'DefaultFrequency': 'Immediate',
          'DefaultValue': '20,20',
          'EmailTemplateId': '4962e788-fb1e-43cf-a2f6-faf713d25e0c',
          'SMSTemplateId': '87_10',
          'NotifyTime': '',
          'NotificationDay': '',
          'TrumpiaKeyword': '',
          'Level': 'Service',
          'CommodityType': 'Gas,Electric',
          'ThresholdType': 'Percent',
          'ThresholdMin': '1',
          'ThresholdMax': '100'
        },
        {
          'InsightName': 'ServiceLevelTieredThresholdExceed',
          'UtilityInsightName': 'ServiceLevelTieredThresholdExceed',
          'AllowedCommunicationChannel': 'File,Email,SMS,EmailandSMS',
          'DefaultCommunicationChannel': 'Email',
          'DefaultFrequency': 'Immediate',
          'DefaultValue': '',
          'EmailTemplateId': 'c1dc511c-0dd7-4208-bad7-8729fa097194',
          'SMSTemplateId': '87_11',
          'NotifyTime': '',
          'NotificationDay': '',
          'TrumpiaKeyword': '',
          'Level': 'Service',
          'CommodityType': 'Gas,Electric',
          'ThresholdType': '',
          'ThresholdMin': '',
          'ThresholdMax': ''
        }
      ]
    },
    {
      'ProgramName': 'Program2',
      'UtilityProgramName': 'Program2',
      'FileExportRequired': false,
      'DoubleOptInRequired': true,
      'TrumpiaKeyword': '',
      'Insights': [
        {
          'InsightName': 'ServiceLevelCostThreshold',
          'UtilityInsightName': 'ServiceLevelCostThreshold',
          'AllowedCommunicationChannel': 'File,Email,SMS,EmailandSMS',
          'DefaultCommunicationChannel': 'Email',
          'DefaultFrequency': 'Immediate',
          'DefaultValue': '25',
          'EmailTemplateId': '87791eaa-0342-45d5-85d4-4645f7a85f7d',
          'SMSTemplateId': '87_8',
          'NotifyTime': '',
          'NotificationDay': '',
          'TrumpiaKeyword': '',
          'Level': 'Service',
          'CommodityType': 'Water',
          'ThresholdType': 'Cost',
          'ThresholdMin': '1',
          'ThresholdMax': '1000000'
        },
        {
          'InsightName': 'ServiceLevelUsageThreshold',
          'UtilityInsightName': 'ServiceLevelUsageThreshold',
          'AllowedCommunicationChannel': 'File,Email,SMS,EmailandSMS',
          'DefaultCommunicationChannel': 'Email',
          'DefaultFrequency': 'Immediate',
          'DefaultValue': '5',
          'EmailTemplateId': 'f7791cc2-9665-4d4b-8a7f-9421daf55e29',
          'SMSTemplateId': '87_7',
          'NotifyTime': '',
          'NotificationDay': '',
          'TrumpiaKeyword': '',
          'Level': 'Service',
          'CommodityType': 'Water',
          'ThresholdType': 'Usage',
          'ThresholdMin': '0.1',
          'ThresholdMax': '1000000'
        },
        {
          'InsightName': 'CostToDate',
          'UtilityInsightName': 'CostToDate',
          'AllowedCommunicationChannel': 'File,Email,SMS,EmailandSMS',
          'DefaultCommunicationChannel': 'Email',
          'DefaultFrequency': 'Weekly',
          'DefaultValue': '',
          'EmailTemplateId': 'b4425b3d-de12-4694-8751-edb41bc5cd79',
          'SMSTemplateId': '87_2',
          'NotifyTime': '20:00',
          'NotificationDay': 'Monday',
          'TrumpiaKeyword': '',
          'Level': 'Service',
          'CommodityType': 'Water',
          'ThresholdType': '',
          'ThresholdMin': '',
          'ThresholdMax': ''
        },
        {
          'InsightName': 'ServiceProjectedCost',
          'UtilityInsightName': 'ServiceProjectedCost',
          'AllowedCommunicationChannel': 'File,Email,SMS,EmailandSMS',
          'DefaultCommunicationChannel': 'Email',
          'DefaultFrequency': 'Daily',
          'DefaultValue': '',
          'EmailTemplateId': '6bd40b25-fc86-480f-a1d4-0f2a22b7d270',
          'SMSTemplateId': '87_4',
          'NotifyTime': '20:00',
          'NotificationDay': 'Monday',
          'TrumpiaKeyword': '',
          'Level': 'Service',
          'CommodityType': 'Water',
          'ThresholdType': '',
          'ThresholdMin': '',
          'ThresholdMax': ''
        },
        {
          'InsightName': 'Usage',
          'UtilityInsightName': 'Usage',
          'AllowedCommunicationChannel': 'File,Email,SMS,EmailandSMS',
          'DefaultCommunicationChannel': 'Email',
          'DefaultFrequency': 'Daily',
          'DefaultValue': '',
          'EmailTemplateId': '3fdb3336-0192-4bb9-bc53-8810dd110a3a',
          'SMSTemplateId': '87_5',
          'NotifyTime': '20:00',
          'NotificationDay': 'Monday',
          'TrumpiaKeyword': '',
          'Level': 'Service',
          'CommodityType': 'Water',
          'ThresholdType': '',
          'ThresholdMin': '',
          'ThresholdMax': ''
        },
        {
          'InsightName': 'DayThreshold',
          'UtilityInsightName': 'DayThreshold',
          'AllowedCommunicationChannel': 'File,Email,SMS,EmailandSMS',
          'DefaultCommunicationChannel': 'Email',
          'DefaultFrequency': 'Immediate',
          'DefaultValue': '0.15',
          'EmailTemplateId': 'ce508747-444b-4e1c-9f39-53629e47260b',
          'SMSTemplateId': '87_9',
          'NotifyTime': '',
          'NotificationDay': '',
          'TrumpiaKeyword': '',
          'Level': 'Service',
          'CommodityType': 'Water',
          'ThresholdType': 'Usage',
          'ThresholdMin': '0.1',
          'ThresholdMax': '1000000'
        },
        {
          'InsightName': 'ServiceLevelTieredThresholdApproaching',
          'UtilityInsightName': 'ServiceLevelTieredThresholdApproaching',
          'AllowedCommunicationChannel': 'File,Email,SMS,EmailandSMS',
          'DefaultCommunicationChannel': 'Email',
          'DefaultFrequency': 'Immediate',
          'DefaultValue': '20',
          'EmailTemplateId': '4962e788-fb1e-43cf-a2f6-faf713d25e0c',
          'SMSTemplateId': '87_10',
          'NotifyTime': '',
          'NotificationDay': '',
          'TrumpiaKeyword': '',
          'Level': 'Service',
          'CommodityType': 'Water',
          'ThresholdType': 'Percent',
          'ThresholdMin': '1',
          'ThresholdMax': '100'
        },
        {
          'InsightName': 'ServiceLevelTieredThresholdExceed',
          'UtilityInsightName': 'ServiceLevelTieredThresholdExceed',
          'AllowedCommunicationChannel': 'File,Email,SMS,EmailandSMS',
          'DefaultCommunicationChannel': 'Email',
          'DefaultFrequency': 'Immediate',
          'DefaultValue': '',
          'EmailTemplateId': 'c1dc511c-0dd7-4208-bad7-8729fa097194',
          'SMSTemplateId': '87_11',
          'NotifyTime': '',
          'NotificationDay': '',
          'TrumpiaKeyword': '',
          'Level': 'Service',
          'CommodityType': 'Water',
          'ThresholdType': '',
          'ThresholdMin': '',
          'ThresholdMax': ''
        }
      ]
    }
  ],
  'ExtendedAttributes': 'normalDecimalFormat=0.00,altNumericFormat=0',
  'AlterInformation': 'apply changes indicated'
}"

                }
            };
            return JsonConvert.DeserializeObject<ClientSettings>(clientSettingsJson[0].Value);
        }
    }
}
