﻿using System.Linq;
using CE.Models.Insights.EF;

namespace CE.Insights.Test.Helpers
{
    class TestActionPremisePlansDbSet : TestDbSet<ActionPremisePlan>
    {
        public override ActionPremisePlan Find(params object[] keyValues)
        {
            return this.SingleOrDefault(ActionPremisePlan => ActionPremisePlan.PremiseID == (string)keyValues.Single());
        }
    }
}
