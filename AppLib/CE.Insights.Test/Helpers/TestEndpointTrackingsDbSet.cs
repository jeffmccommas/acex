﻿using System.Linq;
using CE.Models.Insights.EF;

namespace CE.Insights.Test.Helpers
{
    class TestEndpointTrackingsDbSet: TestDbSet<EndpointTracking>
    {
        public override EndpointTracking Find(params object[] keyValues)
        {
            return this.SingleOrDefault(EndpointTracking => EndpointTracking.EndpointID == (int)keyValues.Single());
        }

    }
}
