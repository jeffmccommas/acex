﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.Registrar;
using AutoMapper;
using AO.Entities;
using CE.AO.Models;
using CE.Insights.Test.Helpers;
using CE.Models;
using CE.Models.Insights;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;
using SubscriptionModel = CE.Insights.Models.SubscriptionModel;

namespace CE.Insights.Test.Models
{
    [TestClass()]
    public class SubscriptionModelTests
    {
        #region Constants for content
        private const string ClientSpace = "netnln2itvho";
        private const string ClientAccessToken = "c58ec8d495044174267c257f105914f7fb9567127a879b2419e1c3d405a54be2";
        //private const string DefaultUrl = "https://cdn.contentful.com";
        //private const string DefaultSpace = "gwdmhnzd2xyu";
        //private const string DefaultAccessToken = "130a40b027b6ecdccfc982f9d2898bd9bbdd462cc8b68f6555ce3f41666634fc";
        //private const string StringCeApplianceList1 = "CEApplianceList1";
        //private const string StringCeEnduseList1 = "CEEnduseList1";

        #endregion

        private Mock<ICassandraRepository> _cassandraRepository;
        private Mock<IClientConfigFacade> _clientConfigFacade;
        private UnityContainer _container;
        [TestInitialize]
        public void TestInit()
        {
            _cassandraRepository = new Mock<ICassandraRepository>();
            _clientConfigFacade = new Mock<IClientConfigFacade>();
            _container = new UnityContainer();

            (new DataStorageRegistrar()).Initialize<TransientLifetimeManager>(_container);
            _container.RegisterInstance(_clientConfigFacade.Object);


            //Mapper.CreateMap<SubscriptionEntity, AO.Models.SubscriptionModel>().ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<AO.Models.SubscriptionModel, SubscriptionEntity>().ForAllUnmappedMembers(o => o.Ignore());
            //Mapper.CreateMap<BillingEntity, BillingModel>();
            //Mapper.CreateMap<CustomerEntity, CustomerModel>();
            //Mapper.CreateMap<PremiseEntity, PremiseModel>();
            //Mapper.CreateMap<CustomerModel, CustomerEntity>();
            //Mapper.CreateMap<SmsTemplateEntity, SmsTemplateModel>();
        }

        [TestCleanup]
        public void TestClean()
        {
            //noop
        }


        [TestMethod()]
        public void SubscriptionV1_GetAllInsights_NoCustomer_Test()
        {
            _container.RegisterInstance(_cassandraRepository.Object);
            var clientId = 87;
            var customerId = "default";
            var accountId = "default";

            // mock no previous subscription subscribed
            IList<SubscriptionEntity> subList = new List<SubscriptionEntity>();
            _cassandraRepository.Setup(t => t.GetAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>())).Returns(Task.FromResult(subList));

            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();

            var model = new SubscriptionModel(_container);

            var result = model.GetAllInsightPrograms(clientId, customerId, accountId);

            Assert.IsNull(result);
            _clientConfigFacade.Verify();

        }
        [TestMethod()]
        public void SubscriptionV1_GetAllInsights_Default_Test()
        {
            _container.RegisterInstance(_cassandraRepository.Object);

            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId = "EM001";
            var rateClass = "RG-1";
            var servicePointId = "SP_BTD001a1";
            var serviceContractId = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);
            

            // mock no previous subscription subscribed
            IList<SubscriptionEntity> subList = new List<SubscriptionEntity>();
            _cassandraRepository.Setup(t => t.GetAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>())).Returns(Task.FromResult(subList));

            // mock billing entities data
            var mockBillingEntity = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId,
                servicePointId, meterId, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();

            var model = new SubscriptionModel(_container);

            var result = model.GetAllInsightPrograms(clientId, customerId, accountId);

            Assert.IsNotNull(result);

            Assert.AreEqual(11, result.Count);
            foreach (var insight in result)
            {
                Assert.AreEqual(false, insight.IsSelected);
            }

            _clientConfigFacade.Verify();
        }

        [TestMethod()]
        public void SubscriptionV1_GetAllInsights_DefaultwithMockOneInsightProgramSubscribed_Test()
        {
            var model = new SubscriptionModel(_container);
            _container.RegisterInstance(_cassandraRepository.Object);

            var program = "Program1";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            double threshold = 5;

            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId = "EM001";
            var rateClass = "RG-1";
            var servicePointId = "SP_BTD001a1";
            var serviceContractId = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock billing entities data
            var mockBillingEntity = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId,
                servicePointId, meterId, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);

            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight1, false, 0);
            var mockInsight2 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight2, true, threshold);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight7, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            insightList.Add(mockInsight2);
            insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            insightList.Add(mockInsight8);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));
            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();

            var result = model.GetAllInsightPrograms(clientId, customerId, accountId);

            Assert.IsNotNull(result);
            Assert.AreEqual(11, result.Count);
            foreach (var insight in result)
            {
                if (insight.ProgramName.ToLower() == program.ToLower() && insight.InsightTypeName.ToLower() == insight2.ToLower())
                {
                    Assert.AreEqual(true, insight.IsSelected);
                    Assert.AreEqual(threshold, insight.SubscriberThresholdValue);
                }
                else
                    Assert.AreEqual(false, insight.IsSelected);
            }
            _clientConfigFacade.Verify();
        }

        [TestMethod()]
        public void SubscriptionV1_GetAllInsights_DefaultwithMockOneInsightNoProgramSubscribed1_Test()
        {
            _container.RegisterInstance(_cassandraRepository.Object);

            var model = new SubscriptionModel(_container);

            var program = "Program1";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            double threshold = 5;

            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId = "EM001";
            var rateClass = "RG-1";
            var servicePointId = "SP_BTD001a1";
            var serviceContractId = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock billing entities data
            var mockBillingEntity = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId,
                servicePointId, meterId, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);

            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight1, false, 0);
            var mockInsight2 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight2, true, threshold);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight7, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            insightList.Add(mockInsight2);
            insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            insightList.Add(mockInsight8);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            IList<SubscriptionEntity> programSubscription = new List<SubscriptionEntity>();
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programSubscription))
                .Returns(Task.FromResult(insightList));
            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();

            var result = model.GetAllInsightPrograms(clientId, customerId, accountId);

            Assert.IsNotNull(result);
            Assert.AreEqual(11, result.Count);
            foreach (var insight in result)
            {
                if (insight.ProgramName.ToLower() == program.ToLower() && insight.InsightTypeName.ToLower() == insight2.ToLower())
                {
                    Assert.AreEqual(true, insight.IsSelected);
                    Assert.AreEqual(threshold, insight.SubscriberThresholdValue);
                }
                else
                    Assert.AreEqual(false, insight.IsSelected);
            }
            _clientConfigFacade.Verify();
        }

        [TestMethod()]
        public void SubscriptionV1_GetAllInsights_DefaultwithMockOneInsightNoProgramSubscribed2_Test()
        {
            _container.RegisterInstance(_cassandraRepository.Object);

            var model = new SubscriptionModel(_container);

            var program = "Program1";
            var insight2 = "ServiceLevelUsageThreshold";
            double threshold = 5;
            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId = "EM001";
            var rateClass = "RG-1";
            var servicePointId = "SP_BTD001a1";
            var serviceContractId = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock billing entities data
            var mockBillingEntity = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId,
                servicePointId, meterId, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);

            // mock subscription insight entity
            var mockInsight2 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight2, true, threshold);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight2);
            IList<SubscriptionEntity> programSubscription = new List<SubscriptionEntity>();
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programSubscription))
                .Returns(Task.FromResult(insightList));
            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();

            var result = model.GetAllInsightPrograms(clientId, customerId, accountId);

            Assert.IsNotNull(result);
            Assert.AreEqual(11, result.Count);
            foreach (var insight in result)
            {
                if (insight.ProgramName.ToLower() == program.ToLower() && insight.InsightTypeName.ToLower() == insight2.ToLower())
                {
                    Assert.AreEqual(true, insight.IsSelected);
                    Assert.AreEqual(threshold, insight.SubscriberThresholdValue);
                }
                else
                    Assert.AreEqual(false, insight.IsSelected);
            }
            _clientConfigFacade.Verify();
        }

        [TestMethod()]
        public void SubscriptionV1_GetAllInsights_DefaultwithMockOneInsightSubscribed_Test()
        {
            _container.RegisterInstance(_cassandraRepository.Object);

            var model = new SubscriptionModel(_container);

            var program = "Program1";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId = "EM001";
            var rateClass = "RG-1";
            var servicePointId = "SP_BTD001a1";
            var serviceContractId = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock billing entities data
            var mockBillingEntity = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId,
                servicePointId, meterId, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);

            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight1, false, 0);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight7, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            insightList.Add(mockInsight8);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));
            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();

            var result = model.GetAllInsightPrograms(clientId, customerId, accountId);

            Assert.IsNotNull(result);
            Assert.AreEqual(11, result.Count);
            foreach (var insight in result)
            {
                if (insight.ProgramName.ToLower() == program.ToLower() && insight.InsightTypeName.ToLower() == insight2.ToLower())
                    Assert.AreEqual(true, insight.IsSelected);
                else
                    Assert.AreEqual(false, insight.IsSelected);
            }
            _clientConfigFacade.Verify();
        }

        [TestMethod()]
        public void SubscriptionV1_GetAllInsights_DefaultwithMockProgramSubscribed_Test()
        {
            _container.RegisterInstance(_cassandraRepository.Object);

            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId = "EM001";
            var rateClass = "RG-1";
            var servicePointId = "SP_BTD001a1";
            var serviceContractId = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock billing entities data
            var mockBillingEntity = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId,
                servicePointId, meterId, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);

            var program = "Program1";

            var model = new SubscriptionModel(_container);

            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            IList<SubscriptionEntity> insightSubscription = new List<SubscriptionEntity>();
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightSubscription));
            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();

            var result = model.GetAllInsightPrograms(clientId, customerId, accountId);

            Assert.IsNotNull(result);
            Assert.AreEqual(11, result.Count);
            foreach (var insight in result)
            {
                if (insight.ProgramName.ToLower() == program.ToLower())
                    Assert.AreEqual(true, insight.IsSelected);
                else
                    Assert.AreEqual(false, insight.IsSelected);
            }
            _clientConfigFacade.Verify();
        }

        [TestMethod()]
        public void SubscriptionV1_GetSubscriptions_DefaultwithMockProgramSubscribed_Test()
        {
            _container.RegisterInstance(_cassandraRepository.Object);

            const string program = "Program1";
            const string commodity = "electric";

            const int clientId = 87;
            const string customerId = "BTD001";
            const string accountId = "BTD001a1";
            const string premiseId = "BTD001a1p1";
            const string billCycleScheduleId = "Cycle01";
            const string meterId = "EM001";
            const string rateClass = "RG-1";
            const string servicePointId = "SP_BTD001a1";
            const string serviceContractId = "BTD001a1_BTD001a1p1_1_EM001";

            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock billing entities data
            var mockBillingEntity = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId,
                servicePointId, meterId, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity> { mockBillingEntity };
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);

            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };

            var request = new SubscriptionRequest
            {
                AccountId = accountId,
                CustomerId = customerId
            };

            var model = new SubscriptionModel(_container);

            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity> { mockProgramSubscription };
            IList<SubscriptionEntity> insightSubscription = new List<SubscriptionEntity>();
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightSubscription));
            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();

            var result = model.GetSubscriptions(clientUser, request);

            Assert.IsNotNull(result);
            var serviceInsights = result.Customer.Accounts.SelectMany(a => a.Premises).SelectMany(p => p.Services);
            foreach (var serviceInsight in serviceInsights)
            {
                foreach (var insight in serviceInsight.Insights)
                {
                    if (!insight.Name.ToLower().StartsWith(program.ToLower()))
                    {
                        continue;
                    }
                    Assert.AreEqual(commodity, serviceInsight.CommodityKey);
                    Assert.AreEqual(true, insight.IsSelected);
                }
            }
            var accountInsights = result.Customer.Accounts.SelectMany(a => a.Insights);
            foreach (var insight in accountInsights)
            {
                if (insight.Name.ToLower().StartsWith(program.ToLower()))
                {
                    Assert.AreEqual(true, insight.IsSelected);
                }
            }
            _clientConfigFacade.Verify();
        }

        [TestMethod()]
        public void SubscriptionV1_GetSubscriptions_DefaultwithMockOneInsightSubscribed1_Test()
        {
            _container.RegisterInstance(_cassandraRepository.Object);

            const string program = "Program1";
            const string insight1 = "ServiceLevelCostThreshold";
            const string insight2 = "ServiceLevelUsageThreshold";
            const string insight3 = "BillToDate";
            const string insight4 = "CostToDate";
            const string insight5 = "AccountProjectedCost";
            const string insight6 = "ServiceProjectedCost";
            const string insight7 = "Usage";
            const string insight8 = "AccountLevelCostThreshold";
            const string insight9 = "DayThreshold";
            const string insight10 = "ServiceLevelTieredThresholdApproaching";
            const string insight11 = "ServiceLevelTieredThresholdExceed";
            const decimal threshold = 5;
            const string programInsight = "Program 2.ServiceLevelUsageThreshold";
            const string commodity = "electric";

            const int clientId = 87;
            const string customerId = "BTD001";
            const string accountId = "BTD001a1";
            const string premiseId = "BTD001a1p1";
            const string billCycleScheduleId = "Cycle01";
            const string meterId = "EM001";
            const string rateClass = "RG-1";
            const string servicePointId = "SP_BTD001a1";
            const string serviceContractId = "BTD001a1_BTD001a1p1_1_EM001";

            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock billing entities data
            var mockBillingEntity = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId,
                servicePointId, meterId, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate, 3);
            var services = new List<BillingEntity> { mockBillingEntity };
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);

            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };

            var request = new SubscriptionRequest
            {
                AccountId = accountId,
                CustomerId = customerId
            };

            var model = new SubscriptionModel(_container);

            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity> { mockProgramSubscription };
            
            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight1, false, 0);
            var mockInsight2 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight2, true, Convert.ToDouble(threshold));
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight7, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>
            {
                mockInsight1,
                mockInsight2,
                mockInsight3,
                mockInsight4,
                mockInsight5,
                mockInsight6,
                mockInsight7,
                mockInsight8,
                mockInsight9,
                mockInsight10,
                mockInsight11
            };
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));
            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();

            var result = model.GetSubscriptions(clientUser, request);

            Assert.IsNotNull(result);
            foreach (var serviceInsight in result.Customer.Accounts.SelectMany(a => a.Premises).SelectMany(p => p.Services))
            {
                foreach (var insight in serviceInsight.Insights)
                {
                    if (string.Equals(insight.Name, programInsight, StringComparison.CurrentCultureIgnoreCase))
                    {
                        Assert.AreEqual(true, insight.IsSelected);
                        Assert.AreEqual(threshold, insight.Threshold);
                    }
                    else
                    {
                        Assert.AreEqual(false, insight.IsSelected);
                    }

                    if (insight.Name.ToLower().StartsWith(program.ToLower()))
                    {
                        Assert.AreEqual(commodity, serviceInsight.CommodityKey);
                    }
                }

            }

            foreach (var insight in result.Customer.Accounts.SelectMany(a => a.Insights))
            {
                Assert.AreEqual(false, insight.IsSelected);
            }

            _clientConfigFacade.Verify();
        }

        [TestMethod()]
        public void SubscriptionV1_GetSubscriptions_DefaultwithMockOneInsightSubscribed2_Test()
        {
            _container.RegisterInstance(_cassandraRepository.Object);


            const string program = "Program1";
            const string insight1 = "ServiceLevelCostThreshold";
            //var insight2 = "ServiceLevelUsageThreshold";
            const string insight3 = "BillToDate";
            const string insight4 = "CostToDate";
            const string insight5 = "AccountProjectedCost";
            const string insight6 = "ServiceProjectedCost";
            const string insight7 = "Usage";
            const string insight8 = "AccountLevelCostThreshold";
            const string insight9 = "DayThreshold";
            const string insight10 = "ServiceLevelTieredThresholdApproaching";
            const string insight11 = "ServiceLevelTieredThresholdExceed";
            const string programInsight = "Program1.ServiceLevelUsageThreshold";
            const string commodity = "electric";

            const int clientId = 87;
            const string customerId = "BTD001";
            const string accountId = "BTD001a1";
            const string premiseId = "BTD001a1p1";
            const string billCycleScheduleId = "Cycle01";
            const string meterId = "EM001";
            const string rateClass = "RG-1";
            const string servicePointId = "SP_BTD001a1";
            const string serviceContractId = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock billing entities data
            var mockBillingEntity = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId,
                servicePointId, meterId, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity> { mockBillingEntity };
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();

            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };

            var request = new SubscriptionRequest
            {
                AccountId = accountId,
                CustomerId = customerId
            };

            var model = new SubscriptionModel(_container);

            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity> { mockProgramSubscription };
            // mock subscription insight entity

            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight1, false, 0);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight7, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>
            {
                mockInsight1,
                mockInsight3,
                mockInsight4,
                mockInsight5,
                mockInsight6,
                mockInsight7,
                mockInsight8,
                mockInsight9,
                mockInsight10,
                mockInsight11
            };
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));

            var result = model.GetSubscriptions(clientUser, request);

            Assert.IsNotNull(result);
            foreach (var serviceInsight in result.Customer.Accounts.SelectMany(a => a.Premises).SelectMany(p => p.Services))
            {
                foreach (var insight in serviceInsight.Insights)
                {
                    Assert.AreEqual(
                        string.Equals(insight.Name, programInsight, StringComparison.CurrentCultureIgnoreCase),
                        insight.IsSelected);

                    if (insight.Name.StartsWith(program.ToLower()))
                    {
                        Assert.AreEqual(commodity, serviceInsight.CommodityKey);
                    }
                }
            }

            _clientConfigFacade.Verify();
        }


        [TestMethod()]
        public void SubscriptionV1_GetSubscriptions_DefaultwithMockOneInsightSubscribed2Servcie_Test()
        {
            _container.RegisterInstance(_cassandraRepository.Object);


            const string program = "Program1";
            const string insight1 = "ServiceLevelCostThreshold";
            //const string insight2 = "ServiceLevelUsageThreshold";
            const string insight3 = "BillToDate";
            const string insight4 = "CostToDate";
            const string insight5 = "AccountProjectedCost";
            const string insight6 = "ServiceProjectedCost";
            const string insight7 = "Usage";
            const string insight8 = "AccountLevelCostThreshold";
            const string insight9 = "DayThreshold";
            const string insight10 = "ServiceLevelTieredThresholdApproaching";
            const string insight11 = "ServiceLevelTieredThresholdExceed";
            const string programInsight = "Program1.ServiceLevelUsageThreshold";
            const string commodity = "electric";

            const int clientId = 87;
            const string customerId = "BTD001";
            const string accountId = "BTD001a1";
            const string premiseId = "BTD001a1p1";
            const string billCycleScheduleId = "Cycle01";
            const string meterId1 = "EM001";
            const string rateClass = "RG-1";
            const string servicePointId1 = "SP_BTD001a1";
            const string serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            const string meterId2 = "EM002";
            const string servicePointId2 = "SP_BTD002a1";
            const string serviceContractId2 = "BTD002a1_BTD002a1p1_1_EM002";
            //const double threshold = 90;
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);


            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var mockBillingEntity2 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId2,
                servicePointId2, meterId2, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity> { mockBillingEntity1, mockBillingEntity2 };
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);

            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };

            var request = new SubscriptionRequest
            {
                AccountId = accountId,
                CustomerId = customerId
            };

            var model = new SubscriptionModel(_container);

            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity> { mockProgramSubscription };

            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>
            {
                mockInsight1,
                mockInsight3,
                mockInsight4,
                mockInsight5,
                mockInsight6,
                mockInsight7,
                mockInsight8,
                mockInsight9,
                mockInsight10,
                mockInsight11
            };

            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));
            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();

            var result = model.GetSubscriptions(clientUser, request);

            Assert.IsNotNull(result);
            foreach (var serviceInsight in result.Customer.Accounts.SelectMany(a => a.Premises).SelectMany(p => p.Services))
            {
                foreach (var insight in serviceInsight.Insights)
                {
                    if (string.Equals(insight.Name, programInsight, StringComparison.CurrentCultureIgnoreCase) &&
                        string.Equals(serviceInsight.Id, serviceContractId1, StringComparison.CurrentCultureIgnoreCase) ||
                        string.Equals(serviceInsight.Id, serviceContractId2, StringComparison.CurrentCultureIgnoreCase) &&
                        string.Equals(insight.Level, "service", StringComparison.CurrentCultureIgnoreCase))
                    {
                        Assert.AreEqual(true, insight.IsSelected);
                    }
                    else
                    {
                        Assert.AreEqual(false, insight.IsSelected);
                    }

                    if (insight.Name.StartsWith(program.ToLower()))
                    {
                        Assert.AreEqual(commodity, serviceInsight.CommodityKey);
                    }
                }
            }
            _clientConfigFacade.Verify();
        }

        [TestMethod()]
        public void SubscriptionV1_GetSubscribeList_NoExistingSubscription_Service_Test()
        {
            _container.RegisterInstance(_cassandraRepository.Object);

            const string program = "Program1";
            //const string insight1 = "ServiceLevelCostThreshold";
            const string insight2 = "ServiceLevelUsageThreshold";
            //const string insight3 = "BillToDate";
            //const string insight4 = "CostToDate";
            //const string insight5 = "AccountProjectedCost";
            //const string insight6 = "ServiceProjectedCost";
            //const string insight7 = "Usage";
            //const string insight8 = "AccountLevelCostThreshold";
            //const string insight9 = "DayThreshold";
            //const string insight10 = "ServiceLevelTieredThresholdApproaching";
            //const string insight11 = "ServiceLevelTieredThresholdExceed";
            const string programInsight = "Program1.ServiceLevelUsageThreshold";
            //const string commodity = "water";

            const int clientId = 87;
            const string customerId = "BTD001";
            const string accountId = "BTD001a1";
            const string premiseId = "BTD001a1p1";
            const string billCycleScheduleId = "Cycle01";
            const string meterId1 = "EM001";
            const string rateClass = "RG-1";
            const string servicePointId1 = "SP_BTD001a1";
            const string serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";

            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);


            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity> { mockBillingEntity1 };
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);

            // mock no subscription data
            // mock no previous subscription subscribed
            IList<SubscriptionEntity> subList = new List<SubscriptionEntity>();
            _cassandraRepository.Setup(t => t.GetAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>())).Returns(Task.FromResult(subList));
            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();

            var subscribeList = new List<SubscribedInsight>();
            var subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = true,
                Level = "service",
                Name = programInsight,
                PremiseId = premiseId,
                ServiceId = serviceContractId1,
                //ServiceId = servicePointId1,
                Channel = "Email",
                //IsEmailOptInCompleted = false,
                //IsSmsOptInCompleted = false
            };
            subscribeList.Add(subscribeInsight);
            var model = new SubscriptionModel(_container);
            var valid = true;
            var result = model.GetSubscribeList(clientId, customerId, subscribeList, ref valid);

            Assert.IsNotNull(result);
            Assert.AreEqual(11, result.Count);

            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(true, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);

            foreach (var insight in insights)
            {
                Assert.AreEqual(false, insight.IsSelected);
                Assert.AreNotEqual(insight2.ToLower(), insight.InsightTypeName.ToLower());
                Assert.AreEqual(accountId, insight.AccountId);
                Assert.IsNotNull(insight.ServiceContractId);
                //Assert.IsNotNull(insight.ServicePointId);
                Assert.IsNotNull(insight.PremiseId);
                Assert.AreEqual(customerId, insight.CustomerId);
                Assert.IsNotNull(insight.Channel);
                Assert.AreNotEqual(string.Empty, insight.Channel);
                Assert.AreEqual(true, insight.IsEmailOptInCompleted);
                Assert.AreEqual(true, insight.IsSmsOptInCompleted);
            }

            _clientConfigFacade.Verify();
        }

        [TestMethod()]
        public void SubscriptionV1_GetSubscribeList_NoExistingSubscription_Account_Test()
        {
            _container.RegisterInstance(_cassandraRepository.Object);

            const string program = "Program1";
            const string programInsight = "Program1.BillToDate";
            //const string insight1 = "ServiceLevelCostThreshold";
            //const string insight2 = "ServiceLevelUsageThreshold";
            const string insight3 = "BillToDate";
            //const string insight4 = "CostToDate";
            //const string insight5 = "AccountProjectedCost";
            //const string insight6 = "ServiceProjectedCost";
            //const string insight7 = "Usage";
            //const string insight8 = "AccountLevelCostThreshold";
            //const string insight9 = "DayThreshold";
            //const string insight10 = "ServiceLevelTieredThresholdApproaching";
            //const string insight11 = "ServiceLevelTieredThresholdExceed";
            //const string commodity = "water";

            const int clientId = 87;
            const string customerId = "BTD001";
            const string accountId = "BTD001a1";
            const string premiseId = "BTD001a1p1";
            const string billCycleScheduleId = "Cycle01";
            const string meterId1 = "EM001";
            const string rateClass = "RG-1";
            const string servicePointId1 = "SP_BTD001a1";
            const string serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";

            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);


            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity> { mockBillingEntity1 };
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock no subscription data
            // mock no previous subscription subscribed
            IList<SubscriptionEntity> subList = new List<SubscriptionEntity>();
            _cassandraRepository.Setup(t => t.GetAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>())).Returns(Task.FromResult(subList));
            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();

            var subscribeList = new List<SubscribedInsight>();
            var subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = true,
                Level = "account",
                Name = programInsight,
                Channel = "Email",
                //IsEmailOptInCompleted = false,
                //IsSmsOptInCompleted = false
            };

            subscribeList.Add(subscribeInsight);
            var model = new SubscriptionModel(_container);
            var valid = true;
            var result = model.GetSubscribeList(clientId, customerId, subscribeList, ref valid);

            Assert.IsNotNull(result);
            Assert.AreEqual(11, result.Count);

            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(true, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);

            foreach (var insight in insights)
            {
                Assert.AreEqual(false, insight.IsSelected);
                Assert.AreNotEqual(insight3.ToLower(), insight.InsightTypeName.ToLower());
                Assert.AreEqual(accountId, insight.AccountId);
                Assert.AreEqual(customerId, insight.CustomerId);
                Assert.IsNotNull(insight.Channel);
                Assert.AreNotEqual(string.Empty, insight.Channel);
                Assert.AreEqual(true, insight.IsEmailOptInCompleted);
                Assert.AreEqual(true, insight.IsSmsOptInCompleted);
            }

            _clientConfigFacade.Verify();
        }

        [TestMethod()]
        public void SubscriptionV1_GetSubscribeList_NoExistingSubscription_Mixed_Test()
        {
            _container.RegisterInstance(_cassandraRepository.Object);

            const string programInsight1 = "Program1.ServiceLevelUsageThreshold";
            const string programInsight2 = "Program1.BillToDate";
            //const string program1 = "Program 1";
            //const string program2 = "Program 2";
            //const string insight1 = "ServiceLevelCostThreshold";
            const string insight2 = "ServiceLevelUsageThreshold";
            const string insight3 = "BillToDate";
            //const string insight4 = "CostToDate";
            //const string insight5 = "AccountProjectedCost";
            //const string insight6 = "ServiceProjectedCost";
            //const string insight7 = "Usage";
            //const string insight8 = "AccountLevelCostThreshold";
            //const string insight9 = "DayThreshold";
            //const string insight10 = "ServiceLevelTieredThresholdApproaching";
            //const string insight11 = "ServiceLevelTieredThresholdExceed";
            //const string commodity = "water";

            const int clientId = 87;
            const string customerId = "BTD001";
            const string accountId = "BTD001a1";
            const string premiseId = "BTD001a1p1";
            const string billCycleScheduleId = "Cycle01";
            const string meterId1 = "EM001";
            const string rateClass = "RG-1";
            const string servicePointId1 = "SP_BTD001a1";
            const string serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";

            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);


            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity> { mockBillingEntity1 };
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock no subscription data
            IList<SubscriptionEntity> subList = new List<SubscriptionEntity>();
            _cassandraRepository.Setup(t => t.GetAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>())).Returns(Task.FromResult(subList));
            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();

            var subscribeList = new List<SubscribedInsight>();
            var subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = true,
                Level = "service",
                Name = programInsight1,
                PremiseId = premiseId,
                //ServiceContractId = serviceContractId1,
                ServiceId = serviceContractId1,
                Channel = "Email",
                //IsEmailOptInCompleted = false,
                //IsSmsOptInCompleted = false
            };
            subscribeList.Add(subscribeInsight);

            subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = true,
                Level = "account",
                Name = programInsight2,
                PremiseId = premiseId,
                //ServiceContractId = serviceContractId1,
                ServiceId = serviceContractId1,
                Channel = "Email",
                //IsEmailOptInCompleted = false,
                //IsSmsOptInCompleted = false
            };
            subscribeList.Add(subscribeInsight);

            var model = new SubscriptionModel(_container);
            var valid = true;
            var result = model.GetSubscribeList(clientId, customerId, subscribeList, ref valid);

            Assert.IsNotNull(result);
            Assert.AreEqual(10, result.Count);

            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(1, subscribedProgram.Count);

            foreach (var program in subscribedProgram)
            {
                Assert.AreEqual(true, program.IsSelected);
                Assert.AreEqual(accountId, program.AccountId);
                Assert.AreEqual(customerId, program.CustomerId);
                Assert.IsNull(program.Channel);
            }

            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            foreach (var insight in insights)
            {
                Assert.AreEqual(false, insight.IsSelected);
                Assert.AreNotEqual(insight2.ToLower(), insight.InsightTypeName.ToLower());
                Assert.AreNotEqual(insight3.ToLower(), insight.InsightTypeName.ToLower());
                Assert.AreEqual(accountId, insight.AccountId);
                Assert.IsNotNull(insight.ServiceContractId);
                //Assert.IsNotNull(insight.ServicePointId);
                Assert.IsNotNull(insight.PremiseId);
                Assert.AreEqual(customerId, insight.CustomerId);
                Assert.IsNotNull(insight.Channel);
                Assert.AreNotEqual(string.Empty, insight.Channel);
                Assert.AreEqual(true, insight.IsEmailOptInCompleted);
                Assert.AreEqual(true, insight.IsSmsOptInCompleted);
            }
            _clientConfigFacade.Verify();
        }

        [TestMethod()]
        public void SubscriptionV1_GetSubscribeList_ExistingSubscription_Service_Test()
        {
            _container.RegisterInstance(_cassandraRepository.Object);

            const string program = "Program1";
            const string programInsight = "Program1.ServiceLevelCostThreshold";
            const string insight1 = "ServiceLevelCostThreshold";
            //const string  insight2 = "ServiceLevelUsageThreshold";
            const string insight3 = "BillToDate";
            const string insight4 = "CostToDate";
            const string insight5 = "AccountProjectedCost";
            const string insight6 = "ServiceProjectedCost";
            const string insight7 = "Usage";
            const string insight8 = "AccountLevelCostThreshold";
            const string insight9 = "DayThreshold";
            const string insight10 = "ServiceLevelTieredThresholdApproaching";
            const string insight11 = "ServiceLevelTieredThresholdExceed";
            //const string  commodity = "water";

            const int clientId = 87;
            const string customerId = "BTD001";
            const string accountId = "BTD001a1";
            const string premiseId = "BTD001a1p1";
            const string billCycleScheduleId = "Cycle01";
            const string meterId1 = "EM001";
            const string rateClass = "RG-1";
            const string servicePointId1 = "SP_BTD001a1";
            const string serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            const string channel = "email";

            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);


            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity> { mockBillingEntity1 };
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);

            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity> { mockProgramSubscription };
            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);

            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>
            {
                mockInsight1,
                mockInsight3,
                mockInsight4,
                mockInsight5,
                mockInsight6,
                mockInsight7,
                mockInsight8,
                mockInsight9,
                mockInsight10,
                mockInsight11
            };

            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));
            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();

            var subscribeList = new List<SubscribedInsight>();
            var subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = true,
                Level = "service",
                Name = programInsight,
                PremiseId = premiseId,
                //ServiceContractId = serviceContractId1,
                ServiceId = serviceContractId1,
                Channel = channel
            };

            subscribeList.Add(subscribeInsight);
            var model = new SubscriptionModel(_container);
            var valid = true;

            var result = model.GetSubscribeList(clientId, customerId, subscribeList, ref valid);

            Assert.IsNotNull(result);
            Assert.AreEqual(11, result.Count);

            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(true, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);

            foreach (var insight in insights)
            {
                Assert.AreEqual(
                    string.Equals(insight.InsightTypeName, insight1, StringComparison.CurrentCultureIgnoreCase),
                    insight.IsSelected);

                Assert.AreEqual(accountId, insight.AccountId);
                Assert.IsNotNull(insight.ServiceContractId);
                //Assert.IsNotNull(insight.ServicePointId);
                Assert.IsNotNull(insight.PremiseId);
                Assert.AreEqual(customerId, insight.CustomerId);
                Assert.IsNotNull(insight.Channel);
                Assert.AreNotEqual(string.Empty, insight.Channel);
                Assert.AreEqual(true, insight.IsEmailOptInCompleted);
                Assert.AreEqual(true, insight.IsSmsOptInCompleted);
            }
            _clientConfigFacade.Verify();
        }

        [TestMethod()]
        public void SubscriptionV1_GetSubscribeList_ExistingSubscription_Service2_Test()
        {
            _container.RegisterInstance(_cassandraRepository.Object);

            const string program = "Program1";
            const string programInsight2 = "Program1.ServiceLevelUsageThreshold";
            const string programInsight1 = "Program1.ServiceLevelCostThreshold";
            const string insight1 = "ServiceLevelCostThreshold";
            //const string  insight2 = "ServiceLevelUsageThreshold";
            const string insight3 = "BillToDate";
            const string insight4 = "CostToDate";
            const string insight5 = "AccountProjectedCost";
            const string insight6 = "ServiceProjectedCost";
            const string insight7 = "Usage";
            const string insight8 = "AccountLevelCostThreshold";
            const string insight9 = "DayThreshold";
            const string insight10 = "ServiceLevelTieredThresholdApproaching";
            const string insight11 = "ServiceLevelTieredThresholdExceed";
            //const string  commodity = "water";

            const int clientId = 87;
            const string customerId = "BTD001";
            const string accountId = "BTD001a1";
            const string premiseId = "BTD001a1p1";
            const string billCycleScheduleId = "Cycle01";
            const string meterId1 = "EM001";
            const string rateClass = "RG-1";
            const string servicePointId1 = "SP_BTD001a1";
            const string serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";

            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);


            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity> { mockBillingEntity1 };
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);

            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity> { mockProgramSubscription };
            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);

            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>
            {
                mockInsight1,
                mockInsight3,
                mockInsight4,
                mockInsight5,
                mockInsight6,
                mockInsight7,
                mockInsight8,
                mockInsight9,
                mockInsight10,
                mockInsight11
            };
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));
            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();

            var subscribeList = new List<SubscribedInsight>();
            var subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = true,
                Level = "service",
                Name = programInsight1,
                PremiseId = premiseId,
                //ServiceContractId = serviceContractId1,
                ServiceId = serviceContractId1,
                Channel = "Email",
                //IsEmailOptInCompleted = false,
                //IsSmsOptInCompleted = false
            };
            subscribeList.Add(subscribeInsight);
            subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = true,
                Level = "service",
                Name = programInsight2,
                PremiseId = premiseId,
                //ServiceContractId = serviceContractId1,
                ServiceId = serviceContractId1,
                Channel = "Email",
                //IsEmailOptInCompleted = false,
                //IsSmsOptInCompleted = false
            };
            subscribeList.Add(subscribeInsight);

            var model = new SubscriptionModel(_container);
            var valid = true;

            var result = model.GetSubscribeList(clientId, customerId, subscribeList, ref valid);

            Assert.IsNotNull(result);
            Assert.AreEqual(11, result.Count);

            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(true, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);

            foreach (var insight in insights)
            {
                Assert.AreEqual(string.Equals(insight.InsightTypeName, insight1, StringComparison.CurrentCultureIgnoreCase), insight.IsSelected);

                Assert.AreEqual(accountId, insight.AccountId);
                Assert.IsNotNull(insight.ServiceContractId);
                //Assert.IsNotNull(insight.ServicePointId);
                Assert.IsNotNull(insight.PremiseId);
                Assert.AreEqual(customerId, insight.CustomerId);
                Assert.IsNotNull(insight.Channel);
                Assert.AreNotEqual(string.Empty, insight.Channel);
                Assert.AreEqual(true, insight.IsEmailOptInCompleted);
                Assert.AreEqual(true, insight.IsSmsOptInCompleted);
            }
            _clientConfigFacade.Verify();
        }


        [TestMethod()]
        public void SubscriptionV1_GetSubscribeList_ExistingSubscription_Account_Test()
        {
            _container.RegisterInstance(_cassandraRepository.Object);

            const string program = "Program1";
            const string programInsight = "Program1.AccountProjectedCost";
            //const string  insight1 = "ServiceLevelCostThreshold";
            //const string  insight2 = "ServiceLevelUsageThreshold";
            //const string  insight3 = "BillToDate";
            //const string  insight4 = "CostToDate";
            const string insight5 = "AccountProjectedCost";
            //const string  insight6 = "ServiceProjectedCost";
            //const string  insight7 = "Usage";
            const string insight8 = "AccountLevelCostThreshold";
            //const string  insight9 = "DayThreshold";
            //const string  insight10 = "ServiceLevelTieredThresholdApproaching";
            //const string  insight11 = "ServiceLevelTieredThresholdExceed";
            //const string  commodity = "water";

            const int clientId = 87;
            const string customerId = "BTD001";
            const string accountId = "BTD001a1";
            const string premiseId = "BTD001a1p1";
            const string billCycleScheduleId = "Cycle01";
            const string meterId1 = "EM001";
            const string rateClass = "RG-1";
            const string servicePointId1 = "SP_BTD001a1";
            const string serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            const string channel = "email";

            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);


            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity> { mockBillingEntity1 };
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);

            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity> { mockProgramSubscription };
            // mock subscription insight entity
            //var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            //var mockInsight2 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight2, false, 0);
            //var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            //var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            //var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            //var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            //var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            //var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);

            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>
            {
                //mockInsight1,
               //mockInsight2,
                //mockInsight4,
                mockInsight5,
                //mockInsight6,
                //mockInsight7,
                mockInsight8
                //mockInsight9,
                //mockInsight10,
                //mockInsight11
            };
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));
            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();

            var subscribeList = new List<SubscribedInsight>();
            var subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = true,
                Level = "account",
                Name = programInsight,
                PremiseId = premiseId,
                ServiceId = serviceContractId1,
                //ServiceId = servicePointId1,
                Channel = channel
            };

            subscribeList.Add(subscribeInsight);
            var model = new SubscriptionModel(_container);
            var valid = true;

            var result = model.GetSubscribeList(clientId, customerId, subscribeList, ref valid);

            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Count);

            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(true, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);

            foreach (var insight in insights)
            {
                if (string.Equals(insight.InsightTypeName, insight5, StringComparison.CurrentCultureIgnoreCase))
                {
                    Assert.AreEqual(true, insight.IsSelected);
                    Assert.AreEqual(string.Empty, insight.ServiceContractId);
                    //Assert.AreEqual(string.Empty, insight.ServicePointId);
                    Assert.AreEqual(string.Empty, insight.PremiseId);
                }
                else
                {
                    Assert.AreEqual(false, insight.IsSelected);
                }

                Assert.AreEqual(accountId, insight.AccountId);
                Assert.AreEqual(customerId, insight.CustomerId);
                Assert.IsNotNull(insight.Channel);
                Assert.AreNotEqual(string.Empty, insight.Channel);
                Assert.AreEqual(true, insight.IsEmailOptInCompleted);
                Assert.AreEqual(true, insight.IsSmsOptInCompleted);
            }
            _clientConfigFacade.Verify();
        }

        [TestMethod()]
        public void SubscriptionV1_GetSubscribeList_ExistingSubscription_Account_subscribeandunsubscribe_Test()
        {
            _container.RegisterInstance(_cassandraRepository.Object);

            const string program = "Program1";
            const string programInsight = "Program1.AccountProjectedCost";
            const string programInsight2 = "Program1.BillToDate";
            //const string  insight1 = "ServiceLevelCostThreshold";
            //const string  insight2 = "ServiceLevelUsageThreshold";
            //const string  insight3 = "BillToDate";
            //const string  insight4 = "CostToDate";
            const string insight5 = "AccountProjectedCost";
            //const string  insight6 = "ServiceProjectedCost";
            //const string  insight7 = "Usage";
            const string insight8 = "AccountLevelCostThreshold";
            //const string  insight9 = "DayThreshold";
            //const string  insight10 = "ServiceLevelTieredThresholdApproaching";
            //const string  insight11 = "ServiceLevelTieredThresholdExceed";
            //const string  commodity = "water";

            const int clientId = 87;
            const string customerId = "BTD001";
            const string accountId = "BTD001a1";
            const string premiseId = "BTD001a1p1";
            const string billCycleScheduleId = "Cycle01";
            const string meterId1 = "EM001";
            const string rateClass = "RG-1";
            const string servicePointId1 = "SP_BTD001a1";
            const string serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            const string channel = "email";

            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity> { mockBillingEntity1 };
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);

            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity> { mockProgramSubscription };
            // mock subscription insight entity
            //var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            //var mockInsight2 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight2, false, 0);
            //var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            //var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            //var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            //var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            //var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            //var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);

            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>
            {
                //mockInsight1,
               //mockInsight2,
                //mockInsight4,
                mockInsight5,
                //mockInsight6,
                //mockInsight7,
                mockInsight8
                //mockInsight9,
                //mockInsight10,
                //mockInsight11
            };
            _cassandraRepository.SetupSequence(
                 t => t.GetAsync(It.IsAny<string>(),
                 It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                 .Returns(Task.FromResult(programList))
                 .Returns(Task.FromResult(insightList));
            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();

            var subscribeList = new List<SubscribedInsight>();
            var subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = true,
                Level = "account",
                Name = programInsight,
                PremiseId = premiseId,
                //ServiceContractId = serviceContractId1,
                ServiceId = serviceContractId1,
                Channel = channel
            };

            var subscribeInsight2 = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = false,
                Level = "account",
                Name = programInsight2,
                PremiseId = premiseId,
                //ServiceContractId = serviceContractId1,
                ServiceId = serviceContractId1
            };
            subscribeList.Add(subscribeInsight);
            subscribeList.Add(subscribeInsight2);

            var model = new SubscriptionModel(_container);
            var valid = true;

            var result = model.GetSubscribeList(clientId, customerId, subscribeList, ref valid);

            Assert.IsNotNull(result);
            Assert.AreEqual(4, result.Count);

            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(true, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);

            foreach (var insight in insights)
            {
                if (string.Equals(insight.InsightTypeName, insight5, StringComparison.CurrentCultureIgnoreCase))
                {
                    Assert.AreEqual(true, insight.IsSelected);
                    Assert.AreEqual(string.Empty, insight.ServiceContractId);
                    //Assert.AreEqual(string.Empty, insight.ServicePointId);
                    Assert.AreEqual(string.Empty, insight.PremiseId);
                }
                else
                {
                    Assert.AreEqual(false, insight.IsSelected);
                }

                Assert.AreEqual(accountId, insight.AccountId);
                Assert.AreEqual(customerId, insight.CustomerId);
                Assert.IsNotNull(insight.Channel);
                Assert.AreNotEqual(string.Empty, insight.Channel);
                Assert.AreEqual(true, insight.IsEmailOptInCompleted);
                Assert.AreEqual(true, insight.IsSmsOptInCompleted);
            }
            _clientConfigFacade.Verify();
        }

        [TestMethod()]
        public void SubscriptionV1_GetSubscribeList_ExistingSubscription_Account2_Test()
        {
            _container.RegisterInstance(_cassandraRepository.Object);

            const string program = "Program1";
            const string programInsight1 = "Program1.BillToDate";
            const string programInsight2 = "Program1.AccountProjectedCost";
            //const string  insight1 = "ServiceLevelCostThreshold";
            //const string  insight2 = "ServiceLevelUsageThreshold";
            //const string  insight3 = "BillToDate";
            //const string  insight4 = "CostToDate";
            const string insight5 = "AccountProjectedCost";
            //const string  insight6 = "ServiceProjectedCost";
            //const string  insight7 = "Usage";
            const string insight8 = "AccountLevelCostThreshold";
            //const string  insight9 = "DayThreshold";
            //const string  insight10 = "ServiceLevelTieredThresholdApproaching";
            //const string  insight11 = "ServiceLevelTieredThresholdExceed";
            //const string  commodity = "water";

            const int clientId = 87;
            const string customerId = "BTD001";
            const string accountId = "BTD001a1";
            const string premiseId = "BTD001a1p1";
            const string billCycleScheduleId = "Cycle01";
            const string meterId1 = "EM001";
            const string rateClass = "RG-1";
            const string servicePointId1 = "SP_BTD001a1";
            const string serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";

            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity> { mockBillingEntity1 };
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);

            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity> { mockProgramSubscription };
            // mock subscription insight entity
            //var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            //var mockInsight2 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight2, false, 0);
            //var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            //var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            //var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            //var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            //var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            //var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);

            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>
            {
                //mockInsight1,
                //mockInsight2,
                //mockInsight4,
                mockInsight5,
                //mockInsight6,
                //mockInsight7,
                mockInsight8
                //mockInsight9,
                //mockInsight10,
                //mockInsight11
            };
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));
            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();
            var subscribeList = new List<SubscribedInsight>();
            var subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = true,
                Level = "account",
                Name = programInsight1,
                PremiseId = premiseId,
                //ServiceContractId = serviceContractId1,
                ServiceId = serviceContractId1,
                Channel = "Email"
            };
            subscribeList.Add(subscribeInsight);

            subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = true,
                Level = "account",
                Name = programInsight2,
                PremiseId = premiseId,
                //ServiceContractId = serviceContractId1,
                ServiceId = serviceContractId1,
                Channel = "Email"
            };
            subscribeList.Add(subscribeInsight);

            var model = new SubscriptionModel(_container);
            var valid = true;

            var result = model.GetSubscribeList(clientId, customerId, subscribeList, ref valid);

            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Count);

            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(true, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);

            foreach (var insight in insights)
            {
                if (string.Equals(insight.InsightTypeName, insight5, StringComparison.CurrentCultureIgnoreCase))
                {
                    Assert.AreEqual(true, insight.IsSelected);
                    Assert.AreEqual(string.Empty, insight.ServiceContractId);
                    //Assert.AreEqual(string.Empty, insight.ServicePointId);
                    Assert.AreEqual(string.Empty, insight.PremiseId);
                }
                else
                {
                    Assert.AreEqual(false, insight.IsSelected);
                }

                Assert.AreEqual(accountId, insight.AccountId);
                Assert.AreEqual(customerId, insight.CustomerId);
                Assert.IsNotNull(insight.Channel);
                Assert.AreNotEqual(string.Empty, insight.Channel);
                Assert.AreEqual(true, insight.IsEmailOptInCompleted);
                Assert.AreEqual(true, insight.IsSmsOptInCompleted);
            }
            _clientConfigFacade.Verify();
        }


        [TestMethod()]
        public void SubscriptionV1_GetSubscribeList_ExistingSubscription_Mixed_Test()
        {
            _container.RegisterInstance(_cassandraRepository.Object);

            const string programInsight1 = "Program1.ServiceLevelCostThreshold";
            const string programInsight2 = "Program1.BillToDate";
            const string program1 = "Program 1";
            const string insight1 = "ServiceLevelCostThreshold";
            //const string  insight2 = "ServiceLevelUsageThreshold";
            const string insight3 = "BillToDate";
            const string insight4 = "CostToDate";
            //const string  insight5 = "AccountProjectedCost";
            const string insight6 = "ServiceProjectedCost";
            const string insight7 = "Usage";
            const string insight8 = "AccountLevelCostThreshold";
            const string insight9 = "DayThreshold";
            const string insight10 = "ServiceLevelTieredThresholdApproaching";
            const string insight11 = "ServiceLevelTieredThresholdExceed";
            //const string  commodity = "water";

            const int clientId = 87;
            const string customerId = "BTD001";
            const string accountId = "BTD001a1";
            const string premiseId = "BTD001a1p1";
            const string billCycleScheduleId = "Cycle01";
            const string meterId1 = "EM001";
            const string rateClass = "RG-1";
            const string servicePointId1 = "SP_BTD001a1";
            const string serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";

            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity> { mockBillingEntity1 };
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);

            // mock subscription program entity
            var mockProgramSubscription1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity> { mockProgramSubscription1 };
            // mock subscription insight entity
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program1, insight3, false, 0);
            //var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program1, insight5, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program1, insight8, false, 0);
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, insight1, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, insight4, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, insight7, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, insight11, false, 0);

            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>
            {
                mockInsight1,
                mockInsight3,
                mockInsight4,
                //mockInsight5
                mockInsight6,
                mockInsight7,
                mockInsight8,
                mockInsight9,
                mockInsight10,
                mockInsight11
            };
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));
            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();

            var subscribeList = new List<SubscribedInsight>();
            var subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = true,
                Level = "service",
                Name = programInsight1,
                PremiseId = premiseId,
                //ServiceContractId = serviceContractId1,
                ServiceId = serviceContractId1,
                Channel = "Email"
            };
            subscribeList.Add(subscribeInsight);

            subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = true,
                Level = "account",
                Name = programInsight2,
                PremiseId = premiseId,
                //ServiceContractId = serviceContractId1,
                ServiceId = serviceContractId1,
                Channel = "Email"
            };
            subscribeList.Add(subscribeInsight);

            var model = new SubscriptionModel(_container);
            var valid = true;

            var result = model.GetSubscribeList(clientId, customerId, subscribeList, ref valid);

            Assert.IsNotNull(result);
            Assert.AreEqual(10, result.Count);

            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(1, subscribedProgram.Count);
            foreach (var program in subscribedProgram)
            {
                Assert.AreEqual(true, program.IsSelected);
                Assert.AreEqual(accountId, program.AccountId);
                Assert.AreEqual(customerId, program.CustomerId);
                Assert.IsNull(program.Channel);
            }

            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            foreach (var insight in insights)
            {
                if (string.Equals(insight.InsightTypeName, insight3, StringComparison.CurrentCultureIgnoreCase))
                {
                    Assert.AreEqual(true, insight.IsSelected);
                    Assert.AreEqual(string.Empty, insight.ServiceContractId);
                    //Assert.AreEqual(string.Empty, insight.ServicePointId);
                    Assert.AreEqual(string.Empty, insight.PremiseId);
                }
                else if (string.Equals(insight.InsightTypeName, insight1, StringComparison.CurrentCultureIgnoreCase))
                {
                    Assert.AreEqual(true, insight.IsSelected);
                    Assert.IsNotNull(insight.ServiceContractId);
                    //Assert.IsNotNull(insight.ServicePointId);
                    Assert.IsNotNull(insight.PremiseId);
                }
                else
                {
                    Assert.AreEqual(false, insight.IsSelected);
                }

                Assert.AreEqual(accountId, insight.AccountId);
                if (!string.IsNullOrEmpty(insight.ServiceContractId))
                {
                    Assert.IsNotNull(insight.ServiceContractId);
                    //Assert.IsNotNull(insight.ServicePointId);
                    Assert.IsNotNull(insight.PremiseId);
                }
                Assert.AreEqual(customerId, insight.CustomerId);
                Assert.IsNotNull(insight.Channel);
                Assert.AreNotEqual(string.Empty, insight.Channel);
            }
            _clientConfigFacade.Verify();
        }

        [TestMethod()]
        public void SubscriptionV1_GetSubscribeList_ExistingSubscription_Mixed2_Test()
        {
            _container.RegisterInstance(_cassandraRepository.Object);

            const string programInsight1 = "Program1.ServiceLevelCostThreshold";
            const string programInsight2 = "Program1.ServiceLevelUsageThreshold";
            const string programInsight3 = "Program1.BillToDate";
            const string program1 = "Program1";
            const string insight1 = "ServiceLevelCostThreshold";
            //const string  insight2 = "ServiceLevelUsageThreshold";
            const string insight3 = "BillToDate";
            const string insight4 = "CostToDate";
            //const string  insight5 = "AccountProjectedCost";
            const string insight6 = "ServiceProjectedCost";
            const string insight7 = "Usage";
            //const string  insight8 = "AccountLevelCostThreshold";
            const string insight9 = "DayThreshold";
            const string insight10 = "ServiceLevelTieredThresholdApproaching";
            const string insight11 = "ServiceLevelTieredThresholdExceed";
            //const string  commodity = "water";

            const int clientId = 87;
            const string customerId = "BTD001";
            const string accountId = "BTD001a1";
            const string premiseId = "BTD001a1p1";
            const string billCycleScheduleId = "Cycle01";
            const string meterId1 = "EM001";
            const string rateClass = "RG-1";
            const string servicePointId1 = "SP_BTD001a1";
            const string serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";

            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity> { mockBillingEntity1 };
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);

            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity> { mockProgramSubscription };
            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, insight1, false, 0);
            //var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, insight4, false, 0);
            //var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, insight7, false, 0);
            //var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, insight11, false, 0);

            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>
            {
                mockInsight1,
                //mockInsight3,
                mockInsight4,
                //mockInsight5
                mockInsight6,
                mockInsight7,
                //mockInsight8,
                mockInsight9,
                mockInsight10,
                mockInsight11
            };
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));
            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();

            var subscribeList = new List<SubscribedInsight>();
            var subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = true,
                Level = "service",
                Name = programInsight1,
                PremiseId = premiseId,
                //ServiceContractId = serviceContractId1,
                ServiceId = serviceContractId1,
                Channel = "Email"
            };
            subscribeList.Add(subscribeInsight);

            subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = true,
                Level = "service",
                Name = programInsight2,
                PremiseId = premiseId,
                //ServiceContractId = serviceContractId1,
                ServiceId = serviceContractId1,
                Channel = "Email",
                //IsEmailOptInCompleted = false,
                //IsSmsOptInCompleted = false
            };
            subscribeList.Add(subscribeInsight);

            subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = true,
                Level = "account",
                Name = programInsight3,
                PremiseId = premiseId,
                //ServiceContractId = serviceContractId1,
                ServiceId = serviceContractId1,
                Channel = "Email",
                //IsEmailOptInCompleted = false,
                //IsSmsOptInCompleted = false
            };
            subscribeList.Add(subscribeInsight);

            var model = new SubscriptionModel(_container);
            var valid = true;

            var result = model.GetSubscribeList(clientId, customerId, subscribeList, ref valid);
            Assert.IsNotNull(result);
            Assert.AreEqual(8, result.Count);

            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            foreach (var program in subscribedProgram)
            {
                Assert.AreEqual(true, program.IsSelected);
                Assert.AreEqual(accountId, program.AccountId);
                Assert.AreEqual(customerId, program.CustomerId);
                Assert.IsNull(program.Channel);
            }

            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            foreach (var insight in insights)
            {
                if (string.Equals(insight.InsightTypeName, insight3, StringComparison.CurrentCultureIgnoreCase))
                {
                    Assert.AreEqual(true, insight.IsSelected);
                    Assert.AreEqual(string.Empty, insight.ServiceContractId);
                    //Assert.AreEqual(string.Empty, insight.ServicePointId);
                    Assert.AreEqual(string.Empty, insight.PremiseId);
                }
                else if (string.Equals(insight.InsightTypeName, insight1, StringComparison.CurrentCultureIgnoreCase))
                {
                    Assert.AreEqual(true, insight.IsSelected);
                    Assert.AreNotEqual(string.Empty, insight.ServiceContractId);
                    //Assert.AreNotEqual(string.Empty, insight.ServicePointId);
                    Assert.AreNotEqual(string.Empty, insight.PremiseId);
                }
                else
                {
                    Assert.AreEqual(false, insight.IsSelected);
                }

                Assert.AreEqual(accountId, insight.AccountId);
                if (!string.IsNullOrEmpty(insight.ServiceContractId))
                {
                    Assert.AreNotEqual(string.Empty, insight.ServiceContractId);
                    //Assert.AreNotEqual(string.Empty, insight.ServicePointId);
                    Assert.AreNotEqual(string.Empty, insight.PremiseId);
                }
                Assert.AreEqual(customerId, insight.CustomerId);
                Assert.IsNotNull(insight.Channel);
                Assert.AreNotEqual(string.Empty, insight.Channel);
            }
            _clientConfigFacade.Verify();
        }


        [TestMethod()]
        public void SubscriptionV1_GetSubscribeList_UnScbscribeAllExistingSubscription_Service_Test()
        {
            _container.RegisterInstance(_cassandraRepository.Object);

            const string program = "Program1";
            const string programInsight = "Program1.ServiceLevelUsageThreshold";
            const string insight1 = "ServiceLevelCostThreshold";
            // const string  insight2 = "ServiceLevelUsageThreshold";
            const string insight3 = "BillToDate";
            const string insight4 = "CostToDate";
            const string insight5 = "AccountProjectedCost";
            const string insight6 = "ServiceProjectedCost";
            const string insight7 = "Usage";
            const string insight8 = "AccountLevelCostThreshold";
            const string insight9 = "DayThreshold";
            const string insight10 = "ServiceLevelTieredThresholdApproaching";
            const string insight11 = "ServiceLevelTieredThresholdExceed";
            // const string  commodity = "water";

            const int clientId = 87;
            const string customerId = "BTD001";
            const string accountId = "BTD001a1";
            const string premiseId = "BTD001a1p1";
            const string billCycleScheduleId = "Cycle01";
            const string meterId1 = "EM001";
            const string rateClass = "RG-1";
            const string servicePointId1 = "SP_BTD001a1";
            const string serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";

            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity> { mockBillingEntity1 };
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);

            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity> { mockProgramSubscription };
            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);

            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>
            {
                mockInsight1,
                mockInsight3,
                mockInsight4,
                mockInsight5,
                mockInsight6,
                mockInsight7,
                mockInsight8,
                mockInsight9,
                mockInsight10,
                mockInsight11
            };
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));
            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();
            var subscribeList = new List<SubscribedInsight>();
            var subscription = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = false,
                Level = "service",
                Name = programInsight,
                PremiseId = premiseId,
                //ServiceContractId = serviceContractId1,
                ServiceId = serviceContractId1
            };

            subscribeList.Add(subscription);
            var model = new SubscriptionModel(_container);
            var valid = true;

            var result = model.GetSubscribeList(clientId, customerId, subscribeList, ref valid);

            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count);

            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));

            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(false, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);

            foreach (var insight in insights)
            {
                Assert.AreEqual(false, insight.IsSelected);
                Assert.AreEqual(accountId, insight.AccountId);
                Assert.IsNotNull(insight.ServiceContractId);
                Assert.AreEqual(customerId, insight.CustomerId);
                Assert.IsNotNull(insight.Channel);
                Assert.AreNotEqual(string.Empty, insight.Channel);
                Assert.AreEqual(true, insight.IsEmailOptInCompleted);
                Assert.AreEqual(true, insight.IsSmsOptInCompleted);
            }
            _clientConfigFacade.Verify();


        }

        #region "SubsribeInsights"
        [TestMethod()]
        public void SubscriptionV1_SubscribeInsights_Subscription_noExistingSubscription_Account_Test()
        {
            _container.RegisterInstance(_cassandraRepository.Object);

            var programInsight1 = "Program1.BillToDate";

            const int clientId = 87;
            const string customerId = "BTD001";
            const string accountId = "BTD001a1";
            const string premiseId = "BTD001a1p1";
            const string billCycleScheduleId = "Cycle01";
            const string meterId1 = "EM001";
            const string rateClass = "RG-1";
            const string servicePointId1 = "SP_BTD001a1";
            const string serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            const string channel = "email";
            var firstname = "Anjana";
            var lastname = "Khaire";
            var addr1 = "16 Laurel Ave";
            var addr2 = "Suite 100";
            var city = "Wellesley";
            var state = "MA";
            var zip = "02481";
            var email = "";
            var phone = "";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity> { mockBillingEntity1 };
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);


            // mock no previous subscription subscribed
            IList<SubscriptionEntity> subList = new List<SubscriptionEntity>();
            _cassandraRepository.Setup(t => t.GetAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>())).Returns(Task.FromResult(subList));
            _cassandraRepository.Setup(t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>())).ReturnsAsync(null);

            // mock customer entity
            var mockCustomer = TestHelpers.CreateMockResidentialCustomerEntityData(clientId, customerId, firstname, lastname,
                addr1, addr2, city, state, zip, email, phone);
            _cassandraRepository.Setup(c => c.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<CustomerEntity, bool>>>()))
                .Returns(Task.FromResult(mockCustomer));

            // mock insert subscription
            _cassandraRepository.Setup(c => c.InsertOrUpdateAsync(It.IsAny<SubscriptionEntity>(), It.IsAny<string>(), It.IsAny<char>()))
                .Returns(Task.FromResult(true));
            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();

            var subscribeList = new List<SubscribedInsight>();
            var subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = true,
                Level = "account",
                Name = programInsight1,
                PremiseId = premiseId,
                //ServiceContractId = serviceContractId1,
                ServiceId = serviceContractId1,
                Channel = channel
            };
            subscribeList.Add(subscribeInsight);

            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };

            var post = new SubscriptionPostRequest
            {
                Customer = new SubscribedCustomerInsight
                {
                    Id = customerId,
                    Insights = subscribeList
                }
            };

            var model = new SubscriptionModel(_container);

            var result = model.SubscribeInsights(clientUser, post);
            Thread.Sleep(10000);
            _cassandraRepository.Verify(c => c.InsertOrUpdateAsync(It.IsAny<SubscriptionEntity>(), It.IsAny<string>(), It.IsAny<char>()), Times.AtLeast(22));

            Assert.IsNotNull(result);
            _clientConfigFacade.Verify();
        }

        [TestMethod()]
        public void SubscriptionV1_SubscribeInsights_Subscription_noExistingSubscription_Service_Test()
        {
            _container.RegisterInstance(_cassandraRepository.Object);

            var programInsight1 = "Program1.ServiceLevelCostThreshold";

            const int clientId = 87;
            const string customerId = "BTD001";
            const string accountId = "BTD001a1";
            const string premiseId = "BTD001a1p1";
            const string billCycleScheduleId = "Cycle01";
            const string meterId1 = "EM001";
            const string rateClass = "RG-1";
            const string servicePointId1 = "SP_BTD001a1";
            const string serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            const string channel = "email";
            var firstname = "Anjana";
            var lastname = "Khaire";
            var addr1 = "16 Laurel Ave";
            var addr2 = "Suite 100";
            var city = "Wellesley";
            var state = "MA";
            var zip = "02481";
            var email = "";
            var phone = "";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity> { mockBillingEntity1 };
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);


            // mock no previous subscription subscribed
            IList<SubscriptionEntity> subList = new List<SubscriptionEntity>();
            _cassandraRepository.Setup(t => t.GetAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>())).Returns(Task.FromResult(subList));
            _cassandraRepository.Setup(t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>())).ReturnsAsync(null);

            // mock customer entity
            var mockCustomer = TestHelpers.CreateMockResidentialCustomerEntityData(clientId, customerId, firstname, lastname,
                addr1, addr2, city, state, zip, email, phone);
            _cassandraRepository.Setup(c => c.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<CustomerEntity, bool>>>()))
                .Returns(Task.FromResult(mockCustomer));

            // mock insert subscription
            _cassandraRepository.Setup(c => c.InsertOrUpdateAsync(It.IsAny<SubscriptionEntity>(), It.IsAny<string>(), It.IsAny<char>()))
                .Returns(Task.FromResult(true));
            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();

            var subscribeList = new List<SubscribedInsight>();
            var subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = true,
                Level = "service",
                Name = programInsight1,
                PremiseId = premiseId,
                //ServiceContractId = serviceContractId1,
                ServiceId = serviceContractId1,
                Channel = channel
            };
            subscribeList.Add(subscribeInsight);

            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };

            var post = new SubscriptionPostRequest
            {
                Customer = new SubscribedCustomerInsight
                {
                    Id = customerId,
                    Insights = subscribeList
                }
            };

            var model = new SubscriptionModel(_container);

            var result = model.SubscribeInsights(clientUser, post);
            Thread.Sleep(10000);
            _cassandraRepository.Verify(c => c.InsertOrUpdateAsync(It.IsAny<SubscriptionEntity>(), It.IsAny<string>(), It.IsAny<char>()), Times.AtLeast(22));

            Assert.IsNotNull(result);
            _clientConfigFacade.Verify();
        }

        [TestMethod()]
        public void SubscriptionV1_SubscribeInsights_Subscription_Service2_Test()
        {
            _container.RegisterInstance(_cassandraRepository.Object);

            var program = "Program1";
            var programInsight1 = "Program1.ServiceLevelCostThreshold";
            var programInsight2 = "Program1.ServiceLevelUsageThreshold";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";

            const int clientId = 87;
            const string customerId = "BTD001";
            const string accountId = "BTD001a1";
            const string premiseId = "BTD001a1p1";
            const string billCycleScheduleId = "Cycle01";
            const string meterId1 = "EM001";
            const string rateClass = "RG-1";
            const string servicePointId1 = "SP_BTD001a1";
            const string serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            const string channel = "email";
            var firstname = "Anjana";
            var lastname = "Khaire";
            var addr1 = "16 Laurel Ave";
            var addr2 = "Suite 100";
            var city = "Wellesley";
            var state = "MA";
            var zip = "02481";
            var email = "";
            var phone = "";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity> { mockBillingEntity1 };
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);


            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, true, 0);
            var mockInsight2 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight2, false, 0);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight2);
            insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            insightList.Add(mockInsight8);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));
            _cassandraRepository.Setup(
                t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .ReturnsAsync(mockInsight1);
            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();

            // mock customer entity
            var mockCustomer = TestHelpers.CreateMockResidentialCustomerEntityData(clientId, customerId, firstname, lastname,
                addr1, addr2, city, state, zip, email, phone);
            _cassandraRepository.Setup(c => c.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<CustomerEntity, bool>>>()))
                .Returns(Task.FromResult(mockCustomer));

            // mock insert subscription
            _cassandraRepository.Setup(c => c.InsertOrUpdateAsync(It.IsAny<SubscriptionEntity>(), It.IsAny<string>(), It.IsAny<char>()))
                .Returns(Task.FromResult(true));

            var subscribeList = new List<SubscribedInsight>();
            var subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = true,
                Level = "service",
                Name = programInsight1,
                PremiseId = premiseId,
                //ServiceContractId = serviceContractId1,
                ServiceId = serviceContractId1,
                Channel = channel
            };
            subscribeList.Add(subscribeInsight);
            subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = true,
                Level = "service",
                Name = programInsight2,
                PremiseId = premiseId,
                //ServiceContractId = serviceContractId1,
                ServiceId = serviceContractId1,
                Channel = channel
            };
            subscribeList.Add(subscribeInsight);

            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };

            var post = new SubscriptionPostRequest
            {
                Customer = new SubscribedCustomerInsight
                {
                    Id = customerId,
                    Insights = subscribeList
                }
            };

            var model = new SubscriptionModel(_container);

            var result = model.SubscribeInsights(clientUser, post);

            Thread.Sleep(10000);
            _cassandraRepository.Verify(c => c.InsertOrUpdateAsync(It.IsAny<SubscriptionEntity>(), It.IsAny<string>(), It.IsAny<char>()),Times.AtLeast(22));

            Assert.IsNotNull(result);
            _clientConfigFacade.Verify();
        }

        [TestMethod()]
        public void SubscriptionV1_SubscribeInsights_Subscription_Service3_Test()
        {
            _container.RegisterInstance(_cassandraRepository.Object);

            var program = "Program1";
            var programInsight1 = "Program1.CostToDate";
            var programInsight2 = "Program1.ServiceLevelUsageThreshold";
            //var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";

            const int clientId = 87;
            const string customerId = "BTD001";
            const string accountId = "BTD001a1";
            const string premiseId = "BTD001a1p1";
            const string billCycleScheduleId = "Cycle01";
            const string meterId1 = "EM001";
            const string rateClass = "RG-1";
            const string servicePointId1 = "SP_BTD001a1";
            const string serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            const string channel = "email";
            var firstname = "Anjana";
            var lastname = "Khaire";
            var addr1 = "16 Laurel Ave";
            var addr2 = "Suite 100";
            var city = "Wellesley";
            var state = "MA";
            var zip = "02481";
            var email = "";
            var phone = "";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity> { mockBillingEntity1 };
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);


            // mock no previous subscription subscribed
            IList<SubscriptionEntity> subList = new List<SubscriptionEntity>();
            _cassandraRepository.Setup(t => t.GetAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>())).Returns(Task.FromResult(subList));
            _cassandraRepository.Setup(t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>())).ReturnsAsync(null);

            // mock customer entity
            var mockCustomer = TestHelpers.CreateMockResidentialCustomerEntityData(clientId, customerId, firstname, lastname,
                addr1, addr2, city, state, zip, email, phone);
            _cassandraRepository.Setup(c => c.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<CustomerEntity, bool>>>()))
                .Returns(Task.FromResult(mockCustomer));

            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity
            //var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, true, 0);
            var mockInsight2 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight2, true, 0);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight2);
            insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            insightList.Add(mockInsight8);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));
            _cassandraRepository.Setup(
                t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .ReturnsAsync(mockInsight2);
            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();

            var subscribeList = new List<SubscribedInsight>();
            var subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = true,
                Level = "service",
                Name = programInsight1,
                PremiseId = premiseId,
                //ServiceContractId = serviceContractId1,
                ServiceId = serviceContractId1,
                Channel = channel
            };
            subscribeList.Add(subscribeInsight);
            subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = false,
                Level = "service",
                Name = programInsight2,
                PremiseId = premiseId,
                //ServiceContractId = serviceContractId1,
                ServiceId = serviceContractId1,
                Channel = channel
            };
            subscribeList.Add(subscribeInsight);

            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };

            var post = new SubscriptionPostRequest
            {
                Customer = new SubscribedCustomerInsight
                {
                    Id = customerId,
                    Insights = subscribeList
                }
            };

            var model = new SubscriptionModel(_container);

            var result = model.SubscribeInsights(clientUser, post);
            Thread.Sleep(10000);
            _cassandraRepository.Verify(c => c.InsertOrUpdateAsync(It.IsAny<SubscriptionEntity>(), It.IsAny<string>(), It.IsAny<char>()), Times.AtLeast(22));
            Assert.IsNotNull(result);
            _clientConfigFacade.Verify();
        }

        [TestMethod()]
        public void SubscriptionV1_SubscribeInsights_Subscription_Service4_Test()
        {
            _container.RegisterInstance(_cassandraRepository.Object);

            var program = "Program1";
            var programInsight1 = "Program1.ServiceLevelCostThreshold";
            //var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";

            const int clientId = 87;
            const string customerId = "BTD001";
            const string accountId = "BTD001a1";
            const string premiseId = "BTD001a1p1";
            const string billCycleScheduleId = "Cycle01";
            const string meterId1 = "EM001";
            const string rateClass = "RG-1";
            const string servicePointId1 = "SP_BTD001a1";
            const string serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            //const string channel = "email";
            var firstname = "Anjana";
            var lastname = "Khaire";
            var addr1 = "16 Laurel Ave";
            var addr2 = "Suite 100";
            var city = "Wellesley";
            var state = "MA";
            var zip = "02481";
            var email = "";
            var phone = "";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity> { mockBillingEntity1 };
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity
            //var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            var mockInsight2 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight2, true, 0);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight2);
            insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            insightList.Add(mockInsight8);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));
            _cassandraRepository.Setup(t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>())).ReturnsAsync(null);

            // mock customer entity
            var mockCustomer = TestHelpers.CreateMockResidentialCustomerEntityData(clientId, customerId, firstname, lastname,
                addr1, addr2, city, state, zip, email, phone);
            _cassandraRepository.Setup(c => c.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<CustomerEntity, bool>>>()))
                .Returns(Task.FromResult(mockCustomer));

            // mock insert subscription
            _cassandraRepository.Setup(c => c.InsertOrUpdateAsync(It.IsAny<SubscriptionEntity>(), It.IsAny<string>(), It.IsAny<char>()))
                .Returns(Task.FromResult(true));
            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();
            var subscribeList = new List<SubscribedInsight>();
            var subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = false,
                Level = "service",
                Name = programInsight1,
                PremiseId = premiseId,
                //ServiceContractId = serviceContractId1,
                ServiceId = serviceContractId1
            };
            subscribeList.Add(subscribeInsight);

            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };

            var post = new SubscriptionPostRequest
            {
                Customer = new SubscribedCustomerInsight
                {
                    Id = customerId,
                    Insights = subscribeList
                }
            };

            var model = new SubscriptionModel(_container);

            var result = model.SubscribeInsights(clientUser, post);
            Thread.Sleep(10000);
            _cassandraRepository.Verify(c => c.InsertOrUpdateAsync(It.IsAny<SubscriptionEntity>(), It.IsAny<string>(), It.IsAny<char>()), Times.AtLeast(2));

            Assert.IsNotNull(result);
            _clientConfigFacade.Verify();
        }

        [TestMethod()]
        public void SubscriptionV1_SubscribeInsights_Subscription_Account_Test()
        {
            _container.RegisterInstance(_cassandraRepository.Object);

            var program = "Program1";
            var programInsight1 = "Program1.BillToDate";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";

            const int clientId = 87;
            const string customerId = "BTD001";
            const string accountId = "BTD001a1";
            const string premiseId = "BTD001a1p1";
            const string billCycleScheduleId = "Cycle01";
            const string meterId1 = "EM001";
            const string rateClass = "RG-1";
            const string servicePointId1 = "SP_BTD001a1";
            const string serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            const string channel = "email";
            var firstname = "Anjana";
            var lastname = "Khaire";
            var addr1 = "16 Laurel Ave";
            var addr2 = "Suite 100";
            var city = "Wellesley";
            var state = "MA";
            var zip = "02481";
            var email = "";
            var phone = "";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity> { mockBillingEntity1 };
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            var mockInsight2 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight2, false, 0);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            insightList.Add(mockInsight2);
            insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            insightList.Add(mockInsight8);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));
            _cassandraRepository.Setup(t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>())).ReturnsAsync(mockInsight3);

            // mock customer entity
            var mockCustomer = TestHelpers.CreateMockResidentialCustomerEntityData(clientId, customerId, firstname, lastname,
                addr1, addr2, city, state, zip, email, phone);
            _cassandraRepository.Setup(c => c.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<CustomerEntity, bool>>>()))
                .Returns(Task.FromResult(mockCustomer));

            // mock insert subscription
            _cassandraRepository.Setup(c => c.InsertOrUpdateAsync(It.IsAny<SubscriptionEntity>(), It.IsAny<string>(), It.IsAny<char>()))
                .Returns(Task.FromResult(true));
            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();

            var subscribeList = new List<SubscribedInsight>();
            var subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = true,
                Level = "account",
                Name = programInsight1,
                PremiseId = premiseId,
                //ServiceContractId = serviceContractId1,
                ServiceId = serviceContractId1,
                Channel = channel
            };
            subscribeList.Add(subscribeInsight);

            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };

            var post = new SubscriptionPostRequest
            {
                Customer = new SubscribedCustomerInsight
                {
                    Id = customerId,
                    Insights = subscribeList
                }
            };

            var model = new SubscriptionModel(_container);

            var result = model.SubscribeInsights(clientUser, post);
            Thread.Sleep(10000);
            _cassandraRepository.Verify(c => c.InsertOrUpdateAsync(It.IsAny<SubscriptionEntity>(), It.IsAny<string>(), It.IsAny<char>()), Times.AtLeast(24));
            
            Assert.IsNotNull(result);
            _clientConfigFacade.Verify();
        }

        [TestMethod()]
        public void SubscriptionV1_SubscribeInsights_Subscription_Account2_Test()
        {
            _container.RegisterInstance(_cassandraRepository.Object);

            var program = "Program1";
            var programInsight1 = "Program1.BillToDate";
            var programInsight2 = "Program1.AccountProjectedCost";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";

            const int clientId = 87;
            const string customerId = "BTD001";
            const string accountId = "BTD001a1";
            const string premiseId = "BTD001a1p1";
            const string billCycleScheduleId = "Cycle01";
            const string meterId1 = "EM001";
            const string rateClass = "RG-1";
            const string servicePointId1 = "SP_BTD001a1";
            const string serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            const string channel = "email";
            var firstname = "Anjana";
            var lastname = "Khaire";
            var addr1 = "16 Laurel Ave";
            var addr2 = "Suite 100";
            var city = "Wellesley";
            var state = "MA";
            var zip = "02481";
            var email = "";
            var phone = "";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity> { mockBillingEntity1 };
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            var mockInsight2 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight2, false, 0);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, true, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            insightList.Add(mockInsight2);
            insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            insightList.Add(mockInsight8);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));
            _cassandraRepository.Setup(t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>())).ReturnsAsync(mockInsight3);

            // mock customer entity
            var mockCustomer = TestHelpers.CreateMockResidentialCustomerEntityData(clientId, customerId, firstname, lastname,
                addr1, addr2, city, state, zip, email, phone);
            _cassandraRepository.Setup(c => c.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<CustomerEntity, bool>>>()))
                .Returns(Task.FromResult(mockCustomer));

            // mock insert subscription
            _cassandraRepository.Setup(c => c.InsertOrUpdateAsync(It.IsAny<SubscriptionEntity>(), It.IsAny<string>(), It.IsAny<char>()))
                .Returns(Task.FromResult(true));
            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();

            var subscribeList = new List<SubscribedInsight>();
            var subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = true,
                Level = "account",
                Name = programInsight1,
                PremiseId = premiseId,
                //ServiceContractId = serviceContractId1,
                ServiceId = serviceContractId1,
                Channel = channel
            };
            subscribeList.Add(subscribeInsight);
            subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = true,
                Level = "account",
                Name = programInsight2,
                PremiseId = premiseId,
                //ServiceContractId = serviceContractId1,
                ServiceId = serviceContractId1,
                Channel = channel
            };
            subscribeList.Add(subscribeInsight);

            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };

            var post = new SubscriptionPostRequest
            {
                Customer = new SubscribedCustomerInsight
                {
                    Id = customerId,
                    Insights = subscribeList
                }
            };

            var model = new SubscriptionModel(_container);

            var result = model.SubscribeInsights(clientUser, post);
            Thread.Sleep(10000);
            _cassandraRepository.Verify(c => c.InsertOrUpdateAsync(It.IsAny<SubscriptionEntity>(), It.IsAny<string>(), It.IsAny<char>()), Times.AtLeast(22));

            Assert.IsNotNull(result);
            _clientConfigFacade.Verify();
        }

        [TestMethod()]
        public void SubscriptionV1_SubscribeInsights_Subscription_Account3_Test()
        {
            _container.RegisterInstance(_cassandraRepository.Object);

            var program = "Program1";
            var programInsight1 = "Program1.AccountLevelCostThreshold";
            var programInsight2 = "Program1.AccountProjectedCost";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";

            const int clientId = 87;
            const string customerId = "BTD001";
            const string accountId = "BTD001a1";
            const string premiseId = "BTD001a1p1";
            const string billCycleScheduleId = "Cycle01";
            const string meterId1 = "EM001";
            const string rateClass = "RG-1";
            const string servicePointId1 = "SP_BTD001a1";
            const string serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            const string channel = "email";
            var firstname = "Anjana";
            var lastname = "Khaire";
            var addr1 = "16 Laurel Ave";
            var addr2 = "Suite 100";
            var city = "Wellesley";
            var state = "MA";
            var zip = "02481";
            var email = "";
            var phone = "";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity> { mockBillingEntity1 };
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            var mockInsight2 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight2, false, 0);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, true, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, true, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            insightList.Add(mockInsight2);
            insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            insightList.Add(mockInsight8);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));
            _cassandraRepository.Setup(t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>())).ReturnsAsync(mockInsight3);

            // mock customer entity
            var mockCustomer = TestHelpers.CreateMockResidentialCustomerEntityData(clientId, customerId, firstname, lastname,
                addr1, addr2, city, state, zip, email, phone);
            _cassandraRepository.Setup(c => c.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<CustomerEntity, bool>>>()))
                .Returns(Task.FromResult(mockCustomer));

            // mock insert subscription
            _cassandraRepository.Setup(c => c.InsertOrUpdateAsync(It.IsAny<SubscriptionEntity>(), It.IsAny<string>(), It.IsAny<char>()))
                .Returns(Task.FromResult(true));
            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();

            var subscribeList = new List<SubscribedInsight>();
            var subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = true,
                Level = "account",
                Name = programInsight1,
                PremiseId = premiseId,
                //ServiceContractId = serviceContractId1,
                ServiceId = serviceContractId1,
                Channel = channel
            };
            subscribeList.Add(subscribeInsight);
            subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = true,
                Level = "account",
                Name = programInsight2,
                PremiseId = premiseId,
                //ServiceContractId = serviceContractId1,
                ServiceId = serviceContractId1,
                Channel = channel
            };
            subscribeList.Add(subscribeInsight);

            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };

            var post = new SubscriptionPostRequest
            {
                Customer = new SubscribedCustomerInsight
                {
                    Id = customerId,
                    Insights = subscribeList
                }
            };

            var model = new SubscriptionModel(_container);

            var result = model.SubscribeInsights(clientUser, post);
            Thread.Sleep(10000);
            _cassandraRepository.Verify(c => c.InsertOrUpdateAsync(It.IsAny<SubscriptionEntity>(), It.IsAny<string>(), It.IsAny<char>()), Times.AtLeast(20));

            Assert.IsNotNull(result);
            _clientConfigFacade.Verify();
        }


        [TestMethod()]
        public void SubscriptionV1_SubscribeInsights_UnSubscribe_Test()
        {
            _container.RegisterInstance(_cassandraRepository.Object);

            var program = "Program1";
            var programInsight1 = "Program1.AccountLevelCostThreshold";
            //var programInsight2 = "Program 1.AccountProjectedCost";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";

            const int clientId = 87;
            const string customerId = "BTD001";
            const string accountId = "BTD001a1";
            const string premiseId = "BTD001a1p1";
            const string billCycleScheduleId = "Cycle01";
            const string meterId1 = "EM001";
            const string rateClass = "RG-1";
            const string servicePointId1 = "SP_BTD001a1";
            const string serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            //const string channel = "email";
            var firstname = "Anjana";
            var lastname = "Khaire";
            var addr1 = "16 Laurel Ave";
            var addr2 = "Suite 100";
            var city = "Wellesley";
            var state = "MA";
            var zip = "02481";
            var email = "";
            var phone = "";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity> { mockBillingEntity1 };
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            var mockInsight2 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight2, false, 0);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, true, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, true, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, true, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            insightList.Add(mockInsight2);
            insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            insightList.Add(mockInsight8);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));
            _cassandraRepository.Setup(
                t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(mockInsight8));

            // mock customer entity
            var mockCustomer = TestHelpers.CreateMockResidentialCustomerEntityData(clientId, customerId, firstname, lastname,
                addr1, addr2, city, state, zip, email, phone);
            _cassandraRepository.Setup(c => c.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<CustomerEntity, bool>>>()))
                .Returns(Task.FromResult(mockCustomer));

            // mock insert subscription
            _cassandraRepository.Setup(c => c.InsertOrUpdateAsync(It.IsAny<SubscriptionEntity>(), It.IsAny<string>(), It.IsAny<char>()))
                .Returns(Task.FromResult(true)).Verifiable();
            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();

            var subscribeList = new List<SubscribedInsight>();
            var subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = false,
                Level = "account",
                Name = programInsight1,
                PremiseId = premiseId,
                //ServiceContractId = serviceContractId1,
                ServiceId = serviceContractId1
            };
            subscribeList.Add(subscribeInsight);

            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };

            var post = new SubscriptionPostRequest
            {
                Customer = new SubscribedCustomerInsight
                {
                    Id = customerId,
                    Insights = subscribeList
                }
            };

            var model = new SubscriptionModel(_container);

            var result = model.SubscribeInsights(clientUser, post);
            Assert.IsNotNull(result);
            Thread.Sleep(10000);
            _cassandraRepository.Verify(c => c.InsertOrUpdateAsync(It.IsAny<SubscriptionEntity>(), It.IsAny<string>(), It.IsAny<char>()), Times.AtLeast(2));
            _clientConfigFacade.Verify();

        }



        [TestMethod()]
        public void SubscriptionV1_SubscribeInsights_UnSubscribeAll_Test()
        {
            _container.RegisterInstance(_cassandraRepository.Object);

            var program = "Program1";
            var programInsight1 = "Program1.CostToDate";
            var programInsight2 = "Program1.AccountProjectedCost";
            var programInsight3 = "Program1.BillToDate";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";

            const int clientId = 87;
            const string customerId = "BTD001";
            const string accountId = "BTD001a1";
            const string premiseId = "BTD001a1p1";
            const string billCycleScheduleId = "Cycle01";
            const string meterId1 = "EM001";
            const string rateClass = "RG-1";
            const string servicePointId1 = "SP_BTD001a1";
            const string serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            //const string channel = "email";
            var firstname = "Anjana";
            var lastname = "Khaire";
            var addr1 = "16 Laurel Ave";
            var addr2 = "Suite 100";
            var city = "Wellesley";
            var state = "MA";
            var zip = "02481";
            var email = "";
            var phone = "";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity> { mockBillingEntity1 };
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            var mockInsight2 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight2, false, 0);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, true, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, true, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, true, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            insightList.Add(mockInsight2);
            insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            insightList.Add(mockInsight8);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));
            _cassandraRepository.Setup(
                t => t.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(mockInsight5));

            // mock customer entity
            var mockCustomer = TestHelpers.CreateMockResidentialCustomerEntityData(clientId, customerId, firstname, lastname,
                addr1, addr2, city, state, zip, email, phone);
            _cassandraRepository.Setup(c => c.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<CustomerEntity, bool>>>()))
                .Returns(Task.FromResult(mockCustomer));

            // mock insert subscription
            _cassandraRepository.Setup(c => c.InsertOrUpdateAsync(It.IsAny<SubscriptionEntity>(), It.IsAny<string>(), It.IsAny<char>()))
                .Returns(Task.FromResult(true)).Verifiable();
            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();


            var subscribeList = new List<SubscribedInsight>();
            var subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = false,
                Level = "account",
                Name = programInsight2,
                PremiseId = premiseId,
                //ServiceContractId = serviceContractId1,
                ServiceId = serviceContractId1
            };
            subscribeList.Add(subscribeInsight);
            var subscribeInsight1 = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = false,
                Level = "service",
                Name = programInsight1,
                PremiseId = premiseId,
                //ServiceContractId = serviceContractId1,
                ServiceId = serviceContractId1
            };
            subscribeList.Add(subscribeInsight1);
            var subscribeInsight2 = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = false,
                Level = "account",
                Name = programInsight3,
                PremiseId = premiseId,
                //ServiceContractId = serviceContractId1,
                ServiceId = serviceContractId1
            };
            subscribeList.Add(subscribeInsight2);

            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };

            var post = new SubscriptionPostRequest
            {
                Customer = new SubscribedCustomerInsight
                {
                    Id = customerId,
                    Insights = subscribeList
                }
            };

            var model = new SubscriptionModel(_container);

            var result = model.SubscribeInsights(clientUser, post);

            Assert.IsNotNull(result);
            Thread.Sleep(10000);
            _cassandraRepository.Verify(c => c.InsertOrUpdateAsync(It.IsAny<SubscriptionEntity>(), It.IsAny<string>(), It.IsAny<char>()), Times.Exactly(8));
            _clientConfigFacade.Verify();
        }
        #endregion

        [TestMethod()]
        public void SubscriptionV1_GetSubscribeList_NoExistingSubscription_ThresholdUpdate_Account_Test()
        {
            _container.RegisterInstance(_cassandraRepository.Object);

            var program = "Program1";
            var programInsight = "Program1.AccountLevelCostThreshold";
            //var insight1 = "ServiceLevelCostThreshold";
            //var insight2 = "ServiceLevelUsageThreshold";
            //var insight3 = "BillToDate";
            //var insight4 = "CostToDate";
            //var insight5 = "AccountProjectedCost";
            //var insight6 = "ServiceProjectedCost";
            //var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            //var insight9 = "DayThreshold";
            //var insight10 = "ServiceLevelTieredThresholdApproaching";
            //var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";
            var threshold = 200m;
            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);
            const string channel = "email";


            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock no previous subscription subscribed
            IList<SubscriptionEntity> subList = new List<SubscriptionEntity>();
            _cassandraRepository.Setup(t => t.GetAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>())).Returns(Task.FromResult(subList));
            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();
            var subscribeList = new List<SubscribedInsight>();
            var subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = true,
                Level = "account",
                Name = programInsight,
                Threshold = threshold,
                Channel = channel
            };
            subscribeList.Add(subscribeInsight);
            var model = new SubscriptionModel(_container);
            var valid = true;
            var result = model.GetSubscribeList(clientId, customerId, subscribeList, ref valid);

            Assert.IsNotNull(result);
            Assert.AreEqual(12, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(true, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);
            foreach (var insight in insights)
            {
                if (insight.InsightTypeName.ToLower() == insight8.ToLower())
                {
                    Assert.AreEqual(true, insight.IsSelected);
                    Assert.AreEqual((double)threshold, insight.SubscriberThresholdValue);
                }
                else
                {
                    Assert.AreEqual(false, insight.IsSelected);
                    Assert.AreNotEqual((double)threshold, insight.SubscriberThresholdValue);
                }
                Assert.AreEqual(accountId, insight.AccountId);
                Assert.AreEqual(customerId, insight.CustomerId);
            }
            _clientConfigFacade.Verify();
        }

        [TestMethod()]
        public void SubscriptionV1_GetSubscribeList_NoExistingSubscription_ChannelUpdate_Account_Test()
        {
            _container.RegisterInstance(_cassandraRepository.Object);

            var program = "Program1";
            var programInsight = "Program1.AccountLevelCostThreshold";
            //var insight1 = "ServiceLevelCostThreshold";
            //var insight2 = "ServiceLevelUsageThreshold";
            //var insight3 = "BillToDate";
            //var insight4 = "CostToDate";
            //var insight5 = "AccountProjectedCost";
            //var insight6 = "ServiceProjectedCost";
            //var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            //var insight9 = "DayThreshold";
            //var insight10 = "ServiceLevelTieredThresholdApproaching";
            //var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";
            var channel = "EmailandSMS";
            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);


            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock no previous subscription subscribed
            IList<SubscriptionEntity> subList = new List<SubscriptionEntity>();
            _cassandraRepository.Setup(t => t.GetAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>())).Returns(Task.FromResult(subList));
            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();
            var subscribeList = new List<SubscribedInsight>();
            var subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = true,
                Level = "account",
                Name = programInsight,
                Channel = channel
            };
            subscribeList.Add(subscribeInsight);
            var model = new SubscriptionModel(_container);
            var valid = true;
            var result = model.GetSubscribeList(clientId, customerId, subscribeList, ref valid);

            Assert.IsNotNull(result);
            Assert.AreEqual(12, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(true, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);
            foreach (var insight in insights)
            {
                if (insight.InsightTypeName.ToLower() == insight8.ToLower())
                {
                    Assert.AreEqual(true, insight.IsSelected);
                    Assert.AreEqual(channel.ToLower(), insight.Channel.ToLower());
                }
                else
                {
                    Assert.AreEqual(false, insight.IsSelected);
                    Assert.AreNotEqual(channel.ToLower(), insight.Channel.ToLower());
                }
                Assert.AreEqual(accountId, insight.AccountId);
                Assert.AreEqual(customerId, insight.CustomerId);
            }
            _clientConfigFacade.Verify();
        }

        [TestMethod()]
        public void SubscriptionV1_GetSubscribeList_NoExistingSubscription_ThresholdUpdate_Service_Test()
        {
            _container.RegisterInstance(_cassandraRepository.Object);

            var program = "Program1";
            var programInsight = "Program1.ServiceLevelUsageThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var threshold = 200m;
            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);
            const string channel = "email";

            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock no previous subscription subscribed
            IList<SubscriptionEntity> subList = new List<SubscriptionEntity>();
            _cassandraRepository.Setup(t => t.GetAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>())).Returns(Task.FromResult(subList));
            
            var subscribeList = new List<SubscribedInsight>();
            var subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = true,
                Level = "service",
                Name = programInsight,
                PremiseId = premiseId,
                //ServiceContractId = serviceContractId1,
                ServiceId = serviceContractId1,
                Threshold = threshold,
                Channel = channel
            };
            subscribeList.Add(subscribeInsight);
            var model = new SubscriptionModel(_container);
            var valid = true;
            var result = model.GetSubscribeList(clientId, customerId, subscribeList, ref valid);

            Assert.IsNotNull(result);
            Assert.AreEqual(12, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(true, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);
            foreach (var insight in insights)
            {
                if (insight.InsightTypeName.ToLower() == insight2.ToLower())
                {
                    Assert.AreEqual(true, insight.IsSelected);
                    Assert.AreEqual((double)threshold, insight.SubscriberThresholdValue);
                    Assert.AreEqual(serviceContractId1, insight.ServiceContractId);
                    //Assert.AreEqual(servicePointId1, insight.ServicePointId);
                    Assert.AreEqual(premiseId, insight.PremiseId);
                }
                else
                {
                    Assert.AreEqual(false, insight.IsSelected);
                    Assert.AreNotEqual((double)threshold, insight.SubscriberThresholdValue);
                }

                Assert.AreEqual(accountId, insight.AccountId);
                Assert.AreEqual(customerId, insight.CustomerId);
            }
            _clientConfigFacade.Verify();
        }

        [TestMethod()]
        public void SubscriptionV1_GetSubscribeList_NoExistingSubscription_ChannelUpdate_Service_Test()
        {
            _container.RegisterInstance(_cassandraRepository.Object);

            var program = "Program1";
            var programInsight = "Program1.ServiceLevelUsageThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var channel = "EmailandSMS";
            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services); 
            // mock no previous subscription subscribed
            IList<SubscriptionEntity> subList = new List<SubscriptionEntity>();
            _cassandraRepository.Setup(t => t.GetAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>())).Returns(Task.FromResult(subList));


            var subscribeList = new List<SubscribedInsight>();
            var subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = true,
                Level = "service",
                Name = programInsight,
                PremiseId = premiseId,
                //ServiceContractId = serviceContractId1,
                ServiceId = serviceContractId1,
                Channel = channel
            };
            subscribeList.Add(subscribeInsight);
            var model = new SubscriptionModel(_container);
            var valid = true;
            var result = model.GetSubscribeList(clientId, customerId, subscribeList, ref valid);

            Assert.IsNotNull(result);
            Assert.AreEqual(12, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(true, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);
            foreach (var insight in insights)
            {
                if (insight.InsightTypeName.ToLower() == insight2.ToLower())
                {
                    Assert.AreEqual(true, insight.IsSelected);
                    Assert.AreEqual(channel.ToLower(), insight.Channel.ToLower());
                    Assert.AreEqual(serviceContractId1, insight.ServiceContractId);
                    //Assert.AreEqual(servicePointId1, insight.ServicePointId);
                    Assert.AreEqual(premiseId, insight.PremiseId);
                }
                else
                {
                    Assert.AreEqual(false, insight.IsSelected);
                    Assert.AreNotEqual(channel.ToLower(), insight.Channel.ToLower());
                }

                Assert.AreEqual(accountId, insight.AccountId);
                Assert.AreEqual(customerId, insight.CustomerId);
            }
            _clientConfigFacade.Verify();
        }

        [TestMethod()]
        public void SubscriptionV1_GetSubscribeList_ExistingSubscription_ThresholdUpdate_Account_Test()
        {
            _container.RegisterInstance(_cassandraRepository.Object);

            var program = "Program1";
            var programInsight = "Program1.AccountLevelCostThreshold";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";
            var threshold = 200m;
            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);
            const string channel = "email";

            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            var mockInsight2 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight2, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            insightList.Add(mockInsight2);
            insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));


            var subscribeList = new List<SubscribedInsight>();
            var subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = true,
                Level = "account",
                Name = programInsight,
                Threshold = threshold,
                Channel = channel
            };
            subscribeList.Add(subscribeInsight);
            var model = new SubscriptionModel(_container);
            var valid = true;
            var result = model.GetSubscribeList(clientId, customerId, subscribeList, ref valid);


            Assert.IsNotNull(result);
            Assert.AreEqual(12, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(true, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);
            foreach (var insight in insights)
            {
                if (insight.InsightTypeName.ToLower() == insight8.ToLower())
                {
                    Assert.AreEqual(true, insight.IsSelected);
                    Assert.AreEqual(string.Empty, insight.ServiceContractId);
                    Assert.AreEqual(string.Empty, insight.ServicePointId);
                    Assert.AreEqual(string.Empty, insight.PremiseId);
                    Assert.AreEqual((double)threshold, insight.SubscriberThresholdValue);
                }
                else
                {
                    Assert.AreEqual(false, insight.IsSelected);
                    Assert.AreNotEqual((double)threshold, insight.SubscriberThresholdValue);
                }

                Assert.AreEqual(accountId, insight.AccountId);
                Assert.AreEqual(customerId, insight.CustomerId);
            }
            _clientConfigFacade.Verify();
        }

        [TestMethod()]
        public void SubscriptionV1_GetSubscribeList_ExistingSubscription_ChannelUpdate_Account_Test()
        {
            _container.RegisterInstance(_cassandraRepository.Object);

            var program = "Program1";
            var programInsight = "Program1.AccountLevelCostThreshold";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";
            var channel = "EmailandSMS";
            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            var mockInsight2 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight2, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            insightList.Add(mockInsight2);
            insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));


            var subscribeList = new List<SubscribedInsight>();
            var subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = true,
                Level = "account",
                Name = programInsight,
                Channel = channel
            };
            subscribeList.Add(subscribeInsight);
            var model = new SubscriptionModel(_container);
            var valid = true;
            var result = model.GetSubscribeList(clientId, customerId, subscribeList, ref valid);


            Assert.IsNotNull(result);
            Assert.AreEqual(12, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(true, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);
            foreach (var insight in insights)
            {
                if (insight.InsightTypeName.ToLower() == insight8.ToLower())
                {
                    Assert.AreEqual(true, insight.IsSelected);
                    Assert.AreEqual(string.Empty, insight.ServiceContractId);
                    Assert.AreEqual(string.Empty, insight.ServicePointId);
                    Assert.AreEqual(string.Empty, insight.PremiseId);
                    Assert.AreEqual(channel.ToLower(), insight.Channel.ToLower());
                }
                else
                {
                    Assert.AreEqual(false, insight.IsSelected);
                    Assert.AreNotEqual(channel, insight.Channel);
                }

                Assert.AreEqual(accountId, insight.AccountId);
                Assert.AreEqual(customerId, insight.CustomerId);
            }
            _clientConfigFacade.Verify();
        }

        [TestMethod()]
        public void SubscriptionV1_GetSubscribeList_ExistingSubscription_ThresholdUpdate_Service_Test()
        {
            _container.RegisterInstance(_cassandraRepository.Object);

            var program = "Program1";
            var programInsight = "Program1.ServiceLevelUsageThreshold";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";
            var threshold = 200m;
            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);
            const string channel = "email";

            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            insightList.Add(mockInsight8);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));


            var subscribeList = new List<SubscribedInsight>();
            var subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = true,
                Level = "service",
                Name = programInsight,
                PremiseId = premiseId,
                //ServiceContractId = serviceContractId1,
                ServiceId = serviceContractId1,
                Threshold = threshold,
                Channel = channel
            };
            subscribeList.Add(subscribeInsight);
            var model = new SubscriptionModel(_container);
            var valid = true;
            var result = model.GetSubscribeList(clientId, customerId, subscribeList, ref valid);

            Assert.IsNotNull(result);
            Assert.AreEqual(12, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(true, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);
            foreach (var insight in insights)
            {
                if (insight.InsightTypeName.ToLower() == insight2.ToLower())
                {
                    Assert.AreEqual(true, insight.IsSelected);
                    Assert.AreEqual((double)threshold, insight.SubscriberThresholdValue);
                    Assert.AreEqual(serviceContractId1, insight.ServiceContractId);
                    //Assert.AreEqual(servicePointId1, insight.ServicePointId);
                    Assert.AreEqual(premiseId, insight.PremiseId);
                }
                else
                {
                    Assert.AreEqual(false, insight.IsSelected);
                    Assert.AreNotEqual((double)threshold, insight.SubscriberThresholdValue);
                }

                Assert.AreEqual(accountId, insight.AccountId);
                Assert.AreEqual(customerId, insight.CustomerId);
            }
            _clientConfigFacade.Verify();
        }
        [TestMethod()]
        public void SubscriptionV1_GetSubscribeList_ExistingSubscription_ChannelUpdate_Service_Test()
        {
            _container.RegisterInstance(_cassandraRepository.Object);

            var program = "Program1";
            var programInsight = "Program1.ServiceLevelUsageThreshold";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";
            var channel = "EmailandSMS";
            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            insightList.Add(mockInsight8);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));

            var subscribeList = new List<SubscribedInsight>();
            var subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = true,
                Level = "service",
                Name = programInsight,
                PremiseId = premiseId,
                //ServiceContractId = serviceContractId1,
                ServiceId = serviceContractId1,
                Channel = channel
            };
            subscribeList.Add(subscribeInsight);
            var model = new SubscriptionModel(_container);
            var valid = true;
            var result = model.GetSubscribeList(clientId, customerId, subscribeList, ref valid);

            Assert.IsNotNull(result);
            Assert.AreEqual(12, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(true, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);
            foreach (var insight in insights)
            {
                if (insight.InsightTypeName.ToLower() == insight2.ToLower())
                {
                    Assert.AreEqual(true, insight.IsSelected);
                    Assert.AreEqual(channel.ToLower(), insight.Channel.ToLower());
                    Assert.AreEqual(serviceContractId1, insight.ServiceContractId);
                    //Assert.AreEqual(servicePointId1, insight.ServicePointId);
                    Assert.AreEqual(premiseId, insight.PremiseId);
                }
                else
                {
                    Assert.AreEqual(false, insight.IsSelected);
                    Assert.AreNotEqual(channel.ToLower(), insight.Channel.ToLower());
                }

                Assert.AreEqual(accountId, insight.AccountId);
                Assert.AreEqual(customerId, insight.CustomerId);
            }
            _clientConfigFacade.Verify();
        }

        [TestMethod()]
        public void SubscriptionV1_GetSubscribeList_ExistingSubscription_FalseInTable_ThresholdUpdate_Account_Test()
        {
            _container.RegisterInstance(_cassandraRepository.Object);

            var program = "Program1";
            var programInsight = "Program1.AccountLevelCostThreshold";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";
            var threshold = 200m;
            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);
            const string channel = "email";

            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            var mockInsight2 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight2, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            insightList.Add(mockInsight2);
            insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            insightList.Add(mockInsight8);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));

            var subscribeList = new List<SubscribedInsight>();
            var subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = true,
                Level = "account",
                Name = programInsight,
                Threshold = threshold,
                Channel = channel
            };
            subscribeList.Add(subscribeInsight);
            var model = new SubscriptionModel(_container);
            var valid = true;
            var result = model.GetSubscribeList(clientId, customerId, subscribeList, ref valid);


            Assert.IsNotNull(result);
            Assert.AreEqual(12, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(true, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);
            foreach (var insight in insights)
            {
                if (insight.InsightTypeName.ToLower() == insight8.ToLower())
                {
                    Assert.AreEqual(true, insight.IsSelected);
                    Assert.AreEqual(string.Empty, insight.ServiceContractId);
                    Assert.AreEqual(string.Empty, insight.ServicePointId);
                    Assert.AreEqual(string.Empty, insight.PremiseId);
                    Assert.AreEqual((double)threshold, insight.SubscriberThresholdValue);
                }
                else
                {
                    Assert.AreEqual(false, insight.IsSelected);
                    Assert.AreNotEqual((double)threshold, insight.SubscriberThresholdValue);
                }

                Assert.AreEqual(accountId, insight.AccountId);
                Assert.AreEqual(customerId, insight.CustomerId);
            }
            _clientConfigFacade.Verify();
        }

        [TestMethod()]
        public void SubscriptionV1_GetSubscribeList_ExistingSubscription_FalseInTable_ChannelUpdate_Account_Test()
        {
            _container.RegisterInstance(_cassandraRepository.Object);

            var program = "Program1";
            var programInsight = "Program1.AccountLevelCostThreshold";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";
            var channel = "EmailandSMS";
            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            var mockInsight2 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight2, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            insightList.Add(mockInsight2);
            insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            insightList.Add(mockInsight8);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));


            var subscribeList = new List<SubscribedInsight>();
            var subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = true,
                Level = "account",
                Name = programInsight,
                Channel = channel
            };
            subscribeList.Add(subscribeInsight);
            var model = new SubscriptionModel(_container);
            var valid = true;
            var result = model.GetSubscribeList(clientId, customerId, subscribeList, ref valid);


            Assert.IsNotNull(result);
            Assert.AreEqual(12, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(true, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);
            foreach (var insight in insights)
            {
                if (insight.InsightTypeName.ToLower() == insight8.ToLower())
                {
                    Assert.AreEqual(true, insight.IsSelected);
                    Assert.AreEqual(string.Empty, insight.ServiceContractId);
                    Assert.AreEqual(string.Empty, insight.ServicePointId);
                    Assert.AreEqual(string.Empty, insight.PremiseId);
                    Assert.AreEqual(channel.ToLower(), insight.Channel.ToLower());
                }
                else
                {
                    Assert.AreEqual(false, insight.IsSelected);
                    Assert.AreNotEqual(channel.ToLower(), insight.Channel.ToLower());
                }

                Assert.AreEqual(accountId, insight.AccountId);
                Assert.AreEqual(customerId, insight.CustomerId);
            }
            _clientConfigFacade.Verify();
        }

        [TestMethod()]
        public void SubscriptionV1_GetSubscribeList_ExistingSubscription_FalseInTable_ThresholdUpdate_Service_Test()
        {
            _container.RegisterInstance(_cassandraRepository.Object);

            var program = "Program1";
            var programInsight = "Program1.ServiceLevelUsageThreshold";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";
            var threshold = 200m;
            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);
            const string channel = "email";

            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            var mockInsight2 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight2, false, 0);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            insightList.Add(mockInsight2);
            insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            insightList.Add(mockInsight8);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));


            var subscribeList = new List<SubscribedInsight>();
            var subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = true,
                Level = "service",
                Name = programInsight,
                PremiseId = premiseId,
                //ServiceContractId = serviceContractId1,
                ServiceId = serviceContractId1,
                Threshold = threshold,
                Channel = channel
            };
            subscribeList.Add(subscribeInsight);
            var model = new SubscriptionModel(_container);
            var valid = true;
            var result = model.GetSubscribeList(clientId, customerId, subscribeList, ref valid);

            Assert.IsNotNull(result);
            Assert.AreEqual(12, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(true, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);
            foreach (var insight in insights)
            {
                if (insight.InsightTypeName.ToLower() == insight2.ToLower())
                {
                    Assert.AreEqual(true, insight.IsSelected);
                    Assert.AreEqual((double)threshold, insight.SubscriberThresholdValue);
                    Assert.AreEqual(serviceContractId1, insight.ServiceContractId);
                    Assert.AreEqual(servicePointId1, insight.ServicePointId);
                    Assert.AreEqual(premiseId, insight.PremiseId);
                }
                else
                {
                    Assert.AreEqual(false, insight.IsSelected);
                    Assert.AreNotEqual((double)threshold, insight.SubscriberThresholdValue);
                }

                Assert.AreEqual(accountId, insight.AccountId);
                Assert.AreEqual(customerId, insight.CustomerId);
            }
            _clientConfigFacade.Verify();
        }
        [TestMethod()]
        public void SubscriptionV1_GetSubscribeList_ExistingSubscription_FalseInTable_ChannelUpdate_Service_Test()
        {
            _container.RegisterInstance(_cassandraRepository.Object);

            var program = "Program1";
            var programInsight = "Program1.ServiceLevelUsageThreshold";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";
            var channel = "EmailandSMS";
            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            var mockInsight2 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight2, false, 0);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            insightList.Add(mockInsight2);
            insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            insightList.Add(mockInsight8);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));


            var subscribeList = new List<SubscribedInsight>();
            var subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = true,
                Level = "service",
                Name = programInsight,
                PremiseId = premiseId,
                //ServiceContractId = serviceContractId1,
                ServiceId = serviceContractId1,
                Channel = channel
            };
            subscribeList.Add(subscribeInsight);
            var model = new SubscriptionModel(_container);
            var valid = true;
            var result = model.GetSubscribeList(clientId, customerId, subscribeList, ref valid);

            Assert.IsNotNull(result);
            Assert.AreEqual(12, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(true, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);
            foreach (var insight in insights)
            {
                if (insight.InsightTypeName.ToLower() == insight2.ToLower())
                {
                    Assert.AreEqual(true, insight.IsSelected);
                    Assert.AreEqual(channel.ToLower(), insight.Channel.ToLower());
                    Assert.AreEqual(serviceContractId1, insight.ServiceContractId);
                    //Assert.AreEqual(servicePointId1, insight.ServicePointId);
                    Assert.AreEqual(premiseId, insight.PremiseId);
                }
                else
                {
                    Assert.AreEqual(false, insight.IsSelected);
                    Assert.AreNotEqual(channel.ToLower(), insight.Channel.ToLower());
                }

                Assert.AreEqual(accountId, insight.AccountId);
                Assert.AreEqual(customerId, insight.CustomerId);
            }
            _clientConfigFacade.Verify();
        }

        [TestMethod()]
        public void SubscriptionV1_GetSubscribeList_ExistingSubscription_TrueInTable_ThresholdUpdate_Account_Test()
        {
            _container.RegisterInstance(_cassandraRepository.Object);

            var program = "Program1";
            var programInsight = "Program1.AccountLevelCostThreshold";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";
            var threshold = 200m;
            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);
            const string channel = "email";

            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            var mockInsight2 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight2, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, true, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            insightList.Add(mockInsight2);
            insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            insightList.Add(mockInsight8);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));


            var subscribeList = new List<SubscribedInsight>();
            var subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = true,
                Level = "account",
                Name = programInsight,
                Threshold = threshold,
                Channel = channel
            };
            subscribeList.Add(subscribeInsight);
            var model = new SubscriptionModel(_container);
            var valid = true;
            var result = model.GetSubscribeList(clientId, customerId, subscribeList, ref valid);


            Assert.IsNotNull(result);
            Assert.AreEqual(12, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(true, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);
            foreach (var insight in insights)
            {
                if (insight.InsightTypeName.ToLower() == insight8.ToLower())
                {
                    Assert.AreEqual(true, insight.IsSelected);
                    Assert.AreEqual(string.Empty, insight.ServiceContractId);
                    Assert.AreEqual(string.Empty, insight.ServicePointId);
                    Assert.AreEqual(string.Empty, insight.PremiseId);
                    Assert.AreEqual((double)threshold, insight.SubscriberThresholdValue);
                }
                else
                {
                    Assert.AreEqual(false, insight.IsSelected);
                    Assert.AreNotEqual((double)threshold, insight.SubscriberThresholdValue);
                }

                Assert.AreEqual(accountId, insight.AccountId);
                Assert.AreEqual(customerId, insight.CustomerId);
            }
            _clientConfigFacade.Verify();
        }

        [TestMethod()]
        public void SubscriptionV1_GetSubscribeList_ExistingSubscription_TrueInTable_ChannelUpdate_Account_Test()
        {
            _container.RegisterInstance(_cassandraRepository.Object);

            var program = "Program1";
            var programInsight = "Program1.AccountLevelCostThreshold";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";
            var channel = "EmailandSMS";
            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            var mockInsight2 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight2, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, true, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            insightList.Add(mockInsight2);
            insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            insightList.Add(mockInsight8);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));


            var subscribeList = new List<SubscribedInsight>();
            var subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = true,
                Level = "account",
                Name = programInsight,
                Channel = channel
            };
            subscribeList.Add(subscribeInsight);
            var model = new SubscriptionModel(_container);
            var valid = true;
            var result = model.GetSubscribeList(clientId, customerId, subscribeList, ref valid);


            Assert.IsNotNull(result);
            Assert.AreEqual(12, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(true, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);
            foreach (var insight in insights)
            {
                if (insight.InsightTypeName.ToLower() == insight8.ToLower())
                {
                    Assert.AreEqual(true, insight.IsSelected);
                    Assert.AreEqual(string.Empty, insight.ServiceContractId);
                    Assert.AreEqual(string.Empty, insight.ServicePointId);
                    Assert.AreEqual(string.Empty, insight.PremiseId);
                    Assert.AreEqual(channel.ToLower(), insight.Channel.ToLower());
                }
                else
                {
                    Assert.AreEqual(false, insight.IsSelected);
                    Assert.AreNotEqual(channel.ToLower(), insight.Channel.ToLower());
                }

                Assert.AreEqual(accountId, insight.AccountId);
                Assert.AreEqual(customerId, insight.CustomerId);
            }
            _clientConfigFacade.Verify();
        }

        [TestMethod()]
        public void SubscriptionV1_GetSubscribeList_ExistingSubscription_TrueInTable_ThresholdUpdate_Service_Test()
        {
            _container.RegisterInstance(_cassandraRepository.Object);

            var program = "Program1";
            var programInsight = "Program1.ServiceLevelUsageThreshold";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";
            var threshold = 200m;
            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);
            const string channel = "email";

            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            var mockInsight2 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight2, true, 0);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            insightList.Add(mockInsight2);
            insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            insightList.Add(mockInsight8);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));


            var subscribeList = new List<SubscribedInsight>();
            var subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = true,
                Level = "service",
                Name = programInsight,
                PremiseId = premiseId,
                //ServiceContractId = serviceContractId1,
                ServiceId = serviceContractId1,
                Threshold = threshold,
                Channel = channel
            };
            subscribeList.Add(subscribeInsight);
            var model = new SubscriptionModel(_container);
            var valid = true;
            var result = model.GetSubscribeList(clientId, customerId, subscribeList, ref valid);

            Assert.IsNotNull(result);
            Assert.AreEqual(12, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(true, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);
            foreach (var insight in insights)
            {
                if (insight.InsightTypeName.ToLower() == insight2.ToLower())
                {
                    Assert.AreEqual(true, insight.IsSelected);
                    Assert.AreEqual((double)threshold, insight.SubscriberThresholdValue);
                    Assert.AreEqual(serviceContractId1, insight.ServiceContractId);
                    //Assert.AreEqual(servicePointId1, insight.ServicePointId);
                    Assert.AreEqual(premiseId, insight.PremiseId);
                }
                else
                {
                    Assert.AreEqual(false, insight.IsSelected);
                    Assert.AreNotEqual((double)threshold, insight.SubscriberThresholdValue);
                }

                Assert.AreEqual(accountId, insight.AccountId);
                Assert.AreEqual(customerId, insight.CustomerId);
            }
            _clientConfigFacade.Verify();
        }
        [TestMethod()]
        public void SubscriptionV1_GetSubscribeList_ExistingSubscription_TrueInTable_ChannelUpdate_Service_Test()
        {
            _container.RegisterInstance(_cassandraRepository.Object);

            var program = "Program1";
            var programInsight = "Program1.ServiceLevelUsageThreshold";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";
            var channel = "EmailandSMS";
            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            var mockInsight2 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight2, true, 0);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            insightList.Add(mockInsight2);
            insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            insightList.Add(mockInsight8);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));


            var subscribeList = new List<SubscribedInsight>();
            var subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = true,
                Level = "service",
                Name = programInsight,
                PremiseId = premiseId,
                //ServiceContractId = serviceContractId1,
                ServiceId = serviceContractId1,
                Channel = channel
            };
            subscribeList.Add(subscribeInsight);
            var model = new SubscriptionModel(_container);
            var valid = true;
            var result = model.GetSubscribeList(clientId, customerId, subscribeList, ref valid);

            Assert.IsNotNull(result);
            Assert.AreEqual(12, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(true, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);
            foreach (var insight in insights)
            {
                if (insight.InsightTypeName.ToLower() == insight2.ToLower())
                {
                    Assert.AreEqual(true, insight.IsSelected);
                    Assert.AreEqual(channel.ToLower(), insight.Channel.ToLower());
                    Assert.AreEqual(serviceContractId1, insight.ServiceContractId);
                    //Assert.AreEqual(servicePointId1, insight.ServicePointId);
                    Assert.AreEqual(premiseId, insight.PremiseId);
                }
                else
                {
                    Assert.AreEqual(false, insight.IsSelected);
                    Assert.AreNotEqual(channel.ToLower(), insight.Channel.ToLower());
                }

                Assert.AreEqual(accountId, insight.AccountId);
                Assert.AreEqual(customerId, insight.CustomerId);
            }
            _clientConfigFacade.Verify();
        }

        [TestMethod()]
        public void SubscriptionV1_GetSubscriptions_GetEmailPhone_Test()
        {
            _container.RegisterInstance(_cassandraRepository.Object);

            var program = "Program1";

            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId = "EM001";
            var rateClass = "RG-1";
            var servicePointId = "SP_BTD001a1";
            var serviceContractId = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            var firstname = "Anjana";
            var lastname = "Khaire";
            var addr1 = "16 Laurel Ave";
            var addr2 = "Suite 100";
            var city = "Wellesley";
            var state = "MA";
            var zip = "02481";
            var email = "test@aclara.com";
            var phone = "781-694-3200";

            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();
            // mock customer entity
            var mockCustomer = TestHelpers.CreateMockResidentialCustomerEntityData(clientId, customerId, firstname, lastname,
                addr1, addr2, city, state, zip, email, phone);
            _cassandraRepository.Setup(c => c.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<CustomerEntity, bool>>>()))
                .Returns(Task.FromResult(mockCustomer));
            // mock billing entities data
            var mockBillingEntity = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId,
                servicePointId, meterId, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);


            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };

            var request = new SubscriptionRequest
            {
                AccountId = accountId,
                CustomerId = customerId
            };

            var model = new SubscriptionModel(_container);

            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));
            var result = model.GetSubscriptions(clientUser, request);

            Assert.IsNotNull(result);
            Assert.AreEqual(email, result.Customer.Email);
            Assert.AreEqual(phone.Replace("-", ""), result.Customer.Phone.Replace("-", ""));
            _clientConfigFacade.Verify();
        }

        [TestMethod()]
        public void SubscriptionV1_UpdateCustomerInfo_Test()
        {
            _container.RegisterInstance(_cassandraRepository.Object);

            var newEmail = "lchu@aclara.com";
            var newPhone = "7816943200";

            var clientId = 87;
            var customerId = "BTD001";
            //var accountId = "BTD001a1";
            //var premiseId = "BTD001a1p1";
            //var billCycleScheduleId = "Cycle01";
            //var meterId = "EM001";
            //var rateClass = "RG-1";
            //var servicePointId = "SP_BTD001a1";
            //var serviceContractId = "BTD001a1_BTD001a1p1_1_EM001";
            //var today = DateTime.Today.Date;
            //var currentStart = today.AddDays(-6);
            ////var currentEnd = currentStart.AddDays(30);
            //var lastBillEndDate = currentStart.AddDays(-1);
            //var lastBillStartDate = lastBillEndDate.AddDays(-29);

            var firstname = "Anjana";
            var lastname = "Khaire";
            var addr1 = "16 Laurel Ave";
            var addr2 = "Suite 100";
            var city = "Wellesley";
            var state = "MA";
            var zip = "02481";
            var email = "test@aclara.com";
            var phone = "781-694-3200";

            // mock customer entity
            var mockCustomer = TestHelpers.CreateMockResidentialCustomerEntityData(clientId, customerId, firstname, lastname,
                addr1, addr2, city, state, zip, email, phone);
            var mockUpdatedCustomer = TestHelpers.CreateMockResidentialCustomerEntityData(clientId, customerId, firstname, lastname,
                addr1, addr2, city, state, zip, newEmail, newPhone);
            _cassandraRepository.SetupSequence(c => c.GetSingleAsync(It.IsAny<string>(), It.IsAny<Expression<Func<CustomerEntity, bool>>>()))
                .Returns(Task.FromResult(mockCustomer))
                .Returns(Task.FromResult(mockUpdatedCustomer));
            _cassandraRepository.Setup(c => c.InsertOrUpdate(It.IsAny<CustomerEntity>(), It.IsAny<string>()))
                .Returns(true);

            var post = new SubscriptionPostRequest
            {
                Customer = new SubscribedCustomerInsight
                {
                    Id = customerId,
                    Email = newEmail,
                    Phone = newPhone
                }
            };

            var model = new SubscriptionModel(_container);
            try
            {
                var reOptin = false;
                model.UpdateCustomerInfo(clientId, post, false, ref reOptin);

                Thread.Sleep(1000);
                _cassandraRepository.Verify(c => c.InsertOrUpdate(It.IsAny<CustomerEntity>(), It.IsAny<string>()), Times.AtLeastOnce);

                var result = model.GetCustomerInfo(clientId, customerId);
                Assert.AreEqual(newEmail, result.EmailAddress);
                Assert.AreEqual(newPhone, result.Phone1);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }


        }

        [TestMethod()]
        public void SubscriptionV1_UpdateCustomerInfoWithSms_Test()
        {
            var  customerManager = new Mock<ICustomer>();
            _container.RegisterInstance(customerManager.Object);
            var newEmail = "lchu@aclara.com";
            var newPhone = "7816943200";

            var clientId = 87;
            var customerId = "BTD001";
            //var accountId = "BTD001a1";
            //var premiseId = "BTD001a1p1";
            //var billCycleScheduleId = "Cycle01";
            //var meterId = "EM001";
            //var rateClass = "RG-1";
            //var servicePointId = "SP_BTD001a1";
            //var serviceContractId = "BTD001a1_BTD001a1p1_1_EM001";
            //var today = DateTime.Today.Date;
            //var currentStart = today.AddDays(-6);
            ////var currentEnd = currentStart.AddDays(30);
            //var lastBillEndDate = currentStart.AddDays(-1);
            //var lastBillStartDate = lastBillEndDate.AddDays(-29);

            var firstname = "Anjana";
            var lastname = "Khaire";
            var addr1 = "16 Laurel Ave";
            var addr2 = "Suite 100";
            var city = "Wellesley";
            var state = "MA";
            var zip = "02481";
            var email = "test@aclara.com";
            var phone = "781-694-3200";
            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();
            //Mapper.Map<CustomerEntity, CustomerModel>(entity)
            var outValue = false;
            // mock customer entity
            var mockCustomer = TestHelpers.CreateMockResidentialCustomerEntityData(clientId, customerId, firstname, lastname,
                addr1, addr2, city, state, zip, email, phone);
            var mockCustomerModel = Mapper.Map<CustomerEntity, CustomerModel>(mockCustomer);
            var mockUpdatedCustomer = TestHelpers.CreateMockResidentialCustomerEntityData(clientId, customerId, firstname, lastname,
                addr1, addr2, city, state, zip, newEmail, newPhone);
            var mockUpdatedCustomerModel = Mapper.Map<CustomerEntity, CustomerModel>(mockUpdatedCustomer);
            customerManager.SetupSequence(c => c.GetCustomerAsync(It.IsAny<int>(),It.IsAny<string>()))
                .Returns(Task.FromResult(mockCustomerModel))
                .Returns(Task.FromResult(mockUpdatedCustomerModel));
            customerManager.Setup(
                c =>
                    c.CreateSubscriptionAndUpdateCustomer(It.IsAny<CustomerModel>(), It.IsAny<string>(),
                        It.IsAny<string>(), It.IsAny<string>())).Returns(true);
            customerManager.Setup(
                c =>
                    c.SmsSubscirptionCheckAndUpdate(It.IsAny<CustomerModel>(), It.IsAny<string>(),
                        It.IsAny<string>(), It.IsAny<string>(), ref outValue)).Returns(true);
            customerManager.Setup(c => c.InsertOrMergeCustomerAsync(It.IsAny<CustomerModel>()))
                .Returns(Task.FromResult(true));
            var post = new SubscriptionPostRequest
            {
                Customer = new SubscribedCustomerInsight
                {
                    Id = customerId,
                    Email = newEmail,
                    Phone = newPhone
                }
            };

            var model = new SubscriptionModel(_container);

            try
            {
                var reOptin = false;
                model.UpdateCustomerInfo(clientId, post, true, ref reOptin);

                Thread.Sleep(1000);
                customerManager.Verify(c => c.InsertOrMergeCustomerAsync(It.IsAny<CustomerModel>()), Times.AtMostOnce);
                customerManager.Verify(c => c.CreateSubscriptionAndUpdateCustomer(It.IsAny<CustomerModel>(), It.IsAny<string>(),
                        It.IsAny<string>(), It.IsAny<string>()), Times.AtMostOnce);
                customerManager.Verify(c => c.SmsSubscirptionCheckAndUpdate(It.IsAny<CustomerModel>(), It.IsAny<string>(),
                        It.IsAny<string>(), It.IsAny<string>(), ref outValue), Times.AtMostOnce);
                var result = model.GetCustomerInfo(clientId, customerId);

                Assert.AreEqual(newEmail, result.EmailAddress);
                Assert.AreEqual(newPhone, result.Phone1);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
            _clientConfigFacade.Verify();
        }

        [TestMethod()]
        public void SubscriptionV1_GetSubscribeList_UOM_Test()
        {
            _container.RegisterInstance(_cassandraRepository.Object);

            var programInsight = "Program1.ServiceLevelUsageThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight9 = "DayThreshold";

            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock no previous subscription subscribed
            IList<SubscriptionEntity> subList = new List<SubscriptionEntity>();
            _cassandraRepository.Setup(t => t.GetAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>())).Returns(Task.FromResult(subList));

            var subscribeList = new List<SubscribedInsight>();
            var subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = true,
                Level = "service",
                Name = programInsight,
                PremiseId = premiseId,
                //ServiceContractId = serviceContractId1,
                ServiceId = serviceContractId1,
                Channel = "Email"
            };
            subscribeList.Add(subscribeInsight);
            var model = new SubscriptionModel(_container);
            var valid = true;
            var result = model.GetSubscribeList(clientId, customerId, subscribeList, ref valid);

            Assert.IsNotNull(result);
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            foreach (var insight in insights)
            {
                if (insight.InsightTypeName.ToLower() == insight2.ToLower() || insight.InsightTypeName.ToLower() == insight9)
                    Assert.AreEqual(0, insight.UomId);
            }
            _clientConfigFacade.Verify();
        }
        [TestMethod()]
        public void SubscriptionV1_GetSubscribeList_ExistingSubscription_UOM_Test()
        {
            _container.RegisterInstance(_cassandraRepository.Object);

            var program = "Program1";
            var programInsight = "Program1.ServiceLevelUsageThreshold";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";
            var channel = "EmailandSMS";
            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            var mockInsight2 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight2, false, 0);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            insightList.Add(mockInsight2);
            insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            insightList.Add(mockInsight8);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));

            var subscribeList = new List<SubscribedInsight>();
            var subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = true,
                Level = "service",
                Name = programInsight,
                PremiseId = premiseId,
                //ServiceContractId = serviceContractId1,
                ServiceId = serviceContractId1,
                Channel = channel
            };
            subscribeList.Add(subscribeInsight);
            var model = new SubscriptionModel(_container);
            var valid = true;
            var result = model.GetSubscribeList(clientId, customerId, subscribeList, ref valid);

            Assert.IsNotNull(result);
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            foreach (var insight in insights)
            {
                if (insight.InsightTypeName.ToLower() == insight2.ToLower() || insight.InsightTypeName.ToLower() == insight9)
                    Assert.AreEqual(0, insight.UomId);
            }
            _clientConfigFacade.Verify();
        }


        [TestMethod()]
        public void SubscriptionV1_GetSubscriptions_NonblockNumber_Test()
        {
            var customerManager = new Mock<ICustomer>();
            _container.RegisterInstance(_cassandraRepository.Object);
            _container.RegisterInstance(customerManager.Object);

            var program = "Program1";

            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId = "EM001";
            var rateClass = "RG-1";
            var servicePointId = "SP_BTD001a1";
            var serviceContractId = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            var firstname = "Anjana";
            var lastname = "Khaire";
            var addr1 = "16 Laurel Ave";
            var addr2 = "Suite 100";
            var city = "Wellesley";
            var state = "MA";
            var zip = "02481";
            var email = "test@aclara.com";
            var phone = "1234567890";
            var requestId = "TestRequestId";
            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();
            // mock customer entity
            var mockCustomer = TestHelpers.CreateMockResidentialCustomerEntityData(clientId, customerId, firstname, lastname,
                addr1, addr2, city, state, zip, email, phone);
            var mockCustomerModel = Mapper.Map<CustomerEntity, CustomerModel>(mockCustomer);
            mockCustomerModel.TrumpiaRequestDetailId = requestId;
            customerManager.Setup(c => c.GetCustomerAsync(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(mockCustomerModel));
            // mock billing entities data
            var mockBillingEntity = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId,
                servicePointId, meterId, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock trumpia detail
            var mockTrumpiaRequest = new TrumpiaRequestDetailEntity {RequestId = requestId};
            _cassandraRepository.Setup(
                c => c.GetSingle(It.IsAny<string>(), It.IsAny<Expression<Func<TrumpiaRequestDetailEntity, bool>>>()))
                .Returns(mockTrumpiaRequest);

            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };

            var request = new SubscriptionRequest
            {
                AccountId = accountId,
                CustomerId = customerId
            };

            var model = new SubscriptionModel(_container);

            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));
            var result = model.GetSubscriptions(clientUser, request);

            Assert.IsNotNull(result);
            Assert.IsNull(result.Message);
            Assert.IsNull(result.SmsWarningCode);
            _clientConfigFacade.Verify();
        }

        [TestMethod()]
        public void SubscriptionV1_GetSubscriptions_BlockNumber_Test()
        {
            var customerManager = new Mock<ICustomer>();
            _container.RegisterInstance(_cassandraRepository.Object);
            _container.RegisterInstance(customerManager.Object);

            var program = "Program1";

            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId = "EM001";
            var rateClass = "RG-1";
            var servicePointId = "SP_BTD001a1";
            var serviceContractId = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            var firstname = "Anjana";
            var lastname = "Khaire";
            var addr1 = "16 Laurel Ave";
            var addr2 = "Suite 100";
            var city = "Wellesley";
            var state = "MA";
            var zip = "02481";
            var email = "test@aclara.com";
            var phone = "1234567890";
            var requestId = "TestRequestId";
            var statusCode = "MPSE0501";
            var errorMessage = "Blocked tools : 6175842838";
            var expectedStatusCode = "BlockedNumberWarnings";
            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();
            // mock customer entity
            var mockCustomer = TestHelpers.CreateMockResidentialCustomerEntityData(clientId, customerId, firstname, lastname,
                addr1, addr2, city, state, zip, email, phone);
            var mockCustomerModel = Mapper.Map<CustomerEntity, CustomerModel>(mockCustomer);
            mockCustomerModel.TrumpiaRequestDetailId = requestId;
            customerManager.Setup(c => c.GetCustomerAsync(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(mockCustomerModel));
            customerManager.Setup(
               c =>
                   c.CreateSubscriptionAndUpdateCustomer(It.IsAny<CustomerModel>(), It.IsAny<string>(),
                       It.IsAny<string>(), It.IsAny<string>())).Returns(false);
            // mock billing entities data
            var mockBillingEntity = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId,
                servicePointId, meterId, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock trumpia detail
            var mockTrumpiaRequest = new TrumpiaRequestDetailEntity
            {
                RequestId = requestId,
                StatusCode = statusCode,
                ErrorMessage = errorMessage,
                Description = "Subscription"
            };
            _cassandraRepository.Setup(
                c => c.GetSingle(It.IsAny<string>(), It.IsAny<Expression<Func<TrumpiaRequestDetailEntity, bool>>>()))
                .Returns(mockTrumpiaRequest);

            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };

            var request = new SubscriptionRequest
            {
                AccountId = accountId,
                CustomerId = customerId
            };

            var model = new SubscriptionModel(_container);

            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));
            var result = model.GetSubscriptions(clientUser, request);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Message);
            Assert.IsNotNull(result.SmsWarningCode);
            Assert.AreEqual(expectedStatusCode, result.SmsWarningCode);
            _clientConfigFacade.Verify();
        }
        [TestMethod()]
        public void SubscriptionV1_SubscribeInsights_NonblockNumber_Test()
        {
            var customerManager = new Mock<ICustomer>();
            var subscriptionFacadeManager = new Mock<ISubscriptionFacade>();
            _container.RegisterInstance(_cassandraRepository.Object);
            _container.RegisterInstance(customerManager.Object);
            _container.RegisterInstance(subscriptionFacadeManager.Object);

            var programInsight1 = "Program1.ServiceLevelCostThreshold";

            const int clientId = 87;
            const string customerId = "BTD001";
            const string accountId = "BTD001a1";
            const string premiseId = "BTD001a1p1";
            const string serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            const string channel = "sms";
            var firstname = "Anjana";
            var lastname = "Khaire";
            var addr1 = "16 Laurel Ave";
            var addr2 = "Suite 100";
            var city = "Wellesley";
            var state = "MA";
            var zip = "02481";
            var email = "";
            var phone = "1234567890";
            var requestId = "TestRequestId";
            var subscriptionId = "testSubscriptionId";
            var statusCode = "";
            var statusMessage = "";
            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();
            // mock customer entity
            var mockCustomer = TestHelpers.CreateMockResidentialCustomerEntityData(clientId, customerId, firstname, lastname,
                addr1, addr2, city, state, zip, email, phone);
            var mockCustomerModel = Mapper.Map<CustomerEntity, CustomerModel>(mockCustomer);
            var mockUpdatedCustomer = TestHelpers.CreateMockResidentialCustomerEntityData(clientId, customerId, firstname, lastname,
                addr1, addr2, city, state, zip, email, phone);
            var mockUpdatedCustomerModel = Mapper.Map<CustomerEntity, CustomerModel>(mockUpdatedCustomer);
            mockUpdatedCustomerModel.TrumpiaRequestDetailId = requestId;
            mockUpdatedCustomerModel.TrumpiaSubscriptionId = subscriptionId;
            customerManager.SetupSequence(c => c.GetCustomerAsync(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(mockCustomerModel))
                .Returns(Task.FromResult(mockUpdatedCustomerModel));
            customerManager.Setup(
               c =>
                   c.CreateSubscriptionAndUpdateCustomer(It.IsAny<CustomerModel>(), It.IsAny<string>(),
                       It.IsAny<string>(), It.IsAny<string>())).Returns(true);
            // mock insert subscription
            _cassandraRepository.Setup(c => c.InsertOrUpdateAsync(It.IsAny<SubscriptionEntity>(), It.IsAny<string>()))
                .Returns(Task.FromResult(true));
            _cassandraRepository.Setup(
                c => c.GetSingle(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>())).Returns(
                    new SubscriptionEntity
                    {
                        AccountId = accountId,
                        CustomerId = customerId,
                        ClientId = clientId,
                        ProgramName = "Program1",
                        InsightTypeName = "BillToDate",
                        Channel = "Sms",
                        IsSelected = true,
                        ServiceContractId = serviceContractId1,
                        IsEmailOptInCompleted = true
                    }
                );

            // mock subscription facade double optin 
            var refVale = string.Empty;
            var subscriptionModel = new AO.Models.SubscriptionModel
            {
                Channel = "sms",
                AccountId = accountId,
                ClientId = clientId,
                CustomerId = customerId,
                InsightTypeName = "insighttest",
                IsDoubleOptIn = true
            };
            var subscriptionList = new List<AO.Models.SubscriptionModel>();
            subscriptionList.Add(subscriptionModel);
            var trumpiaRequestStatus = new TrumpiaRequestDetailModel
            {
                StatusCode =  statusCode,
                ErrorMessage = statusMessage
            };
            subscriptionFacadeManager.Setup(s => s.DoubleOptin(It.IsAny<AO.Models.SubscriptionModel>())).Returns(trumpiaRequestStatus);
            subscriptionFacadeManager.Setup(
                s =>
                    s.ValidateInsightList(It.IsAny<int>(), It.IsAny<List<AO.Models.SubscriptionModel>>(),
                        It.IsAny<string>(), It.IsAny<string>(), ref refVale))
                .Returns(new List<AO.Models.SubscriptionModel>());
            subscriptionFacadeManager.Setup(
                s =>
                    s.GetSubscribeList(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(),
                        It.IsAny<List<AO.Models.SubscriptionModel>>(), ref refVale)).Returns(subscriptionList);

            //GetSubscribeList(clientId, customerId, accountId,
            //        insightList, ref insightErrMsg)

            var subscribeList = new List<SubscribedInsight>();
            var subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = true,
                Level = "service",
                Name = programInsight1,
                PremiseId = premiseId,
                //ServiceContractId = serviceContractId1,
                ServiceId = serviceContractId1,
                Channel = channel
            };
            subscribeList.Add(subscribeInsight);

            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };

            var post = new SubscriptionPostRequest
            {
                Customer = new SubscribedCustomerInsight
                {
                    Id = customerId,
                    Insights = subscribeList
                }
            };

            var model = new SubscriptionModel(_container);

            var result = model.SubscribeInsights(clientUser, post);

            Assert.IsNotNull(result);
            Assert.IsNull(result.Message);
            _clientConfigFacade.Verify();
        }

        [TestMethod()]
        public void SubscriptionV1_SubscribeInsights_BlockNumber_Test()
        {
            var customerManager = new Mock<ICustomer>();
            var subscriptionFacadeManager = new Mock<ISubscriptionFacade>();
            _container.RegisterInstance(_cassandraRepository.Object);
            _container.RegisterInstance(customerManager.Object);
            _container.RegisterInstance(subscriptionFacadeManager.Object);

            var programInsight1 = "Program1.ServiceLevelCostThreshold";

            const int clientId = 87;
            const string customerId = "BTD001";
            const string accountId = "BTD001a1";
            const string premiseId = "BTD001a1p1";
            const string serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            const string channel = "sms";
            var firstname = "Anjana";
            var lastname = "Khaire";
            var addr1 = "16 Laurel Ave";
            var addr2 = "Suite 100";
            var city = "Wellesley";
            var state = "MA";
            var zip = "02481";
            var email = "";
            var phone = "1234567890";
            var requestId = "TestRequestId";
            var subscriptionId = "testSubscriptionId";
            var statusCode = "MPSE0501";
            var statusMessage = "BlockedNumber";
            var expectedStatusCode = "BlockedNumberWarnings";
            // mock clientsettings
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();
            // mock customer entity
            var mockCustomer = TestHelpers.CreateMockResidentialCustomerEntityData(clientId, customerId, firstname, lastname,
                addr1, addr2, city, state, zip, email, phone);
            var mockCustomerModel = Mapper.Map<CustomerEntity, CustomerModel>(mockCustomer);
            var mockUpdatedCustomer = TestHelpers.CreateMockResidentialCustomerEntityData(clientId, customerId, firstname, lastname,
                addr1, addr2, city, state, zip, email, phone);
            var mockUpdatedCustomerModel = Mapper.Map<CustomerEntity, CustomerModel>(mockUpdatedCustomer);
            mockUpdatedCustomerModel.TrumpiaRequestDetailId = requestId;
            mockUpdatedCustomerModel.TrumpiaSubscriptionId = subscriptionId;
            customerManager.SetupSequence(c => c.GetCustomerAsync(It.IsAny<int>(), It.IsAny<string>()))
                .Returns(Task.FromResult(mockCustomerModel))
                .Returns(Task.FromResult(mockUpdatedCustomerModel));
            customerManager.Setup(
               c =>
                   c.CreateSubscriptionAndUpdateCustomer(It.IsAny<CustomerModel>(), It.IsAny<string>(),
                       It.IsAny<string>(), It.IsAny<string>())).Returns(false);
            // mock insert subscription
            _cassandraRepository.Setup(c => c.InsertOrUpdateAsync(It.IsAny<SubscriptionEntity>(), It.IsAny<string>()))
                .Returns(Task.FromResult(true));
            _cassandraRepository.Setup(
                c => c.GetSingle(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>())).Returns(
                    new SubscriptionEntity
                    {
                        AccountId = accountId,
                        CustomerId = customerId,
                        ClientId = clientId,
                        ProgramName = "Program1",
                        InsightTypeName = "BillToDate",
                        Channel = "Sms",
                        IsSelected = true,
                        ServiceContractId = serviceContractId1,
                        IsEmailOptInCompleted = true
                    }
                );

            // mock subscription facade double optin 
            var refVale = string.Empty;
            var subscriptionModel = new AO.Models.SubscriptionModel
            {
                Channel = "sms",
                AccountId = accountId,
                ClientId = clientId,
                CustomerId = customerId,
                InsightTypeName = "insighttest",
                IsDoubleOptIn = true
            };
            var subscriptionList = new List<AO.Models.SubscriptionModel>();
            subscriptionList.Add(subscriptionModel);
            var trumpiaRequestStatus = new TrumpiaRequestDetailModel
            {
                StatusCode = statusCode,
                ErrorMessage = statusMessage
            };
            subscriptionFacadeManager.Setup(s => s.DoubleOptin(It.IsAny<AO.Models.SubscriptionModel>())).Returns(trumpiaRequestStatus);
            subscriptionFacadeManager.Setup(
                s =>
                    s.ValidateInsightList(It.IsAny<int>(), It.IsAny<List<AO.Models.SubscriptionModel>>(),
                        It.IsAny<string>(), It.IsAny<string>(), ref refVale))
                .Returns(new List<AO.Models.SubscriptionModel>());
            subscriptionFacadeManager.Setup(
                s =>
                    s.GetSubscribeList(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(),
                        It.IsAny<List<AO.Models.SubscriptionModel>>(), ref refVale)).Returns(subscriptionList);

            //GetSubscribeList(clientId, customerId, accountId,
            //        insightList, ref insightErrMsg)

            var subscribeList = new List<SubscribedInsight>();
            var subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = true,
                Level = "service",
                Name = programInsight1,
                PremiseId = premiseId,
                //ServiceContractId = serviceContractId1,
                ServiceId = serviceContractId1,
                Channel = channel
            };
            subscribeList.Add(subscribeInsight);

            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };

            var post = new SubscriptionPostRequest
            {
                Customer = new SubscribedCustomerInsight
                {
                    Id = customerId,
                    Insights = subscribeList
                }
            };

            var model = new SubscriptionModel(_container);

            var result = model.SubscribeInsights(clientUser, post);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Message);
            Assert.AreEqual(expectedStatusCode, result.SmsWarningCode);
            _clientConfigFacade.Verify();
        }
    }
}
