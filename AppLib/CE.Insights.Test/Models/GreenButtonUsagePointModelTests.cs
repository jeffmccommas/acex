﻿using AO.Business;
using AO.BusinessContracts;
using AO.Entities;
using AutoMapper;
using CE.AO.Models;
using CE.GreenButtonConnect;
using CE.Insights.Test.Helpers;
using CE.Models;
using CE.Models.Insights;
using CE.Models.Insights.Types;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;

// ReSharper disable once CheckNamespace
namespace CE.Insights.Models.Tests
{
    [TestClass()]
    public class GreenButtonUsagePointModelTests
    {

        private UnityContainer _container;
        private Mock<ITallAMI> _tallAmi;
        private GreenButtonConfig _config;
        private TestInsightsEntitiesMock _insightsEntitesMock;
        private Mock<InsightsEfRepository> _insightRepository;
        private string _greenButtonDomain = "http://www.greenbutton.com";


        public TestContext TestContext { get; set; }

        [TestInitialize]
        public void TestInit()
        {
            _container = new UnityContainer();
            SetupAutoMapper();

            _tallAmi = new Mock<ITallAMI>();

            _container.RegisterInstance(_tallAmi.Object);

            _config = TestGreenButtonHelper.GetGbConnectConfigSetting();

            _insightsEntitesMock = new TestInsightsEntitiesMock();

            _insightRepository = new Mock<InsightsEfRepository>(_insightsEntitesMock.MockInsightsEntities.Object);

        }
        
        [TestMethod(), TestCategory("GreenButtonConnect"), TestCategory("BVT")]
        [DeploymentItem("TestData\\GetUsagePointTestData.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
            "|DataDirectory|\\GetUsagePointTestData.xml", "UsagePoint", DataAccessMethod.Sequential)]
        public void GetUsagePointTest()
        {
            UsagePointTest();
        }

        [TestMethod(), TestCategory("GreenButtonConnect"), TestCategory("BVT")]
        [DeploymentItem("TestData\\GetUsagePointQueryParamTestData.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
    "|DataDirectory|\\GetUsagePointQueryParamTestData.xml", "UsagePoint", DataAccessMethod.Sequential)]
        public void GetUsagePoint_QueryParam_Test()
        {
            UsagePointTest();
        }

        [TestMethod(), TestCategory("GreenButtonConnect"), TestCategory("BVT")]
        [DeploymentItem("TestData\\GetUsagePointIntervalMeteringTestData.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
"|DataDirectory|\\GetUsagePointIntervalMeteringTestData.xml", "UsagePoint", DataAccessMethod.Sequential)]
        public void GetUsagePoint_IntervalMetering_Test()
        {
            UsagePointTest();
        }

        [TestMethod(), TestCategory("GreenButtonConnect"), TestCategory("BVT")]
        [DeploymentItem("TestData\\GetUsagePointCommodityIntervalTestData.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
"|DataDirectory|\\GetUsagePointCommodityIntervalTestData.xml", "UsagePoint", DataAccessMethod.Sequential)]
        public void GetUsagePoint_CommodityInterval_Test()
        {
            UsagePointTest();
        }


        [TestMethod(), TestCategory("GreenButtonConnect"), TestCategory("BVT")]
        [DeploymentItem("TestData\\GetAllUsagePonintsTestData.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
"|DataDirectory|\\GetAllUsagePonintsTestData.xml", "UsagePoint", DataAccessMethod.Sequential)]
        public void GetAllUsagePointsTest()
        {
            try
            {
                var clientId = Convert.ToInt32(TestContext.DataRow["clientId"].ToString());
                var accountId = TestContext.DataRow["accountId"].ToString();
                var meterid1 = TestContext.DataRow["meterid1"].ToString();
                var meterid2 = TestContext.DataRow["meterid2"].ToString();
                var servicePointId = TestContext.DataRow["servicePointId"].ToString();
                var isBatch = Convert.ToBoolean(TestContext.DataRow["isBatch"].ToString());
                var start = TestContext.DataRow["StartDate"].ToString();
                var end = TestContext.DataRow["EndDate"].ToString();
                DateTime? startDate = !string.IsNullOrEmpty(start) ? Convert.ToDateTime(start) : (DateTime?)null;
                DateTime? endDate = !string.IsNullOrEmpty(end) ? Convert.ToDateTime(end) : (DateTime?)null;
                var commodityType = Convert.ToInt32(TestContext.DataRow["commodityType"].ToString());
                var uomid = Convert.ToInt32(TestContext.DataRow["uomid"].ToString());
                var accessToken = TestContext.DataRow["accessToken"].ToString();
                var subscriptionId = Convert.ToInt64(TestContext.DataRow["subscriptionId"].ToString());
                var clientSecretCode = TestContext.DataRow["clientSecretCode"].ToString();
                var status = TestContext.DataRow["status"].ToString();

                var mockAmiEnd = endDate ?? DateTime.UtcNow.Date;
                var mockAmiStart = startDate ?? DateTime.UtcNow.Date;
                var amiMock1 = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterid1, servicePointId,
                    mockAmiStart, mockAmiEnd, mockAmiEnd.AddDays(10), commodityType, uomid);
                var amiMock2 = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterid2, servicePointId,
                    mockAmiStart, mockAmiEnd, mockAmiEnd.AddDays(10), commodityType, uomid);
                var readingMock = TestHelpers.CreateMockAmiHourlyReading(mockAmiEnd, mockAmiEnd);
                _tallAmi.Setup(a => a.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), meterid1, accountId, clientId))
                    .Returns(amiMock1)
                    .Verifiable();
                _tallAmi.Setup(a => a.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), meterid2, accountId, clientId))
                   .Returns(amiMock2)
                   .Verifiable();
                int testNoOfDays;
                _tallAmi.Setup(
                    m => m.GetReadings(It.IsAny<IEnumerable<TallAmiModel>>(), out testNoOfDays, It.IsAny<bool>()))
                    .Returns(readingMock);
                var authorizations = GetAuthorizations(accessToken, clientSecretCode);

                var meters = new List<string>();
                //meters.Add(TestContext.DataRow["meterid1"].ToString());
                //meters.Add(TestContext.DataRow["meterid2"].ToString());

                _insightRepository.Setup(x => x.GetMeterList(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<GreenButtonScope>())).Returns(() => meters);

                var model = new GreenButtonUsagePointModel(clientId, _greenButtonDomain, authorizations, _config, _container, _insightRepository.Object);
                var result = model.GetAllUsagePointsGreenButton(subscriptionId, accessToken, startDate, endDate, isBatch);

                Assert.IsNotNull(result);
                Assert.AreEqual(status, result.Status.ToString());
                if (status == "NoError")
                {
                    Assert.IsNotNull(result.GreenButtonXml);
                    if (isBatch)
                        _tallAmi.Verify();
                }
                else
                    Assert.IsNull(result.GreenButtonXml);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        private void UsagePointTest()
        {
            try
            {
                var clientId = Convert.ToInt32(TestContext.DataRow["clientId"].ToString());
                var accountId = TestContext.DataRow["accountId"].ToString();
                var meterid = TestContext.DataRow["meterid"].ToString();
                var encrytedMeterid = Helper.ConvertStringToHex(meterid, Encoding.Unicode);
                var servicePointId = TestContext.DataRow["servicePointId"].ToString();
                var isBatch = Convert.ToBoolean(TestContext.DataRow["isBatch"].ToString());
                var isDaily = Convert.ToBoolean(TestContext.DataRow["isDaily"].ToString());
                var isTou = Convert.ToBoolean(TestContext.DataRow["isTou"].ToString());
                var start = TestContext.DataRow["StartDate"].ToString();
                var end = TestContext.DataRow["EndDate"].ToString();
                DateTime? startDate = !string.IsNullOrEmpty(start) ? Convert.ToDateTime(start) : (DateTime?)null;
                DateTime? endDate = !string.IsNullOrEmpty(end) ? Convert.ToDateTime(end) : (DateTime?)null;
                var commodityType = Convert.ToInt32(TestContext.DataRow["commodityType"].ToString());
                var uomid = Convert.ToInt32(TestContext.DataRow["uomid"].ToString());
                var accessToken = TestContext.DataRow["accessToken"].ToString();
                var clientSecretCode = TestContext.DataRow["clientSecretCode"].ToString();
                var subscriptionId = Convert.ToInt64(TestContext.DataRow["subscriptionId"].ToString());
                var status = TestContext.DataRow["status"].ToString();

                var mockAmiEnd = endDate ?? DateTime.UtcNow.Date;
                var mockAmiStart = startDate ?? DateTime.UtcNow.Date;
                List<TallAmiModel> amiMock;
                List<RateModel.Reading> readingMock;
                if (isDaily)
                {
                    if(isTou)
                        amiMock = TestHelpers.CreateMockAmiDailyModelWithTou(clientId, accountId, meterid, servicePointId,
                            mockAmiStart, mockAmiEnd, mockAmiEnd.AddDays(10), commodityType, uomid);
                    else
                        amiMock = TestHelpers.CreateMockAmiDailyModel(clientId, accountId, meterid, servicePointId,
                        mockAmiStart, mockAmiEnd, mockAmiEnd.AddDays(10), commodityType, uomid);
                    readingMock = TestHelpers.CreateMockAmiDailyReading(mockAmiEnd, mockAmiEnd);
                }
                else
                {
                    amiMock = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterid, servicePointId,
                        mockAmiStart, mockAmiEnd, mockAmiEnd.AddDays(10), commodityType, uomid);
                    readingMock = TestHelpers.CreateMockAmiHourlyReading(mockAmiEnd, mockAmiEnd);
                    
                }

                _tallAmi.Setup(a => a.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), meterid, accountId, clientId))
                    .Returns(amiMock)
                    .Verifiable();
                int testNoOfDays;
                _tallAmi.Setup(
                    m => m.GetReadings(It.IsAny<IEnumerable<TallAmiModel>>(), out testNoOfDays, It.IsAny<bool>()))
                    .Returns(readingMock);
                var authorizations = GetAuthorizations(accessToken, clientSecretCode);

                var meters = new List<string>();
                meters.Add(TestContext.DataRow["meterid"].ToString());

                _insightRepository.Setup(x => x.GetMeterList(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<GreenButtonScope>())).Returns(() => meters);


                var model = new GreenButtonUsagePointModel(clientId, _greenButtonDomain, authorizations, _config, _container, _insightRepository.Object);
                var result = model.GetUsagePointGreenButton(subscriptionId, accessToken, encrytedMeterid, startDate,
                    endDate, isBatch);

                Assert.IsNotNull(result);
                Assert.AreEqual(status, result.Status.ToString());
                if (status == "NoError")
                {
                    Assert.IsNotNull(result.GreenButtonXml);
                    if (isBatch)
                        _tallAmi.Verify();
                }
                else
                    Assert.IsNull(result.GreenButtonXml);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        private void SetupAutoMapper()
        {
            Mapper.CreateMap<RateModel.Reading, ConsumptionData>()
                 .ForMember(dest => dest.DateTime, opts => opts.MapFrom(src => src.Timestamp))
                 .ForMember(dest => dest.Value,
                     opts =>
                         opts.MapFrom(
                             src => src.TimeOfUse == RateModel.Enums.TimeOfUse.Undefined ? (decimal)src.Quantity : 0))
                 .ForAllUnmappedMembers(o => o.Ignore());

            Mapper.CreateMap<RateModel.Reading, ConsumptionTouData>()
                .ForMember(dest => dest.Value, opts => opts.MapFrom(src => (decimal)src.Quantity))
                .ForMember(dest => dest.TouKey,
                    opts =>
                        opts.MapFrom(
                            src =>
                                src.TimeOfUse == RateModel.Enums.TimeOfUse.Undefined
                                    ? TimeOfUseType.NoTou.ToString().ToLower()
                                    : src.TimeOfUse.ToString().ToLower()))
                .ForAllUnmappedMembers(o => o.Ignore());


            Mapper.CreateMap<ConsumptionByMeterEntity, TallAmiModel>()
    .ForAllUnmappedMembers(o => o.Ignore());

            Mapper.CreateMap<TallAmiModel, ConsumptionByMeterEntity>()
                .ForAllUnmappedMembers(o => o.Ignore());
        }
        
        private List<GreenButtonAuthorization> GetAuthorizations(string accessToken, string clientSecretCode)
        {
            var insightsEntitesMock = new TestInsightsEntitiesMock();
            var insightRepository = new InsightsEfRepository(insightsEntitesMock.MockInsightsEntities.Object);

            var client = clientSecretCode.Split(',')[0];
            var secret = clientSecretCode.Split(',')[1];

            var authorizations = insightRepository.GetGreenButtonAuthorizations(client, secret);

            return authorizations.FindAll(a => a.AccessToken == accessToken);
        }

       

    }
}
