﻿using AO.Business;
using AO.BusinessContracts;
using AO.Entities;
using AutoMapper;
using CE.AO.Models;
using CE.GreenButtonConnect;
using CE.Insights.Models;
using CE.Insights.Test.Helpers;
using CE.Models;
using CE.Models.Insights;
using CE.Models.Insights.Types;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;


namespace CE.Insights.Test.Models
{
    [TestClass()]
    public class GreenButtonBaseModelTests
    {
        private UnityContainer _container;
        private Mock<ITallAMI> _tallAmi;
        private GreenButtonConfig _config;
        private TestInsightsEntitiesMock _insightsEntitesMock;
        private Mock<InsightsEfRepository> _insightRepository;

        private string _greenButtonDomain = "http://www.greenbutton.com";

        public TestContext TestContext { get; set; }

        [TestInitialize]
        public void TestInit()
        {
            _container = new UnityContainer();
            SetupAutoMapper();

            _tallAmi = new Mock<ITallAMI>();

            _container.RegisterInstance(_tallAmi.Object);

            _config = TestGreenButtonHelper.GetGbConnectConfigSetting();

            _insightsEntitesMock = new TestInsightsEntitiesMock();

            _insightRepository = new Mock<InsightsEfRepository>(_insightsEntitesMock.MockInsightsEntities.Object);

        }

        [TestMethod(), TestCategory("GreenButtonConnect"), TestCategory("BVT")]
        [DeploymentItem("TestData\\GetReadingTypesTestData.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
            "|DataDirectory|\\GetReadingTypesTestData.xml", "ReadingType", DataAccessMethod.Sequential)]
        public void GetReadingTypesByAmiListTest()
        {
            try
            {
                var clientId = Convert.ToInt32(TestContext.DataRow["clientId"].ToString());
                var accountId = TestContext.DataRow["accountId"].ToString();
                var meterid = TestContext.DataRow["meterid"].ToString();
                var servicePointId = TestContext.DataRow["servicePointId"].ToString();
                var isBatch = Convert.ToBoolean(TestContext.DataRow["isBatch"].ToString());
                var startDate = Convert.ToDateTime(TestContext.DataRow["startDate"].ToString());
                var endDate = Convert.ToDateTime(TestContext.DataRow["endDate"].ToString());
                var commodityType = Convert.ToInt32(TestContext.DataRow["commodityType"].ToString());
                var uomid = Convert.ToInt32(TestContext.DataRow["uomid"].ToString());
                var accessToken = TestContext.DataRow["accessToken"].ToString();
                var clientSecretCode = TestContext.DataRow["clientSecretCode"].ToString();
                var singleInterval = Convert.ToBoolean(TestContext.DataRow["singleInterval"].ToString());
                var expectedCount = Convert.ToInt32(TestContext.DataRow["ExpectedCount"].ToString());

                var amiMock = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterid, servicePointId,
                    startDate, endDate, endDate.AddDays(10), commodityType, uomid);
                var scopeMock = GetMockScope(accessToken, clientSecretCode);
                var readingMock = TestHelpers.CreateMockAmiHourlyReading(startDate, endDate);

                if (!singleInterval)
                {
                    var ami15Mock = TestHelpers.CreateMockAmi15AmiModel(clientId, accountId, meterid, servicePointId,
                        startDate, endDate, endDate.AddDays(10));
                    amiMock.AddRange(ami15Mock);

                    var reading15Mock = TestHelpers.CreateMockAmi15Reading(startDate, endDate);

                    int testNoOfDays;
                    _tallAmi.SetupSequence(m => m.GetReadings(It.IsAny<IEnumerable<TallAmiModel>>(), out testNoOfDays, It.IsAny<bool>()))
                    .Returns(readingMock)
                    .Returns(reading15Mock);
                }
                else
                {
                    int testNoOfDays;
                    _tallAmi.Setup(
                        m => m.GetReadings(It.IsAny<IEnumerable<TallAmiModel>>(), out testNoOfDays, It.IsAny<bool>()))
                        .Returns(readingMock);
                }
                var authorizations = new List<GreenButtonAuthorization>
                {
                    new GreenButtonAuthorization
                    {
                        ClientId = clientId.ToString(),
                        AuthorizationId = 90,
                        ApplicationInformationId = 26,
                        SubscriptionId = 1,
                        AccessToken = accessToken
                    }
                };

                var model = new GreenButtonBaseModel(clientId, _greenButtonDomain, authorizations, _container, _insightRepository.Object, _config);
                var result = model.GetReadingTypes(amiMock, scopeMock, isBatch);

                Assert.IsNotNull(result);
                Assert.AreEqual(expectedCount, result.Count);
                foreach (var readingType in result)
                {
                    if (isBatch)
                    {
                        Assert.IsNotNull(readingType.ConsumptionList);
                    }
                    else
                        Assert.IsNull(readingType.ConsumptionList);
                }
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod(), TestCategory("GreenButtonConnect"), TestCategory("BVT")]
        [DeploymentItem("TestData\\GetReadingTypesByMeterIdTestData.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
            "|DataDirectory|\\GetReadingTypesByMeterIdTestData.xml", "ReadingType", DataAccessMethod.Sequential)]
        public void GetReadingTypesByMeterIdTest()
        {
            try
            {
                var clientId = Convert.ToInt32(TestContext.DataRow["clientId"].ToString());
                var accountId = TestContext.DataRow["accountId"].ToString();
                var meterid = TestContext.DataRow["meterid"].ToString();
                var servicePointId = TestContext.DataRow["servicePointId"].ToString();
                var startDate = Convert.ToDateTime(TestContext.DataRow["startDate"].ToString());
                var endDate = Convert.ToDateTime(TestContext.DataRow["endDate"].ToString());
                var commodityType = Convert.ToInt32(TestContext.DataRow["commodityType"].ToString());
                var uomid = Convert.ToInt32(TestContext.DataRow["uomid"].ToString());
                var accessToken = TestContext.DataRow["accessToken"].ToString();
                var clientSecretCode = TestContext.DataRow["clientSecretCode"].ToString();
                var singleInterval = Convert.ToBoolean(TestContext.DataRow["singleInterval"].ToString());
                var expectedCount = Convert.ToInt32(TestContext.DataRow["ExpectedCount"].ToString());

                var amiMock = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterid, servicePointId,
                    startDate, endDate, endDate.AddDays(10), commodityType, uomid);
                var scopeMock = GetMockScope(accessToken, clientSecretCode);
                var readingMock = TestHelpers.CreateMockAmiHourlyReading(startDate, endDate);

                if (!singleInterval)
                {

                    var ami15Mock = TestHelpers.CreateMockAmi15AmiModel(clientId, accountId, meterid, servicePointId,
                        startDate, endDate, endDate.AddDays(10));
                    amiMock.AddRange(ami15Mock);

                    var reading15Mock = TestHelpers.CreateMockAmi15Reading(startDate, endDate);

                    int testNoOfDays;
                    _tallAmi.SetupSequence(m => m.GetReadings(It.IsAny<IEnumerable<TallAmiModel>>(), out testNoOfDays, It.IsAny<bool>()))
                    .Returns(readingMock)
                    .Returns(reading15Mock);
                }
                else
                {
                    int testNoOfDays;
                    _tallAmi.Setup(
                        m => m.GetReadings(It.IsAny<IEnumerable<TallAmiModel>>(), out testNoOfDays, It.IsAny<bool>()))
                        .Returns(readingMock);
                }
                _tallAmi.Setup(a => a.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), meterid, accountId, clientId))
                .Returns(amiMock)
                .Verifiable();
                var authorizations = new List<GreenButtonAuthorization>
                {
                    new GreenButtonAuthorization
                    {
                        ClientId = clientId.ToString(),
                        AuthorizationId = 90,
                        ApplicationInformationId = 26,
                        SubscriptionId = 1
                    }
                };

                var model = new GreenButtonBaseModel(clientId, _greenButtonDomain, authorizations, _container, _insightRepository.Object, _config);
                var status = GreenButtonErrorType.NoError;
                var result = model.GetReadingTypes(accountId, meterid, startDate,endDate,scopeMock, ref status);

                Assert.IsNotNull(result);
                Assert.AreEqual(expectedCount, result.Count);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod(), TestCategory("GreenButtonConnect"), TestCategory("BVT")]
        [DeploymentItem("TestData\\GetReadingTypeTestData.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
           "|DataDirectory|\\GetReadingTypeTestData.xml", "ReadingType", DataAccessMethod.Sequential)]
        public void GetReadingTypeTest()
        {
            try
            {
                var clientId = Convert.ToInt32(TestContext.DataRow["clientId"].ToString());
                var subscriptionId = Convert.ToInt64(TestContext.DataRow["SubscriptionId"].ToString());
                var meterId = TestContext.DataRow["meterId"].ToString();
                var readingTypeId = TestContext.DataRow["ReadingTypeId"].ToString();
                var hexedReadingTypeId = Helper.ConvertStringToHex(readingTypeId, Encoding.Unicode);
                var expectedStatus = TestContext.DataRow["status"].ToString();


                var authorizations = new List<GreenButtonAuthorization>
                {
                    new GreenButtonAuthorization
                    {
                        ClientId = clientId.ToString(),
                        AuthorizationId = 90,
                        ApplicationInformationId = 26,
                        SubscriptionId = subscriptionId
                    }
                };

                var model = new GreenButtonBaseModel(clientId, _greenButtonDomain, authorizations, _container, _insightRepository.Object, _config);
                var status = GreenButtonErrorType.NoError;
                var result = model.GetReadingType(subscriptionId,meterId,hexedReadingTypeId, ref status);

                Assert.AreEqual(expectedStatus, status.ToString());
                if(status == GreenButtonErrorType.NoError)
                    Assert.IsNotNull(result);
                else
                    Assert.IsNull(result);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod(), TestCategory("GreenButtonConnect"), TestCategory("BVT")]
        [DeploymentItem("TestData\\GetAllMeterReadingTestData.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
"|DataDirectory|\\GetAllMeterReadingTestData.xml", "MeterReading", DataAccessMethod.Sequential)]
        public void GetAllMeterReadingsTest()
        {
            try
            {
                var clientId = Convert.ToInt32(TestContext.DataRow["clientId"].ToString());
                var accountId = TestContext.DataRow["accountId"].ToString();
                var meterid1 = TestContext.DataRow["meterid"].ToString();
                var encrytedMeterid = Helper.ConvertStringToHex(meterid1, Encoding.Unicode);
                var servicePointId = TestContext.DataRow["servicePointId"].ToString();
                var generateMeterReading = Convert.ToBoolean(TestContext.DataRow["generateMeterReading"].ToString());
                var start = TestContext.DataRow["StartDate"].ToString();
                var end = TestContext.DataRow["EndDate"].ToString();
                DateTime? startDate = !string.IsNullOrEmpty(start) ? Convert.ToDateTime(start) : (DateTime?)null;
                DateTime? endDate = !string.IsNullOrEmpty(end) ? Convert.ToDateTime(end) : (DateTime?)null;
                var commodityType = Convert.ToInt32(TestContext.DataRow["commodityType"].ToString());
                var uomid = Convert.ToInt32(TestContext.DataRow["uomid"].ToString());
                var accessToken = TestContext.DataRow["accessToken"].ToString();
                var clientSecretCode = TestContext.DataRow["clientSecretCode"].ToString();
                var subscriptionId = Convert.ToInt64(TestContext.DataRow["subscriptionId"].ToString());
                var status = TestContext.DataRow["status"].ToString();

                var mockAmiEnd = endDate ?? DateTime.UtcNow.Date;
                var mockAmiStart = startDate ?? DateTime.UtcNow.Date;
                var amiMock1 = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterid1, servicePointId,
                    mockAmiStart, mockAmiEnd, mockAmiEnd.AddDays(10), commodityType, uomid);
                var readingMock = TestHelpers.CreateMockAmiHourlyReading(mockAmiEnd, mockAmiEnd);
                _tallAmi.Setup(a => a.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), meterid1, accountId, clientId))
                    .Returns(amiMock1)
                    .Verifiable();
                int testNoOfDays;
                _tallAmi.Setup(
                    m => m.GetReadings(It.IsAny<IEnumerable<TallAmiModel>>(), out testNoOfDays, It.IsAny<bool>()))
                    .Returns(readingMock);
                var authorizations = GetAuthorizations(accessToken, clientSecretCode);

                var model = new GreenButtonMeterReadingModel(clientId, _greenButtonDomain, authorizations, _config, _container, _insightRepository.Object);
                var result = model.GetAllMeterReadingsGreenButton(accessToken, subscriptionId, encrytedMeterid,
                    startDate, endDate);

                Assert.IsNotNull(result);
                Assert.AreEqual(status, result.Status.ToString());
                if (status == "NoError")
                {
                    Assert.IsNotNull(result.GreenButtonXml);
                    if (generateMeterReading)
                        _tallAmi.Verify();
                }
                else
                    Assert.IsNull(result.GreenButtonXml);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod(), TestCategory("GreenButtonConnect"), TestCategory("BVT")]
        [DeploymentItem("TestData\\GetMeterReadingTestData.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
"|DataDirectory|\\GetMeterReadingTestData.xml", "MeterReading", DataAccessMethod.Sequential)]
        public void GetMeterReadingTest()
        {
            try
            {
                var clientId = Convert.ToInt32(TestContext.DataRow["clientId"].ToString());
                var meterid1 = TestContext.DataRow["meterid"].ToString();
                var encrytedMeterid = Helper.ConvertStringToHex(meterid1, Encoding.Unicode);
                var accessToken = TestContext.DataRow["accessToken"].ToString();
                var clientSecretCode = TestContext.DataRow["clientSecretCode"].ToString();
                var subscriptionId = Convert.ToInt64(TestContext.DataRow["subscriptionId"].ToString());
                var readingTypeId = TestContext.DataRow["ReadingTypeId"].ToString();
                var hexedReadingTypeId = Helper.ConvertStringToHex(readingTypeId, Encoding.Unicode);
                var status = TestContext.DataRow["status"].ToString();

                
                var authorizations = GetAuthorizations(accessToken, clientSecretCode);

                var model = new GreenButtonMeterReadingModel(clientId, _greenButtonDomain, authorizations, _config, _container, _insightRepository.Object);

                var result = model.GetMeterReadingGreenButton(accessToken, subscriptionId, encrytedMeterid,
                    hexedReadingTypeId);

                Assert.IsNotNull(result);
                Assert.AreEqual(status, result.Status.ToString());
                if (status == "NoError")
                {
                    Assert.IsNotNull(result.GreenButtonXml);
                }
                else
                    Assert.IsNull(result.GreenButtonXml);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }


        [TestMethod(), TestCategory("GreenButtonConnect"), TestCategory("BVT")]
        [DeploymentItem("TestData\\GetAllReadingTypesTestData.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
"|DataDirectory|\\GetAllReadingTypesTestData.xml", "ReadingType", DataAccessMethod.Sequential)]
        public void GetAllReadingTypesTest()
        {
            try
            {
                var clientId = Convert.ToInt32(TestContext.DataRow["clientId"].ToString());
                var accountId = TestContext.DataRow["accountId"].ToString();
                var meterid1 = TestContext.DataRow["meterid"].ToString();
                var servicePointId = TestContext.DataRow["servicePointId"].ToString();
                var start = TestContext.DataRow["StartDate"].ToString();
                var end = TestContext.DataRow["EndDate"].ToString();
                DateTime? startDate = !string.IsNullOrEmpty(start) ? Convert.ToDateTime(start) : (DateTime?)null;
                DateTime? endDate = !string.IsNullOrEmpty(end) ? Convert.ToDateTime(end) : (DateTime?)null;
                var commodityType = Convert.ToInt32(TestContext.DataRow["commodityType"].ToString());
                var uomid = Convert.ToInt32(TestContext.DataRow["uomid"].ToString());
                var accessToken = TestContext.DataRow["accessToken"].ToString();
                var clientSecretCode = TestContext.DataRow["clientSecretCode"].ToString();
                var status = TestContext.DataRow["status"].ToString();

                var meters = new List<string>();
                meters.Add(TestContext.DataRow["meterid"].ToString());

                _insightRepository.Setup(x => x.GetMeterList(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<GreenButtonScope>())).Returns(() => meters);


                var mockAmiEnd = endDate ?? DateTime.UtcNow.Date;
                var mockAmiStart = startDate ?? DateTime.UtcNow.Date;
                var amiMock1 = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterid1, servicePointId,
                    mockAmiStart, mockAmiEnd, mockAmiEnd.AddDays(10), commodityType, uomid);
                var readingMock = TestHelpers.CreateMockAmiHourlyReading(mockAmiEnd, mockAmiEnd);
                _tallAmi.Setup(a => a.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<string>(), accountId, clientId))
                    .Returns(amiMock1);
                int testNoOfDays;
                _tallAmi.Setup(
                    m => m.GetReadings(It.IsAny<IEnumerable<TallAmiModel>>(), out testNoOfDays, It.IsAny<bool>()))
                    .Returns(readingMock);
                var authorizations = GetAuthorizations(accessToken, clientSecretCode);

                var model =  new GreenButtonReadingTypeModel(clientId,_greenButtonDomain,authorizations,_config, _container, _insightRepository.Object);
                var result = model.GetAllReadingTypesGreenButton(accessToken);
                

                Assert.IsNotNull(result);
                Assert.AreEqual(status, result.Status.ToString());
                if (status == "NoError")
                {
                    Assert.IsNotNull(result.GreenButtonXml);
                }
                else
                    Assert.IsNull(result.GreenButtonXml);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod(), TestCategory("GreenButtonConnect"), TestCategory("BVT")]
        [DeploymentItem("TestData\\GetReadingTypeTestData2.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
"|DataDirectory|\\GetReadingTypeTestData2.xml", "ReadingType", DataAccessMethod.Sequential)]
        public void GetReadingTypeGreenButtonTest()
        {
            try
            {
                var clientId = Convert.ToInt32(TestContext.DataRow["clientId"].ToString());
                var accessToken = TestContext.DataRow["accessToken"].ToString();
                var clientSecretCode = TestContext.DataRow["clientSecretCode"].ToString();
                var status = TestContext.DataRow["status"].ToString();
                var readingTypeId = TestContext.DataRow["readingTypeId"].ToString();
                var hexedReadingTypeId = Helper.ConvertStringToHex(readingTypeId, Encoding.Unicode);

                var meters = new List<string>();
                meters.Add(TestContext.DataRow["meterid"].ToString());

                _insightRepository.Setup(x => x.GetMeterList(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<GreenButtonScope>())).Returns(() => meters);


                var authorizations = GetAuthorizations(accessToken, clientSecretCode);

                var model = new GreenButtonReadingTypeModel(clientId, _greenButtonDomain, authorizations, _config, _container, _insightRepository.Object);
                var result = model.GetReadingTypesGreenButton(accessToken, hexedReadingTypeId);


                Assert.IsNotNull(result);
                Assert.AreEqual(status, result.Status.ToString());
                if (status == "NoError")
                {
                    Assert.IsNotNull(result.GreenButtonXml);
                }
                else
                    Assert.IsNull(result.GreenButtonXml);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod(), TestCategory("GreenButtonConnect"), TestCategory("BVT")]
        [DeploymentItem("TestData\\GetAllLocalTimeParamsTestData.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
"|DataDirectory|\\GetAllLocalTimeParamsTestData.xml", "LocalTimeParam", DataAccessMethod.Sequential)]
        public void GetAllLocalTimeParmasTest()
        {
            try
            {
                var clientId = Convert.ToInt32(TestContext.DataRow["clientId"].ToString());
                var accountId = TestContext.DataRow["accountId"].ToString();
                var meterid1 = TestContext.DataRow["meterid"].ToString();
                var servicePointId = TestContext.DataRow["servicePointId"].ToString();
                var start = TestContext.DataRow["StartDate"].ToString();
                var end = TestContext.DataRow["EndDate"].ToString();
                DateTime? startDate = !string.IsNullOrEmpty(start) ? Convert.ToDateTime(start) : (DateTime?)null;
                DateTime? endDate = !string.IsNullOrEmpty(end) ? Convert.ToDateTime(end) : (DateTime?)null;
                var commodityType = Convert.ToInt32(TestContext.DataRow["commodityType"].ToString());
                var uomid = Convert.ToInt32(TestContext.DataRow["uomid"].ToString());
                var accessToken = TestContext.DataRow["accessToken"].ToString();
                var clientSecretCode = TestContext.DataRow["clientSecretCode"].ToString();
                var status = TestContext.DataRow["status"].ToString();

                var meters = new List<string>();
                meters.Add(TestContext.DataRow["meterid"].ToString());

                _insightRepository.Setup(x => x.GetMeterList(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<GreenButtonScope>())).Returns(() => meters);


                var mockAmiEnd = endDate ?? DateTime.UtcNow.Date;
                var mockAmiStart = startDate ?? DateTime.UtcNow.Date;
                var amiMock1 = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterid1, servicePointId,
                    mockAmiStart, mockAmiEnd, mockAmiEnd.AddDays(10), commodityType, uomid);
                var readingMock = TestHelpers.CreateMockAmiHourlyReading(mockAmiEnd, mockAmiEnd);
                _tallAmi.Setup(a => a.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<string>(), accountId, clientId))
                    .Returns(amiMock1);
                int testNoOfDays;
                _tallAmi.Setup(
                    m => m.GetReadings(It.IsAny<IEnumerable<TallAmiModel>>(), out testNoOfDays, It.IsAny<bool>()))
                    .Returns(readingMock);
                var authorizations = GetAuthorizations(accessToken, clientSecretCode);

                var model = new GreenButtonLocalTimeParametersModel(clientId, _greenButtonDomain, authorizations, _config, _container, _insightRepository.Object);
                var result = model.GetAllLocalTimeParamatersGreenButton(accessToken);


                Assert.IsNotNull(result);
                Assert.AreEqual(status, result.Status.ToString());
                if (status == "NoError")
                {
                    Assert.IsNotNull(result.GreenButtonXml);
                }
                else
                    Assert.IsNull(result.GreenButtonXml);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod(), TestCategory("GreenButtonConnect"), TestCategory("BVT")]
        [DeploymentItem("TestData\\GetLocalTimeParamTestData.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
"|DataDirectory|\\GetLocalTimeParamTestData.xml", "LocalTimeParam", DataAccessMethod.Sequential)]
        public void GetLocalTimeParmaTest()
        {
            try
            {
                var clientId = Convert.ToInt32(TestContext.DataRow["clientId"].ToString());
                var accountId = TestContext.DataRow["accountId"].ToString();
                var meterid1 = TestContext.DataRow["meterid"].ToString();
                var localTimeParamId = Convert.ToInt32(TestContext.DataRow["localtTimeParamId"].ToString());
                var servicePointId = TestContext.DataRow["servicePointId"].ToString();
                var start = TestContext.DataRow["StartDate"].ToString();
                var end = TestContext.DataRow["EndDate"].ToString();
                DateTime? startDate = !string.IsNullOrEmpty(start) ? Convert.ToDateTime(start) : (DateTime?)null;
                DateTime? endDate = !string.IsNullOrEmpty(end) ? Convert.ToDateTime(end) : (DateTime?)null;
                var commodityType = Convert.ToInt32(TestContext.DataRow["commodityType"].ToString());
                var uomid = Convert.ToInt32(TestContext.DataRow["uomid"].ToString());
                var accessToken = TestContext.DataRow["accessToken"].ToString();
                var clientSecretCode = TestContext.DataRow["clientSecretCode"].ToString();
                var status = TestContext.DataRow["status"].ToString();

                var meters = new List<string>();
                meters.Add(TestContext.DataRow["meterid"].ToString());

                _insightRepository.Setup(x => x.GetMeterList(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<GreenButtonScope>())).Returns(() => meters);




                var mockAmiEnd = endDate ?? DateTime.UtcNow.Date;
                var mockAmiStart = startDate ?? DateTime.UtcNow.Date;
                var amiMock1 = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterid1, servicePointId,
                    mockAmiStart, mockAmiEnd, mockAmiEnd.AddDays(10), commodityType, uomid);
                var readingMock = TestHelpers.CreateMockAmiHourlyReading(mockAmiEnd, mockAmiEnd);
                _tallAmi.Setup(a => a.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<string>(), accountId, clientId))
                    .Returns(amiMock1);
                int testNoOfDays;
                _tallAmi.Setup(
                    m => m.GetReadings(It.IsAny<IEnumerable<TallAmiModel>>(), out testNoOfDays, It.IsAny<bool>()))
                    .Returns(readingMock);
                var authorizations = GetAuthorizations(accessToken, clientSecretCode);

                var model = new GreenButtonLocalTimeParametersModel(clientId, _greenButtonDomain, authorizations, _config, _container, _insightRepository.Object);
                var result = model.GetLocalTimeParamaterGreenButton(accessToken, localTimeParamId);


                Assert.IsNotNull(result);
                Assert.AreEqual(status, result.Status.ToString());
                if (status == "NoError")
                {
                    Assert.IsNotNull(result.GreenButtonXml);
                }
                else
                    Assert.IsNull(result.GreenButtonXml);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod(), TestCategory("GreenButtonConnect"), TestCategory("BVT")]
        [DeploymentItem("TestData\\GetAllIntervalBlocksTestData.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
"|DataDirectory|\\GetAllIntervalBlocksTestData.xml", "IntervalBlock", DataAccessMethod.Sequential)]
        public void GetAllIntervalBlocksTest()
        {
            try
            {

                var clientId = Convert.ToInt32(TestContext.DataRow["clientId"].ToString());
                var meterid = TestContext.DataRow["meterid"].ToString();
                var encrytedMeterid = Helper.ConvertStringToHex(meterid, Encoding.Unicode);
                var accessToken = TestContext.DataRow["accessToken"].ToString();
                var clientSecretCode = TestContext.DataRow["clientSecretCode"].ToString();
                var encryptedAccessToken = GenerateAccessToken(clientSecretCode);
                if (!clientSecretCode.Contains("Encrypted"))
                    encryptedAccessToken = accessToken;
                var subscriptionId = Convert.ToInt64(TestContext.DataRow["subscriptionId"].ToString());
                var readingTypeId = TestContext.DataRow["ReadingTypeId"].ToString();
                var hexedReadingTypeId = Helper.ConvertStringToHex(readingTypeId, Encoding.Unicode);
                var status = TestContext.DataRow["status"].ToString();
                var accountId = TestContext.DataRow["accountId"].ToString();
                var servicePointId = TestContext.DataRow["servicePointId"].ToString();
                var generateAmi = Convert.ToBoolean(TestContext.DataRow["generateAmi"].ToString());
                var start = TestContext.DataRow["startDate"].ToString();
                var end = TestContext.DataRow["endDate"].ToString();
                DateTime? startDate = !string.IsNullOrEmpty(start) ? Convert.ToDateTime(start) : (DateTime?)null;
                DateTime? endDate = !string.IsNullOrEmpty(end) ? Convert.ToDateTime(end) : (DateTime?)null;
                var commodityType = Convert.ToInt32(TestContext.DataRow["commodityType"].ToString());
                var uomid = Convert.ToInt32(TestContext.DataRow["uomid"].ToString());
                
                if(generateAmi)
                {
                    var mockAmiEnd = endDate ?? DateTime.UtcNow.Date;
                    var mockAmiStart = startDate ?? DateTime.UtcNow.Date;
                    var amiMock1 = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterid, servicePointId,
                        mockAmiStart, mockAmiEnd, mockAmiEnd.AddDays(10), commodityType, uomid);
                   //var scopeMock = GetMockScope(accessToken);
                    var readingMock = TestHelpers.CreateMockAmiHourlyReading(mockAmiEnd, mockAmiEnd);
                    _tallAmi.Setup(a => a.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), meterid, accountId, clientId))
                        .Returns(amiMock1)
                        .Verifiable();
                    int testNoOfDays;
                    _tallAmi.Setup(
                        m => m.GetReadings(It.IsAny<IEnumerable<TallAmiModel>>(), out testNoOfDays, It.IsAny<bool>()))
                        .Returns(readingMock);
                }
                var authorizations = GetAuthorizations(encryptedAccessToken, clientSecretCode);

                var model = new GreenButtonIntervalBlockModel(clientId, _greenButtonDomain, authorizations, _config, _container, _insightRepository.Object);
                var result = model.GetAllIntervalBlocksGreenButton(encryptedAccessToken, subscriptionId, encrytedMeterid,
                    hexedReadingTypeId, startDate, endDate);

                Assert.IsNotNull(result);
                Assert.AreEqual(status, result.Status.ToString());
                if (status == "NoError")
                {
                    Assert.IsNotNull(result.GreenButtonXml);
                    if (generateAmi)
                        _tallAmi.Verify();
                }
                else
                    Assert.IsNull(result.GreenButtonXml);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod(), TestCategory("GreenButtonConnect"), TestCategory("BVT")]
        [DeploymentItem("TestData\\GetIntervalBlocksTestData.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
"|DataDirectory|\\GetIntervalBlocksTestData.xml", "IntervalBlock", DataAccessMethod.Sequential)]
        public void GetIntervalBlocksTest()
        {
            try
            {

                var clientId = Convert.ToInt32(TestContext.DataRow["clientId"].ToString());
                var meterid = TestContext.DataRow["meterid"].ToString();
                var encrytedMeterid = Helper.ConvertStringToHex(meterid, Encoding.Unicode);
                var accessToken = TestContext.DataRow["accessToken"].ToString();
                var clientSecretCode = TestContext.DataRow["clientSecretCode"].ToString();
                var encryptedAccessToken = GenerateAccessToken(clientSecretCode);
                if (!clientSecretCode.Contains("Encrypted"))
                    encryptedAccessToken = accessToken;
                var subscriptionId = Convert.ToInt64(TestContext.DataRow["subscriptionId"].ToString());
                var readingTypeId = TestContext.DataRow["ReadingTypeId"].ToString();
                var hexedReadingTypeId = Helper.ConvertStringToHex(readingTypeId, Encoding.Unicode);
                var status = TestContext.DataRow["status"].ToString();
                var accountId = TestContext.DataRow["accountId"].ToString();
                var servicePointId = TestContext.DataRow["servicePointId"].ToString();
                var generateAmi = Convert.ToBoolean(TestContext.DataRow["generateAmi"].ToString());
                var start = TestContext.DataRow["startDate"].ToString();
                var end = TestContext.DataRow["endDate"].ToString();
                var intervalBlockId = Helper.ConvertStringToHex($"{readingTypeId}_{start}", Encoding.Unicode);
                DateTime? startDate = !string.IsNullOrEmpty(start) ? Convert.ToDateTime(start) : (DateTime?)null;
                DateTime? endDate = !string.IsNullOrEmpty(end) ? Convert.ToDateTime(end) : (DateTime?)null;
                var commodityType = Convert.ToInt32(TestContext.DataRow["commodityType"].ToString());
                var uomid = Convert.ToInt32(TestContext.DataRow["uomid"].ToString());

                if (generateAmi)
                {
                    var mockAmiEnd = endDate ?? DateTime.UtcNow.Date;
                    var mockAmiStart = startDate ?? DateTime.UtcNow.Date;
                    var amiMock1 = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterid, servicePointId,
                        mockAmiStart, mockAmiEnd, mockAmiEnd.AddDays(10), commodityType, uomid);
                    var readingMock = TestHelpers.CreateMockAmiHourlyReading(mockAmiEnd, mockAmiEnd);
                    _tallAmi.Setup(a => a.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), meterid, accountId, clientId))
                        .Returns(amiMock1)
                        .Verifiable();
                    int testNoOfDays;
                    _tallAmi.Setup(
                        m => m.GetReadings(It.IsAny<IEnumerable<TallAmiModel>>(), out testNoOfDays, It.IsAny<bool>()))
                        .Returns(readingMock);
                }
                var authorizations = GetAuthorizations(encryptedAccessToken, clientSecretCode);

                var model = new GreenButtonIntervalBlockModel(clientId, _greenButtonDomain, authorizations, _config, _container, _insightRepository.Object);
                var result = model.GetIntervalBlocksGreenButton(encryptedAccessToken, subscriptionId, encrytedMeterid,
                    hexedReadingTypeId, intervalBlockId);

                Assert.IsNotNull(result);
                Assert.AreEqual(status, result.Status.ToString());
                if (status == "NoError")
                {
                    Assert.IsNotNull(result.GreenButtonXml);
                    if (generateAmi)
                        _tallAmi.Verify();
                }
                else
                    Assert.IsNull(result.GreenButtonXml);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }


        [TestMethod(), TestCategory("GreenButtonConnect"), TestCategory("BVT")]
        [DeploymentItem("TestData\\GetSerivceStatusTestData.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
"|DataDirectory|\\GetSerivceStatusTestData.xml", "ServiceStatus", DataAccessMethod.Sequential)]
        public void GetServiceStatusTest()
        {
            try
            {

                var clientId = Convert.ToInt32(TestContext.DataRow["clientId"].ToString());
                var clientSecretCode = TestContext.DataRow["clientSecretCode"].ToString();
                var encryptedAccessToken = GenerateAccessToken(clientSecretCode);
                var isRegistrationAccessToken =
                    Convert.ToBoolean(TestContext.DataRow["isRegistrationAccessToken"].ToString());
                var authorizations = GetAuthorizations(encryptedAccessToken, clientSecretCode);

                var model = new GreenButtonBaseModel(clientId, _greenButtonDomain, authorizations, _container,
                    _insightRepository.Object, _config);
                var result = model.GetSerivceStatus(encryptedAccessToken);

                Assert.IsNotNull(result);
                Assert.IsNotNull(result.GreenButtonXml);

                Assert.AreEqual(true,
                    isRegistrationAccessToken
                        ? result.GreenButtonXml.Contains("<currentStatus>1</currentStatus>")
                        : result.GreenButtonXml.Contains("<currentStatus>0</currentStatus>"));
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod(), TestCategory("GreenButtonConnect"), TestCategory("BVT")]
        [DeploymentItem("TestData\\GetApplicationInfoTestData.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
"|DataDirectory|\\GetApplicationInfoTestData.xml", "ApplicationInfo", DataAccessMethod.Sequential)]
        public void GetApplicationInfoTest()
        {
            try
            {

                var clientId = Convert.ToInt32(TestContext.DataRow["clientId"].ToString());
                var clientSecretCode = TestContext.DataRow["clientSecretCode"].ToString();
                var encryptedAccessToken = GenerateAccessToken(clientSecretCode);
                var status = TestContext.DataRow["status"].ToString();
                var applicationInfoId = Convert.ToInt64(TestContext.DataRow["applicationInfoId"].ToString());
                var authorizations = GetAuthorizations(encryptedAccessToken, clientSecretCode);

                var model = new GreenButtonBaseModel(clientId, _greenButtonDomain, authorizations, _container,
                    _insightRepository.Object, _config);
                var result = model.GetApplicationInfo(encryptedAccessToken, applicationInfoId);

                Assert.IsNotNull(result);
                Assert.AreEqual(status, result.Status.ToString());
                if (status == "NoError")
                {
                    Assert.IsNotNull(result.GreenButtonXml);
                }
                else
                    Assert.IsNull(result.GreenButtonXml);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod(), TestCategory("GreenButtonConnect"), TestCategory("BVT")]
        [DeploymentItem("TestData\\GetAuthorizationTestData.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
"|DataDirectory|\\GetAuthorizationTestData.xml", "Authorizations", DataAccessMethod.Sequential)]
        public void GetAllAuthorizationGreenButtonTest()
        {
            try
            {

                var clientId = Convert.ToInt32(TestContext.DataRow["clientId"].ToString());
                var clientSecretCode = TestContext.DataRow["clientSecretCode"].ToString();
                var encryptedAccessToken = GenerateAccessToken(clientSecretCode);
                var status = TestContext.DataRow["status"].ToString();
                var authorizations = GetAuthorizations(encryptedAccessToken, clientSecretCode);

                var model = new GreenButtonAuthorizationModel(clientId, _greenButtonDomain, authorizations, _config, _container, _insightRepository.Object);
                var result = model.GetAllAuthorizationGreenButton(encryptedAccessToken);

                Assert.IsNotNull(result);
                Assert.AreEqual(status, result.Status.ToString());
                if (status == "NoError")
                {
                    Assert.IsNotNull(result.GreenButtonXml);
                }
                else
                    Assert.IsNull(result.GreenButtonXml);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod(), TestCategory("GreenButtonConnect"), TestCategory("BVT")]
        [DeploymentItem("TestData\\GetAuthorizationTestData.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
"|DataDirectory|\\GetAuthorizationTestData.xml", "AuthorizationById", DataAccessMethod.Sequential)]
        public void GetAuthorizationGreenButtonTest()
        {
            try
            {

                var clientId = Convert.ToInt32(TestContext.DataRow["clientId"].ToString());
                var clientSecretCode = TestContext.DataRow["clientSecretCode"].ToString();
                var encryptedAccessToken = GenerateAccessToken(clientSecretCode);
                var status = TestContext.DataRow["status"].ToString();
                var id = Convert.ToInt64(TestContext.DataRow["AuthorizationId"].ToString());
                var authorizations = GetAuthorizations(encryptedAccessToken, clientSecretCode);

                var model = new GreenButtonAuthorizationModel(clientId, _greenButtonDomain, authorizations, _config, _container, _insightRepository.Object);
                var result = model.GetAuthorizationGreenButton(encryptedAccessToken, id);

                Assert.IsNotNull(result);
                Assert.AreEqual(status, result.Status.ToString());
                if (status == "NoError")
                {
                    Assert.IsNotNull(result.GreenButtonXml);
                }
                else
                    Assert.IsNull(result.GreenButtonXml);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        private void SetupAutoMapper()
        {
            Mapper.CreateMap<RateModel.Reading, ConsumptionData>()
                 .ForMember(dest => dest.DateTime, opts => opts.MapFrom(src => src.Timestamp))
                 .ForMember(dest => dest.Value,
                     opts =>
                         opts.MapFrom(
                             src => src.TimeOfUse == RateModel.Enums.TimeOfUse.Undefined ? (decimal)src.Quantity : 0))
                 .ForAllUnmappedMembers(o => o.Ignore());

            Mapper.CreateMap<RateModel.Reading, ConsumptionTouData>()
                .ForMember(dest => dest.Value, opts => opts.MapFrom(src => (decimal)src.Quantity))
                .ForMember(dest => dest.TouKey,
                    opts =>
                        opts.MapFrom(
                            src =>
                                src.TimeOfUse == RateModel.Enums.TimeOfUse.Undefined
                                    ? TimeOfUseType.NoTou.ToString().ToLower()
                                    : src.TimeOfUse.ToString().ToLower()))
                .ForAllUnmappedMembers(o => o.Ignore());


            Mapper.CreateMap<ConsumptionByMeterEntity, TallAmiModel>()
    .ForAllUnmappedMembers(o => o.Ignore());

            Mapper.CreateMap<TallAmiModel, ConsumptionByMeterEntity>()
                .ForAllUnmappedMembers(o => o.Ignore());
        }
        private GreenButtonScope GetMockScope(string accessToken, string clientSecretCode)
        {
            var insightsEntitesMock = new TestInsightsEntitiesMock();
            var insightRepository = new InsightsEfRepository(insightsEntitesMock.MockInsightsEntities.Object);

            var client = clientSecretCode.Split(',')[0];
            var secret = clientSecretCode.Split(',')[1];

            var authorizations = insightRepository.GetGreenButtonAuthorizations(client, secret);

            return authorizations.Find(a => a.AccessToken == accessToken)?.Scope;
        }

        private List<GreenButtonAuthorization> GetAuthorizations(string accessToken, string clientSecretCode)
        {
            var insightsEntitesMock = new TestInsightsEntitiesMock();
            var insightRepository = new InsightsEfRepository(insightsEntitesMock.MockInsightsEntities.Object);

            var client = clientSecretCode.Split(',')[0];
            var secret = clientSecretCode.Split(',')[1];

            var authorizations = insightRepository.GetGreenButtonAuthorizations(client, secret);

            var accessTokenType = authorizations.Find(a => a.AccessToken == accessToken)?.AccessTokenType;
            if (accessTokenType == GreenButtonAccessTokenType.Customer)
                return authorizations.FindAll(a => a.AccessToken == accessToken);
            return authorizations;
        }
        private string GenerateAccessToken(string plainText)
        {
            var enrypted = Helper.Encrypt(plainText);
            var plainTextBytes = Encoding.UTF8.GetBytes(enrypted);
            return Convert.ToBase64String(plainTextBytes);
        }
    }
}