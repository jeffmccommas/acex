﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using System;
using System.Collections.Generic;
using AO.BusinessContracts;
using AO.Registrar;
using AutoMapper;
using CE.AO.Business;
using CE.AO.Models;
using CE.Insights.Models;
using CE.Insights.Test.Helpers;
using CE.Models;
using CE.Models.Insights;
using Microsoft.Practices.Unity;
using Moq;

namespace CE.Insights.Test.Models
{
    [TestClass()]
    public class GreenButtonAmiModelTests
    {
        #region Constants for content
        private const string ClientSpace = "netnln2itvho";
        private const string ClientAccessToken = "c58ec8d495044174267c257f105914f7fb9567127a879b2419e1c3d405a54be2";

        #endregion

        private Mock<ITallAMI> _amiManager;
        private UnityContainer _container;
        private Mock<IInsightsEFRepository> _insightRepository;

        [TestInitialize]
        public void TestInit()
        {
            _amiManager = new Mock<ITallAMI>();
            _container = new UnityContainer();

            (new DataStorageRegistrar()).Initialize<TransientLifetimeManager>(_container);

            _container.RegisterInstance(_amiManager.Object);
            _insightRepository = new Mock<IInsightsEFRepository>();
        }

        [TestCleanup]
        public void TestClean()
        {
            //noop
        }

        [TestMethod()]
        public void GetAmiReadingsByInterval_Hour_Test()
        {

            Mapper.CreateMap<RateModel.Reading, ConsumptionData>()
               .ForMember(dest => dest.DateTime, opts => opts.MapFrom(src => src.Timestamp))
               .ForMember(dest => dest.Value,
                   opts =>
                       opts.MapFrom(
                           src => src.TimeOfUse == RateModel.Enums.TimeOfUse.Undefined ? (decimal)src.Quantity : 0))
               .ForAllUnmappedMembers(o => o.Ignore());

            Mapper.CreateMap<RateModel.Reading, ConsumptionTouData>()
                .ForMember(dest => dest.Value, opts => opts.MapFrom(src => (decimal)src.Quantity))
                .ForMember(dest => dest.TouKey,
                    opts =>
                        opts.MapFrom(
                            src =>
                                src.TimeOfUse == RateModel.Enums.TimeOfUse.Undefined
                                    ? "notou"
                                    : src.TimeOfUse.ToString().ToLower()))
                .ForAllUnmappedMembers(o => o.Ignore());
            var clientId = 87;
            var accountId = "GB001a1";
            var servicePointId = "SP_GB001a1";
            var meterId = "EM001";
            var startDate = Convert.ToDateTime("12/17/2015");
            var endDate = Convert.ToDateTime("12/22/2015");
            var projected = Convert.ToDateTime("1/17/2016");
            var commodityId = 1;
            var uomId = 0;
            int amiUomId = 99;
            int amiCommodityId = 0;
            var daysOfData = (endDate - startDate).TotalDays + 1;
            var numInterval = daysOfData * 24;
            // get config settings
            var config = TestGreenButtonHelper.GetConfigSetting();
            
            // mock ami hourly data
            var amiModel = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterId, servicePointId, startDate, endDate, projected);
            _amiManager.Setup(m => m.GetAmiList(startDate, endDate, meterId, accountId, clientId)).Returns(amiModel).Verifiable();

            int testNoOfDays;
            var reading = TestHelpers.CreateMockAmiHourlyReading(startDate, endDate);
            _amiManager.Setup(
                a =>
                    a.GetReadings(It.IsAny<IEnumerable<TallAmiModel>>(), out testNoOfDays, It.IsAny<bool>())).Returns(reading).Verifiable();


            var model = new GreenButtonAmiModel(clientId, config, _insightRepository.Object, _container);

            var result = model.GetAmiConsumptionReadingsByInterval(clientId, accountId, meterId, startDate, endDate, 60, ref amiUomId,
                ref amiCommodityId);

            Assert.IsNotNull(result);
            Assert.AreEqual(commodityId, amiCommodityId);
            Assert.AreEqual(uomId, amiUomId);
            Assert.AreEqual(numInterval, result.Count);

            var starttime = result[0].DateTime;
            var endtime = result[1].DateTime;
            var timeDiff = endtime.Subtract(starttime);
            Assert.AreEqual(1, timeDiff.Hours);
            _amiManager.Verify();
        }

        [TestMethod()]
        public void GetAmiReadingsByInterval_30_Test()
        {

            Mapper.CreateMap<RateModel.Reading, ConsumptionData>()
               .ForMember(dest => dest.DateTime, opts => opts.MapFrom(src => src.Timestamp))
               .ForMember(dest => dest.Value,
                   opts =>
                       opts.MapFrom(
                           src => src.TimeOfUse == RateModel.Enums.TimeOfUse.Undefined ? (decimal)src.Quantity : 0))
               .ForAllUnmappedMembers(o => o.Ignore());

            Mapper.CreateMap<RateModel.Reading, ConsumptionTouData>()
                .ForMember(dest => dest.Value, opts => opts.MapFrom(src => (decimal)src.Quantity))
                .ForMember(dest => dest.TouKey,
                    opts =>
                        opts.MapFrom(
                            src =>
                                src.TimeOfUse == RateModel.Enums.TimeOfUse.Undefined
                                    ? "notou"
                                    : src.TimeOfUse.ToString().ToLower()))
                .ForAllUnmappedMembers(o => o.Ignore());
            var clientId = 87;
            var accountId = "GB001a1";
            var servicePointId = "SP_GB001a1";
            var meterId = "EM001";
            var startDate = Convert.ToDateTime("12/17/2015");
            var endDate = Convert.ToDateTime("12/22/2015");
            var projected = Convert.ToDateTime("1/17/2016");
            var commodityId = 1;
            var uomId = 0;
            int amiUomId = 99;
            int amiCommodityId = 0;
            var daysOfData = (endDate - startDate).TotalDays + 1;
            var numInterval = daysOfData * 24 * 2;
            // get config settings
            var config = TestGreenButtonHelper.GetConfigSetting();
            
            // mock ami 30min data
            var amiModel = TestHelpers.CreateMockAmi30Model(clientId, accountId, meterId, servicePointId, startDate, endDate, projected);
            _amiManager.Setup(m => m.GetAmiList(startDate, endDate, meterId, accountId, clientId)).Returns(amiModel).Verifiable();
            
            int testNoOfDays;
            var reading = TestHelpers.CreateMockAmi30Reading(startDate, endDate);
            _amiManager.Setup(
                a =>
                    a.GetReadings(It.IsAny<IEnumerable<TallAmiModel>>(), out testNoOfDays, It.IsAny<bool>())).Returns(reading).Verifiable();



            var model = new GreenButtonAmiModel(clientId, config, _insightRepository.Object, _container);

            var result = model.GetAmiConsumptionReadingsByInterval(clientId, accountId, meterId, startDate, endDate, 30, ref amiUomId,
                ref amiCommodityId);

            Assert.IsNotNull(result);
            Assert.AreEqual(commodityId, amiCommodityId);
            Assert.AreEqual(uomId, amiUomId);
            Assert.AreEqual(numInterval, result.Count);

            var starttime = result[0].DateTime;
            var endtime = result[1].DateTime;
            var timeDiff = endtime.Subtract(starttime);
            Assert.AreEqual(30, timeDiff.Minutes);
            _amiManager.Verify();
        }
        [TestMethod()]
        public void GetAmiReadingsByInterval_15_Test()
        {

            Mapper.CreateMap<RateModel.Reading, ConsumptionData>()
               .ForMember(dest => dest.DateTime, opts => opts.MapFrom(src => src.Timestamp))
               .ForMember(dest => dest.Value,
                   opts =>
                       opts.MapFrom(
                           src => src.TimeOfUse == RateModel.Enums.TimeOfUse.Undefined ? (decimal)src.Quantity : 0))
               .ForAllUnmappedMembers(o => o.Ignore());

            Mapper.CreateMap<RateModel.Reading, ConsumptionTouData>()
                .ForMember(dest => dest.Value, opts => opts.MapFrom(src => (decimal)src.Quantity))
                .ForMember(dest => dest.TouKey,
                    opts =>
                        opts.MapFrom(
                            src =>
                                src.TimeOfUse == RateModel.Enums.TimeOfUse.Undefined
                                    ? "notou"
                                    : src.TimeOfUse.ToString().ToLower()))
                .ForAllUnmappedMembers(o => o.Ignore());
            var clientId = 87;
            var accountId = "GB001a1";
            var servicePointId = "SP_GB001a1";
            var meterId = "EM001";
            var startDate = Convert.ToDateTime("12/17/2015");
            var endDate = Convert.ToDateTime("12/22/2015");
            var projected = Convert.ToDateTime("1/17/2016");
            var commodityId = 1;
            var uomId = 0;
            int amiUomId = 99;
            int amiCommodityId = 0;
            var daysOfData = (endDate - startDate).TotalDays + 1;
            var numInterval = daysOfData * 24 * 4;
            // get config settings
            var config = TestGreenButtonHelper.GetConfigSetting();

            // mock ami 15min data
            var amiModel = TestHelpers.CreateMockAmi15AmiModel(clientId, accountId, meterId, servicePointId, startDate, endDate, projected);
            _amiManager.Setup(m => m.GetAmiList(startDate, endDate, meterId, accountId, clientId)).Returns(amiModel).Verifiable();

            int testNoOfDays;
            var reading = TestHelpers.CreateMockAmi15Reading(startDate, endDate);
            _amiManager.Setup(
                a =>
                    a.GetReadings(It.IsAny<IEnumerable<TallAmiModel>>(), out testNoOfDays, It.IsAny<bool>())).Returns(reading).Verifiable();


            var model = new GreenButtonAmiModel(clientId, config, _insightRepository.Object, _container);

            var result = model.GetAmiConsumptionReadingsByInterval(clientId, accountId, meterId, startDate, endDate, 15, ref amiUomId,
                ref amiCommodityId);

            Assert.IsNotNull(result);
            Assert.AreEqual(commodityId, amiCommodityId);
            Assert.AreEqual(uomId, amiUomId);
            Assert.AreEqual(numInterval, result.Count);

            var starttime = result[0].DateTime;
            var endtime = result[1].DateTime;
            var timeDiff = endtime.Subtract(starttime);
            Assert.AreEqual(15, timeDiff.Minutes);
            _amiManager.Verify();
        }


        [TestMethod()]
        public void GetConsumptionDataList_AllInterval_Test()
        {
            var clientId = 87;
            var accountId = "BTD001a1";
            var servicePointId = "SP_BTD001a1";
            var meterId = "EM001";
            var start15Date = Convert.ToDateTime("12/17/2015");
            var end15Date = Convert.ToDateTime("12/18/2015");
            var start30Date = Convert.ToDateTime("12/19/2015");
            var end30Date = Convert.ToDateTime("12/20/2015");
            var start60Date = Convert.ToDateTime("12/21/2015");
            var end60Date = Convert.ToDateTime("12/22/2015");
            var projected = Convert.ToDateTime("1/17/2016");
            var commodityId = 1;
            var uomId = 0;
            int amiUomId;
            int amiCommodityId;
            double numInterval = 0;
            // get config settings
            var config = TestGreenButtonHelper.GetConfigSetting();

            #region "test data"
            // mock ami 15min data
            var ami15Model = TestHelpers.CreateMockAmi15AmiModel(clientId, accountId, meterId, servicePointId, start15Date, end15Date, projected);
            var reading15 = TestHelpers.CreateMockAmi15Reading(start15Date, end15Date);
            // mock ami 30min data
            var ami30Model = TestHelpers.CreateMockAmi30Model(clientId, accountId, meterId, servicePointId, start30Date, end30Date, projected);
            var reading30 = TestHelpers.CreateMockAmi30Reading(start30Date, end30Date);
            // mock ami hourly data
            var ami60Model = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterId, servicePointId, start60Date, end60Date, projected);
            var reading60 = TestHelpers.CreateMockAmiHourlyReading(start60Date, end60Date);

            _amiManager.SetupSequence(m => m.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), meterId, accountId, clientId))
                .Returns(ami15Model)
                .Returns(ami30Model)
                .Returns(ami60Model);

            int testNoOfDays;
            _amiManager.SetupSequence(
                a =>
                    a.GetReadings(It.IsAny<IEnumerable<TallAmiModel>>(), out testNoOfDays, It.IsAny<bool>()))
                .Returns(reading15)
                .Returns(reading30)
                .Returns(reading60);

            #endregion

            var model = new GreenButtonAmiModel(clientId, config, _insightRepository.Object, _container);

            var result = model.GetConsumptionDataList(clientId, accountId, meterId, start15Date, end60Date, out amiUomId,
                out amiCommodityId);

            Assert.IsNotNull(result);
            Assert.AreEqual(commodityId, amiCommodityId);
            Assert.AreEqual(uomId, amiUomId);
            Assert.AreEqual(3, result.Count);
            for (int i = 0; i < 3; i++)
            {
                Assert.IsNotNull(result[i]);
                var starttime = result[i].ConsumptionDataList[0].DateTime;
                var endtime = result[i].ConsumptionDataList[1].DateTime;
                var timeDiff = endtime.Subtract(starttime);
                if (result[i].ResolutionKey == "hour")
                {
                    var daysOfData = (end60Date - start60Date).TotalDays + 1;
                    numInterval = daysOfData * 24;
                    Assert.AreEqual(1, timeDiff.Hours);
                }
                else if (result[i].ResolutionKey == "fifteen")
                {
                    var daysOfData = (end15Date - start15Date).TotalDays + 1;
                    numInterval = daysOfData * 24 * 4;
                    Assert.AreEqual(15, timeDiff.Minutes);
                }
                else if (result[i].ResolutionKey == "thirty")
                {
                    var daysOfData = (end30Date - start30Date).TotalDays + 1;
                    numInterval = daysOfData * 24 * 2;
                    Assert.AreEqual(30, timeDiff.Minutes);
                }
                Assert.AreEqual(numInterval, result[i].ConsumptionDataList.Count);
            }
        }

        [TestMethod()]
        public void GetConsumptionDataList_TwoInterval_Test()
        {
            var clientId = 87;
            var accountId = "BTD001a1";
            var servicePointId = "SP_BTD001a1";
            var meterId = "EM001";
            var start15Date = Convert.ToDateTime("12/17/2015");
            var end15Date = Convert.ToDateTime("12/18/2015");
            var start60Date = Convert.ToDateTime("12/19/2015");
            var end60Date = Convert.ToDateTime("12/22/2015");
            var projected = Convert.ToDateTime("1/17/2016");
            var commodityId = 1;
            var uomId = 0;
            int amiUomId;
            int amiCommodityId;
            double numInterval = 0;
            // get config settings
            var config = TestGreenButtonHelper.GetConfigSetting();

            #region "test data"
            // mock ami 15min data
            var ami15Model = TestHelpers.CreateMockAmi15AmiModel(clientId, accountId, meterId, servicePointId, start15Date, end15Date, projected);
            var reading15 = TestHelpers.CreateMockAmi15Reading(start15Date, end15Date);
            // mock ami hourly data
            var ami60Model = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterId, servicePointId, start60Date, end60Date, projected);
            var reading60 = TestHelpers.CreateMockAmiHourlyReading(start60Date, end60Date);

            _amiManager.SetupSequence(m => m.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), meterId, accountId, clientId))
                .Returns(ami15Model)
                .Returns(null)
                .Returns(ami60Model);

            int testNoOfDays;
            _amiManager.SetupSequence(
                a =>
                    a.GetReadings(It.IsAny<IEnumerable<TallAmiModel>>(), out testNoOfDays, It.IsAny<bool>()))
                .Returns(reading15)
                .Returns(reading60);
            #endregion

            var model = new GreenButtonAmiModel(clientId, config, _insightRepository.Object, _container);

            var result = model.GetConsumptionDataList(clientId, accountId, meterId, start15Date, end60Date, out amiUomId,
                out amiCommodityId);

            Assert.IsNotNull(result);
            Assert.AreEqual(commodityId, amiCommodityId);
            Assert.AreEqual(uomId, amiUomId);
            Assert.AreEqual(2, result.Count);
            for (int i = 0; i < 2; i++)
            {
                Assert.IsNotNull(result[i]);
                var starttime = result[i].ConsumptionDataList[0].DateTime;
                var endtime = result[i].ConsumptionDataList[1].DateTime;
                var timeDiff = endtime.Subtract(starttime);
                if (result[i].ResolutionKey == "hour")
                {
                    var daysOfData = (end60Date - start60Date).TotalDays + 1;
                    numInterval = daysOfData * 24;
                    Assert.AreEqual(1, timeDiff.Hours);
                }
                else if (result[i].ResolutionKey == "fifteen")
                {
                    var daysOfData = (end15Date - start15Date).TotalDays + 1;
                    numInterval = daysOfData * 24 * 4;
                    Assert.AreEqual(15, timeDiff.Minutes);
                }
                Assert.AreEqual(numInterval, result[i].ConsumptionDataList.Count);
            }
        }

        [TestMethod]
        public void GetGreenButtonAmi_AllInterval_Xml_Test()
        {
            var clientId = 87;
            var customerId = "GB001";
            var accountId = "GB001a1";
            var premiseId = "GB01a1p1";
            var servicePointId = "SP_GB001a1";
            var start15Date = Convert.ToDateTime("12/17/2015");
            var end15Date = Convert.ToDateTime("12/18/2015");
            var start30Date = Convert.ToDateTime("12/19/2015");
            var end30Date = Convert.ToDateTime("12/20/2015");
            var start60Date = Convert.ToDateTime("12/21/2015");
            var end60Date = Convert.ToDateTime("12/22/2015");
            var projected = Convert.ToDateTime("1/17/2016");
            var meterId = "EM001";


            //add property for client user, normally from the stored procedure but hardcoded here for unit test purposes
            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };


            var config = TestGreenButtonHelper.GetConfigSetting();


            #region "test data"
            // mock ami 15min data
            var ami15Model = TestHelpers.CreateMockAmi15AmiModel(clientId, accountId, meterId, servicePointId, start15Date, end15Date, projected);
            var reading15 = TestHelpers.CreateMockAmi15Reading(start15Date, end15Date);
            // mock ami 30min data
            var ami30Model = TestHelpers.CreateMockAmi30Model(clientId, accountId, meterId, servicePointId, start30Date, end30Date, projected);
            var reading30 = TestHelpers.CreateMockAmi30Reading(start30Date, end30Date);
            // mock ami hourly data
            var ami60Model = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterId, servicePointId, start60Date, end60Date, projected);
            var reading60 = TestHelpers.CreateMockAmiHourlyReading(start60Date, end60Date);

            _amiManager.SetupSequence(m => m.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), meterId, accountId, clientId))
                .Returns(ami15Model)
                .Returns(ami30Model)
                .Returns(ami60Model);

            int testNoOfDays;
            _amiManager.SetupSequence(
                a =>
                    a.GetReadings(It.IsAny<IEnumerable<TallAmiModel>>(), out testNoOfDays, It.IsAny<bool>()))
                .Returns(reading15)
                .Returns(reading30)
                .Returns(reading60);
            #endregion

            var model = new GreenButtonAmiModel(clientId, config, _insightRepository.Object, _container);

            var request = new GreenButtonAmiRequest
            {
                CustomerId = customerId,
                AccountId = accountId,
                PremiseId = premiseId,
                MeterIds = meterId,
                StartDate = start15Date,
                EndDate = end60Date,
                OutputFormat = "Xml"
            };

            var result = model.GetGreenButtonAmi(clientUser, request);
            Assert.IsNotNull(result);
            Assert.IsNotNull("xml", result.FileType.ToLower());
            Assert.IsNull(result.File);
            Assert.IsNotNull(result.FileXml);
        }

        [TestMethod]
        public void GetGreenButtonAmi_AllInterval_InvalidOutputFormatXml_Test()
        {
            var clientId = 87;
            var customerId = "GB001";
            var accountId = "GB001a1";
            var premiseId = "GB01a1p1";
            var servicePointId = "SP_GB001a1";
            var start15Date = Convert.ToDateTime("12/17/2015");
            var end15Date = Convert.ToDateTime("12/18/2015");
            var start30Date = Convert.ToDateTime("12/19/2015");
            var end30Date = Convert.ToDateTime("12/20/2015");
            var start60Date = Convert.ToDateTime("12/21/2015");
            var end60Date = Convert.ToDateTime("12/22/2015");
            var projected = Convert.ToDateTime("1/17/2016");
            var meterId = "EM001";


            //add property for client user, normally from the stored procedure but hardcoded here for unit test purposes
            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };


            var config = TestGreenButtonHelper.GetConfigSetting();


            #region "test data"
            // mock ami 15min data
            var ami15Model = TestHelpers.CreateMockAmi15AmiModel(clientId, accountId, meterId, servicePointId, start15Date, end15Date, projected);
            var reading15 = TestHelpers.CreateMockAmi15Reading(start15Date, end15Date);
            // mock ami 30min data
            var ami30Model = TestHelpers.CreateMockAmi30Model(clientId, accountId, meterId, servicePointId, start30Date, end30Date, projected);
            var reading30 = TestHelpers.CreateMockAmi30Reading(start30Date, end30Date);
            // mock ami hourly data
            var ami60Model = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterId, servicePointId, start60Date, end60Date, projected);
            var reading60 = TestHelpers.CreateMockAmiHourlyReading(start60Date, end60Date);

            _amiManager.SetupSequence(m => m.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), meterId, accountId, clientId))
                .Returns(ami15Model)
                .Returns(ami30Model)
                .Returns(ami60Model);

            int testNoOfDays;
            _amiManager.SetupSequence(
                a =>
                    a.GetReadings(It.IsAny<IEnumerable<TallAmiModel>>(), out testNoOfDays, It.IsAny<bool>()))
                .Returns(reading15)
                .Returns(reading30)
                .Returns(reading60);
            #endregion

            var model = new GreenButtonAmiModel(clientId, config, _insightRepository.Object, _container);

            var request = new GreenButtonAmiRequest
            {
                CustomerId = customerId,
                AccountId = accountId,
                PremiseId = premiseId,
                MeterIds = meterId + "," + meterId,
                StartDate = start15Date,
                EndDate = end60Date,
                OutputFormat = "Xml"
            };

            var result = model.GetGreenButtonAmi(clientUser, request);
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.ErrorMessage);
        }

        [TestMethod]
        public void GetGreenButtonAmi_AllInterval_Zip_Test()
        {
            var clientId = 87;
            var customerId = "GB001";
            var accountId = "GB001a1";
            var premiseId = "GB01a1p1";
            var servicePointId = "SP_GB001a1";
            var start15Date = Convert.ToDateTime("12/17/2015");
            var end15Date = Convert.ToDateTime("12/18/2015");
            var start30Date = Convert.ToDateTime("12/19/2015");
            var end30Date = Convert.ToDateTime("12/20/2015");
            var start60Date = Convert.ToDateTime("12/21/2015");
            var end60Date = Convert.ToDateTime("12/22/2015");
            var projected = Convert.ToDateTime("1/17/2016");
            var meterId = "EM001";


            //add property for client user, normally from the stored procedure but hardcoded here for unit test purposes
            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };


            var config = TestGreenButtonHelper.GetConfigSetting();


            #region "test data"
            // mock ami 15min data
            var ami15Model = TestHelpers.CreateMockAmi15AmiModel(clientId, accountId, meterId, servicePointId, start15Date, end15Date, projected);
            var reading15 = TestHelpers.CreateMockAmi15Reading(start15Date, end15Date);
            // mock ami 30min data
            var ami30Model = TestHelpers.CreateMockAmi30Model(clientId, accountId, meterId, servicePointId, start30Date, end30Date, projected);
            var reading30 = TestHelpers.CreateMockAmi30Reading(start30Date, end30Date);
            // mock ami hourly data
            var ami60Model = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterId, servicePointId, start60Date, end60Date, projected);
            var reading60 = TestHelpers.CreateMockAmiHourlyReading(start60Date, end60Date);

            _amiManager.SetupSequence(m => m.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), meterId, accountId, clientId))
                .Returns(ami15Model)
                .Returns(ami30Model)
                .Returns(ami60Model);

            int testNoOfDays;
            _amiManager.SetupSequence(
                a =>
                    a.GetReadings(It.IsAny<IEnumerable<TallAmiModel>>(), out testNoOfDays, It.IsAny<bool>()))
                .Returns(reading15)
                .Returns(reading30)
                .Returns(reading60);
            #endregion

            var model = new GreenButtonAmiModel(clientId, config, _insightRepository.Object, _container);

            var request = new GreenButtonAmiRequest
            {
                CustomerId = customerId,
                AccountId = accountId,
                PremiseId = premiseId,
                MeterIds = meterId,
                StartDate = start15Date,
                EndDate = end60Date,
                OutputFormat = "Zip"
            };

            var result = model.GetGreenButtonAmi(clientUser, request);
            Assert.IsNotNull(result);
            Assert.IsNotNull("zip", result.FileType.ToLower());
            Assert.IsNotNull(result.File);

            ////comment out the following, so the build agent will work
            //using (
            //    var fileStream = new FileStream("C:\\" + result.FileName + ".zip",
            //        FileMode.Create))
            //{
            //    result.File.Seek(0, SeekOrigin.Begin);
            //    result.File.CopyTo(fileStream);
            //}
            //Assert.IsTrue(File.Exists("C:\\" + result.FileName + ".zip"));

            //File.Delete("C:\\" + result.FileName + ".zip");
        }

        [TestMethod]
        public void GetGreenButtonAmi_NoData_Test()
        {
            var clientId = 87;
            var customerId = "GB001";
            var accountId = "GB001a1";
            var premiseId = "GB01a1p1";
            var startDate = Convert.ToDateTime("12/17/2015");
            var endDate = Convert.ToDateTime("12/22/2015");
            var meterId = "EM001";


            //add property for client user, normally from the stored procedure but hardcoded here for unit test purposes
            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };


            var config = TestGreenButtonHelper.GetConfigSetting();


            var model = new GreenButtonAmiModel(clientId, config, _insightRepository.Object, _container);

            var request = new GreenButtonAmiRequest
            {
                CustomerId = customerId,
                AccountId = accountId,
                PremiseId = premiseId,
                MeterIds = meterId,
                StartDate = startDate,
                EndDate = endDate
            };

            var result = model.GetGreenButtonAmi(clientUser, request);
            Assert.IsNull(result);
        }

    }
}