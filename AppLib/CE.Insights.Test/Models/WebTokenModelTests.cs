﻿using CE.Models.Insights;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.Insights.Test.Models
{
    [TestClass()]
    public class WebTokenModelTests
    {
        int _clientId = 227;
        CE.Models.ClientUser _clientUser;
        CE.Insights.Models.WebTokenModel _sut;
        Mock<CE.Models.Insights.DataAccess.DataAccessLayer> _mockDataAccessLayer;

        string _customerId = "X_TESTCustomerId";
        string _accountId = "X_TESTAccountId";
        string _premiseId = "X_TESTPremiseId";
        //string _customerId;
        //string _accountId;
        //string _premiseId;

        [TestInitialize]
        public void TestInit()
        {
           
            _clientUser = new CE.Models.ClientUser
            {
                ClientID = 227
            };

            _mockDataAccessLayer = new Mock<CE.Models.Insights.DataAccess.DataAccessLayer>();
            _mockDataAccessLayer.Setup(o => o.InsertCustomerWebTokenInfo(227, "", "", "", DateTime.Now, "", null, null));

            _mockDataAccessLayer.Setup(o => o.GetAnonymousUser(227, "test2@test.com", "90222-4523", out _customerId,out _accountId,out _premiseId)).Returns(true);


            _sut = new Insights.Models.WebTokenModel(_mockDataAccessLayer.Object);
        }

        [TestMethod()]
        public void CreateWebToken_With_TestAccount_Test()
        {
            WebTokenPostResponse mockWebTokenPostResponse=new WebTokenPostResponse();
            var mockWebTokenPostRequest = new WebTokenPostRequest
            {
                AdditionalInfo = "",
                CustomerId = "",
                EmailAddress = "test@test.com",
                ZipCode = "90221-4523",
                GroupName = "",
                RoleName = "",
                UserId = ""

            };

            _sut.CreateWebToken(_clientUser, mockWebTokenPostRequest, mockWebTokenPostResponse);

            Assert.AreEqual("X_131333",mockWebTokenPostResponse.CustomerId);
        }

        [TestMethod()]
        public void CreateWebToken_Null_EMAIL_ZIP_Test()
        {
            WebTokenPostResponse mockWebTokenPostResponse = new WebTokenPostResponse();
            var mockWebTokenPostRequest = new WebTokenPostRequest
            {
                AdditionalInfo = "",
                CustomerId = "",
                EmailAddress = "",
                ZipCode = "",
                GroupName = "",
                RoleName = "",
                UserId = ""

            };

            _sut.CreateWebToken(_clientUser, mockWebTokenPostRequest, mockWebTokenPostResponse);

            Assert.AreEqual("EitherEmailCustomerId", mockWebTokenPostResponse.Status);
        }

        [TestMethod()]
        public void CreateWebToken_ValidEmail_ValidZipCode_Test()
        {
            WebTokenPostResponse mockWebTokenPostResponse = new WebTokenPostResponse();
            var mockWebTokenPostRequest = new WebTokenPostRequest
            {
                AdditionalInfo = "",
                CustomerId = "",
                EmailAddress = "test2@test.com",
                ZipCode = "90222-4523",
                GroupName = "",
                RoleName = "",
                UserId = ""

            };

            _sut.CreateWebToken(_clientUser, mockWebTokenPostRequest, mockWebTokenPostResponse);

            Assert.AreEqual("X_TESTCustomerId", mockWebTokenPostResponse.CustomerId);
            Assert.AreEqual("X_TESTAccountId", mockWebTokenPostResponse.AccountId);
            Assert.AreEqual("X_TESTPremiseId", mockWebTokenPostResponse.PremiseId);
            Assert.AreEqual("X_TESTCustomerId", mockWebTokenPostResponse.ServicePointId);

        }
    }
}
