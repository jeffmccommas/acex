﻿using AO.Registrar;
using CE.AO.Models;
using CE.Insights.Models;
using CE.Insights.Test.Helpers;
using CE.Models;
using CE.Models.Insights;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using AO.BusinessContracts;
using CE.BillToDate;
using UnitOfMeasureType = CE.BillToDate.UnitOfMeasureType;

namespace CE.Insights.Test.Models
{
    [TestClass]
    public class BillToDateModelTests
    {

        #region Constants for content
        private const string ClientSpace = "netnln2itvho";
        private const string ClientAccessToken = "c58ec8d495044174267c257f105914f7fb9567127a879b2419e1c3d405a54be2";

        #endregion

        private Mock<ITallAMI> _tallAmi;
        private UnityContainer _container;
        private Mock<IBilling> _billing;
        private Mock<IBillingCycleSchedule> _billingCycleSchedule;
        private Mock<ICalculation> _calculation;
        private Mock<IClientConfigFacade> _clientConfigFacade;
        private Mock<IBillToDate> _billToDate;

        [TestInitialize]
        public void TestInit()
        {
            _tallAmi = new Mock<ITallAMI>();
            _billing = new Mock<IBilling>();
            _billingCycleSchedule = new Mock<IBillingCycleSchedule>();
            _calculation = new Mock<ICalculation>();
            _clientConfigFacade = new Mock<IClientConfigFacade>();
            _billToDate = new Mock<IBillToDate>();
            _container = new UnityContainer();

            (new DataStorageRegistrar()).Initialize<TransientLifetimeManager>(_container);

            _container.RegisterInstance(_billingCycleSchedule.Object);
            _container.RegisterInstance(_tallAmi.Object);
            _container.RegisterInstance(_billing.Object);
            _container.RegisterInstance(_calculation.Object);
            _container.RegisterInstance(_clientConfigFacade.Object);
            
        }

        [TestCleanup]
        public void TestClean()
        {
            //noop
        }

        [TestMethod, TestCategory("BilltoDateController"), TestCategory("BVT")]
        public void BillToDateV1_GetAmiReadingsTest()
        {
            var model = new BillToDateModel(_container);

            const int clientId = 87;
            const string accountId = "BTD001a1";
            const string servicePointId = "SP_BTD001a1";
            const string meterId = "EM001";

            var startDate = Convert.ToDateTime("12/17/2015");
            var endDate = Convert.ToDateTime("12/22/2015");
            var projected = Convert.ToDateTime("1/17/2016");

            var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
            var endDateTime = TimeZoneInfo.ConvertTimeToUtc(new DateTime(endDate.Year, endDate.Month, endDate.Day, 23, 59, 59), timeZoneInfo);

            int noOfDays;
            int amiUomId;
            

            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();

            var amiModel = TestHelpers.CreateMockAmi15AmiModel(clientId, accountId, meterId, servicePointId, startDate, endDate, projected);
            _tallAmi.Setup(m => m.GetAmiList(startDate.ToUniversalTime(), endDateTime, meterId, accountId, clientId)).Returns(amiModel).Verifiable();
            var reading = TestHelpers.CreateMockAmi15Reading(startDate, endDate);
            int testNoOfDays;
            _tallAmi.Setup(m => m.GetReadings(It.IsAny<IEnumerable<TallAmiModel>>(), out testNoOfDays, It.IsAny<bool>()))
                .Returns(reading)
                .Verifiable();
            var result = model.GetAmiReadings(clientId, accountId, meterId, startDate, endDate, out noOfDays, out amiUomId);

            Assert.IsNotNull(result);
            Assert.AreEqual(0, amiUomId);
            Assert.AreEqual(startDate, result.LastOrDefault()?.Timestamp);
            _tallAmi.Verify();

        }

        [TestMethod, TestCategory("BilltoDateController"), TestCategory("BVT"), TestCategory("BVT")]
        public void BillToDateV1_GetMeteredSericeList_OldBill_Test1()
        {
            const int clientId = 87;

            var model = new BillToDateModel(_container, clientId);
            var mockBillingModel = CreateOldBillingModelsData();

            DateTime billDate;
            var result = model.GetMeteredSericeList(mockBillingModel, clientId, out billDate);

            Assert.IsNotNull(result);
            Assert.AreEqual(0, result.Count);
        }

        [TestMethod, TestCategory("BilltoDateController")]
        public void BillToDateV1_GetMeteredSericeList_SewerWater_Test1()
        {
            int clientId = 87;
            string customerId = "BTD001";
            string accountId = "BTD001a1";
            string premiseId = "BTD001a1p1";
            string billCycleScheduleId = "Cycle01";
            string meterId = "WM001";
            string meterType = "AMI";
            string sewerRateClass = "W-RES-1";
            string rateClass = "W-RES";
            string servicePointIdSewer = "SP_BTD001a1p1s1_4";
            string servicePointIdWater = "SP_BTD001a1p1s1_3";
            string serviceId = "BTD001a1p1s1";
            int uomId = 6;

            var today = DateTime.Today.Date;
            today = new DateTime(today.Year, today.Month, today.Day, 0, 0, 0, DateTimeKind.Unspecified);
            var currentStart = today.AddDays(-6);
            var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            var mockBtdSettings = TestHelpers.CreateMockBillToDateSettings(clientId);
            var model = new BillToDateModel(_container, mockBtdSettings, null);
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();
            // mock billing data
            var mockModel = new List<BillingModel>();
            var mockSewerModel = CreateBillingModelsData(clientId, customerId, accountId, premiseId, serviceId,
                servicePointIdSewer, meterId, billCycleScheduleId, 4, meterType, sewerRateClass, uomId, lastBillStartDate, lastBillEndDate);
            var mockWaterModel = CreateBillingModelsData(clientId, customerId, accountId, premiseId, serviceId,
                servicePointIdWater, meterId, billCycleScheduleId, 3, meterType, rateClass, uomId, lastBillStartDate, lastBillEndDate);
            mockModel.Add(mockSewerModel);
            mockModel.Add(mockWaterModel);

            // mock bill cycle data
            var billCycle = CreateMockBillCycleScheduleModel(currentStart, currentEnd, billCycleScheduleId, clientId);
            _billingCycleSchedule.Setup(
                m => m.GetBillingCycleScheduleByDateAsync(clientId, billCycleScheduleId, today))
                .Returns(billCycle)
                .Verifiable();

            // mock ami data
            var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
            var endDateTime = TimeZoneInfo.ConvertTimeToUtc(new DateTime(today.Year, today.Month, today.Day, 23, 59, 59), timeZoneInfo);

            var amiModel = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterId, servicePointIdWater, currentStart, today.AddDays(-1), currentEnd);
            _tallAmi.Setup(m => m.GetAmiList(currentStart.ToUniversalTime(), endDateTime, meterId, accountId, clientId)).Returns(amiModel).Verifiable();
            var reading = TestHelpers.CreateMockAmiHourlyReading(currentStart, today);
            // ReSharper disable once RedundantAssignment
            var testNoOfDays = 1;
            _tallAmi.Setup(m => m.GetReadings(It.IsAny<IEnumerable<TallAmiModel>>(), out testNoOfDays, It.IsAny<bool>()))
                .Returns(reading)
                .Verifiable();


            DateTime billDate;
            var result = model.GetMeteredSericeList(mockModel, clientId, out billDate);

            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);
            var lastReadindex = result[0].Meters[0].Readings.Count - 1;
            Assert.AreEqual(currentStart, result[0].Meters[0].Readings[lastReadindex].Timestamp);
            Assert.AreEqual(1, result[0].Meters.Count);
            Assert.AreEqual(rateClass, result[0].Meters[0].RateClass);
            Assert.AreEqual(sewerRateClass, result[0].Meters[0].RateClass2);
            _billingCycleSchedule.Verify();
            _tallAmi.Verify();
        }

        [TestMethod, TestCategory("BilltoDateController"), TestCategory("BVT")]
        public void BillToDateV1_GetMeteredSericeList_SewerWater_Test2()
        {
            const int clientId = 87;
            const string customerId = "BTD001";
            const string accountId = "BTD001a1";
            const string premiseId = "BTD001a1p1";
            const string billCycleScheduleId = "Cycle01";
            const string meterId = "WM001";
            const string meterType = "AMI";
            const string sewerRateClass = "W-RES-1";
            const string rateClass = "W-RES";
            const string servicePointIdSewer = "SP_BTD001a1p1s1_4";
            const string servicePointIdWater = "SP_BTD001a1p1s1_3";
            const string serviceId = "BTD001a1p1s1";
            const int uomId = 6;

            var today = DateTime.Today.Date;
            today = new DateTime(today.Year, today.Month, today.Day, 0, 0, 0, DateTimeKind.Unspecified);
            var currentStart = today.AddDays(-6);
            var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            var mockBtdSettings = TestHelpers.CreateMockBillToDateSettings(clientId);
            var model = new BillToDateModel(_container, mockBtdSettings, null);
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();
            // mock billing data
            var mockModel = new List<BillingModel>();
            var mockSewerModel = CreateBillingModelsData(clientId, customerId, accountId, premiseId, serviceId,
                servicePointIdSewer, meterId, billCycleScheduleId, 4, meterType, sewerRateClass, uomId, lastBillStartDate, lastBillEndDate);
            var mockWaterModel = CreateBillingModelsData(clientId, customerId, accountId, premiseId, serviceId,
                servicePointIdWater, meterId, billCycleScheduleId, 3, meterType, rateClass, uomId, lastBillStartDate, lastBillEndDate);
            mockModel.Add(mockWaterModel);
            mockModel.Add(mockSewerModel);

            // mock bill cycle data
            var billCycle = CreateMockBillCycleScheduleModel(currentStart, currentEnd, billCycleScheduleId, clientId);
            _billingCycleSchedule.Setup(
                m => m.GetBillingCycleScheduleByDateAsync(clientId, billCycleScheduleId, today))
                .Returns(billCycle)
                .Verifiable();

            // mock ami data
            var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
            var endDateTime = TimeZoneInfo.ConvertTimeToUtc(new DateTime(today.Year, today.Month, today.Day, 23, 59, 59), timeZoneInfo);
            var amiModel = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterId, servicePointIdWater, currentStart, today.AddDays(-1), currentEnd);
            _tallAmi.Setup(m => m.GetAmiList(currentStart.ToUniversalTime(), endDateTime, meterId, accountId, clientId)).Returns(amiModel).Verifiable();
            var reading = TestHelpers.CreateMockAmiHourlyReading(currentStart, today);
            // ReSharper disable once RedundantAssignment
            int testNoOfDays = 1;
            _tallAmi.Setup(m => m.GetReadings(It.IsAny<IEnumerable<TallAmiModel>>(), out testNoOfDays, It.IsAny<bool>()))
                .Returns(reading)
                .Verifiable();

            DateTime billDate;
            var result = model.GetMeteredSericeList(mockModel, clientId, out billDate);

            Assert.IsNotNull(result);
            var lastReadingIndex = result[0].Meters[0].Readings.Count - 1;
            Assert.AreEqual(currentStart, result[0].Meters[0].Readings[lastReadingIndex].Timestamp);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(1, result[0].Meters.Count);
            Assert.AreEqual(rateClass, result[0].Meters[0].RateClass);
            Assert.AreEqual(sewerRateClass, result[0].Meters[0].RateClass2);
            _tallAmi.Verify();
            _billingCycleSchedule.Verify();
        }

        [TestMethod, TestCategory("BilltoDateController"), TestCategory("BVT")]
        public void BillToDateV1_GetMeteredSericeList_SewerWater_Test3()
        {
            const int clientId = 87;
            const string customerId = "BTD001";
            const string accountId = "BTD001a1";
            const string premiseId = "BTD001a1p1";
            const string billCycleScheduleId = "Cycle01";
            const string meterId = "WM001";
            const string elecMeterId = "EM001";
            const string meterType = "AMI";
            const string sewerRateClass = "W-RES-1";
            const string rateClass = "W-RES";
            const string elecRateClass = "RG-1";
            const string servicePointIdSewer = "SP_BTD001a1p1s1_4";
            const string servicePointIdWater = "SP_BTD001a1p1s1_3";
            const string servicePointIdElectric = "SP_BTD001a1p1s1_1";
            const string serviceWaterSewerId = "BTD001a1p1s1";
            const string serivceElectId = "BTD001a1p1s2";
            const int uomId = 6;

            var today = DateTime.Today.Date;
            today = new DateTime(today.Year, today.Month, today.Day, 0, 0, 0, DateTimeKind.Unspecified);
            var currentStart = today.AddDays(-6);
            var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            var mockBtdSettings = TestHelpers.CreateMockBillToDateSettings(clientId);
            var model = new BillToDateModel(_container, mockBtdSettings, null);
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();
            // mock billing data
            var mockModel = new List<BillingModel>();
            var mockElecModel = CreateBillingModelsData(clientId, customerId, accountId, premiseId, serivceElectId,
                servicePointIdElectric, elecMeterId, billCycleScheduleId, 1, meterType, elecRateClass, 0, lastBillStartDate, lastBillEndDate);
            var mockSewerModel = CreateBillingModelsData(clientId, customerId, accountId, premiseId, serviceWaterSewerId,
                servicePointIdSewer, meterId, billCycleScheduleId, 4, meterType, sewerRateClass, uomId, lastBillStartDate, lastBillEndDate);
            var mockWaterModel = CreateBillingModelsData(clientId, customerId, accountId, premiseId, serviceWaterSewerId,
                servicePointIdWater, meterId, billCycleScheduleId, 3, meterType, rateClass, uomId, lastBillStartDate, lastBillEndDate);
            mockModel.Add(mockElecModel);
            mockModel.Add(mockWaterModel);
            mockModel.Add(mockSewerModel);

            // mock bill cycle data
            var billCycle = CreateMockBillCycleScheduleModel(currentStart, currentEnd, billCycleScheduleId, clientId);
            _billingCycleSchedule.Setup(
                m => m.GetBillingCycleScheduleByDateAsync(clientId, billCycleScheduleId, today))
                .Returns(billCycle)
                .Verifiable();

            // mock ami data
            var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
            var endDateTime = TimeZoneInfo.ConvertTimeToUtc(new DateTime(today.Year, today.Month, today.Day, 23, 59, 59), timeZoneInfo);
            var amiModel = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterId, servicePointIdWater, currentStart, today.AddDays(-1), currentEnd);
            var elecAmiModel = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, elecMeterId, servicePointIdElectric, currentStart, today.AddDays(-1), currentEnd);
            _tallAmi.Setup(m => m.GetAmiList(currentStart.ToUniversalTime(), endDateTime, meterId, accountId, clientId)).Returns(amiModel).Verifiable();
            _tallAmi.Setup(m => m.GetAmiList(currentStart.ToUniversalTime(), endDateTime, elecMeterId, accountId, clientId)).Returns(elecAmiModel).Verifiable();
            var reading = TestHelpers.CreateMockAmiHourlyReading(currentStart, today);
            // ReSharper disable once RedundantAssignment
            int testNoOfDays = 1;
            _tallAmi.Setup(m => m.GetReadings(It.IsAny<IEnumerable<TallAmiModel>>(), out testNoOfDays, It.IsAny<bool>()))
                .Returns(reading)
                .Verifiable();

            DateTime billDate;
            var result = model.GetMeteredSericeList(mockModel, clientId, out billDate);

            Assert.IsNotNull(result);
            var lastreadingIndex = result[0].Meters[0].Readings.Count - 1;
            Assert.AreEqual(currentStart, result[0].Meters[0].Readings[lastreadingIndex].Timestamp);
            Assert.AreEqual(2, result.Count);
            Assert.AreEqual(1, result[0].Meters.Count);
            Assert.AreEqual(1, result[1].Meters.Count);
            Assert.AreEqual(rateClass, result[1].Meters[0].RateClass);
            Assert.AreEqual(sewerRateClass, result[1].Meters[0].RateClass2);
            Assert.AreEqual(elecRateClass, result[0].Meters[0].RateClass);
            Assert.IsNull(result[0].Meters[0].RateClass2);
            _tallAmi.Verify();
            _billingCycleSchedule.Verify();
        }

        [TestMethod, TestCategory("BilltoDateController"), TestCategory("BVT")]
        public void BillToDateV1_GetMeteredSericeList_SewerWater_Test4()
        {
            const int clientId = 87;
            const string customerId = "BTD001";
            const string accountId = "BTD001a1";
            const string premiseId = "BTD001a1p1";
            const string billCycleScheduleId = "Cycle01";
            const string meterId = "WM001";
            const string elecMeterId = "EM001";
            const string meterType = "AMI";
            const string sewerRateClass = "W-RES-1";
            const string rateClass = "W-RES";
            const string elecRateClass = "RG-1";
            const string servicePointIdSewer = "SP_BTD001a1p1s1_4";
            const string servicePointIdWater = "SP_BTD001a1p1s1_3";
            const string servicePointIdElectric = "SP_BTD001a1p1s1_1";
            const string serviceWaterSewerId = "BTD001a1p1s1";
            const string serivceElectId = "BTD001a1p1s2";
            const int uomId = 6;

            var today = DateTime.Today.Date;
            today = new DateTime(today.Year, today.Month, today.Day, 0, 0, 0, DateTimeKind.Unspecified);
            var currentStart = today.AddDays(-6);
            var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            var mockBtdSettings = TestHelpers.CreateMockBillToDateSettings(clientId);
            var model = new BillToDateModel(_container, mockBtdSettings, null);
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();
            // mock billing data
            var mockModel = new List<BillingModel>();
            var mockElecModel = CreateBillingModelsData(clientId, customerId, accountId, premiseId, serivceElectId,
                servicePointIdElectric, elecMeterId, billCycleScheduleId, 1, meterType, elecRateClass, 0, lastBillStartDate, lastBillEndDate);
            var mockSewerModel = CreateBillingModelsData(clientId, customerId, accountId, premiseId, serviceWaterSewerId,
                servicePointIdSewer, meterId, billCycleScheduleId, 4, meterType, sewerRateClass, uomId, lastBillStartDate, lastBillEndDate);
            var mockWaterModel = CreateBillingModelsData(clientId, customerId, accountId, premiseId, serviceWaterSewerId,
                servicePointIdWater, meterId, billCycleScheduleId, 3, meterType, rateClass, uomId, lastBillStartDate, lastBillEndDate);
            mockModel.Add(mockElecModel);
            mockModel.Add(mockSewerModel);
            mockModel.Add(mockWaterModel);

            // mock bill cycle data
            var billCycle = CreateMockBillCycleScheduleModel(currentStart, currentEnd, billCycleScheduleId, clientId);
            _billingCycleSchedule.Setup(
                m => m.GetBillingCycleScheduleByDateAsync(clientId, billCycleScheduleId, today))
                .Returns(billCycle)
                .Verifiable();

            // mock ami data
            var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
            var endDateTime = TimeZoneInfo.ConvertTimeToUtc(new DateTime(today.Year, today.Month, today.Day, 23, 59, 59), timeZoneInfo);
            var amiModel = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterId, servicePointIdWater, currentStart, today.AddDays(-1), currentEnd);
            var elecAmiModel = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, elecMeterId, servicePointIdElectric, currentStart, today.AddDays(-1), currentEnd);
            _tallAmi.Setup(m => m.GetAmiList(currentStart.ToUniversalTime(), endDateTime, meterId, accountId, clientId)).Returns(amiModel).Verifiable();
            _tallAmi.Setup(m => m.GetAmiList(currentStart.ToUniversalTime(), endDateTime, elecMeterId, accountId, clientId)).Returns(elecAmiModel).Verifiable();
            var reading = TestHelpers.CreateMockAmiHourlyReading(currentStart, today);
            // ReSharper disable once RedundantAssignment
            int testNoOfDays = 1;
            _tallAmi.Setup(m => m.GetReadings(It.IsAny<IEnumerable<TallAmiModel>>(), out testNoOfDays, It.IsAny<bool>()))
                .Returns(reading)
                .Verifiable();


            DateTime billDate;
            var result = model.GetMeteredSericeList(mockModel, clientId, out billDate);

            Assert.IsNotNull(result);
            var lastreadingIndes = result[0].Meters[0].Readings.Count - 1;
            Assert.AreEqual(currentStart, result[0].Meters[0].Readings[lastreadingIndes].Timestamp);
            Assert.AreEqual(2, result.Count);
            Assert.AreEqual(1, result[0].Meters.Count);
            Assert.AreEqual(1, result[1].Meters.Count);
            Assert.AreEqual(rateClass, result[1].Meters[0].RateClass);
            Assert.AreEqual(sewerRateClass, result[1].Meters[0].RateClass2);
            Assert.AreEqual(elecRateClass, result[0].Meters[0].RateClass);
            Assert.IsNull(result[0].Meters[0].RateClass2);
            _tallAmi.Verify();
            _billingCycleSchedule.Verify();
        }

        [TestMethod, TestCategory("BilltoDateController"), TestCategory("BVT")]
        public void BillToDateV1_GetMeteredSericeList_SewerWater_Test5()
        {
            const int clientId = 87;
            const string customerId = "BTD001";
            const string accountId = "BTD001a1";
            const string premiseId = "BTD001a1p1";
            const string billCycleScheduleId = "Cycle01";
            const string meterId = "WM001";
            const string elecMeterId = "EM001";
            const string meterType = "AMI";
            const string sewerRateClass = "W-RES-1";
            const string elecRateClass = "RG-1";
            const string servicePointIdSewer = "SP_BTD001a1p1s1_4";
            const string servicePointIdElectric = "SP_BTD001a1p1s1_1";
            const string serviceWaterSewerId = "BTD001a1p1s1";
            const string serivceElectId = "BTD001a1p1s2";
            const int uomId = 6;

            var today = DateTime.Today.Date;
            today = new DateTime(today.Year, today.Month, today.Day, 0, 0, 0, DateTimeKind.Unspecified);
            var currentStart = today.AddDays(-6);
            var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            var mockBtdSettings = TestHelpers.CreateMockBillToDateSettings(clientId);
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();
            // mock billing data
            var mockModel = new List<BillingModel>();
            var mockElecModel = CreateBillingModelsData(clientId, customerId, accountId, premiseId, serivceElectId,
                servicePointIdElectric, elecMeterId, billCycleScheduleId, 1, meterType, elecRateClass, 0, lastBillStartDate, lastBillEndDate);
            var mockSewerModel = CreateBillingModelsData(clientId, customerId, accountId, premiseId, serviceWaterSewerId,
                servicePointIdSewer, meterId, billCycleScheduleId, 4, meterType, sewerRateClass, uomId, lastBillStartDate, lastBillEndDate);
            mockModel.Add(mockElecModel);
            mockModel.Add(mockSewerModel);

            // mock bill cycle data
            var billCycle = CreateMockBillCycleScheduleModel(currentStart, currentEnd, billCycleScheduleId, clientId);
            _billingCycleSchedule.Setup(
                m => m.GetBillingCycleScheduleByDateAsync(clientId, billCycleScheduleId, today))
                .Returns(billCycle)
                .Verifiable();

            // mock ami data
            var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
            var endDateTime = TimeZoneInfo.ConvertTimeToUtc(new DateTime(today.Year, today.Month, today.Day, 23, 59, 59), timeZoneInfo);
            var amiModel = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterId, servicePointIdSewer, currentStart, today.AddDays(-1), currentEnd);
            var elecAmiModel = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, elecMeterId, servicePointIdElectric, currentStart, today.AddDays(-1), currentEnd);
            _tallAmi.Setup(m => m.GetAmiList(currentStart.ToUniversalTime(), endDateTime, meterId, accountId, clientId)).Returns(amiModel).Verifiable();
            _tallAmi.Setup(m => m.GetAmiList(currentStart.ToUniversalTime(), endDateTime, elecMeterId, accountId, clientId)).Returns(elecAmiModel).Verifiable();
            var reading = TestHelpers.CreateMockAmiHourlyReading(currentStart, today);
            // ReSharper disable once RedundantAssignment
            int testNoOfDays = 1;
            _tallAmi.Setup(m => m.GetReadings(It.IsAny<IEnumerable<TallAmiModel>>(), out testNoOfDays, It.IsAny<bool>()))
                .Returns(reading)
                .Verifiable();

            DateTime billDate;
            var model = new BillToDateModel(_container, mockBtdSettings, null);
            var result = model.GetMeteredSericeList(mockModel, clientId, out billDate);

            Assert.IsNotNull(result);
            var lastreadingIndes = result[0].Meters[0].Readings.Count - 1;
            Assert.AreEqual(currentStart, result[0].Meters[0].Readings[lastreadingIndes].Timestamp);
            Assert.AreEqual(2, result.Count);
            Assert.AreEqual(1, result[0].Meters.Count);
            Assert.AreEqual(1, result[1].Meters.Count);
            Assert.AreEqual(sewerRateClass, result[1].Meters[0].RateClass);
            Assert.AreEqual(sewerRateClass, result[1].Meters[0].RateClass2);
            Assert.AreEqual(elecRateClass, result[0].Meters[0].RateClass);
            Assert.IsNull(result[0].Meters[0].RateClass2);
            _tallAmi.Verify();
            _billingCycleSchedule.Verify();
        }

        /// <summary>
        /// BillToDateV1_GetNonMeteredServiceListTest()
        /// </summary>
        [TestMethod, TestCategory("BilltoDateController"), TestCategory("BVT")]
        public void BillToDateV1_GetNonMeteredServiceListTest()
        {
            const int clientId = 87;
            const string customerId = "BTD001";
            const string accountId = "BTD001a1";
            const string premiseId = "BTD001a1p1";
            const string billCycleScheduleId = "Cycle01";
            const string servicePointId = "SP_BTD001a1";

            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            var model = new BillToDateModel(_container, clientId);

            // mock non billing data
            var mockBillingModel = new List<BillingModel>();
            var mockNonMetered = CreateNonMeteredBillingModels(clientId, customerId, accountId, premiseId, "BTD001a1_BTD001a1p1_10_NM001",
                servicePointId, billCycleScheduleId, lastBillStartDate, lastBillEndDate);
            mockBillingModel.Add(mockNonMetered);

            // mock bill cycle data
            var billCycle = CreateMockBillCycleScheduleModel(currentStart, currentEnd, billCycleScheduleId, clientId);
            _billingCycleSchedule.Setup(
                m => m.GetBillingCycleScheduleByDateAsync(clientId, billCycleScheduleId, currentStart))
                .Returns(billCycle)
                .Verifiable();

            var result = model.GetNonMeteredServiceList(mockBillingModel, clientId, currentStart);

            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(currentStart, result[0].StartDate);
            //Assert.AreEqual(currentEnd, result[0].ProjectedEndDate);
            Assert.AreNotEqual("Electric", result[0].Name);
            Assert.AreNotEqual("Gas", result[0].Name);
            Assert.AreNotEqual("Water", result[0].Name);
            Assert.AreNotEqual("Sewer", result[0].Name);
        }

        [TestMethod, TestCategory("BilltoDateController"), TestCategory("BVT")]
        public void BillToDateV1_GetTotalServiceUsageMonthsForSewerTest()
        {
            const int clientId = 87;
            const string customerId = "BTD001";
            const string accountId = "BTD001a1";
            const string premiseId = "BTD001a1p1";
            const string billCycleScheduleId = "Cycle01";
            const string serviceContractId = "BTD001a1_BTD001a1p1_1_EM001";
            const string meterId = "EM001";
            const string rateClass = "RG-1";
            const string servicePointId = "SP_BTD001a1";

            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            var model = new BillToDateModel(_container, clientId);

            // mock billing model data
            var billings = new List<BillingModel>();
            var usages = new List<double>();

            for (var i = 0; i < 12; i++)
            {
                var usage = 1000;
                switch (lastBillEndDate.Month)
                {
                    case 3:
                        usage = 400;
                        usages.Add(usage);
                        break;
                    case 2:
                        usage = 600;
                        usages.Add(usage);
                        break;
                    case 1:
                        usage = 500;
                        usages.Add(usage);
                        break;
                    case 12:
                        usage = 450;
                        usages.Add(usage);
                        break;
                }

                var mockBillModel = CreateBillingModelsData(clientId, customerId, accountId, premiseId, serviceContractId, servicePointId,
                    meterId, billCycleScheduleId, 4, "AMI", rateClass, 2, lastBillStartDate, lastBillEndDate, usage);

                billings.Add(mockBillModel);
                lastBillEndDate = lastBillStartDate.AddDays(-1);
                lastBillStartDate = lastBillEndDate.AddDays(-29);

            }
            _billing.Setup(
                m => m.GetBillsForService(clientId, customerId, accountId, serviceContractId, today.AddYears(-3)))
                .Returns(billings)
                .Verifiable();

            var expected = usages.OrderByDescending(b => b).ToList()[1];
            var result = model.GetTotalSeriveUsageMonthsForSewer(clientId, customerId, accountId, meterId, serviceContractId);

            Assert.AreEqual(expected, result);
            _billing.Verify();
        }

        /// <summary>
        /// BillToDateV1_GetTotalSeriveUsageMonthsForSewer_NoServiceUsage_Test()
        /// </summary>
        [TestMethod, TestCategory("BilltoDateController"), TestCategory("BVT")]
        public void BillToDateV1_GetTotalSeriveUsageMonthsForSewer_NoServiceUsage_Test()
        {
            const int clientId = 87;
            const string customerId = "BTD001";
            const string accountId = "BTD001a1";
            const string premiseId = "BTD001a1p1";
            const string billCycleScheduleId = "Cycle01";
            const string serviceContractId = "BTD001a1_BTD001a1p1_1_EM001";
            const string meterId = "EM001";
            const string rateClass = "RG-1";
            const string servicePointId = "SP_BTD001a1";
            const double expected = 0;

            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            var model = new BillToDateModel(_container, clientId);

            // mock billing entities data
            var billings = new List<BillingModel>();

            for (var i = 0; i < 12; i++)
            {
                if (lastBillEndDate.Month > 3 && lastBillEndDate.Month < 12)
                {
                    var mockBillModel = CreateBillingModelsData(clientId, customerId, accountId, premiseId, serviceContractId, servicePointId,
                    meterId, billCycleScheduleId, 4, "AMI", rateClass, 2, lastBillStartDate, lastBillEndDate, 1000);

                    billings.Add(mockBillModel);
                    lastBillEndDate = lastBillStartDate.AddDays(-1);
                    lastBillStartDate = lastBillEndDate.AddDays(-29);
                }


            }
            _billing.Setup(
                m => m.GetBillsForService(clientId, customerId, accountId, serviceContractId, today.AddYears(-3)))
                .Returns(billings)
                .Verifiable();

            var result = model.GetTotalSeriveUsageMonthsForSewer(clientId, customerId, accountId, meterId, serviceContractId);

            Assert.AreEqual(expected, result);
            _billing.Verify();
        }

        [TestMethod, TestCategory("BilltoDateController"), TestCategory("BVT")]
        public void BillToDateV1_MapBillToDateResultTest()
        {
            const int clientId = 87;
            const decimal expectedTotalCost = 28;
            const decimal expectedProjectedCost = 30;
            const string expectedStart = "11/19/2015";
            const string expectedEnd = "12/18/2015";
            const string expectedRead = "12/16/2015";
            const double expectedBillDays = 30;
            const double expectedDaysInCycle = 28;


            //var controller = new Controllers.Version1.BillToDateController(container);
            var model = new BillToDateModel(_container, clientId);

            var billToDateResult = CreateBillToDateResultData();

            var result = model.MapBillToDateResult(billToDateResult);


            Assert.AreEqual(expectedTotalCost, result.TotalCost);
            Assert.AreEqual(expectedProjectedCost, result.TotalProjectedCost);
            Assert.AreEqual(expectedStart, result.BillStartDate);
            Assert.AreEqual(expectedEnd, result.BillEndDate);
            Assert.AreEqual(expectedBillDays, result.BillDays);
            Assert.AreEqual(expectedTotalCost * 10, result.Services[0].UseToDate);
            Assert.AreEqual(expectedProjectedCost * 10, result.Services[0].UseProjected);
            Assert.AreEqual(expectedRead, result.Services[0].ReadDate);
            Assert.AreEqual(expectedDaysInCycle, result.Services[0].DaysIntoCycle);
            Assert.AreEqual(expectedProjectedCost / Convert.ToDecimal(expectedBillDays), result.AverageDailyCost);
        }

        [TestMethod, TestCategory("BilltoDateController"), TestCategory("BVT")]
        public void BillToDateV1_MapBillToDateResultTest2()
        {
            const int clientId = 87;
            const decimal expectedTotalCost = 56;
            const decimal expectedProjectedCost = 60;
            const decimal expectedUsage = 280;
            const decimal expectedProjectedUsage = 300;
            const string expectedStart = "11/16/2015";
            const string expectedEnd = "12/18/2015";
            const string expectedRead = "12/16/2015";
            const double expectedBillDays = 33;
            const double expectedDaysInCycle = 28;
            const double expectedDaysInCycle2 = 31;

            //var controller = new Controllers.Version1.BillToDateController(container);
            var model = new BillToDateModel(_container, clientId);

            var billToDateResult = CreateBillToDateResultData2();

            var result = model.MapBillToDateResult(billToDateResult);

            Assert.AreEqual(expectedTotalCost, result.TotalCost);
            Assert.AreEqual(expectedProjectedCost, result.TotalProjectedCost);
            Assert.AreEqual(expectedStart, result.BillStartDate);
            Assert.AreEqual(expectedEnd, result.BillEndDate);
            Assert.AreEqual(expectedBillDays, result.BillDays);
            Assert.AreEqual(expectedUsage, result.Services[0].UseToDate);
            Assert.AreEqual(expectedProjectedUsage, result.Services[0].UseProjected);
            Assert.AreEqual(expectedRead, result.Services[0].ReadDate);
            Assert.AreEqual(expectedDaysInCycle, result.Services[0].DaysIntoCycle);
            Assert.AreEqual(expectedUsage, result.Services[1].UseToDate);
            Assert.AreEqual(expectedProjectedUsage, result.Services[1].UseProjected);
            Assert.AreEqual(expectedRead, result.Services[1].ReadDate);
            Assert.AreEqual(expectedDaysInCycle2, result.Services[1].DaysIntoCycle);
            Assert.AreEqual(Math.Round(expectedTotalCost / Convert.ToDecimal(expectedDaysInCycle2), 2), result.AverageDailyCost);
        }

        /// <summary>
        /// 
        /// </summary>
        [TestMethod, TestCategory("BilltoDateController"), TestCategory("BVT")]
        public void BillToDateV1_CalculateBillToDateTest()
        {
            const string customerId = "test";
            const string accountId = "test";
            const int clientId = 87;
            const int rateCompanyId = 100;

            var start = Convert.ToDateTime("12/17/2015");
            var end = Convert.ToDateTime("12/22/2015");
            var projected = Convert.ToDateTime("1/16/2016");

            var expectedBillDays = (projected - start).TotalDays + 1;
            var exptectedServiceDaysIntoCycle = (end - start).TotalDays + 1;


            var bill = CreateBillToDateTestData(clientId, customerId, accountId, rateCompanyId, start, end, projected);
            var model = new BillToDateModel(_container, bill.ClientID);
            var result = model.CalculateBillToDate(bill, false);

            Assert.IsNotNull(result);
            Assert.AreEqual(expectedBillDays, result.BillDays);
            Assert.AreEqual(start, Convert.ToDateTime(result.BillStartDate));
            Assert.AreEqual(projected, Convert.ToDateTime(result.BillEndDate));
            Assert.AreEqual(exptectedServiceDaysIntoCycle, result.Services[0].DaysIntoCycle);

        }

        /// <summary>
        /// BillToDateV1_GetBillToDateTest()
        /// </summary>
        [TestMethod, TestCategory("BilltoDateController"), TestCategory("BVT")]
        public void BillToDateV1_GetBillToDateTest()
        {
            int clientId = 61;
            string customerId = "BTD001";
            string accountId = "BTD001a1";
            string premiseId = "BTD001a1p1";
            string billCycleScheduleId = "Cycle01";
            string meterId = "M0001w";
            string rateClass = "S1";
            string servicePointId = "SP_BTD001a1";

            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            //var expectedBillDays = (currentEnd - currentStart).TotalDays + 1;

            var billToDateRequest = new BillToDateRequest
            {
                CustomerId = customerId,
                AccountId = accountId
            };

            var mockBtdSettings = TestHelpers.CreateMockBillToDateSettings(clientId);

            var model = new BillToDateModel(_container, mockBtdSettings, _billToDate.Object);

            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();

            // mock billing entities data --- Setup Bills with HCF Uom.
            var mockBillModel = CreateBillingModelsData(clientId, customerId, accountId, premiseId, "BTD001a1_BTD001a1p1_1_EM001", servicePointId,
                    meterId, billCycleScheduleId, 3, "AMI", rateClass, 8, lastBillStartDate, lastBillEndDate, 1000);

            _billing.Setup(m => m.GetAllServicesFromLastBill(clientId, customerId, accountId))
                .Returns(new List<BillingModel> { mockBillModel })
                .Verifiable();
            
            var mockBillResult = CreateBillToDateResultData();

            // any mock result - to ensure we are actually calling bill to data calc instead of using the calculated result
            _billToDate.Setup(b => b.CalculateBillToDate(It.IsAny<BillToDate.Bill>()))
                .Returns(mockBillResult).Verifiable();

            // mock bill cycle data
            var billCycle = CreateMockBillCycleScheduleModel(currentStart, currentEnd, billCycleScheduleId, clientId);
            _billingCycleSchedule.Setup(
                m => m.GetBillingCycleScheduleByDateAsync(clientId, billCycleScheduleId, today))
                .Returns(billCycle)
                .Verifiable();

            // Mock ami data -- Setup AMI with CCF Uom
            var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
            var endDateTime = TimeZoneInfo.ConvertTimeToUtc(new DateTime(today.Year, today.Month, today.Day, 23, 59, 59), timeZoneInfo);
            var amiModel = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterId, servicePointId,
                currentStart, today.AddDays(-1), currentEnd, 3, 2);
            _tallAmi.Setup(m => m.GetAmiList(currentStart.ToUniversalTime(), endDateTime, meterId, accountId, clientId)).Returns(amiModel).Verifiable();
            var reading = TestHelpers.CreateMockAmiHourlyReading(currentStart, today);
            // ReSharper disable once RedundantAssignment
            int testNoOfDays = 1;
            _tallAmi.Setup(m => m.GetReadings(It.IsAny<IEnumerable<TallAmiModel>>(), out testNoOfDays, It.IsAny<bool>()))
                .Returns(reading)
                .Verifiable();

            //add property for client user, normally from the stored procedure but hardcoded here for unit test purposes
            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };


            try
            {
                var response = model.GetBillToDate(clientUser, billToDateRequest);
                Assert.IsNotNull(response);
                //Assert.IsNull(response.BillToDate.LogEntries);
                //Assert.AreEqual(false, response.BillToDate.HasError);
                //Assert.AreEqual(expectedBillDays, response.BillToDate.BillDays);
                //Assert.AreEqual(currentStart, Convert.ToDateTime(response.BillToDate.BillStartDate));
                //Assert.AreEqual(currentEnd, Convert.ToDateTime(response.BillToDate.BillEndDate));
                //Assert.AreEqual("water", response.BillToDate.Services[0].CommodityKey);
                //Assert.AreEqual("hcf", response.BillToDate.Services[0].UomKey);
                _billToDate.Verify();
                _tallAmi.Verify();
                _billing.Verify();
                _billingCycleSchedule.Verify();
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }

        }

        /// <summary>
        /// BillToDateV1_GetBillToDatePostTest()
        /// </summary>
        [TestMethod, TestCategory("BilltoDateController"), TestCategory("BVT")]
        public void BillToDateV1_GetBillToDatePostTest()
        {
            var start = Convert.ToDateTime("12/17/2015");
            var end = Convert.ToDateTime("12/22/2015");
            var projected = Convert.ToDateTime("1/16/2016");

            //var expectedBillDays = (projected - start).TotalDays + 1;
            //var exptectedServiceDaysIntoCycle = (end - start).TotalDays + 1;

            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();

            var mockBillResult = CreateBillToDateResultData();

            // any mock result - to ensure we are actually calling bill to data calc instead of using the calculated result
            _billToDate.Setup(b => b.CalculateBillToDate(It.IsAny<BillToDate.Bill>()))
                .Returns(mockBillResult).Verifiable();


            var post = new BillToDatePostRequest
            {
                AccountId = "BTD001a1",
                CustomerId = "BTD001",
                RateCompanyId = 100,
                Services = new List<BillToDateMeterService>
                {
                    CreatePostServiceData(start, end, projected)
                }
            };

            var nonMeterService = new BillToDateNonMeter
            {
                Id = "BTD001a1nm",
                CommodityKey = "FireProtection",
                StartDate = start.ToShortDateString(),
                EndDate = end.ToShortDateString(),
                ProjectedEndDate = projected.ToShortDateString(),
                Usage = 60,
                Cost = 60,
                UomKey = "trees"
            };
            post.NonMeteredServices = new List<BillToDateNonMeter> { nonMeterService };

            post.Configurations = new List<ContentModel.Entities.ClientConfiguration>();
            var setting = new ContentModel.Entities.ClientConfiguration
            {
                Key = "general.minimumchargetype",
                Category = "billtodateengine",
                Value = "2",
                Description = "Type of minumim charges | 0=NoMinimumCharge | 1=ProrateMinimumCharge | 2=NoProrateMinimumCharge"
            };
            post.Configurations.Add(setting);

            //var controller = new Controllers.Version1.BillToDateController(container);
            //add property for client user, normally from the stored procedure but hardcoded here for unit test purposes
            var clientUser = new ClientUser
            {
                ClientID = 87,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };


            var mockBtdSettings = TestHelpers.CreateMockBillToDateSettings(clientUser.ClientID);

            var model = new BillToDateModel( clientUser.ClientID, _container, mockBtdSettings, post.Configurations, _billToDate.Object);

            var response = model.GetBillToDate(clientUser, post);
            JsonConvert.SerializeObject(post, Formatting.Indented);

            Assert.IsNotNull(response);
            _billToDate.Verify();
        }

        /// <summary>
        /// BillToDateV1_GetBillToDate_Error_Test()
        /// </summary>
        [TestMethod, TestCategory("BilltoDateController"), TestCategory("BVT")]
        public void BillToDateV1_GetBillToDate_Error_Test()
        {
            var billToDateRequest = new BillToDateRequest
            {
                CustomerId = "BTD001",
                AccountId = "BTD001a1"
            };

            var model = new BillToDateModel(_container);

            //add property for client user, normally from the stored procedure but hardcoded here for unit test purposes
            var clientUser = new ClientUser
            {
                ClientID = 87,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };

            var response = model.GetBillToDate(clientUser, billToDateRequest);

            Assert.IsNotNull(response);
            Assert.IsNull(response.BillToDate);
        }

        [TestMethod, TestCategory("BilltoDateController"), TestCategory("BVT")]
        public void BillToDateV1_GetBillToDate_CassandraStorage_Test()
        {
            const int clientId = 87;
            const string customerId = "BTD001";
            const string accountId = "BTD001a1";
            const string premiseId = "BTD001a1p1";
            const string billCycleScheduleId = "Cycle01";
            const string meterId = "EM001";
            const string rateClass = "RG-1";
            const string servicePointId = "SP_BTD001a1";

            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            var expectedBillDays = (currentEnd - currentStart).TotalDays + 1;
            var exptectedServiceDaysIntoCycle = (today - currentStart).TotalDays + 1;

            var billToDateRequest = new BillToDateRequest
            {
                CustomerId = customerId,
                AccountId = accountId
            };

            var mockBtdSettings = TestHelpers.CreateMockBillToDateSettings(clientId);

            var model = new BillToDateModel(_container, mockBtdSettings, _billToDate.Object);


            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();
            // mock billing entities data
            var mockBillModel = CreateBillingModelsData(clientId, customerId, accountId, premiseId, "BTD001a1_BTD001a1p1_1_EM001", servicePointId,
                    meterId, billCycleScheduleId, 0, "AMI", rateClass, 1, lastBillStartDate, lastBillEndDate, 1000);

            _billing.Setup(m => m.GetAllServicesFromLastBill(clientId, customerId, accountId))
                .Returns(new List<BillingModel> { mockBillModel })
                .Verifiable();

            // mock calculate data
            var mockCalculateData = CreateMockCalculateModel(currentStart, today, currentEnd, rateClass);
            _calculation.Setup(m => m.GetBtdCalculationAsync(clientId, accountId))
                .Returns(Task.FromResult(mockCalculateData))
                .Verifiable();
            _calculation.Setup(m => m.GetCtdCalculationAsync(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(mockCalculateData))
                .Verifiable();
            //_calculation.Setup(m => m.GetBtdCalculation(clientId, accountId))
            //    .Returns(mockCalculateData)
            //    .Verifiable();
            //_calculation.Setup(m => m.GetCtdCalculation(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>()))
            //    .Returns(mockCalculateData)
            //    .Verifiable();
            //add property for client user, normally from the stored procedure but hardcoded here for unit test purposes
            var clientUser = new ClientUser
            {
                ClientID = 87,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };

            try
            {
                var response = model.GetBillToDate(clientUser, billToDateRequest);

                _calculation.Verify();
                Assert.IsNotNull(response);
                Assert.AreEqual(false, response.BillToDate.HasError);
                Assert.AreEqual(expectedBillDays, response.BillToDate.BillDays);
                //Assert.AreEqual(currentStart, Convert.ToDateTime(response.BillToDate.BillStartDate));
                //Assert.AreEqual(currentEnd, Convert.ToDateTime(response.BillToDate.BillEndDate));
                Assert.AreEqual(exptectedServiceDaysIntoCycle, response.BillToDate.Services[0].DaysIntoCycle);
                Assert.AreEqual("electric", response.BillToDate.Services[0].CommodityKey);
                Assert.AreEqual("kwh", response.BillToDate.Services[0].UomKey);
                Assert.AreEqual(rateClass, response.BillToDate.Services[0].RateClass);
                _billing.Verify();
                _calculation.Verify();
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod(), TestCategory("BilltoDateController"), TestCategory("BVT")]
        public void BillToDateV1_GetCalcuatedBillToDate_CassandraStorage_Test()
        {
            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId = "EM001";
            var rateClass = "RG-1";
            var servicePointId = "SP_BTD001a1";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);


            var expectedBillDays = (currentEnd - currentStart).TotalDays + 1;
            var exptectedServiceDaysIntoCycle = (today - currentStart).TotalDays + 1;

            var billToDateRequest = new BillToDateRequest();
            billToDateRequest.CustomerId = customerId;
            billToDateRequest.AccountId = accountId;

            var model = new BillToDateModel(_container, clientId);
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();
            // mock billing model data
            var mockBillModel = CreateBillingModelsData(clientId, customerId, accountId, premiseId, "BTD001a1_BTD001a1p1_1_EM001",
                servicePointId, meterId, billCycleScheduleId, 1, "AMI", rateClass, 0, lastBillStartDate, lastBillEndDate);
            var mockBillList = new List<BillingModel>();
            mockBillList.Add(mockBillModel);
            // mock calculate data
            var mockCalculateData = CreateMockCalculateModel(currentStart, today, currentEnd, rateClass);
            _calculation.Setup(m => m.GetBtdCalculationAsync(clientId, accountId))
                .Returns(Task.FromResult(mockCalculateData))
                .Verifiable();
            _calculation.Setup(m => m.GetCtdCalculationAsync(clientId, accountId, "BTD001a1_BTD001a1p1_1_EM001"))
                .Returns(Task.FromResult(mockCalculateData))
                .Verifiable();

            var response = model.GetCalculatedBillToDate(mockBillList, clientId, accountId);

            Assert.IsNotNull(response);
            Assert.AreEqual(false, response.HasError);
            Assert.AreEqual(expectedBillDays, response.BillDays);
            //Assert.AreEqual(currentStart, Convert.ToDateTime(response.BillStartDate));
            //Assert.AreEqual(currentEnd, Convert.ToDateTime(response.BillEndDate));
            Assert.AreEqual(exptectedServiceDaysIntoCycle, response.Services[0].DaysIntoCycle);
            Assert.AreEqual(rateClass, response.Services[0].RateClass);
            _calculation.Verify();
        }

        [TestMethod(), TestCategory("BilltoDateController"), TestCategory("BVT")]
        public void BillToDateV1_GetCalcuatedBillToDate_WaterSewer_CassandraStorage_Test()
        {
            var clientId = 290;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId = "WM001";
            var rateClass = "W-1";
            var sewerRateClass = "S-1";
            var servicePointId = "SP_BTD001a1";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            //C_BTD001a1_BTD001a1p1_1_EM001

            var expectedBillDays = (currentEnd - currentStart).TotalDays + 1;
            var exptectedServiceDaysIntoCycle = (today - currentStart).TotalDays + 1;

            var billToDateRequest = new BillToDateRequest();
            billToDateRequest.CustomerId = customerId;
            billToDateRequest.AccountId = accountId;

            var model = new BillToDateModel(_container, clientId);
            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            _clientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();
            // mock billing model data
            var mockBillModel = CreateBillingModelsData(clientId, customerId, accountId, premiseId, "BTD001a1_BTD001a1p1_1_WM001",
                servicePointId, meterId, billCycleScheduleId, 3, "AMI", rateClass, 0, lastBillStartDate, lastBillEndDate);
            var mockSewerBillModel = CreateBillingModelsData(clientId, customerId, accountId, premiseId, "BTD001a1_BTD001a1p1_1_WM001sewer",
                servicePointId, meterId, billCycleScheduleId, 4, "AMI", sewerRateClass, 0, lastBillStartDate, lastBillEndDate);
            var mockBillList = new List<BillingModel>();
            mockBillList.Add(mockBillModel);
            mockBillList.Add(mockSewerBillModel);
            // mock calculate data
            var mockCalculateData = CreateMockCalculateModel(currentStart, today, currentEnd, rateClass, 3);
            var mockSewerCalculateData = CreateMockCalculateModel(currentStart, today, currentEnd, sewerRateClass, 4);
            _calculation.Setup(m => m.GetBtdCalculationAsync(clientId, accountId))
                .Returns(Task.FromResult(mockCalculateData))
                .Verifiable();
            _calculation.Setup(m => m.GetCtdCalculationAsync(clientId, accountId, "BTD001a1_BTD001a1p1_1_WM001"))
                .Returns(Task.FromResult(mockSewerCalculateData))
                .Verifiable();
            _calculation.Setup(m => m.GetCtdCalculationAsync(clientId, accountId, "BTD001a1_BTD001a1p1_1_WM001sewer"))
                .Returns(Task.FromResult(mockSewerCalculateData))
                .Verifiable();


            var response = model.GetCalculatedBillToDate(mockBillList, clientId, accountId);

            Assert.IsNotNull(response);
            Assert.AreEqual(false, response.HasError);
            Assert.AreEqual(expectedBillDays, response.BillDays);
            //Assert.AreEqual(currentStart, Convert.ToDateTime(response.BillStartDate));
            //Assert.AreEqual(currentEnd, Convert.ToDateTime(response.BillEndDate));
            Assert.AreEqual(exptectedServiceDaysIntoCycle, response.Services[0].DaysIntoCycle);
            foreach (var service in response.Services)
            {
                if (service.CommodityKey.ToLower() == "water")
                {
                    Assert.AreEqual(rateClass, service.RateClass);
                }
                else
                {
                    Assert.AreEqual(sewerRateClass, service.RateClass);
                    Assert.AreEqual("sewer", service.CommodityKey.ToLower());
                }
            }
            _calculation.Verify();
        }

        #region "Create test data"

        private BillToDateResult CreateBillToDateResultData()
        {
            var result = new BillToDateResult();
            result.TotalCost = Convert.ToDouble(28);
            result.TotalProjectedCost = Convert.ToDouble(30);
            result.Bill = new BillToDate.Bill();
            result.Bill.MeteredServices = new MeteredServiceList();

            var meter = new Meter
            {
                Fuel = FuelType.electric,
                MeterID = "test",
                RateClass = "RG-1",
                RateCompanyID = 100,
                PrimaryUnitOfMeasure = UnitOfMeasureType.kWh
            };

            var usage = new RateModel.Usage(280);
            var usages = new List<RateModel.Usage>();
            usages.Add(usage);
            var projectedUsage = new RateModel.Usage(300);
            var projectedUsages = new List<RateModel.Usage>();
            projectedUsages.Add(projectedUsage);
            var costToDateResult = new RateModel.CostToDateResult
            {
                StartDate = Convert.ToDateTime("11/19/2015"),
                ProjectedEndDate = Convert.ToDateTime("12/18/2015"),
                EndDate = Convert.ToDateTime("12/16/2015"),
                Usages = usages,
                ProjectedUsages = projectedUsages,
                Cost = Convert.ToDouble(28),
                ProjectedCost = Convert.ToDouble(30)
            };

            meter.CostToDateResult = costToDateResult;
            var meteredSerivce = new MeteredService();
            meteredSerivce.Meters = new MeterList();
            meteredSerivce.Meters.Add(meter);
            result.Bill.MeteredServices.Add(meteredSerivce);

            return result;
        }

        private BillToDateResult CreateBillToDateResultData2()
        {
            var result = new BillToDateResult();
            result.TotalCost = Convert.ToDouble(56);
            result.TotalProjectedCost = Convert.ToDouble(60);
            result.Bill = new BillToDate.Bill();
            result.Bill.MeteredServices = new MeteredServiceList();

            var meter = new Meter
            {
                Fuel = FuelType.electric,
                MeterID = "test",
                PrimaryUnitOfMeasure = UnitOfMeasureType.kWh,
                RateClass = "RG-1",
                RateCompanyID = 100
            };

            var usage = new RateModel.Usage(280);
            var usages = new List<RateModel.Usage>();
            usages.Add(usage);
            var projectedUsage = new RateModel.Usage(300);
            var projectedUsages = new List<RateModel.Usage>();
            projectedUsages.Add(projectedUsage);
            var costToDateResult = new RateModel.CostToDateResult
            {
                StartDate = Convert.ToDateTime("11/19/2015"),
                ProjectedEndDate = Convert.ToDateTime("12/18/2015"),
                EndDate = Convert.ToDateTime("12/16/2015"),
                Usages = usages,
                ProjectedUsages = projectedUsages,
                Cost = Convert.ToDouble(28),
                ProjectedCost = Convert.ToDouble(30)
            };

            meter.CostToDateResult = costToDateResult;
            var meteredSerivce = new MeteredService();
            meteredSerivce.Meters = new MeterList();
            meteredSerivce.Meters.Add(meter);
            result.Bill.MeteredServices.Add(meteredSerivce);

            costToDateResult = new RateModel.CostToDateResult
            {
                StartDate = Convert.ToDateTime("11/16/2015"),
                ProjectedEndDate = Convert.ToDateTime("12/18/2015"),
                EndDate = Convert.ToDateTime("12/16/2015"),
                Usages = usages,
                ProjectedUsages = projectedUsages,
                Cost = Convert.ToDouble(28),
                ProjectedCost = Convert.ToDouble(30)
            };

            meter = new Meter
            {
                Fuel = FuelType.electric,
                MeterID = "test2",
                PrimaryUnitOfMeasure = UnitOfMeasureType.kWh,
                RateClass = "RG-1",
                RateCompanyID = 100
            };
            meter.CostToDateResult = costToDateResult;
            meteredSerivce = new MeteredService();
            meteredSerivce.Meters = new MeterList();
            meteredSerivce.Meters.Add(meter);
            result.Bill.MeteredServices.Add(meteredSerivce);

            return result;
        }

        private BillToDate.Bill CreateBillToDateTestData(int clientId, string customerId, string accountId, int rateCompanyId, DateTime start, DateTime end, DateTime projectedEnd)
        {
            var service = new MeteredService();
            var meter = new Meter("test", rateCompanyId, "binTierM", FuelType.electric, start, end, projectedEnd);
            meter.PrimaryUnitOfMeasure = UnitOfMeasureType.kWh;
            meter.Readings = CreateDailyReadingData(meter.StartDate ?? DateTime.MinValue, meter.EndDate ?? DateTime.MinValue);
            service.Meters.Add(meter);

            var bill = new BillToDate.Bill(start, clientId, customerId, accountId, rateCompanyId);
            bill.MeteredServices.Add(service);

            return bill;
        }

        private List<RateModel.Reading> CreateDailyReadingData(DateTime startDate, DateTime endDate)
        {
            var readings = new List<RateModel.Reading>();
            var daysOfData = (endDate - startDate).TotalDays + 1;
            for (int j = 0; j < daysOfData; j++)
            {
                var date = startDate.AddDays(j);
                for (int i = 0; i < 24; i++)
                {
                    var timestamp = date.AddHours(i);
                    var reading = new RateModel.Reading(RateModel.Enums.BaseOrTier.TotalServiceUse, RateModel.Enums.TimeOfUse.Undefined, RateModel.Enums.Season.Undefined, timestamp, 10, false);
                    readings.Add(reading);
                }
            }

            return readings;
        }

        private BillToDateMeterService CreatePostServiceData(DateTime startDate, DateTime endDate, DateTime projected)
        {
            var service = new BillToDateMeterService
            {
                CommodityKey = "Electric",
                Id = "BTD001a1s1"
            };
            service.Meters = new List<BillToDateMeter>();
            var meter = new BillToDateMeter
            {
                Id = "BTD001a1s1m1",
                StartDate = startDate.ToShortDateString(),
                EndDate = endDate.ToShortDateString(),
                ProjectedEndDate = projected.ToShortDateString(),
                RateClass = "binTierM",
                UomKey = "kwh"
            };
            meter.Readings = new List<BillToDateReading>();

            var daysOfData = (endDate - startDate).TotalDays + 1;
            for (int j = 0; j < daysOfData; j++)
            {
                var date = startDate.AddDays(j);
                for (int i = 0; i < 24; i++)
                {
                    var timestamp = date.AddHours(i);
                    var reading = new BillToDateReading
                    {
                        Timestamp = timestamp.ToString(CultureInfo.InvariantCulture),
                        EditCode = "Direct",
                        TouBin = "NoTOU",
                        Reading = 10
                    };
                    meter.Readings.Add(reading);
                }
            }
            service.Meters.Add(meter);

            return service;
        }

        private BillingModel CreateBillingModelsData(int clientId, string customerId, string accountId,
            string premiseId, string serviceContractId, string servicePointId, string meterid, string billCycle,
            int commodityId, string meterType, string rateClass, int uomId, DateTime start, DateTime end, double usage = 607)
        {
            var days = Convert.ToInt32((end - start).TotalDays + 1);

            var billingModel = new BillingModel
            {
                ClientId = clientId,
                CustomerId = customerId,
                AccountId = accountId,
                PremiseId = premiseId,
                BillCycleScheduleId = billCycle,
                BillDays = days,
                BillPeriodType = 1,
                CommodityId = commodityId,
                EndDate = end,
                IsFault = false,
                MeterId = meterid,
                MeterType = meterType,
                RateClass = rateClass,
                ReadDate = end,
                ReadQuality = "Actual",
                ServiceContractId = serviceContractId,
                ServicePointId = servicePointId,
                Source = "Utility",
                StartDate = start,
                TotalCost = 148,
                TotalUsage = usage,
                UOMId = uomId
            };

            return billingModel;
        }

        private List<BillingModel> CreateOldBillingModelsData()
        {
            var billingModels = new List<BillingModel>();

            //electric
            var billingModel = new BillingModel
            {
                ClientId = 87,
                CustomerId = "BTD001",
                AccountId = "BTD001a1",
                PremiseId = "BTD001a1p1",
                BillCycleScheduleId = "Cycle01",
                BillDays = 31,
                BillPeriodType = 1,
                CommodityId = 1,
                EndDate = Convert.ToDateTime("9/19/2015"),
                IsFault = true,
                MeterId = "EM001",
                MeterType = "AMI",
                RateClass = "RG-1",
                ReadDate = Convert.ToDateTime("9/19/2015"),
                ReadQuality = "Actual",
                ServiceContractId = "BTD001a1_BTD001a1p1_1_EM001",
                ServicePointId = "SP_BTD001a1",
                Source = "Utility",
                StartDate = Convert.ToDateTime("8/22/2015"),
                TotalCost = 148,
                TotalUsage = 607,
                UOMId = 0
            };
            billingModels.Add(billingModel);

            return billingModels;
        }

        private BillingModel CreateNonMeteredBillingModels(int clientId, string customerId, string accountId,
            string premiseId, string serviceContractId, string serviceId, string billCycle, DateTime start,
            DateTime end)
        {

            var days = Convert.ToInt32((end - start).TotalDays + 1);
            var billingModel = new BillingModel
            {
                ClientId = clientId,
                CustomerId = customerId,
                AccountId = accountId,
                PremiseId = premiseId,
                BillCycleScheduleId = billCycle,
                BillDays = days,
                BillPeriodType = 1,
                CommodityId = 5,
                EndDate = end,
                IsFault = false,
                MeterType = "NonMetered",
                ReadDate = end,
                ReadQuality = "Actual",
                ServiceContractId = serviceContractId,
                ServicePointId = serviceId,
                Source = "Utility",
                StartDate = start,
                TotalCost = 148,
                TotalUsage = 607,
                UOMId = 9
            };

            return billingModel;
        }

        private CalculationModel CreateMockCalculateModel(DateTime start, DateTime end, DateTime projected, string rateClass, int commodityId = 1)
        {
            var days = Convert.ToInt32((end - start).TotalDays + 1);
            var result = new CalculationModel
            {
                BillCycleStartDate = start.ToShortDateString(),
                BillCycleEndDate = projected.ToShortDateString(),
                BillDays = days,
                Cost = 1000,
                ProjectedCost = 1000,
                AverageDailyCost = 1000 / days,
                CommodityId = commodityId,
                AsOfAmiDate = end,
                AsOfCalculationDate = start,
                Usage = 1000,
                ProjectedUsage = 1000,
                Unit = 0,
                RateClass = rateClass
            };

            return result;
        }


        private BillCycleScheduleModel CreateMockBillCycleScheduleModel(DateTime start, DateTime end, string cycle, int clientId)
        {
            var billCycle = new BillCycleScheduleModel
            {
                ClientId = clientId.ToString(),
                BeginDate = start.ToString("MM-dd-yyyy"),
                EndDate = end.ToString("MM-dd-yyyy"),
                BillCycleScheduleName = cycle,
                Description = cycle
            };

            return billCycle;
        }


        #endregion

    }
}