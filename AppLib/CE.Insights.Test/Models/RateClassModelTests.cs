﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CE.Infrastructure;
using CE.Models;
using CE.Models.Insights;
using System;
using System.Linq;

namespace CE.Insights.Models.Tests
{
    [TestClass()]
    public class RateClassModelTests
    {
        #region Constants for content
        private const string ClientSpace = "netnln2itvho";
        private const string ClientAccessToken = "c58ec8d495044174267c257f105914f7fb9567127a879b2419e1c3d405a54be2";
        #endregion

        [TestMethod()]
        public void RateV1_GetBasicRateInfo_Test()
        {
            var rateClass = "demoWaterTiers";
            var rateCompanyId = 100;

            // expectedResult
            var customerType = "residential";
            var commodity = "water";
            var uom = "gal";
            var isTiered = true;
            var isSeason = false;
            var isTou = false;
            var isPTR = false;

            var model = new RateClassModel();

            var result = model.GetBasicRateInfo(rateCompanyId, rateClass, DateTime.Today.Date);

            Assert.IsNotNull(result);
            Assert.AreEqual(rateClass, result.RateClassID);
            Assert.AreEqual(isTiered, result.IsTiered);
            Assert.AreEqual(isSeason, result.IsSeasonal);
            Assert.AreEqual(isTou, result.IsTimeOfUse);
            Assert.AreEqual(isPTR, result.ContainsPTR);
            Assert.AreEqual(commodity, result.CommodityKey);
            Assert.AreEqual(uom, result.UomKey);
            Assert.AreEqual(customerType, result.CustomerType.ToLower());
        }

        [TestMethod()]
        public void RateV1_GetBasicRateInfo_RateHelper_Test()
        {
            var rateClass = "#rc~demoWaterTiers|idre~false";
            var rateCompanyId = 100;

            // expectedResult
            var expectedRateClass = "demoWaterTiers";
            var customerType = "residential";
            var commodity = "water";
            var uom = "gal";
            var isTiered = true;
            var isSeason = false;
            var isTou = false;
            var isPTR = false;

            var model = new RateClassModel();

            var result = model.GetBasicRateInfo(rateCompanyId, rateClass, DateTime.Today.Date);

            Assert.IsNotNull(result);
            Assert.AreEqual(expectedRateClass, result.RateClassID);
            Assert.AreEqual(isTiered, result.IsTiered);
            Assert.AreEqual(isSeason, result.IsSeasonal);
            Assert.AreEqual(isTou, result.IsTimeOfUse);
            Assert.AreEqual(isPTR, result.ContainsPTR);
            Assert.AreEqual(commodity, result.CommodityKey);
            Assert.AreEqual(uom, result.UomKey);
            Assert.AreEqual(customerType, result.CustomerType.ToLower());
        }

        [TestMethod()]
        public void RateV1_GetBasicRateInfo_NonExistedRateClass_Test()
        {
            var rateClass = "noRateClass";
            var rateCompanyId = 100;

            // expectedResult

            var model = new RateClassModel();

            var result = model.GetBasicRateInfo(rateCompanyId, rateClass, DateTime.Today.Date);

            Assert.IsNull(result);
        }

        [TestMethod()]
        public void RateV1_GetDetailedRateInfo_Test()
        {
            var rateClass = "demoWaterTiers";
            var rateCompanyId = 100;
            RateClass rateClassInfo = null;

            // expectedResult
            var customerType = "residential";
            var commodity = "water";
            var uom = "gal";
            var isTiered = true;
            var isSeason = false;
            var isTou = false;
            var isPTR = false;
            var useCostType = "STD_ENGY";
            var serviceCostType = "STD_CUST";
            var partType = "Primary";
            var undefined = "Undefined";
            var tier1Threshold = 500m;
            var tier2Threshold = 700m;
            var serviceCharge = 21.54m;
            var tier1UseCharge = 0.0015m;
            var tier2UseCharge = 0.00175m;
            var tier3UseCharge = 0.0022m;
            var tier1 = "Tier1";
            var tier2 = "Tier2";

            var model = new RateClassModel();

            var result = model.GetDetailedRateInfo(rateCompanyId, rateClass, DateTime.Today.Date, ref rateClassInfo);

            Assert.IsNotNull(result);
            Assert.AreEqual(rateClass, rateClassInfo.RateClassID);
            Assert.AreEqual(isTiered, rateClassInfo.IsTiered);
            Assert.AreEqual(isSeason, rateClassInfo.IsSeasonal);
            Assert.AreEqual(isTou, rateClassInfo.IsTimeOfUse);
            Assert.AreEqual(isPTR, rateClassInfo.ContainsPTR);
            Assert.AreEqual(commodity, rateClassInfo.CommodityKey);
            Assert.AreEqual(uom, rateClassInfo.UomKey);
            Assert.AreEqual(customerType, rateClassInfo.CustomerType.ToLower());
            Assert.AreEqual(3, result.UseCharges.Count());
            Assert.AreEqual(1, result.ServiceCharges.Count());
            Assert.AreEqual(2, result.TierBoundaries.Count());
            Assert.AreEqual(0, result.MinimumCharges.Count());
            Assert.AreEqual(0, result.TaxCharges.Count());
            Assert.AreEqual(0, result.SeasonBoundaries.Count());
            Assert.AreEqual(useCostType, result.UseCharges[0].CostKey);
            Assert.AreEqual(useCostType, result.UseCharges[1].CostKey);
            Assert.AreEqual(useCostType, result.UseCharges[2].CostKey);
            Assert.AreEqual(undefined, result.UseCharges[0].SeasonKey);
            Assert.AreEqual(undefined, result.UseCharges[1].SeasonKey);
            Assert.AreEqual(undefined, result.UseCharges[2].SeasonKey);
            Assert.AreEqual(partType, result.UseCharges[0].PartKey);
            Assert.AreEqual(partType, result.UseCharges[1].PartKey);
            Assert.AreEqual(partType, result.UseCharges[2].PartKey);
            Assert.AreEqual(tier1UseCharge, result.UseCharges[0].ChargeValue);
            Assert.AreEqual(tier2UseCharge, result.UseCharges[1].ChargeValue);
            Assert.AreEqual(tier3UseCharge, result.UseCharges[2].ChargeValue);
            Assert.AreEqual(serviceCharge, result.ServiceCharges[0].ChargeValue);
            Assert.AreEqual(serviceCostType, result.ServiceCharges[0].CostKey);
            Assert.AreEqual(undefined, result.ServiceCharges[0].RebateClassKey);
            Assert.AreEqual(tier1, result.TierBoundaries[0].BaseOrTierKey);
            Assert.AreEqual(tier2, result.TierBoundaries[1].BaseOrTierKey);
            Assert.AreEqual(tier1Threshold, result.TierBoundaries[0].Threshold);
            Assert.AreEqual(tier2Threshold, result.TierBoundaries[1].Threshold);
        }

        [TestMethod()]
        public void RateV1_GetDetailedRateInfo_TieredTOUSeasonal_Test()
        {
            var rateClass = "binTP3";
            var rateCompanyId = 100;
            RateClass rateClassInfo = null;

            // expectedResult
            var customerType = "residential";
            var commodity = "electric";
            var uom = "kwh";
            var isTiered = true;
            var isSeason = true;
            var isTou = true;
            var isPTR = false;
            var undefined = "Undefined";

            var model = new RateClassModel();

            var result = model.GetDetailedRateInfo(rateCompanyId, rateClass, DateTime.Today.Date, ref rateClassInfo);

            Assert.IsNotNull(result);
            Assert.AreEqual(rateClass, rateClassInfo.RateClassID);
            Assert.AreEqual(isTiered, rateClassInfo.IsTiered);
            Assert.AreEqual(isSeason, rateClassInfo.IsSeasonal);
            Assert.AreEqual(isTou, rateClassInfo.IsTimeOfUse);
            Assert.AreEqual(isPTR, rateClassInfo.ContainsPTR);
            Assert.AreEqual(commodity, rateClassInfo.CommodityKey);
            Assert.AreEqual(uom, rateClassInfo.UomKey);
            Assert.AreEqual(customerType, rateClassInfo.CustomerType.ToLower());
            Assert.AreEqual(17, result.UseCharges.Count());
            Assert.AreEqual(1, result.ServiceCharges.Count());
            Assert.AreEqual(12, result.TierBoundaries.Count());
            Assert.AreEqual(12, result.TouBoundaries.Count());
            Assert.AreEqual(0, result.MinimumCharges.Count());
            Assert.AreEqual(0, result.TaxCharges.Count());
            Assert.AreEqual(3, result.SeasonBoundaries.Count());

            Assert.AreEqual(6, result.UseCharges.Count(u => u.SeasonKey == "Summer"));
            Assert.AreEqual(10, result.UseCharges.Count(u => u.SeasonKey == "Winter"));
            Assert.AreEqual(5, result.UseCharges.Count(u => u.BaseOrTierKey == "Tier1"));
            Assert.AreEqual(5, result.UseCharges.Count(u => u.BaseOrTierKey == "Tier2"));
            Assert.AreEqual(5, result.UseCharges.Count(u => u.BaseOrTierKey == "Tier3"));
            Assert.AreEqual(6, result.UseCharges.Count(u => u.TimeOfUseKey == "OnPeak"));
            Assert.AreEqual(6, result.UseCharges.Count(u => u.TimeOfUseKey == "OffPeak"));
            Assert.AreEqual(5, result.UseCharges.Count(u => u.TimeOfUseKey == undefined));
            Assert.AreEqual(3, result.UseCharges.Count(u => u.SeasonKey == "Summer" && u.TimeOfUseKey == "OnPeak"));
            Assert.AreEqual(3, result.UseCharges.Count(u => u.SeasonKey == "Winter" && u.TimeOfUseKey == "OnPeak"));
            Assert.AreEqual(3, result.UseCharges.Count(u => u.SeasonKey == "Summer" && u.TimeOfUseKey == "OffPeak"));
            Assert.AreEqual(3, result.UseCharges.Count(u => u.SeasonKey == "Winter" && u.TimeOfUseKey == "OffPeak"));
            Assert.AreEqual(1, result.UseCharges.Count(u => u.BaseOrTierKey == "Tier1" && u.SeasonKey == "Winter" && u.TimeOfUseKey == "OffPeak"));
            Assert.AreEqual(1, result.UseCharges.Count(u => u.BaseOrTierKey == "Tier2" && u.SeasonKey == "Winter" && u.TimeOfUseKey == "OnPeak"));

            Assert.AreEqual(1, result.TierBoundaries.Count(t => t.BaseOrTierKey == "Tier1" && t.TimeOfUseKey == "OnPeak" && t.SeasonKey == "Summer"));
            Assert.AreEqual(1, result.TierBoundaries.Count(t => t.BaseOrTierKey == "Tier2" && t.TimeOfUseKey == "OnPeak" && t.SeasonKey == "Summer"));
            Assert.AreEqual(1, result.TierBoundaries.Count(t => t.BaseOrTierKey == "Tier1" && t.TimeOfUseKey == "OffPeak" && t.SeasonKey == "Summer"));
            Assert.AreEqual(1, result.TierBoundaries.Count(t => t.BaseOrTierKey == "Tier2" && t.TimeOfUseKey == "OffPeak" && t.SeasonKey == "Summer"));
            Assert.AreEqual(1, result.TierBoundaries.Count(t => t.BaseOrTierKey == "Tier1" && t.TimeOfUseKey == "OnPeak" && t.SeasonKey == "Winter"));
            Assert.AreEqual(1, result.TierBoundaries.Count(t => t.BaseOrTierKey == "Tier2" && t.TimeOfUseKey == "OnPeak" && t.SeasonKey == "Winter"));
            Assert.AreEqual(1, result.TierBoundaries.Count(t => t.BaseOrTierKey == "Tier1" && t.TimeOfUseKey == "OffPeak" && t.SeasonKey == "Winter"));
            Assert.AreEqual(1, result.TierBoundaries.Count(t => t.BaseOrTierKey == "Tier2" && t.TimeOfUseKey == "OffPeak" && t.SeasonKey == "Winter"));
            Assert.AreEqual(1, result.TierBoundaries.Count(t => t.BaseOrTierKey == "Tier1" && t.TimeOfUseKey == undefined && t.SeasonKey == "Summer"));
            Assert.AreEqual(1, result.TierBoundaries.Count(t => t.BaseOrTierKey == "Tier2" && t.TimeOfUseKey == undefined && t.SeasonKey == "Summer"));
            Assert.AreEqual(1, result.TierBoundaries.Count(t => t.BaseOrTierKey == "Tier1" && t.TimeOfUseKey == undefined && t.SeasonKey == "Winter"));
            Assert.AreEqual(1, result.TierBoundaries.Count(t => t.BaseOrTierKey == "Tier2" && t.TimeOfUseKey == undefined && t.SeasonKey == "Winter"));

            Assert.AreEqual(2, result.TouBoundaries.Count(t => t.TimeOfUseKey == "OffPeak" && t.SeasonKey == "Summer" && t.DayKey == "Weekday"));
            Assert.AreEqual(2, result.TouBoundaries.Count(t => t.TimeOfUseKey == "OffPeak" && t.SeasonKey == "Summer" && t.DayKey == "WeekendHoliday"));
            Assert.AreEqual(1, result.TouBoundaries.Count(t => t.TimeOfUseKey == "OnPeak" && t.SeasonKey == "Summer" && t.DayKey == "Weekday"));
            Assert.AreEqual(1, result.TouBoundaries.Count(t => t.TimeOfUseKey == "OnPeak" && t.SeasonKey == "Summer" && t.DayKey == "WeekendHoliday"));
            Assert.AreEqual(2, result.TouBoundaries.Count(t => t.TimeOfUseKey == "OffPeak" && t.SeasonKey == "Winter" && t.DayKey == "Weekday"));
            Assert.AreEqual(2, result.TouBoundaries.Count(t => t.TimeOfUseKey == "OffPeak" && t.SeasonKey == "Winter" && t.DayKey == "WeekendHoliday"));
            Assert.AreEqual(1, result.TouBoundaries.Count(t => t.TimeOfUseKey == "OnPeak" && t.SeasonKey == "Winter" && t.DayKey == "Weekday"));
            Assert.AreEqual(1, result.TouBoundaries.Count(t => t.TimeOfUseKey == "OnPeak" && t.SeasonKey == "Winter" && t.DayKey == "WeekendHoliday"));

            Assert.AreEqual(2, result.SeasonBoundaries.Count(s => s.SeasonKey == "Winter"));
            Assert.AreEqual(1, result.SeasonBoundaries.Count(s => s.SeasonKey == "Summer"));
        }

        [TestMethod()]
        public void RateV1_FinalizeTierBoundaries_Date_Test()
        {
            var rateClass = "binTP3";
            var rateCompanyId = 100;

            var model = new RateClassModel();

            var result = model.FinalizeTierBoundaries(rateCompanyId, rateClass, DateTime.Today.Date, DateTime.MinValue, DateTime.MinValue, DateTime.MinValue, false);

            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count);
        }


        [TestMethod()]
        public void RateV1_FinalizeTierBoundaries_DateRange_Test()
        {
            var rateClass = "binTP3";
            var rateCompanyId = 100;
            var season = "Summer";

            var model = new RateClassModel();

            var result = model.FinalizeTierBoundaries(rateCompanyId, rateClass, DateTime.Today.Date, Convert.ToDateTime("5/1/2015"), Convert.ToDateTime("5/15/2015"), DateTime.MinValue, false);

            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count);
            Assert.AreEqual(season, result[0].SeasonKey);
        }

        [TestMethod()]
        public void RateV1_GetRate_Basic_Test()
        {
            var clientId = 87;
            var rateClass = "binTP3";
            var rateCompanyId = 100;
            var date = DateTime.Today.Date;
            //var season = "Summer";

            // expectedResult
            var customerType = "residential";
            var commodity = "electric";
            var uom = "kwh";
            var isTiered = true;
            var isSeason = true;
            var isTou = true;
            var isPTR = false;
            //var undefined = "Undefined";


            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new System.Collections.Generic.List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                },
                RateCompanyID = rateCompanyId
            };

            var request = new CE.Models.Insights.RateRequest {
                RateClass = rateClass,
                Date = date                
            };
            var model = new RateClassModel();

            var result = model.GetRate(clientUser, request);

            Assert.IsNotNull(result);
            Assert.IsNull(result.Details);
            Assert.IsNotNull(result.RateClass);
            Assert.AreEqual(commodity, result.RateClass.CommodityKey);
            Assert.AreEqual(customerType, result.RateClass.CustomerType.ToLower());
            Assert.AreEqual(isSeason, result.RateClass.IsSeasonal);
            Assert.AreEqual(isTiered, result.RateClass.IsTiered);
            Assert.AreEqual(isTou, result.RateClass.IsTimeOfUse);
            Assert.AreEqual(rateClass, result.RateClass.RateClassID);
            Assert.AreEqual(uom, result.RateClass.UomKey);
            Assert.AreEqual(isPTR, result.RateClass.ContainsPTR);
        }

        [TestMethod()]
        public void RateV1_GetRate_Basic_NotExist_Test()
        {
            var clientId = 87;
            var rateClass = "InvalidRateClass";
            var rateCompanyId = 100;
            var date = DateTime.Today.Date;


            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new System.Collections.Generic.List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                },
                RateCompanyID = rateCompanyId
            };

            var request = new CE.Models.Insights.RateRequest
            {
                RateClass = rateClass,
                Date = date
            };
            var model = new RateClassModel();

            var result = model.GetRate(clientUser, request);

            Assert.IsNotNull(result);
            Assert.AreEqual("No data found", result.Message);
        }


        [TestMethod()]
        public void RateV1_GetRate_details_Test()
        {
            var clientId = 87;
            var rateClass = "binTP3";
            var rateCompanyId = 100;
            var date = DateTime.Today.Date;
            //var season = "Summer";

            // expectedResult
            var customerType = "residential";
            var commodity = "electric";
            var uom = "kwh";
            var isTiered = true;
            var isSeason = true;
            var isTou = true;
            var isPTR = false;
            //var undefined = "Undefined";


            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new System.Collections.Generic.List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                },
                RateCompanyID = rateCompanyId
            };

            var request = new CE.Models.Insights.RateRequest
            {
                RateClass = rateClass,
                Date = date,
                IncludeRateDetails = true
            };
            var model = new RateClassModel();

            var result = model.GetRate(clientUser, request);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Details);
            Assert.IsNotNull(result.RateClass);
            Assert.IsNull(result.Details.FinalizedTierBoundaries);
            Assert.AreEqual(commodity, result.RateClass.CommodityKey);
            Assert.AreEqual(customerType, result.RateClass.CustomerType.ToLower());
            Assert.AreEqual(isSeason, result.RateClass.IsSeasonal);
            Assert.AreEqual(isTiered, result.RateClass.IsTiered);
            Assert.AreEqual(isTou, result.RateClass.IsTimeOfUse);
            Assert.AreEqual(rateClass, result.RateClass.RateClassID);
            Assert.AreEqual(uom, result.RateClass.UomKey);
            Assert.AreEqual(isPTR, result.RateClass.ContainsPTR);
            Assert.AreEqual(17, result.Details.UseCharges.Count());
            Assert.AreEqual(1, result.Details.ServiceCharges.Count());
            Assert.AreEqual(12, result.Details.TierBoundaries.Count());
            Assert.AreEqual(12, result.Details.TouBoundaries.Count());
            Assert.AreEqual(0, result.Details.MinimumCharges.Count());
            Assert.AreEqual(0, result.Details.TaxCharges.Count());
            Assert.AreEqual(3, result.Details.SeasonBoundaries.Count());
        }

        [TestMethod()]
        public void RateV1_GetRate_Detail_NotExist_Test()
        {
            var clientId = 87;
            var rateClass = "InvalidRateClass";
            var rateCompanyId = 100;
            var date = DateTime.Today.Date;


            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new System.Collections.Generic.List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                },
                RateCompanyID = rateCompanyId
            };

            var request = new CE.Models.Insights.RateRequest
            {
                RateClass = rateClass,
                Date = date,
                IncludeRateDetails = true
            };
            var model = new RateClassModel();

            var result = model.GetRate(clientUser, request);

            Assert.IsNotNull(result);
            Assert.AreEqual("No data found", result.Message);
        }

        [TestMethod()]
        public void RateV1_GetRate_details_date_startDate_Test()
        {
            var clientId = 87;
            var rateClass = "binTP3";
            var rateCompanyId = 100;
            var date = DateTime.Today.Date;

            // expectedResult
            var customerType = "residential";
            var commodity = "electric";
            var uom = "kwh";
            var isTiered = true;
            var isSeason = true;
            var isTou = true;
            var isPTR = false;


            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new System.Collections.Generic.List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                },
                RateCompanyID = rateCompanyId
            };

            var request = new CE.Models.Insights.RateRequest
            {
                RateClass = rateClass,
                Date = date,
                IncludeRateDetails = true,
                StartDate = Convert.ToDateTime("5/1/2015")
            };
            var model = new RateClassModel();

            var result = model.GetRate(clientUser, request);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Details);
            Assert.IsNotNull(result.RateClass);
            Assert.IsNull(result.Details.FinalizedTierBoundaries);
            Assert.AreEqual(commodity, result.RateClass.CommodityKey);
            Assert.AreEqual(customerType, result.RateClass.CustomerType.ToLower());
            Assert.AreEqual(isSeason, result.RateClass.IsSeasonal);
            Assert.AreEqual(isTiered, result.RateClass.IsTiered);
            Assert.AreEqual(isTou, result.RateClass.IsTimeOfUse);
            Assert.AreEqual(rateClass, result.RateClass.RateClassID);
            Assert.AreEqual(uom, result.RateClass.UomKey);
            Assert.AreEqual(isPTR, result.RateClass.ContainsPTR);
            Assert.AreEqual(17, result.Details.UseCharges.Count());
            Assert.AreEqual(1, result.Details.ServiceCharges.Count());
            Assert.AreEqual(12, result.Details.TierBoundaries.Count());
            Assert.AreEqual(12, result.Details.TouBoundaries.Count());
            Assert.AreEqual(0, result.Details.MinimumCharges.Count());
            Assert.AreEqual(0, result.Details.TaxCharges.Count());
            Assert.AreEqual(3, result.Details.SeasonBoundaries.Count());
        }

        [TestMethod()]
        public void RateV1_GetRate_details_date_endDate_Test()
        {
            var clientId = 87;
            var rateClass = "binTP3";
            var rateCompanyId = 100;
            var date = DateTime.Today.Date;

            // expectedResult
            var customerType = "residential";
            var commodity = "electric";
            var uom = "kwh";
            var isTiered = true;
            var isSeason = true;
            var isTou = true;
            var isPTR = false;


            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new System.Collections.Generic.List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                },
                RateCompanyID = rateCompanyId
            };

            var request = new CE.Models.Insights.RateRequest
            {
                RateClass = rateClass,
                Date = date,
                IncludeRateDetails = true,
                EndDate = Convert.ToDateTime("5/15/2015")
            };
            var model = new RateClassModel();

            var result = model.GetRate(clientUser, request);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Details);
            Assert.IsNotNull(result.RateClass);
            Assert.IsNull(result.Details.FinalizedTierBoundaries);
            Assert.AreEqual(commodity, result.RateClass.CommodityKey);
            Assert.AreEqual(customerType, result.RateClass.CustomerType.ToLower());
            Assert.AreEqual(isSeason, result.RateClass.IsSeasonal);
            Assert.AreEqual(isTiered, result.RateClass.IsTiered);
            Assert.AreEqual(isTou, result.RateClass.IsTimeOfUse);
            Assert.AreEqual(rateClass, result.RateClass.RateClassID);
            Assert.AreEqual(uom, result.RateClass.UomKey);
            Assert.AreEqual(isPTR, result.RateClass.ContainsPTR);
            Assert.AreEqual(17, result.Details.UseCharges.Count());
            Assert.AreEqual(1, result.Details.ServiceCharges.Count());
            Assert.AreEqual(12, result.Details.TierBoundaries.Count());
            Assert.AreEqual(12, result.Details.TouBoundaries.Count());
            Assert.AreEqual(0, result.Details.MinimumCharges.Count());
            Assert.AreEqual(0, result.Details.TaxCharges.Count());
            Assert.AreEqual(3, result.Details.SeasonBoundaries.Count());
        }

        [TestMethod()]
        public void RateV1_GetRate_details_date_startEndDate_Test()
        {
            var clientId = 87;
            var rateClass = "binTP3";
            var rateCompanyId = 100;
            var date = DateTime.Today.Date;

            // expectedResult
            var customerType = "residential";
            var commodity = "electric";
            var uom = "kwh";
            var isTiered = true;
            var isSeason = true;
            var isTou = true;
            var isPTR = false;


            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new System.Collections.Generic.List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                },
                RateCompanyID = rateCompanyId
            };

            var request = new CE.Models.Insights.RateRequest
            {
                RateClass = rateClass,
                Date = date,
                IncludeRateDetails = true,
                StartDate = Convert.ToDateTime("5/1/2015"),
                EndDate = Convert.ToDateTime("5/15/2015")
            };
            var model = new RateClassModel();

            var result = model.GetRate(clientUser, request);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Details);
            Assert.IsNotNull(result.RateClass);
            Assert.IsNull(result.Details.FinalizedTierBoundaries);
            Assert.AreEqual(commodity, result.RateClass.CommodityKey);
            Assert.AreEqual(customerType, result.RateClass.CustomerType.ToLower());
            Assert.AreEqual(isSeason, result.RateClass.IsSeasonal);
            Assert.AreEqual(isTiered, result.RateClass.IsTiered);
            Assert.AreEqual(isTou, result.RateClass.IsTimeOfUse);
            Assert.AreEqual(rateClass, result.RateClass.RateClassID);
            Assert.AreEqual(uom, result.RateClass.UomKey);
            Assert.AreEqual(isPTR, result.RateClass.ContainsPTR);
            Assert.AreEqual(17, result.Details.UseCharges.Count());
            Assert.AreEqual(1, result.Details.ServiceCharges.Count());
            Assert.AreEqual(12, result.Details.TierBoundaries.Count());
            Assert.AreEqual(12, result.Details.TouBoundaries.Count());
            Assert.AreEqual(0, result.Details.MinimumCharges.Count());
            Assert.AreEqual(0, result.Details.TaxCharges.Count());
            Assert.AreEqual(3, result.Details.SeasonBoundaries.Count());
        }

        [TestMethod()]
        public void RateV1_GetRate_details_date_startEndProjectedDate_Test()
        {
            var clientId = 87;
            var rateClass = "binTP3";
            var rateCompanyId = 100;
            var date = DateTime.Today.Date;

            // expectedResult
            var customerType = "residential";
            var commodity = "electric";
            var uom = "kwh";
            var isTiered = true;
            var isSeason = true;
            var isTou = true;
            var isPTR = false;


            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new System.Collections.Generic.List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                },
                RateCompanyID = rateCompanyId
            };

            var request = new CE.Models.Insights.RateRequest
            {
                RateClass = rateClass,
                Date = date,
                IncludeRateDetails = true,
                StartDate = Convert.ToDateTime("5/1/2015"),
                EndDate = Convert.ToDateTime("5/15/2015"),
                ProjectedEndDate = Convert.ToDateTime("5/25/2015")
            };
            var model = new RateClassModel();

            var result = model.GetRate(clientUser, request);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Details);
            Assert.IsNotNull(result.RateClass);
            Assert.IsNull(result.Details.FinalizedTierBoundaries);
            Assert.AreEqual(commodity, result.RateClass.CommodityKey);
            Assert.AreEqual(customerType, result.RateClass.CustomerType.ToLower());
            Assert.AreEqual(isSeason, result.RateClass.IsSeasonal);
            Assert.AreEqual(isTiered, result.RateClass.IsTiered);
            Assert.AreEqual(isTou, result.RateClass.IsTimeOfUse);
            Assert.AreEqual(rateClass, result.RateClass.RateClassID);
            Assert.AreEqual(uom, result.RateClass.UomKey);
            Assert.AreEqual(isPTR, result.RateClass.ContainsPTR);
            Assert.AreEqual(17, result.Details.UseCharges.Count());
            Assert.AreEqual(1, result.Details.ServiceCharges.Count());
            Assert.AreEqual(12, result.Details.TierBoundaries.Count());
            Assert.AreEqual(12, result.Details.TouBoundaries.Count());
            Assert.AreEqual(0, result.Details.MinimumCharges.Count());
            Assert.AreEqual(0, result.Details.TaxCharges.Count());
            Assert.AreEqual(3, result.Details.SeasonBoundaries.Count());
        }

        [TestMethod()]
        public void RateV1_GetRate_details_nofinal_date_startEndProjectedDate_Test()
        {
            var clientId = 87;
            var rateClass = "binTP3";
            var rateCompanyId = 100;
            var date = DateTime.Today.Date;

            // expectedResult
            var customerType = "residential";
            var commodity = "electric";
            var uom = "kwh";
            var isTiered = true;
            var isSeason = true;
            var isTou = true;
            var isPTR = false;


            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new System.Collections.Generic.List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                },
                RateCompanyID = rateCompanyId
            };

            var request = new CE.Models.Insights.RateRequest
            {
                RateClass = rateClass,
                Date = date,
                IncludeRateDetails = true,
                StartDate = Convert.ToDateTime("5/1/2015"),
                EndDate = Convert.ToDateTime("5/15/2015"),
                ProjectedEndDate = Convert.ToDateTime("5/25/2015"),
                UseProjectedEndDateToFinalizeTier = true
            };
            var model = new RateClassModel();

            var result = model.GetRate(clientUser, request);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Details);
            Assert.IsNotNull(result.RateClass);
            Assert.IsNull(result.Details.FinalizedTierBoundaries);
            Assert.AreEqual(commodity, result.RateClass.CommodityKey);
            Assert.AreEqual(customerType, result.RateClass.CustomerType.ToLower());
            Assert.AreEqual(isSeason, result.RateClass.IsSeasonal);
            Assert.AreEqual(isTiered, result.RateClass.IsTiered);
            Assert.AreEqual(isTou, result.RateClass.IsTimeOfUse);
            Assert.AreEqual(rateClass, result.RateClass.RateClassID);
            Assert.AreEqual(uom, result.RateClass.UomKey);
            Assert.AreEqual(isPTR, result.RateClass.ContainsPTR);
            Assert.AreEqual(17, result.Details.UseCharges.Count());
            Assert.AreEqual(1, result.Details.ServiceCharges.Count());
            Assert.AreEqual(12, result.Details.TierBoundaries.Count());
            Assert.AreEqual(12, result.Details.TouBoundaries.Count());
            Assert.AreEqual(0, result.Details.MinimumCharges.Count());
            Assert.AreEqual(0, result.Details.TaxCharges.Count());
            Assert.AreEqual(3, result.Details.SeasonBoundaries.Count());
        }

        [TestMethod()]
        public void RateV1_GetRate_final_Test()
        {
            var clientId = 87;
            var rateClass = "binTP3";
            var rateCompanyId = 100;
            var date = DateTime.Today.Date;

            // expectedResult
            var customerType = "residential";
            var commodity = "electric";
            var uom = "kwh";
            var isTiered = true;
            var isSeason = true;
            var isTou = true;
            var isPTR = false;


            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new System.Collections.Generic.List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                },
                RateCompanyID = rateCompanyId
            };

            var request = new CE.Models.Insights.RateRequest
            {
                RateClass = rateClass,
                Date = date,
                IncludeRateDetails = true,
                IncludeFinalizedTierBoundaries = true
            };
            var model = new RateClassModel();

            var result = model.GetRate(clientUser, request);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Details);
            Assert.IsNotNull(result.RateClass);
            Assert.IsNotNull(result.Details.FinalizedTierBoundaries);
            Assert.AreEqual(commodity, result.RateClass.CommodityKey);
            Assert.AreEqual(customerType, result.RateClass.CustomerType.ToLower());
            Assert.AreEqual(isSeason, result.RateClass.IsSeasonal);
            Assert.AreEqual(isTiered, result.RateClass.IsTiered);
            Assert.AreEqual(isTou, result.RateClass.IsTimeOfUse);
            Assert.AreEqual(rateClass, result.RateClass.RateClassID);
            Assert.AreEqual(uom, result.RateClass.UomKey);
            Assert.AreEqual(isPTR, result.RateClass.ContainsPTR);
            Assert.AreEqual(17, result.Details.UseCharges.Count());
            Assert.AreEqual(1, result.Details.ServiceCharges.Count());
            Assert.AreEqual(12, result.Details.TierBoundaries.Count());
            Assert.AreEqual(12, result.Details.TouBoundaries.Count());
            Assert.AreEqual(0, result.Details.MinimumCharges.Count());
            Assert.AreEqual(0, result.Details.TaxCharges.Count());
            Assert.AreEqual(3, result.Details.SeasonBoundaries.Count());
            Assert.AreEqual(2, result.Details.FinalizedTierBoundaries.Count);
        }

        [TestMethod()]
        public void RateV1_GetRate_final_date_startDate_Test()
        {
            var clientId = 87;
            var rateClass = "binTP3";
            var rateCompanyId = 100;
            var date = DateTime.Today.Date;

            // expectedResult
            var customerType = "residential";
            var commodity = "electric";
            var uom = "kwh";
            var isTiered = true;
            var isSeason = true;
            var isTou = true;
            var isPTR = false;


            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new System.Collections.Generic.List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                },
                RateCompanyID = rateCompanyId
            };

            var request = new CE.Models.Insights.RateRequest
            {
                RateClass = rateClass,
                Date = date,
                IncludeRateDetails = true,
                StartDate = Convert.ToDateTime("5/1/2015"),
                IncludeFinalizedTierBoundaries = true
            };
            var model = new RateClassModel();

            var result = model.GetRate(clientUser, request);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Details);
            Assert.IsNotNull(result.RateClass);
            Assert.IsNotNull(result.Details.FinalizedTierBoundaries);
            Assert.AreEqual(commodity, result.RateClass.CommodityKey);
            Assert.AreEqual(customerType, result.RateClass.CustomerType.ToLower());
            Assert.AreEqual(isSeason, result.RateClass.IsSeasonal);
            Assert.AreEqual(isTiered, result.RateClass.IsTiered);
            Assert.AreEqual(isTou, result.RateClass.IsTimeOfUse);
            Assert.AreEqual(rateClass, result.RateClass.RateClassID);
            Assert.AreEqual(uom, result.RateClass.UomKey);
            Assert.AreEqual(isPTR, result.RateClass.ContainsPTR);
            Assert.AreEqual(17, result.Details.UseCharges.Count());
            Assert.AreEqual(1, result.Details.ServiceCharges.Count());
            Assert.AreEqual(12, result.Details.TierBoundaries.Count());
            Assert.AreEqual(12, result.Details.TouBoundaries.Count());
            Assert.AreEqual(0, result.Details.MinimumCharges.Count());
            Assert.AreEqual(0, result.Details.TaxCharges.Count());
            Assert.AreEqual(3, result.Details.SeasonBoundaries.Count());
            Assert.AreEqual(2, result.Details.FinalizedTierBoundaries.Count);

        }

        [TestMethod()]
        public void RateV1_GetRate_final_date_endDate_Test()
        {
            var clientId = 87;
            var rateClass = "binTP3";
            var rateCompanyId = 100;
            var date = DateTime.Today.Date;

            // expectedResult
            var customerType = "residential";
            var commodity = "electric";
            var uom = "kwh";
            var isTiered = true;
            var isSeason = true;
            var isTou = true;
            var isPTR = false;


            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new System.Collections.Generic.List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                },
                RateCompanyID = rateCompanyId
            };

            var request = new CE.Models.Insights.RateRequest
            {
                RateClass = rateClass,
                Date = date,
                IncludeRateDetails = true,
                EndDate = Convert.ToDateTime("5/15/2015"),
                IncludeFinalizedTierBoundaries = true
            };
            var model = new RateClassModel();

            var result = model.GetRate(clientUser, request);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Details);
            Assert.IsNotNull(result.RateClass);
            Assert.IsNotNull(result.Details.FinalizedTierBoundaries);
            Assert.AreEqual(commodity, result.RateClass.CommodityKey);
            Assert.AreEqual(customerType, result.RateClass.CustomerType.ToLower());
            Assert.AreEqual(isSeason, result.RateClass.IsSeasonal);
            Assert.AreEqual(isTiered, result.RateClass.IsTiered);
            Assert.AreEqual(isTou, result.RateClass.IsTimeOfUse);
            Assert.AreEqual(rateClass, result.RateClass.RateClassID);
            Assert.AreEqual(uom, result.RateClass.UomKey);
            Assert.AreEqual(isPTR, result.RateClass.ContainsPTR);
            Assert.AreEqual(17, result.Details.UseCharges.Count());
            Assert.AreEqual(1, result.Details.ServiceCharges.Count());
            Assert.AreEqual(12, result.Details.TierBoundaries.Count());
            Assert.AreEqual(12, result.Details.TouBoundaries.Count());
            Assert.AreEqual(0, result.Details.MinimumCharges.Count());
            Assert.AreEqual(0, result.Details.TaxCharges.Count());
            Assert.AreEqual(3, result.Details.SeasonBoundaries.Count());
            Assert.AreEqual(2, result.Details.FinalizedTierBoundaries.Count);
        }

        [TestMethod()]
        public void RateV1_GetRate_final_date_startEndDate_Test()
        {
            var clientId = 87;
            var rateClass = "binTP3";
            var rateCompanyId = 100;
            var date = DateTime.Today.Date;

            // expectedResult
            var customerType = "residential";
            var commodity = "electric";
            var uom = "kwh";
            var isTiered = true;
            var isSeason = true;
            var isTou = true;
            var isPTR = false;


            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new System.Collections.Generic.List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                },
                RateCompanyID = rateCompanyId
            };

            var request = new CE.Models.Insights.RateRequest
            {
                RateClass = rateClass,
                Date = date,
                IncludeRateDetails = true,
                StartDate = Convert.ToDateTime("5/1/2015"),
                EndDate = Convert.ToDateTime("5/15/2015"),
                IncludeFinalizedTierBoundaries = true
            };
            var model = new RateClassModel();

            var result = model.GetRate(clientUser, request);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Details);
            Assert.IsNotNull(result.RateClass);
            Assert.IsNotNull(result.Details.FinalizedTierBoundaries);
            Assert.AreEqual(commodity, result.RateClass.CommodityKey);
            Assert.AreEqual(customerType, result.RateClass.CustomerType.ToLower());
            Assert.AreEqual(isSeason, result.RateClass.IsSeasonal);
            Assert.AreEqual(isTiered, result.RateClass.IsTiered);
            Assert.AreEqual(isTou, result.RateClass.IsTimeOfUse);
            Assert.AreEqual(rateClass, result.RateClass.RateClassID);
            Assert.AreEqual(uom, result.RateClass.UomKey);
            Assert.AreEqual(isPTR, result.RateClass.ContainsPTR);
            Assert.AreEqual(17, result.Details.UseCharges.Count());
            Assert.AreEqual(1, result.Details.ServiceCharges.Count());
            Assert.AreEqual(12, result.Details.TierBoundaries.Count());
            Assert.AreEqual(12, result.Details.TouBoundaries.Count());
            Assert.AreEqual(0, result.Details.MinimumCharges.Count());
            Assert.AreEqual(0, result.Details.TaxCharges.Count());
            Assert.AreEqual(3, result.Details.SeasonBoundaries.Count());
        }

        [TestMethod()]
        public void RateV1_GetRate_final_date_startEndProjectedDate_Test()
        {
            var clientId = 87;
            var rateClass = "binTP3";
            var rateCompanyId = 100;
            var date = DateTime.Today.Date;

            // expectedResult
            var customerType = "residential";
            var commodity = "electric";
            var uom = "kwh";
            var isTiered = true;
            var isSeason = true;
            var isTou = true;
            var isPTR = false;


            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new System.Collections.Generic.List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                },
                RateCompanyID = rateCompanyId
            };

            var request = new CE.Models.Insights.RateRequest
            {
                RateClass = rateClass,
                Date = date,
                IncludeRateDetails = true,
                StartDate = Convert.ToDateTime("5/1/2015"),
                EndDate = Convert.ToDateTime("5/15/2015"),
                ProjectedEndDate = Convert.ToDateTime("5/25/2015"),
                IncludeFinalizedTierBoundaries = true
            };
            var model = new RateClassModel();

            var result = model.GetRate(clientUser, request);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Details);
            Assert.IsNotNull(result.RateClass);
            Assert.IsNotNull(result.Details.FinalizedTierBoundaries);
            Assert.AreEqual(commodity, result.RateClass.CommodityKey);
            Assert.AreEqual(customerType, result.RateClass.CustomerType.ToLower());
            Assert.AreEqual(isSeason, result.RateClass.IsSeasonal);
            Assert.AreEqual(isTiered, result.RateClass.IsTiered);
            Assert.AreEqual(isTou, result.RateClass.IsTimeOfUse);
            Assert.AreEqual(rateClass, result.RateClass.RateClassID);
            Assert.AreEqual(uom, result.RateClass.UomKey);
            Assert.AreEqual(isPTR, result.RateClass.ContainsPTR);
            Assert.AreEqual(17, result.Details.UseCharges.Count());
            Assert.AreEqual(1, result.Details.ServiceCharges.Count());
            Assert.AreEqual(12, result.Details.TierBoundaries.Count());
            Assert.AreEqual(12, result.Details.TouBoundaries.Count());
            Assert.AreEqual(0, result.Details.MinimumCharges.Count());
            Assert.AreEqual(0, result.Details.TaxCharges.Count());
            Assert.AreEqual(3, result.Details.SeasonBoundaries.Count());
            Assert.AreEqual(2, result.Details.FinalizedTierBoundaries.Count);
        }

        [TestMethod()]
        public void RateV1_GetRate_final_useProjected_date_startEndProjectedDate_Test()
        {
            var clientId = 87;
            var rateClass = "binTP3";
            var rateCompanyId = 100;
            var date = DateTime.Today.Date;

            // expectedResult
            var customerType = "residential";
            var commodity = "electric";
            var uom = "kwh";
            var isTiered = true;
            var isSeason = true;
            var isTou = true;
            var isPTR = false;


            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new System.Collections.Generic.List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                },
                RateCompanyID = rateCompanyId
            };

            var request = new CE.Models.Insights.RateRequest
            {
                RateClass = rateClass,
                Date = date,
                IncludeRateDetails = true,
                StartDate = Convert.ToDateTime("5/1/2015"),
                EndDate = Convert.ToDateTime("5/15/2015"),
                ProjectedEndDate = Convert.ToDateTime("5/25/2015"),
                IncludeFinalizedTierBoundaries = true,
                UseProjectedEndDateToFinalizeTier = true
            };
            var model = new RateClassModel();

            var result = model.GetRate(clientUser, request);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Details);
            Assert.IsNotNull(result.RateClass);
            Assert.IsNotNull(result.Details.FinalizedTierBoundaries);
            Assert.AreEqual(commodity, result.RateClass.CommodityKey);
            Assert.AreEqual(customerType, result.RateClass.CustomerType.ToLower());
            Assert.AreEqual(isSeason, result.RateClass.IsSeasonal);
            Assert.AreEqual(isTiered, result.RateClass.IsTiered);
            Assert.AreEqual(isTou, result.RateClass.IsTimeOfUse);
            Assert.AreEqual(rateClass, result.RateClass.RateClassID);
            Assert.AreEqual(uom, result.RateClass.UomKey);
            Assert.AreEqual(isPTR, result.RateClass.ContainsPTR);
            Assert.AreEqual(17, result.Details.UseCharges.Count());
            Assert.AreEqual(1, result.Details.ServiceCharges.Count());
            Assert.AreEqual(12, result.Details.TierBoundaries.Count());
            Assert.AreEqual(12, result.Details.TouBoundaries.Count());
            Assert.AreEqual(0, result.Details.MinimumCharges.Count());
            Assert.AreEqual(0, result.Details.TaxCharges.Count());
            Assert.AreEqual(3, result.Details.SeasonBoundaries.Count());
            Assert.AreEqual(2, result.Details.FinalizedTierBoundaries.Count);
        }

        [TestMethod()]
        public void RateV1_GetRate_final_useProjected_date_startEndDate_Test()
        {
            var clientId = 87;
            var rateClass = "binTP3";
            var rateCompanyId = 100;
            var date = DateTime.Today.Date;


            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new System.Collections.Generic.List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                },
                RateCompanyID = rateCompanyId
            };

            var request = new CE.Models.Insights.RateRequest
            {
                RateClass = rateClass,
                Date = date,
                IncludeRateDetails = true,
                StartDate = Convert.ToDateTime("5/1/2015"),
                EndDate = Convert.ToDateTime("5/15/2015"),
                IncludeFinalizedTierBoundaries = true,
                UseProjectedEndDateToFinalizeTier = true
            };
            var model = new RateClassModel();

            var result = model.GetRate(clientUser, request);

            Assert.IsNotNull(result);
            Assert.IsNull(result.Details);
            Assert.IsNull(result.RateClass);
            Assert.AreEqual(CEConfiguration.Required_Start_End_ProjectedDate, result.Message);
        }
    }
}