﻿using System;
using System.Collections.Generic;
//using System.IO; // used for 
using CE.Insights.Models;
using CE.Insights.Test.Helpers;
using CE.Models;
using CE.Models.Insights;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;


namespace CE.Insights.Test.Models
{
    [TestClass()]
    public class GreenButtonBillModelTests
    {
        #region Constants for content
        private const string ClientSpace = "netnln2itvho";
        private const string ClientAccessToken = "c58ec8d495044174267c257f105914f7fb9567127a879b2419e1c3d405a54be2";

        #endregion
        private Mock<IInsightsEFRepository> _insightRepository;

        [TestInitialize]
        public void TestInit()
        {
            _insightRepository = new Mock<IInsightsEFRepository>();
        }
        
        [TestMethod, TestCategory("GreenButtonBill"), TestCategory("BVT")]
        public void GetGreenButtonBill_Zip_Test()
        {
            var clientId = 87;
            var customerId = "GB001";
            var accountId = "GB001a1";
            var premiseId = "GB01a1p1";
            var serviceId = "GB01a1p1s1";
            var serviceId2 = "GB01a1p1s2";
            var rateClass = "RG-1";
            var rateClass2 = "RTGas";
            var uomKey = "kwh";
            var commodityKey = "electric";
            var commodityKey2 = "gas";
            var uomKey2 = "ccf";
            var addr1 = "16 Laurel Ave";
            var city = "Wellesley";
            var state = "MA";
            var zip = "02481";
            var billDays = 30;
            var firstname = "TestFirst";
            var lastname = "TestLast";

            //add property for client user, normally from the stored procedure but hardcoded here for unit test purposes
            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };


            var config = TestGreenButtonHelper.GetConfigSetting();

            var gbBillModel = new GreenButtonBillModel(clientId, config, _insightRepository.Object);
            

            var mockData = TestGreenButtonHelper.GetMockMultipleServiceSinglePremiseSingleAccountData(clientId, customerId, accountId,
               premiseId, serviceId, serviceId2, commodityKey, commodityKey2, uomKey, uomKey2, billDays, rateClass, rateClass2, addr1, city, state, zip, firstname,
               lastname);
            _insightRepository.Setup(c => c.GetBills(clientId, It.IsAny<BillRequest>())).Returns(mockData);

            var gbBillRequest = new GreenButtonBillRequest
            {
                CustomerId = customerId,
                OutputFormat = "Zip"
            };

            var result = gbBillModel.GetGreenButtonBill(clientUser, gbBillRequest);
            

            Assert.IsNotNull(result);
            Assert.IsNotNull("zip", result.FileType.ToLower());
            Assert.IsNotNull(result.File);

            // comment out the following, so the build agent will work
            //using (
            //    var fileStream = new FileStream("C:\\" + result.FileName + ".zip",
            //        FileMode.Create))
            //{
            //    result.File.Seek(0, SeekOrigin.Begin);
            //    result.File.CopyTo(fileStream);
            //}
            //Assert.IsTrue(File.Exists("C:\\" + result.FileName + ".zip"));

            //File.Delete("C:\\" + result.FileName + ".zip");
        }

        [TestMethod()]
        public void GetGreenButtonBill_invalidOutputFormatRequest_Test()
        {
            var clientId = 87;
            var customerId = "GB001";
            var accountId = "GB001a1";
            var premiseId = "GB01a1p1";
            var serviceId = "GB01a1p1s1";
            var serviceId2 = "GB01a1p1s2";
            var rateClass = "RG-1";
            var rateClass2 = "RTGas";
            var uomKey = "kwh";
            var commodityKey = "electric";
            var commodityKey2 = "gas";
            var uomKey2 = "ccf";
            var addr1 = "16 Laurel Ave";
            var city = "Wellesley";
            var state = "MA";
            var zip = "02481";
            var billDays = 30;
            var firstname = "TestFirst";
            var lastname = "TestLast";

            //add property for client user, normally from the stored procedure but hardcoded here for unit test purposes
            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };


            var config = TestGreenButtonHelper.GetConfigSetting();

            var gbBillModel = new GreenButtonBillModel(clientId, config, _insightRepository.Object);


            var mockData = TestGreenButtonHelper.GetMockMultipleServiceSinglePremiseSingleAccountData(clientId, customerId, accountId,
               premiseId, serviceId, serviceId2, commodityKey, commodityKey2, uomKey, uomKey2, billDays, rateClass, rateClass2, addr1, city, state, zip, firstname,
               lastname);
            _insightRepository.Setup(c => c.GetBills(clientId, It.IsAny<BillRequest>())).Returns(mockData);

            var gbBillRequest = new GreenButtonBillRequest
            {
                CustomerId = customerId,
                OutputFormat = "Xml"
            };

            var result = gbBillModel.GetGreenButtonBill(clientUser, gbBillRequest);


            Assert.IsNotNull(result);
            Assert.IsNotNull(result.ErrorMessage);
            Assert.AreEqual("AccountId/PremiseId/ServiceContractId is required for xml output format", result.ErrorMessage);
        }

        [TestMethod, TestCategory("GreenButtonBill"), TestCategory("BVT")]
        public void GetGreenButtonBill_Xml_Test()
        {
            var clientId = 87;
            var customerId = "GB001";
            var accountId = "GB001a1";
            var premiseId = "GB01a1p1";
            var serviceId = "GB01a1p1s1";
            var serviceId2 = "GB01a1p1s2";
            var rateClass = "RG-1";
            var rateClass2 = "RTGas";
            var uomKey = "kwh";
            var commodityKey = "electric";
            var commodityKey2 = "gas";
            var uomKey2 = "ccf";
            var addr1 = "16 Laurel Ave";
            var city = "Wellesley";
            var state = "MA";
            var zip = "02481";
            var billDays = 30;
            var firstname = "TestFirst";
            var lastname = "TestLast";

            //add property for client user, normally from the stored procedure but hardcoded here for unit test purposes
            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };


            var config = TestGreenButtonHelper.GetConfigSetting();

            var gbBillModel = new GreenButtonBillModel(clientId, config, _insightRepository.Object);


            var mockData = TestGreenButtonHelper.GetMockMultipleServiceSinglePremiseSingleAccountData(clientId, customerId, accountId,
               premiseId, serviceId, serviceId2, commodityKey, commodityKey2, uomKey, uomKey2, billDays, rateClass, rateClass2, addr1, city, state, zip, firstname,
               lastname);
            _insightRepository.Setup(c => c.GetBills(clientId, It.IsAny<BillRequest>())).Returns(mockData);

            var gbBillRequest = new GreenButtonBillRequest
            {
                CustomerId = customerId,
                AccountId = accountId,
                PremiseId = premiseId,
                ServiceContractId = serviceId,
                OutputFormat = "Xml"
            };

            var result = gbBillModel.GetGreenButtonBill(clientUser, gbBillRequest);


            Assert.IsNotNull(result);
            Assert.IsNotNull("xml", result.FileType.ToLower());
            Assert.IsNull(result.File);
            Assert.IsNotNull(result.FileXml);
        }
    }
}