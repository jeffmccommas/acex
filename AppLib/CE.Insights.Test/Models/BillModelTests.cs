﻿//using AutoMapper;
//using CE.AO.Business;
//using CE.AO.DataAccess;
//using CE.AO.Entities;
//using CE.AO.Entities.TableStorageEntities;
//using CE.AO.Models;
//using CE.Insights.Test.Helpers;
//using CE.Models.Insights;
//using Microsoft.Practices.Unity;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using Moq;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using AO.Registrar;

//// ReSharper disable once CheckNamespace
//namespace CE.Insights.Models.Tests
//{
//    [TestClass()]
//    public class BillModelTests
//    {

//        private Mock<ITableRepository> _tableRepository;
//        private UnityContainer _container;
//        [TestInitialize]
//        public void TestInit()
//        {
//            _tableRepository = new Mock<ITableRepository>();
//            _container = new UnityContainer();

//            (new DataStorageRegistrar()).Initialize<TransientLifetimeManager>(_container);

//            Mapper.CreateMap<CustomerEntity, CustomerModel>();
//            Mapper.CreateMap<PremiseEntity, PremiseModel>();
//            Mapper.CreateMap<BillingEntity, BillingModel>();
//        }

//        [TestCleanup]
//        public void TestClean()
//        {
//            //noop
//        }
//        [TestMethod()]
//        public void BillV2_GetCustomerInfo_TableStorage_Test()
//        {
//            var clientId = 87;
//            var customerId = "123";
//            var firstname = "Anjana";
//            var lastname = "Khaire";

//            var model = new BillModel(_container);

//            // Get customer info from table storage
//            var result = model.GetCustomerInfo(clientId, customerId);

//            // if nothing from table storage, test with mock data
//            if (result == null)
//            {
//                BillV2_GetCustomerInfo_Mock_Test();
//            }
//            else
//            {
//                Assert.IsNotNull(result);
//                Assert.AreEqual(customerId, result.CustomerId);
//                Assert.AreEqual(firstname, result.FirstName);
//                Assert.AreEqual(lastname, result.LastName);
//            }
//        }

//        [TestMethod()]
//        public void BillV2_GetCustomerInfo_Mock_Test()
//        {
//            _container.RegisterInstance(_tableRepository.Object);

//            var clientId = 87;
//            var customerId = "123";
//            var firstname = "Anjana";
//            var lastname = "Khaire";
//            var addr1 = "16 Laurel Ave";
//            var addr2 = "Suite 100";
//            var city = "Wellesley";
//            var state = "MA";
//            var zip = "02481";
//            var email = "test@aclara.com";

//            var model = new BillModel(_container);
//            // mock customer entity
//            var mockCustomer = TestHelpers.CreateMockResidentialCustomerEntityData(clientId, customerId, firstname, lastname,
//                addr1, addr2, city, state, zip, email);
//            _tableRepository.Setup(c => c.GetSingleAsync<CustomerEntity>(Constants.TableNames.CustomerPremiseProfile, string.Format("{0}_{1}", clientId, customerId.ToLower()), "C")).ReturnsAsync(mockCustomer);

//            var result = model.GetCustomerInfo(clientId, customerId);

//            Assert.IsNotNull(result);
//            Assert.AreEqual(customerId, result.CustomerId);
//            Assert.AreEqual(firstname, result.FirstName);
//            Assert.AreEqual(lastname, result.LastName);
//            Assert.AreEqual(addr1, result.AddressLine1);
//            Assert.AreEqual(addr2, result.AddressLine2);
//            Assert.AreEqual(city, result.City);
//            Assert.AreEqual(state, result.State);
//            Assert.AreEqual(zip, result.PostalCode);
//            Assert.AreEqual(email, result.EmailAddress);
//        }


//        [TestMethod()]
//        public void BillV2_GetCustomerInfo_NoData_Test()
//        {

//            var clientId = 87;
//            var customerId = "NoData";

//            var model = new BillModel(_container);
//            // Get customer info from table storage
//            var result = model.GetCustomerInfo(clientId, customerId);

//            Assert.IsNull(result);
//        }

//        [TestMethod()]
//        public void BillV2_GetPremiseInfo_TableStorage_Test()
//        {
//            var clientId = 87;
//            var customerId = "123";


//            var model = new BillModel(_container);

//            // Get customer info from table storage
//            var result = model.GetPremisesInfo(clientId, customerId);

//            // if nothing from table storage, test with mock data
//            if (result == null || (result.Count == 0))
//            {
//                BillV2_GetPremiseInfo_Single_Mock_Test();
//            }
//            else
//            {
//                Assert.IsNotNull(result);
//                Assert.AreEqual(customerId, result[0].CustomerId);
//            }
//        }

//        [TestMethod()]
//        public void BillV2_GetPremiseInfo_Single_Mock_Test()
//        {
//            _container.RegisterInstance(_tableRepository.Object);

//            var clientId = 87;
//            var customerId = "123";
//            var accountId = "a123";
//            var premiseId = "p123";
//            var addr1 = "16 Laurel Ave";
//            var addr2 = "Suite 100";
//            var city = "Wellesley";
//            var state = "MA";
//            var zip = "02481";

//            var model = new BillModel(_container);
//            // mock premise entity
//            var mockPremise = TestHelpers.CreateMockPremiseEntityData(clientId, customerId, premiseId, accountId, addr1, addr2, city, state, zip);
//            var mockPremises = new List<PremiseEntity>();
//            mockPremises.Add(mockPremise);
//            var filter = string.Format("(PartitionKey eq '{0}_{1}') and ((RowKey ge 'P') and (RowKey lt 'Q'))", clientId, customerId.ToLower());
//            _tableRepository.Setup(s => s.GetAllAsync<PremiseEntity>(Constants.TableNames.CustomerPremiseProfile, filter)).Returns(mockPremises);

//            var result = model.GetPremisesInfo(clientId, customerId);

//            Assert.IsNotNull(result);
//            Assert.AreEqual(1, result.Count());
//            Assert.AreEqual(customerId, result[0].CustomerId);
//            Assert.AreEqual(premiseId, result[0].PremiseId);
//            Assert.AreEqual(accountId, result[0].AccountId);
//            Assert.AreEqual(addr1, result[0].AddressLine1);
//            Assert.AreEqual(addr2, result[0].AddressLine2);
//            Assert.AreEqual(city, result[0].City);
//            Assert.AreEqual(state, result[0].State);
//            Assert.AreEqual(zip, result[0].PostalCode);
//        }

//        [TestMethod()]
//        public void BillV2_GetPremiseInfo_Multiple_Mock_Test()
//        {
//            _container.RegisterInstance(_tableRepository.Object);

//            var clientId = 87;
//            var customerId = "123";
//            var accountId = "a123";
//            var premiseId_1 = "p123_1";
//            var premiseId_2 = "p123_2";
//            var addr1 = "16 Laurel Ave";
//            var addr2 = "Suite 100";
//            var city = "Wellesley";
//            var state = "MA";
//            var zip = "02481";

//            var model = new BillModel(_container);

//            // mock premise entity
//            var mockPremise1 = TestHelpers.CreateMockPremiseEntityData(clientId, customerId, premiseId_1, accountId, addr1, addr2, city, state, zip);
//            var mockPremise2 = TestHelpers.CreateMockPremiseEntityData(clientId, customerId, premiseId_2, accountId, addr1, addr2, city, state, zip);
//            var mockPremises = new List<PremiseEntity>();
//            mockPremises.Add(mockPremise1);
//            mockPremises.Add(mockPremise2);
//            var filter = string.Format("(PartitionKey eq '{0}_{1}') and ((RowKey ge 'P') and (RowKey lt 'Q'))", clientId, customerId.ToLower());
//            _tableRepository.Setup(s => s.GetAllAsync<PremiseEntity>(Constants.TableNames.CustomerPremiseProfile, filter)).Returns(mockPremises);

//            var result = model.GetPremisesInfo(clientId, customerId);

//            Assert.IsNotNull(result);
//            Assert.AreEqual(2, result.Count());
//            Assert.AreEqual(customerId, result[0].CustomerId);
//            Assert.AreEqual(premiseId_1, result[0].PremiseId);
//            Assert.AreEqual(accountId, result[0].AccountId);
//            Assert.AreEqual(addr1, result[0].AddressLine1);
//            Assert.AreEqual(addr2, result[0].AddressLine2);
//            Assert.AreEqual(city, result[0].City);
//            Assert.AreEqual(state, result[0].State);
//            Assert.AreEqual(zip, result[0].PostalCode);
//            Assert.AreEqual(customerId, result[1].CustomerId);
//            Assert.AreEqual(premiseId_2, result[1].PremiseId);
//            Assert.AreEqual(accountId, result[1].AccountId);
//            Assert.AreEqual(addr1, result[1].AddressLine1);
//            Assert.AreEqual(addr2, result[1].AddressLine2);
//            Assert.AreEqual(city, result[1].City);
//            Assert.AreEqual(state, result[1].State);
//            Assert.AreEqual(zip, result[1].PostalCode);
//        }
//        [TestMethod()]
//        public void BillV2_GetPremiseInfo_NoData_Test()
//        {
//            var clientId = 87;
//            var customerId = "2";

//            var model = new BillModel(_container);

//            // Get customer info from table storage
//            var result = model.GetPremisesInfo(clientId, customerId);

//            Assert.IsNotNull(result);
//            Assert.AreEqual(0, result.Count());
//        }

//        [TestMethod()]
//        public void BillV2_GetServiceList_TableStorage_Test()
//        {
//            var clientId = 87;
//            var customerId = "LK3";
//            var accountId = "AcntNo104";

//            var model = new BillModel(_container);

//            // Get customer info from table storage
//            var result = model.GetServiceList(clientId, customerId, accountId);

//            // if nothing from table storage, test with mock data
//            if (result == null || (result.Count == 0))
//            {
//                BillV2_GetServiceList_Single_Mock_Test();
//            }
//            else
//            {
//                Assert.IsNotNull(result);                
//                Assert.AreEqual(3, result.Count());
//                Assert.IsNotNull(result[0].RateClass);
//                Assert.AreNotEqual(string.Empty, result[0].RateClass);
//            }
//        }

//        [TestMethod()]
//        public void BillV2_GetServiceList_Single_Mock_Test()
//        {
//            _container.RegisterInstance(_tableRepository.Object);

//            var clientId = 87;
//            var customerId = "BTD001";
//            var accountId = "BTD001a1";
//            var premiseId = "BTD001a1p1";
//            var billCycleScheduleId = "Cycle01";
//            var meterId = "EM001";
//            var rateClass = "RG-1";
//            var servicePointId = "SP_BTD001a1";
//            var serviceContractId = "BTD001a1_BTD001a1p1_1_EM001";
//            var today = DateTime.Today.Date;
//            var currentStart = today.AddDays(-6);
//            var lastBillEndDate = currentStart.AddDays(-1);
//            var lastBillStartDate = lastBillEndDate.AddDays(-29);

//            var model = new BillModel(_container);
//            // mock billing entities data
//            var mockBillingEntity = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId,
//                servicePointId, meterId, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
//            var services = new List<BillingEntity>();
//            services.Add(mockBillingEntity);
//            var filter = string.Format("(PartitionKey eq '{0}_{1}_{2}') and ((RowKey ge 'B') and (RowKey lt 'C'))", clientId.ToString(), customerId.ToLower(), accountId.ToLower());
//            _tableRepository.Setup(s => s.GetAllAsync<BillingEntity>(Constants.TableNames.Billing, filter)).Returns(services);

//            var result = model.GetServiceList(clientId, customerId, accountId);

//            Assert.IsNotNull(result);
//            Assert.AreEqual(1, result.Count());
//            Assert.AreEqual(customerId, result[0].CustomerId);
//            Assert.AreEqual(rateClass, result[0].RateClass);
//            Assert.AreEqual(serviceContractId, result[0].ServiceContractId);
//        }

//        [TestMethod()]
//        public void BillV2_GetServiceList_Multiple_Mock_Test()
//        {
//            _container.RegisterInstance(_tableRepository.Object);

//            var clientId = 87;
//            var customerId = "BTD001";
//            var accountId = "BTD001a1";
//            var premiseId = "BTD001a1p1";
//            var billCycleScheduleId = "Cycle01";
//            var meterId1 = "EM001";
//            var meterId2 = "EM002";
//            var rateClass = "RG-1";
//            var servicePointId1 = "SP_BTD001a1_1";
//            var servicePointId2 = "SP_BTD001a1_2";
//            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_1_EM001";
//            var serviceContractId2 = "BTD001a1_BTD001a1p1_2_1_EM002";
//            var today = DateTime.Today.Date;
//            var currentStart = today.AddDays(-6);
//            var lastBillEndDate = currentStart.AddDays(-1);
//            var lastBillStartDate = lastBillEndDate.AddDays(-29);

//            var model = new BillModel(_container);
//            // mock billing entities data
//            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
//                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
//            var mockBillingEntity2 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId2,
//                servicePointId2, meterId2, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
//            var services = new List<BillingEntity>();
//            services.Add(mockBillingEntity1);
//            services.Add(mockBillingEntity2);
//            var filter = string.Format("(PartitionKey eq '{0}_{1}_{2}') and ((RowKey ge 'B') and (RowKey lt 'C'))", clientId.ToString(), customerId.ToLower(), accountId.ToLower());
//            _tableRepository.Setup(s => s.GetAllAsync<BillingEntity>(Constants.TableNames.Billing, filter)).Returns(services);

//            var result = model.GetServiceList(clientId, customerId, accountId);

//            Assert.IsNotNull(result);
//            Assert.AreEqual(2, result.Count());
//            Assert.AreEqual(customerId, result[0].CustomerId);
//            Assert.AreEqual(serviceContractId1, result[0].ServiceContractId);
//            Assert.AreEqual(rateClass, result[0].RateClass);
//            Assert.AreEqual(customerId, result[1].CustomerId);
//            Assert.AreEqual(serviceContractId2, result[1].ServiceContractId);
//            Assert.AreEqual(rateClass, result[1].RateClass);
//        }

//        [TestMethod()]
//        public void BillV2_GetServiceList_NoData_Test()
//        {
//            var clientId = 87;
//            var customerId = "NoData";
//            var accountId = "AcntNo104";

//            var model = new BillModel(_container);

//            // Get customer info from table storage
//            var result = model.GetServiceList(clientId, customerId, accountId);

//            Assert.IsNotNull(result);
//            Assert.AreEqual(0, result.Count());
//        }

//        [TestMethod()]
//        public void BillV2_GetBillsInfo_Mock_Test()
//        {
//            _container.RegisterInstance(_tableRepository.Object);

//            var clientId = 87;
//            var customerId = "BTD001";
//            var accountId = "BTD001a1";
//            var premiseId = "BTD001a1p1";
//            var billCycleScheduleId = "Cycle01";
//            var meterId1 = "EM001";
//            var rateClass = "RG-1";
//            var servicePointId1 = "SP_BTD001a1_1";
//            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_1_EM001";
//            var today = DateTime.Today.Date;
//            var currentStart = today.AddDays(-6);
//            var currentEnd = currentStart.AddDays(30);
//            var lastBillEndDate = currentStart.AddDays(-1);
//            var end = currentEnd;
//            var start = currentStart;

//            var model = new BillModel(_container);
//            // mock billing entities data
//            var services = new List<BillingEntity>();
//            for (int i = 0; i < 5; i++)
//            {
//                end = start.AddDays(-1);
//                start = end.AddDays(-29);
//                var mockBillingEntity = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
//                    servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, start, end);

//                services.Add(mockBillingEntity);
//            }
//            var filter = string.Format("(PartitionKey eq '{0}_{1}_{2}') and ((RowKey le 'H_{3}_{4}') and (RowKey ge 'H_{3}_{5}'))", clientId.ToString(), customerId.ToLower(), accountId.ToLower(), serviceContractId1.ToLower(), today.ToString("yyyy-MM-dd"), start.ToString("yyyy-MM-dd"));
//            _tableRepository.Setup(s => s.GetAllAsync<BillingEntity>(Constants.TableNames.Billing, filter)).Returns(services);

//            var result = model.GetBillsInfo(clientId, customerId, accountId, serviceContractId1, start, today);

//            Assert.IsNotNull(result);
//            Assert.AreEqual(5, result.Count());
//            Assert.AreEqual(customerId, result[0].CustomerId);
//            Assert.AreEqual(serviceContractId1, result[0].ServiceContractId);
//            Assert.AreEqual(lastBillEndDate, result[0].EndDate);
//            Assert.AreEqual(end, result[4].EndDate);
//        }

//        [TestMethod()]
//        public void BillV2_GetBillsInfo_TableStorage_Test()
//        {
//            var clientId = 87;
//            var customerId = "LK3";
//            var accountId = "AcntNo104";
//            var serviceContractId1 = "AcntNo104_p3_5_m105";
//            var end = DateTime.Today.Date;
//            var start = Convert.ToDateTime("1/1/2016");
//            var date = Convert.ToDateTime("1/22/2016");

//            var model = new BillModel(_container);

//            var result = model.GetBillsInfo(clientId, customerId, accountId, serviceContractId1, start, end);

//            if (result == null || (result.Count == 0))
//            {
//                BillV2_GetBillsInfo_Mock_Test();
//            }
//            else
//            {
//                Assert.IsNotNull(result);
//                Assert.AreEqual(2, result.Count());
//                Assert.AreEqual(customerId, result[0].CustomerId);
//                Assert.AreEqual(serviceContractId1, result[0].ServiceContractId);
//                Assert.AreEqual(date.Date, result[0].EndDate.Date);
//            }

//        }

//        public void BillV2_GetBillsInfo_NoData_Test()
//        {

//            var clientId = 87;
//            var customerId = "NoData";
//            var accountId = "AcntNo104";
//            var serviceContractId1 = "AcntNo104_p3_5_m105";
//            var end = DateTime.Today.Date;
//            var start = Convert.ToDateTime("1/1/2016");

//            var model = new BillModel(_container);

//            var result = model.GetBillsInfo(clientId, customerId, accountId, serviceContractId1, start, end);

//            Assert.IsNotNull(result);
//            Assert.AreEqual(0, result.Count());

//        }

//        [TestMethod()]
//        public void BillV2_GetCustomerBill_TableStorage_Test()
//        {
//            var clientId = 87;  
//            var customerId = "LK3";
//            var end = DateTime.Today.Date;
//            var start = Convert.ToDateTime("1/1/2016");

//            var billRequest = new BillRequest
//            {
//                CustomerId = customerId,
//                StartDate = start,
//                EndDate = end,
//                Count = 8,
//                IncludeContent = false
//            };


//            var model = new BillModel(_container);

//            // Get customer info from table storage
//            var result = model.GetCustomerBill(clientId, billRequest);

//            // if nothing from table storage, test with mock data
//            if (result == null)
//            {
//                BillV2_GetCustomerBill_Mock_SingleAccountSinglePremiseSingleSerivce_Test();
//            }
//            else
//            {
//                Assert.IsNotNull(result);
//                Assert.AreEqual(customerId, result.Id);
//            }
//        }

//        [TestMethod()]
//        public void BillV2_GetCustomerBill_Mock_SingleAccountSinglePremiseSingleSerivce_Test()
//        {
//            _container.RegisterInstance(_tableRepository.Object);

//            var clientId = 87;
//            var firstname = "TestFirst";
//            var lastname = "TestLast";
//            var customerId = "BTD001";
//            var accountId = "BTD001a1";
//            var premiseId = "BTD001a1p1";
//            var addr1 = "16 Laurel Ave";
//            var addr2 = "Suite 100";
//            var city = "Wellesley";
//            var state = "MA";
//            var zip = "02481";
//            var email = "test@aclara.com";
//            var billCycleScheduleId = "Cycle01";
//            var meterId1 = "EM001";
//            var rateClass = "RG-1";
//            var servicePointId1 = "SP_BTD001a1_1";
//            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_1_EM001";
//            var today = DateTime.Today.Date;
//            var currentStart = today.AddDays(-6);
//            var lastBillEndDate = currentStart.AddDays(-1);
//            var lastBillStartDate = lastBillEndDate.AddDays(-29);
//            DateTime end;
//            var start = currentStart;

//            // mock billing entities data
//            var bills = new List<BillingEntity>();
//            for (int i = 0; i < 5; i++)
//            {
//                end = start.AddDays(-1);
//                start = end.AddDays(-29);
//                var mockBillingEntity = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
//                    servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, start, end);

//                bills.Add(mockBillingEntity);
//            }
//            var filter = string.Format("(PartitionKey eq '{0}_{1}_{2}') and ((RowKey le 'H_{3}_{4}') and (RowKey ge 'H_{3}_{5}'))", clientId.ToString(), customerId.ToLower(), accountId.ToLower(), serviceContractId1.ToLower(), today.ToString("yyyy-MM-dd"), start.ToString("yyyy-MM-dd"));
//            _tableRepository.Setup(s => s.GetAllAsync<BillingEntity>(Constants.TableNames.Billing, filter)).Returns(bills);
//            // mock services data
//            var mockServicesEntity = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
//                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
//            var services = new List<BillingEntity>();
//            services.Add(mockServicesEntity);
//            var filterService = string.Format("(PartitionKey eq '{0}_{1}_{2}') and ((RowKey ge 'B') and (RowKey lt 'C'))", clientId.ToString(), customerId.ToLower(), accountId.ToLower());
//            _tableRepository.Setup(s => s.GetAllAsync<BillingEntity>(Constants.TableNames.Billing, filterService)).Returns(services);
//            // mock premise entity
//            var mockPremise = TestHelpers.CreateMockPremiseEntityData(clientId, customerId, premiseId, accountId, addr1, addr2, city, state, zip);
//            var mockPremises = new List<PremiseEntity>();
//            mockPremises.Add(mockPremise);
//            var filterPremise = string.Format("(PartitionKey eq '{0}_{1}') and ((RowKey ge 'P') and (RowKey lt 'Q'))", clientId, customerId.ToLower());
//            _tableRepository.Setup(s => s.GetAllAsync<PremiseEntity>(Constants.TableNames.CustomerPremiseProfile, filterPremise)).Returns(mockPremises);
//            // mock customer entity
//            var mockCustomer = TestHelpers.CreateMockResidentialCustomerEntityData(clientId, customerId, firstname, lastname,
//                addr1, addr2, city, state, zip, email);
//            _tableRepository.Setup(c => c.GetSingleAsync<CustomerEntity>(Constants.TableNames.CustomerPremiseProfile, string.Format("{0}_{1}", clientId, customerId.ToLower()), "C")).ReturnsAsync(mockCustomer);

//            var billRequest = new BillRequest
//            {
//                CustomerId = customerId,
//                StartDate = start,
//                EndDate = today,
//                Count = 8,
//                IncludeContent = false
//            };

//            var model = new BillModel(_container);

//            var result = model.GetCustomerBill(clientId, billRequest);

//            Assert.IsNotNull(result);
//            Assert.AreEqual(customerId, result.Id);
//            Assert.AreEqual(1, result.Accounts.Count);
//            Assert.AreEqual(accountId, result.Accounts[0].Id);
//            Assert.AreEqual(5, result.Accounts[0].Bills.Count);
//            Assert.AreEqual(1, result.Accounts[0].Bills[0].Premises.Count);
//            Assert.AreEqual(premiseId, result.Accounts[0].Bills[0].Premises[0].Id);
//            Assert.AreEqual(1, result.Accounts[0].Bills[0].Premises[0].Service.Count);
//            Assert.AreEqual(serviceContractId1, result.Accounts[0].Bills[0].Premises[0].Service[0].Id);
//            Assert.AreEqual(rateClass, result.Accounts[0].Bills[0].Premises[0].Service[0].RateClass);
//        }
//        [TestMethod()]
//        public void BillV2_GetCustomerBill_Mock_SingleAccountSinglePremiseMultSerivce_Test()
//        {
//            _container.RegisterInstance(_tableRepository.Object);

//            var clientId = 87;
//            var firstname = "TestFirst";
//            var lastname = "TestLast";
//            var customerId = "BTD001";
//            var accountId = "BTD001a1";
//            var premiseId = "BTD001a1p1";
//            var addr1 = "16 Laurel Ave";
//            var addr2 = "Suite 100";
//            var city = "Wellesley";
//            var state = "MA";
//            var zip = "02481";
//            var email = "test@aclara.com";
//            var billCycleScheduleId = "Cycle01";
//            var meterId1 = "EM001";
//            var meterId2 = "EM002";
//            var rateClass = "RG-1";
//            var servicePointId1 = "SP_BTD001a1_1";
//            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_1_EM001";
//            var servicePointId2 = "SP_BTD001a1_2";
//            var serviceContractId2 = "BTD001a1_BTD001a1p1_2_1_EM001";
//            var today = DateTime.Today.Date;
//            var currentStart = today.AddDays(-6);
//            var lastBillEndDate = currentStart.AddDays(-1);
//            var lastBillStartDate = lastBillEndDate.AddDays(-29);
//            DateTime end;
//            var start = currentStart;
//            var oldestStart = DateTime.MaxValue;

//            // mock billing entities data
//            var bills1 = new List<BillingEntity>();
//            for (int i = 0; i < 5; i++)
//            {
//                end = start.AddDays(-1);
//                start = end.AddDays(-29);

//                if (start < oldestStart) oldestStart = start;
//                var mockBillingEntity = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
//                    servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, start, end);

//                bills1.Add(mockBillingEntity);
//            }
//            start = currentStart;
//            var bills2 = new List<BillingEntity>();
//            for (int i = 0; i < 3; i++)
//            {
//                end = start.AddDays(-1);
//                start = end.AddDays(-29);
//                if (start < oldestStart) oldestStart = start;
//                var mockBillingEntity = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId2,
//                    servicePointId2, meterId2, billCycleScheduleId, rateClass, 1000, start, end);

//                bills2.Add(mockBillingEntity);
//            }
//            var filter1 = string.Format("(PartitionKey eq '{0}_{1}_{2}') and ((RowKey le 'H_{3}_{4}') and (RowKey ge 'H_{3}_{5}'))", clientId.ToString(), customerId.ToLower(), accountId.ToLower(), serviceContractId1.ToLower(), today.ToString("yyyy-MM-dd"), oldestStart.ToString("yyyy-MM-dd"));
//            _tableRepository.Setup(s => s.GetAllAsync<BillingEntity>(Constants.TableNames.Billing, filter1)).Returns(bills1);

//            var filter2 = string.Format("(PartitionKey eq '{0}_{1}_{2}') and ((RowKey le 'H_{3}_{4}') and (RowKey ge 'H_{3}_{5}'))", clientId.ToString(), customerId.ToLower(), accountId.ToLower(), serviceContractId2.ToLower(), today.ToString("yyyy-MM-dd"), oldestStart.ToString("yyyy-MM-dd"));
//            _tableRepository.Setup(s => s.GetAllAsync<BillingEntity>(Constants.TableNames.Billing, filter2)).Returns(bills2);
//            // mock services data
//            var mockServicesEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
//                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
//            var services = new List<BillingEntity>();
//            services.Add(mockServicesEntity1);
//            var mockServicesEntity2 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId2,
//                servicePointId2, meterId2, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
//            services.Add(mockServicesEntity2);
//            var filterService = string.Format("(PartitionKey eq '{0}_{1}_{2}') and ((RowKey ge 'B') and (RowKey lt 'C'))", clientId.ToString(), customerId.ToLower(), accountId.ToLower());
//            _tableRepository.Setup(s => s.GetAllAsync<BillingEntity>(Constants.TableNames.Billing, filterService)).Returns(services);
//            // mock premise entity
//            var mockPremise = TestHelpers.CreateMockPremiseEntityData(clientId, customerId, premiseId, accountId, addr1, addr2, city, state, zip);
//            var mockPremises = new List<PremiseEntity>();
//            mockPremises.Add(mockPremise);
//            var filterPremise = string.Format("(PartitionKey eq '{0}_{1}') and ((RowKey ge 'P') and (RowKey lt 'Q'))", clientId, customerId.ToLower());
//            _tableRepository.Setup(s => s.GetAllAsync<PremiseEntity>(Constants.TableNames.CustomerPremiseProfile, filterPremise)).Returns(mockPremises);
//            // mock customer entity
//            var mockCustomer = TestHelpers.CreateMockResidentialCustomerEntityData(clientId, customerId, firstname, lastname,
//                addr1, addr2, city, state, zip, email);
//            _tableRepository.Setup(c => c.GetSingleAsync<CustomerEntity>(Constants.TableNames.CustomerPremiseProfile, string.Format("{0}_{1}", clientId, customerId.ToLower()), "C")).ReturnsAsync(mockCustomer);

//            var billRequest = new BillRequest
//            {
//                CustomerId = customerId,
//                StartDate = oldestStart,
//                EndDate = today,
//                Count = 8,
//                IncludeContent = false
//            };

//            var model = new BillModel(_container);

//            var result = model.GetCustomerBill(clientId, billRequest);

//            Assert.IsNotNull(result);
//            Assert.AreEqual(customerId, result.Id);
//            Assert.AreEqual(1, result.Accounts.Count);
//            Assert.AreEqual(accountId, result.Accounts[0].Id);
//            Assert.AreEqual(5, result.Accounts[0].Bills.Count);
//            Assert.AreEqual(1, result.Accounts[0].Bills[0].Premises.Count);
//            Assert.AreEqual(premiseId, result.Accounts[0].Bills[0].Premises[0].Id);
//            Assert.AreEqual(2, result.Accounts[0].Bills[0].Premises[0].Service.Count);
//            Assert.AreEqual(rateClass, result.Accounts[0].Bills[0].Premises[0].Service[0].RateClass);
//            var serivce1Count = 0;
//            var service2Count = 0;
//            foreach (var bill in result.Accounts[0].Bills)
//            {
//                foreach (var premise in bill.Premises)
//                {
//                    foreach (var service in premise.Service)
//                    {
//                        if (service.Id == serviceContractId1)
//                            serivce1Count++;
//                        else if (service.Id == serviceContractId2)
//                            service2Count++;
//                    }
//                }
//            }
//            Assert.AreEqual(5, serivce1Count);
//            Assert.AreEqual(3, service2Count);
//        }
//        [TestMethod()]
//        public void BillV2_GetCustomerBill_Mock_SingleAccountSinglePremiseMultSerivce_DiffBillCycle_Test()
//        {
//            _container.RegisterInstance(_tableRepository.Object);

//            var clientId = 87;
//            var firstname = "TestFirst";
//            var lastname = "TestLast";
//            var customerId = "BTD001";
//            var accountId = "BTD001a1";
//            var premiseId = "BTD001a1p1";
//            var addr1 = "16 Laurel Ave";
//            var addr2 = "Suite 100";
//            var city = "Wellesley";
//            var state = "MA";
//            var zip = "02481";
//            var email = "test@aclara.com";
//            var billCycleScheduleId = "Cycle01";
//            var meterId1 = "EM001";
//            var meterId2 = "EM002";
//            var rateClass = "RG-1";
//            var servicePointId1 = "SP_BTD001a1_1";
//            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_1_EM001";
//            var servicePointId2 = "SP_BTD001a1_2";
//            var serviceContractId2 = "BTD001a1_BTD001a1p1_2_1_EM001";
//            var today = DateTime.Today.Date;
//            var currentStart = today.AddDays(-6);
//            var lastBillEndDate = currentStart.AddDays(-1);
//            var lastBillStartDate = lastBillEndDate.AddDays(-29);
//            DateTime end;
//            var start = currentStart;
//            var oldestStart = DateTime.MaxValue;

//            // mock billing entities data
//            var bills1 = new List<BillingEntity>();
//            for (int i = 0; i < 5; i++)
//            {
//                end = start.AddDays(-1);
//                start = end.AddDays(-29);

//                if (start < oldestStart) oldestStart = start;
//                var mockBillingEntity = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
//                    servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, start, end);

//                bills1.Add(mockBillingEntity);
//            }
//            start = currentStart;
//            var bills2 = new List<BillingEntity>();
//            for (int i = 0; i < 3; i++)
//            {
//                end = start.AddDays(-1);
//                start = end.AddDays(-30);
//                if (start < oldestStart) oldestStart = start;
//                var mockBillingEntity = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId2,
//                    servicePointId2, meterId2, billCycleScheduleId, rateClass, 1000, start, end);

//                bills2.Add(mockBillingEntity);
//            }
//            var filter1 = string.Format("(PartitionKey eq '{0}_{1}_{2}') and ((RowKey le 'H_{3}_{4}') and (RowKey ge 'H_{3}_{5}'))", clientId.ToString(), customerId.ToLower(), accountId.ToLower(), serviceContractId1.ToLower(), today.ToString("yyyy-MM-dd"), oldestStart.ToString("yyyy-MM-dd"));
//            _tableRepository.Setup(s => s.GetAllAsync<BillingEntity>(Constants.TableNames.Billing, filter1)).Returns(bills1);

//            var filter2 = string.Format("(PartitionKey eq '{0}_{1}_{2}') and ((RowKey le 'H_{3}_{4}') and (RowKey ge 'H_{3}_{5}'))", clientId.ToString(), customerId.ToLower(), accountId.ToLower(), serviceContractId2.ToLower(), today.ToString("yyyy-MM-dd"), oldestStart.ToString("yyyy-MM-dd"));
//            _tableRepository.Setup(s => s.GetAllAsync<BillingEntity>(Constants.TableNames.Billing, filter2)).Returns(bills2);
//            // mock services data
//            var mockServicesEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
//                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
//            var services = new List<BillingEntity>();
//            services.Add(mockServicesEntity1);
//            var mockServicesEntity2 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId2,
//                servicePointId2, meterId2, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
//            services.Add(mockServicesEntity2);
//            var filterService = string.Format("(PartitionKey eq '{0}_{1}_{2}') and ((RowKey ge 'B') and (RowKey lt 'C'))", clientId.ToString(), customerId.ToLower(), accountId.ToLower());
//            _tableRepository.Setup(s => s.GetAllAsync<BillingEntity>(Constants.TableNames.Billing, filterService)).Returns(services);
//            // mock premise entity
//            var mockPremise = TestHelpers.CreateMockPremiseEntityData(clientId, customerId, premiseId, accountId, addr1, addr2, city, state, zip);
//            var mockPremises = new List<PremiseEntity>();
//            mockPremises.Add(mockPremise);
//            var filterPremise = string.Format("(PartitionKey eq '{0}_{1}') and ((RowKey ge 'P') and (RowKey lt 'Q'))", clientId, customerId.ToLower());
//            _tableRepository.Setup(s => s.GetAllAsync<PremiseEntity>(Constants.TableNames.CustomerPremiseProfile, filterPremise)).Returns(mockPremises);
//            // mock customer entity
//            var mockCustomer = TestHelpers.CreateMockResidentialCustomerEntityData(clientId, customerId, firstname, lastname,
//                addr1, addr2, city, state, zip, email);
//            _tableRepository.Setup(c => c.GetSingleAsync<CustomerEntity>(Constants.TableNames.CustomerPremiseProfile, string.Format("{0}_{1}", clientId, customerId.ToLower()), "C")).ReturnsAsync(mockCustomer);

//            var billRequest = new BillRequest
//            {
//                CustomerId = customerId,
//                StartDate = oldestStart,
//                EndDate = today,
//                Count = 8,
//                IncludeContent = false
//            };

//            var model = new BillModel(_container);

//            var result = model.GetCustomerBill(clientId, billRequest);

//            Assert.IsNotNull(result);
//            Assert.AreEqual(customerId, result.Id);
//            Assert.AreEqual(1, result.Accounts.Count);
//            Assert.AreEqual(accountId, result.Accounts[0].Id);
//            Assert.AreEqual(8, result.Accounts[0].Bills.Count);
//            Assert.AreEqual(1, result.Accounts[0].Bills[0].Premises.Count);
//            Assert.AreEqual(premiseId, result.Accounts[0].Bills[0].Premises[0].Id);
//            var serivce1Count = 0;
//            var service2Count = 0;
//            var service1BillStart = string.Empty;
//            var service2BillStart = string.Empty;
//            foreach (var bill in result.Accounts[0].Bills)
//            {
//                if (bill.Premises[0].Service[0].Id == serviceContractId1)
//                {
//                    serivce1Count++;
//                    if (service1BillStart == string.Empty) service1BillStart = bill.Premises[0].Service[0].BillStartDate;
//                }
//                else if (bill.Premises[0].Service[0].Id == serviceContractId2)
//                {
//                    service2Count++;
//                    if (service2BillStart == string.Empty) service2BillStart = bill.Premises[0].Service[0].BillStartDate;
//                }
//            }
//            Assert.AreEqual(5, serivce1Count);
//            Assert.AreEqual(3, service2Count);
//            Assert.AreNotEqual(service1BillStart, service2BillStart);
//        }
//        [TestMethod()]
//        public void BillV2_GetCustomerBill_Mock_SingleAccountMultiPremiseSingleSerivce_Test()
//        {
//            _container.RegisterInstance(_tableRepository.Object);

//            var clientId = 87;
//            var firstname = "TestFirst";
//            var lastname = "TestLast";
//            var customerId = "BTD001";
//            var accountId = "BTD001a1";
//            var premiseId1 = "BTD001a1p1";
//            var premiseId2 = "BTD001a1p2";
//            var addr1 = "16 Laurel Ave";
//            var addr2 = "Suite 100";
//            var city = "Wellesley";
//            var state = "MA";
//            var zip = "02481";
//            var email = "test@aclara.com";
//            var billCycleScheduleId = "Cycle01";
//            var meterId1 = "EM001";
//            var meterId2 = "EM002";
//            var rateClass = "RG-1";
//            var servicePointId1 = "SP_BTD001a1_1";
//            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_1_EM001";
//            var servicePointId2 = "SP_BTD001a1_2";
//            var serviceContractId2 = "BTD001a1_BTD001a1p2_2_1_EM001";
//            var today = DateTime.Today.Date;
//            var currentStart = today.AddDays(-6);
//            var lastBillEndDate = currentStart.AddDays(-1);
//            var lastBillStartDate = lastBillEndDate.AddDays(-29);
//            DateTime end;
//            var start = currentStart;
//            var oldestStart = DateTime.MaxValue;

//            // mock billing entities data
//            var bills1 = new List<BillingEntity>();
//            for (int i = 0; i < 5; i++)
//            {
//                end = start.AddDays(-1);
//                start = end.AddDays(-29);

//                if (start < oldestStart) oldestStart = start;
//                var mockBillingEntity = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId1, serviceContractId1,
//                    servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, start, end);

//                bills1.Add(mockBillingEntity);
//            }
//            start = currentStart;
//            var bills2 = new List<BillingEntity>();
//            for (int i = 0; i < 3; i++)
//            {
//                end = start.AddDays(-1);
//                start = end.AddDays(-29);
//                if (start < oldestStart) oldestStart = start;
//                var mockBillingEntity = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId2, serviceContractId2,
//                    servicePointId2, meterId2, billCycleScheduleId, rateClass, 1000, start, end);

//                bills2.Add(mockBillingEntity);
//            }
//            var filter1 = string.Format("(PartitionKey eq '{0}_{1}_{2}') and ((RowKey le 'H_{3}_{4}') and (RowKey ge 'H_{3}_{5}'))", clientId.ToString(), customerId.ToLower(), accountId.ToLower(), serviceContractId1.ToLower(), today.ToString("yyyy-MM-dd"), oldestStart.ToString("yyyy-MM-dd"));
//            _tableRepository.Setup(s => s.GetAllAsync<BillingEntity>(Constants.TableNames.Billing, filter1)).Returns(bills1);

//            var filter2 = string.Format("(PartitionKey eq '{0}_{1}_{2}') and ((RowKey le 'H_{3}_{4}') and (RowKey ge 'H_{3}_{5}'))", clientId.ToString(), customerId.ToLower(), accountId.ToLower(), serviceContractId2.ToLower(), today.ToString("yyyy-MM-dd"), oldestStart.ToString("yyyy-MM-dd"));
//            _tableRepository.Setup(s => s.GetAllAsync<BillingEntity>(Constants.TableNames.Billing, filter2)).Returns(bills2);
//            // mock services data
//            var mockServicesEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId1, serviceContractId1,
//                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
//            var services = new List<BillingEntity>();
//            services.Add(mockServicesEntity1);
//            var mockServicesEntity2 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId2, serviceContractId2,
//                servicePointId2, meterId2, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
//            services.Add(mockServicesEntity2);
//            var filterService = string.Format("(PartitionKey eq '{0}_{1}_{2}') and ((RowKey ge 'B') and (RowKey lt 'C'))", clientId.ToString(), customerId.ToLower(), accountId.ToLower());
//            _tableRepository.Setup(s => s.GetAllAsync<BillingEntity>(Constants.TableNames.Billing, filterService)).Returns(services);
//            // mock premise entity
//            var mockPremise1 = TestHelpers.CreateMockPremiseEntityData(clientId, customerId, premiseId1, accountId, addr1, addr2, city, state, zip);
//            var mockPremise2 = TestHelpers.CreateMockPremiseEntityData(clientId, customerId, premiseId2, accountId, addr1, addr2, city, state, zip);
//            var mockPremises = new List<PremiseEntity>();
//            mockPremises.Add(mockPremise1);
//            mockPremises.Add(mockPremise2);
//            var filterPremise = string.Format("(PartitionKey eq '{0}_{1}') and ((RowKey ge 'P') and (RowKey lt 'Q'))", clientId, customerId.ToLower());
//            _tableRepository.Setup(s => s.GetAllAsync<PremiseEntity>(Constants.TableNames.CustomerPremiseProfile, filterPremise)).Returns(mockPremises);
//            // mock customer entity
//            var mockCustomer = TestHelpers.CreateMockResidentialCustomerEntityData(clientId, customerId, firstname, lastname,
//                addr1, addr2, city, state, zip, email);
//            _tableRepository.Setup(c => c.GetSingleAsync<CustomerEntity>(Constants.TableNames.CustomerPremiseProfile, string.Format("{0}_{1}", clientId, customerId.ToLower()), "C")).ReturnsAsync(mockCustomer);

//            var billRequest = new BillRequest
//            {
//                CustomerId = customerId,
//                StartDate = oldestStart,
//                EndDate = today,
//                Count = 8,
//                IncludeContent = false
//            };

//            var model = new BillModel(_container);

//            var result = model.GetCustomerBill(clientId, billRequest);

//            Assert.IsNotNull(result);
//            Assert.AreEqual(customerId, result.Id);
//            Assert.AreEqual(1, result.Accounts.Count);
//            Assert.AreEqual(accountId, result.Accounts[0].Id);
//            Assert.AreEqual(5, result.Accounts[0].Bills.Count);
//            Assert.AreEqual(2, result.Accounts[0].Bills[0].Premises.Count);
//            var service1Count = 0;
//            var service2Count = 0;
//            foreach (var bill in result.Accounts[0].Bills)
//            {
//                foreach (var premise in bill.Premises)
//                {
//                    if (premise.Id == premiseId1)
//                        service1Count++;
//                    else if (premise.Id == premiseId2)
//                        service2Count++;
//                }
//            }
//            Assert.AreEqual(5, service1Count);
//            Assert.AreEqual(3, service2Count);
//        }
//        [TestMethod()]
//        public void BillV2_GetCustomerBill_Mock_SingleAccountMultiPremiseSingleSerivce_DiffBillCycle_Test()
//        {
//            _container.RegisterInstance(_tableRepository.Object);

//            var clientId = 87;
//            var firstname = "TestFirst";
//            var lastname = "TestLast";
//            var customerId = "BTD001";
//            var accountId = "BTD001a1";
//            var premiseId1 = "BTD001a1p1";
//            var premiseId2 = "BTD001a1p2";
//            var addr1 = "16 Laurel Ave";
//            var addr2 = "Suite 100";
//            var city = "Wellesley";
//            var state = "MA";
//            var zip = "02481";
//            var email = "test@aclara.com";
//            var billCycleScheduleId = "Cycle01";
//            var meterId1 = "EM001";
//            var meterId2 = "EM002";
//            var rateClass = "RG-1";
//            var servicePointId1 = "SP_BTD001a1_1";
//            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_1_EM001";
//            var servicePointId2 = "SP_BTD001a1_2";
//            var serviceContractId2 = "BTD001a1_BTD001a1p2_2_1_EM001";
//            var today = DateTime.Today.Date;
//            var currentStart = today.AddDays(-6);
//            var lastBillEndDate = currentStart.AddDays(-1);
//            var lastBillStartDate = lastBillEndDate.AddDays(-29);
//            DateTime end;
//            var start = currentStart;
//            var oldestStart = DateTime.MaxValue;

//            // mock billing entities data
//            var bills1 = new List<BillingEntity>();
//            for (int i = 0; i < 5; i++)
//            {
//                end = start.AddDays(-1);
//                start = end.AddDays(-29);

//                if (start < oldestStart) oldestStart = start;
//                var mockBillingEntity = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId1, serviceContractId1,
//                    servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, start, end);

//                bills1.Add(mockBillingEntity);
//            }
//            start = currentStart;
//            var bills2 = new List<BillingEntity>();
//            for (int i = 0; i < 3; i++)
//            {
//                end = start.AddDays(-1);
//                start = end.AddDays(-30);
//                if (start < oldestStart) oldestStart = start;
//                var mockBillingEntity = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId2, serviceContractId2,
//                    servicePointId2, meterId2, billCycleScheduleId, rateClass, 1000, start, end);

//                bills2.Add(mockBillingEntity);
//            }
//            var filter1 = string.Format("(PartitionKey eq '{0}_{1}_{2}') and ((RowKey le 'H_{3}_{4}') and (RowKey ge 'H_{3}_{5}'))", clientId.ToString(), customerId.ToLower(), accountId.ToLower(), serviceContractId1.ToLower(), today.ToString("yyyy-MM-dd"), oldestStart.ToString("yyyy-MM-dd"));
//            _tableRepository.Setup(s => s.GetAllAsync<BillingEntity>(Constants.TableNames.Billing, filter1)).Returns(bills1);

//            var filter2 = string.Format("(PartitionKey eq '{0}_{1}_{2}') and ((RowKey le 'H_{3}_{4}') and (RowKey ge 'H_{3}_{5}'))", clientId.ToString(), customerId.ToLower(), accountId.ToLower(), serviceContractId2.ToLower(), today.ToString("yyyy-MM-dd"), oldestStart.ToString("yyyy-MM-dd"));
//            _tableRepository.Setup(s => s.GetAllAsync<BillingEntity>(Constants.TableNames.Billing, filter2)).Returns(bills2);
//            // mock services data
//            var mockServicesEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId1, serviceContractId1,
//                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
//            var services = new List<BillingEntity>();
//            services.Add(mockServicesEntity1);
//            var mockServicesEntity2 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId2, serviceContractId2,
//                servicePointId2, meterId2, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
//            services.Add(mockServicesEntity2);
//            var filterService = string.Format("(PartitionKey eq '{0}_{1}_{2}') and ((RowKey ge 'B') and (RowKey lt 'C'))", clientId.ToString(), customerId.ToLower(), accountId.ToLower());
//            _tableRepository.Setup(s => s.GetAllAsync<BillingEntity>(Constants.TableNames.Billing, filterService)).Returns(services);
//            // mock premise entity
//            var mockPremise1 = TestHelpers.CreateMockPremiseEntityData(clientId, customerId, premiseId1, accountId, addr1, addr2, city, state, zip);
//            var mockPremise2 = TestHelpers.CreateMockPremiseEntityData(clientId, customerId, premiseId2, accountId, addr1, addr2, city, state, zip);
//            var mockPremises = new List<PremiseEntity>();
//            mockPremises.Add(mockPremise1);
//            mockPremises.Add(mockPremise2);
//            var filterPremise = string.Format("(PartitionKey eq '{0}_{1}') and ((RowKey ge 'P') and (RowKey lt 'Q'))", clientId, customerId.ToLower());
//            _tableRepository.Setup(s => s.GetAllAsync<PremiseEntity>(Constants.TableNames.CustomerPremiseProfile, filterPremise)).Returns(mockPremises);
//            // mock customer entity
//            var mockCustomer = TestHelpers.CreateMockResidentialCustomerEntityData(clientId, customerId, firstname, lastname,
//                addr1, addr2, city, state, zip, email);
//            _tableRepository.Setup(c => c.GetSingleAsync<CustomerEntity>(Constants.TableNames.CustomerPremiseProfile, string.Format("{0}_{1}", clientId, customerId.ToLower()), "C")).ReturnsAsync(mockCustomer);

//            var billRequest = new BillRequest
//            {
//                CustomerId = customerId,
//                StartDate = oldestStart,
//                EndDate = today,
//                Count = 8,
//                IncludeContent = false
//            };

//            var model = new BillModel(_container);

//            var result = model.GetCustomerBill(clientId, billRequest);

//            Assert.IsNotNull(result);
//            Assert.AreEqual(customerId, result.Id);
//            Assert.AreEqual(1, result.Accounts.Count);
//            Assert.AreEqual(accountId, result.Accounts[0].Id);
//            Assert.AreEqual(8, result.Accounts[0].Bills.Count);
//            Assert.AreEqual(1, result.Accounts[0].Bills[0].Premises.Count);
//            var service1Count = 0;
//            var service2Count = 0;
//            foreach (var bill in result.Accounts[0].Bills)
//            {
//                foreach (var premise in bill.Premises)
//                {
//                    if (premise.Id == premiseId1)
//                        service1Count++;
//                    else if (premise.Id == premiseId2)
//                        service2Count++;
//                }
//            }
//            Assert.AreEqual(5, service1Count);
//            Assert.AreEqual(3, service2Count);
//        }
//        [TestMethod()]
//        public void BillV2_GetCustomerBill_Mock_MultAccountSinglePremiseSingleSerivce_Test()
//        {
//            _container.RegisterInstance(_tableRepository.Object);

//            var clientId = 87;
//            var firstname = "TestFirst";
//            var lastname = "TestLast";
//            var customerId = "BTD001";
//            var accountId1 = "BTD001a1";
//            var accountId2 = "BTD001a2";
//            var premiseId1 = "BTD001a1p1";
//            var premiseId2 = "BTD001a2p1";
//            var addr1 = "16 Laurel Ave";
//            var addr2 = "Suite 100";
//            var city = "Wellesley";
//            var state = "MA";
//            var zip = "02481";
//            var email = "test@aclara.com";
//            var billCycleScheduleId = "Cycle01";
//            var meterId1 = "EM001";
//            var meterId2 = "EM002";
//            var rateClass = "RG-1";
//            var servicePointId1 = "SP_BTD001a1_1";
//            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_1_EM001";
//            var servicePointId2 = "SP_BTD001a2_2";
//            var serviceContractId2 = "BTD001a2_BTD001a2p1_2_1_EM001";
//            var today = DateTime.Today.Date;
//            var currentStart = today.AddDays(-6);
//            var lastBillEndDate = currentStart.AddDays(-1);
//            var lastBillStartDate = lastBillEndDate.AddDays(-29);
//            DateTime end;
//            var start = currentStart;
//            var oldestStart = DateTime.MaxValue;

//            // mock billing entities data
//            var bills1 = new List<BillingEntity>();
//            for (int i = 0; i < 5; i++)
//            {
//                end = start.AddDays(-1);
//                start = end.AddDays(-29);

//                if (start < oldestStart) oldestStart = start;
//                var mockBillingEntity = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId1, premiseId1, serviceContractId1,
//                    servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, start, end);

//                bills1.Add(mockBillingEntity);
//            }
//            start = currentStart;
//            var bills2 = new List<BillingEntity>();
//            for (int i = 0; i < 3; i++)
//            {
//                end = start.AddDays(-1);
//                start = end.AddDays(-29);
//                if (start < oldestStart) oldestStart = start;
//                var mockBillingEntity = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId2, premiseId2, serviceContractId2,
//                    servicePointId2, meterId2, billCycleScheduleId, rateClass, 1000, start, end);

//                bills2.Add(mockBillingEntity);
//            }
//            var filter1 = string.Format("(PartitionKey eq '{0}_{1}_{2}') and ((RowKey le 'H_{3}_{4}') and (RowKey ge 'H_{3}_{5}'))", clientId.ToString(), customerId.ToLower(), accountId1.ToLower(), serviceContractId1.ToLower(), today.ToString("yyyy-MM-dd"), oldestStart.ToString("yyyy-MM-dd"));
//            _tableRepository.Setup(s => s.GetAllAsync<BillingEntity>(Constants.TableNames.Billing, filter1)).Returns(bills1);

//            var filter2 = string.Format("(PartitionKey eq '{0}_{1}_{2}') and ((RowKey le 'H_{3}_{4}') and (RowKey ge 'H_{3}_{5}'))", clientId.ToString(), customerId.ToLower(), accountId2.ToLower(), serviceContractId2.ToLower(), today.ToString("yyyy-MM-dd"), oldestStart.ToString("yyyy-MM-dd"));
//            _tableRepository.Setup(s => s.GetAllAsync<BillingEntity>(Constants.TableNames.Billing, filter2)).Returns(bills2);
//            // mock services data
//            var mockServicesEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId1, premiseId1, serviceContractId1,
//                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
//            var services1 = new List<BillingEntity>();
//            services1.Add(mockServicesEntity1);
//            var filterService1 = string.Format("(PartitionKey eq '{0}_{1}_{2}') and ((RowKey ge 'B') and (RowKey lt 'C'))", clientId.ToString(), customerId.ToLower(), accountId1.ToLower());
//            _tableRepository.Setup(s => s.GetAllAsync<BillingEntity>(Constants.TableNames.Billing, filterService1)).Returns(services1);
//            var mockServicesEntity2 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId2, premiseId2, serviceContractId2,
//                servicePointId2, meterId2, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
//            var services2 = new List<BillingEntity>();
//            services2.Add(mockServicesEntity2);
//            var filterService2 = string.Format("(PartitionKey eq '{0}_{1}_{2}') and ((RowKey ge 'B') and (RowKey lt 'C'))", clientId.ToString(), customerId.ToLower(), accountId2.ToLower());
//            _tableRepository.Setup(s => s.GetAllAsync<BillingEntity>(Constants.TableNames.Billing, filterService2)).Returns(services2);

//            // mock premise entity
//            var mockPremise1 = TestHelpers.CreateMockPremiseEntityData(clientId, customerId, premiseId1, accountId1, addr1, addr2, city, state, zip);
//            var mockPremise2 = TestHelpers.CreateMockPremiseEntityData(clientId, customerId, premiseId2, accountId2, addr1, addr2, city, state, zip);
//            var mockPremises = new List<PremiseEntity>();
//            mockPremises.Add(mockPremise1);
//            mockPremises.Add(mockPremise2);
//            var filterPremise = string.Format("(PartitionKey eq '{0}_{1}') and ((RowKey ge 'P') and (RowKey lt 'Q'))", clientId, customerId.ToLower());
//            _tableRepository.Setup(s => s.GetAllAsync<PremiseEntity>(Constants.TableNames.CustomerPremiseProfile, filterPremise)).Returns(mockPremises);
//            // mock customer entity
//            var mockCustomer = TestHelpers.CreateMockResidentialCustomerEntityData(clientId, customerId, firstname, lastname,
//                addr1, addr2, city, state, zip, email);
//            _tableRepository.Setup(c => c.GetSingleAsync<CustomerEntity>(Constants.TableNames.CustomerPremiseProfile, string.Format("{0}_{1}", clientId, customerId.ToLower()), "C")).ReturnsAsync(mockCustomer);

//            var billRequest = new BillRequest
//            {
//                CustomerId = customerId,
//                StartDate = oldestStart,
//                EndDate = today,
//                Count = 8,
//                IncludeContent = false
//            };

//            var model = new BillModel(_container);

//            var result = model.GetCustomerBill(clientId, billRequest);

//            Assert.IsNotNull(result);
//            Assert.AreEqual(customerId, result.Id);
//            Assert.AreEqual(2, result.Accounts.Count);
//            Assert.AreEqual(accountId1, result.Accounts[0].Id);
//            Assert.AreEqual(accountId2, result.Accounts[1].Id);
//            Assert.AreEqual(5, result.Accounts[0].Bills.Count);
//            Assert.AreEqual(3, result.Accounts[1].Bills.Count);
//            Assert.AreEqual(1, result.Accounts[0].Bills[0].Premises.Count);
//            Assert.AreEqual(1, result.Accounts[1].Bills[0].Premises.Count);
//        }
//        [TestMethod()]
//        public void BillV2_GetCustomerBill_Mock_MultAccountSinglePremiseSingleSerivce_DiffBillCycle_Test()
//        {
//            _container.RegisterInstance(_tableRepository.Object);

//            var clientId = 87;
//            var firstname = "TestFirst";
//            var lastname = "TestLast";
//            var customerId = "BTD001";
//            var accountId1 = "BTD001a1";
//            var accountId2 = "BTD001a2";
//            var premiseId1 = "BTD001a1p1";
//            var premiseId2 = "BTD001a2p1";
//            var addr1 = "16 Laurel Ave";
//            var addr2 = "Suite 100";
//            var city = "Wellesley";
//            var state = "MA";
//            var zip = "02481";
//            var email = "test@aclara.com";
//            var billCycleScheduleId = "Cycle01";
//            var meterId1 = "EM001";
//            var meterId2 = "EM002";
//            var rateClass = "RG-1";
//            var servicePointId1 = "SP_BTD001a1_1";
//            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_1_EM001";
//            var servicePointId2 = "SP_BTD001a2_2";
//            var serviceContractId2 = "BTD001a2_BTD001a2p1_2_1_EM001";
//            var today = DateTime.Today.Date;
//            var currentStart = today.AddDays(-6);
//            var lastBillEndDate = currentStart.AddDays(-1);
//            var lastBillStartDate = lastBillEndDate.AddDays(-29);
//            DateTime end;
//            var start = currentStart;
//            var oldestStart = DateTime.MaxValue;

//            // mock billing entities data
//            var bills1 = new List<BillingEntity>();
//            for (int i = 0; i < 5; i++)
//            {
//                end = start.AddDays(-1);
//                start = end.AddDays(-29);

//                if (start < oldestStart) oldestStart = start;
//                var mockBillingEntity = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId1, premiseId1, serviceContractId1,
//                    servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, start, end);

//                bills1.Add(mockBillingEntity);
//            }
//            start = currentStart;
//            var bills2 = new List<BillingEntity>();
//            for (int i = 0; i < 3; i++)
//            {
//                end = start.AddDays(-1);
//                start = end.AddDays(-30);
//                if (start < oldestStart) oldestStart = start;
//                var mockBillingEntity = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId2, premiseId2, serviceContractId2,
//                    servicePointId2, meterId2, billCycleScheduleId, rateClass, 1000, start, end);

//                bills2.Add(mockBillingEntity);
//            }
//            var filter1 = string.Format("(PartitionKey eq '{0}_{1}_{2}') and ((RowKey le 'H_{3}_{4}') and (RowKey ge 'H_{3}_{5}'))", clientId.ToString(), customerId.ToLower(), accountId1.ToLower(), serviceContractId1.ToLower(), today.ToString("yyyy-MM-dd"), oldestStart.ToString("yyyy-MM-dd"));
//            _tableRepository.Setup(s => s.GetAllAsync<BillingEntity>(Constants.TableNames.Billing, filter1)).Returns(bills1);

//            var filter2 = string.Format("(PartitionKey eq '{0}_{1}_{2}') and ((RowKey le 'H_{3}_{4}') and (RowKey ge 'H_{3}_{5}'))", clientId.ToString(), customerId.ToLower(), accountId2.ToLower(), serviceContractId2.ToLower(), today.ToString("yyyy-MM-dd"), oldestStart.ToString("yyyy-MM-dd"));
//            _tableRepository.Setup(s => s.GetAllAsync<BillingEntity>(Constants.TableNames.Billing, filter2)).Returns(bills2);
//            // mock services data
//            var mockServicesEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId1, premiseId1, serviceContractId1,
//                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
//            var services1 = new List<BillingEntity>();
//            services1.Add(mockServicesEntity1);
//            var filterService1 = string.Format("(PartitionKey eq '{0}_{1}_{2}') and ((RowKey ge 'B') and (RowKey lt 'C'))", clientId.ToString(), customerId.ToLower(), accountId1.ToLower());
//            _tableRepository.Setup(s => s.GetAllAsync<BillingEntity>(Constants.TableNames.Billing, filterService1)).Returns(services1);
//            var mockServicesEntity2 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId2, premiseId2, serviceContractId2,
//                servicePointId2, meterId2, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
//            var services2 = new List<BillingEntity>();
//            services2.Add(mockServicesEntity2);
//            var filterService2 = string.Format("(PartitionKey eq '{0}_{1}_{2}') and ((RowKey ge 'B') and (RowKey lt 'C'))", clientId.ToString(), customerId.ToLower(), accountId2.ToLower());
//            _tableRepository.Setup(s => s.GetAllAsync<BillingEntity>(Constants.TableNames.Billing, filterService2)).Returns(services2);

//            // mock premise entity
//            var mockPremise1 = TestHelpers.CreateMockPremiseEntityData(clientId, customerId, premiseId1, accountId1, addr1, addr2, city, state, zip);
//            var mockPremise2 = TestHelpers.CreateMockPremiseEntityData(clientId, customerId, premiseId2, accountId2, addr1, addr2, city, state, zip);
//            var mockPremises = new List<PremiseEntity>();
//            mockPremises.Add(mockPremise1);
//            mockPremises.Add(mockPremise2);
//            var filterPremise = string.Format("(PartitionKey eq '{0}_{1}') and ((RowKey ge 'P') and (RowKey lt 'Q'))", clientId, customerId.ToLower());
//            _tableRepository.Setup(s => s.GetAllAsync<PremiseEntity>(Constants.TableNames.CustomerPremiseProfile, filterPremise)).Returns(mockPremises);
//            // mock customer entity
//            var mockCustomer = TestHelpers.CreateMockResidentialCustomerEntityData(clientId, customerId, firstname, lastname,
//                addr1, addr2, city, state, zip, email);
//            _tableRepository.Setup(c => c.GetSingleAsync<CustomerEntity>(Constants.TableNames.CustomerPremiseProfile, string.Format("{0}_{1}", clientId, customerId.ToLower()), "C")).ReturnsAsync(mockCustomer);

//            var billRequest = new BillRequest
//            {
//                CustomerId = customerId,
//                StartDate = oldestStart,
//                EndDate = today,
//                Count = 8,
//                IncludeContent = false
//            };

//            var model = new BillModel(_container);

//            var result = model.GetCustomerBill(clientId, billRequest);

//            Assert.IsNotNull(result);
//            Assert.AreEqual(customerId, result.Id);
//            Assert.AreEqual(2, result.Accounts.Count);
//            Assert.AreEqual(accountId1, result.Accounts[0].Id);
//            Assert.AreEqual(accountId2, result.Accounts[1].Id);
//            Assert.AreEqual(5, result.Accounts[0].Bills.Count);
//            Assert.AreEqual(3, result.Accounts[1].Bills.Count);
//            Assert.AreEqual(1, result.Accounts[0].Bills[0].Premises.Count);
//            Assert.AreEqual(1, result.Accounts[1].Bills[0].Premises.Count);
//        }

//        [TestMethod()]
//        public void BillV2_GetCustomerBill_NoData_Test()
//        {
//            var clientId = 87;
//            var customerId = "NoData";
//            var end = DateTime.Today.Date;
//            var start = Convert.ToDateTime("1/1/2016");

//            var billRequest = new BillRequest
//            {
//                CustomerId = customerId,
//                StartDate = start,
//                EndDate = end,
//                Count = 1,
//                IncludeContent = false
//            };


//            var model = new BillModel(_container);

//            // Get customer info from table storage
//            var result = model.GetCustomerBill(clientId, billRequest);

//            Assert.IsNull(result);
//        }

//        [TestMethod()]
//        public void BillV2_GetWeatherInfoTest()
//        {
//            var service = new BillService
//            {
//                Id = "SC_9000EG1P_electric",
//                BillStartDate = "2015-12-07",
//                BillEndDate = "2016-01-04",
//                CommodityKey = "electric",
//                UOMKey = "kwh",
//                BillDays = 28,
//                TotalServiceUse = 671,
//                CostofUsage = 148.47m,
//                AdditionalServiceCost = 0,
//                AverageTemperature = 0
//            };
//            var premise = new BillPremise
//            {
//                Id = "9000EG1P",
//                Zip = "902214523",
//                Service = new List<BillService>()
//            };
//            premise.Service.Add(service);
//            var bill = new Bill
//            {
//                BillDate = "2016-01-04",
//                Premises = new List<BillPremise>()
//            };
//            bill.Premises.Add(premise);
//            var account = new BillAccount
//            {
//                Id = "9000EG1",
//                Bills = new List<Bill>()
//            };
//            account.Bills.Add(bill);
//            var customer = new BillCustomer
//            {
//                Id = "9000EG1",
//                Accounts = new List<BillAccount>()
//            };
//            customer.Accounts.Add(account);

//            var model = new BillModel(_container);

//            model.GetWeatherInfo(customer);

//            Assert.AreEqual(55, customer.Accounts[0].Bills[0].Premises[0].Service[0].AverageTemperature);
//        }

//        [TestMethod()]
//        public void BillV2_GetBills_TableStorage_Test()
//        {
//            var clientId = 87;
//            var customerId = "LK3";
//            var end = DateTime.Today.Date;
//            var start = Convert.ToDateTime("1/1/2016");

//            var billRequest = new BillRequest
//            {
//                CustomerId = customerId,
//                StartDate = start,
//                EndDate = end,
//                Count = 4,
//                IncludeContent = false
//            };


//            var model = new BillModel(_container);

//            // Get customer info from table storage
//            var result = model.GetBills(clientId, billRequest);

//            //// if nothing from table storage, test with mock data
//            if (result.Customer == null)
//            {
//                BillV2_GetBills_Mock_SingleAccountSinglePremiseSingleSerivce_Test();
//            }
//            else
//            {
//                Assert.IsNotNull(result);
//                Assert.AreEqual(customerId, result.Customer.Id);
//            }
//        }


//        [TestMethod()]
//        public void BillV2_GetBills_Mock_SingleAccountSinglePremiseSingleSerivce_Test()
//        {
//            _container.RegisterInstance(_tableRepository.Object);

//            var clientId = 87;
//            var firstname = "TestFirst";
//            var lastname = "TestLast";
//            var customerId = "BTD001";
//            var accountId = "BTD001a1";
//            var premiseId = "BTD001a1p1";
//            var addr1 = "16 Laurel Ave";
//            var addr2 = "Suite 100";
//            var city = "Wellesley";
//            var state = "MA";
//            var zip = "02481";
//            var email = "test@aclara.com";
//            var billCycleScheduleId = "Cycle01";
//            var meterId1 = "EM001";
//            var rateClass = "RG-1";
//            var servicePointId1 = "SP_BTD001a1_1";
//            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_1_EM001";
//            var today = DateTime.Today.Date;
//            var currentStart = today.AddDays(-6);
//            var lastBillEndDate = currentStart.AddDays(-1);
//            var lastBillStartDate = lastBillEndDate.AddDays(-29);
//            DateTime end;
//            var start = currentStart;

//            // mock billing entities data
//            var bills = new List<BillingEntity>();
//            for (int i = 0; i < 5; i++)
//            {
//                end = start.AddDays(-1);
//                start = end.AddDays(-29);
//                var mockBillingEntity = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
//                    servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, start, end);

//                bills.Add(mockBillingEntity);
//            }
//            var filter = string.Format("(PartitionKey eq '{0}_{1}_{2}') and ((RowKey le 'H_{3}_{4}') and (RowKey ge 'H_{3}_{5}'))", clientId.ToString(), customerId.ToLower(), accountId.ToLower(), serviceContractId1.ToLower(), today.ToString("yyyy-MM-dd"), start.ToString("yyyy-MM-dd"));
//            _tableRepository.Setup(s => s.GetAllAsync<BillingEntity>(Constants.TableNames.Billing, filter)).Returns(bills);
//            // mock services data
//            var mockServicesEntity = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
//                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
//            var services = new List<BillingEntity>();
//            services.Add(mockServicesEntity);
//            var filterService = string.Format("(PartitionKey eq '{0}_{1}_{2}') and ((RowKey ge 'B') and (RowKey lt 'C'))", clientId.ToString(), customerId.ToLower(), accountId.ToLower());
//            _tableRepository.Setup(s => s.GetAllAsync<BillingEntity>(Constants.TableNames.Billing, filterService)).Returns(services);
//            // mock premise entity
//            var mockPremise = TestHelpers.CreateMockPremiseEntityData(clientId, customerId, premiseId, accountId, addr1, addr2, city, state, zip);
//            var mockPremises = new List<PremiseEntity>();
//            mockPremises.Add(mockPremise);
//            var filterPremise = string.Format("(PartitionKey eq '{0}_{1}') and ((RowKey ge 'P') and (RowKey lt 'Q'))", clientId, customerId.ToLower());
//            _tableRepository.Setup(s => s.GetAllAsync<PremiseEntity>(Constants.TableNames.CustomerPremiseProfile, filterPremise)).Returns(mockPremises);
//            // mock customer entity
//            var mockCustomer = TestHelpers.CreateMockResidentialCustomerEntityData(clientId, customerId, firstname, lastname,
//                addr1, addr2, city, state, zip, email);
//            _tableRepository.Setup(c => c.GetSingleAsync<CustomerEntity>(Constants.TableNames.CustomerPremiseProfile, string.Format("{0}_{1}", clientId, customerId.ToLower()), "C")).ReturnsAsync(mockCustomer);

//            var billRequest = new BillRequest
//            {
//                CustomerId = customerId,
//                StartDate = start,
//                EndDate = today,
//                Count = 8,
//                IncludeContent = false
//            };

//            var model = new BillModel(_container);

//            var result = model.GetBills(clientId, billRequest);

//            Assert.IsNotNull(result);
//            Assert.AreEqual(customerId, result.Customer.Id);
//            Assert.AreEqual(1, result.Customer.Accounts.Count);
//            Assert.AreEqual(accountId, result.Customer.Accounts[0].Id);
//            Assert.AreEqual(5, result.Customer.Accounts[0].Bills.Count);
//            Assert.AreEqual(1, result.Customer.Accounts[0].Bills[0].Premises.Count);
//            Assert.AreEqual(premiseId, result.Customer.Accounts[0].Bills[0].Premises[0].Id);
//            Assert.AreEqual(1, result.Customer.Accounts[0].Bills[0].Premises[0].Service.Count);
//            Assert.AreEqual(serviceContractId1, result.Customer.Accounts[0].Bills[0].Premises[0].Service[0].Id);
//            Assert.AreEqual(rateClass, result.Customer.Accounts[0].Bills[0].Premises[0].Service[0].RateClass);
//        }

//        [TestMethod()]
//        public void BillV2_GetBills_Mock_SingleAccountSinglePremiseSingleSerivce_Count1_Test()
//        {
//            _container.RegisterInstance(_tableRepository.Object);

//            var clientId = 87;
//            var firstname = "TestFirst";
//            var lastname = "TestLast";
//            var customerId = "BTD001";
//            var accountId = "BTD001a1";
//            var premiseId = "BTD001a1p1";
//            var addr1 = "16 Laurel Ave";
//            var addr2 = "Suite 100";
//            var city = "Wellesley";
//            var state = "MA";
//            var zip = "02481";
//            var email = "test@aclara.com";
//            var billCycleScheduleId = "Cycle01";
//            var meterId1 = "EM001";
//            var rateClass = "RG-1";
//            var servicePointId1 = "SP_BTD001a1_1";
//            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_1_EM001";
//            var today = DateTime.Today.Date;
//            var currentStart = today.AddDays(-6);
//            var lastBillEndDate = currentStart.AddDays(-1);
//            var lastBillStartDate = lastBillEndDate.AddDays(-29);
//            DateTime end;
//            var start = currentStart;

//            // mock billing entities data
//            var bills = new List<BillingEntity>();
//            for (int i = 0; i < 5; i++)
//            {
//                end = start.AddDays(-1);
//                start = end.AddDays(-29);
//                var mockBillingEntity = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
//                    servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, start, end);

//                bills.Add(mockBillingEntity);
//            }
//            var filter = string.Format("(PartitionKey eq '{0}_{1}_{2}') and ((RowKey le 'H_{3}_{4}') and (RowKey ge 'H_{3}_{5}'))", clientId.ToString(), customerId.ToLower(), accountId.ToLower(), serviceContractId1.ToLower(), today.ToString("yyyy-MM-dd"), start.ToString("yyyy-MM-dd"));
//            _tableRepository.Setup(s => s.GetAllAsync<BillingEntity>(Constants.TableNames.Billing, filter)).Returns(bills);
//            // mock services data
//            var mockServicesEntity = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
//                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
//            var services = new List<BillingEntity>();
//            services.Add(mockServicesEntity);
//            var filterService = string.Format("(PartitionKey eq '{0}_{1}_{2}') and ((RowKey ge 'B') and (RowKey lt 'C'))", clientId.ToString(), customerId.ToLower(), accountId.ToLower());
//            _tableRepository.Setup(s => s.GetAllAsync<BillingEntity>(Constants.TableNames.Billing, filterService)).Returns(services);
//            // mock premise entity
//            var mockPremise = TestHelpers.CreateMockPremiseEntityData(clientId, customerId, premiseId, accountId, addr1, addr2, city, state, zip);
//            var mockPremises = new List<PremiseEntity>();
//            mockPremises.Add(mockPremise);
//            var filterPremise = string.Format("(PartitionKey eq '{0}_{1}') and ((RowKey ge 'P') and (RowKey lt 'Q'))", clientId, customerId.ToLower());
//            _tableRepository.Setup(s => s.GetAllAsync<PremiseEntity>(Constants.TableNames.CustomerPremiseProfile, filterPremise)).Returns(mockPremises);
//            // mock customer entity
//            var mockCustomer = TestHelpers.CreateMockResidentialCustomerEntityData(clientId, customerId, firstname, lastname,
//                addr1, addr2, city, state, zip, email);
//            _tableRepository.Setup(c => c.GetSingleAsync<CustomerEntity>(Constants.TableNames.CustomerPremiseProfile, string.Format("{0}_{1}", clientId, customerId.ToLower()), "C")).ReturnsAsync(mockCustomer);

//            var billRequest = new BillRequest
//            {
//                CustomerId = customerId,
//                StartDate = start,
//                EndDate = today,
//                IncludeContent = false
//            };

//            var model = new BillModel(_container);

//            var result = model.GetBills(clientId, billRequest);

//            Assert.IsNotNull(result);
//            Assert.AreEqual(customerId, result.Customer.Id);
//            Assert.AreEqual(1, result.Customer.Accounts.Count);
//            Assert.AreEqual(accountId, result.Customer.Accounts[0].Id);
//            Assert.AreEqual(1, result.Customer.Accounts[0].Bills.Count);
//            Assert.AreEqual(1, result.Customer.Accounts[0].Bills[0].Premises.Count);
//            Assert.AreEqual(premiseId, result.Customer.Accounts[0].Bills[0].Premises[0].Id);
//            Assert.AreEqual(1, result.Customer.Accounts[0].Bills[0].Premises[0].Service.Count);
//            Assert.AreEqual(serviceContractId1, result.Customer.Accounts[0].Bills[0].Premises[0].Service[0].Id);
//            Assert.AreEqual(rateClass, result.Customer.Accounts[0].Bills[0].Premises[0].Service[0].RateClass);
//        }

//        [TestMethod()]
//        public void BillV2_GetBills_NoData_Test()
//        {
//            var clientId = 87;
//            var customerId = "NoData";
//            var end = DateTime.Today.Date;
//            var start = Convert.ToDateTime("1/1/2016");

//            var billRequest = new BillRequest
//            {
//                CustomerId = customerId,
//                StartDate = start,
//                EndDate = end,
//                Count = 4,
//                IncludeContent = false
//            };


//            var model = new BillModel(_container);

//            // Get customer info from table storage
//            var result = model.GetBills(clientId, billRequest);

//            Assert.IsNotNull(result);
//            Assert.IsNull(result.Customer);
//            Assert.AreEqual("No data found", result.Message);
//        }
//    }
//}