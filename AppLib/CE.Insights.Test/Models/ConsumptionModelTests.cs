﻿using AutoMapper;
using AO.Registrar;
using AO.BusinessContracts;
using CE.AO.Business;
using CE.AO.Models;
using CE.Insights.Test.Helpers;
using CE.Models;
using CE.Models.Insights;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Enums = CE.RateModel.Enums;

// ReSharper disable once CheckNamespace
namespace CE.Insights.Models.Tests
{
    [TestClass()]
    public class ConsumptionModelTests
    {
        #region Constants for content
        private const string ClientSpace = "netnln2itvho";
        private const string ClientAccessToken = "c58ec8d495044174267c257f105914f7fb9567127a879b2419e1c3d405a54be2";

        #endregion

        private Mock<ITallAMI> _amiManager;
        private UnityContainer _container;
        private Mock<IInsightsEFRepository> _insightRepository;
      

        [TestInitialize]
        public void TestInit()
        {
            _amiManager = new Mock<ITallAMI>();
            _container = new UnityContainer();
           

            (new DataStorageRegistrar()).Initialize<TransientLifetimeManager>(_container);

            _container.RegisterInstance(_amiManager.Object);
           _insightRepository = new Mock<IInsightsEFRepository>();
        }

        [TestCleanup]
        public void TestClean()
        {
            //noop
        }

        [TestMethod()]
        public void ConsumptionV1_GetReadingsTest_60Interval()
        {
            var clientId = 87;
            var accountId = "Acct87";
            var servicePointId = "servicePt87";
            var meterId = "EM001";
            var startDate = Convert.ToDateTime("12/17/2015");
            var endDate = Convert.ToDateTime("12/22/2015");
            var projected = Convert.ToDateTime("1/17/2016");
            var requestedResolution = "hour";

            // mock ami hourly data
            var amiModel = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterId, servicePointId, startDate, endDate, projected);
            _amiManager.Setup(m => m.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), meterId, clientId, It.IsAny<IDictionary<string, string>>())).Returns(amiModel).Verifiable();
            
            int testNoOfDays;
            var reading = TestHelpers.CreateMockAmiHourlyReading(startDate, endDate);
            _amiManager.Setup(
                a =>
                    a.GetReadings(It.IsAny<IEnumerable<TallAmiModel>>(), out testNoOfDays, It.IsAny<bool>())).Returns(reading).Verifiable();
            var model = new ConsumptionModel(_container);
            var result = model.GetReadings(clientId, accountId, new List<string> { meterId }, startDate, endDate, requestedResolution);//, out availableResolution, out amiUomId, out amiCommodityId);

            Assert.IsNotNull(result);
            var sortedResult = result.Result.Readings.OrderBy(a => a.Timestamp).ToList();
            Assert.AreEqual(startDate, sortedResult[0].Timestamp);
            Assert.AreEqual(startDate.AddHours(1), sortedResult[1].Timestamp);
            _amiManager.Verify();

        }

        [TestMethod()]
        public void ConsumptionV1_GetReadings_Month_Test()
        {
            var clientId = 87;
            var accountId = "Acct87";
            var servicePointId = "servicePt87";
            var projected = Convert.ToDateTime("1/17/2016");
            var meterId = "EM001";
            var startDate = Convert.ToDateTime("10/17/2015");
            var endDate = Convert.ToDateTime("1/17/2016");
            var requestedResolution = "month";
            // ReSharper disable once RedundantAssignment
            //int amiUomId;
            //int amiCommodityId;
            var meterIds = new List<string>();
            meterIds.Add(meterId);

            // mock ami hourly data
            var amiModel = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterId, servicePointId, startDate, endDate, projected);
            _amiManager.Setup(m => m.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), meterId, clientId, It.IsAny<IDictionary<string, string>>())).Returns(amiModel).Verifiable();
           
            var model = new ConsumptionModel(_container);
            //string availableResolution;
            var result = model.GetReadings(clientId, accountId, meterIds, startDate, endDate, requestedResolution);//, out availableResolution, out amiUomId, out amiCommodityId);


            Assert.IsNotNull(result);
            Assert.AreEqual(4, result.Result.Readings.Count());
            var sortedResult = result.Result.Readings.OrderBy(r => r.Timestamp).ToList();
            Assert.AreEqual(Convert.ToDateTime("12/1/2015"), sortedResult[2].Timestamp);
            Assert.AreEqual("month,day,hour", result.Result.AvailableResolultion);
            _amiManager.Verify();
        }

        [TestMethod()]
        public void ConsumptionV1_GetReadingsTest_30Interval()
        {
            var clientId = 87;
            var meterId = "EM001";
            var accountId = "Acct87";
            var servicePointId = "servicePt87";
            var projected = Convert.ToDateTime("1/17/2016");
            var startDate = Convert.ToDateTime("12/17/2015");
            var endDate = Convert.ToDateTime("12/22/2015");
            var requestedResolution = "thirty";

            // mock ami 30min data
            var amiModel = TestHelpers.CreateMockAmi30Model(clientId, accountId, meterId, servicePointId, startDate, endDate, projected);
            _amiManager.Setup(m => m.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), meterId, clientId, It.IsAny<IDictionary<string, string>>())).Returns(amiModel).Verifiable();
           
            int testNoOfDays;
            var readings = TestHelpers.CreateMockAmi30Reading(startDate, endDate);
            _amiManager.Setup(
                a =>
                    a.GetReadings(It.IsAny<IEnumerable<TallAmiModel>>(), out testNoOfDays, It.IsAny<bool>())).Returns(readings).Verifiable();


            var model = new ConsumptionModel(_container);
            var result = model.GetReadings(clientId, accountId, new List<string> { meterId }, startDate, endDate, requestedResolution);//, out availableResolution, out amiUomId, out amiCommodityId);

            Assert.IsNotNull(result);
            var sortedResult = result.Result.Readings.OrderBy(a => a.Timestamp).ToList();
            Assert.AreEqual(startDate, sortedResult[0].Timestamp);
            Assert.AreEqual(startDate.AddMinutes(30), sortedResult[1].Timestamp);
            _amiManager.Verify();
        }

        [TestMethod()]
        public void ConsumptionV1_GetReadingsTest_15Interval()
        {
            var clientId = 87;
            var meterId = "EM001";
            var accountId = "Acct87";
            var servicePointId = "servicePt87";
            var projected = Convert.ToDateTime("1/17/2016");
            var startDate = Convert.ToDateTime("12/17/2015");
            var endDate = Convert.ToDateTime("12/22/2015");
            var requestedResolution = "fifteen";

            // mock ami 15min data
            var amiModel = TestHelpers.CreateMockAmi15AmiModel(clientId, accountId, meterId, servicePointId, startDate, endDate, projected);
            _amiManager.Setup(m => m.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), meterId, clientId, It.IsAny<IDictionary<string, string>>())).Returns(amiModel).Verifiable();
          
            int testNoOfDays;
            var readings = TestHelpers.CreateMockAmi15Reading(startDate, endDate);
            _amiManager.Setup(
                a =>
                    a.GetReadings(It.IsAny<IEnumerable<TallAmiModel>>(), out testNoOfDays, It.IsAny<bool>())).Returns(readings).Verifiable();

            var model = new ConsumptionModel(_container);
            var result = model.GetReadings(clientId, accountId, new List<string> { meterId }, startDate, endDate, requestedResolution);//, out availableResolution, out amiUomId, out amiCommodityId);

            Assert.IsNotNull(result);
            var sortedResult = result.Result.Readings.OrderBy(a => a.Timestamp).ToList();
            Assert.AreEqual(startDate, sortedResult[0].Timestamp);
            Assert.AreEqual(startDate.AddMinutes(15), sortedResult[1].Timestamp);
            _amiManager.Verify();
        }


        [TestMethod()]
        public void ConsumptionV1_GetReadings_Day_Test()
        {
            var clientId = 87;
            var meterId = "EM001";
            var accountId = "Acct87";
            var servicePointId = "servicePt87";
            var projected = Convert.ToDateTime("1/17/2016");
            var startDate = Convert.ToDateTime("12/17/2015");
            var endDate = Convert.ToDateTime("12/22/2015");

            var startDateUtc = TimeZoneInfo.ConvertTimeToUtc(startDate,
                TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"));
            var endDateUtc = TimeZoneInfo.ConvertTimeToUtc(endDate,
                TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"));
            var requestedResolution = "day";
            //int amiUomId;
            //int amiCommodityId;
            var meterIds = new List<string>();
            meterIds.Add(meterId);

            // mock ami hourly data
            var amiModel = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterId, servicePointId, startDateUtc, endDateUtc, projected);
            _amiManager.Setup(m => m.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), meterId, clientId, It.IsAny<IDictionary<string, string>>())).Returns(amiModel).Verifiable();
          
            var model = new ConsumptionModel(_container);
            //string availableResolution;
            var result = model.GetReadings(clientId, accountId, meterIds, startDateUtc, endDateUtc, requestedResolution);//, out availableResolution, out amiUomId, out amiCommodityId);


            Assert.IsNotNull(result);
            Assert.AreEqual(6, result.Result.Readings.Count());
            var sortedResult = result.Result.Readings.OrderBy(r => r.Timestamp).ToList();
            Assert.AreEqual(Convert.ToDateTime("12/19/2015"), sortedResult[2].Timestamp);
            Assert.AreEqual(Convert.ToDateTime("12/17/2015"), sortedResult[0].Timestamp);
            _amiManager.Verify();
        }

        [TestMethod()]
        public void ConsumptionV1_GetReadingsTest_AggregatedDaily()
        {
            var clientId = 87;
            var meterId = "EM001";
            var meterId2 = "EM002";
            var accountId = "Acct87";
            var servicePointId = "servicePt1";
            var servicePointId2 = "servicePt2";
            var projected = Convert.ToDateTime("1/17/2016");
            var startDate = Convert.ToDateTime("12/17/2015");
            var endDate = Convert.ToDateTime("12/22/2015");
            var startDateUtc = TimeZoneInfo.ConvertTimeToUtc(startDate,
                TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"));
            var endDateUtc = TimeZoneInfo.ConvertTimeToUtc(endDate,
                TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"));
            var requestedResolution = "day";

            // mock ami hourly data
            var amiModel1 = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterId, servicePointId, startDateUtc, endDateUtc, projected);
            var amiModel2 = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterId2, servicePointId2, startDateUtc, endDateUtc, projected);
            _amiManager.Setup(m => m.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), meterId, clientId, It.IsAny<IDictionary<string, string>>())).Returns(amiModel1).Verifiable();
            _amiManager.Setup(m => m.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), meterId2, clientId, It.IsAny<IDictionary<string, string>>())).Returns(amiModel2).Verifiable();


            var model = new ConsumptionModel(_container);
            var result = model.GetReadings(clientId, accountId, new List<string> { meterId, meterId2 }, startDateUtc, endDateUtc, requestedResolution);//, out availableResolution, out amiUomId, out amiCommodityId);

            Assert.IsNotNull(result);
            var sortedResult = result.Result.Readings.OrderBy(a => a.Timestamp).ToList();
            Assert.AreEqual(startDate, sortedResult[0].Timestamp);
            Assert.AreEqual(startDate.AddDays(1), sortedResult[1].Timestamp);
            _amiManager.Verify();
        }

        [TestMethod()]
        public void ConsumptionV1_GetReadingsTest_AggregatedMonthlyReadingsTest()
        {
            var clientId = 87;
            var meterId = "EM001";
            var meterId2 = "EM002";
            var accountId = "Acct87";
            var servicePointId = "servicePt1";
            var servicePointId2 = "servicePt2";
            var projected = Convert.ToDateTime("1/17/2016");
            var startDate = Convert.ToDateTime("10/17/2015");
            var endDate = Convert.ToDateTime("1/17/2016");
            var requestedResolution = "month";
            var startDateUtc = TimeZoneInfo.ConvertTimeToUtc(startDate,
                TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"));
            var endDateUtc = TimeZoneInfo.ConvertTimeToUtc(endDate,
                TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"));
            //string availableResolution;
            //int amiUomId;
            //int amiCommodityId;

            // mock ami hourly data
            var amiModel1 = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterId, servicePointId, startDateUtc, endDateUtc, projected);
            var amiModel2 = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterId2, servicePointId2, startDateUtc, endDateUtc, projected);
            _amiManager.Setup(m => m.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), meterId, clientId, It.IsAny<IDictionary<string, string>>())).Returns(amiModel1).Verifiable();
            _amiManager.Setup(m => m.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), meterId2, clientId, It.IsAny<IDictionary<string, string>>())).Returns(amiModel2).Verifiable();


            var model = new ConsumptionModel(_container);
            var result = model.GetReadings(clientId, accountId, new List<string> { meterId, meterId2 }, startDateUtc, endDateUtc, requestedResolution);//, out availableResolution, out amiUomId, out amiCommodityId);


            Assert.IsNotNull(result);
            Assert.AreEqual(4, result.Result.Readings.Count());
            var sortedResult = result.Result.Readings.OrderBy(r => r.Timestamp).ToList();
            Assert.AreEqual(Convert.ToDateTime("12/1/2015"), sortedResult[2].Timestamp);
            _amiManager.Verify();
        }


        [TestMethod()]
        public void ConsumptionV1_GetReadingsTest_AggregatedInterval15ReadingsTest()
        {
            var clientId = 87;
            var meterId = "EM001";
            var meterId2 = "EM002";
            var accountId = "Acct87";
            var servicePointId = "servicePt1";
            var servicePointId2 = "servicePt2";
            var projected = Convert.ToDateTime("1/17/2016");
            var startDate = Convert.ToDateTime("12/17/2015");
            var endDate = Convert.ToDateTime("12/22/2015");
            var requestedResolution = "fifteen";
            var startDateUtc = TimeZoneInfo.ConvertTimeToUtc(startDate,
                TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"));
            var endDateUtc = TimeZoneInfo.ConvertTimeToUtc(endDate,
                TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"));

            // mock ami 15min data
            var amiModel1 = TestHelpers.CreateMockAmi15AmiModel(clientId, accountId, meterId, servicePointId, startDateUtc, endDateUtc, projected);
            var amiModel2 = TestHelpers.CreateMockAmi15AmiModel(clientId, accountId, meterId2, servicePointId2, startDateUtc, endDateUtc, projected);
            _amiManager.Setup(m => m.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), meterId, clientId, It.IsAny<IDictionary<string, string>>())).Returns(amiModel1).Verifiable();
            _amiManager.Setup(m => m.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), meterId2, clientId, It.IsAny<IDictionary<string, string>>())).Returns(amiModel2).Verifiable();

            int testNoOfDays;
            var readings = TestHelpers.CreateMockAmi15Reading(startDate, endDate.AddDays(-1));
            _amiManager.Setup(
                a =>
                    a.GetReadings(It.IsAny<IEnumerable<TallAmiModel>>(), out testNoOfDays, It.IsAny<bool>())).Returns(readings).Verifiable();

            var model = new ConsumptionModel(_container);

            var result = model.GetReadings(clientId, accountId, new List<string> { meterId, meterId2 }, startDateUtc, endDateUtc, requestedResolution);//, out availableResolution, out amiUomId, out amiCommodityId);

            Assert.IsNotNull(result);
            var sortedResult = result.Result.Readings.OrderByDescending(r => r.Timestamp).ToList();
            Assert.AreEqual(readings[3].Quantity * 2, sortedResult[3].Quantity);
            Assert.AreEqual(readings[3].Timestamp, sortedResult[3].Timestamp);
            _amiManager.Verify();
        }

        [TestMethod()]
        public void ConsumptionV1_GetReadingsTest_AggregatedInterval30ReadingsTest()
        {
            var clientId = 87;
            var meterId = "EM001";
            var meterId2 = "EM002";
            var accountId = "Acct87";
            var servicePointId = "servicePt1";
            var servicePointId2 = "servicePt2";
            var projected = Convert.ToDateTime("1/17/2016");
            var startDate = Convert.ToDateTime("12/17/2015");
            var endDate = Convert.ToDateTime("12/22/2015");
            var requestedResolution = "thirty";
            var startDateUtc = TimeZoneInfo.ConvertTimeToUtc(startDate,
                TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"));
            var endDateUtc = TimeZoneInfo.ConvertTimeToUtc(endDate,
                TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"));

            // mock ami 30min data
            var amiModel1 = TestHelpers.CreateMockAmi30Model(clientId, accountId, meterId, servicePointId, startDateUtc, endDateUtc, projected);
            var amiModel2 = TestHelpers.CreateMockAmi30Model(clientId, accountId, meterId2, servicePointId2, startDateUtc, endDateUtc, projected);
            _amiManager.Setup(m => m.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), meterId, clientId, It.IsAny<IDictionary<string, string>>())).Returns(amiModel1).Verifiable();
            _amiManager.Setup(m => m.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), meterId2, clientId, It.IsAny<IDictionary<string, string>>())).Returns(amiModel2).Verifiable();

            int testNoOfDays;
            var readings = TestHelpers.CreateMockAmi30Reading(startDate, endDate.AddDays(-1));
            _amiManager.Setup(
                    a =>
                        a.GetReadings(It.IsAny<IEnumerable<TallAmiModel>>(), out testNoOfDays, It.IsAny<bool>()))
                .Returns(readings)
                .Verifiable();

            var model = new ConsumptionModel(_container);
            var result = model.GetReadings(clientId, accountId, new List<string> { meterId, meterId2 }, startDateUtc, endDateUtc, requestedResolution);//, out availableResolution, out amiUomId, out amiCommodityId);

            Assert.IsNotNull(result);
            var sortedResult = result.Result.Readings.OrderByDescending(r => r.Timestamp).ToList();
            Assert.AreEqual(readings[3].Quantity * 2, sortedResult[3].Quantity);
            Assert.AreEqual(readings[3].Timestamp, sortedResult[3].Timestamp);
            _amiManager.Verify();
        }

        [TestMethod()]
        public void ConsumptionV1_GetReadingsTest_AggregatedInterval60ReadingsTest()
        {
            var clientId = 87;
            var meterId = "EM001";
            var meterId2 = "EM002";
            var accountId = "Acct87";
            var servicePointId = "servicePt1";
            var servicePointId2 = "servicePt2";
            var projected = Convert.ToDateTime("1/17/2016");
            var startDate = Convert.ToDateTime("12/17/2015");
            var endDate = Convert.ToDateTime("12/22/2015");
            var requestedResolution = "hour";
            var startDateUtc = TimeZoneInfo.ConvertTimeToUtc(startDate,
                TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"));
            var endDateUtc = TimeZoneInfo.ConvertTimeToUtc(endDate,
                TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"));
            
            // mock ami hourly data
            var amiModel1 = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterId, servicePointId, startDateUtc, endDateUtc, projected);
            var amiModel2 = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterId2, servicePointId2, startDateUtc, endDateUtc, projected);
            _amiManager.Setup(m => m.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), meterId, clientId, It.IsAny<IDictionary<string, string>>())).Returns(amiModel1).Verifiable();
            _amiManager.Setup(m => m.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), meterId2, clientId, It.IsAny<IDictionary<string, string>>())).Returns(amiModel2).Verifiable();
            int testNoOfDays;
            var reading = TestHelpers.CreateMockAmiHourlyReading(startDate, endDate.AddDays(-1));
            _amiManager.Setup(
                    a =>
                        a.GetReadings(It.IsAny<IEnumerable<TallAmiModel>>(), out testNoOfDays, It.IsAny<bool>()))
                .Returns(reading)
                .Verifiable();

            var model = new ConsumptionModel(_container);
            var result = model.GetReadings(clientId, accountId, new List<string> { meterId, meterId2 }, startDateUtc, endDateUtc, requestedResolution);//, out availableResolution, out amiUomId, out amiCommodityId);

            Assert.IsNotNull(result);
            var sortedResult = result.Result.Readings.OrderByDescending(r => r.Timestamp).ToList();
            Assert.AreEqual(reading[3].Quantity * 2, sortedResult[3].Quantity);
            Assert.AreEqual(reading[3].Timestamp, sortedResult[3].Timestamp);
            _amiManager.Verify();
        }


        [TestMethod, TestCategory("Consumption"), TestCategory("BVT")]
        public void ConsumptionV1_GetConsumption_Hour_TimeZone_PassInUtcTime_Test()
        {

            var clientId = 87;
            var customerId = "BTD001";
            var servicePointId = "servicePt1";
            var premiseId = "BTD001a1p1";
            var accountId = "BTD001a1";
            var meterId = "EM001";
            var projected = DateTime.SpecifyKind(Convert.ToDateTime("1/17/2016"), DateTimeKind.Utc);
            var startDate = DateTime.SpecifyKind(Convert.ToDateTime("10/17/2015"), DateTimeKind.Utc);
            var endDate = DateTime.SpecifyKind(Convert.ToDateTime("1/17/2016"), DateTimeKind.Utc);
            var requestedResolution = "hour";


            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };

            var request = new ConsumptionRequest
            {
                CustomerId = customerId,
                PremiseId = premiseId,
                AccountId = accountId,
                MeterIds = meterId,
                StartDate = startDate,
                EndDate = endDate,
                Count = 0,
                ResolutionKey = requestedResolution,
                IncludeResolutions = true,
                CommodityKey = "electric"
            };

            // mock ami hourly data
            var amiModel1 = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterId, servicePointId, startDate, endDate, projected);
            _amiManager.Setup(m => m.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), meterId, clientId, It.IsAny<IDictionary<string, string>>())).Returns(amiModel1).Verifiable();
            var readings = TestHelpers.CreateMockAmiHourlyReading(startDate.ToLocalTime(), endDate.ToLocalTime());
            int testNoOfDays;
            _amiManager.Setup(
                    a =>
                        a.GetReadings(It.IsAny<IEnumerable<TallAmiModel>>(), out testNoOfDays, It.IsAny<bool>()))
                .Returns(readings)
                .Verifiable();
            var model = new ConsumptionModel(_container);
            var result = model.GetConsumption(clientUser, request);


            Assert.IsNotNull(result);
            Assert.AreEqual("month,day,hour", result.ResolutionsAvailable);
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(startDate.ToLocalTime(), result.Data[0].DateTime);
            _amiManager.Verify();
        }


        [TestMethod, TestCategory("Consumption"), TestCategory("BVT")]
        public void ConsumptionV1_GetConsumption_Hour_TimeZone_PassInUnSpecTime_Test()
        {

            var clientId = 87;
            var customerId = "BTD001";
            var premiseId = "BTD001a1p1";
            var accountId = "BTD001a1";
            var servicePointId = "servicePt1";
            var meterId = "EM001";
            var projected = Convert.ToDateTime("1/17/2016");
            var startDate = Convert.ToDateTime("10/17/2015");
            var endDate = Convert.ToDateTime("1/17/2016");
            var requestedResolution = "hour";

            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };

            var request = new ConsumptionRequest
            {
                CustomerId = customerId,
                PremiseId = premiseId,
                AccountId = accountId,
                MeterIds = meterId,
                StartDate = startDate,
                EndDate = endDate,
                Count = 0,
                ResolutionKey = requestedResolution,
                IncludeResolutions = true,
                CommodityKey = "electric"
            };

            // mock ami hourly data
            var amiModel1 = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterId, servicePointId, DateTime.SpecifyKind(startDate, DateTimeKind.Utc), DateTime.SpecifyKind(endDate, DateTimeKind.Utc), projected);
            _amiManager.Setup(m => m.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), meterId, clientId, It.IsAny<IDictionary<string, string>>())).Returns(amiModel1).Verifiable();
            var readings = TestHelpers.CreateMockAmiHourlyReading(DateTime.SpecifyKind(startDate, DateTimeKind.Utc).ToLocalTime(), DateTime.SpecifyKind(endDate, DateTimeKind.Utc).ToLocalTime());
            int testNoOfDays;
            _amiManager.Setup(
                    a =>
                        a.GetReadings(It.IsAny<IEnumerable<TallAmiModel>>(), out testNoOfDays, It.IsAny<bool>()))
                .Returns(readings)
                .Verifiable();
            var model = new ConsumptionModel(_container);
            var result = model.GetConsumption(clientUser, request);


            Assert.IsNotNull(result);
            Assert.AreEqual("month,day,hour", result.ResolutionsAvailable);
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(DateTime.SpecifyKind(startDate, DateTimeKind.Utc).ToLocalTime(), result.Data[0].DateTime);
            _amiManager.Verify();
        }


        [TestMethod, TestCategory("Consumption"), TestCategory("BVT")]
        public void ConsumptionV1_GetConsumption_Hour_TimeZone_PassInLocalTime_Test()
        {

            var clientId = 87;
            var customerId = "BTD001";
            var premiseId = "BTD001a1p1";
            var accountId = "BTD001a1";
            var servicePointId = "servicePt1";
            var meterId = "EM001";
            var projected = DateTime.SpecifyKind(Convert.ToDateTime("1/17/2016"), DateTimeKind.Local);
            var startDate = DateTime.SpecifyKind(Convert.ToDateTime("10/17/2015"), DateTimeKind.Local);
            var endDate = DateTime.SpecifyKind(Convert.ToDateTime("1/17/2016"), DateTimeKind.Local);
            var requestedResolution = "hour";

            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };

            var request = new ConsumptionRequest
            {
                CustomerId = customerId,
                PremiseId = premiseId,
                AccountId = accountId,
                MeterIds = meterId,
                StartDate = startDate,
                EndDate = endDate,
                Count = 0,
                ResolutionKey = requestedResolution,
                IncludeResolutions = true,
                CommodityKey = "electric"
            };

            var utcStart = startDate.ToUniversalTime();
            var utcEnd = endDate.ToUniversalTime();

            // mock ami hourly data
            var amiModel1 = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterId, servicePointId, utcStart, utcEnd, projected);
            _amiManager.Setup(m => m.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), meterId, clientId, It.IsAny<IDictionary<string, string>>())).Returns(amiModel1).Verifiable();
            var readings = TestHelpers.CreateMockAmiHourlyReading(startDate, endDate);
            int testNoOfDays;
            _amiManager.Setup(
                    a =>
                        a.GetReadings(It.IsAny<IEnumerable<TallAmiModel>>(), out testNoOfDays, It.IsAny<bool>()))
                .Returns(readings)
                .Verifiable();
            var model = new ConsumptionModel(_container);
            var result = model.GetConsumption(clientUser, request);


            Assert.IsNotNull(result);
            Assert.AreEqual("month,day,hour", result.ResolutionsAvailable);
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(startDate, result.Data[0].DateTime);
            _amiManager.Verify();
        }
        [TestMethod, TestCategory("Consumption"), TestCategory("BVT")]
        public void ConsumptionV1_GetConsumption_RecentHour_Test()
        {
            var clientId = 87;
            var customerId = "BTD001";
            var premiseId = "BTD001a1p1";
            var accountId = "BTD001a1";
            var servicePointId = "servicePt1";
            var meterId = "EM001";
            var projected = Convert.ToDateTime("1/17/2016");
            var startDate = Convert.ToDateTime("10/17/2015");
            var endDate = Convert.ToDateTime("1/17/2016");
            var startDateUtc = TimeZoneInfo.ConvertTimeToUtc(startDate,
                TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"));
            var endDateUtc = TimeZoneInfo.ConvertTimeToUtc(endDate,
                TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"));
            var requestedResolution = "hour";

            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };

            var request = new ConsumptionRequest
            {
                CustomerId = customerId,
                PremiseId = premiseId,
                AccountId = accountId,
                MeterIds = meterId,
                StartDate = startDateUtc,
                EndDate = endDateUtc,
                Count = 5,
                ResolutionKey = requestedResolution,
                IncludeResolutions = true,
                CommodityKey = "electric"
            };

            // mock ami hourly data
            var amiModel1 = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterId, servicePointId, startDateUtc, endDateUtc, projected);
            _amiManager.Setup(m => m.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), meterId, clientId, It.IsAny<IDictionary<string, string>>())).Returns(amiModel1).Verifiable();
            var readings = TestHelpers.CreateMockAmiHourlyReading(startDateUtc.ToLocalTime(), endDateUtc.ToLocalTime());
            int testNoOfDays;
            _amiManager.Setup(
                    a =>
                        a.GetReadings(It.IsAny<IEnumerable<TallAmiModel>>(), out testNoOfDays, It.IsAny<bool>()))
                .Returns(readings)
                .Verifiable();

            var model = new ConsumptionModel(_container);
            var result = model.GetConsumption(clientUser, request);


            Assert.IsNotNull(result);
            Assert.AreEqual("month,day,hour", result.ResolutionsAvailable);
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(5, result.Data.Count);
            Assert.AreEqual(
                Convert.ToDecimal(readings.Where(r => r.Timestamp == result.Data[4].DateTime)
                    .Sum(r => r.Quantity)), result.Data[4].Value);
            _amiManager.Verify();
        }

        [TestMethod, TestCategory("Consumption"), TestCategory("BVT")]
        public void ConsumptionV1_GetConsumption_Day_Test()
        {
            var clientId = 87;
            var customerId = "BTD001";
            var premiseId = "BTD001a1p1";
            var accountId = "BTD001a1";
            var servicePointId = "servicePt1";
            var meterId = "EM001";
            var projected = Convert.ToDateTime("1/17/2016");
            var startDate = Convert.ToDateTime("12/17/2015");
            var endDate = Convert.ToDateTime("1/17/2016");
            var requestedResolution = "day";

            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };

            var request = new ConsumptionRequest
            {
                CustomerId = customerId,
                PremiseId = premiseId,
                AccountId = accountId,
                MeterIds = meterId,
                StartDate = startDate,
                EndDate = endDate,
                Count = 0,
                ResolutionKey = requestedResolution,
                IncludeResolutions = true,
                CommodityKey = "electric"
            };

            // mock ami hourly data
            var amiModel1 = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterId, servicePointId, startDate, endDate, projected);
            _amiManager.Setup(m => m.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), meterId, clientId, It.IsAny<IDictionary<string, string>>())).Returns(amiModel1).Verifiable();
            
            var model = new ConsumptionModel(_container);
            var result = model.GetConsumption(clientUser, request);


            Assert.IsNotNull(result);
            Assert.AreEqual("month,day,hour", result.ResolutionsAvailable);
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(TimeZoneInfo.ConvertTimeFromUtc(DateTime.SpecifyKind(startDate, DateTimeKind.Utc),
                TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time")).Date, result.Data[0].DateTime.Date);
            _amiManager.Verify();
        }



        [TestMethod, TestCategory("Consumption"), TestCategory("BVT")]
        public void ConsumptionV1_GetConsumption_RecentDay_Test()
        {
            var clientId = 87;
            var customerId = "BTD001";
            var premiseId = "BTD001a1p1";
            var accountId = "BTD001a1";
            var servicePointId = "servicePt1";
            var meterId = "EM001";
            var projected = Convert.ToDateTime("1/17/2016");
            var startDate = Convert.ToDateTime("12/17/2015");
            var endDate = Convert.ToDateTime("1/17/2016");
            var startDateUtc = TimeZoneInfo.ConvertTimeToUtc(startDate,
                TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"));
            var endDateUtc = TimeZoneInfo.ConvertTimeToUtc(endDate,
                TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"));
            var requestedResolution = "day";

            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };

            var request = new ConsumptionRequest
            {
                CustomerId = customerId,
                PremiseId = premiseId,
                AccountId = accountId,
                MeterIds = meterId,
                StartDate = startDateUtc,
                EndDate = endDateUtc,
                Count = 5,
                ResolutionKey = requestedResolution,
                IncludeResolutions = true,
                CommodityKey = "electric"
            };

            // mock ami hourly data
            var amiModel1 = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterId, servicePointId, startDateUtc, endDateUtc, projected);
            _amiManager.Setup(m => m.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), meterId, clientId, It.IsAny<IDictionary<string, string>>())).Returns(amiModel1).Verifiable();
           
            var model = new ConsumptionModel(_container);
            //var amiReadings = model.GetAmiReadings(clientId, meterId, startDate, endDate, requestedResolution, out intervalType);

            var result = model.GetConsumption(clientUser, request);

            var readings = TestHelpers.CreateMockAmiHourlyReading(startDate, endDate);

            Assert.IsNotNull(result);
            Assert.AreEqual("month,day,hour", result.ResolutionsAvailable);
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(5, result.Data.Count);
            Assert.AreEqual(Convert.ToDateTime("1/13/2016"), result.Data[0].DateTime.Date);
            Assert.AreEqual(
                Convert.ToDecimal(readings.Where(r => r.Timestamp.Date == result.Data[0].DateTime)
                    .Sum(r => r.Quantity)), result.Data[0].Value);
            Assert.AreEqual(
                Convert.ToDecimal(readings.Where(r => r.Timestamp.Date == result.Data[4].DateTime)
                    .Sum(r => r.Quantity)), result.Data[4].Value);

            _amiManager.Verify();
        }


        [TestMethod, TestCategory("Consumption"), TestCategory("BVT")]
        public void ConsumptionV1_GetConsumption_AggregatedRecentDailyReadingsTest()
        {
            var clientId = 87;
            var customerId = "BTD001";
            var premiseId = "BTD001a1p1";
            var meterId = "EM001";
            var meterId2 = "EM001";
            var accountId = "Acct87";
            var servicePointId = "servicePt1";
            var servicePointId2 = "servicePt2";
            var projected = Convert.ToDateTime("1/17/2016");
            var startDate = Convert.ToDateTime("12/17/2015");
            var endDate = Convert.ToDateTime("12/23/2015");  // end date not inclusive
            var startDateUtc = TimeZoneInfo.ConvertTimeToUtc(startDate,
                TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"));
            var endDateUtc = TimeZoneInfo.ConvertTimeToUtc(endDate,
                TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"));
            var requestedResolution = "day";

            // mock ami hourly data
            var amiModel1 = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterId, servicePointId, startDateUtc, endDateUtc.AddDays(-1), projected);
            var amiModel2 = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterId2, servicePointId2, startDateUtc, endDateUtc.AddDays(-1), projected);
            _amiManager.Setup(m => m.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), meterId, clientId, It.IsAny<IDictionary<string, string>>())).Returns(amiModel1).Verifiable();
            _amiManager.Setup(m => m.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), meterId2, clientId, It.IsAny<IDictionary<string, string>>())).Returns(amiModel2).Verifiable();

            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };

            var request = new ConsumptionRequest
            {
                CustomerId = customerId,
                PremiseId = premiseId,
                AccountId = accountId,
                MeterIds = meterId + "," + meterId2,
                StartDate = startDateUtc,
                EndDate = endDateUtc,
                Count = 5,
                ResolutionKey = requestedResolution,
                IncludeResolutions = true,
                CommodityKey = "electric"
            };

            //var model = new ConsumptionModel(_container);
            //var result = model.GetReadings(clientId, accountId, new List<string> { meterId, meterId2 }, startDateUtc, endDateUtc, 5, requestedResolution, out availableResolution, out amiUomId, out amiCommodityId);

            var model = new ConsumptionModel(_container);
            var result = model.GetConsumption(clientUser, request);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(5, result.Data.Count);
            Assert.AreEqual(Convert.ToDecimal(48), result.Data[3].Value);
            Assert.AreEqual(Convert.ToDateTime("12/18/2015"), result.Data[0].DateTime);
        }


        [TestMethod, TestCategory("Consumption"), TestCategory("BVT")]
        public void ConsumptionV1_GetConsumption_AggregatedRecentMonthlyReadingsTest()
        {
            var clientId = 87;
            var customerId = "BTD001";
            var premiseId = "BTD001a1p1";
            var meterId = "EM001";
            var meterId2 = "EM001";
            var accountId = "Acct87";
            var servicePointId = "servicePt1";
            var servicePointId2 = "servicePt2";
            var projected = Convert.ToDateTime("1/17/2016");
            var startDate = Convert.ToDateTime("10/17/2015");
            var endDate = Convert.ToDateTime("1/17/2016");
            var requestedResolution = "month";
            var startDateUtc = TimeZoneInfo.ConvertTimeToUtc(startDate,
                TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"));
            var endDateUtc = TimeZoneInfo.ConvertTimeToUtc(endDate,
                TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"));

            // mock ami hourly data
            var amiModel1 = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterId, servicePointId, startDateUtc, endDateUtc, projected);
            var amiModel2 = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterId2, servicePointId2, startDateUtc, endDateUtc, projected);
            _amiManager.Setup(m => m.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), meterId, clientId, It.IsAny<IDictionary<string, string>>())).Returns(amiModel1).Verifiable();
            _amiManager.Setup(m => m.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), meterId2, clientId, It.IsAny<IDictionary<string, string>>())).Returns(amiModel2).Verifiable();

            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };

            var request = new ConsumptionRequest
            {
                CustomerId = customerId,
                PremiseId = premiseId,
                AccountId = accountId,
                MeterIds = meterId + "," + meterId2,
                StartDate = startDateUtc,
                EndDate = endDateUtc,
                Count = 2,
                ResolutionKey = requestedResolution,
                IncludeResolutions = true,
                CommodityKey = "electric"
            };

            var model = new ConsumptionModel(_container);
            var result = model.GetConsumption(clientUser, request);

            var readings = TestHelpers.CreateMockAmiHourlyReading(startDate, endDate);
            var expectedTotal = readings.Where(r => r.Timestamp.Month == 12 && r.Timestamp.Year == 2015).Sum(r => r.Quantity);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(2, result.Data.Count());
            Assert.AreEqual(Convert.ToDateTime("12/1/2015"), result.Data[0].DateTime);
            Assert.AreEqual(Convert.ToDecimal(expectedTotal * 2), result.Data[0].Value);

        }

        [TestMethod, TestCategory("Consumption"), TestCategory("BVT")]
        public void ConsumptionV1_GetConsumption_AggregatedRecentInterval15ReadingsTest()
        {
            var clientId = 87;
            var customerId = "BTD001";
            var premiseId = "BTD001a1p1";
            var meterId = "EM001";
            var meterId2 = "EM002";
            var accountId = "Acct87";
            var servicePointId = "servicePt1";
            var servicePointId2 = "servicePt2";
            var projected = Convert.ToDateTime("1/17/2016");
            var startDate = Convert.ToDateTime("12/17/2015");
            var endDate = Convert.ToDateTime("12/22/2015");
            var requestedResolution = "fifteen";
            var startDateUtc = TimeZoneInfo.ConvertTimeToUtc(startDate,
                TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"));
            var endDateUtc = TimeZoneInfo.ConvertTimeToUtc(endDate,
                TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"));

            // mock ami 15min data
            var amiModel1 = TestHelpers.CreateMockAmi15AmiModel(clientId, accountId, meterId, servicePointId, startDateUtc, endDateUtc, projected);
            var amiModel2 = TestHelpers.CreateMockAmi15AmiModel(clientId, accountId, meterId2, servicePointId2, startDateUtc, endDateUtc, projected);
            _amiManager.Setup(m => m.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), meterId, clientId, It.IsAny<IDictionary<string, string>>())).Returns(amiModel1).Verifiable();
            _amiManager.Setup(m => m.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), meterId2, clientId, It.IsAny<IDictionary<string, string>>())).Returns(amiModel2).Verifiable();
             int testNoOfDays;
            var readings = TestHelpers.CreateMockAmi15Reading(startDate, endDate);
            _amiManager.Setup(
                    a =>
                        a.GetReadings(It.IsAny<IEnumerable<TallAmiModel>>(), out testNoOfDays, It.IsAny<bool>()))
                .Returns(readings)
                .Verifiable();

            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };

            var request = new ConsumptionRequest
            {
                CustomerId = customerId,
                PremiseId = premiseId,
                AccountId = accountId,
                MeterIds = meterId + "," + meterId2,
                StartDate = startDateUtc,
                EndDate = endDateUtc,
                Count = 5,
                ResolutionKey = requestedResolution,
                IncludeResolutions = true,
                CommodityKey = "electric"
            };

            var model = new ConsumptionModel(_container);
            var result = model.GetConsumption(clientUser, request);


            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(5, result.Data.Count());
            var expectedResult = readings.OrderByDescending(r => r.Timestamp).ToList();
            Assert.AreEqual(Convert.ToDecimal(expectedResult[3].Quantity * 2), result.Data[3].Value);
            _amiManager.Verify();
        }

        [TestMethod, TestCategory("Consumption"), TestCategory("BVT")]
        public void ConsumptionV1_GetConsumption_Month_Test()
        {
            var clientId = 87;
            var customerId = "BTD001";
            var premiseId = "BTD001a1p1";
            var accountId = "BTD001a1";
            var servicePointId = "servicePt1";
            var meterId = "EM001";
            var projected = Convert.ToDateTime("1/17/2016");
            var startDate = Convert.ToDateTime("10/17/2015");
            var endDate = Convert.ToDateTime("1/17/2016");
            var requestedResolution = "month";

            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };

            var request = new ConsumptionRequest
            {
                CustomerId = customerId,
                PremiseId = premiseId,
                AccountId = accountId,
                MeterIds = meterId,
                StartDate = startDate,
                EndDate = endDate,
                Count = 0,
                ResolutionKey = requestedResolution,
                IncludeResolutions = true,
                CommodityKey = "electric"
            };
            // mock ami hourly data
            var amiModel1 = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterId, servicePointId, startDate, endDate, projected);
            _amiManager.Setup(m => m.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), meterId, clientId, It.IsAny<IDictionary<string, string>>())).Returns(amiModel1).Verifiable();
            

            var model = new ConsumptionModel(_container);
            var result = model.GetConsumption(clientUser, request);


            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(4, result.Data.Count());
            Assert.AreEqual(Convert.ToDateTime("12/1/2015"), result.Data[2].DateTime);
            Assert.AreEqual("month,day,hour", result.ResolutionsAvailable);
            _amiManager.Verify();
        }

        [TestMethod, TestCategory("Consumption"), TestCategory("BVT")]
        public void ConsumptionV1_GetConsumption_RecentMonth_Test()
         
        {
            var clientId = 87;
            var customerId = "BTD001";
            var premiseId = "BTD001a1p1";
            var accountId = "BTD001a1";
            var servicePointId = "servicePt1";
            var meterId = "EM001";
            var projected = Convert.ToDateTime("1/17/2016");
            var requestedResolution = "month";
            var startDateUtc = new DateTime(2015, 10, 17, 0, 0, 0, DateTimeKind.Utc);
            var endDateUtc = new DateTime(2016, 1, 18, 0, 0, 0, DateTimeKind.Utc);// end date not inclusive

            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };

            var request = new ConsumptionRequest
            {
                CustomerId = customerId,
                PremiseId = premiseId,
                AccountId = accountId,
                MeterIds = meterId,
                StartDate = startDateUtc,
                EndDate = endDateUtc,
                Count = 2,
                ResolutionKey = requestedResolution,
                IncludeResolutions = true,
                CommodityKey = "electric"
            };

            // mock ami hourly data
            var amiModel1 = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterId, servicePointId, startDateUtc, endDateUtc.AddDays(-1), projected);
            _amiManager.Setup(m => m.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), meterId, clientId, It.IsAny<IDictionary<string, string>>())).Returns(amiModel1).Verifiable();
            
            var model = new ConsumptionModel(_container);
            var result = model.GetConsumption(clientUser, request);

            var readings = TestHelpers.CreateMockAmiHourlyReading(startDateUtc, endDateUtc.AddDays(-1));

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(2, result.Data.Count());
            Assert.AreEqual(Convert.ToDateTime("12/1/2015"), result.Data[0].DateTime);
            Assert.AreEqual("month,day,hour", result.ResolutionsAvailable);
            Assert.AreEqual(
                Convert.ToDecimal(readings
                    .Where(r => TimeZoneInfo.ConvertTimeFromUtc(r.Timestamp,
                                    TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time")).Month ==
                                result.Data[1].DateTime.Month &&
                                TimeZoneInfo.ConvertTimeFromUtc(r.Timestamp,
                                    TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time")).Year ==
                                result.Data[1].DateTime.Year)
                    .Sum(r => r.Quantity)), result.Data[1].Value);
            Assert.AreEqual(
                Convert.ToDecimal(readings
                    .Where(r => TimeZoneInfo.ConvertTimeFromUtc(r.Timestamp,
                                    TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time")).Month ==
                                result.Data[0].DateTime.Month &&
                                TimeZoneInfo.ConvertTimeFromUtc(r.Timestamp,
                                    TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time")).Year ==
                                result.Data[0].DateTime.Year)
                    .Sum(r => r.Quantity)), result.Data[0].Value);
            _amiManager.Verify();
        }

        [TestMethod, TestCategory("Consumption"), TestCategory("BVT")]
        public void ConsumptionV1_GetConsumption_NoData_Test()
        {
            var clientId = 87;
            var customerId = "BTD001";
            var premiseId = "BTD001a1p1";
            var accountId = "BTD001a1";
            var meterId = "EM001";
            var startDate = Convert.ToDateTime("10/17/2015");
            var endDate = Convert.ToDateTime("1/17/2016");
            var requestedResolution = "day";

            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };

            var request = new ConsumptionRequest
            {
                CustomerId = customerId,
                PremiseId = premiseId,
                AccountId = accountId,
                MeterIds = meterId,
                StartDate = startDate,
                EndDate = endDate,
                Count = 0,
                ResolutionKey = requestedResolution,
                IncludeResolutions = true,
                CommodityKey = "electric"
            };

            var model = new ConsumptionModel(_container);
            var result = model.GetConsumption(clientUser, request);


            Assert.IsNotNull(result);
            Assert.IsNull(result.Data);
        }

        [TestMethod()]
        public void GetWeatherDataTest()
        {
            var clientId = 87;
            var zipcode = "02481";
            var startDate = Convert.ToDateTime("12/17/2015");
            var endDate = Convert.ToDateTime("12/22/2015");


            Mapper.CreateMap<WeatherDb, ConsumptionWeather>()
                .ForMember(dest => dest.Date, opts => opts.MapFrom(src => src.WeatherReadingDate))
                .ForMember(dest => dest.AvgTemp, opts => opts.MapFrom(src => src.AvgTemp))
                .ForAllUnmappedMembers(o => o.Ignore());

            var model = new ConsumptionModel(_container, _insightRepository.Object);

            // mock weather data
            var weatherReadings = TestHelpers.CreateMockWeatherData(startDate, endDate);
            _insightRepository.Setup(i => i.GetWeather(clientId, It.Is<WeatherRequest>(r => r.ZipCode == zipcode && r.StartDate == startDate && r.EndDate == endDate))).Returns(weatherReadings);

            var result = model.GetWeatherData(clientId, startDate, endDate, zipcode, "us", "day");

            Assert.IsNotNull(result.Result.Weather);
            Assert.AreEqual(6, result.Result.Weather.Count);
            Assert.AreEqual(startDate, result.Result.Weather[0].Date);
        }

        [TestMethod()]
        public void ConsumptionV1_GetReadingsTest_DailyInterval_NoTou()
        {
            _container.RegisterInstance(_amiManager.Object);

            var clientId = 87;
            var accountId = "BTD001a1";
            var servicePointId = "SP_BTD001a1";
            var meterId = "EM001";
            var startDate = Convert.ToDateTime("12/17/2015");
            var endDate = Convert.ToDateTime("12/22/2015");
            var projected = Convert.ToDateTime("1/17/2016");
            var requestedResolution = "day";
            var startDateUtc = TimeZoneInfo.ConvertTimeToUtc(startDate,
                TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"));
            var endDateUtc = TimeZoneInfo.ConvertTimeToUtc(endDate,
                TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"));

            var daysOfData = (endDate.Date - startDate.Date).TotalDays + 1;

            // mock ami daily data
            var amiModel1 = TestHelpers.CreateMockAmiDailyModel(clientId, accountId, meterId, servicePointId, startDateUtc, endDateUtc, projected);
            _amiManager.Setup(m => m.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), meterId, clientId, It.IsAny<IDictionary<string, string>>())).Returns(amiModel1).Verifiable();
            
            var readings = TestHelpers.CreateMockEnumerableAmiDailyData(clientId, accountId, meterId, servicePointId, startDate, endDate, projected);
            var amiDailyIntervalModels = readings as IList<AmiDailyIntervalModel> ?? readings.ToList();
            // mock ami hourly rate model data
            var rmReadings = TestHelpers.ConvertDailyToReadings(amiDailyIntervalModels);
            int testNoOfDays;
            _amiManager.Setup(
                    a =>
                        a.GetReadings(It.IsAny<IEnumerable<TallAmiModel>>(), out testNoOfDays, It.IsAny<bool>()))
                .Returns(rmReadings)
                .Verifiable();

            var model = new ConsumptionModel(_container);
            var result = model.GetReadings(clientId, accountId, new List<string> { meterId }, startDateUtc, endDateUtc, requestedResolution);//, out availableResolution, out amiUomId, out amiCommodityId);

            Assert.IsNotNull(result);
            Assert.AreEqual(daysOfData, result.Result.Readings.Count);
            Assert.AreEqual(startDate, result.Result.Readings[0].Timestamp);
            Assert.AreEqual(startDate.AddDays(1), result.Result.Readings[1].Timestamp);
            _amiManager.Verify();
        }

        [TestMethod()]
        public void ConsumptionV1_GetReadingsTest_DailyInterval_Tou()
        {
            _container.RegisterInstance(_amiManager.Object);

            var clientId = 87;
            var accountId = "BTD001a1";
            var servicePointId = "SP_BTD001a1";
            var meterId = "EM001";
            var startDate = Convert.ToDateTime("12/17/2015");
            var endDate = Convert.ToDateTime("12/22/2015");
            var projected = Convert.ToDateTime("1/17/2016");
            var requestedResolution = "day";
            var startDateUtc = TimeZoneInfo.ConvertTimeToUtc(startDate,
                TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"));
            var endDateUtc = TimeZoneInfo.ConvertTimeToUtc(endDate,
                TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"));

            var daysOfData = (endDate.Date - startDate.Date).TotalDays + 1;

            // mock ami daily data
            var amiModel1 = TestHelpers.CreateMockAmiDailyModelWithTou(clientId, accountId, meterId, servicePointId, startDateUtc, endDateUtc, projected);
            _amiManager.Setup(m => m.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), meterId, clientId, It.IsAny<IDictionary<string, string>>())).Returns(amiModel1).Verifiable();
            
            // mock ami hourly data
            var readings = TestHelpers.CreateMockEnumerableAmiDailyDataTouWithTotal(clientId, accountId, meterId, servicePointId, startDate, endDate, projected);
            var amiDailyIntervalModels = readings as IList<AmiDailyIntervalModel> ?? readings.ToList();
            // mock ami hourly rate model data
            var rmReadings = TestHelpers.ConvertDailyToReadings(amiDailyIntervalModels);
            int testNoOfDays;
            _amiManager.Setup(
                    a =>
                        a.GetReadings(It.IsAny<IEnumerable<TallAmiModel>>(), out testNoOfDays, It.IsAny<bool>()))
                .Returns(rmReadings)
                .Verifiable();

            var model = new ConsumptionModel(_container);
            var result = model.GetReadings(clientId, accountId, new List<string> { meterId }, startDate, endDate, requestedResolution);//, out availableResolution, out amiUomId, out amiCommodityId);

            Assert.IsNotNull(result);
            Assert.AreEqual(daysOfData * 6, result.Result.Readings.Count);
            Assert.AreEqual(startDate, result.Result.Readings[0].Timestamp);

        }


        [TestMethod, TestCategory("Consumption"), TestCategory("BVT")]
        public void ConsumptionV1_GetComsuption_RecentDaily_Tou()
        {
            _container.RegisterInstance(_amiManager.Object);

            var clientId = 87;
            var customerId = "BTD001";
            var premiseId = "BTD001a1p1";
            var filterValues = new Dictionary<string, string>();
            filterValues.Add("AccountId", "BTD001a1");
            filterValues.Add("ServicePointId", "SP_BTD001a1");
            var accountId = "BTD001a1";
            var servicePointId = "SP_BTD001a1";
            var meterId = "EM001";
            var startDate = Convert.ToDateTime("12/17/2015");
            var endDate = Convert.ToDateTime("12/22/2015");
            var projected = Convert.ToDateTime("1/17/2016");
            var requestedResolution = "day";
            var startDateUtc = TimeZoneInfo.ConvertTimeToUtc(startDate,
                TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"));
            var endDateUtc = TimeZoneInfo.ConvertTimeToUtc(endDate,
                TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"));

            // mock ami daily data
            var amiModel1 = TestHelpers.CreateMockAmiDailyModelWithTou(clientId, accountId, meterId, servicePointId, startDateUtc, endDateUtc, projected);
            _amiManager.Setup(m => m.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), meterId, clientId, It.IsAny<IDictionary<string,string>>())).Returns(amiModel1).Verifiable();


            // mock ami hourly data
            var readings = TestHelpers.CreateMockEnumerableAmiDailyDataTouWithTotal(clientId, accountId, meterId, servicePointId, startDate, endDate, projected);
            var amiDailyIntervalModels = readings as IList<AmiDailyIntervalModel> ?? readings.ToList();
            // mock ami hourly rate model data
            var rmReadings = TestHelpers.ConvertDailyToReadings(amiDailyIntervalModels);
            int testNoOfDays;
            _amiManager.Setup(
                    a =>
                        a.GetReadings(It.IsAny<IEnumerable<TallAmiModel>>(), out testNoOfDays, It.IsAny<bool>()))
                .Returns(rmReadings)
                .Verifiable();

            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };

            var request = new ConsumptionRequest
            {
                CustomerId = customerId,
                PremiseId = premiseId,
                AccountId = accountId,
                MeterIds = meterId,
                StartDate = startDateUtc,
                EndDate = endDateUtc,
                Count = 5,
                ResolutionKey = requestedResolution,
                IncludeResolutions = true,
                CommodityKey = "electric"
            };

            var model = new ConsumptionModel(_container);
            var result = model.GetConsumption(clientUser, request);

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(5, result.Data.Count);
            Assert.AreEqual(Convert.ToDateTime("12/18/2015"), result.Data[0].DateTime);
            Assert.AreEqual(Convert.ToDecimal(rmReadings.Where(r => r.Timestamp.Date == result.Data[0].DateTime.Date && r.TimeOfUse != Enums.TimeOfUse.Undefined).Sum(r => r.Quantity)), result.Data[0].Value);
            Assert.AreEqual(Convert.ToDecimal(rmReadings.Where(r => r.Timestamp.Date == result.Data[4].DateTime.Date && r.TimeOfUse != Enums.TimeOfUse.Undefined).Sum(r => r.Quantity)), result.Data[4].Value);

        }

    }
}
