﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AO.BusinessContracts;
using AO.Entities;
using AO.Registrar;
//using CE.ContentModel;
//using CE.AO.DataAccess;
using CE.Infrastructure;
using CE.Insights.Test.Helpers;
using CE.Models;
using CE.Models.Insights;
using CE.Models.Insights.EF;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;
using CE.AO.Business;
using AutoMapper;
using CE.AO.Models;
using CE.Insights.Models;
using SubscriptionModel = CE.AO.Models.SubscriptionModel;

//using Microsoft.WindowsAzure.Storage.Table;
//using CE.AO.Entities;
//using CE.ContentModel;

namespace CE.Insights.Test
{
    /// <summary>
    /// Unit Tests for CE.Insights prototype functionality
    /// </summary>
    [TestClass]
    public class UnitTest1
    {

        #region Constants for content
        private const string ClientSpace = "netnln2itvho";
        private const string ClientAccessToken = "c58ec8d495044174267c257f105914f7fb9567127a879b2419e1c3d405a54be2";
        //private const string DefaultUrl = "https://cdn.contentful.com";
        //private const string DefaultSpace = "gwdmhnzd2xyu";
        //private const string DefaultAccessToken = "130a40b027b6ecdccfc982f9d2898bd9bbdd462cc8b68f6555ce3f41666634fc";
        //private const string StringCeApplianceList1 = "CEApplianceList1";
        //private const string StringCeEnduseList1 = "CEEnduseList1";
        #endregion
            

        //note: move this to separate test file, exclude by category for now on ms build tasks where !DBTest
        [TestCategory("DBTest"), TestMethod]
        [Description("db test for verifying content controller version 1 returns content when include content is false and required fields type and keys supplied. Checks for content currency is not null on response- there are currently at least 2")]
        public void ContentV1_Get_DBContentCurrencyFoundSuccess_WhenOnlyRequiredFieldsandIncContentParamsSupplied()
        {
            //Arrange
            var controller = new Insights.Controllers.Version1.ContentController();
            var configuration = new HttpConfiguration();
            var request = new HttpRequestMessage();
            controller.Request = request;
            controller.Request.Properties["MS_HttpConfiguration"] = configuration;
            //add property for client user, normally from the stored procedure but hardcoded here for unit test purposes
            var clientUser = new ClientUser
            {
                ClientID = 87,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };

            controller.Request.Properties[CEConfiguration.CEClientUser] = clientUser;
            controller.Request.Properties[CEConfiguration.CELocale] = "es-ES";

            var contentRequest = new ContentRequest()
            {
                Refresh = false
            };
            contentRequest.Type = "Currency";
            contentRequest.Keys = "usd,eur";
            contentRequest.IncludeContent = true;
            Console.Out.WriteLine("Request");
            Console.Out.Write(JsonConvert.SerializeObject(contentRequest, Formatting.Indented));

            //Act
            var result = controller.Get(contentRequest);
            Assert.IsNotNull(result);

            Console.Out.WriteLine("");
            Console.Out.WriteLine("Response");
            Console.Out.Write(JsonConvert.SerializeObject(result.Content.ReadAsAsync<ContentResponse>().Result, Formatting.Indented));

            var resultsMessage = result.Content.ReadAsAsync<ContentResponse>().Result.Message;
            bool resultsCurrencynotnull = result.Content.ReadAsAsync<ContentResponse>().Result.Content.Currency != null;
            Assert.AreEqual("refresh false", resultsMessage);
            //currency content brought back. If nothing found is null on response which would be a failure
            Assert.AreEqual(true, resultsCurrencynotnull, "currency is null for type Currency and keys of usd and eur; expected currency values not returned");
        }


        [TestCategory("Version1"), TestCategory("Get"), TestCategory("EchoController"), TestCategory("Encryption"), TestCategory("BVT"), TestMethod]
        [Description("Simple unit test for verifying echo controller version 1 returns bad request when unencrypted string on the Enc=xxx request and no secret key")]
        public void EchoEncV1_Get_BadRequestReturned_WhenUnencryptEnc()
        {
            //Arrange
            Insights.Controllers.Version1.EchoController controller = new Insights.Controllers.Version1.EchoController();
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            // Act 
            var result = controller.Get("enc?Say=1&Name=Hello&Number=1");

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual("Bad Request", result.ReasonPhrase);

        }

        [TestCategory("Version1"), TestCategory("Get"), TestCategory("EndpointTrackingController"), TestCategory("BVT"), TestMethod]
        [Description("Simple unit test for verifying endpoint tracking controller version 1 returns response of includedetails false from mock repository (response) when includedetails is not provided in the request")]
        public void Endpointtrackingv1_get_responsedefaultincludedetailfalse_when_noincludedetailparam()
        {
            //Arrange
            var endpointTrackingRequest = new EndpointTrackingRequest();
            string startdate = "4/1/2014";
            DateTime startdt = Convert.ToDateTime(startdate);
            string enddate = "5/1/2014";
            DateTime enddt = Convert.ToDateTime(enddate);
            endpointTrackingRequest.EndDate = enddt;
            endpointTrackingRequest.StartDate = startdt;
            endpointTrackingRequest.Verb = "GET";
            var context = new TestInsightEntities();
            //Note: moq won't work without inherit interface for context  - 
            //just using the base class inheritance on it own without moq with constructor for the context for this non db test
            //var context = new Mock<TestInsightEntities>();
            //context.Setup(s => s.Clients.Add(new Client { ClientID = 87, Name = "TestDemo", Description = "TestDemoDescription" }));
            //context.Setup(s => s.Endpoints.Add(new CE.CE.Models.Insights.EF.Endpoint { EndpointID = 1, Name = "TestEndpoint1", ShortName = "testendpointv1", SortOrder = 1 }));
            //context.Setup(s => s.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 1, UserID = 1, EndpointID = 1, NewDate = new DateTime(2014, 5, 1, 10, 00, 00) }));
            //context.Setup(s => s.Users.Add( new User { UserID = 1, ClientID = 87, EnableInd = true }));
            context.Clients.Add(new Client { ClientID = 87, Name = "TestDemo", Description = "TestDemoDescription", RateCompanyID = 100, ReferrerID = 0, AuthType = Byte.MaxValue, EnableInd = false, NewDate = new DateTime(2014, 5, 1, 10, 00, 00), UpdDate = new DateTime(2015, 5, 1, 10, 00, 00) });
            context.Endpoints.Add(new CE.Models.Insights.EF.Endpoint { EndpointID = 1, Name = "TestEndpoint1", ShortName = "testendpointv1", SortOrder = 1 });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 1, UserID = 1, EndpointID = 1, NewDate = new DateTime(2014, 5, 1, 10, 00, 00) });
            context.Users.Add(new User { UserID = 1, ClientID = 87, EnableInd = true });
            // var testrepo = new InsightsEFRepository(context.Object);
            var testrepo = new InsightsEfRepository(context);
            var controller = new Insights.Controllers.Version1.EndpointTrackingController(testrepo);
            var configuration = new HttpConfiguration();
            var request = new HttpRequestMessage();
            controller.Request = request;
            controller.Request.Properties["MS_HttpConfiguration"] = configuration;
            //add property for client user, normally from the stored procedure but hardcoded here for unit test purposes
            var clientUser = new ClientUser
            {
                ClientID = 87,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };
            //add cms access key and cns space values for testing - normally from results of executing stored procedure 
            //but here assumes no db access - we don't care about the content returned
            controller.Request.Properties[CEConfiguration.CEClientUser] = clientUser;
            controller.Request.Properties[CEConfiguration.CELocale] = "en-US";

            //Act
            var result = controller.Get(endpointTrackingRequest);

            //Assert
            Assert.IsNotNull(result);

            //is include details false on response even though we did not provide it in the request
            var contentresponse = result.Content.ReadAsStringAsync().Result;
            Assert.IsTrue(contentresponse.Contains("\"IncludeDetails\":false"), "endpointtrackingv1 response failed as did not contain includedetails false");
        }

        [TestCategory("Version1"), TestCategory("Get"), TestCategory("EndpointTrackingController"), TestMethod]
        [Description("Simple unit test for verifying endpoint tracking controller version 1 returns response of pageindex 1 from repository (response) when pageindex 1 is on request and include details set to yes")]
        public void Endpointtrackingv1_get_responsedpageindex1_when_pageindex1onrequest()
        {
            //Arrange
            var endpointTrackingRequest = new EndpointTrackingRequest();
            endpointTrackingRequest.EndDate = new DateTime(2014, 5, 1, 14, 30, 00, 00, DateTimeKind.Utc);
            endpointTrackingRequest.StartDate = new DateTime(2014, 5, 1, 13, 00, 00, 00, DateTimeKind.Utc);
            endpointTrackingRequest.IncludeDetails = true;
            var context = new TestInsightEntities();
            context.Clients.Add(new Client { ClientID = 87, Name = "TestDemo", Description = "TestDemoDescription" });
            context.Endpoints.Add(new CE.Models.Insights.EF.Endpoint { EndpointID = 1, Name = "TestEndpoint1", ShortName = "testendpoint1v1", SortOrder = 1 });
            context.Endpoints.Add(new CE.Models.Insights.EF.Endpoint { EndpointID = 2, Name = "TestEndpoint2", ShortName = "testendpoint2v1", SortOrder = 1 });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 1, UserID = 1, EndpointID = 1, NewDate = new DateTime(2014, 5, 1, 14, 00, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 5, 1, 14, 00, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine1", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "12", Query = "?Say=QAtesttext&Name=Test123&Number=6", Verb = "GET", IncludeInd = true, SourceIPAddress = "111.123", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 2, UserID = 1, EndpointID = 1, NewDate = new DateTime(2014, 5, 1, 14, 30, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 5, 1, 14, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine2", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "13", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            //this one it should not retrieve as is outside the date specified on the request
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 3, UserID = 1, EndpointID = 1, NewDate = new DateTime(2014, 5, 1, 16, 30, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 5, 1, 16, 00, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine2", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "13", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });

            //2nd endpoint
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 4, UserID = 1, EndpointID = 2, NewDate = new DateTime(2014, 5, 1, 14, 15, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 5, 1, 14, 15, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine1", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/billdisagg", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "12", Query = "?Say=QAtesttext&Name=Test123&Number=6", Verb = "GET", IncludeInd = true, SourceIPAddress = "111.123", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 5, UserID = 1, EndpointID = 2, NewDate = new DateTime(2014, 5, 1, 14, 25, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 5, 1, 14, 25, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine2", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/billdisagg", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "13", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.Users.Add(new User { UserID = 1, ClientID = 87, ActorName = "Test87user1", EnableInd = true });
            var testrepo = new InsightsEfRepository(context);
            var controller = new Insights.Controllers.Version1.EndpointTrackingController(testrepo);
            var configuration = new HttpConfiguration();
            var request = new HttpRequestMessage();
            controller.Request = request;
            controller.Request.Properties["MS_HttpConfiguration"] = configuration;
            //add property for client user, normally from the stored procedure but hardcoded here for unit test purposes
            var clientUser = new ClientUser
            {
                ClientID = 87,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };
            controller.Request.Properties[CEConfiguration.CEClientUser] = clientUser;
            controller.Request.Properties[CEConfiguration.CELocale] = "en-US";


            //Act
            var result = controller.Get(endpointTrackingRequest);

            //Assert
            Assert.IsNotNull(result);

            Console.Out.WriteLine("");
            Console.Out.WriteLine("Response");
            Console.Out.Write(JsonConvert.SerializeObject(result.Content.ReadAsAsync<EndpointTrackingResponse>().Result, Formatting.Indented));

            var contentresponse = result.Content.ReadAsStringAsync().Result;
            //is pageindex 1 on response even though we did not provide it in the request
            Assert.IsTrue(contentresponse.Contains("\"PageIndex\":1"), "endpointtrackingv1 response failed as did not contain PageIndex of 1");

        }


        [TestCategory("Version1"), TestCategory("Get"), TestCategory("EndpointTrackingController"), TestCategory("BVT"), TestMethod]
        [Description("Equivalence class partitioning (testing subset of possible values on controller if statement) unit test for verifying endpoint tracking controller version 1 returns total call 0 count from mock repository (response) when response is null")]
        public void Endpointtrackingv1_get_TotalCall0Returned_WhenResponseNull()
        {
            //Arrange
            var endpointTrackingRequest = new EndpointTrackingRequest();
            string startdate = "4/1/2014";
            DateTime startdt = Convert.ToDateTime(startdate);
            string enddate = "5/1/2014";
            DateTime enddt = Convert.ToDateTime(enddate);
            endpointTrackingRequest.EndDate = enddt;
            endpointTrackingRequest.StartDate = startdt;
            endpointTrackingRequest.Verb = "GET";
            var context = new TestInsightEntities();
            context.Clients.Add(new Client { ClientID = 87, Name = "TestDemo", Description = "TestDemoDescription" });
            context.Endpoints.Add(new CE.Models.Insights.EF.Endpoint { EndpointID = 1, Name = "TestEndpoint1", ShortName = "testendpointv1", SortOrder = 1 });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 1, UserID = 1, EndpointID = 1, NewDate = new DateTime(2014, 5, 1, 10, 00, 00) });
            context.Users.Add(new User { UserID = 1, ClientID = 87, EnableInd = true });
            var testrepo = new InsightsEfRepository(context);
            var controller = new Insights.Controllers.Version1.EndpointTrackingController(testrepo);
            var configuration = new HttpConfiguration();
            var request = new HttpRequestMessage();
            controller.Request = request;
            controller.Request.Properties["MS_HttpConfiguration"] = configuration;
            //add property for client user, normally from the stored procedure but hardcoded here for unit test purposes
            var clientUser = new ClientUser
            {
                ClientID = 87,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };
            controller.Request.Properties[CEConfiguration.CEClientUser] = clientUser;
            controller.Request.Properties[CEConfiguration.CELocale] = "en-US";
            //mock insights repository and use mock data of response with null data
            var repo = new Mock<IInsightsEFRepository>();
            //passing in null response to see how controller handles it
            repo.Setup(s => s.GetEndpointTracking(clientUser.ClientID, endpointTrackingRequest)).Returns((EndpointTrackingResponse)null);
            //Act
            var result = controller.Get(endpointTrackingRequest);

            //Assert
            Assert.IsNotNull(result);
            //check for total calls 0 on response 
            var contentresponse = result.Content.ReadAsStringAsync().Result;
            Assert.IsTrue(contentresponse.Contains("\"TotalCalls\":0"), "endpointtrackingv1 response failed as did not contain total calls 0");
        }



        [TestCategory("Version1"), TestCategory("Post"), TestCategory("ProfileController"), TestMethod]
        [Description("Unit test to verify that controller post has correct behaviour, controller calls profile post method and response is successful")]
        public void Profilev1_Post_ResponseSuccessProfileAttributesPost_WhenOneCustOnePremise()
        {
            //Arrange
            var repo = new Mock<IInsightsEFRepository>();
            var profileModel = new Mock<IProfileModel>();
            ProfilePostResponse res = TestHelpers.createRepoProfile();
            var configuration = new HttpConfiguration();
            var request = new HttpRequestMessage();


            //add property for client user, normally from the stored procedure but hardcoded here for unit test purposes
            var clientUser = new ClientUser
            {
                ClientID = 256,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };
            var profileRequest = new ProfilePostRequest();
            var srcName = "csr";
            profileRequest.Customer = new ProfileCustomer();
            profileRequest.Customer.Id = "Test256Cust1";
            profileRequest.Customer.Attributes = new List<ProfileAttribute>();
            profileRequest.Customer.Attributes.Add(new ProfileAttribute() { AttributeKey = "greenprogram.enrollmentstatus", AttributeValue = "greenprogram.enrollmentstatus.enrolled", SourceKey = srcName });
            profileRequest.Customer.Accounts = new List<ProfileAccount>();
            profileRequest.Validate = true;
            var pa = new ProfileAccount();
            pa.Id = "Test256Cust1Account1";
            pa.Attributes = new List<ProfileAttribute>();
            pa.Premises = new List<ProfilePremise>();

            var pp = new ProfilePremise();
            pp.Id = "Test256Cust1Account1Premise1";
            pp.Attributes = new List<ProfileAttribute>();
            //update the sourcename with utc date so can find it later via entity framework linq select against table
            pp.Attributes.Add(new ProfileAttribute() { AttributeKey = "pool.heatingfrqcy", AttributeValue = "pool.heatingfrqcy.regularly", SourceKey = srcName });
            pp.Attributes.Add(new ProfileAttribute() { AttributeKey = "pool.size", AttributeValue = "2000", SourceKey = srcName });
            pa.Premises.Add(pp);
            profileRequest.Customer.Accounts.Add(pa);


            repo.Setup(s => s.PostProfile(clientUser.ClientID, It.IsAny<ProfilePostRequest>())).Returns(res).Verifiable();
            profileModel.Setup(p => p.ValidateAttributes(clientUser, It.IsAny<string>(), It.IsAny<string>(),
                profileRequest, It.IsAny<List<string>>(), It.IsAny<List<string>>(), It.IsAny<List<string>>(),
                It.IsAny<List<string>>())).Returns(true);
            var controller = new Insights.Controllers.Version1.ProfileController(repo.Object, profileModel.Object);

            controller.Request = request;
            controller.Request.Properties["MS_HttpConfiguration"] = configuration;
            controller.Request.Properties[CEConfiguration.CEClientUser] = clientUser;
            controller.Request.Properties[CEConfiguration.CELocale] = "en-US";
            //Act
            var result = controller.Post(profileRequest);

            //Assert
            Assert.IsNotNull(result);
            Console.Out.WriteLine("");
            Console.Out.WriteLine("Response");
            Console.Out.Write(JsonConvert.SerializeObject(result.Content.ReadAsAsync<ProfilePostResponse>().Result, Formatting.Indented));

            repo.Verify();
            Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
        }

        [TestCategory("Version1"), TestCategory("Post"), TestCategory("ProfileController"), TestMethod]
        [Description("Unit test to verify that controller post validates duplicate attributes and returns bad request with the duplicate attribute message and name.")]
        public void Profilev1_Post_ResponseBadRequestWDupMessage_WhenDuplicatePremiseAttributes()
        {
            //Arrange
            var repo = new Mock<IInsightsEFRepository>();
            ProfilePostResponse res = TestHelpers.createRepoProfile();
            var configuration = new HttpConfiguration();
            var request = new HttpRequestMessage();


            //add property for client user, normally from the stored procedure but hardcoded here for unit test purposes
            var clientUser = new ClientUser
            {
                ClientID = 256,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };
            var profileRequest = new ProfilePostRequest();
            var srcName = "csr";
            profileRequest.Customer = new ProfileCustomer();
            profileRequest.Customer.Id = "Test256Cust1";
            profileRequest.Customer.Attributes = new List<ProfileAttribute>();
            profileRequest.Customer.Attributes.Add(new ProfileAttribute() { AttributeKey = "greenprogram.enrollmentstatus", AttributeValue = "programstatus.enrolled", SourceKey = srcName });
            profileRequest.Customer.Accounts = new List<ProfileAccount>();
            profileRequest.Validate = true;
            var pa = new ProfileAccount();
            pa.Id = "Test256Cust1Account1";
            pa.Attributes = new List<ProfileAttribute>();
            pa.Attributes.Add(new ProfileAttribute() { AttributeKey = "budgetbilling.enrollmentstatus", AttributeValue = "budgetbilling.enrollmentstatus.enrolled", SourceKey = srcName });
            pa.Premises = new List<ProfilePremise>();

            var pp = new ProfilePremise();
            pp.Id = "Test256Cust1Account1Premise1";
            pp.Attributes = new List<ProfileAttribute>();
            pp.Attributes.Add(new ProfileAttribute() { AttributeKey = "pool.poolheater", AttributeValue = "300", SourceKey = srcName });
            pp.Attributes.Add(new ProfileAttribute() { AttributeKey = "pool.size", AttributeValue = "2000", SourceKey = srcName });
            pp.Attributes.Add(new ProfileAttribute() { AttributeKey = "pool.size", AttributeValue = "3000", SourceKey = srcName });
            pa.Premises.Add(pp);
            profileRequest.Customer.Accounts.Add(pa);


            repo.Setup(s => s.PostProfile(clientUser.ClientID, profileRequest)).Returns(res);
            var controller = new Insights.Controllers.Version1.ProfileController(repo.Object);

            controller.Request = request;
            controller.Request.Properties["MS_HttpConfiguration"] = configuration;
            controller.Request.Properties[CEConfiguration.CEClientUser] = clientUser;
            controller.Request.Properties[CEConfiguration.CELocale] = "en-US";
            //Act
            var result = controller.Post(profileRequest);

            //Assert
            Assert.IsNotNull(result);
            //  var resultSuccess = result.IsSuccessStatusCode;
            Assert.AreEqual(HttpStatusCode.BadRequest, result.StatusCode);
            Assert.IsTrue(result.Content.ReadAsStringAsync().Result.Contains(CEConfiguration.Invalid_DuplicateProfileAttributeKey));
        }

        [TestCategory("Version1"), TestCategory("Post"), TestCategory("ProfileController"), TestMethod]
        [Description("Unit test to verify that controller post validates invalid source key for two profile attribute and returns bad request with the invalid source key message for that attributekey")]
        public void Profilev1_Post_ResponseBadRequestWInvalidSourceMessage_WhenInvalidSourceKeyandValidateTrue()
        {
            //Arrange
            var repo = new Mock<IInsightsEFRepository>();
            ProfilePostResponse res = TestHelpers.createRepoProfile();
            var configuration = new HttpConfiguration();
            var request = new HttpRequestMessage();


            //add property for client user, normally from the stored procedure but hardcoded here for unit test purposes
            var clientUser = new ClientUser
            {
                ClientID = 256,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };
            var profileRequest = new ProfilePostRequest();
            var invalidsrcName = "invalidsourcekey";
            var validsrcName = "csr";
            profileRequest.Customer = new ProfileCustomer();
            profileRequest.Customer.Id = "Test256Cust1";
            profileRequest.Customer.Attributes = new List<ProfileAttribute>();
            profileRequest.Customer.Attributes.Add(new ProfileAttribute() { AttributeKey = "greenprogram.enrollmentstatus", AttributeValue = "greenprogram.enrollmentstatus.enrolled", SourceKey = invalidsrcName });
            profileRequest.Customer.Accounts = new List<ProfileAccount>();
            profileRequest.Validate = true;
            var pa = new ProfileAccount();
            pa.Id = "Test256Cust1Account1";
            pa.Attributes = new List<ProfileAttribute>();
            pa.Attributes.Add(new ProfileAttribute() { AttributeKey = "budgetbilling.enrollmentstatus", AttributeValue = "budgetbilling.enrollmentstatus.enrolled", SourceKey = validsrcName });
            pa.Premises = new List<ProfilePremise>();

            var pp = new ProfilePremise();
            pp.Id = "Test256Cust1Account1Premise1";
            pp.Attributes = new List<ProfileAttribute>();
            pp.Attributes.Add(new ProfileAttribute() { AttributeKey = "heatsystem.count", AttributeValue = "1", SourceKey = invalidsrcName });
            pp.Attributes.Add(new ProfileAttribute() { AttributeKey = "heatsystem.style", AttributeValue = "heatsystem.style.airsourceheatpump", SourceKey = validsrcName });
            pa.Premises.Add(pp);
            profileRequest.Customer.Accounts.Add(pa);


            repo.Setup(s => s.PostProfile(clientUser.ClientID, profileRequest)).Returns(res);
            var controller = new Insights.Controllers.Version1.ProfileController(repo.Object);

            controller.Request = request;
            controller.Request.Properties["MS_HttpConfiguration"] = configuration;
            controller.Request.Properties[CEConfiguration.CEClientUser] = clientUser;
            controller.Request.Properties[CEConfiguration.CELocale] = "en-US";
            //Act
            var result = controller.Post(profileRequest);

            //Assert
            Assert.IsNotNull(result);

            Console.Out.WriteLine("");
            Console.Out.WriteLine("Response");
            Console.Out.Write(JsonConvert.SerializeObject(result.Content.ReadAsAsync<ProfilePostResponse>().Result, Formatting.Indented));

            Assert.AreEqual(HttpStatusCode.BadRequest, result.StatusCode);
            Assert.IsTrue(result.Content.ReadAsStringAsync().Result.Contains(CEConfiguration.Invalid_ProfileAttributeSource));
        }

        [TestCategory("Version1"), TestCategory("Post"), TestCategory("ProfileController"), TestMethod]
        [Description("Unit test to verify that controller handles when request is null gracefully, with bad request returned on the response")]
        public void Profilev1_Post_BadRequest_WhenRequestIsNull()
        {
            //Arrange
            var repo = new Mock<IInsightsEFRepository>();
            var configuration = new HttpConfiguration();
            var request = new HttpRequestMessage();


            //add property for client user, normally from the stored procedure but hardcoded here for unit test purposes
            var clientUser = new ClientUser
            {
                ClientID = 256,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };
            ProfilePostRequest profileRequest;
            profileRequest = null;
            repo.Setup(s => s.PostProfile(clientUser.ClientID, profileRequest)).Returns((ProfilePostResponse)null);
            var controller = new Insights.Controllers.Version1.ProfileController(repo.Object);

            controller.Request = request;
            controller.Request.Properties["MS_HttpConfiguration"] = configuration;
            controller.Request.Properties[CEConfiguration.CEClientUser] = clientUser;
            controller.Request.Properties[CEConfiguration.CELocale] = "en-US";
            //Act
            var result = controller.Post(null);
            //Assert returns expected bad request
            Assert.AreEqual("BadRequest", result.StatusCode.ToString());

        }

        [TestCategory("Version1"), TestCategory("Get"), TestCategory("EndpointTrackingController"), TestCategory("BVT"), TestMethod]
        [Description("Unit test to verify endpoint tracking response when request with detail for a client for a day between one hour filtered by one endpoint. The 3rd date outside range should not be retrieved. Does count of 2 for endtrackdetails on assert. Does search for one of valid dates if true passes. Does search for one of invalid dates if false (not in response) passes.")]
        public void Endpointtrackingv1_get_OneHourDataReturned_WhenReqwDetailOneHourOneEndpoint()
        {
            //Arrange
            var endpointTrackingRequest = new EndpointTrackingRequest();
            endpointTrackingRequest.EndDate = new DateTime(2014, 5, 1, 14, 30, 00, 00, DateTimeKind.Utc);
            endpointTrackingRequest.StartDate = new DateTime(2014, 5, 1, 13, 00, 00, 00, DateTimeKind.Utc);
            endpointTrackingRequest.IncludeDetails = true;
            endpointTrackingRequest.EndpointId = 1;
            var context = new TestInsightEntities();
            context.Clients.Add(new Client { ClientID = 87, Name = "TestDemo", Description = "TestDemoDescription" });
            context.Endpoints.Add(new CE.Models.Insights.EF.Endpoint { EndpointID = 1, Name = "TestEndpoint1", ShortName = "testendpointv1", SortOrder = 1 });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 1, UserID = 1, EndpointID = 1, NewDate = new DateTime(2014, 5, 1, 14, 00, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 5, 1, 14, 00, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine1", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "12", Query = "?Say=QAtesttext&Name=Test123&Number=6", Verb = "GET", IncludeInd = true, SourceIPAddress = "111.123", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 2, UserID = 1, EndpointID = 1, NewDate = new DateTime(2014, 5, 1, 14, 30, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 5, 1, 14, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine2", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "13", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            //this one it should not retrieve as is outside the date specified on the request
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 3, UserID = 1, EndpointID = 1, NewDate = new DateTime(2014, 5, 1, 16, 30, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 5, 1, 16, 00, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine2", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "13", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.Users.Add(new User { UserID = 1, ClientID = 87, ActorName = "Test87user1", EnableInd = true });
            var testrepo = new InsightsEfRepository(context);
            var controller = new Insights.Controllers.Version1.EndpointTrackingController(testrepo);
            var configuration = new HttpConfiguration();
            var request = new HttpRequestMessage();
            controller.Request = request;
            controller.Request.Properties["MS_HttpConfiguration"] = configuration;
            //add property for client user, normally from the stored procedure but hardcoded here for unit test purposes
            var clientUser = new ClientUser
            {
                ClientID = 87,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };
            controller.Request.Properties[CEConfiguration.CEClientUser] = clientUser;
            controller.Request.Properties[CEConfiguration.CELocale] = "en-US";

            //Act
            var result = controller.Get(endpointTrackingRequest);

            //Assert
            Assert.IsNotNull(result);
            var resultscountTrackdetails = result.Content.ReadAsAsync<EndpointTrackingResponse>().Result.TrackingDetails.Count;
            Assert.IsTrue(resultscountTrackdetails == 2, "Two endpoints returned from results retrieval");
            var contentresponse = result.Content.ReadAsStringAsync().Result;
            Assert.IsTrue(contentresponse.Contains("\"LogDate\":\"2014-05-01T14:30:00Z\""), "endpointtrackingv1 response failed as did not contain correct log date on response matching request date");
            Assert.IsFalse(contentresponse.Contains("\"LogDate\":\"2014-05-01T16:30:00Z\""), "endpointtrackingv1 response failed as it contained log date on response outside of the requested datetime range");

            Console.Out.WriteLine("");
            Console.Out.WriteLine("Response");
            Console.Out.Write(JsonConvert.SerializeObject(result.Content.ReadAsAsync<EndpointTrackingResponse>().Result, Formatting.Indented));

        }

        [TestCategory("Version1"), TestCategory("Get"), TestCategory("EndpointTrackingController"), TestCategory("BVT"), TestMethod]
        [Description("Unit test to verify endpoint tracking response when request with detail for a client for a day between one hour filtered by many endpoints. The 3rd date outside range should not be retrieved. Does count of total calls 4 for endpoints on assert. Does search for one of valid dates if true passes. Does search for one of invalid dates if false (not in response) passes.")]
        public void Endpointtrackingv1_get_OneHourDataReturned_WhenReqwDetailOneHourManyEndpoints()
        {
            //Arrange
            var endpointTrackingRequest = new EndpointTrackingRequest();
            endpointTrackingRequest.EndDate = new DateTime(2014, 5, 1, 14, 30, 00, 00, DateTimeKind.Utc);
            endpointTrackingRequest.StartDate = new DateTime(2014, 5, 1, 13, 00, 00, 00, DateTimeKind.Utc);
            endpointTrackingRequest.IncludeDetails = true;
            var context = new TestInsightEntities();
            context.Clients.Add(new Client { ClientID = 87, Name = "TestDemo", Description = "TestDemoDescription" });
            context.Endpoints.Add(new CE.Models.Insights.EF.Endpoint { EndpointID = 1, Name = "TestEndpoint1", ShortName = "testendpoint1v1", SortOrder = 1 });
            context.Endpoints.Add(new CE.Models.Insights.EF.Endpoint { EndpointID = 2, Name = "TestEndpoint2", ShortName = "testendpoint2v1", SortOrder = 1 });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 1, UserID = 1, EndpointID = 1, NewDate = new DateTime(2014, 5, 1, 14, 00, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 5, 1, 14, 00, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine1", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "12", Query = "?Say=QAtesttext&Name=Test123&Number=6", Verb = "GET", IncludeInd = true, SourceIPAddress = "111.123", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 2, UserID = 1, EndpointID = 1, NewDate = new DateTime(2014, 5, 1, 14, 30, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 5, 1, 14, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine2", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "13", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            //this one it should not retrieve as is outside the date specified on the request
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 3, UserID = 1, EndpointID = 1, NewDate = new DateTime(2014, 5, 1, 16, 30, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 5, 1, 16, 00, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine2", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "13", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });

            //2nd endpoint
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 4, UserID = 1, EndpointID = 2, NewDate = new DateTime(2014, 5, 1, 14, 15, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 5, 1, 14, 15, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine1", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/billdisagg", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "12", Query = "?Say=QAtesttext&Name=Test123&Number=6", Verb = "GET", IncludeInd = true, SourceIPAddress = "111.123", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 5, UserID = 1, EndpointID = 2, NewDate = new DateTime(2014, 5, 1, 14, 25, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 5, 1, 14, 25, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine2", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/billdisagg", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "13", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.Users.Add(new User { UserID = 1, ClientID = 87, ActorName = "Test87user1", EnableInd = true });
            var testrepo = new InsightsEfRepository(context);
            var controller = new Insights.Controllers.Version1.EndpointTrackingController(testrepo);
            var configuration = new HttpConfiguration();
            var request = new HttpRequestMessage();
            controller.Request = request;
            controller.Request.Properties["MS_HttpConfiguration"] = configuration;
            //add property for client user, normally from the stored procedure but hardcoded here for unit test purposes
            var clientUser = new ClientUser
            {
                ClientID = 87,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };
            controller.Request.Properties[CEConfiguration.CEClientUser] = clientUser;
            controller.Request.Properties[CEConfiguration.CELocale] = "en-US";

            //Act
            var result = controller.Get(endpointTrackingRequest);

            //Assert
            Assert.IsNotNull(result);
            //total calls should be 4 - 2 for each endpoint. The 5th endpoint should not have been retrieved (16:30 UTC) as is outside the request parameters for dates - is tested on the assert below
            var contentresponse1 = result.Content.ReadAsStringAsync().Result;
            Assert.IsTrue(contentresponse1.Contains("\"TotalCalls\":4"), "endpointtrackingv1 response failed as did not contain total calls 4");
            var contentresponse = result.Content.ReadAsStringAsync().Result;
            Assert.IsTrue(contentresponse.Contains("\"LogDate\":\"2014-05-01T14:15:00Z\""), "endpointtrackingv1 response failed as did not contain correct log date on response matching request date");
            Assert.IsFalse(contentresponse.Contains("\"LogDate\":\"2014-05-01T16:30:00Z\""), "endpointtrackingv1 response failed as it contained log date on response outside of the requested datetime range");

            Console.Out.WriteLine("");
            Console.Out.WriteLine("Response");
            Console.Out.Write(JsonConvert.SerializeObject(result.Content.ReadAsAsync<EndpointTrackingResponse>().Result, Formatting.Indented));

        }

        [TestCategory("Version1"), TestCategory("Get"), TestCategory("EndpointTrackingController"), TestCategory("BVT"), TestMethod]
        [Description("Unit test to verify endpoint tracking response when request with detail for a client for 15 days for one endpoint. The 16thd date outside range should not be retrieved. Does count of total calls 16 for endpoints on assert. Does search for one of valid dates if true passes. Does search for one of invalid dates if false (not in response) passes.")]
        public void Endpointtrackingv1_get_15DaysResponse_WhenReqwDetail15DaysOneEndpoint()
        {
            //Arrange
            var endpointTrackingRequest = new EndpointTrackingRequest();
            endpointTrackingRequest.EndDate = new DateTime(2014, 5, 15, 23, 59, 59, 00, DateTimeKind.Utc);
            endpointTrackingRequest.StartDate = new DateTime(2014, 5, 1, 00, 00, 00, 00, DateTimeKind.Utc);
            endpointTrackingRequest.IncludeDetails = true;
            var context = new TestInsightEntities();
            context.Clients.Add(new Client { ClientID = 87, Name = "TestDemo", Description = "TestDemoDescription" });
            context.Endpoints.Add(new CE.Models.Insights.EF.Endpoint { EndpointID = 1, Name = "TestEndpoint1", ShortName = "testendpoint1v1", SortOrder = 1 });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 1, UserID = 1, EndpointID = 1, NewDate = new DateTime(2014, 5, 1, 14, 00, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 5, 1, 14, 00, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine1", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "12", Query = "?Say=QAtesttext&Name=Test123&Number=6", Verb = "GET", IncludeInd = true, SourceIPAddress = "111.123", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 2, UserID = 1, EndpointID = 1, NewDate = new DateTime(2014, 5, 2, 14, 30, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 5, 2, 14, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine2", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "13", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 3, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 5, 3, 15, 30, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 5, 3, 15, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine2", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "13", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 4, UserID = 1, EndpointID = 1, NewDate = new DateTime(2014, 5, 4, 16, 30, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 5, 4, 16, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine2", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "17", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 5, UserID = 1, EndpointID = 1, NewDate = new DateTime(2014, 5, 5, 17, 30, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 5, 5, 17, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine2", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "13", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 6, UserID = 1, EndpointID = 1, NewDate = new DateTime(2014, 5, 6, 18, 30, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 5, 6, 18, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine2", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "16", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 7, UserID = 1, EndpointID = 1, NewDate = new DateTime(2014, 5, 7, 19, 30, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 5, 7, 19, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine2", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "13", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 8, UserID = 1, EndpointID = 1, NewDate = new DateTime(2014, 5, 8, 20, 00, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 5, 8, 20, 00, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine2", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "13", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 9, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 5, 9, 14, 30, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 5, 9, 14, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine2", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "15", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 10, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 5, 10, 22, 30, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 5, 10, 22, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine2", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "13", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 11, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 5, 11, 23, 30, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 5, 11, 23, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine2", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "14", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 12, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 5, 12, 23, 30, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 5, 12, 23, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine3", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "14", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 13, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 5, 13, 23, 8, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 5, 13, 8, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine3", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "14", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 14, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 5, 14, 23, 9, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 5, 14, 9, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine3", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "14", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 15, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 5, 15, 23, 11, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 5, 15, 11, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine3", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "14", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            //this one it should not retrieve as is outside the date specified on the request
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 16, UserID = 1, EndpointID = 1, NewDate = new DateTime(2014, 5, 16, 00, 00, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 5, 16, 16, 00, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine2", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "13", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });

            context.Users.Add(new User { UserID = 1, ClientID = 87, ActorName = "Test87user1", EnableInd = true });
            context.Users.Add(new User { UserID = 2, ClientID = 87, ActorName = "Test87user2", EnableInd = true, NewDate = new DateTime(2014, 5, 1, 10, 00, 00), UpdDate = new DateTime(2015, 5, 1, 10, 00, 00), CEAccessKeyID = "7320D04481A949B8AB4BB04CDD1A307E" });
            var testrepo = new InsightsEfRepository(context);
            var controller = new Insights.Controllers.Version1.EndpointTrackingController(testrepo);
            var configuration = new HttpConfiguration();
            var request = new HttpRequestMessage();
            controller.Request = request;
            controller.Request.Properties["MS_HttpConfiguration"] = configuration;
            //add property for client user, normally from the stored procedure but hardcoded here for unit test purposes
            var clientUser = new ClientUser
            {
                ClientID = 87,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };
            controller.Request.Properties[CEConfiguration.CEClientUser] = clientUser;
            controller.Request.Properties[CEConfiguration.CELocale] = "en-US";

            //Act
            var result = controller.Get(endpointTrackingRequest);

            //Assert
            Assert.IsNotNull(result);
            //total calls should be 15 at various times - one per day. The 16th endpoint should not have been retrieved (5/16 UTC) as is outside the request parameters for dates - is tested on the assert below
            var contentresponse1 = result.Content.ReadAsStringAsync().Result;
            Assert.IsTrue(contentresponse1.Contains("\"TotalCalls\":15"), "endpointtrackingv1 response failed as did not contain total calls 15");
            var contentresponse = result.Content.ReadAsStringAsync().Result;
            Assert.IsTrue(contentresponse.Contains("\"LogDate\":\"2014-05-01T14:00:00Z\""), "endpointtrackingv1 response failed as did not contain correct log date on response matching request date");
            Assert.IsFalse(contentresponse.Contains("\"LogDate\":\"2014-05-16T00:00:00Z\""), "endpointtrackingv1 response failed as it contained log date on response outside of the requested datetime range");
        }


        [TestCategory("Version1"), TestCategory("Get"), TestCategory("EndpointTrackingController"), TestCategory("BVT"), TestMethod]
        [Description("Unit test to verify endpoint tracking response when request with detail for a client for one month for one endpoint. Does count of total calls 30 for endpoints on assert. Does search for one of valid dates if true passes. Does search for one of dates for following month if false (not in response) passes.")]
        public void Endpointtrackingv1_get_OneMonthDataReturned_WhenReqwDetailOneMonth()
        {
            //Arrange
            var endpointTrackingRequest = new EndpointTrackingRequest();
            endpointTrackingRequest.EndDate = new DateTime(2014, 4, 30, 23, 59, 59, 00, DateTimeKind.Utc);
            endpointTrackingRequest.StartDate = new DateTime(2014, 4, 1, 00, 00, 00, 00, DateTimeKind.Utc);
            endpointTrackingRequest.IncludeDetails = true;
            var context = new TestInsightEntities();
            context.Clients.Add(new Client { ClientID = 87, Name = "TestDemo", Description = "TestDemoDescription" });
            context.Endpoints.Add(new CE.Models.Insights.EF.Endpoint { EndpointID = 1, Name = "TestEndpoint1", ShortName = "testendpoint1v1", SortOrder = 1 });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 1, UserID = 1, EndpointID = 1, NewDate = new DateTime(2014, 4, 1, 14, 00, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 1, 14, 00, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine1", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "12", Query = "?Say=QAtesttext&Name=Test123&Number=6", Verb = "GET", IncludeInd = true, SourceIPAddress = "111.123", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 2, UserID = 1, EndpointID = 1, NewDate = new DateTime(2014, 4, 2, 14, 30, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 2, 14, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine2", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "13", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 3, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 4, 3, 15, 30, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 3, 15, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine2", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "13", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 4, UserID = 1, EndpointID = 1, NewDate = new DateTime(2014, 4, 4, 16, 30, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 4, 16, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine2", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "17", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 5, UserID = 1, EndpointID = 1, NewDate = new DateTime(2014, 4, 5, 17, 30, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 5, 17, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine2", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "13", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 6, UserID = 1, EndpointID = 1, NewDate = new DateTime(2014, 4, 6, 18, 30, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 6, 18, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine2", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "16", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 7, UserID = 1, EndpointID = 1, NewDate = new DateTime(2014, 4, 7, 19, 30, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 7, 19, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine2", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "13", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 8, UserID = 1, EndpointID = 1, NewDate = new DateTime(2014, 4, 8, 20, 00, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 8, 20, 00, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine2", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "13", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 9, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 4, 9, 14, 30, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 9, 14, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine2", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "15", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 10, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 4, 10, 22, 30, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 10, 22, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine2", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "13", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 11, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 4, 11, 23, 30, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 11, 23, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine2", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "14", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 12, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 4, 12, 23, 30, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 12, 23, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine3", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "14", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 13, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 4, 13, 23, 8, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 13, 8, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine3", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "14", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 14, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 4, 14, 23, 9, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 14, 9, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine3", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "14", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 15, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 4, 15, 23, 11, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 15, 11, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine3", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "14", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 16, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 4, 16, 23, 11, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 16, 11, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine3", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "14", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 17, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 4, 17, 23, 11, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 17, 11, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine3", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "14", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 18, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 4, 18, 23, 11, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 18, 11, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine3", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "14", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 19, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 4, 19, 23, 11, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 19, 11, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine3", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "14", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 20, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 4, 20, 23, 11, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 20, 11, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine3", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "14", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 21, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 4, 21, 23, 11, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 21, 11, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine3", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "14", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 22, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 4, 22, 23, 11, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 22, 11, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine3", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "14", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 23, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 4, 23, 23, 11, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 23, 11, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine3", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "14", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 24, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 4, 24, 23, 11, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 24, 11, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine3", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "14", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 25, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 4, 25, 23, 11, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 25, 11, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine3", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "14", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 26, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 4, 26, 23, 11, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 26, 11, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine3", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "14", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 27, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 4, 27, 23, 11, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 27, 11, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine3", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "14", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 28, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 4, 28, 23, 11, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 28, 11, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine3", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "14", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 29, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 4, 29, 23, 11, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 29, 11, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine3", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "14", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 30, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 4, 30, 23, 11, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 30, 11, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine3", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "14", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US", XCEMeta = "#cecp:1#" });
            //this one it should not retrieve as is outside the date specified on the request
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 31, UserID = 1, EndpointID = 1, NewDate = new DateTime(2014, 5, 1, 00, 00, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 5, 1, 00, 00, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine2", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "13", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });

            context.Users.Add(new User { UserID = 1, ClientID = 87, ActorName = "Test87user1", EnableInd = true });
            context.Users.Add(new User { UserID = 2, ClientID = 87, ActorName = "Test87user2", EnableInd = true });
            var testrepo = new InsightsEfRepository(context);
            var controller = new Insights.Controllers.Version1.EndpointTrackingController(testrepo);
            var configuration = new HttpConfiguration();
            var request = new HttpRequestMessage();
            controller.Request = request;
            controller.Request.Properties["MS_HttpConfiguration"] = configuration;
            //add property for client user, normally from the stored procedure but hardcoded here for unit test purposes
            var clientUser = new ClientUser
            {
                ClientID = 87,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };
            controller.Request.Properties[CEConfiguration.CEClientUser] = clientUser;
            controller.Request.Properties[CEConfiguration.CELocale] = "en-US";

            //Act
            var result = controller.Get(endpointTrackingRequest);

            //Assert
            Assert.IsNotNull(result);
            //total calls should be 30 at various times - one per day. The 31th endpoint should not have been retrieved (5/1 UTC) as is outside the request parameters for dates - is tested on the assert below
            var contentresponse1 = result.Content.ReadAsStringAsync().Result;
            Assert.IsTrue(contentresponse1.Contains("\"TotalCalls\":30"), "endpointtrackingv1 response failed as did not contain total calls 30");
            var contentresponse = result.Content.ReadAsStringAsync().Result;
            Assert.IsTrue(contentresponse.Contains("\"LogDate\":\"2014-04-01T14:00:00Z\""), "endpointtrackingv1 response failed as did not contain correct log date on response matching request date");
            Assert.IsFalse(contentresponse.Contains("\"LogDate\":\"2014-05-01T00:00:00Z\""), "endpointtrackingv1 response failed as it contained log date on response outside of the requested datetime range");
        }

        [TestCategory("Version1"), TestCategory("Get"), TestCategory("EndpointTrackingController"), TestCategory("BVT"), TestMethod]
        [Description("Unit test to verify endpoint tracking response when request with detail for a client by userid, verb, endpoint and messageid. The 16thd date outside range should not be retrieved. Does count of total calls 16 for endpoints on assert. Does search for one of valid dates if true passes. Does search for one of invalid dates if false (not in response) passes.")]
        public void Endpointtrackingv1_get_DataReturned_WhenReqwDetail_ByUserID_Verb_Endpoint_MessageID()
        {
            //Arrange
            var endpointTrackingRequest = new EndpointTrackingRequest();
            endpointTrackingRequest.EndDate = new DateTime(2014, 4, 30, 23, 59, 59, 00, DateTimeKind.Utc);
            endpointTrackingRequest.StartDate = new DateTime(2014, 4, 1, 00, 00, 00, 00, DateTimeKind.Utc);
            endpointTrackingRequest.UserId = 1;
            endpointTrackingRequest.MessageId = "55";
            endpointTrackingRequest.EndpointId = 1;
            endpointTrackingRequest.Verb = "GET";
            endpointTrackingRequest.IncludeDetails = true;
            var context = new TestInsightEntities();
            context.Clients.Add(new Client { ClientID = 87, Name = "TestDemo", Description = "TestDemoDescription" });
            context.Endpoints.Add(new CE.Models.Insights.EF.Endpoint { EndpointID = 1, Name = "TestEndpoint1", ShortName = "testendpoint1v1", SortOrder = 1 });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 1, UserID = 1, EndpointID = 1, NewDate = new DateTime(2014, 4, 1, 14, 00, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 1, 14, 00, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine1", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "12", Query = "?Say=QAtesttext&Name=Test123&Number=6", Verb = "GET", IncludeInd = true, SourceIPAddress = "111.123", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 2, UserID = 1, EndpointID = 1, NewDate = new DateTime(2014, 4, 2, 14, 30, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 2, 14, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine2", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "13", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 3, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 4, 3, 15, 30, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 3, 15, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine2", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "13", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 4, UserID = 1, EndpointID = 1, NewDate = new DateTime(2014, 4, 4, 16, 30, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 4, 16, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine2", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "17", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 5, UserID = 1, EndpointID = 1, NewDate = new DateTime(2014, 4, 5, 17, 30, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 5, 17, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine2", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "13", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 6, UserID = 1, EndpointID = 1, NewDate = new DateTime(2014, 4, 6, 18, 30, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 6, 18, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine2", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "55", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 7, UserID = 1, EndpointID = 1, NewDate = new DateTime(2014, 4, 7, 19, 30, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 7, 19, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine2", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "13", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 8, UserID = 1, EndpointID = 1, NewDate = new DateTime(2014, 4, 8, 20, 00, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 8, 20, 00, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine2", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "13", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 9, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 4, 9, 14, 30, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 9, 14, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine2", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "15", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 10, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 4, 10, 22, 30, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 10, 22, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine2", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "13", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 11, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 4, 11, 23, 30, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 11, 23, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine2", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "14", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 12, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 4, 12, 23, 30, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 12, 23, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine3", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "14", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 13, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 4, 13, 23, 8, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 13, 8, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine3", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "14", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 14, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 4, 14, 23, 9, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 14, 9, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine3", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "14", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 15, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 4, 15, 23, 11, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 15, 11, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine3", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "14", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 16, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 4, 16, 23, 11, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 16, 11, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine3", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "14", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 17, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 4, 17, 23, 11, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 17, 11, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine3", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "14", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 18, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 4, 18, 23, 11, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 18, 11, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine3", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "14", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 19, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 4, 19, 23, 11, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 19, 11, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine3", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "14", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 20, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 4, 20, 23, 11, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 20, 11, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine3", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "14", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 21, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 4, 21, 23, 11, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 21, 11, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine3", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "14", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 22, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 4, 22, 23, 11, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 22, 11, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine3", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "14", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 23, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 4, 23, 23, 11, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 23, 11, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine3", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "14", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 24, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 4, 24, 23, 11, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 24, 11, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine3", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "14", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 25, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 4, 25, 23, 11, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 25, 11, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine3", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "14", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 26, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 4, 26, 23, 11, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 26, 11, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine3", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "14", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 27, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 4, 27, 23, 11, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 27, 11, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine3", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "14", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 28, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 4, 28, 23, 11, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 28, 11, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine3", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "14", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 29, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 4, 29, 23, 11, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 29, 11, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine3", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "14", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 30, UserID = 2, EndpointID = 1, NewDate = new DateTime(2014, 4, 30, 23, 11, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 4, 30, 11, 30, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine3", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "14", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });
            //this one it should not retrieve as is outside the date specified on the request
            context.EndpointTrackings.Add(new EndpointTracking { EndpointTrackingID = 31, UserID = 1, EndpointID = 1, NewDate = new DateTime(2014, 5, 1, 00, 00, 00, 00, DateTimeKind.Utc), LogDate = new DateTime(2014, 5, 1, 00, 00, 00, 00, DateTimeKind.Utc), MachineName = "TestMachine2", PartitionKey = 1, ClientID = 87, ResourceName = "/api/v1/echo", UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36", XCEChannel = "unitTest vstudio", XCEMessageId = "13", Query = "?Say=QAtesttext&Name=Test456&Number=3", Verb = "GET", IncludeInd = true, SourceIPAddress = "123.545", XCELocale = "en-US" });

            context.Users.Add(new User { UserID = 1, ClientID = 87, ActorName = "Test87user1", EnableInd = true });
            context.Users.Add(new User { UserID = 2, ClientID = 87, ActorName = "Test87user2", EnableInd = true });
            var testrepo = new InsightsEfRepository(context);
            var controller = new Insights.Controllers.Version1.EndpointTrackingController(testrepo);
            var configuration = new HttpConfiguration();
            var request = new HttpRequestMessage();
            controller.Request = request;
            controller.Request.Properties["MS_HttpConfiguration"] = configuration;
            //add property for client user, normally from the stored procedure but hardcoded here for unit test purposes
            var clientUser = new ClientUser
            {
                ClientID = 87,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };
            controller.Request.Properties[CEConfiguration.CEClientUser] = clientUser;
            controller.Request.Properties[CEConfiguration.CELocale] = "en-US";

            //Act
            var result = controller.Get(endpointTrackingRequest);

            //Assert
            Assert.IsNotNull(result);
            //total calls should be 1 for a userid, messageid , verb . Message ID on response should match the request message id
            var contentresponse1 = result.Content.ReadAsStringAsync().Result;
            Assert.IsTrue(contentresponse1.Contains("\"TotalCalls\":1"), "endpointtrackingv1 response failed as did not contain total calls 1");
            var contentresponse = result.Content.ReadAsStringAsync().Result;
            Assert.IsTrue(contentresponse.Contains("\"MessageId\":\"55\""), "endpointtrackingv1 response failed as did not contain correct messageid on response matching request");
        }



        [TestCategory("Version1"), TestCategory("Post"), TestCategory("ActionPlanController"), TestMethod]
        [Description("Unit test to verify action plan/status successfully posted for this one customer with one action plan")]
        public void ActionPlanv1_Post_ResponseSuccess_WhenOneCustomerOneValidActionPlan()
        {
            //Arrange
            var repo = new Mock<IInsightsEFRepository>();
            var actionPlanModel = new Mock<IActionPlanModel>();
            var configuration = new HttpConfiguration();
            var request = new HttpRequestMessage();


            //add property for client user, normally from the stored procedure but hardcoded here for unit test purposes
            var clientUser = new ClientUser
            {
                ClientID = 256,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };
            var actionPlanRequest = new ActionPlanPostRequest();
            actionPlanRequest.Validate = true;

            //ActionStatus
            ActionStatus actionStat = new ActionStatus
            {
                ActionKey = "lowerwatertempgas",
                Status = "completed",
                SourceKey = "csr"
            };
            //add to list of actionstatuses
            var listStatus = new List<ActionStatus>();
            listStatus.Add(actionStat);
            //ActionPlanPremise
            ActionPlanPremise app = new ActionPlanPremise
            {
                Id = "test256custid1acctid1premid1",
                ActionStatuses = listStatus
            };
            //add to premiselist
            var listPremises = new List<ActionPlanPremise>();
            listPremises.Add(app);
            //ActionPlanAccount
            ActionPlanAccount apa = new ActionPlanAccount
            {
                Id = "test256custid1acctid1",
                Premises = listPremises
            };

            //add to action plan account list
            var listActionPlanAccounts = new List<ActionPlanAccount>();
            listActionPlanAccounts.Add(apa);
            //Customer
            var customer = new ActionPlanCustomer();
            customer.Id = "test256custid1";
            actionPlanRequest.Customer = customer;
            actionPlanRequest.Customer.Accounts = listActionPlanAccounts;


            repo.Setup(s => s.PostActionPlan(clientUser.ClientID, actionPlanRequest)).Returns((ActionPlanPostResponse)null).Verifiable();
            actionPlanModel.Setup(a => a.ValidateActions(clientUser, It.IsAny<string>(), It.IsAny<string>(),
                actionPlanRequest, It.IsAny<List<string>>(), It.IsAny<List<string>>(), It.IsAny<List<string>>(),
                It.IsAny<List<string>>())).Returns(true).Verifiable();

            var controller =
                new Insights.Controllers.Version1.ActionPlanController(repo.Object, actionPlanModel.Object);
            controller.Request = request;
            controller.Request.Properties["MS_HttpConfiguration"] = configuration;
            controller.Request.Properties[CEConfiguration.CEClientUser] = clientUser;
            controller.Request.Properties[CEConfiguration.CELocale] = "en-US";
            //Act
            var result = controller.Post(actionPlanRequest);

            //Assert
            Assert.IsNotNull(result);
            actionPlanModel.Verify();
            repo.Verify();
            //  var resultSuccess = result.IsSuccessStatusCode;
            Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
        }

        [TestCategory("Version1"), TestCategory("Post"), TestCategory("ActionPlanController"), TestMethod]
        [Description("Unit test to verify  multiple premises for a customer action plan/status post successfully")]
        public void ActionPlanv1_Post_ResponseSuccess_WhenOneCustomerTwoPremisesValidActionPlans()
        {
            //Arrange
            var repo = new Mock<IInsightsEFRepository>();
            var actionPlanModel = new Mock<IActionPlanModel>();
            var configuration = new HttpConfiguration();
            var request = new HttpRequestMessage();


            //add property for client user, normally from the stored procedure but hardcoded here for unit test purposes
            var clientUser = new ClientUser
            {
                ClientID = 256,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };
            var actionPlanRequest = new ActionPlanPostRequest();
            actionPlanRequest.Validate = true;


            //ActionStatus
            ActionStatus actionStat = new ActionStatus
            {
                ActionKey = "lowerwatertempgas",
                Status = "completed",
                SourceKey = "csr"
            };

            //second action first account
            ActionStatus actionStat2 = new ActionStatus
            {
                ActionKey = "skipdrydishwasher",
                Status = "selected",
                SourceKey = "csr"
            };

            //add to list of actionstatuses
            var listStatus = new List<ActionStatus>();
            listStatus.Add(actionStat);
            listStatus.Add(actionStat2);


            //ActionStatus
            ActionStatus actionStat3 = new ActionStatus
            {
                ActionKey = "lowerwatertempgas",
                Status = "rejected",
                SourceKey = "csr"
            };

            //second action first account
            ActionStatus actionStat4 = new ActionStatus
            {
                ActionKey = "washclothesincold",
                Status = "presented",
                SourceKey = "csr"
            };


            //add to list of actionstatuses for second account
            var listStatus2 = new List<ActionStatus>();
            listStatus2.Add(actionStat3);
            listStatus2.Add(actionStat4);


            //ActionPlanPremise
            ActionPlanPremise app = new ActionPlanPremise
            {
                Id = "test256custid1acctid1premid1",
                ActionStatuses = listStatus
            };

            ActionPlanPremise app2 = new ActionPlanPremise
            {
                Id = "test256custid1acctid2premid2",
                ActionStatuses = listStatus2
            };
            //add to premiselist
            var listPremises = new List<ActionPlanPremise>();
            listPremises.Add(app);
            //ActionPlanAccount
            ActionPlanAccount apa = new ActionPlanAccount
            {
                Id = "test256custid1acctid1",
                Premises = listPremises
            };

            var listPremises2 = new List<ActionPlanPremise>();
            listPremises2.Add(app2);
            //second account for customer
            ActionPlanAccount apa2 = new ActionPlanAccount
            {
                Id = "test256custid1acctid2",
                Premises = listPremises2
            };

            //add to action plan account list
            var listActionPlanAccounts = new List<ActionPlanAccount>();
            listActionPlanAccounts.Add(apa);
            listActionPlanAccounts.Add(apa2);
            //Customer
            var customer = new ActionPlanCustomer();
            customer.Id = "test256custid1";
            actionPlanRequest.Customer = customer;
            actionPlanRequest.Customer.Accounts = listActionPlanAccounts;


            repo.Setup(s => s.PostActionPlan(clientUser.ClientID, actionPlanRequest)).Returns((ActionPlanPostResponse)null).Verifiable();
            actionPlanModel.Setup(a => a.ValidateActions(clientUser, It.IsAny<string>(), It.IsAny<string>(),
                actionPlanRequest, It.IsAny<List<string>>(), It.IsAny<List<string>>(), It.IsAny<List<string>>(),
                It.IsAny<List<string>>())).Returns(true).Verifiable();

            var controller =
                new Insights.Controllers.Version1.ActionPlanController(repo.Object, actionPlanModel.Object);

            controller.Request = request;
            controller.Request.Properties["MS_HttpConfiguration"] = configuration;
            controller.Request.Properties[CEConfiguration.CEClientUser] = clientUser;
            controller.Request.Properties[CEConfiguration.CELocale] = "en-US";
            //Act
            var result = controller.Post(actionPlanRequest);

            //Assert
            Assert.IsNotNull(result);
            actionPlanModel.Verify();
            repo.Verify();
            //  var resultSuccess = result.IsSuccessStatusCode;
            Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
        }

        [TestCategory("Version1"), TestCategory("Post"), TestCategory("ActionPlanController"), TestMethod]
        [Description("Unit test to verify bad request returned on response for this customer when status is invalid on the post")]
        public void ActionPlanv1_Post_ResponseBadRequest_WhenInvalidStatus()
        {
            //Arrange
            var repo = new Mock<IInsightsEFRepository>();
            var configuration = new HttpConfiguration();
            var request = new HttpRequestMessage();


            //add property for client user, normally from the stored procedure but hardcoded here for unit test purposes
            var clientUser = new ClientUser
            {
                ClientID = 256,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };
            var actionPlanRequest = new ActionPlanPostRequest();
            actionPlanRequest.Validate = true;


            //ActionStatus using invalidstatus which does not match the enums for Status
            ActionStatus actionStat = new ActionStatus
            {
                ActionKey = "lowerwatertempgas",
                Status = "invalidstatus",
                SourceKey = "csr"
            };
            //add to list of actionstatuses
            var listStatus = new List<ActionStatus>();
            listStatus.Add(actionStat);
            //ActionPlanPremise
            ActionPlanPremise app = new ActionPlanPremise
            {
                Id = "test256custid1acctid1premid1",
                ActionStatuses = listStatus
            };
            //add to premiselist
            var listPremises = new List<ActionPlanPremise>();
            listPremises.Add(app);
            //ActionPlanAccount
            ActionPlanAccount apa = new ActionPlanAccount
            {
                Id = "test256custid1acctid1",
                Premises = listPremises
            };

            //add to action plan account list
            var listActionPlanAccounts = new List<ActionPlanAccount>();
            listActionPlanAccounts.Add(apa);
            //Customer
            var customer = new ActionPlanCustomer();
            customer.Id = "test256custid1";
            actionPlanRequest.Customer = customer;
            actionPlanRequest.Customer.Accounts = listActionPlanAccounts;


            repo.Setup(s => s.PostActionPlan(clientUser.ClientID, actionPlanRequest)).Returns((ActionPlanPostResponse)null);
            var controller = new Insights.Controllers.Version1.ActionPlanController(repo.Object);

            controller.Request = request;
            controller.Request.Properties["MS_HttpConfiguration"] = configuration;
            controller.Request.Properties[CEConfiguration.CEClientUser] = clientUser;
            controller.Request.Properties[CEConfiguration.CELocale] = "en-US";
            //Act
            var result = controller.Post(actionPlanRequest);

            //Assert
            Assert.IsNotNull(result);
            // should get bad request because is invalid status
            Assert.AreEqual(HttpStatusCode.BadRequest, result.StatusCode);
        }


        [TestCategory("Version2"), TestCategory("Get"), TestCategory("EchoController"), TestCategory("BVT"), TestMethod]
        [Description("Simple unit test for verifying echo controller version 2 returns response data of echorequest Say parameter value and success status code ")]
        public void EchoV2_Get_SayParameterandSuccessCodeReturned_WhenValidRequest()
        {
            var echoRequest = new EchoRequest();
            echoRequest.Say = "QAversion2";
            echoRequest.Number = 2;
            echoRequest.Name = "HelloQATest";

            //Arrange
            Insights.Controllers.Version2.EchoController controller = new Insights.Controllers.Version2.EchoController();
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            // Act 
            var result = controller.Get(echoRequest);

            //Assert
            Assert.IsNotNull(result);
            var contentresponse = result.Content.ReadAsStringAsync().Result;
            Assert.IsTrue(contentresponse.Contains(echoRequest.Say), "echov2 response failed as did not contain original request Say value");
            Assert.IsTrue(result.IsSuccessStatusCode);
        }

        //     [TestCategory("Version2"), TestCategory("Get"), TestCategory("EchoController"), TestCategory("BVT"), TestMethod]
        //     [Description("Creating a failed test deliberately to see behavior Simple unit test for verifying echo controller version 2 returns response data of echorequest Say parameter value and success status code ")]
        //    public void EchoV2_Get_WrongSayParameterandSuccessCodeReturned_WhenValidRequest()
        //    {
        //        var echoRequest = new CE.Models.Insights.EchoRequest();
        //        echoRequest.Say = "QAversion2";
        //       echoRequest.Number = 2;
        //       echoRequest.Name = "HelloQATest";

        //Arrange
        //       Controllers.Version2.EchoController controller = new Controllers.Version2.EchoController();
        //       controller.Request = new HttpRequestMessage();
        //       controller.Configuration = new HttpConfiguration();
        // Act 
        //       var result = controller.Get(echoRequest);

        //Assert
        //       Assert.IsNotNull(result);
        //       var contentresponse = result.Content.ReadAsStringAsync().Result;
        //       Assert.IsTrue(contentresponse.Contains("QAversion3"), "echov2 response failed as did not contain original request Say value");
        //       Assert.IsTrue(result.IsSuccessStatusCode);
        //   }


        [TestCategory("Version2"), TestCategory("Post"), TestCategory("EchoController"), TestCategory("BVT"), TestMethod]
        [Description("Unit test for verifying echo controller version 2 post returns response data and success status code returned")]
        public void EchoV2_Post_SayParameterandSuccessCodeReturned_WhenValidRequest()
        {
            var echoRequest = new EchoRequest();
            echoRequest.Say = "QAversion2Post";
            echoRequest.Number = 2;
            echoRequest.Name = "HelloQATest";

            //Arrange
            Insights.Controllers.Version2.EchoController controller = new Insights.Controllers.Version2.EchoController();
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            // Act 
            var result = controller.Post(echoRequest);

            //Assert
            Assert.IsNotNull(result);
            var contentresponse = result.Content.ReadAsStringAsync().Result;
            Assert.IsTrue(contentresponse.Contains(echoRequest.Say), "echov2 post response failed as did not contain original request Say value");
            Assert.IsTrue(result.IsSuccessStatusCode);

        }
        [TestMethod, TestCategory("BilltoDateController"), TestCategory("BVT")]
        public void BillToDateV1_Get_Test()
        {
            var billToDateRequest = new BillToDateRequest();
            billToDateRequest.CustomerId = "1000";
            billToDateRequest.AccountId = "a1";


            var container = new UnityContainer();

            (new DataStorageRegistrar()).Initialize<TransientLifetimeManager>(container);



            Mapper.CreateMap<BillingEntity, BillingModel>();
            Mapper.CreateMap<BillCycleScheduleEntity, BillCycleScheduleModel>();
            Mapper.CreateMap<CalculationEntity, CalculationModel>()
                .ForAllUnmappedMembers(o => o.Ignore());

            Mapper.CreateMap<CalculationModel, CalculationEntity>()
                .ForAllUnmappedMembers(o => o.Ignore());

            var controller = new Insights.Controllers.Version1.BillToDateController(container);
            var configuration = new HttpConfiguration();
            var request = new HttpRequestMessage();

            controller.Request = request;
            controller.Request.Properties["MS_HttpConfiguration"] = configuration;
            //add property for client user, normally from the stored procedure but hardcoded here for unit test purposes
            var clientUser = new ClientUser
            {
                ClientID = 87,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };

            controller.Request.Properties[CEConfiguration.CEClientUser] = clientUser;
            controller.Request.Properties[CEConfiguration.CELocale] = "es-ES";

            //UnityConfig.GetConfiguredContainer();

            var result = controller.Get(billToDateRequest);
            //var result = model.GetBillInfo(clientUser, billToDateRequest);

            Assert.IsNotNull(result);
            //Assert.AreSame(billToDateRequest.CustomerId, result.FirstOrDefault().CustomerId);
        }

        //[TestMethod, TestCategory("ConsumptionController"), TestCategory("BVT")]
        //public void ConsumptionV2_Get_Test()
        //{
        //    //var consumptionRequest = new CE.Models.Insights.ConsumptionRequest();
        //    //consumptionRequest.CustomerId = "1000";
        //    //consumptionRequest.AccountId = "a1";

        //    var clientId = 87;
        //    var customerId = "BTD001";
        //    var premiseId = "BTD001a1p1";
        //    var accountId = "BTD001a1";
        //    var servicePointId = "SP_BTD001a1";
        //    var meterId = "EM001";
        //    var startDate = Convert.ToDateTime("10/17/2015");
        //    var endDate = Convert.ToDateTime("1/17/2016");
        //    var projected = Convert.ToDateTime("1/17/2016");
        //    var requestedResolution = "day";

        //    var meterIds = new List<string>();
        //    meterIds.Add(meterId);

        //    var consumptionRequest = new ConsumptionRequest
        //    {
        //        CustomerId = customerId,
        //        PremiseId = premiseId,
        //        AccountId = accountId,
        //        MeterIds = meterId,
        //        StartDate = startDate,
        //        EndDate = endDate,
        //        Count = 0,
        //        ResolutionKey = requestedResolution,
        //        IncludeResolutions = true,
        //        CommodityKey = "electric"
        //    };


        //    var container = new UnityContainer();

        //    RegisterTypesForUnity.RegisterTypes(container);

        //    // mock ami hourly data
        //    var tableRepository = new Mock<ITableRepository>();
        //    container.RegisterInstance(tableRepository.Object);
        //    var readings = TestHelpers.CreateMockAzureAmiHourlyData(clientId, accountId, meterId, servicePointId, startDate, endDate, projected);
        //    var filter = string.Format("(PartitionKey eq '{0}_{1}') and ((RowKey ge '{2}') and (RowKey le '{3}'))", clientId.ToString(), meterId.ToLower(), startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"));
        //    tableRepository.Setup(s => s.GetAllAsync<DynamicTableEntity>(Constants.TableNames.Ami, filter)).Returns(readings);

        //    var controller = new Controllers.Version2.ConsumptionController(container);
        //    var configuration = new HttpConfiguration();
        //    var request = new HttpRequestMessage();

        //    controller.Request = request;
        //    controller.Request.Properties["MS_HttpConfiguration"] = configuration;
        //    //add property for client user, normally from the stored procedure but hardcoded here for unit test purposes
        //    var clientUser = new ClientUser
        //    {
        //        ClientID = clientId,
        //        ClientProperties = new List<ClientProperties>
        //        {
        //            new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
        //            new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
        //        }
        //    };

        //    controller.Request.Properties[CEConfiguration.CEClientUser] = clientUser;
        //    controller.Request.Properties[CEConfiguration.CELocale] = "es-ES";

        //    //UnityConfig.GetConfiguredContainer();

        //    var result = controller.Get(consumptionRequest);
        //    //var result = model.GetBillInfo(clientUser, billToDateRequest);

        //    Assert.IsNotNull(result);
        //    //Assert.AreEqual("month,day,hour", result.ResolutionsAvailable);
        //    //Assert.IsNotNull(result.Data);
        //    //Assert.AreEqual(startDate, result.Data[0].DateTime);
        //    //Assert.AreSame(billToDateRequest.CustomerId, result.FirstOrDefault().CustomerId);
        //}

        //[TestMethod, TestCategory("ConsumptionController"), TestCategory("BVT")]
        //public void ConsumptionV2_Get_IncludeAverageUsage_Test()
        //{
        //    //var consumptionRequest = new CE.Models.Insights.ConsumptionRequest();
        //    //consumptionRequest.CustomerId = "1000";
        //    //consumptionRequest.AccountId = "a1";

        //    var clientId = 87;
        //    var customerId = "BTD001";
        //    var premiseId = "BTD001a1p1";
        //    var accountId = "BTD001a1";
        //    var servicePointId = "SP_BTD001a1";
        //    var meterId = "EM001";
        //    var startDate = Convert.ToDateTime("10/17/2015");
        //    var endDate = Convert.ToDateTime("1/17/2016");
        //    var projected = Convert.ToDateTime("1/17/2016");
        //    var requestedResolution = "day";

        //    var meterIds = new List<string>();
        //    meterIds.Add(meterId);

        //    var consumptionRequest = new ConsumptionRequest
        //    {
        //        CustomerId = customerId,
        //        PremiseId = premiseId,
        //        AccountId = accountId,
        //        MeterIds = meterId,
        //        StartDate = startDate,
        //        EndDate = endDate,
        //        Count = 0,
        //        ResolutionKey = requestedResolution,
        //        IncludeResolutions = true,
        //        CommodityKey = "electric",
        //        IncludeAverageUsage = true

        //    };

        //    var container = new UnityContainer();

        //    RegisterTypesForUnity.RegisterTypes(container);

        //    // mock ami hourly data
        //    var tableRepository = new Mock<ITableRepository>();
        //    container.RegisterInstance(tableRepository.Object);
        //    var readings = TestHelpers.CreateMockAzureAmiHourlyData(clientId, accountId, meterId, servicePointId, startDate, endDate, projected);
        //    var filter = string.Format("(PartitionKey eq '{0}_{1}') and ((RowKey ge '{2}') and (RowKey le '{3}'))", clientId.ToString(), meterId.ToLower(), startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"));
        //    tableRepository.Setup(s => s.GetAllAsync<DynamicTableEntity>(Constants.TableNames.Ami, filter)).Returns(readings);

        //    var controller = new Controllers.Version2.ConsumptionController(container);
        //    var configuration = new HttpConfiguration();
        //    var request = new HttpRequestMessage();

        //    controller.Request = request;
        //    controller.Request.Properties["MS_HttpConfiguration"] = configuration;
        //    //add property for client user, normally from the stored procedure but hardcoded here for unit test purposes
        //    var clientUser = new ClientUser
        //    {
        //        ClientID = clientId,
        //        ClientProperties = new List<ClientProperties>
        //        {
        //            new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
        //            new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
        //        }
        //    };

        //    controller.Request.Properties[CEConfiguration.CEClientUser] = clientUser;
        //    controller.Request.Properties[CEConfiguration.CELocale] = "es-ES";

        //    //UnityConfig.GetConfiguredContainer();

        //    var result = controller.Get(consumptionRequest);

        //    Assert.IsNotNull(result);
        //    Assert.IsNotNull(result.Content);
        //    bool averageUsageNotNull = result.Content.ReadAsAsync<ConsumptionResponse>().Result.AverageUsage != null;
        //    var averageUsage = result.Content.ReadAsAsync<ConsumptionResponse>().Result.AverageUsage;
        //    Assert.IsTrue(averageUsageNotNull);
        //    Assert.AreEqual(24, averageUsage);
        //}

        //[TestMethod, TestCategory("ConsumptionController"), TestCategory("BVT")]
        //public void ConsumptionV2_Get_NotIncludeAverageUsage_Test()
        //{
        //    //var consumptionRequest = new CE.Models.Insights.ConsumptionRequest();
        //    //consumptionRequest.CustomerId = "1000";
        //    //consumptionRequest.AccountId = "a1";

        //    var clientId = 87;
        //    var customerId = "BTD001";
        //    var premiseId = "BTD001a1p1";
        //    var accountId = "BTD001a1";
        //    var servicePointId = "SP_BTD001a1";
        //    var meterId = "EM001";
        //    var startDate = Convert.ToDateTime("10/17/2015");
        //    var endDate = Convert.ToDateTime("1/17/2016");
        //    var projected = Convert.ToDateTime("1/17/2016");
        //    var requestedResolution = "day";

        //    var meterIds = new List<string>();
        //    meterIds.Add(meterId);

        //    var consumptionRequest = new ConsumptionRequest
        //    {
        //        CustomerId = customerId,
        //        PremiseId = premiseId,
        //        AccountId = accountId,
        //        MeterIds = meterId,
        //        StartDate = startDate,
        //        EndDate = endDate,
        //        Count = 0,
        //        ResolutionKey = requestedResolution,
        //        IncludeResolutions = true,
        //        CommodityKey = "electric",
        //        //IncludeAverageUsage = true

        //    };

        //    var container = new UnityContainer();

        //    (new DataStorageRegistrar()).Initialize<TransientLifetimeManager>(container);


        //    // mock ami hourly data
        //    var tableRepository = new Mock<ITableRepository>();
        //    container.RegisterInstance(tableRepository.Object);
        //    var readings = TestHelpers.CreateMockAzureAmiHourlyData(clientId, accountId, meterId, servicePointId, startDate, endDate, projected);
        //    var filter = string.Format("(PartitionKey eq '{0}_{1}') and ((RowKey ge '{2}') and (RowKey le '{3}'))", clientId.ToString(), meterId, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"));
        //    tableRepository.Setup(s => s.GetAllAsync<Ami15MinuteIntervalEntity>(Constants.TableNames.Ami, filter)).Returns(readings);

        //    var controller = new Controllers.Version2.ConsumptionController(container);
        //    var configuration = new HttpConfiguration();
        //    var request = new HttpRequestMessage();

        //    controller.Request = request;
        //    controller.Request.Properties["MS_HttpConfiguration"] = configuration;
        //    //add property for client user, normally from the stored procedure but hardcoded here for unit test purposes
        //    var clientUser = new ClientUser
        //    {
        //        ClientID = clientId,
        //        ClientProperties = new List<ClientProperties>
        //        {
        //            new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
        //            new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
        //        }
        //    };

        //    controller.Request.Properties[CEConfiguration.CEClientUser] = clientUser;
        //    controller.Request.Properties[CEConfiguration.CELocale] = "es-ES";

        //    //UnityConfig.GetConfiguredContainer();

        //    var result = controller.Get(consumptionRequest);

        //    Assert.IsNotNull(result);
        //    Assert.IsNotNull(result.Content);
        //    bool averageUsageNotNull = result.Content.ReadAsAsync<ConsumptionResponse>().Result.AverageUsage != null;
        //    Assert.IsFalse(averageUsageNotNull);
        //}

        //[TestMethod, TestCategory("ConsumptionController"), TestCategory("BVT")]
        //public void ConsumptionV2_Get_Weather_Test()
        //{
        //    //var consumptionRequest = new CE.Models.Insights.ConsumptionRequest();
        //    //consumptionRequest.CustomerId = "1000";
        //    //consumptionRequest.AccountId = "a1";

        //    var clientId = 87;
        //    var customerId = "BTD001";
        //    var premiseId = "BTD001a1p1";
        //    var accountId = "BTD001a1";
        //    var servicePointId = "SP_BTD001a1";
        //    var meterId = "EM001";
        //    var startDate = Convert.ToDateTime("10/17/2015");
        //    var endDate = Convert.ToDateTime("1/17/2016");
        //    var projected = Convert.ToDateTime("1/17/2016");
        //    var requestedResolution = "day";
        //    var zip = "02481";

        //    var meterIds = new List<string>();
        //    meterIds.Add(meterId);

        //    var consumptionRequest = new ConsumptionRequest
        //    {
        //        CustomerId = customerId,
        //        PremiseId = premiseId,
        //        AccountId = accountId,
        //        MeterIds = meterId,
        //        StartDate = startDate,
        //        EndDate = endDate,
        //        Count = 0,
        //        ResolutionKey = requestedResolution,
        //        IncludeResolutions = true,
        //        CommodityKey = "electric",
        //        ZipCode = zip,
        //        IncludeWeather = true
        //    };


        //    var container = new UnityContainer();

        //    RegisterTypesForUnity.RegisterTypes(container);

        //    // mock ami hourly data
        //    var tableRepository = new Mock<ITableRepository>();
        //    container.RegisterInstance(tableRepository.Object);
        //    var readings = TestHelpers.CreateMockAzureAmiHourlyData(clientId, accountId, meterId, servicePointId, startDate, endDate, projected);
        //    var filter = string.Format("(PartitionKey eq '{0}_{1}') and ((RowKey ge '{2}') and (RowKey le '{3}'))", clientId.ToString(), meterId.ToLower(), startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"));
        //    tableRepository.Setup(s => s.GetAllAsync<DynamicTableEntity>(Constants.TableNames.Ami, filter)).Returns(readings);

        //    // mock weather data
        //    var insightRepository = new Mock<IInsightsEFRepository>();
        //    var weatherReadings = TestHelpers.CreateMockWeatherData(startDate, endDate);
        //    insightRepository.Setup(i => i.GetWeather(clientId, It.Is<WeatherRequest>(r => r.ZipCode == zip && r.StartDate == startDate && r.EndDate == endDate))).Returns(weatherReadings);


        //    var controller = new Controllers.Version2.ConsumptionController(container, insightRepository.Object);

        //    var configuration = new HttpConfiguration();
        //    var request = new HttpRequestMessage();

        //    controller.Request = request;
        //    controller.Request.Properties["MS_HttpConfiguration"] = configuration;
        //    //add property for client user, normally from the stored procedure but hardcoded here for unit test purposes
        //    var clientUser = new ClientUser
        //    {
        //        ClientID = clientId,
        //        ClientProperties = new List<ClientProperties>
        //        {
        //            new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
        //            new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
        //        }
        //    };

        //    controller.Request.Properties[CEConfiguration.CEClientUser] = clientUser;
        //    controller.Request.Properties[CEConfiguration.CELocale] = "es-ES";

        //    //UnityConfig.GetConfiguredContainer();

        //    var result = controller.Get(consumptionRequest);
        //    //var result = model.GetBillInfo(clientUser, billToDateRequest);

        //    Assert.IsNotNull(result);
        //    //Assert.AreEqual("month,day,hour", result.ResolutionsAvailable);
        //    //Assert.IsNotNull(result.Data);
        //    //Assert.AreEqual(startDate, result.Data[0].DateTime);
        //    //Assert.AreSame(billToDateRequest.CustomerId, result.FirstOrDefault().CustomerId);
        //}

        //[TestMethod, TestCategory("ConsumptionController"), TestCategory("BVT")]
        //public void ConsumptionV2_Get_NoResolutionKey_Test()
        //{
        //    //var consumptionRequest = new CE.Models.Insights.ConsumptionRequest();
        //    //consumptionRequest.CustomerId = "1000";
        //    //consumptionRequest.AccountId = "a1";

        //    var clientId = 87;
        //    var customerId = "BTD001";
        //    var premiseId = "BTD001a1p1";
        //    var accountId = "BTD001a1";
        //    var servicePointId = "SP_BTD001a1";
        //    var meterId = "EM001";
        //    var startDate = Convert.ToDateTime("10/17/2015");
        //    var endDate = Convert.ToDateTime("1/17/2016");
        //    var projected = Convert.ToDateTime("1/17/2016");

        //    var meterIds = new List<string>();
        //    meterIds.Add(meterId);

        //    var consumptionRequest = new ConsumptionRequest
        //    {
        //        CustomerId = customerId,
        //        PremiseId = premiseId,
        //        AccountId = accountId,
        //        MeterIds = meterId,
        //        StartDate = startDate,
        //        EndDate = endDate,
        //        Count = 0,
        //        IncludeResolutions = true,
        //        CommodityKey = "electric"
        //    };


        //    var container = new UnityContainer();

        //    (new DataStorageRegistrar()).Initialize<TransientLifetimeManager>(container);


        //    // mock ami hourly data
        //    var tableRepository = new Mock<ITableRepository>();
        //    container.RegisterInstance(tableRepository.Object);
        //    var readings = TestHelpers.CreateMockAzureAmiHourlyData(clientId, accountId, meterId, servicePointId, startDate, endDate, projected);
        //    var filter = string.Format("(PartitionKey eq '{0}_{1}') and ((RowKey ge '{2}') and (RowKey le '{3}'))", clientId.ToString(), meterId, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"));
        //    tableRepository.Setup(s => s.GetAllAsync<Ami15MinuteIntervalEntity>(Constants.TableNames.Ami, filter)).Returns(readings);

        //    var controller = new Controllers.Version2.ConsumptionController(container);
        //    var configuration = new HttpConfiguration();
        //    var request = new HttpRequestMessage();

        //    controller.Request = request;
        //    controller.Request.Properties["MS_HttpConfiguration"] = configuration;
        //    //add property for client user, normally from the stored procedure but hardcoded here for unit test purposes
        //    var clientUser = new ClientUser
        //    {
        //        ClientID = clientId,
        //        ClientProperties = new List<ClientProperties>
        //        {
        //            new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
        //            new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
        //        }
        //    };

        //    controller.Request.Properties[CEConfiguration.CEClientUser] = clientUser;
        //    controller.Request.Properties[CEConfiguration.CELocale] = "es-ES";

        //    //UnityConfig.GetConfiguredContainer();

        //    var result = controller.Get(consumptionRequest);
        //    //var result = model.GetBillInfo(clientUser, billToDateRequest);

        //    Assert.IsNotNull(result);
        //    //Assert.AreEqual("month,day,hour", result.ResolutionsAvailable);
        //    //Assert.IsNotNull(result.Data);
        //    //Assert.AreEqual(startDate, result.Data[0].DateTime);
        //    //Assert.AreSame(billToDateRequest.CustomerId, result.FirstOrDefault().CustomerId);
        //}

        //[TestMethod, TestCategory("BillController"), TestCategory("BVT")]
        //public void BillV2_Get_Test()
        //{
        //    var tableRepository = new Mock<ITableRepository>();
        //    var container = new UnityContainer();

        //    (new DataStorageRegistrar()).Initialize<TransientLifetimeManager>(container);


        //    Mapper.CreateMap<CustomerEntity, CustomerModel>();
        //    Mapper.CreateMap<PremiseEntity, PremiseModel>();
        //    Mapper.CreateMap<BillingEntity, BillingModel>();
        //    container.RegisterInstance(tableRepository.Object);


        //    var clientId = 87;
        //    var firstname = "TestFirst";
        //    var lastname = "TestLast";
        //    var customerId = "BTD001";
        //    var accountId = "BTD001a1";
        //    var premiseId = "BTD001a1p1";
        //    var addr1 = "16 Laurel Ave";
        //    var addr2 = "Suite 100";
        //    var city = "Wellesley";
        //    var state = "MA";
        //    var zip = "02481";
        //    var email = "test@aclara.com";
        //    var billCycleScheduleId = "Cycle01";
        //    var meterId1 = "EM001";
        //    var rateClass = "RG-1";
        //    var servicePointId1 = "SP_BTD001a1_1";
        //    var serviceContractId1 = "BTD001a1_BTD001a1p1_1_1_EM001";
        //    var today = DateTime.Today.Date;
        //    var currentStart = today.AddDays(-6);
        //    var lastBillEndDate = currentStart.AddDays(-1);
        //    var lastBillStartDate = lastBillEndDate.AddDays(-29);
        //    DateTime end;
        //    var start = currentStart;

        //    // mock billing entities data
        //    var bills = new List<BillingEntity>();
        //    for (int i = 0; i < 5; i++)
        //    {
        //        end = start.AddDays(-1);
        //        start = end.AddDays(-29);
        //        var mockBillingEntity = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
        //            servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, start, end);

        //        bills.Add(mockBillingEntity);
        //    }
        //    var filter = string.Format("(PartitionKey eq '{0}_{1}_{2}') and ((RowKey le 'H_{3}_{4}') and (RowKey ge 'H_{3}_{5}'))", clientId.ToString(), customerId.ToLower(), accountId.ToLower(), serviceContractId1.ToLower(), today.ToString("yyyy-MM-dd"), start.ToString("yyyy-MM-dd"));
        //    tableRepository.Setup(s => s.GetAllAsync<BillingEntity>(Constants.TableNames.Billing, filter)).Returns(bills);
        //    // mock services data
        //    var mockServicesEntity = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
        //        servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
        //    var services = new List<BillingEntity>();
        //    services.Add(mockServicesEntity);
        //    var filterService = string.Format("(PartitionKey eq '{0}_{1}_{2}') and ((RowKey ge 'B') and (RowKey lt 'C'))", clientId.ToString(), customerId.ToLower(), accountId.ToLower());
        //    tableRepository.Setup(s => s.GetAllAsync<BillingEntity>(Constants.TableNames.Billing, filterService)).Returns(services);
        //    // mock premise entity
        //    var mockPremise = TestHelpers.CreateMockPremiseEntityData(clientId, customerId, premiseId, accountId, addr1, addr2, city, state, zip);
        //    var mockPremises = new List<PremiseEntity>();
        //    mockPremises.Add(mockPremise);
        //    var filterPremise = string.Format("(PartitionKey eq '{0}_{1}') and ((RowKey ge 'P') and (RowKey lt 'Q'))", clientId, customerId.ToLower());
        //    tableRepository.Setup(s => s.GetAllAsync<PremiseEntity>(Constants.TableNames.CustomerPremiseProfile, filterPremise)).Returns(mockPremises);
        //    // mock customer entity
        //    var mockCustomer = TestHelpers.CreateMockResidentialCustomerEntityData(clientId, customerId, firstname, lastname,
        //        addr1, addr2, city, state, zip, email);
        //    tableRepository.Setup(c => c.GetSingleAsync<CustomerEntity>(Constants.TableNames.CustomerPremiseProfile, string.Format("{0}_{1}", clientId, customerId.ToLower()), "C")).ReturnsAsync(mockCustomer);

        //    var controller = new Controllers.Version2.BillController(container);
        //    //var model = new billm(container);
        //    var configuration = new HttpConfiguration();
        //    var request = new HttpRequestMessage();

        //    controller.Request = request;
        //    controller.Request.Properties["MS_HttpConfiguration"] = configuration;
        //    //add property for client user, normally from the stored procedure but hardcoded here for unit test purposes
        //    var clientUser = new ClientUser
        //    {
        //        ClientID = clientId,
        //        ClientProperties = new List<ClientProperties>
        //        {
        //            new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
        //            new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
        //        }
        //    };

        //    controller.Request.Properties[CEConfiguration.CEClientUser] = clientUser;
        //    controller.Request.Properties[CEConfiguration.CELocale] = "es-ES";

        //    var billRequest = new BillRequest
        //    {
        //        CustomerId = customerId,
        //        StartDate = start,
        //        EndDate = today,
        //        Count = 8,
        //        IncludeContent = false
        //    };

        //    var result = controller.Get(billRequest);

        //    Assert.IsNotNull(result);


        //}


        [TestMethod, TestCategory("RateController"), TestCategory("BVT")]
        public void RateV1_Get_Test()
        {


            var clientId = 87;
            var rateClass = "RG-1";
            var today = DateTime.Today.Date;


            var controller = new Insights.Controllers.Version1.RateController();
            //var model = new billm(container);
            var configuration = new HttpConfiguration();
            var request = new HttpRequestMessage();

            controller.Request = request;
            controller.Request.Properties["MS_HttpConfiguration"] = configuration;
            //add property for client user, normally from the stored procedure but hardcoded here for unit test purposes
            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };

            controller.Request.Properties[CEConfiguration.CEClientUser] = clientUser;
            controller.Request.Properties[CEConfiguration.CELocale] = "es-ES";

            var rateRequest = new RateRequest
            {
                RateClass = rateClass,
                Date = today,
                IncludeRateDetails = true,
                StartDate = Convert.ToDateTime("5/1/2015"),
                EndDate = Convert.ToDateTime("5/15/2015"),
                IncludeFinalizedTierBoundaries = true,
                UseProjectedEndDateToFinalizeTier = true
            };

            var result = controller.Get(rateRequest);

            Assert.IsNotNull(result);


        }

        [TestMethod, TestCategory("SubscriptionController"), TestCategory("BVT")]
        public void SubscriptioinV1_Get_Test()
        {

            var clientId = 87;
            var customerId = "993774751";
            var accountId = "1218337093";

            var container = new UnityContainer();

            (new DataStorageRegistrar()).Initialize<TransientLifetimeManager>(container);

            var iClientConfigFacade = new Mock<IClientConfigFacade>();

            container.RegisterInstance(iClientConfigFacade.Object);

            var mockClientSetting = TestHelpers.CreateMockDefaultSubscriptionString();
            var clientSetting = JsonConvert.DeserializeObject<ClientSettings>(mockClientSetting);

            iClientConfigFacade.Setup(t => t.GetClientSettings(It.IsAny<int>())).Returns(clientSetting).Verifiable();


            Mapper.CreateMap<SubscriptionEntity, SubscriptionModel>().ForAllUnmappedMembers(o => o.Ignore());
            Mapper.CreateMap<SubscriptionModel, SubscriptionEntity>().ForAllUnmappedMembers(o => o.Ignore());
            Mapper.CreateMap<BillingEntity, BillingModel>();
            Mapper.CreateMap<CustomerEntity, CustomerModel>();
            Mapper.CreateMap<PremiseEntity, PremiseModel>();
            Mapper.CreateMap<CustomerModel, CustomerEntity>();

            var controller = new Insights.Controllers.Version1.SubscriptionController(container);
            //var model = new billm(container);
            var configuration = new HttpConfiguration();
            var request = new HttpRequestMessage();

            controller.Request = request;
            controller.Request.Properties["MS_HttpConfiguration"] = configuration;
            //add property for client user, normally from the stored procedure but hardcoded here for unit test purposes
            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };

            controller.Request.Properties[CEConfiguration.CEClientUser] = clientUser;
            controller.Request.Properties[CEConfiguration.CELocale] = "es-ES";

            var rateRequest = new SubscriptionRequest
            {
                AccountId = accountId,
                CustomerId = customerId
            };

            var result = controller.Get(rateRequest);

            Assert.IsNotNull(result);
            Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);


        }

        [TestMethod, TestCategory("SubscriptionController"), TestCategory("BVT")]
        public void SubscriptioinV1_Post_Test()
        {

            var clientId = 87;
            var customerId = "993774751";
            var accountId = "1218337093";
            var premiseId = "1218337000";
            var serviceContractId1 = "1218337093_1218337000_2_M_1218337000_gas";
            var programInsight1 = "Program 1.AccountLevelCostThreshold";

            var container = new UnityContainer();

            (new DataStorageRegistrar()).Initialize<TransientLifetimeManager>(container);



            Mapper.CreateMap<SubscriptionEntity, SubscriptionModel>().ForAllUnmappedMembers(o => o.Ignore());
            Mapper.CreateMap<SubscriptionModel, SubscriptionEntity>().ForAllUnmappedMembers(o => o.Ignore());
            Mapper.CreateMap<BillingEntity, BillingModel>();

            var controller = new Insights.Controllers.Version1.SubscriptionController(container);
            //var model = new billm(container);
            var configuration = new HttpConfiguration();
            var request = new HttpRequestMessage();

            controller.Request = request;
            controller.Request.Properties["MS_HttpConfiguration"] = configuration;
            //add property for client user, normally from the stored procedure but hardcoded here for unit test purposes
            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };

            controller.Request.Properties[CEConfiguration.CEClientUser] = clientUser;
            controller.Request.Properties[CEConfiguration.CELocale] = "es-ES";

            var subscribeList = new List<SubscribedInsight>();
            var subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = true,
                Level = "account",
                Name = programInsight1,
                PremiseId = premiseId,
                //ServiceContractId = serviceContractId1,
                ServiceId = serviceContractId1
            };
            subscribeList.Add(subscribeInsight);



            var post = new SubscriptionPostRequest
            {
                Customer = new SubscribedCustomerInsight
                {
                    Id = customerId,
                    Insights = subscribeList
                }
            };

            var result = controller.Post(post);

            Assert.IsNotNull(result);
            Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);


        }

        [TestMethod, TestCategory("SubscriptionController"), TestCategory("BVT")]
        public void SubscriptioinV1_Post_Unsubscribe_Test()
        {

            var clientId = 87;
            var customerId = "993774751";
            var accountId = "1218337093";
            var premiseId = "1218337000";
            var serviceContractId1 = "1218337093_1218337000_2_M_1218337000_gas";
            var programInsight1 = "Program 1.AccountLevelCostThreshold";

            var container = new UnityContainer();

            (new DataStorageRegistrar()).Initialize<TransientLifetimeManager>(container);



            Mapper.CreateMap<SubscriptionEntity, SubscriptionModel>().ForAllUnmappedMembers(o => o.Ignore());
            Mapper.CreateMap<SubscriptionModel, SubscriptionEntity>().ForAllUnmappedMembers(o => o.Ignore());
            Mapper.CreateMap<BillingEntity, BillingModel>();

            var controller = new Insights.Controllers.Version1.SubscriptionController(container);
            //var model = new billm(container);
            var configuration = new HttpConfiguration();
            var request = new HttpRequestMessage();

            controller.Request = request;
            controller.Request.Properties["MS_HttpConfiguration"] = configuration;
            //add property for client user, normally from the stored procedure but hardcoded here for unit test purposes
            var clientUser = new ClientUser
            {
                ClientID = clientId,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };

            controller.Request.Properties[CEConfiguration.CEClientUser] = clientUser;
            controller.Request.Properties[CEConfiguration.CELocale] = "es-ES";

            var subscribeList = new List<SubscribedInsight>();
            var subscribeInsight = new SubscribedInsight
            {
                AccountId = accountId,
                IsSelected = false,
                Level = "account",
                Name = programInsight1,
                PremiseId = premiseId,
                //ServiceContractId = serviceContractId1,
                ServiceId = serviceContractId1
            };
            subscribeList.Add(subscribeInsight);



            var post = new SubscriptionPostRequest
            {
                Customer = new SubscribedCustomerInsight
                {
                    Id = customerId,
                    Insights = subscribeList
                }
            };

            var result = controller.Post(post);

            Assert.IsNotNull(result);
            Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);


        }

        [TestMethod, TestCategory("GreenButtonBill"), TestCategory("BVT")]
        public void GreenButtonBillV1_Get_Zip_Test()
        {

            try
            {
                var clientId = 87;
                var customerId = "993774751";
                //var accountId = "1218337093";
                //var premiseId = "1218337000";
                //var servicePointId1 = "SP_M_1218337000_gas";
                //var serviceContractId1 = "1218337093_1218337000_2_M_1218337000_gas";

                var controller = new Insights.Controllers.Version1.GreenButtonBillController();
                //var model = new billm(container);
                var configuration = new HttpConfiguration();
                var request = new HttpRequestMessage();

                controller.Request = request;
                controller.Request.Properties["MS_HttpConfiguration"] = configuration;
                //add property for client user, normally from the stored procedure but hardcoded here for unit test purposes
                var clientUser = new ClientUser
                {
                    ClientID = clientId,
                    ClientProperties = new List<ClientProperties>
                    {
                        new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                        new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                    }
                };

                controller.Request.Properties[CEConfiguration.CEClientUser] = clientUser;
                controller.Request.Properties[CEConfiguration.CELocale] = "es-ES";



                var gbBillRequest = new GreenButtonBillRequest
                {
                    CustomerId = customerId,
                    OutputFormat = "Zip"
                };

                var result = controller.Get(gbBillRequest);

                Assert.IsNotNull(result);
                Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
                // comment out the following, so the build agent will work
                //var data = result.Content.ReadAsByteArrayAsync().Result;
                //using (var ms = new MemoryStream(data))
                //{
                //    using (
                //    var fileStream = new FileStream(@"C:\\" + result.Content.Headers.ContentDisposition.FileName,
                //        FileMode.Create))
                //    {
                //        ms.Seek(0, SeekOrigin.Begin);
                //        ms.CopyTo(fileStream);
                //    }
                //}

                //Assert.IsTrue(File.Exists(@"C:\\" + result.Content.Headers.ContentDisposition.FileName));

                //File.Delete(@"C:\\" + result.Content.Headers.ContentDisposition.FileName);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }

        }

        ///// <summary>
        ///// Verifies file format and stream
        ///// </summary>
        //[TestMethod, TestCategory("GreenButtonBill")]
        //public void GreenButtonBillV1_Get_Zip_FileVerify()
        //{

        //    try
        //    {
        //        var clientId = 87;
        //        var customerId = "993774751";
        //        //var accountId = "1218337093";
        //        //var premiseId = "1218337000";
        //        //var servicePointId1 = "SP_M_1218337000_gas";
        //        //var serviceContractId1 = "1218337093_1218337000_2_M_1218337000_gas";

        //        var controller = new Insights.Controllers.Version1.GreenButtonBillController();
        //        //var model = new billm(container);
        //        var configuration = new HttpConfiguration();
        //        var request = new HttpRequestMessage();

        //        controller.Request = request;
        //        controller.Request.Properties["MS_HttpConfiguration"] = configuration;
        //        //add property for client user, normally from the stored procedure but hardcoded here for unit test purposes
        //        var clientUser = new ClientUser
        //        {
        //            ClientID = clientId,
        //            ClientProperties = new List<ClientProperties>
        //            {
        //                new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
        //                new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
        //            }
        //        };

        //        controller.Request.Properties[CEConfiguration.CEClientUser] = clientUser;
        //        controller.Request.Properties[CEConfiguration.CELocale] = "es-ES";



        //        var gbBillRequest = new GreenButtonBillRequest
        //        {
        //            CustomerId = customerId,
        //            OutputFormat = "Zip"
        //        };

        //        var result = controller.Get(gbBillRequest);

        //        Assert.IsNotNull(result);
        //        Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);

        //        var data = result.Content.ReadAsByteArrayAsync().Result;
        //        using (var ms = new MemoryStream(data))
        //        {
        //            using (
        //            var fileStream = new FileStream(@"C:\\" + result.Content.Headers.ContentDisposition.FileName,
        //              FileMode.Create))
        //            {
        //                ms.Seek(0, SeekOrigin.Begin);
        //                ms.CopyTo(fileStream);
        //            }
        //        }

        //        Assert.IsTrue(File.Exists(@"C:\\" + result.Content.Headers.ContentDisposition.FileName));
        //        File.Delete(@"C:\\" + result.Content.Headers.ContentDisposition.FileName);
        //    }
        //    catch (Exception ex)
        //    {
        //        Assert.Fail(ex.Message);
        //    }

        //}
        [TestMethod, TestCategory("GreenButtonBill"), TestCategory("BVT")]
        public void GreenButtonBillV1_Get_Zip2_Test()
        {

            try
            {
                var clientId = 87;
                var customerId = "993774751";
                var accountId = "1218337093";
                var premiseId = "1218337000";
                //var servicePointId1 = "SP_M_1218337000_gas";
                var serviceContractId1 = "SC_1218337000_gas";

                var controller = new Insights.Controllers.Version1.GreenButtonBillController();
                //var model = new billm(container);
                var configuration = new HttpConfiguration();
                var request = new HttpRequestMessage();

                controller.Request = request;
                controller.Request.Properties["MS_HttpConfiguration"] = configuration;
                //add property for client user, normally from the stored procedure but hardcoded here for unit test purposes
                var clientUser = new ClientUser
                {
                    ClientID = clientId,
                    ClientProperties = new List<ClientProperties>
                    {
                        new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                        new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                    }
                };

                controller.Request.Properties[CEConfiguration.CEClientUser] = clientUser;
                controller.Request.Properties[CEConfiguration.CELocale] = "es-ES";



                var gbBillRequest = new GreenButtonBillRequest
                {
                    CustomerId = customerId,
                    AccountId = accountId,
                    PremiseId = premiseId,
                    ServiceContractId = serviceContractId1,
                    OutputFormat = "Zip"
                };

                var result = controller.Get(gbBillRequest);

                Assert.IsNotNull(result);
                Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);

            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }

        }

        ///// <summary>
        ///// Verify green button download is in xmlformat on content header matching original request format
        ///// </summary>
        //[TestMethod, TestCategory("GreenButtonBill")]
        //public void GreenButtonBillV1_Get_Xml_MediaXML()
        //{

        //    try
        //    {
        //        var clientId = 87;
        //        var customerId = "993774751";
        //        var accountId = "1218337093";
        //        var premiseId = "1218337000";
        //        var serviceContractId1 = "SC_189354704";

        //        var controller = new Insights.Controllers.Version1.GreenButtonBillController();
        //        //var model = new billm(container);
        //        var configuration = new HttpConfiguration();
        //        var request = new HttpRequestMessage();

        //        controller.Request = request;
        //        controller.Request.Properties["MS_HttpConfiguration"] = configuration;
        //        //add property for client user, normally from the stored procedure but hardcoded here for unit test purposes
        //        var clientUser = new ClientUser
        //        {
        //            ClientID = clientId,
        //            ClientProperties = new List<ClientProperties>
        //            {
        //                new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
        //                new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
        //            }
        //        };

        //        controller.Request.Properties[CEConfiguration.CEClientUser] = clientUser;
        //        controller.Request.Properties[CEConfiguration.CELocale] = "es-ES";



        //        var gbBillRequest = new GreenButtonBillRequest
        //        {
        //            CustomerId = customerId,
        //            AccountId = accountId,
        //            PremiseId = premiseId,
        //            ServiceContractId = serviceContractId1,
        //            OutputFormat = "Xml"
        //        };

        //        var result = controller.Get(gbBillRequest);

        //        Assert.IsNotNull(result);
        //        Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
        //        //Assert.AreEqual("application/xml", result.Content.Headers.ContentType.MediaType);
        //    }
        //    catch (Exception ex)
        //    {
        //        Assert.Fail(ex.Message);
        //    }

        //}


        [TestMethod, TestCategory("GreenButtonBill"), TestCategory("BVT")]
        public void GreenButtonBillV1_Get_Xml_Test()
        {

            try
            {
                var clientId = 87;
                var customerId = "993774751";
                var accountId = "1218337093";
                var premiseId = "1218337000";
                //var servicePointId1 = "SP_M_1218337000_gas";
                var serviceContractId1 = "SC_1218337000_gas";

                var controller = new Insights.Controllers.Version1.GreenButtonBillController();
                //var model = new billm(container);
                var configuration = new HttpConfiguration();
                var request = new HttpRequestMessage();

                controller.Request = request;
                controller.Request.Properties["MS_HttpConfiguration"] = configuration;
                //add property for client user, normally from the stored procedure but hardcoded here for unit test purposes
                var clientUser = new ClientUser
                {
                    ClientID = clientId,
                    ClientProperties = new List<ClientProperties>
                    {
                        new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                        new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                    }
                };

                controller.Request.Properties[CEConfiguration.CEClientUser] = clientUser;
                controller.Request.Properties[CEConfiguration.CELocale] = "es-ES";



                var gbBillRequest = new GreenButtonBillRequest
                {
                    CustomerId = customerId,
                    AccountId = accountId,
                    PremiseId = premiseId,
                    ServiceContractId = serviceContractId1,
                    OutputFormat = "Xml"
                };

                var result = controller.Get(gbBillRequest);

                Assert.IsNotNull(result);
                Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }

        }


        //[TestMethod, TestCategory("GreenButtonAmi")]
        //public void GreenButtonAmiV1_Get_Zip_FileVerify()
        //{

        //    try
        //    {
        //        var container = new UnityContainer();

        //        RegisterTypesForUnity.RegisterTypes(container);

        //        var clientId = 87;
        //        var customerId = "GB001";
        //        var accountId = "GB001a1";
        //        var premiseId = "GB01a1p1";
        //        //var serviceId = "GB01a1p1s1";
        //        var servicePointId = "SP_GB001a1";
        //        var start15Date = Convert.ToDateTime("12/17/2015");
        //        var end15Date = Convert.ToDateTime("12/18/2015");
        //        var start30Date = Convert.ToDateTime("12/19/2015");
        //        var end30Date = Convert.ToDateTime("12/20/2015");
        //        var start60Date = Convert.ToDateTime("12/21/2015");
        //        var end60Date = Convert.ToDateTime("12/22/2015");
        //        var projected = Convert.ToDateTime("1/17/2016");
        //        var commodityId = 1;
        //        var uomId = 0;
        //        var meterId = "EM001";

        //        #region "test data"
        //        var tableRepository = new Mock<ITableRepository>();
        //        container.RegisterInstance(tableRepository.Object);
        //        var readings = new List<DynamicTableEntity>();
        //        // mock ami 15min data
        //        var fifteenreadings = TestHelpers.CreateMockAzureAmi15Data(clientId, accountId, meterId, servicePointId,
        //            start15Date, end15Date, projected, commodityId, uomId);
        //        foreach (var dynamicTableEntity in fifteenreadings)
        //        {
        //            readings.Add(dynamicTableEntity);
        //        }
        //        // mock ami 30min data
        //        var thirtyreadings = TestHelpers.CreateMockAzureAmi30Data(clientId, accountId, meterId, servicePointId,
        //            start30Date, end30Date, projected, commodityId, uomId);
        //        foreach (var dynamicTableEntity in thirtyreadings)
        //        {
        //            readings.Add(dynamicTableEntity);
        //        }

        //        // mock ami hourly data
        //        var hourreadings = TestHelpers.CreateMockAzureAmiHourlyData(clientId, accountId, meterId, servicePointId,
        //            start60Date, end60Date, projected, commodityId, uomId);
        //        foreach (var dynamicTableEntity in hourreadings)
        //        {
        //            readings.Add(dynamicTableEntity);
        //        }
        //        var filter = string.Format("(PartitionKey eq '{0}_{1}') and ((RowKey ge '{2}') and (RowKey le '{3}'))",
        //            clientId.ToString(), meterId.ToLower(), start15Date.ToString("yyyy-MM-dd"), end60Date.ToString("yyyy-MM-dd"));
        //        tableRepository.Setup(s => s.GetAllAsync<DynamicTableEntity>(Constants.TableNames.Ami, filter))
        //            .Returns(readings);
        //        #endregion

        //        var controller = new Insights.Controllers.Version1.GreenButtonAmiController(container);
        //        //var model = new billm(container);
        //        var configuration = new HttpConfiguration();
        //        var request = new HttpRequestMessage();

        //        controller.Request = request;
        //        controller.Request.Properties["MS_HttpConfiguration"] = configuration;
        //        //add property for client user, normally from the stored procedure but hardcoded here for unit test purposes
        //        var clientUser = new ClientUser
        //        {
        //            ClientID = clientId,
        //            ClientProperties = new List<ClientProperties>
        //            {
        //                new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
        //                new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
        //            }
        //        };

        //        controller.Request.Properties[CEConfiguration.CEClientUser] = clientUser;
        //        controller.Request.Properties[CEConfiguration.CELocale] = "es-ES";



        //        var gbBillRequest = new GreenButtonAmiRequest
        //        {
        //            CustomerId = customerId,
        //            AccountId = accountId,
        //            PremiseId = premiseId,
        //            MeterIds = meterId,
        //            StartDate = start15Date,
        //            EndDate = end60Date,
        //            OutputFormat = "Zip"
        //        };

        //        var result = controller.Get(gbBillRequest);

        //        Assert.IsNotNull(result);
        //        Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);

        //        var data = result.Content.ReadAsByteArrayAsync().Result;
        //        using (var ms = new MemoryStream(data))
        //        {
        //            using (
        //            var fileStream = new FileStream(@"C:\\" + result.Content.Headers.ContentDisposition.FileName,
        //              FileMode.Create))
        //            {
        //                ms.Seek(0, SeekOrigin.Begin);
        //                ms.CopyTo(fileStream);
        //            }
        //        }

        //        Assert.IsTrue(File.Exists(@"C:\\" + result.Content.Headers.ContentDisposition.FileName));
        //        File.Delete(@"C:\\" + result.Content.Headers.ContentDisposition.FileName);
        //    }
        //    catch (Exception ex)
        //    {
        //        Assert.Fail(ex.Message);
        //    }

        //}
        //[TestMethod, TestCategory("GreenButtonAmi"), TestCategory("BVT")]
        //public void GreenButtonAmiV1_Get_Zip2_Test()
        //{

        //    try
        //    {
        //        var container = new UnityContainer();

        //        RegisterTypesForUnity.RegisterTypes(container);

        //        var clientId = 87;
        //        var customerId = "GB001";
        //        var accountId = "GB001a1";
        //        var premiseId = "GB01a1p1";
        //        //var serviceId = "GB01a1p1s1";
        //        var servicePointId = "SP_GB001a1";
        //        var start15Date = Convert.ToDateTime("12/17/2015");
        //        var end15Date = Convert.ToDateTime("12/18/2015");
        //        var start30Date = Convert.ToDateTime("12/19/2015");
        //        var end30Date = Convert.ToDateTime("12/20/2015");
        //        var start60Date = Convert.ToDateTime("12/21/2015");
        //        var end60Date = Convert.ToDateTime("12/22/2015");
        //        var projected = Convert.ToDateTime("1/17/2016");
        //        var commodityId = 1;
        //        var uomId = 0;
        //        var meterId = "EM001";

        //        #region "test data"
        //        var tableRepository = new Mock<ITableRepository>();
        //        container.RegisterInstance(tableRepository.Object);
        //        var readings = new List<DynamicTableEntity>();
        //        // mock ami 15min data
        //        var fifteenreadings = TestHelpers.CreateMockAzureAmi15Data(clientId, accountId, meterId, servicePointId,
        //            start15Date, end15Date, projected, commodityId, uomId);
        //        foreach (var dynamicTableEntity in fifteenreadings)
        //        {
        //            readings.Add(dynamicTableEntity);
        //        }
        //        // mock ami 30min data
        //        var thirtyreadings = TestHelpers.CreateMockAzureAmi30Data(clientId, accountId, meterId, servicePointId,
        //            start30Date, end30Date, projected, commodityId, uomId);
        //        foreach (var dynamicTableEntity in thirtyreadings)
        //        {
        //            readings.Add(dynamicTableEntity);
        //        }

        //        // mock ami hourly data
        //        var hourreadings = TestHelpers.CreateMockAzureAmiHourlyData(clientId, accountId, meterId, servicePointId,
        //            start60Date, end60Date, projected, commodityId, uomId);
        //        foreach (var dynamicTableEntity in hourreadings)
        //        {
        //            readings.Add(dynamicTableEntity);
        //        }
        //        var filter = string.Format("(PartitionKey eq '{0}_{1}') and ((RowKey ge '{2}') and (RowKey le '{3}'))",
        //            clientId.ToString(), meterId.ToLower(), start15Date.ToString("yyyy-MM-dd"), end60Date.ToString("yyyy-MM-dd"));
        //        tableRepository.Setup(s => s.GetAllAsync<DynamicTableEntity>(Constants.TableNames.Ami, filter))
        //            .Returns(readings);
        //        #endregion

        //        var controller = new Insights.Controllers.Version1.GreenButtonAmiController(container);
        //        //var model = new billm(container);
        //        var configuration = new HttpConfiguration();
        //        var request = new HttpRequestMessage();

        //        controller.Request = request;
        //        controller.Request.Properties["MS_HttpConfiguration"] = configuration;
        //        //add property for client user, normally from the stored procedure but hardcoded here for unit test purposes
        //        var clientUser = new ClientUser
        //        {
        //            ClientID = clientId,
        //            ClientProperties = new List<ClientProperties>
        //            {
        //                new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
        //                new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
        //            }
        //        };

        //        controller.Request.Properties[CEConfiguration.CEClientUser] = clientUser;
        //        controller.Request.Properties[CEConfiguration.CELocale] = "es-ES";



        //        var gbBillRequest = new GreenButtonAmiRequest
        //        {
        //            CustomerId = customerId,
        //            AccountId = accountId,
        //            PremiseId = premiseId,
        //            MeterIds = meterId,
        //            StartDate = start15Date,
        //            EndDate = end60Date,
        //            OutputFormat = "Zip"
        //        };

        //        var result = controller.Get(gbBillRequest);

        //        Assert.IsNotNull(result);
        //        Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);

        //    }
        //    catch (Exception ex)
        //    {
        //        Assert.Fail(ex.Message);
        //    }

        //}

        ///// <summary>
        ///// Verify green button download is in xmlformat on content header matching original request format
        ///// </summary>
        //[TestMethod, TestCategory("GreenButtonAmi")]
        //public void GreenButtonAmiV1_Get_Xml_MediaXML()
        //{

        //    try
        //    {
        //        var container = new UnityContainer();

        //        RegisterTypesForUnity.RegisterTypes(container);

        //        var clientId = 87;
        //        var customerId = "GB001";
        //        var accountId = "GB001a1";
        //        var premiseId = "GB01a1p1";
        //        //var serviceId = "GB01a1p1s1";
        //        var servicePointId = "SP_GB001a1";
        //        var start15Date = Convert.ToDateTime("12/17/2015");
        //        var end15Date = Convert.ToDateTime("12/18/2015");
        //        var start30Date = Convert.ToDateTime("12/19/2015");
        //        var end30Date = Convert.ToDateTime("12/20/2015");
        //        var start60Date = Convert.ToDateTime("12/21/2015");
        //        var end60Date = Convert.ToDateTime("12/22/2015");
        //        var projected = Convert.ToDateTime("1/17/2016");
        //        var commodityId = 1;
        //        var uomId = 0;
        //        var meterId = "EM001";

        //        #region "test data"
        //        var tableRepository = new Mock<ITableRepository>();
        //        container.RegisterInstance(tableRepository.Object);
        //        var readings = new List<DynamicTableEntity>();
        //        // mock ami 15min data
        //        var fifteenreadings = TestHelpers.CreateMockAzureAmi15Data(clientId, accountId, meterId, servicePointId,
        //            start15Date, end15Date, projected, commodityId, uomId);
        //        foreach (var dynamicTableEntity in fifteenreadings)
        //        {
        //            readings.Add(dynamicTableEntity);
        //        }
        //        // mock ami 30min data
        //        var thirtyreadings = TestHelpers.CreateMockAzureAmi30Data(clientId, accountId, meterId, servicePointId,
        //            start30Date, end30Date, projected, commodityId, uomId);
        //        foreach (var dynamicTableEntity in thirtyreadings)
        //        {
        //            readings.Add(dynamicTableEntity);
        //        }

        //        // mock ami hourly data
        //        var hourreadings = TestHelpers.CreateMockAzureAmiHourlyData(clientId, accountId, meterId, servicePointId,
        //            start60Date, end60Date, projected, commodityId, uomId);
        //        foreach (var dynamicTableEntity in hourreadings)
        //        {
        //            readings.Add(dynamicTableEntity);
        //        }
        //        var filter = string.Format("(PartitionKey eq '{0}_{1}') and ((RowKey ge '{2}') and (RowKey le '{3}'))",
        //            clientId.ToString(), meterId.ToLower(), start15Date.ToString("yyyy-MM-dd"), end60Date.ToString("yyyy-MM-dd"));
        //        tableRepository.Setup(s => s.GetAllAsync<DynamicTableEntity>(Constants.TableNames.Ami, filter))
        //            .Returns(readings);
        //        #endregion

        //        var controller = new Insights.Controllers.Version1.GreenButtonAmiController(container);
        //        //var model = new billm(container);
        //        var configuration = new HttpConfiguration();
        //        var request = new HttpRequestMessage();

        //        controller.Request = request;
        //        controller.Request.Properties["MS_HttpConfiguration"] = configuration;
        //        //add property for client user, normally from the stored procedure but hardcoded here for unit test purposes
        //        var clientUser = new ClientUser
        //        {
        //            ClientID = clientId,
        //            ClientProperties = new List<ClientProperties>
        //            {
        //                new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
        //                new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
        //            }
        //        };

        //        controller.Request.Properties[CEConfiguration.CEClientUser] = clientUser;
        //        controller.Request.Properties[CEConfiguration.CELocale] = "es-ES";



        //        var gbBillRequest = new GreenButtonAmiRequest
        //        {
        //            CustomerId = customerId,
        //            AccountId = accountId,
        //            PremiseId = premiseId,
        //            MeterIds = meterId,
        //            StartDate = start15Date,
        //            EndDate = end60Date,
        //            OutputFormat = "Xml"
        //        };

        //        var result = controller.Get(gbBillRequest);

        //        Assert.IsNotNull(result);
        //        Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
        //        Assert.AreEqual("application/xml", result.Content.Headers.ContentType.MediaType);
        //    }
        //    catch (Exception ex)
        //    {
        //        Assert.Fail(ex.Message);
        //    }

        //}

        //[TestMethod, TestCategory("GreenButtonAmi"), TestCategory("BVT")]
        //public void GreenButtonAmiV1_Get_Xml_Test()
        //{

        //    try
        //    {
        //        var container = new UnityContainer();

        //        RegisterTypesForUnity.RegisterTypes(container);

        //        var clientId = 87;
        //        var customerId = "GB001";
        //        var accountId = "GB001a1";
        //        var premiseId = "GB01a1p1";
        //        //var serviceId = "GB01a1p1s1";
        //        var servicePointId = "SP_GB001a1";
        //        var start15Date = Convert.ToDateTime("12/17/2015");
        //        var end15Date = Convert.ToDateTime("12/18/2015");
        //        var start30Date = Convert.ToDateTime("12/19/2015");
        //        var end30Date = Convert.ToDateTime("12/20/2015");
        //        var start60Date = Convert.ToDateTime("12/21/2015");
        //        var end60Date = Convert.ToDateTime("12/22/2015");
        //        var projected = Convert.ToDateTime("1/17/2016");
        //        var commodityId = 1;
        //        var uomId = 0;
        //        var meterId = "EM001";

        //        #region "test data"
        //        var tableRepository = new Mock<ITableRepository>();
        //        container.RegisterInstance(tableRepository.Object);
        //        var readings = new List<DynamicTableEntity>();
        //        // mock ami 15min data
        //        var fifteenreadings = TestHelpers.CreateMockAzureAmi15Data(clientId, accountId, meterId, servicePointId,
        //            start15Date, end15Date, projected, commodityId, uomId);
        //        foreach (var dynamicTableEntity in fifteenreadings)
        //        {
        //            readings.Add(dynamicTableEntity);
        //        }
        //        // mock ami 30min data
        //        var thirtyreadings = TestHelpers.CreateMockAzureAmi30Data(clientId, accountId, meterId, servicePointId,
        //            start30Date, end30Date, projected, commodityId, uomId);
        //        foreach (var dynamicTableEntity in thirtyreadings)
        //        {
        //            readings.Add(dynamicTableEntity);
        //        }

        //        // mock ami hourly data
        //        var hourreadings = TestHelpers.CreateMockAzureAmiHourlyData(clientId, accountId, meterId, servicePointId,
        //            start60Date, end60Date, projected, commodityId, uomId);
        //        foreach (var dynamicTableEntity in hourreadings)
        //        {
        //            readings.Add(dynamicTableEntity);
        //        }
        //        var filter = string.Format("(PartitionKey eq '{0}_{1}') and ((RowKey ge '{2}') and (RowKey le '{3}'))",
        //            clientId.ToString(), meterId.ToLower(), start15Date.ToString("yyyy-MM-dd"), end60Date.ToString("yyyy-MM-dd"));
        //        tableRepository.Setup(s => s.GetAllAsync<DynamicTableEntity>(Constants.TableNames.Ami, filter))
        //            .Returns(readings);
        //        #endregion

        //        var controller = new Insights.Controllers.Version1.GreenButtonAmiController(container);
        //        //var model = new billm(container);
        //        var configuration = new HttpConfiguration();
        //        var request = new HttpRequestMessage();

        //        controller.Request = request;
        //        controller.Request.Properties["MS_HttpConfiguration"] = configuration;
        //        //add property for client user, normally from the stored procedure but hardcoded here for unit test purposes
        //        var clientUser = new ClientUser
        //        {
        //            ClientID = clientId,
        //            ClientProperties = new List<ClientProperties>
        //            {
        //                new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
        //                new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
        //            }
        //        };

        //        controller.Request.Properties[CEConfiguration.CEClientUser] = clientUser;
        //        controller.Request.Properties[CEConfiguration.CELocale] = "es-ES";



        //        var gbBillRequest = new GreenButtonAmiRequest
        //        {
        //            CustomerId = customerId,
        //            AccountId = accountId,
        //            PremiseId = premiseId,
        //            MeterIds = meterId,
        //            StartDate = start15Date,
        //            EndDate = end60Date,
        //            OutputFormat = "Zip"
        //        };

        //        var result = controller.Get(gbBillRequest);

        //        Assert.IsNotNull(result);
        //        Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
        //    }
        //    catch (Exception ex)
        //    {
        //        Assert.Fail(ex.Message);
        //    }

        //}


        [TestCategory("Version1"), TestCategory("Post"), TestCategory("EnergyModelController"), TestMethod]
        [Description("Unit test to successfully post business energymodel for multiple appliances and climateid 3 and test results matching with Xergy EUI testcases")]
        public void EnergyModelv1_Post_ResponseSuccess_MultipleAppliancesClimateID3()
        {
            //Arrange
            //EnergyModelPostResponse res = null;
            var configuration = new HttpConfiguration();
            var request = new HttpRequestMessage();

            //add property for client user, normally from the stored procedure but hardcoded here for unit test purposes
            var clientUser = new ClientUser
            {
                ClientID = 87,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };
            var energymodelRequest = new EnergyModelPostRequest();
            energymodelRequest.Data = new EMRequestContainer();
            energymodelRequest.Data.ProfileAttributes = new List<EMProfileAttribute>();
            energymodelRequest.Data.ProfileAttributes.Add(new EMProfileAttribute() { Key = "premisetype", OptionKey = "premisetype.business", OptionValue = "0" });

            energymodelRequest.Data.ProfileAttributes.Add(new EMProfileAttribute() { Key = "business.totalarea", OptionKey = "business.totalarea.value", OptionValue = "13953.2944822347" });
            energymodelRequest.Data.ProfileAttributes.Add(new EMProfileAttribute() { Key = "business.cooking.fuel", OptionKey = "business.cooking.fuel.electric", OptionValue = "0" });
            energymodelRequest.Data.ProfileAttributes.Add(new EMProfileAttribute() { Key = "business.heating.fuel", OptionKey = "business.heating.fuel.electric", OptionValue = "0" });
            energymodelRequest.Data.ProfileAttributes.Add(new EMProfileAttribute() { Key = "business.waterheating.fuel", OptionKey = "business.waterheating.fuel.electric", OptionValue = "0" });
            energymodelRequest.Data.ProfileAttributes.Add(new EMProfileAttribute() { Key = "business.buildingcategory", OptionKey = "buildingcategory.school", OptionValue = "0" });
            energymodelRequest.Data.StandAloneAppliances = true;
            energymodelRequest.Data.Appliances = new List<EMAppliance>();

            energymodelRequest.Data.Appliances.Add(new EMAppliance() { Name = "businessheating" });
            energymodelRequest.Data.Appliances.Add(new EMAppliance() { Name = "businesscooling" });
            energymodelRequest.Data.Appliances.Add(new EMAppliance() { Name = "businesswaterheating" });
            energymodelRequest.Data.Appliances.Add(new EMAppliance() { Name = "businessventilation" });
            energymodelRequest.Data.Appliances.Add(new EMAppliance() { Name = "businesslighting" });
            energymodelRequest.Data.Appliances.Add(new EMAppliance() { Name = "businesscooking" });
            energymodelRequest.Data.Appliances.Add(new EMAppliance() { Name = "businessrefrigeration" });
            energymodelRequest.Data.Appliances.Add(new EMAppliance() { Name = "businessofficeequipment" });
            energymodelRequest.Data.Appliances.Add(new EMAppliance() { Name = "businesscomputers" });
            energymodelRequest.Data.Appliances.Add(new EMAppliance() { Name = "businessmisc" });

            energymodelRequest.Data.RegionParameters = new EMRegionParameters() { ApplianceRegionId = 6, TemperatureRegionId = 62, SolarRegionId = 3, State = "FL", ClimateId = 3 };

            //repo.Setup(s => s.PostProfile(clientUser.ClientID, energymodelRequest)).Returns(res);

            var controller = new Insights.Controllers.Version1.EnergyModelController { Request = request };

            controller.Request.Properties["MS_HttpConfiguration"] = configuration;
            controller.Request.Properties[CEConfiguration.CEClientUser] = clientUser;
            controller.Request.Properties[CEConfiguration.CELocale] = "en-US";

            //Act
            var result = controller.Post(energymodelRequest);
            //repo.Setup(s => s.Data).Returns(res.Data);

            //Assert
            Assert.IsNotNull(result);
            //  var resultSuccess = result.IsSuccessStatusCode;
            Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
            /*foreach (var item in res.Appliances)
            {
                Console.WriteLine(item.EnergyUses[0].Quantity);
            }*/


        }

    }
}