﻿using AO.BusinessContracts;
using AO.Registrar;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CE.Insights.Test
{
    [TestClass()]
    public class ClientSettingsTests
    {
        private UnityContainer _container;
        private IClientConfigFacade _clientConfigFacade;
        [TestInitialize]
        public void TestInit()
        {
            _container = new UnityContainer();

            (new DataStorageRegistrar()).Initialize<TransientLifetimeManager>(_container);
            _clientConfigFacade = _container.Resolve<IClientConfigFacade>();
            //// This initializes the automapper!
            //new ContentModel.AutoMapperBootStrapper().BootStrap();
        }

        [TestMethod()]
        public void GetClientSettings87Test()
        {
            var clientId = 87;
            var username = "aclaradev";
            var password = "Xaclaradev2311X";
            var emailFrom = "support@aclarax.com";
            
            var settings = _clientConfigFacade.GetClientSettings(clientId);

            Assert.IsNotNull(settings);
            Assert.AreEqual(emailFrom, settings.OptInEmailFrom);
            Assert.AreEqual(username, settings.OptInEmailUserName);
            Assert.AreEqual(password, settings.OptInEmailPassword);
        }

        [TestMethod()]
        public void GetClientSettings256Test()
        {
            var clientId = 256;
            var username = "aclaradev";
            var password = "Xaclaradev2311X";
            var emailFrom = "support@aclarax.com";
            var settings = _clientConfigFacade.GetClientSettings(clientId);

            Assert.IsNotNull(settings);
            Assert.AreEqual(emailFrom, settings.OptInEmailFrom);
            Assert.AreEqual(username, settings.OptInEmailUserName);
            Assert.AreEqual(password, settings.OptInEmailPassword);
        }


        [TestMethod()]
        public void GetClientSettings61Test()
        {
            var clientId = 61;
            var username = "aclaradev";
            var password = "Xaclaradev2311X";
            var emailFrom = "support@aclarax.com";
            var settings = _clientConfigFacade.GetClientSettings(clientId);
            Assert.IsNotNull(settings);
            Assert.AreEqual(emailFrom, settings.OptInEmailFrom);
            Assert.AreEqual(username, settings.OptInEmailUserName);
            Assert.AreEqual(password, settings.OptInEmailPassword);
        }


        [TestMethod()]
        public void GetClientSettings290Test()
        {
            var clientId = 290;
            var username = "aclaradev";
            var password = "Xaclaradev2311X";
            var emailFrom = "support@aclarax.com";
            var settings = _clientConfigFacade.GetClientSettings(clientId);

            Assert.IsNotNull(settings);
            Assert.AreEqual(emailFrom, settings.OptInEmailFrom);
            Assert.AreEqual(username, settings.OptInEmailUserName);
            Assert.AreEqual(password, settings.OptInEmailPassword);
        }

        //[TestMethod()]
        //public void GetClientSettingsQa87Test()
        //{
        //    var clientId = 87;
        //    var username = "aclaraqa";
        //    var password = "Xaclaraqa2311X";
        //    var emailFrom = "support@aclara.com";
        //    var settings = ClientSettings.GetClientSettings(clientId);

        //    Assert.IsNotNull(settings);
        //    Assert.AreEqual(emailFrom, settings.DoubleOptInEmailFrom);
        //    Assert.AreEqual(username, settings.DoubleOptInEmailUserName);
        //    Assert.AreEqual(password, settings.DoubleOptInEmailPassword);
        //}

        //[TestMethod()]
        //public void GetClientSettingsQa256Test()
        //{
        //    var clientId = 256;
        //    var username = "aclaraqa";
        //    var password = "Xaclaraqa2311X";
        //    var emailFrom = "support@aclara.com";
        //    var settings = ClientSettings.GetClientSettings(clientId);

        //    Assert.IsNotNull(settings);
        //    Assert.AreEqual(emailFrom, settings.DoubleOptInEmailFrom);
        //    Assert.AreEqual(username, settings.DoubleOptInEmailUserName);
        //    Assert.AreEqual(password, settings.DoubleOptInEmailPassword);
        //}


        //[TestMethod()]
        //public void GetClientSettingsQa61Test()
        //{
        //    var clientId = 61;
        //    var username = "aclaraqa";
        //    var password = "Xaclaraqa2311X";
        //    var emailFrom = "support@aclara.com";
        //    var settings = ClientSettings.GetClientSettings(clientId);
        //    Assert.IsNotNull(settings);
        //    Assert.AreEqual(emailFrom, settings.DoubleOptInEmailFrom);
        //    Assert.AreEqual(username, settings.DoubleOptInEmailUserName);
        //    Assert.AreEqual(password, settings.DoubleOptInEmailPassword);
        //}


        //[TestMethod()]
        //public void GetClientSettingsQa290Test()
        //{
        //    var clientId = 290;
        //    var username = "aclaraqa";
        //    var password = "Xaclaraqa2311X";
        //    var emailFrom = "support@aclara.com";
        //    var settings = ClientSettings.GetClientSettings(clientId);

        //    Assert.IsNotNull(settings);
        //    Assert.AreEqual(emailFrom, settings.DoubleOptInEmailFrom);
        //    Assert.AreEqual(username, settings.DoubleOptInEmailUserName);
        //    Assert.AreEqual(password, settings.DoubleOptInEmailPassword);
        //}

        //[TestMethod()]
        //public void GetClientSettingsUat87Test()
        //{
        //    var clientId = 87;
        //    var username = "aclarauat";
        //    var password = "Xaclarauat2311X";
        //    var emailFrom = "support@aclara.com";
        //    var settings = ClientSettings.GetClientSettings(clientId);

        //    Assert.IsNotNull(settings);
        //    Assert.AreEqual(emailFrom, settings.DoubleOptInEmailFrom);
        //    Assert.AreEqual(username, settings.DoubleOptInEmailUserName);
        //    Assert.AreEqual(password, settings.DoubleOptInEmailPassword);
        //}

        //[TestMethod()]
        //public void GetClientSettingsUat256Test()
        //{
        //    var clientId = 256;
        //    var username = "aclarauat";
        //    var password = "Xaclarauat2311X";
        //    var emailFrom = "support@aclara.com";
        //    var settings = ClientSettings.GetClientSettings(clientId);

        //    Assert.IsNotNull(settings);
        //    Assert.AreEqual(emailFrom, settings.DoubleOptInEmailFrom);
        //    Assert.AreEqual(username, settings.DoubleOptInEmailUserName);
        //    Assert.AreEqual(password, settings.DoubleOptInEmailPassword);
        //}


        //[TestMethod()]
        //public void GetClientSettingsUat61Test()
        //{
        //    var clientId = 61;
        //    var username = "ladwpuat";
        //    var password = "Xladwpuat2311X";
        //    var emailFrom = "support@ladwp.com";
        //    var settings = ClientSettings.GetClientSettings(clientId);
        //    Assert.IsNotNull(settings);
        //    Assert.AreEqual(emailFrom, settings.DoubleOptInEmailFrom);
        //    Assert.AreEqual(username, settings.DoubleOptInEmailUserName);
        //    Assert.AreEqual(password, settings.DoubleOptInEmailPassword);
        //}


        //[TestMethod()]
        //public void GetClientSettingsUat290Test()
        //{
        //    var clientId = 290;
        //    var username = "lusuat";
        //    var password = "Xlusuat2311X";
        //    var emailFrom = "support@lus.com";
        //    var settings = ClientSettings.GetClientSettings(clientId);

        //    Assert.IsNotNull(settings);
        //    Assert.AreEqual(emailFrom, settings.DoubleOptInEmailFrom);
        //    Assert.AreEqual(username, settings.DoubleOptInEmailUserName);
        //    Assert.AreEqual(password, settings.DoubleOptInEmailPassword);
        //}

        //[TestMethod()]
        //public void GetClientSettingsProd87Test()
        //{
        //    var clientId = 87;
        //    var username = "aclaraprod";
        //    var password = "Xaclaraprod2311X";
        //    var emailFrom = "support@aclara.com";
        //    var settings = ClientSettings.GetClientSettings(clientId);

        //    Assert.IsNotNull(settings);
        //    Assert.AreEqual(emailFrom, settings.DoubleOptInEmailFrom);
        //    Assert.AreEqual(username, settings.DoubleOptInEmailUserName);
        //    Assert.AreEqual(password, settings.DoubleOptInEmailPassword);
        //}

        //[TestMethod()]
        //public void GetClientSettingsProd256Test()
        //{
        //    var clientId = 256;
        //    var username = "aclaraprod";
        //    var password = "Xaclaraprod2311X";
        //    var emailFrom = "support@aclara.com";
        //    var settings = ClientSettings.GetClientSettings(clientId);

        //    Assert.IsNotNull(settings);
        //    Assert.AreEqual(emailFrom, settings.DoubleOptInEmailFrom);
        //    Assert.AreEqual(username, settings.DoubleOptInEmailUserName);
        //    Assert.AreEqual(password, settings.DoubleOptInEmailPassword);
        //}


        //[TestMethod()]
        //public void GetClientSettingsProd61Test()
        //{
        //    var clientId = 61;
        //    var username = "ladwpprod";
        //    var password = "Xladwpprod2311X";
        //    var emailFrom = "support@ladwp.com";
        //    var settings = ClientSettings.GetClientSettings(clientId);
        //    Assert.IsNotNull(settings);
        //    Assert.AreEqual(emailFrom, settings.DoubleOptInEmailFrom);
        //    Assert.AreEqual(username, settings.DoubleOptInEmailUserName);
        //    Assert.AreEqual(password, settings.DoubleOptInEmailPassword);
        //}


        //[TestMethod()]
        //public void GetClientSettingsProd290Test()
        //{
        //    var clientId = 290;
        //    var username = "lusprod";
        //    var password = "Xlusprod2311X";
        //    var emailFrom = "support@lus.com";
        //    var settings = ClientSettings.GetClientSettings(clientId);

        //    Assert.IsNotNull(settings);
        //    Assert.AreEqual(emailFrom, settings.DoubleOptInEmailFrom);
        //    Assert.AreEqual(username, settings.DoubleOptInEmailUserName);
        //    Assert.AreEqual(password, settings.DoubleOptInEmailPassword);
        //}

    }
}