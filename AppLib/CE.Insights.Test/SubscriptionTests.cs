﻿using AO.BusinessContracts;
using AO.DataAccess.Contracts;
using AO.Entities;
using CE.AO.Models;
using CE.Insights.Test.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;



// ReSharper disable once CheckNamespace
namespace AO.Business.Tests
{
    [TestClass()]
    public class SubscriptionTests
    {
        private SubscriptionFacade _subscriptionFacadeManager;
        private Mock<IClientConfigFacade> _clientConfigFacadeMock;
        private Customer  _customerManager;
        private Billing _billingManager;
        private Subscription _subscriptionManager;
        private Mock<ISendEmail> _sendMailMock;
        private Mock<INotification> _notificationMock;
        private Mock<ITrumpiaRequestDetail> _trumpiaRequestDetailMock;
        private Mock<ISendSms> _sendSmsMock;
        private Mock<LogModel> _logModelMock;

        private Mock<ICassandraRepository> _cassandraRepository;
        //private UnityContainer _container;
        [TestInitialize]
        public void TestInit()
        {
            _sendSmsMock = new Mock<ISendSms>();
            //_subscriptionMock = new Mock<ISubscription>();
            _cassandraRepository = new Mock<ICassandraRepository>();
            _clientConfigFacadeMock = new Mock<IClientConfigFacade>();
            _logModelMock = new Mock<LogModel>();
            _logModelMock.Object.DisableLog = true;
            _billingManager = new Billing(_logModelMock.Object,
                new Lazy<ICassandraRepository>(() => _cassandraRepository.Object));
            _customerManager = new Customer(_logModelMock.Object,
              new Lazy<ICassandraRepository>(() => _cassandraRepository.Object),
              new Lazy<ISendSms>(() => _sendSmsMock.Object));
            _clientConfigFacadeMock = new Mock<IClientConfigFacade>();
            _sendMailMock = new Mock<ISendEmail>();
            _notificationMock = new Mock<INotification>();
            _trumpiaRequestDetailMock = new Mock<ITrumpiaRequestDetail>();
            _subscriptionManager = new Subscription(_logModelMock.Object, _cassandraRepository.Object,
                new Lazy<IClientConfigFacade>(() => _clientConfigFacadeMock.Object));

            _subscriptionFacadeManager = new SubscriptionFacade(_logModelMock.Object,
                new Lazy<IBilling>(() => _billingManager), new Lazy<ISubscription>(() => _subscriptionManager),
                new Lazy<ICustomer>(() => _customerManager), new Lazy<INotification>(() => _notificationMock.Object),
                new Lazy<IClientConfigFacade>(() => _clientConfigFacadeMock.Object),
                new Lazy<ISendEmail>(() => _sendMailMock.Object),
                new Lazy<ITrumpiaRequestDetail>(() => _trumpiaRequestDetailMock.Object));

            AutoMapperConfig.AutoMapperMappings();
            //_container = new UnityContainer();

            //(new DataStorageRegistrar()).Initialize<TransientLifetimeManager>(_container);


            //Mapper.CreateMap<SubscriptionEntity, SubscriptionModel>()
            //    .ForAllUnmappedMembers(o => o.Ignore());
            //Mapper.CreateMap<SubscriptionModel, SubscriptionEntity>()
            //    .ForAllUnmappedMembers(o => o.Ignore());

            //Mapper.CreateMap<BillingEntity, BillingModel>();

            //Mapper.CreateMap<CustomerEntity, CustomerModel>()
            //    .ForAllUnmappedMembers(o => o.Ignore());
        }

        [TestMethod()]
        public void GetSettingsWithoutSubscribedProgramsTest()
        {

            //_container.RegisterInstance(_cassandraRepository.Object);
            //_container.RegisterInstance(_clientConfigFacadeMock.Object);

            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId = "EM001";
            var rateClass = "RG-1";
            var servicePointId = "SP_BTD001a1";
            var serviceContractId = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock billing entities data
            var mockBillingEntity = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId,
                servicePointId, meterId, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock client settings
            var mockClientSettings = TestHelpers.CreateMockClientSettings();
            _clientConfigFacadeMock.Setup(c => c.GetClientSettings(It.IsAny<int>())).Returns(mockClientSettings);
            // mock empty subscription
            IList<SubscriptionEntity> subList = new List<SubscriptionEntity>();
            _cassandraRepository.Setup(t => t.GetAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>())).Returns(Task.FromResult(subList));

            var result = _subscriptionFacadeManager.GetDefaultListWithSubscribedPrograms(clientId, customerId, accountId);

            Assert.AreEqual(11, result.Count);

            foreach (var insight in result)
            {
                Assert.AreEqual(false, insight.IsSelected);

            }

        }

        [TestMethod()]
        public void GetSettingsWithSubscribedProgramsTest()
        {
            //_container.RegisterInstance(_cassandraRepository.Object);

            #region "test data"
            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId = "EM001";
            var rateClass = "RG-1";
            var servicePointId = "SP_BTD001a1";
            var serviceContractId = "BTD001a1_BTD001a1p1_1_EM001";
            var program = "Program1";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock client settings
            var mockClientSettings = TestHelpers.CreateMockClientSettings();
            _clientConfigFacadeMock.Setup(c => c.GetClientSettings(It.IsAny<int>())).Returns(mockClientSettings);
            // mock billing entities data
            var mockBillingEntity = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId,
                servicePointId, meterId, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight1, false, 0);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight7, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            insightList.Add(mockInsight8);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));
            #endregion

            var result = _subscriptionFacadeManager.GetDefaultListWithSubscribedPrograms(clientId, customerId, accountId);

            Assert.AreEqual(11, result.Count);
            foreach (var insight in result)
            {
                if (insight.InsightTypeName == insight2 && insight.ProgramName == program)
                {
                    Assert.AreEqual(insight2, insight.InsightTypeName);
                    Assert.AreEqual(true, insight.IsSelected);
                }
                else
                {
                    Assert.AreEqual(false, insight.IsSelected);
                }
            }
        }

        [TestMethod()]
        public void GetSettingsWithSubscribedProgramsDoubleOptInCompletedTest()
        {
            //_container.RegisterInstance(_tableRepository.Object);

            #region "test data"
            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId = "EM001";
            var rateClass = "RG-1";
            var servicePointId = "SP_BTD001a1";
            var serviceContractId = "BTD001a1_BTD001a1p1_1_EM001";
            var program = "Program1";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock client settings
            var mockClientSettings = TestHelpers.CreateMockClientSettings();
            _clientConfigFacadeMock.Setup(c => c.GetClientSettings(It.IsAny<int>())).Returns(mockClientSettings);
            // mock billing entities data
            var mockBillingEntity = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId,
                servicePointId, meterId, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight1, false, 0);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight7, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            insightList.Add(mockInsight8);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));
            #endregion
            
            var result = _subscriptionFacadeManager.GetDefaultListWithSubscribedPrograms(clientId, customerId, accountId);

            Assert.AreEqual(11, result.Count);
            foreach (var insight in result)
            {
                Assert.AreEqual(true, insight.IsSmsOptInCompleted);
                Assert.AreEqual(true, insight.IsEmailOptInCompleted);
                if (insight.InsightTypeName == insight2 && insight.ProgramName == program)
                {
                    Assert.AreEqual(insight2, insight.InsightTypeName);
                    Assert.AreEqual(true, insight.IsSelected);
                }
                else
                {
                    Assert.AreEqual(false, insight.IsSelected);
                }
            }
        }

        [TestMethod()]
        public void GetSettingsWithSubscribedPrograms2Test()
        {
            //_container.RegisterInstance(_tableRepository.Object);

            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId = "EM001";
            var rateClass = "RG-1";
            var servicePointId = "SP_BTD001a1";
            var serviceContractId = "BTD001a1_BTD001a1p1_1_EM001";
            var program = "Program1";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            double threshold = 90;
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock client settings
            var mockClientSettings = TestHelpers.CreateMockClientSettings();
            _clientConfigFacadeMock.Setup(c => c.GetClientSettings(It.IsAny<int>())).Returns(mockClientSettings);
            // mock billing entities data
            var mockBillingEntity = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId,
                servicePointId, meterId, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight1, false, 0);
            var mockInsight2 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight2, true, threshold);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight7, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            insightList.Add(mockInsight2);
            insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            insightList.Add(mockInsight8);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));
            
            var result = _subscriptionFacadeManager.GetDefaultListWithSubscribedPrograms(clientId, customerId, accountId);

            Assert.AreEqual(11, result.Count);
            foreach (var insight in result)
            {
                if (insight.InsightTypeName == insight2 && insight.ProgramName == program)
                {
                    Assert.AreEqual(insight2, insight.InsightTypeName);
                    Assert.AreEqual(threshold, insight.SubscriberThresholdValue);
                    Assert.AreEqual(true, insight.IsSelected);
                }
                else
                {
                    Assert.AreEqual(false, insight.IsSelected);
                }
            }
        }

        [TestMethod()]
        public void GetSettingsWithSubscribedPrograms2ServiceTest()
        {
            //_container.RegisterInstance(_tableRepository.Object);

            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var meterId2 = "EM002";
            var servicePointId2 = "SP_BTD002a1";
            var serviceContractId2 = "BTD002a1_BTD002a1p1_1_EM002";
            var program = "Program1";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            double threshold = 90;
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock client settings
            var mockClientSettings = TestHelpers.CreateMockClientSettings();
            _clientConfigFacadeMock.Setup(c => c.GetClientSettings(It.IsAny<int>())).Returns(mockClientSettings);
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var mockBillingEntity2 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId2,
                servicePointId2, meterId2, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            services.Add(mockBillingEntity2); _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                 .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity
            // service 1
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            var mockInsight2 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight2, true, threshold);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);
            // service 2
            var mockInsight12 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight1, false, 0);
            var mockInsight13 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight2, false, 0);
            var mockInsight14 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight15 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight4, false, 0);
            var mockInsight16 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight17 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight6, false, 0);
            var mockInsight18 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight7, false, 0);
            var mockInsight19 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            var mockInsight20 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight9, false, 0);
            var mockInsight21 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight10, false, 0);
            var mockInsight22 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            insightList.Add(mockInsight2);
            insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            insightList.Add(mockInsight8);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            insightList.Add(mockInsight12);
            insightList.Add(mockInsight13);
            insightList.Add(mockInsight14);
            insightList.Add(mockInsight15);
            insightList.Add(mockInsight16);
            insightList.Add(mockInsight17);
            insightList.Add(mockInsight18);
            insightList.Add(mockInsight19);
            insightList.Add(mockInsight20);
            insightList.Add(mockInsight21);
            insightList.Add(mockInsight22);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));

            var result = _subscriptionFacadeManager.GetDefaultListWithSubscribedPrograms(clientId, customerId, accountId);

            Assert.AreEqual(19, result.Count);
            foreach (var insight in result)
            {
                if (insight.InsightTypeName == insight2 && insight.ProgramName == program && (insight.ServiceContractId == serviceContractId1))
                {
                    Assert.AreEqual(insight2, insight.InsightTypeName);
                    Assert.AreEqual(threshold, insight.SubscriberThresholdValue);
                    Assert.AreEqual(true, insight.IsSelected);
                }
                else
                {
                    Assert.AreEqual(false, insight.IsSelected);
                }
            }
        }

        [TestMethod()]
        public void GetSettingsWithSubscribedAccountLevelPrograms2ServiceTest()
        {
            //_container.RegisterInstance(_tableRepository.Object);

            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var meterId2 = "EM002";
            var servicePointId2 = "SP_BTD002a1";
            var serviceContractId2 = "BTD002a1_BTD002a1p1_1_EM002";
            var program = "Program1";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            double threshold = 90;
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock client settings
            var mockClientSettings = TestHelpers.CreateMockClientSettings();
            _clientConfigFacadeMock.Setup(c => c.GetClientSettings(It.IsAny<int>())).Returns(mockClientSettings);
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var mockBillingEntity2 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId2,
                servicePointId2, meterId2, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            services.Add(mockBillingEntity2);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            var mockInsight2 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight2, false, 0);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, true, threshold);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);
            // service 2
            var mockInsight12 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight1, false, 0);
            var mockInsight13 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight2, false, 0);
            var mockInsight15 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight4, false, 0);
            var mockInsight17 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight6, false, 0);
            var mockInsight18 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight7, false, 0);
            var mockInsight20 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight9, false, 0);
            var mockInsight21 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight10, false, 0);
            var mockInsight22 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            insightList.Add(mockInsight2);
            insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            insightList.Add(mockInsight8);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            insightList.Add(mockInsight12);
            insightList.Add(mockInsight13);
            insightList.Add(mockInsight15);
            insightList.Add(mockInsight17);
            insightList.Add(mockInsight18);
            insightList.Add(mockInsight20);
            insightList.Add(mockInsight21);
            insightList.Add(mockInsight22);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));

            var result = _subscriptionFacadeManager.GetDefaultListWithSubscribedPrograms(clientId, customerId, accountId);

            Assert.AreEqual(19, result.Count);
            foreach (var insight in result)
            {
                if (insight.InsightTypeName == insight3 && insight.ProgramName == program && insight.AccountId == accountId)
                {
                    Assert.AreEqual(insight3, insight.InsightTypeName);
                    Assert.AreEqual(threshold, insight.SubscriberThresholdValue);
                    Assert.AreEqual(true, insight.IsSelected);
                }
                else
                {
                    Assert.AreEqual(false, insight.IsSelected);
                }
            }
        }

        //// Post unit tests
        [TestMethod()]
        public void GetSubscribeList_ExistingSubscription_3EServices_Test()
        {
            //_container.RegisterInstance(_tableRepository.Object);

            var program = "Program1";
            //var programInsight = "Program 2.ServiceLevelCostThreshold";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            //var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            //var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            //var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";

            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var meterId2 = "EM002";
            var meterId3 = "EM003";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1p1s1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1s1_1_EM001";
            var servicePointId2 = "SP_BTD001a1p1s2";
            var serviceContractId2 = "BTD001a1_BTD001a1p1s2_1_EM002";
            var servicePointId3 = "SP_BTD001a1p1s3";
            var serviceContractId3 = "BTD001a1_BTD001a1p1s3_1_EM003";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock client settings
            var mockClientSettings = TestHelpers.CreateMockClientSettings();
            _clientConfigFacadeMock.Setup(c => c.GetClientSettings(It.IsAny<int>())).Returns(mockClientSettings);
            // mock billing entities data - 3 electric services
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var mockBillingEntity2 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId2,
                servicePointId2, meterId2, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var mockBillingEntity3 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId3,
                servicePointId3, meterId3, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            services.Add(mockBillingEntity2);
            services.Add(mockBillingEntity3);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity - only meter 1 insight 2 is subscribed previously
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);
            // service 2
            var mockInsight21 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight1, false, 0);
            var mockInsight22 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight2, false, 0);
            var mockInsight24 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight4, false, 0);
            var mockInsight26 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight6, false, 0);
            var mockInsight27 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight7, false, 0);
            var mockInsight29 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight9, false, 0);
            var mockInsight210 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight10, false, 0);
            var mockInsight211 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight11, false, 0);
            // service 3
            var mockInsight31 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId3, program, insight1, false, 0);
            var mockInsight32 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId3, program, insight2, false, 0);
            var mockInsight34 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId3, program, insight4, false, 0);
            var mockInsight36 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId3, program, insight6, false, 0);
            var mockInsight37 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId3, program, insight7, false, 0);
            var mockInsight39 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId3, program, insight9, false, 0);
            var mockInsight310 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId3, program, insight10, false, 0);
            var mockInsight311 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId3, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            insightList.Add(mockInsight4);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            insightList.Add(mockInsight21);
            insightList.Add(mockInsight22);
            insightList.Add(mockInsight24);
            insightList.Add(mockInsight26);
            insightList.Add(mockInsight27);
            insightList.Add(mockInsight29);
            insightList.Add(mockInsight210);
            insightList.Add(mockInsight211);
            insightList.Add(mockInsight31);
            insightList.Add(mockInsight32);
            insightList.Add(mockInsight34);
            insightList.Add(mockInsight36);
            insightList.Add(mockInsight37);
            insightList.Add(mockInsight39);
            insightList.Add(mockInsight310);
            insightList.Add(mockInsight311);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));

            var subscribeList = new List<SubscriptionModel>();
            var subscribeInsight1 = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program,
                InsightTypeName = insight1,
                PremiseId = premiseId,
                ServiceContractId = serviceContractId1,
                ServicePointId = servicePointId1,
                CustomerId = customerId,
                ClientId = clientId,
                Channel = "Email"
            };
            var subscribeInsight2 = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program,
                InsightTypeName = insight1,
                PremiseId = premiseId,
                ServiceContractId = serviceContractId2,
                ServicePointId = servicePointId2,
                CustomerId = customerId,
                ClientId = clientId,
                Channel = "Email"
            };
            var subscribeInsight3 = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program,
                InsightTypeName = insight4,
                PremiseId = premiseId,
                ServiceContractId = serviceContractId2,
                ServicePointId = servicePointId2,
                CustomerId = customerId,
                ClientId = clientId,
                Channel = "Email"
            };
            // subscrib - insight 1 for E1 and E2, insight 4 for E2
            subscribeList.Add(subscribeInsight1);
            subscribeList.Add(subscribeInsight2);
            subscribeList.Add(subscribeInsight3);

            var result = _subscriptionFacadeManager.GetSubscribeList(clientId, customerId, accountId, subscribeList);


            Assert.IsNotNull(result);
            Assert.AreEqual(24, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(true, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);
            foreach (var insight in insights)
            {
                if (insight.InsightTypeName.ToLower() == insight1.ToLower() && (insight.ServiceContractId == serviceContractId1 || insight.ServiceContractId == serviceContractId2))
                {
                    Assert.AreEqual(true, insight.IsSelected);
                }
                else if (insight.InsightTypeName.ToLower() == insight4.ToLower() && insight.ServiceContractId == serviceContractId2)
                {
                    Assert.AreEqual(true, insight.IsSelected);
                }
                else
                    Assert.AreEqual(false, insight.IsSelected);

                Assert.AreEqual(accountId, insight.AccountId);
                Assert.IsNotNull(insight.ServiceContractId);
                Assert.IsNotNull(insight.ServicePointId);
                Assert.IsNotNull(insight.PremiseId);
                Assert.AreEqual(customerId, insight.CustomerId);
                Assert.IsNotNull(insight.Channel);
                Assert.AreNotEqual(string.Empty, insight.Channel);
            }



        }

        [TestMethod()]
        public void GetSubscribeList_ExistingSubscription_3EServices_subscribeandunscribe_Test()
        {

            var program = "Program1";
            //var programInsight = "Program 2.ServiceLevelCostThreshold";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            //var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            //var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            //var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";

            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var meterId2 = "EM002";
            var meterId3 = "EM003";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1p1s1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1s1_1_EM001";
            var servicePointId2 = "SP_BTD001a1p1s2";
            var serviceContractId2 = "BTD001a1_BTD001a1p1s2_1_EM002";
            var servicePointId3 = "SP_BTD001a1p1s3";
            var serviceContractId3 = "BTD001a1_BTD001a1p1s3_1_EM003";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock client settings
            var mockClientSettings = TestHelpers.CreateMockClientSettings();
            _clientConfigFacadeMock.Setup(c => c.GetClientSettings(It.IsAny<int>())).Returns(mockClientSettings);
            // mock billing entities data - 3 electric services
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var mockBillingEntity2 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId2,
                servicePointId2, meterId2, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var mockBillingEntity3 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId3,
                servicePointId3, meterId3, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            services.Add(mockBillingEntity2);
            services.Add(mockBillingEntity3);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity - only meter 1 insight 2 is subscribed previously
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            //var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            //var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            //var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);
            // service 2
            var mockInsight21 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight1, false, 0);
            var mockInsight22 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight2, false, 0);
            var mockInsight24 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight4, false, 0);
            var mockInsight26 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight6, false, 0);
            var mockInsight27 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight7, false, 0);
            var mockInsight29 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight9, false, 0);
            var mockInsight210 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight10, false, 0);
            var mockInsight211 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight11, false, 0);
            // service 3
            var mockInsight31 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId3, program, insight1, false, 0);
            var mockInsight32 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId3, program, insight2, false, 0);
            var mockInsight34 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId3, program, insight4, false, 0);
            var mockInsight36 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId3, program, insight6, false, 0);
            var mockInsight37 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId3, program, insight7, false, 0);
            var mockInsight39 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId3, program, insight9, false, 0);
            var mockInsight310 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId3, program, insight10, false, 0);
            var mockInsight311 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId3, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            //insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            //insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            //insightList.Add(mockInsight8);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            insightList.Add(mockInsight21);
            insightList.Add(mockInsight22);
            insightList.Add(mockInsight24);
            insightList.Add(mockInsight26);
            insightList.Add(mockInsight27);
            insightList.Add(mockInsight29);
            insightList.Add(mockInsight210);
            insightList.Add(mockInsight211);
            insightList.Add(mockInsight31);
            insightList.Add(mockInsight32);
            insightList.Add(mockInsight34);
            insightList.Add(mockInsight36);
            insightList.Add(mockInsight37);
            insightList.Add(mockInsight39);
            insightList.Add(mockInsight310);
            insightList.Add(mockInsight311);
            _cassandraRepository.SetupSequence(
               t => t.GetAsync(It.IsAny<string>(),
               It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
               .Returns(Task.FromResult(programList))
               .Returns(Task.FromResult(insightList));

            var subscribeList = new List<SubscriptionModel>();
            var subscribeInsight1 = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program,
                InsightTypeName = insight1,
                PremiseId = premiseId,
                ServiceContractId = serviceContractId1,
                ServicePointId = servicePointId1,
                CustomerId = customerId,
                ClientId = clientId,
                Channel = "Email"
            };
            var subscribeInsight2 = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program,
                InsightTypeName = insight1,
                PremiseId = premiseId,
                ServiceContractId = serviceContractId2,
                ServicePointId = servicePointId2,
                CustomerId = customerId,
                ClientId = clientId,
                Channel = "Email"
            };
            var subscribeInsight3 = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program,
                InsightTypeName = insight4,
                PremiseId = premiseId,
                ServiceContractId = serviceContractId2,
                ServicePointId = servicePointId2,
                CustomerId = customerId,
                ClientId = clientId,
                Channel = "Email"
            };

            var subscribeInsight4 = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = false,
                ProgramName = program,
                InsightTypeName = insight2,
                PremiseId = premiseId,
                ServiceContractId = serviceContractId1,
                ServicePointId = servicePointId1,
                CustomerId = customerId,
                ClientId = clientId
            };
            // subscrib - insight 1 for E1 and E2, insight 4 for E2
            subscribeList.Add(subscribeInsight1);
            subscribeList.Add(subscribeInsight2);
            subscribeList.Add(subscribeInsight3);
            subscribeList.Add(subscribeInsight4);

            var result = _subscriptionFacadeManager.GetSubscribeList(clientId, customerId, accountId, subscribeList);


            Assert.IsNotNull(result);
            Assert.AreEqual(25, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(true, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);
            foreach (var insight in insights)
            {
                if (insight.InsightTypeName.ToLower() == insight1.ToLower() && (insight.ServiceContractId == serviceContractId1 || insight.ServiceContractId == serviceContractId2))
                {
                    Assert.AreEqual(true, insight.IsSelected);
                }
                else if (insight.InsightTypeName.ToLower() == insight4.ToLower() && insight.ServiceContractId == serviceContractId2)
                {
                    Assert.AreEqual(true, insight.IsSelected);
                }
                else if (insight.InsightTypeName.ToLower() == insight2.ToLower() && insight.ServiceContractId == serviceContractId1)
                {
                    Assert.AreEqual(false, insight.IsSelected);
                }
                else
                    Assert.AreEqual(false, insight.IsSelected);

                Assert.AreEqual(accountId, insight.AccountId);
                Assert.IsNotNull(insight.ServiceContractId);
                Assert.IsNotNull(insight.ServicePointId);
                Assert.IsNotNull(insight.PremiseId);
                Assert.AreEqual(customerId, insight.CustomerId);
                Assert.IsNotNull(insight.Channel);
                Assert.AreNotEqual(string.Empty, insight.Channel);
            }



        }

        [TestMethod()]
        public void GetSubscribeList_ExistingSubscription_3EServices_subscribeandunscribewholeprogram_Test()
        {

            var program = "Program1";
            //var programInsight = "Program 2.ServiceLevelCostThreshold";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            //var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            //var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            //var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";

            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var meterId2 = "EM002";
            var meterId3 = "EM003";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1p1s1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1s1_1_EM001";
            var servicePointId2 = "SP_BTD001a1p1s2";
            var serviceContractId2 = "BTD001a1_BTD001a1p1s2_1_EM002";
            var servicePointId3 = "SP_BTD001a1p1s3";
            var serviceContractId3 = "BTD001a1_BTD001a1p1s3_1_EM003";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock client settings
            var mockClientSettings = TestHelpers.CreateMockClientSettings();
            _clientConfigFacadeMock.Setup(c => c.GetClientSettings(It.IsAny<int>())).Returns(mockClientSettings);
            // mock billing entities data - 3 electric services
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var mockBillingEntity2 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId2,
                servicePointId2, meterId2, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var mockBillingEntity3 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId3,
                servicePointId3, meterId3, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            services.Add(mockBillingEntity2);
            services.Add(mockBillingEntity3);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity - only meter 1 insight 2 is subscribed previously
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            //var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            //var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            //var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);
            // service 2
            var mockInsight21 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight1, false, 0);
            var mockInsight22 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight2, false, 0);
            var mockInsight24 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight4, false, 0);
            var mockInsight26 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight6, false, 0);
            var mockInsight27 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight7, false, 0);
            var mockInsight29 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight9, false, 0);
            var mockInsight210 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight10, false, 0);
            var mockInsight211 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight11, false, 0);
            // service 3
            var mockInsight31 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId3, program, insight1, false, 0);
            var mockInsight32 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId3, program, insight2, false, 0);
            var mockInsight34 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId3, program, insight4, false, 0);
            var mockInsight36 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId3, program, insight6, false, 0);
            var mockInsight37 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId3, program, insight7, false, 0);
            var mockInsight39 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId3, program, insight9, false, 0);
            var mockInsight310 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId3, program, insight10, false, 0);
            var mockInsight311 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId3, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            //insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            //insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            //insightList.Add(mockInsight8);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            insightList.Add(mockInsight21);
            insightList.Add(mockInsight22);
            insightList.Add(mockInsight24);
            insightList.Add(mockInsight26);
            insightList.Add(mockInsight27);
            insightList.Add(mockInsight29);
            insightList.Add(mockInsight210);
            insightList.Add(mockInsight211);
            insightList.Add(mockInsight31);
            insightList.Add(mockInsight32);
            insightList.Add(mockInsight34);
            insightList.Add(mockInsight36);
            insightList.Add(mockInsight37);
            insightList.Add(mockInsight39);
            insightList.Add(mockInsight310);
            insightList.Add(mockInsight311);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));


            var subscribeList = new List<SubscriptionModel>();
            //var subscribeInsight1 = new SubscriptionModel
            //{
            //    AccountId = accountId,
            //    IsSelected = true,
            //    ProgramName = program,
            //    InsightTypeName = insight1,
            //    PremiseId = premiseId,
            //    ServiceContractId = serviceContractId1,
            //    ServicePointId = servicePointId1,
            //    CustomerId = customerId,
            //    ClientId = clientId
            //};
            //var subscribeInsight2 = new SubscriptionModel
            //{
            //    AccountId = accountId,
            //    IsSelected = true,
            //    ProgramName = program,
            //    InsightTypeName = insight1,
            //    PremiseId = premiseId,
            //    ServiceContractId = serviceContractId2,
            //    ServicePointId = servicePointId2,
            //    CustomerId = customerId,
            //    ClientId = clientId
            //};
            //var subscribeInsight3 = new SubscriptionModel
            //{
            //    AccountId = accountId,
            //    IsSelected = true,
            //    ProgramName = program,
            //    InsightTypeName = insight4,
            //    PremiseId = premiseId,
            //    ServiceContractId = serviceContractId2,
            //    ServicePointId = servicePointId2,
            //    CustomerId = customerId,
            //    ClientId = clientId
            //};

            var subscribeInsight4 = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = false,
                ProgramName = program,
                InsightTypeName = insight2,
                PremiseId = premiseId,
                ServiceContractId = serviceContractId1,
                ServicePointId = servicePointId1,
                CustomerId = customerId,
                ClientId = clientId
            };

            //subscribeList.Add(subscribeInsight1);
            //subscribeList.Add(subscribeInsight2);
            //subscribeList.Add(subscribeInsight3);
            subscribeList.Add(subscribeInsight4);

            var result = _subscriptionFacadeManager.GetSubscribeList(clientId, customerId, accountId, subscribeList);


            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            foreach (var insight in insights)
            {
                if (string.IsNullOrEmpty(insight.InsightTypeName))
                {
                    Assert.AreEqual(false, insight.IsSelected);
                }
                else
                    Assert.AreEqual(false, insight.IsSelected);

                Assert.AreEqual(accountId, insight.AccountId);
                Assert.IsNotNull(insight.ServiceContractId);
                Assert.IsNotNull(insight.ServicePointId);
                Assert.IsNotNull(insight.PremiseId);
                Assert.AreEqual(customerId, insight.CustomerId);
                Assert.IsNotNull(insight.Channel);
                Assert.AreNotEqual(string.Empty, insight.Channel);
            }



        }

        [TestMethod()]
        public void GetSubscribeList_ExistingSubscription_3EServices_subscribeandunscribewholeprogram2_Test()
        {
            var program = "Program1";
            //var program1 = "Program 1";
            //var programInsight = "Program 2.ServiceLevelCostThreshold";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";

            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var meterId2 = "EM002";
            var meterId3 = "EM003";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1p1s1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1s1_1_EM001";
            var servicePointId2 = "SP_BTD001a1p1s2";
            var serviceContractId2 = "BTD001a1_BTD001a1p1s2_1_EM002";
            var servicePointId3 = "SP_BTD001a1p1s3";
            var serviceContractId3 = "BTD001a1_BTD001a1p1s3_1_EM003";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock client settings
            var mockClientSettings = TestHelpers.CreateMockClientSettings();
            _clientConfigFacadeMock.Setup(c => c.GetClientSettings(It.IsAny<int>())).Returns(mockClientSettings);
            // mock billing entities data - 3 electric services
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var mockBillingEntity2 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId2,
                servicePointId2, meterId2, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var mockBillingEntity3 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId3,
                servicePointId3, meterId3, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            services.Add(mockBillingEntity2);
            services.Add(mockBillingEntity3);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity - only meter 1 insight 2 is subscribed previously
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            //var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);
            // serivce 2
            var mockInsight21 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight1, false, 0);
            var mockInsight22 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight2, false, 0);
            var mockInsight24 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight4, false, 0);
            var mockInsight26 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight6, false, 0);
            var mockInsight27 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight7, false, 0);
            var mockInsight29 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight9, false, 0);
            var mockInsight210 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight10, false, 0);
            var mockInsight211 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight11, false, 0);
            // service 3
            var mockInsight31 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId3, program, insight1, false, 0);
            var mockInsight32 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId3, program, insight2, false, 0);
            var mockInsight34 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId3, program, insight4, false, 0);
            var mockInsight36 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId3, program, insight6, false, 0);
            var mockInsight37 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId3, program, insight7, false, 0);
            var mockInsight39 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId3, program, insight9, false, 0);
            var mockInsight310 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId3, program, insight10, false, 0);
            var mockInsight311 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId3, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight3);
            insightList.Add(mockInsight5);
            insightList.Add(mockInsight1);
            insightList.Add(mockInsight4);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            //insightList.Add(mockInsight8);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            insightList.Add(mockInsight21);
            insightList.Add(mockInsight22);
            insightList.Add(mockInsight24);
            insightList.Add(mockInsight26);
            insightList.Add(mockInsight27);
            insightList.Add(mockInsight29);
            insightList.Add(mockInsight210);
            insightList.Add(mockInsight211);
            insightList.Add(mockInsight31);
            insightList.Add(mockInsight32);
            insightList.Add(mockInsight34);
            insightList.Add(mockInsight36);
            insightList.Add(mockInsight37);
            insightList.Add(mockInsight39);
            insightList.Add(mockInsight310);
            insightList.Add(mockInsight311);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));

            var subscribeList = new List<SubscriptionModel>();
            var subscribeInsight1 = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program,
                InsightTypeName = insight1,
                PremiseId = premiseId,
                ServiceContractId = serviceContractId1,
                ServicePointId = servicePointId1,
                CustomerId = customerId,
                ClientId = clientId,
                Channel = "Email"
            };
            var subscribeInsight2 = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program,
                InsightTypeName = insight1,
                PremiseId = premiseId,
                ServiceContractId = serviceContractId2,
                ServicePointId = servicePointId2,
                CustomerId = customerId,
                ClientId = clientId,
                Channel = "Email"
            };
            var subscribeInsight3 = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program,
                InsightTypeName = insight4,
                PremiseId = premiseId,
                ServiceContractId = serviceContractId2,
                ServicePointId = servicePointId2,
                CustomerId = customerId,
                ClientId = clientId,
                Channel = "Email"
            };

            var subscribeInsight4 = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = false,
                ProgramName = program,
                InsightTypeName = insight8,
                CustomerId = customerId,
                ClientId = clientId
            };

            subscribeList.Add(subscribeInsight1);
            subscribeList.Add(subscribeInsight2);
            subscribeList.Add(subscribeInsight3);
            subscribeList.Add(subscribeInsight4);

            var result = _subscriptionFacadeManager.GetSubscribeList(clientId, customerId, accountId, subscribeList);


            Assert.IsNotNull(result);
            Assert.AreEqual(27, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            //Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);
            //Assert.AreEqual(false, subscribedProgram[0].IsSelected);
            foreach (var insight in insights)
            {
                if (insight.ProgramName == program)
                {
                    if (insight.InsightTypeName.ToLower() == insight1.ToLower() && (insight.ServiceContractId == serviceContractId1 || insight.ServiceContractId == serviceContractId2))
                    {
                        Assert.AreEqual(true, insight.IsSelected);
                    }
                    else if (insight.InsightTypeName.ToLower() == insight4.ToLower() && insight.ServiceContractId == serviceContractId2)
                    {
                        Assert.AreEqual(true, insight.IsSelected);
                    }
                    else
                        Assert.AreEqual(false, insight.IsSelected);
                }

                Assert.AreEqual(accountId, insight.AccountId);
                Assert.IsNotNull(insight.ServiceContractId);
                Assert.IsNotNull(insight.ServicePointId);
                Assert.IsNotNull(insight.PremiseId);
                Assert.AreEqual(customerId, insight.CustomerId);
                Assert.IsNotNull(insight.Channel);
                Assert.AreNotEqual(string.Empty, insight.Channel);
            }



        }

        [TestMethod()]
        public void GetSubscribeList_ExistingSubscription_2EServices_Test()
        {

            var program = "Program1";
            //var programInsight = "Program 2.ServiceLevelCostThreshold";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            //var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            //var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            //var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";

            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var meterId2 = "EM002";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1p1s1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1s1_1_EM001";
            var servicePointId2 = "SP_BTD001a1p1s2";
            var serviceContractId2 = "BTD001a1_BTD001a1p1s2_1_EM002";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock client settings
            var mockClientSettings = TestHelpers.CreateMockClientSettings();
            _clientConfigFacadeMock.Setup(c => c.GetClientSettings(It.IsAny<int>())).Returns(mockClientSettings);
            // mock billing entities data - 3 electric services
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var mockBillingEntity2 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId2,
                servicePointId2, meterId2, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            services.Add(mockBillingEntity2);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity - only meter 1 insight 2 is subscribed previously
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            //var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            //var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            //var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);
            // serivce 2
            var mockInsight21 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight1, false, 0);
            var mockInsight22 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight2, false, 0);
            var mockInsight24 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight4, false, 0);
            var mockInsight26 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight6, false, 0);
            var mockInsight27 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight7, false, 0);
            var mockInsight29 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight9, false, 0);
            var mockInsight210 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight10, false, 0);
            var mockInsight211 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            //insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            //insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            //insightList.Add(mockInsight8);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            insightList.Add(mockInsight21);
            insightList.Add(mockInsight22);
            insightList.Add(mockInsight24);
            insightList.Add(mockInsight26);
            insightList.Add(mockInsight27);
            insightList.Add(mockInsight29);
            insightList.Add(mockInsight210);
            insightList.Add(mockInsight211);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));

            var subscribeList = new List<SubscriptionModel>();
            var subscribeInsight1 = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program,
                InsightTypeName = insight1,
                PremiseId = premiseId,
                ServiceContractId = serviceContractId1,
                ServicePointId = servicePointId1,
                CustomerId = customerId,
                ClientId = clientId,
                Channel = "Email"
            };
            var subscribeInsight2 = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program,
                InsightTypeName = insight1,
                PremiseId = premiseId,
                ServiceContractId = serviceContractId2,
                ServicePointId = servicePointId2,
                CustomerId = customerId,
                ClientId = clientId,
                Channel = "Email"
            };
            var subscribeInsight3 = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program,
                InsightTypeName = insight4,
                PremiseId = premiseId,
                ServiceContractId = serviceContractId2,
                ServicePointId = servicePointId2,
                CustomerId = customerId,
                ClientId = clientId,
                Channel = "Email"
            };
            // subscrib - insight 1 for E1 and E2, insight 4 for E2
            subscribeList.Add(subscribeInsight1);
            subscribeList.Add(subscribeInsight2);
            subscribeList.Add(subscribeInsight3);

            var result = _subscriptionFacadeManager.GetSubscribeList(clientId, customerId, accountId, subscribeList);


            Assert.IsNotNull(result);
            Assert.AreEqual(16, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(true, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);
            foreach (var insight in insights)
            {
                if (insight.InsightTypeName.ToLower() == insight1.ToLower() && (insight.ServiceContractId == serviceContractId1 || insight.ServiceContractId == serviceContractId2))
                {
                    Assert.AreEqual(true, insight.IsSelected);
                }
                else if (insight.InsightTypeName.ToLower() == insight4.ToLower() && insight.ServiceContractId == serviceContractId2)
                {
                    Assert.AreEqual(true, insight.IsSelected);
                }
                else
                    Assert.AreEqual(false, insight.IsSelected);

                Assert.AreEqual(accountId, insight.AccountId);
                Assert.IsNotNull(insight.ServiceContractId);
                Assert.IsNotNull(insight.ServicePointId);
                Assert.IsNotNull(insight.PremiseId);
                Assert.AreEqual(customerId, insight.CustomerId);
                Assert.IsNotNull(insight.Channel);
                Assert.AreNotEqual(string.Empty, insight.Channel);
            }



        }


        [TestMethod()]
        public void GetSubscribeList_ExistingSubscription_2EServices_subscribeandunscribe_Test()
        {

            var program = "Program1";
            //var programInsight = "Program 2.ServiceLevelCostThreshold";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            //var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            //var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            //var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";

            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var meterId2 = "EM002";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1p1s1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1s1_1_EM001";
            var servicePointId2 = "SP_BTD001a1p1s2";
            var serviceContractId2 = "BTD001a1_BTD001a1p1s2_1_EM002";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock client settings
            var mockClientSettings = TestHelpers.CreateMockClientSettings();
            _clientConfigFacadeMock.Setup(c => c.GetClientSettings(It.IsAny<int>())).Returns(mockClientSettings);
            // mock billing entities data - 3 electric services
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var mockBillingEntity2 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId2,
                servicePointId2, meterId2, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            services.Add(mockBillingEntity2);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity - only meter 1 insight 2 is subscribed previously
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            //var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            //var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            //var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);
            // serivce 2
            var mockInsight21 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight1, false, 0);
            var mockInsight22 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight2, false, 0);
            var mockInsight24 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight4, false, 0);
            var mockInsight26 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight6, false, 0);
            var mockInsight27 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight7, false, 0);
            var mockInsight29 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight9, false, 0);
            var mockInsight210 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight10, false, 0);
            var mockInsight211 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId2, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            //insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            //insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            //insightList.Add(mockInsight8);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            insightList.Add(mockInsight21);
            insightList.Add(mockInsight22);
            insightList.Add(mockInsight24);
            insightList.Add(mockInsight26);
            insightList.Add(mockInsight27);
            insightList.Add(mockInsight29);
            insightList.Add(mockInsight210);
            insightList.Add(mockInsight211);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));

            var subscribeList = new List<SubscriptionModel>();
            var subscribeInsight1 = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program,
                InsightTypeName = insight1,
                PremiseId = premiseId,
                ServiceContractId = serviceContractId1,
                ServicePointId = servicePointId1,
                CustomerId = customerId,
                ClientId = clientId,
                Channel = "Email",
            };
            var subscribeInsight2 = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program,
                InsightTypeName = insight1,
                PremiseId = premiseId,
                ServiceContractId = serviceContractId2,
                ServicePointId = servicePointId2,
                CustomerId = customerId,
                ClientId = clientId,
                Channel = "Email"
            };
            var subscribeInsight3 = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program,
                InsightTypeName = insight4,
                PremiseId = premiseId,
                ServiceContractId = serviceContractId2,
                ServicePointId = servicePointId2,
                CustomerId = customerId,
                ClientId = clientId,
                Channel = "Email"
            };

            var subscribeInsight4 = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = false,
                ProgramName = program,
                InsightTypeName = insight2,
                PremiseId = premiseId,
                ServiceContractId = serviceContractId1,
                ServicePointId = servicePointId1,
                CustomerId = customerId,
                ClientId = clientId
            };
            // subscrib - insight 1 for E1 and E2, insight 4 for E2
            subscribeList.Add(subscribeInsight1);
            subscribeList.Add(subscribeInsight2);
            subscribeList.Add(subscribeInsight3);
            subscribeList.Add(subscribeInsight4);

            var result = _subscriptionFacadeManager.GetSubscribeList(clientId, customerId, accountId, subscribeList);


            Assert.IsNotNull(result);
            Assert.AreEqual(17, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(true, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);
            foreach (var insight in insights)
            {
                if (insight.InsightTypeName.ToLower() == insight1.ToLower() && (insight.ServiceContractId == serviceContractId1 || insight.ServiceContractId == serviceContractId2))
                {
                    Assert.AreEqual(true, insight.IsSelected);
                }
                else if (insight.InsightTypeName.ToLower() == insight4.ToLower() && insight.ServiceContractId == serviceContractId2)
                {
                    Assert.AreEqual(true, insight.IsSelected);
                }
                else if (insight.InsightTypeName.ToLower() == insight2.ToLower() && insight.ServiceContractId == serviceContractId1)
                {
                    Assert.AreEqual(false, insight.IsSelected);
                }
                else
                    Assert.AreEqual(false, insight.IsSelected);

                Assert.AreEqual(accountId, insight.AccountId);
                Assert.IsNotNull(insight.ServiceContractId);
                Assert.IsNotNull(insight.ServicePointId);
                Assert.IsNotNull(insight.PremiseId);
                Assert.AreEqual(customerId, insight.CustomerId);
                Assert.IsNotNull(insight.Channel);
                Assert.AreNotEqual(string.Empty, insight.Channel);
            }



        }

        [TestMethod()]
        public void GetSubscribeList_NoExistingSubscription_Service_Test()
        {

            var program = "Program1";
            //var programInsight = "Program 2.ServiceLevelUsageThreshold";
            //var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            //var insight3 = "BillToDate";
            //var insight4 = "CostToDate";
            //var insight5 = "AccountProjectedCost";
            //var insight6 = "ServiceProjectedCost";
            //var insight7 = "Usage";
            //var insight8 = "AccountLevelCostThreshold";
            //var insight9 = "DayThreshold";
            //var insight10 = "ServiceLevelTieredThresholdApproaching";
            //var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";

            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock client settings
            var mockClientSettings = TestHelpers.CreateMockClientSettings();
            _clientConfigFacadeMock.Setup(c => c.GetClientSettings(It.IsAny<int>())).Returns(mockClientSettings);
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock empty subscription
            IList<SubscriptionEntity> subList = new List<SubscriptionEntity>();
            _cassandraRepository.Setup(t => t.GetAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>())).Returns(Task.FromResult(subList));

            var subscribeList = new List<SubscriptionModel>();
            var subscribeInsight = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program,
                InsightTypeName = insight2,
                PremiseId = premiseId,
                ServiceContractId = serviceContractId1,
                ServicePointId = servicePointId1,
                CustomerId = customerId,
                ClientId = clientId,
                Channel = "Email"
            };
            subscribeList.Add(subscribeInsight);

            var result = _subscriptionFacadeManager.GetSubscribeList(clientId, customerId, accountId, subscribeList);

            Assert.IsNotNull(result);
            Assert.AreEqual(11, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(true, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);
            foreach (var insight in insights)
            {
                Assert.AreEqual(false, insight.IsSelected);
                Assert.AreNotEqual(insight2.ToLower(), insight.InsightTypeName.ToLower());
                Assert.AreEqual(accountId, insight.AccountId);
                Assert.IsNotNull(insight.ServiceContractId);
                Assert.IsNotNull(insight.ServicePointId);
                Assert.IsNotNull(insight.PremiseId);
                Assert.AreEqual(customerId, insight.CustomerId);
                Assert.IsNotNull(insight.Channel);
                Assert.AreNotEqual(string.Empty, insight.Channel);
            }
        }

        [TestMethod()]
        public void GetSubscribeList_NoExistingSubscription_Account_Test()
        {
            var program = "Program1";
            //var programInsight = "Program 1.BillToDate";
            //var insight1 = "ServiceLevelCostThreshold";
            //var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            //var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            //var insight6 = "ServiceProjectedCost";
            //var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            //var insight9 = "DayThreshold";
            //var insight10 = "ServiceLevelTieredThresholdApproaching";
            //var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";

            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock client settings
            var mockClientSettings = TestHelpers.CreateMockClientSettings();
            _clientConfigFacadeMock.Setup(c => c.GetClientSettings(It.IsAny<int>())).Returns(mockClientSettings);
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
            servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock empty subscription
            IList<SubscriptionEntity> subList = new List<SubscriptionEntity>();
            _cassandraRepository.Setup(t => t.GetAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>())).Returns(Task.FromResult(subList));


            var subscribeList = new List<SubscriptionModel>();
            var subscribeInsight = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program,
                InsightTypeName = insight3,
                CustomerId = customerId,
                ClientId = clientId,
                Channel = "Email"
            };
            subscribeList.Add(subscribeInsight);

            var result = _subscriptionFacadeManager.GetSubscribeList(clientId, customerId, accountId, subscribeList);


            Assert.IsNotNull(result);
            Assert.AreEqual(11, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(true, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);
            foreach (var insight in insights)
            {
                Assert.AreEqual(false, insight.IsSelected);
                Assert.AreNotEqual(insight3.ToLower(), insight.InsightTypeName.ToLower());
                Assert.AreEqual(accountId, insight.AccountId);
                if (insight.InsightTypeName == insight3 || insight.InsightTypeName == insight5 ||
                    insight.InsightTypeName == insight8)
                {
                    Assert.AreEqual(string.Empty, insight.ServiceContractId);
                    Assert.AreEqual(string.Empty, insight.ServicePointId);
                    Assert.AreEqual(string.Empty, insight.PremiseId);
                }
                Assert.AreEqual(customerId, insight.CustomerId);
                Assert.IsNotNull(insight.Channel);
                Assert.AreNotEqual(string.Empty, insight.Channel);
            }

        }

        [TestMethod()]
        public void GetSubscribeList_NoExistingSubscription_Mixed_Test()
        {
            var program1 = "Program1";
            //var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            //var insight4 = "CostToDate";
            //var insight5 = "AccountProjectedCost";
            //var insight6 = "ServiceProjectedCost";
            //var insight7 = "Usage";
            //var insight8 = "AccountLevelCostThreshold";
            //var insight9 = "DayThreshold";
            //var insight10 = "ServiceLevelTieredThresholdApproaching";
            //var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";

            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock client settings
            var mockClientSettings = TestHelpers.CreateMockClientSettings();
            _clientConfigFacadeMock.Setup(c => c.GetClientSettings(It.IsAny<int>())).Returns(mockClientSettings);
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock empty subscription
            IList<SubscriptionEntity> subList = new List<SubscriptionEntity>();
            _cassandraRepository.Setup(t => t.GetAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>())).Returns(Task.FromResult(subList));


            var subscribeList = new List<SubscriptionModel>();
            var subscribeInsight = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program1,
                InsightTypeName = insight2,
                PremiseId = premiseId,
                ServiceContractId = serviceContractId1,
                ServicePointId = servicePointId1,
                CustomerId = customerId,
                ClientId = clientId,
                Channel = "Email"
            };
            subscribeList.Add(subscribeInsight);
            subscribeInsight = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program1,
                InsightTypeName = insight3,
                PremiseId = string.Empty,
                ServiceContractId = string.Empty,
                ServicePointId = string.Empty,
                CustomerId = customerId,
                ClientId = clientId,
                Channel = "Email"
            };
            subscribeList.Add(subscribeInsight);

            var result = _subscriptionFacadeManager.GetSubscribeList(clientId, customerId, accountId, subscribeList);


            Assert.IsNotNull(result);
            Assert.AreEqual(10, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(1, subscribedProgram.Count);
            foreach (var program in subscribedProgram)
            {
                Assert.AreEqual(true, program.IsSelected);
                Assert.AreEqual(program1.ToLower(), program.ProgramName.ToLower());
                Assert.AreEqual(accountId, program.AccountId);
                Assert.AreEqual(customerId, program.CustomerId);
                Assert.IsNull(program.Channel);
            }
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            foreach (var insight in insights)
            {
                Assert.AreEqual(false, insight.IsSelected);
                Assert.AreNotEqual(insight2.ToLower(), insight.InsightTypeName.ToLower());
                Assert.AreNotEqual(insight3.ToLower(), insight.InsightTypeName.ToLower());
                Assert.AreEqual(accountId, insight.AccountId);
                Assert.IsNotNull(insight.ServiceContractId);
                Assert.IsNotNull(insight.ServicePointId);
                Assert.IsNotNull(insight.PremiseId);
                Assert.AreEqual(customerId, insight.CustomerId);
                Assert.IsNotNull(insight.Channel);
                Assert.AreNotEqual(string.Empty, insight.Channel);
            }


        }

        [TestMethod()]
        public void GetSubscribeList_ExistingSubscription_Service_Test()
        {

            var program = "Program1";
            //var programInsight = "Program 2.ServiceLevelCostThreshold";
            var insight1 = "ServiceLevelCostThreshold";
            //var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";

            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock client settings
            var mockClientSettings = TestHelpers.CreateMockClientSettings();
            _clientConfigFacadeMock.Setup(c => c.GetClientSettings(It.IsAny<int>())).Returns(mockClientSettings);
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId,
                serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId,
                serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId,
                serviceContractId1, program, insight1, false, 0);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId,
                string.Empty, program, insight3, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId,
                serviceContractId1, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId,
                string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId,
                serviceContractId1, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId,
                serviceContractId1, program, insight7, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId,
                string.Empty, program, insight8, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId,
                serviceContractId1, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId,
                serviceContractId1, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId,
                serviceContractId1, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            insightList.Add(mockInsight8);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));


            var subscribeList = new List<SubscriptionModel>();
            var subscribeInsight = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program,
                InsightTypeName = insight1,
                PremiseId = premiseId,
                ServiceContractId = serviceContractId1,
                ServicePointId = servicePointId1,
                CustomerId = customerId,
                ClientId = clientId,
                Channel = "Email"
            };
            subscribeList.Add(subscribeInsight);

            var result = _subscriptionFacadeManager.GetSubscribeList(clientId, customerId, accountId, subscribeList);


            Assert.IsNotNull(result);
            Assert.AreEqual(11, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(true, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);
            foreach (var insight in insights)
            {
                if (insight.InsightTypeName.ToLower() == insight1.ToLower())
                {
                    Assert.AreEqual(true, insight.IsSelected);
                }
                else
                    Assert.AreEqual(false, insight.IsSelected);

                Assert.AreEqual(accountId, insight.AccountId);
                Assert.IsNotNull(insight.ServiceContractId);
                Assert.IsNotNull(insight.ServicePointId);
                Assert.IsNotNull(insight.PremiseId);
                Assert.AreEqual(customerId, insight.CustomerId);
                Assert.IsNotNull(insight.Channel);
                Assert.AreNotEqual(string.Empty, insight.Channel);
            }


        }


        [TestMethod()]
        public void GetSubscribeList_ExistingSubscription_Service2_Test()
        {

            var program = "Program1";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";

            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock client settings
            var mockClientSettings = TestHelpers.CreateMockClientSettings();
            _clientConfigFacadeMock.Setup(c => c.GetClientSettings(It.IsAny<int>())).Returns(mockClientSettings);
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            insightList.Add(mockInsight8);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));

            var subscribeList = new List<SubscriptionModel>();
            var subscribeInsight = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program,
                InsightTypeName = insight1,
                PremiseId = premiseId,
                ServiceContractId = serviceContractId1,
                ServicePointId = servicePointId1,
                CustomerId = customerId,
                ClientId = clientId,
                Channel = "Email"
            };
            subscribeList.Add(subscribeInsight);
            subscribeInsight = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program,
                InsightTypeName = insight2,
                PremiseId = premiseId,
                ServiceContractId = serviceContractId1,
                ServicePointId = servicePointId1,
                CustomerId = customerId,
                ClientId = clientId,
                Channel = "Email"
            };
            subscribeList.Add(subscribeInsight);

            var result = _subscriptionFacadeManager.GetSubscribeList(clientId, customerId, accountId, subscribeList);

            Assert.IsNotNull(result);
            Assert.AreEqual(11, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(true, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);
            foreach (var insight in insights)
            {
                if (insight.InsightTypeName.ToLower() == insight1.ToLower())
                {
                    Assert.AreEqual(true, insight.IsSelected);
                }
                else
                    Assert.AreEqual(false, insight.IsSelected);

                Assert.AreEqual(accountId, insight.AccountId);
                Assert.IsNotNull(insight.ServiceContractId);
                Assert.IsNotNull(insight.ServicePointId);
                Assert.IsNotNull(insight.PremiseId);
                Assert.AreEqual(customerId, insight.CustomerId);
                Assert.IsNotNull(insight.Channel);
                Assert.AreNotEqual(string.Empty, insight.Channel);
            }
        }

        [TestMethod()]
        public void GetSubscribeList_ExistingSubscription_Service2_unsubscribe_Test()
        {

            var program = "Program1";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";

            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock client settings
            var mockClientSettings = TestHelpers.CreateMockClientSettings();
            _clientConfigFacadeMock.Setup(c => c.GetClientSettings(It.IsAny<int>())).Returns(mockClientSettings);
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                 .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            insightList.Add(mockInsight8);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));

            var subscribeList = new List<SubscriptionModel>();
            var subscribeInsight = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program,
                InsightTypeName = insight1,
                PremiseId = premiseId,
                ServiceContractId = serviceContractId1,
                ServicePointId = servicePointId1,
                CustomerId = customerId,
                ClientId = clientId,
                Channel = "Email"
            };
            subscribeList.Add(subscribeInsight);
            subscribeInsight = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = false,
                ProgramName = program,
                InsightTypeName = insight2,
                PremiseId = premiseId,
                ServiceContractId = serviceContractId1,
                ServicePointId = servicePointId1,
                CustomerId = customerId,
                ClientId = clientId
            };
            subscribeList.Add(subscribeInsight);

            var result = _subscriptionFacadeManager.GetSubscribeList(clientId, customerId, accountId, subscribeList);

            Assert.IsNotNull(result);
            Assert.AreEqual(12, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(true, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);
            foreach (var insight in insights)
            {
                if (insight.InsightTypeName.ToLower() == insight1.ToLower())
                {
                    Assert.AreEqual(true, insight.IsSelected);
                }
                else
                    Assert.AreEqual(false, insight.IsSelected);

                Assert.AreEqual(accountId, insight.AccountId);
                Assert.IsNotNull(insight.ServiceContractId);
                Assert.IsNotNull(insight.ServicePointId);
                Assert.IsNotNull(insight.PremiseId);
                Assert.AreEqual(customerId, insight.CustomerId);
                Assert.IsNotNull(insight.Channel);
                Assert.AreNotEqual(string.Empty, insight.Channel);
            }
        }


        [TestMethod()]
        public void GetSubscribeList_ExistingSubscription_Account_Test()
        {
            var program = "Program1";
            //var programInsight = "Program 1.AccountProjectedCost";
            //var insight1 = "ServiceLevelCostThreshold";
            //var insight2 = "ServiceLevelUsageThreshold";
            //var insight3 = "BillToDate";
            //var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            //var insight6 = "ServiceProjectedCost";
            //var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            //var insight9 = "DayThreshold";
            //var insight10 = "ServiceLevelTieredThresholdApproaching";
            //var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";

            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock client settings
            var mockClientSettings = TestHelpers.CreateMockClientSettings();
            _clientConfigFacadeMock.Setup(c => c.GetClientSettings(It.IsAny<int>())).Returns(mockClientSettings);
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity
            //var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            //var mockInsight2 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight2, false, 0);
            //var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            //var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            //var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            //var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            //var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            //var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            //insightList.Add(mockInsight1);
            //insightList.Add(mockInsight2);
            //insightList.Add(mockInsight4);
            insightList.Add(mockInsight5);
            //insightList.Add(mockInsight6);
            //insightList.Add(mockInsight7);
            insightList.Add(mockInsight8);
            //insightList.Add(mockInsight9);
            //insightList.Add(mockInsight10);
            //insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));

            var subscribeList = new List<SubscriptionModel>();
            var subscribeInsight = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program,
                InsightTypeName = insight5,
                PremiseId = string.Empty,
                ServiceContractId = string.Empty,
                ServicePointId = string.Empty,
                CustomerId = customerId,
                ClientId = clientId,
                Channel = "Email"
            };
            subscribeList.Add(subscribeInsight);

            var result = _subscriptionFacadeManager.GetSubscribeList(clientId, customerId, accountId, subscribeList);


            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(true, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);
            foreach (var insight in insights)
            {
                if (insight.InsightTypeName.ToLower() == insight5.ToLower())
                {
                    Assert.AreEqual(true, insight.IsSelected);
                    Assert.AreEqual(string.Empty, insight.ServiceContractId);
                    Assert.AreEqual(string.Empty, insight.ServicePointId);
                    Assert.AreEqual(string.Empty, insight.PremiseId);
                }
                else
                    Assert.AreEqual(false, insight.IsSelected);

                Assert.AreEqual(accountId, insight.AccountId);
                Assert.AreEqual(customerId, insight.CustomerId);
                Assert.IsNotNull(insight.Channel);
                Assert.AreNotEqual(string.Empty, insight.Channel);
            }
        }

        [TestMethod()]
        public void GetSubscribeList_ExistingSubscription_Account_unscribe_Test()
        {
            var program = "Program1";
            //var programInsight = "Program 1.AccountProjectedCost";
            //var insight1 = "ServiceLevelCostThreshold";
            //var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            //var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            //var insight6 = "ServiceProjectedCost";
            //var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            //var insight9 = "DayThreshold";
            //var insight10 = "ServiceLevelTieredThresholdApproaching";
            //var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";

            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock client settings
            var mockClientSettings = TestHelpers.CreateMockClientSettings();
            _clientConfigFacadeMock.Setup(c => c.GetClientSettings(It.IsAny<int>())).Returns(mockClientSettings);
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity
            //var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            //var mockInsight2 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight2, false, 0);
            //var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            //var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            //var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            //var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            //var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            //var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            //insightList.Add(mockInsight1);
            //insightList.Add(mockInsight2);
            //insightList.Add(mockInsight4);
            insightList.Add(mockInsight5);
            //insightList.Add(mockInsight6);
            //insightList.Add(mockInsight7);
            insightList.Add(mockInsight8);
            //insightList.Add(mockInsight9);
            //insightList.Add(mockInsight10);
            //insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));

            var subscribeList = new List<SubscriptionModel>();
            var subscribeInsight = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program,
                InsightTypeName = insight5,
                PremiseId = string.Empty,
                ServiceContractId = string.Empty,
                ServicePointId = string.Empty,
                CustomerId = customerId,
                ClientId = clientId,
                Channel = "Email"
            };
            var subscribeInsight2 = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = false,
                ProgramName = program,
                InsightTypeName = insight3,
                PremiseId = string.Empty,
                ServiceContractId = string.Empty,
                ServicePointId = string.Empty,
                CustomerId = customerId,
                ClientId = clientId
            };
            subscribeList.Add(subscribeInsight);
            subscribeList.Add(subscribeInsight2);

            var result = _subscriptionFacadeManager.GetSubscribeList(clientId, customerId, accountId, subscribeList);


            Assert.IsNotNull(result);
            Assert.AreEqual(4, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(true, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);
            foreach (var insight in insights)
            {
                if (insight.InsightTypeName.ToLower() == insight5.ToLower())
                {
                    Assert.AreEqual(true, insight.IsSelected);
                    Assert.AreEqual(string.Empty, insight.ServiceContractId);
                    Assert.AreEqual(string.Empty, insight.ServicePointId);
                    Assert.AreEqual(string.Empty, insight.PremiseId);
                }
                else
                    Assert.AreEqual(false, insight.IsSelected);

                Assert.AreEqual(accountId, insight.AccountId);
                Assert.AreEqual(customerId, insight.CustomerId);
                Assert.IsNotNull(insight.Channel);
                Assert.AreNotEqual(string.Empty, insight.Channel);
            }
        }

        [TestMethod()]
        public void GetSubscribeList_ExistingSubscription_Account2_Test()
        {
            var program = "Program1";
            //var programInsight1 = "Program 1.BillToDate";
            //var programInsight2 = "Program 1.AccountProjectedCost";
            //var insight1 = "ServiceLevelCostThreshold";
            //var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            //var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            //var insight6 = "ServiceProjectedCost";
            //var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            //var insight9 = "DayThreshold";
            //var insight10 = "ServiceLevelTieredThresholdApproaching";
            //var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";

            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock client settings
            var mockClientSettings = TestHelpers.CreateMockClientSettings();
            _clientConfigFacadeMock.Setup(c => c.GetClientSettings(It.IsAny<int>())).Returns(mockClientSettings);
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity
            //var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            //var mockInsight2 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight2, false, 0);
            //var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            //var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            //var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            //var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            //var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            //var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            //insightList.Add(mockInsight1);
            //insightList.Add(mockInsight2);
            //insightList.Add(mockInsight4);
            insightList.Add(mockInsight5);
            //insightList.Add(mockInsight6);
            //insightList.Add(mockInsight7);
            insightList.Add(mockInsight8);
            //insightList.Add(mockInsight9);
            //insightList.Add(mockInsight10);
            //insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));

            var subscribeList = new List<SubscriptionModel>();
            var subscribeInsight = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program,
                InsightTypeName = insight3,
                PremiseId = string.Empty,
                ServiceContractId = string.Empty,
                ServicePointId = string.Empty,
                ClientId = clientId,
                CustomerId = customerId,
                Channel = "Email"
            };
            subscribeList.Add(subscribeInsight);
            subscribeInsight = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program,
                InsightTypeName = insight5,
                PremiseId = string.Empty,
                ServiceContractId = string.Empty,
                ServicePointId = string.Empty,
                ClientId = clientId,
                CustomerId = customerId,
                Channel = "Email"
            };
            subscribeList.Add(subscribeInsight);

            var result = _subscriptionFacadeManager.GetSubscribeList(clientId, customerId, accountId, subscribeList);

            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(true, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);
            foreach (var insight in insights)
            {
                if (insight.InsightTypeName.ToLower() == insight5.ToLower())
                {
                    Assert.AreEqual(true, insight.IsSelected);
                    Assert.AreEqual(string.Empty, insight.ServiceContractId);
                    Assert.AreEqual(string.Empty, insight.ServicePointId);
                    Assert.AreEqual(string.Empty, insight.PremiseId);
                }
                else
                    Assert.AreEqual(false, insight.IsSelected);
                Assert.AreEqual(accountId, insight.AccountId);
                Assert.AreEqual(customerId, insight.CustomerId);
                Assert.IsNotNull(insight.Channel);
                Assert.AreNotEqual(string.Empty, insight.Channel);
            }

        }


        [TestMethod()]
        public void GetSubscribeList_ExistingSubscription_Mixed_Test()
        {
            var program1 = "Program1";
            //var program2 = "Program2";
            //var programInsight1 = "Program 3.ServiceLevelCostThreshold";
            //var programInsight2 = "Program 1.BillToDate";
            var insight1 = "ServiceLevelCostThreshold";
            //var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            //var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";

            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock client settings
            var mockClientSettings = TestHelpers.CreateMockClientSettings();
            _clientConfigFacadeMock.Setup(c => c.GetClientSettings(It.IsAny<int>())).Returns(mockClientSettings);
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription1);
            // mock subscription insight entity
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program1, insight3, false, 0);
            //var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program1, insight5, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program1, insight8, false, 0);
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, insight1, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, insight4, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, insight7, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            //insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            insightList.Add(mockInsight8);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));

            var subscribeList = new List<SubscriptionModel>();
            var subscribeInsight = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program1,
                InsightTypeName = insight1,
                PremiseId = premiseId,
                ServiceContractId = serviceContractId1,
                ServicePointId = servicePointId1,
                ClientId = clientId,
                CustomerId = customerId,
                Channel = "Email"
            };
            subscribeList.Add(subscribeInsight);
            subscribeInsight = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program1,
                InsightTypeName = insight3,
                PremiseId = string.Empty,
                ServiceContractId = string.Empty,
                ServicePointId = string.Empty,
                ClientId = clientId,
                CustomerId = customerId,
                Channel = "Email"
            };
            subscribeList.Add(subscribeInsight);
            var result = _subscriptionFacadeManager.GetSubscribeList(clientId, customerId, accountId, subscribeList);

            Assert.IsNotNull(result);
            Assert.AreEqual(10, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(1, subscribedProgram.Count);
            foreach (var program in subscribedProgram)
            {
                Assert.AreEqual(true, program.IsSelected);
                Assert.AreEqual(accountId, program.AccountId);
                Assert.AreEqual(customerId, program.CustomerId);
                Assert.IsNull(program.Channel);
            }
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            foreach (var insight in insights)
            {
                if (insight.InsightTypeName.ToLower() == insight3.ToLower())
                {
                    Assert.AreEqual(true, insight.IsSelected);
                    Assert.AreEqual(string.Empty, insight.ServiceContractId);
                    Assert.AreEqual(string.Empty, insight.ServicePointId);
                    Assert.AreEqual(string.Empty, insight.PremiseId);
                }
                else if (insight.InsightTypeName.ToLower() == insight1.ToLower())
                {
                    Assert.AreEqual(true, insight.IsSelected);
                    Assert.IsNotNull(insight.ServiceContractId);
                    Assert.IsNotNull(insight.ServicePointId);
                    Assert.IsNotNull(insight.PremiseId);
                }
                else
                    Assert.AreEqual(false, insight.IsSelected);

                Assert.AreEqual(accountId, insight.AccountId);
                if (!string.IsNullOrEmpty(insight.ServiceContractId))
                {
                    Assert.IsNotNull(insight.ServiceContractId);
                    Assert.IsNotNull(insight.ServicePointId);
                    Assert.IsNotNull(insight.PremiseId);
                }
                Assert.AreEqual(customerId, insight.CustomerId);
                Assert.IsNotNull(insight.Channel);
                Assert.AreNotEqual(string.Empty, insight.Channel);
            }

        }

        [TestMethod()]
        public void GetSubscribeList_ExistingSubscription_Mixed3_Test()
        {
            var program1 = "Program1";
            var insight1 = "ServiceLevelCostThreshold";
            //var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            //var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            //var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";

            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock client settings
            var mockClientSettings = TestHelpers.CreateMockClientSettings();
            _clientConfigFacadeMock.Setup(c => c.GetClientSettings(It.IsAny<int>())).Returns(mockClientSettings);
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            //var mockProgramSubscription1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, string.Empty, true, 0);
            var mockProgramSubscription2 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            //programList.Add(mockProgramSubscription1);
            programList.Add(mockProgramSubscription2);
            // mock subscription insight entity
            //var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program1, insight3, false, 0);
            ////var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program1, insight5, false, 0);
            //var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program1, insight8, false, 0);
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, insight1, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, insight4, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, insight7, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            //insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            //insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            //insightList.Add(mockInsight8);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));

            var subscribeList = new List<SubscriptionModel>();
            var subscribeInsight = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program1,
                InsightTypeName = insight1,
                PremiseId = premiseId,
                ServiceContractId = serviceContractId1,
                ServicePointId = servicePointId1,
                ClientId = clientId,
                CustomerId = customerId,
                Channel = "Email"
            };
            subscribeList.Add(subscribeInsight);
            subscribeInsight = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = false,
                ProgramName = program1,
                InsightTypeName = insight3,
                PremiseId = string.Empty,
                ServiceContractId = string.Empty,
                ServicePointId = string.Empty,
                ClientId = clientId,
                CustomerId = customerId
            };
            subscribeList.Add(subscribeInsight);
            var result = _subscriptionFacadeManager.GetSubscribeList(clientId, customerId, accountId, subscribeList);

            Assert.IsNotNull(result);
            Assert.AreEqual(9, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(1, subscribedProgram.Count);
            foreach (var program in subscribedProgram)
            {
                Assert.AreEqual(true, program.IsSelected);
                Assert.AreEqual(accountId, program.AccountId);
                Assert.AreEqual(customerId, program.CustomerId);
                Assert.IsNull(program.Channel);
            }
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            foreach (var insight in insights)
            {
                if (insight.InsightTypeName.ToLower() == insight3.ToLower())
                {
                    Assert.AreEqual(false, insight.IsSelected);
                    Assert.AreEqual(string.Empty, insight.ServiceContractId);
                    Assert.AreEqual(string.Empty, insight.ServicePointId);
                    Assert.AreEqual(string.Empty, insight.PremiseId);
                }
                else if (insight.InsightTypeName.ToLower() == insight1.ToLower())
                {
                    Assert.AreEqual(true, insight.IsSelected);
                    Assert.IsNotNull(insight.ServiceContractId);
                    Assert.IsNotNull(insight.ServicePointId);
                    Assert.IsNotNull(insight.PremiseId);
                }
                else
                    Assert.AreEqual(false, insight.IsSelected);

                Assert.AreEqual(accountId, insight.AccountId);
                if (!string.IsNullOrEmpty(insight.ServiceContractId))
                {
                    Assert.IsNotNull(insight.ServiceContractId);
                    Assert.IsNotNull(insight.ServicePointId);
                    Assert.IsNotNull(insight.PremiseId);
                }
                Assert.AreEqual(customerId, insight.CustomerId);
                Assert.IsNotNull(insight.Channel);
                Assert.AreNotEqual(string.Empty, insight.Channel);
            }

        }

        [TestMethod()]
        public void GetSubscribeList_ExistingSubscription_Mixed4_Test()
        {
            var program1 = "Program1";
            //var programInsight1 = "Program 2.ServiceLevelCostThreshold";
            //var programInsight2 = "Program 1.BillToDate";
            var insight1 = "ServiceLevelCostThreshold";
            //var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            //var insight4 = "CostToDate";
            //var insight5 = "AccountProjectedCost";
            //var insight6 = "ServiceProjectedCost";
            //var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            //var insight9 = "DayThreshold";
            //var insight10 = "ServiceLevelTieredThresholdApproaching";
            //var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";

            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock client settings
            var mockClientSettings = TestHelpers.CreateMockClientSettings();
            _clientConfigFacadeMock.Setup(c => c.GetClientSettings(It.IsAny<int>())).Returns(mockClientSettings);
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, string.Empty, true, 0);
            //var mockProgramSubscription2 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program2, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription1);
            //programList.Add(mockProgramSubscription2);
            // mock subscription insight entity
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program1, insight3, false, 0);
            //var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program1, insight5, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program1, insight8, false, 0);
            //var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program2, insight1, false, 0);
            //var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program2, insight4, false, 0);
            //var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program2, insight6, false, 0);
            //var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program2, insight7, false, 0);
            //var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program2, insight9, false, 0);
            //var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program2, insight10, false, 0);
            //var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program2, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            //insightList.Add(mockInsight1);
            insightList.Add(mockInsight3);
            //insightList.Add(mockInsight4);
            //insightList.Add(mockInsight5);
            //insightList.Add(mockInsight6);
            //insightList.Add(mockInsight7);
            insightList.Add(mockInsight8);
            //insightList.Add(mockInsight9);
            //insightList.Add(mockInsight10);
            //insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));

            var subscribeList = new List<SubscriptionModel>();
            var subscribeInsight = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program1,
                InsightTypeName = insight1,
                PremiseId = premiseId,
                ServiceContractId = serviceContractId1,
                ServicePointId = servicePointId1,
                ClientId = clientId,
                CustomerId = customerId,
                Channel = "Email"
            };
            subscribeList.Add(subscribeInsight);
            subscribeInsight = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program1,
                InsightTypeName = insight3,
                PremiseId = string.Empty,
                ServiceContractId = string.Empty,
                ServicePointId = string.Empty,
                ClientId = clientId,
                CustomerId = customerId,
                Channel = "Email"
            };
            subscribeList.Add(subscribeInsight);
            var result = _subscriptionFacadeManager.GetSubscribeList(clientId, customerId, accountId, subscribeList);

            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(1, subscribedProgram.Count);
            foreach (var program in subscribedProgram)
            {
                Assert.AreEqual(true, program.IsSelected);
                Assert.AreEqual(accountId, program.AccountId);
                Assert.AreEqual(customerId, program.CustomerId);
                Assert.IsNull(program.Channel);
            }
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            foreach (var insight in insights)
            {
                if (insight.InsightTypeName.ToLower() == insight3.ToLower())
                {
                    Assert.AreEqual(true, insight.IsSelected);
                    Assert.AreEqual(string.Empty, insight.ServiceContractId);
                    Assert.AreEqual(string.Empty, insight.ServicePointId);
                    Assert.AreEqual(string.Empty, insight.PremiseId);
                }
                else if (insight.InsightTypeName.ToLower() == insight1.ToLower())
                {
                    Assert.AreEqual(true, insight.IsSelected);
                    Assert.IsNotNull(insight.ServiceContractId);
                    Assert.IsNotNull(insight.ServicePointId);
                    Assert.IsNotNull(insight.PremiseId);
                }
                else
                    Assert.AreEqual(false, insight.IsSelected);

                Assert.AreEqual(accountId, insight.AccountId);
                if (!string.IsNullOrEmpty(insight.ServiceContractId))
                {
                    Assert.IsNotNull(insight.ServiceContractId);
                    Assert.IsNotNull(insight.ServicePointId);
                    Assert.IsNotNull(insight.PremiseId);
                }
                Assert.AreEqual(customerId, insight.CustomerId);
                Assert.IsNotNull(insight.Channel);
                Assert.AreNotEqual(string.Empty, insight.Channel);
            }

        }

        [TestMethod()]
        public void GetSubscribeList_ExistingSubscription_Mixed2_Test()
        {
            var program1 = "Program1";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            //var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            //var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";

            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock client settings
            var mockClientSettings = TestHelpers.CreateMockClientSettings();
            _clientConfigFacadeMock.Setup(c => c.GetClientSettings(It.IsAny<int>())).Returns(mockClientSettings);
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, insight1, false, 0);
            //var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, insight4, false, 0);
            //var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, insight7, false, 0);
            //var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            //insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            //insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            //insightList.Add(mockInsight8);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));

            var subscribeList = new List<SubscriptionModel>();
            var subscribeInsight = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program1,
                InsightTypeName = insight1,
                PremiseId = premiseId,
                ServiceContractId = serviceContractId1,
                ServicePointId = servicePointId1,
                ClientId = clientId,
                CustomerId = customerId,
                Channel = "Email"
            };
            subscribeList.Add(subscribeInsight);
            subscribeInsight = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program1,
                InsightTypeName = insight2,
                PremiseId = premiseId,
                ServiceContractId = serviceContractId1,
                ServicePointId = servicePointId1,
                ClientId = clientId,
                CustomerId = customerId,
                Channel = "Email"
            };
            subscribeList.Add(subscribeInsight);
            subscribeInsight = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program1,
                InsightTypeName = insight3,
                PremiseId = string.Empty,
                ServiceContractId = string.Empty,
                ServicePointId = string.Empty,
                ClientId = clientId,
                CustomerId = customerId,
                Channel = "Email"
            };
            subscribeList.Add(subscribeInsight);

            var result = _subscriptionFacadeManager.GetSubscribeList(clientId, customerId, accountId, subscribeList);


            Assert.IsNotNull(result);
            Assert.AreEqual(8, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            foreach (var program in subscribedProgram)
            {
                Assert.AreEqual(true, program.IsSelected);
                Assert.AreEqual(accountId, program.AccountId);
                Assert.AreEqual(customerId, program.CustomerId);
                Assert.IsNull(program.Channel);
            }
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            foreach (var insight in insights)
            {
                if (insight.InsightTypeName.ToLower() == insight3.ToLower())
                {
                    Assert.AreEqual(true, insight.IsSelected);
                    Assert.AreEqual(string.Empty, insight.ServiceContractId);
                    Assert.AreEqual(string.Empty, insight.ServicePointId);
                    Assert.AreEqual(string.Empty, insight.PremiseId);
                }
                else if (insight.InsightTypeName.ToLower() == insight1.ToLower())
                {
                    Assert.AreEqual(true, insight.IsSelected);
                    Assert.AreNotEqual(string.Empty, insight.ServiceContractId);
                    Assert.AreNotEqual(string.Empty, insight.ServicePointId);
                    Assert.AreNotEqual(string.Empty, insight.PremiseId);
                }
                else
                    Assert.AreEqual(false, insight.IsSelected);

                Assert.AreEqual(accountId, insight.AccountId);
                if (!string.IsNullOrEmpty(insight.ServiceContractId))
                {
                    Assert.AreNotEqual(string.Empty, insight.ServiceContractId);
                    Assert.AreNotEqual(string.Empty, insight.ServicePointId);
                    Assert.AreNotEqual(string.Empty, insight.PremiseId);
                }
                Assert.AreEqual(customerId, insight.CustomerId);
                Assert.IsNotNull(insight.Channel);
                Assert.AreNotEqual(string.Empty, insight.Channel);
            }

        }

        [TestMethod()]
        public void GetSubscribeList_UnScbscribeAllExistingSubscription_Service2_Test()
        {
            var program = "Program1";
            //var programInsight = "Program 2.ServiceLevelCostThreshold";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            //var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            //var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            //var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";

            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock client settings
            var mockClientSettings = TestHelpers.CreateMockClientSettings();
            _clientConfigFacadeMock.Setup(c => c.GetClientSettings(It.IsAny<int>())).Returns(mockClientSettings);
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            //var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            //var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            //var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            //insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            //insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            //insightList.Add(mockInsight8);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));
            
            var subscribeList = new List<SubscriptionModel>();
            var subscribeInsight = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = false,
                ProgramName = program,
                InsightTypeName = insight2,
                PremiseId = premiseId,
                ServiceContractId = serviceContractId1,
                ServicePointId = servicePointId1,
                CustomerId = customerId,
                ClientId = clientId
            };
            subscribeList.Add(subscribeInsight);


            var result = _subscriptionFacadeManager.GetSubscribeList(clientId, customerId, accountId, subscribeList);


            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);
            //var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            //Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            //Assert.AreEqual(false, subscribedProgram[0].IsSelected);
            //Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            //Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            foreach (var insight in insights)
            {
                Assert.AreEqual(false, insight.IsSelected);
                Assert.AreEqual(accountId, insight.AccountId);
                Assert.IsNotNull(insight.ServiceContractId);
                Assert.AreEqual(customerId, insight.CustomerId);
                Assert.IsNotNull(insight.Channel);
                Assert.AreNotEqual(string.Empty, insight.Channel);
            }



        }

        [TestMethod]
        public void InsertSubscriptionTest()
        {

            var clientId = 87;
            var customerId = "subInsertTest";
            var accountId = "subInsertTestAcc";
            var premiseId = "subInsertTestPremise";
            var serviceId = "subInsertTestService";
            var serviceContractId = "subInsertTestServiceContract";
            var program = "Program1";
            var insight2 = "ServiceLevelUsageThreshold";

            // mock subscription insert
            _cassandraRepository.Setup(t => t.GetSingle(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>())).Returns((SubscriptionEntity)null).Verifiable();
            _cassandraRepository.Setup(t => t.InsertOrUpdate(It.IsAny<SubscriptionEntity>(), It.IsAny<string>())).Returns(true).Verifiable();
            
            //var subscribeList = new List<SubscriptionModel>();
            var subscribeInsight = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program,
                InsightTypeName = insight2,
                PremiseId = premiseId,
                ServiceContractId = serviceContractId,
                ServicePointId = serviceId,
                CustomerId = customerId,
                ClientId = clientId
            };
            //subscribeList.Add(subscribeInsight);

            var result = _subscriptionManager.InsertSubscription(subscribeInsight);

            Assert.AreEqual(true, result);
            _cassandraRepository.Verify(t => t.GetSingle(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()), Times.AtMost(1));
            _cassandraRepository.Verify(
                t => t.InsertOrUpdateAsync(It.Is<SubscriptionEntity>(s => s.LastModifiedDate >= DateTime.MinValue), It.IsAny<string>()), Times.AtMost(2));
        }

        [TestMethod()]
        public void GetSubscribeList_NoExistingSubscription_ThresholdUpdate_Account_Test()
        {
            var program = "Program1";
            var insight3 = "BillToDate";
            var insight5 = "AccountProjectedCost";
            var insight8 = "AccountLevelCostThreshold";

            double threshold = 200;

            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock client settings
            var mockClientSettings = TestHelpers.CreateMockClientSettings();
            _clientConfigFacadeMock.Setup(c => c.GetClientSettings(It.IsAny<int>())).Returns(mockClientSettings);
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                 .Returns(services);
            // mock empty subscription
            IList<SubscriptionEntity> subList = new List<SubscriptionEntity>();
            _cassandraRepository.Setup(t => t.GetAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>())).Returns(Task.FromResult(subList));

            var subscribeList = new List<SubscriptionModel>();
            var subscribeInsight = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program,
                InsightTypeName = insight8,
                CustomerId = customerId,
                ClientId = clientId,
                SubscriberThresholdValue = threshold,
                Channel = "Email"
            };
            subscribeList.Add(subscribeInsight);

            var result = _subscriptionFacadeManager.GetSubscribeList(clientId, customerId, accountId, subscribeList);


            Assert.IsNotNull(result);
            Assert.AreEqual(12, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(true, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);
            foreach (var insight in insights)
            {
                Assert.AreEqual(accountId, insight.AccountId);
                if (insight.InsightTypeName == insight3 || insight.InsightTypeName == insight5 ||
                    insight.InsightTypeName == insight8)
                {
                    Assert.AreEqual(string.Empty, insight.ServiceContractId);
                    Assert.AreEqual(string.Empty, insight.ServicePointId);
                    Assert.AreEqual(string.Empty, insight.PremiseId);
                }

                if (insight.InsightTypeName == insight8)
                {
                    Assert.AreEqual(threshold, insight.SubscriberThresholdValue);
                    Assert.AreEqual(true, insight.IsSelected);
                }
                else
                {
                    Assert.AreEqual(false, insight.IsSelected);
                    Assert.AreNotEqual(threshold, insight.SubscriberThresholdValue);
                }
                Assert.AreEqual(customerId, insight.CustomerId);
                Assert.AreNotEqual(string.Empty, insight.Channel);
            }

        }

        [TestMethod()]
        public void GetSubscribeList_NoExistingSubscription_ChannelUpdate_Account_Test()
        {
            var program = "Program1";
            var insight3 = "BillToDate";
            var insight5 = "AccountProjectedCost";
            var insight8 = "AccountLevelCostThreshold";

            var channel = "EmailandSMS";

            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock client settings
            var mockClientSettings = TestHelpers.CreateMockClientSettings();
            _clientConfigFacadeMock.Setup(c => c.GetClientSettings(It.IsAny<int>())).Returns(mockClientSettings);
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock empty subscription
            IList<SubscriptionEntity> subList = new List<SubscriptionEntity>();
            _cassandraRepository.Setup(t => t.GetAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>())).Returns(Task.FromResult(subList));

            var subscribeList = new List<SubscriptionModel>();
            var subscribeInsight = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program,
                InsightTypeName = insight8,
                CustomerId = customerId,
                ClientId = clientId,
                Channel = channel
            };
            subscribeList.Add(subscribeInsight);

            var result = _subscriptionFacadeManager.GetSubscribeList(clientId, customerId, accountId, subscribeList);


            Assert.IsNotNull(result);
            Assert.AreEqual(12, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(true, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);
            foreach (var insight in insights)
            {
                Assert.AreEqual(accountId, insight.AccountId);
                if (insight.InsightTypeName == insight3 || insight.InsightTypeName == insight5 ||
                    insight.InsightTypeName == insight8)
                {
                    Assert.AreEqual(string.Empty, insight.ServiceContractId);
                    Assert.AreEqual(string.Empty, insight.ServicePointId);
                    Assert.AreEqual(string.Empty, insight.PremiseId);
                }

                if (insight.InsightTypeName == insight8)
                {
                    Assert.AreEqual(channel, insight.Channel);
                    Assert.AreEqual(true, insight.IsSelected);
                }
                else
                {
                    Assert.AreNotEqual(channel, insight.Channel);
                    Assert.AreEqual(false, insight.IsSelected);
                }
                Assert.AreEqual(customerId, insight.CustomerId);
                Assert.AreNotEqual(string.Empty, insight.Channel);
            }

        }

        [TestMethod()]
        public void GetSubscribeList_ExistingSubscription_ThresholdUpdate_Account_Test()
        {
            var program = "Program1";
            //var programInsight = "Program 1.AccountProjectedCost";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";

            double threshold = 200;

            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock client settings
            var mockClientSettings = TestHelpers.CreateMockClientSettings();
            _clientConfigFacadeMock.Setup(c => c.GetClientSettings(It.IsAny<int>())).Returns(mockClientSettings);
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            var mockInsight2 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight2, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            insightList.Add(mockInsight2);
            insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));

            var subscribeList = new List<SubscriptionModel>();
            var subscribeInsight = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program,
                InsightTypeName = insight8,
                PremiseId = string.Empty,
                ServiceContractId = string.Empty,
                ServicePointId = string.Empty,
                CustomerId = customerId,
                ClientId = clientId,
                SubscriberThresholdValue = threshold,
                Channel = "Email"
            };
            subscribeList.Add(subscribeInsight);

            var result = _subscriptionFacadeManager.GetSubscribeList(clientId, customerId, accountId, subscribeList);


            Assert.IsNotNull(result);
            Assert.AreEqual(12, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(true, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);
            foreach (var insight in insights)
            {
                if (insight.InsightTypeName.ToLower() == insight8.ToLower())
                {
                    Assert.AreEqual(true, insight.IsSelected);
                    Assert.AreEqual(threshold, insight.SubscriberThresholdValue);
                    Assert.AreEqual(string.Empty, insight.ServiceContractId);
                    Assert.AreEqual(string.Empty, insight.ServicePointId);
                    Assert.AreEqual(string.Empty, insight.PremiseId);
                }
                else
                {
                    Assert.AreEqual(false, insight.IsSelected);

                    Assert.AreNotEqual(threshold, insight.SubscriberThresholdValue);
                }

                Assert.AreEqual(accountId, insight.AccountId);
                Assert.AreEqual(customerId, insight.CustomerId);
                Assert.AreNotEqual(string.Empty, insight.Channel);
            }
        }

        [TestMethod()]
        public void GetSubscribeList_ExistingSubscription_ChannelUpdate_Account_Test()
        {
            var program = "Program1";
            //var programInsight = "Program 1.AccountProjectedCost";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";

            var channel = "EmailandSMS";

            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock client settings
            var mockClientSettings = TestHelpers.CreateMockClientSettings();
            _clientConfigFacadeMock.Setup(c => c.GetClientSettings(It.IsAny<int>())).Returns(mockClientSettings);
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            var mockInsight2 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight2, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            insightList.Add(mockInsight2);
            insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));

            var subscribeList = new List<SubscriptionModel>();
            var subscribeInsight = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program,
                InsightTypeName = insight8,
                PremiseId = string.Empty,
                ServiceContractId = string.Empty,
                ServicePointId = string.Empty,
                CustomerId = customerId,
                ClientId = clientId,
                Channel = channel
            };
            subscribeList.Add(subscribeInsight);

            var result = _subscriptionFacadeManager.GetSubscribeList(clientId, customerId, accountId, subscribeList);


            Assert.IsNotNull(result);
            Assert.AreEqual(12, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(true, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);
            foreach (var insight in insights)
            {
                if (insight.InsightTypeName.ToLower() == insight8.ToLower())
                {
                    Assert.AreEqual(true, insight.IsSelected);
                    Assert.AreEqual(channel, insight.Channel);
                    Assert.AreEqual(string.Empty, insight.ServiceContractId);
                    Assert.AreEqual(string.Empty, insight.ServicePointId);
                    Assert.AreEqual(string.Empty, insight.PremiseId);
                }
                else
                {
                    Assert.AreEqual(false, insight.IsSelected);

                    Assert.AreNotEqual(channel, insight.Channel);
                }

                Assert.AreEqual(accountId, insight.AccountId);
                Assert.AreEqual(customerId, insight.CustomerId);
                Assert.AreNotEqual(string.Empty, insight.Channel);
            }
        }

        [TestMethod()]
        public void GetSubscribeList_ExistingSubscription_FalseInTable_ThresholdUpdate_Account_Test()
        {
            var program = "Program1";
            //var programInsight = "Program 1.AccountProjectedCost";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";

            double threshold = 200;

            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock client settings
            var mockClientSettings = TestHelpers.CreateMockClientSettings();
            _clientConfigFacadeMock.Setup(c => c.GetClientSettings(It.IsAny<int>())).Returns(mockClientSettings);
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            var mockInsight2 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight2, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            insightList.Add(mockInsight2);
            insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            insightList.Add(mockInsight8);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));

            var subscribeList = new List<SubscriptionModel>();
            var subscribeInsight = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program,
                InsightTypeName = insight8,
                PremiseId = string.Empty,
                ServiceContractId = string.Empty,
                ServicePointId = string.Empty,
                CustomerId = customerId,
                ClientId = clientId,
                SubscriberThresholdValue = threshold,
                Channel = "Email"
            };
            subscribeList.Add(subscribeInsight);

            var result = _subscriptionFacadeManager.GetSubscribeList(clientId, customerId, accountId, subscribeList);


            Assert.IsNotNull(result);
            Assert.AreEqual(12, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(true, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);
            foreach (var insight in insights)
            {
                if (insight.InsightTypeName.ToLower() == insight8.ToLower())
                {
                    Assert.AreEqual(true, insight.IsSelected);
                    Assert.AreEqual(threshold, insight.SubscriberThresholdValue);
                    Assert.AreEqual(string.Empty, insight.ServiceContractId);
                    Assert.AreEqual(string.Empty, insight.ServicePointId);
                    Assert.AreEqual(string.Empty, insight.PremiseId);
                }
                else
                {
                    Assert.AreEqual(false, insight.IsSelected);

                    Assert.AreNotEqual(threshold, insight.SubscriberThresholdValue);
                }

                Assert.AreEqual(accountId, insight.AccountId);
                Assert.AreEqual(customerId, insight.CustomerId);
                Assert.AreNotEqual(string.Empty, insight.Channel);
            }
        }

        [TestMethod()]
        public void GetSubscribeList_ExistingSubscription_FalseInTable_ChannelUpdate_Account_Test()
        {
            var program = "Program1";
            //var programInsight = "Program 1.AccountProjectedCost";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";

            var channel = "EmailandSMS";

            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock client settings
            var mockClientSettings = TestHelpers.CreateMockClientSettings();
            _clientConfigFacadeMock.Setup(c => c.GetClientSettings(It.IsAny<int>())).Returns(mockClientSettings);
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            var mockInsight2 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight2, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            insightList.Add(mockInsight2);
            insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            insightList.Add(mockInsight8);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));

            var subscribeList = new List<SubscriptionModel>();
            var subscribeInsight = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program,
                InsightTypeName = insight8,
                PremiseId = string.Empty,
                ServiceContractId = string.Empty,
                ServicePointId = string.Empty,
                CustomerId = customerId,
                ClientId = clientId,
                Channel = channel
            };
            subscribeList.Add(subscribeInsight);

            var result = _subscriptionFacadeManager.GetSubscribeList(clientId, customerId, accountId, subscribeList);


            Assert.IsNotNull(result);
            Assert.AreEqual(12, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(true, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);
            foreach (var insight in insights)
            {
                if (insight.InsightTypeName.ToLower() == insight8.ToLower())
                {
                    Assert.AreEqual(true, insight.IsSelected);
                    Assert.AreEqual(channel, insight.Channel);
                    Assert.AreEqual(string.Empty, insight.ServiceContractId);
                    Assert.AreEqual(string.Empty, insight.ServicePointId);
                    Assert.AreEqual(string.Empty, insight.PremiseId);
                }
                else
                {
                    Assert.AreEqual(false, insight.IsSelected);

                    Assert.AreNotEqual(channel, insight.Channel);
                }

                Assert.AreEqual(accountId, insight.AccountId);
                Assert.AreEqual(customerId, insight.CustomerId);
                Assert.AreNotEqual(string.Empty, insight.Channel);
            }
        }
        [TestMethod()]
        public void GetSubscribeList_ExistingSubscription_TrueInTable_ThresholdUpdate_Account_Test()
        {
            var program = "Program1";
            //var programInsight = "Program 1.AccountProjectedCost";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";

            double threshold = 200;

            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock client settings
            var mockClientSettings = TestHelpers.CreateMockClientSettings();
            _clientConfigFacadeMock.Setup(c => c.GetClientSettings(It.IsAny<int>())).Returns(mockClientSettings);
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            var mockInsight2 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight2, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, true, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            insightList.Add(mockInsight2);
            insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            insightList.Add(mockInsight8);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                 t => t.GetAsync(It.IsAny<string>(),
                 It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                 .Returns(Task.FromResult(programList))
                 .Returns(Task.FromResult(insightList));

            var subscribeList = new List<SubscriptionModel>();
            var subscribeInsight = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program,
                InsightTypeName = insight8,
                PremiseId = string.Empty,
                ServiceContractId = string.Empty,
                ServicePointId = string.Empty,
                CustomerId = customerId,
                ClientId = clientId,
                SubscriberThresholdValue = threshold,
                Channel = "Email"
            };
            subscribeList.Add(subscribeInsight);

            var result = _subscriptionFacadeManager.GetSubscribeList(clientId, customerId, accountId, subscribeList);


            Assert.IsNotNull(result);
            Assert.AreEqual(12, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(true, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);
            foreach (var insight in insights)
            {
                if (insight.InsightTypeName.ToLower() == insight8.ToLower())
                {
                    Assert.AreEqual(true, insight.IsSelected);
                    Assert.AreEqual(threshold, insight.SubscriberThresholdValue);
                    Assert.AreEqual(string.Empty, insight.ServiceContractId);
                    Assert.AreEqual(string.Empty, insight.ServicePointId);
                    Assert.AreEqual(string.Empty, insight.PremiseId);
                }
                else
                {
                    Assert.AreEqual(false, insight.IsSelected);

                    Assert.AreNotEqual(threshold, insight.SubscriberThresholdValue);
                }

                Assert.AreEqual(accountId, insight.AccountId);
                Assert.AreEqual(customerId, insight.CustomerId);
                Assert.AreNotEqual(string.Empty, insight.Channel);
            }
        }

        [TestMethod()]
        public void GetSubscribeList_ExistingSubscription_TrueInTable_ChannelUpdate_Account_Test()
        {
            var program = "Program1";
            //var programInsight = "Program 1.AccountProjectedCost";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";

            var channel = "EmailandSMS";

            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock client settings
            var mockClientSettings = TestHelpers.CreateMockClientSettings();
            _clientConfigFacadeMock.Setup(c => c.GetClientSettings(It.IsAny<int>())).Returns(mockClientSettings);
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            var mockInsight2 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight2, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, true, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            insightList.Add(mockInsight2);
            insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            insightList.Add(mockInsight8);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));

            var subscribeList = new List<SubscriptionModel>();
            var subscribeInsight = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program,
                InsightTypeName = insight8,
                PremiseId = string.Empty,
                ServiceContractId = string.Empty,
                ServicePointId = string.Empty,
                CustomerId = customerId,
                ClientId = clientId,
                Channel = channel
            };
            subscribeList.Add(subscribeInsight);

            var result = _subscriptionFacadeManager.GetSubscribeList(clientId, customerId, accountId, subscribeList);


            Assert.IsNotNull(result);
            Assert.AreEqual(12, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(true, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);
            foreach (var insight in insights)
            {
                if (insight.InsightTypeName.ToLower() == insight8.ToLower())
                {
                    Assert.AreEqual(true, insight.IsSelected);
                    Assert.AreEqual(channel, insight.Channel);
                    Assert.AreEqual(string.Empty, insight.ServiceContractId);
                    Assert.AreEqual(string.Empty, insight.ServicePointId);
                    Assert.AreEqual(string.Empty, insight.PremiseId);
                }
                else
                {
                    Assert.AreEqual(false, insight.IsSelected);

                    Assert.AreNotEqual(channel, insight.Channel);
                }

                Assert.AreEqual(accountId, insight.AccountId);
                Assert.AreEqual(customerId, insight.CustomerId);
                Assert.AreNotEqual(string.Empty, insight.Channel);
            }
        }


        [TestMethod()]
        public void GetSubscribeList_NoExistingSubscription_ThresholdUpdate_Service_Test()
        {
            var program = "Program1";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight5 = "AccountProjectedCost";
            var insight8 = "AccountLevelCostThreshold";

            double threshold = 200;

            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock client settings
            var mockClientSettings = TestHelpers.CreateMockClientSettings();
            _clientConfigFacadeMock.Setup(c => c.GetClientSettings(It.IsAny<int>())).Returns(mockClientSettings);
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock empty subscription
            IList<SubscriptionEntity> subList = new List<SubscriptionEntity>();
            _cassandraRepository.Setup(t => t.GetAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>())).Returns(Task.FromResult(subList));

            var subscribeList = new List<SubscriptionModel>();
            var subscribeInsight = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program,
                InsightTypeName = insight2,
                PremiseId = premiseId,
                ServiceContractId = serviceContractId1,
                ServicePointId = servicePointId1,
                CustomerId = customerId,
                ClientId = clientId,
                SubscriberThresholdValue = threshold,
                Channel = "Email"
            };
            subscribeList.Add(subscribeInsight);

            var result = _subscriptionFacadeManager.GetSubscribeList(clientId, customerId, accountId, subscribeList);


            Assert.IsNotNull(result);
            Assert.AreEqual(12, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(true, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);
            foreach (var insight in insights)
            {
                Assert.AreEqual(accountId, insight.AccountId);
                if (insight.InsightTypeName == insight3 || insight.InsightTypeName == insight5 ||
                    insight.InsightTypeName == insight8)
                {
                    Assert.AreEqual(string.Empty, insight.ServiceContractId);
                    Assert.AreEqual(string.Empty, insight.ServicePointId);
                    Assert.AreEqual(string.Empty, insight.PremiseId);
                }

                if (insight.InsightTypeName == insight2)
                {
                    Assert.AreEqual(threshold, insight.SubscriberThresholdValue);
                    Assert.AreEqual(true, insight.IsSelected);
                }
                else
                {
                    Assert.AreEqual(false, insight.IsSelected);
                    Assert.AreNotEqual(threshold, insight.SubscriberThresholdValue);
                }
                Assert.AreEqual(customerId, insight.CustomerId);
                Assert.AreNotEqual(string.Empty, insight.Channel);
            }

        }

        [TestMethod()]
        public void GetSubscribeList_NoExistingSubscription_ChannelUpdate_Service_Test()
        {
            var program = "Program1";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight5 = "AccountProjectedCost";
            var insight8 = "AccountLevelCostThreshold";

            var channel = "EmailandSMS";

            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock client settings
            var mockClientSettings = TestHelpers.CreateMockClientSettings();
            _clientConfigFacadeMock.Setup(c => c.GetClientSettings(It.IsAny<int>())).Returns(mockClientSettings);
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                 .Returns(services);
            // mock empty subscription
            IList<SubscriptionEntity> subList = new List<SubscriptionEntity>();
            _cassandraRepository.Setup(t => t.GetAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>())).Returns(Task.FromResult(subList));

            var subscribeList = new List<SubscriptionModel>();
            var subscribeInsight = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program,
                InsightTypeName = insight2,
                PremiseId = premiseId,
                ServiceContractId = serviceContractId1,
                ServicePointId = servicePointId1,
                CustomerId = customerId,
                ClientId = clientId,
                Channel = channel
            };
            subscribeList.Add(subscribeInsight);

            var result = _subscriptionFacadeManager.GetSubscribeList(clientId, customerId, accountId, subscribeList);


            Assert.IsNotNull(result);
            Assert.AreEqual(12, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(true, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);
            foreach (var insight in insights)
            {
                Assert.AreEqual(accountId, insight.AccountId);
                if (insight.InsightTypeName == insight3 || insight.InsightTypeName == insight5 ||
                    insight.InsightTypeName == insight8)
                {
                    Assert.AreEqual(string.Empty, insight.ServiceContractId);
                    Assert.AreEqual(string.Empty, insight.ServicePointId);
                    Assert.AreEqual(string.Empty, insight.PremiseId);
                }

                if (insight.InsightTypeName == insight2)
                {
                    Assert.AreEqual(channel, insight.Channel);
                    Assert.AreEqual(true, insight.IsSelected);
                }
                else
                {
                    Assert.AreNotEqual(channel, insight.Channel);
                    Assert.AreEqual(false, insight.IsSelected);
                }
                Assert.AreEqual(customerId, insight.CustomerId);
                Assert.AreNotEqual(string.Empty, insight.Channel);
            }

        }

        [TestMethod()]
        public void GetSubscribeList_ExistingSubscription_ThresholdUpdate_Service_Test()
        {
            var program = "Program1";
            //var programInsight = "Program 1.AccountProjectedCost";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";

            double threshold = 200;

            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock client settings
            var mockClientSettings = TestHelpers.CreateMockClientSettings();
            _clientConfigFacadeMock.Setup(c => c.GetClientSettings(It.IsAny<int>())).Returns(mockClientSettings);
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            insightList.Add(mockInsight8);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));

            var subscribeList = new List<SubscriptionModel>();
            var subscribeInsight = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program,
                InsightTypeName = insight2,
                PremiseId = premiseId,
                ServiceContractId = serviceContractId1,
                ServicePointId = servicePointId1,
                CustomerId = customerId,
                ClientId = clientId,
                SubscriberThresholdValue = threshold,
                Channel = "Email"
            };
            subscribeList.Add(subscribeInsight);

            var result = _subscriptionFacadeManager.GetSubscribeList(clientId, customerId, accountId, subscribeList);


            Assert.IsNotNull(result);
            Assert.AreEqual(12, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(true, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);
            foreach (var insight in insights)
            {
                if (insight.InsightTypeName.ToLower() == insight2.ToLower())
                {
                    Assert.AreEqual(true, insight.IsSelected);
                    Assert.AreEqual(threshold, insight.SubscriberThresholdValue);
                    Assert.AreEqual(serviceContractId1, insight.ServiceContractId);
                    Assert.AreEqual(servicePointId1, insight.ServicePointId);
                    Assert.AreEqual(premiseId, insight.PremiseId);
                }
                else
                {
                    Assert.AreEqual(false, insight.IsSelected);
                    Assert.AreNotEqual(threshold, insight.SubscriberThresholdValue);
                }

                Assert.AreEqual(accountId, insight.AccountId);
                Assert.AreEqual(customerId, insight.CustomerId);
                Assert.AreNotEqual(string.Empty, insight.Channel);
            }
        }

        [TestMethod()]
        public void GetSubscribeList_ExistingSubscription_ChannelUpdate_Service_Test()
        {
            var program = "Program1";
            //var programInsight = "Program 1.AccountProjectedCost";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";

            var channel = "EmailandSMS";

            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock client settings
            var mockClientSettings = TestHelpers.CreateMockClientSettings();
            _clientConfigFacadeMock.Setup(c => c.GetClientSettings(It.IsAny<int>())).Returns(mockClientSettings);
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            insightList.Add(mockInsight8);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                 t => t.GetAsync(It.IsAny<string>(),
                 It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                 .Returns(Task.FromResult(programList))
                 .Returns(Task.FromResult(insightList));
            var subscribeList = new List<SubscriptionModel>();
            var subscribeInsight = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program,
                InsightTypeName = insight2,
                PremiseId = premiseId,
                ServiceContractId = serviceContractId1,
                ServicePointId = servicePointId1,
                CustomerId = customerId,
                ClientId = clientId,
                Channel = channel
            };
            subscribeList.Add(subscribeInsight);

            var result = _subscriptionFacadeManager.GetSubscribeList(clientId, customerId, accountId, subscribeList);


            Assert.IsNotNull(result);
            Assert.AreEqual(12, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(true, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);
            foreach (var insight in insights)
            {
                if (insight.InsightTypeName.ToLower() == insight2.ToLower())
                {
                    Assert.AreEqual(true, insight.IsSelected);
                    Assert.AreEqual(channel, insight.Channel);
                    Assert.AreEqual(serviceContractId1, insight.ServiceContractId);
                    Assert.AreEqual(servicePointId1, insight.ServicePointId);
                    Assert.AreEqual(premiseId, insight.PremiseId);
                }
                else
                {
                    Assert.AreEqual(false, insight.IsSelected);
                    Assert.AreNotEqual(channel, insight.Channel);
                }

                Assert.AreEqual(accountId, insight.AccountId);
                Assert.AreEqual(customerId, insight.CustomerId);
                Assert.AreNotEqual(string.Empty, insight.Channel);
            }
        }

        [TestMethod()]
        public void GetSubscribeList_ExistingSubscription_FalseInTable_ThresholdUpdate_Service_Test()
        {
            var program = "Program1";
            //var programInsight = "Program 1.AccountProjectedCost";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";

            double threshold = 200;

            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock client settings
            var mockClientSettings = TestHelpers.CreateMockClientSettings();
            _clientConfigFacadeMock.Setup(c => c.GetClientSettings(It.IsAny<int>())).Returns(mockClientSettings);
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            var mockInsight2 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight2, false, 0);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            insightList.Add(mockInsight2);
            insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            insightList.Add(mockInsight8);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));

            var subscribeList = new List<SubscriptionModel>();
            var subscribeInsight = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program,
                InsightTypeName = insight2,
                PremiseId = premiseId,
                ServiceContractId = serviceContractId1,
                ServicePointId = servicePointId1,
                CustomerId = customerId,
                ClientId = clientId,
                SubscriberThresholdValue = threshold,
                Channel = "Email"
            };
            subscribeList.Add(subscribeInsight);

            var result = _subscriptionFacadeManager.GetSubscribeList(clientId, customerId, accountId, subscribeList);

            Assert.IsNotNull(result);
            Assert.AreEqual(12, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(true, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);
            foreach (var insight in insights)
            {
                if (insight.InsightTypeName.ToLower() == insight2.ToLower())
                {
                    Assert.AreEqual(true, insight.IsSelected);
                    Assert.AreEqual(threshold, insight.SubscriberThresholdValue);
                    Assert.AreEqual(serviceContractId1, insight.ServiceContractId);
                    Assert.AreEqual(servicePointId1, insight.ServicePointId);
                    Assert.AreEqual(premiseId, insight.PremiseId);
                }
                else
                {
                    Assert.AreEqual(false, insight.IsSelected);
                    Assert.AreNotEqual(threshold, insight.SubscriberThresholdValue);
                }

                Assert.AreEqual(accountId, insight.AccountId);
                Assert.AreEqual(customerId, insight.CustomerId);
                Assert.AreNotEqual(string.Empty, insight.Channel);
            }
        }

        [TestMethod()]
        public void GetSubscribeList_ExistingSubscription_FalseInTable_ChannelUpdate_Service_Test()
        {
            var program = "Program1";
            //var programInsight = "Program 1.AccountProjectedCost";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";

            var channel = "EmailandSMS";

            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock client settings
            var mockClientSettings = TestHelpers.CreateMockClientSettings();
            _clientConfigFacadeMock.Setup(c => c.GetClientSettings(It.IsAny<int>())).Returns(mockClientSettings);
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            var mockInsight2 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight2, false, 0);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            insightList.Add(mockInsight2);
            insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            insightList.Add(mockInsight8);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));

            var subscribeList = new List<SubscriptionModel>();
            var subscribeInsight = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program,
                InsightTypeName = insight2,
                PremiseId = premiseId,
                ServiceContractId = serviceContractId1,
                ServicePointId = servicePointId1,
                CustomerId = customerId,
                ClientId = clientId,
                Channel = channel
            };
            subscribeList.Add(subscribeInsight);

            var result = _subscriptionFacadeManager.GetSubscribeList(clientId, customerId, accountId, subscribeList);


            Assert.IsNotNull(result);
            Assert.AreEqual(12, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(true, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);
            foreach (var insight in insights)
            {
                if (insight.InsightTypeName.ToLower() == insight2.ToLower())
                {
                    Assert.AreEqual(true, insight.IsSelected);
                    Assert.AreEqual(channel, insight.Channel);
                    Assert.AreEqual(serviceContractId1, insight.ServiceContractId);
                    Assert.AreEqual(servicePointId1, insight.ServicePointId);
                    Assert.AreEqual(premiseId, insight.PremiseId);
                }
                else
                {
                    Assert.AreEqual(false, insight.IsSelected);
                    Assert.AreNotEqual(channel, insight.Channel);
                }

                Assert.AreEqual(accountId, insight.AccountId);
                Assert.AreEqual(customerId, insight.CustomerId);
                Assert.AreNotEqual(string.Empty, insight.Channel);
            }
        }

        [TestMethod()]
        public void GetSubscribeList_ExistingSubscription_TrueInTable_ThresholdUpdate_Service_Test()
        {
            var program = "Program1";
            //var programInsight = "Program 1.AccountProjectedCost";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";

            double threshold = 200;

            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock client settings
            var mockClientSettings = TestHelpers.CreateMockClientSettings();
            _clientConfigFacadeMock.Setup(c => c.GetClientSettings(It.IsAny<int>())).Returns(mockClientSettings);
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            var mockInsight2 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight2, true, 0);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            insightList.Add(mockInsight2);
            insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            insightList.Add(mockInsight8);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));

            var subscribeList = new List<SubscriptionModel>();
            var subscribeInsight = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program,
                InsightTypeName = insight2,
                PremiseId = premiseId,
                ServiceContractId = serviceContractId1,
                ServicePointId = servicePointId1,
                CustomerId = customerId,
                ClientId = clientId,
                SubscriberThresholdValue = threshold,
                Channel = "Email"
            };
            subscribeList.Add(subscribeInsight);

            var result = _subscriptionFacadeManager.GetSubscribeList(clientId, customerId, accountId, subscribeList);


            Assert.IsNotNull(result);
            Assert.AreEqual(12, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(true, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);
            foreach (var insight in insights)
            {
                if (insight.InsightTypeName.ToLower() == insight2.ToLower())
                {
                    Assert.AreEqual(true, insight.IsSelected);
                    Assert.AreEqual(threshold, insight.SubscriberThresholdValue);
                    Assert.AreEqual(serviceContractId1, insight.ServiceContractId);
                    Assert.AreEqual(servicePointId1, insight.ServicePointId);
                    Assert.AreEqual(premiseId, insight.PremiseId);
                }
                else
                {
                    Assert.AreEqual(false, insight.IsSelected);
                    Assert.AreNotEqual(threshold, insight.SubscriberThresholdValue);
                }

                Assert.AreEqual(accountId, insight.AccountId);
                Assert.AreEqual(customerId, insight.CustomerId);
                Assert.AreNotEqual(string.Empty, insight.Channel);
            }
        }

        [TestMethod()]
        public void GetSubscribeList_ExistingSubscription_TrueInTable_ChannelUpdate_Service_Test()
        {
            var program = "Program1";
            //var programInsight = "Program 1.AccountProjectedCost";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";

            var channel = "EmailandSMS";

            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock client settings
            var mockClientSettings = TestHelpers.CreateMockClientSettings();
            _clientConfigFacadeMock.Setup(c => c.GetClientSettings(It.IsAny<int>())).Returns(mockClientSettings);
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);
            var mockInsight2 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight2, true, 0);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight7, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            insightList.Add(mockInsight2);
            insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            insightList.Add(mockInsight8);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));

            var subscribeList = new List<SubscriptionModel>();
            var subscribeInsight = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program,
                InsightTypeName = insight2,
                PremiseId = premiseId,
                ServiceContractId = serviceContractId1,
                ServicePointId = servicePointId1,
                CustomerId = customerId,
                ClientId = clientId,
                Channel = channel
            };
            subscribeList.Add(subscribeInsight);

            var result = _subscriptionFacadeManager.GetSubscribeList(clientId, customerId, accountId, subscribeList);


            Assert.IsNotNull(result);
            Assert.AreEqual(12, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            Assert.AreEqual(program.ToLower(), subscribedProgram[0].ProgramName.ToLower());
            Assert.AreEqual(true, subscribedProgram[0].IsSelected);
            Assert.AreEqual(accountId, subscribedProgram[0].AccountId);
            Assert.AreEqual(customerId, subscribedProgram[0].CustomerId);
            Assert.IsNull(subscribedProgram[0].Channel);
            foreach (var insight in insights)
            {
                if (insight.InsightTypeName.ToLower() == insight2.ToLower())
                {
                    Assert.AreEqual(true, insight.IsSelected);
                    Assert.AreEqual(channel, insight.Channel);
                    Assert.AreEqual(serviceContractId1, insight.ServiceContractId);
                    Assert.AreEqual(servicePointId1, insight.ServicePointId);
                    Assert.AreEqual(premiseId, insight.PremiseId);
                }
                else
                {
                    Assert.AreEqual(false, insight.IsSelected);
                    Assert.AreNotEqual(channel, insight.Channel);
                }

                Assert.AreEqual(accountId, insight.AccountId);
                Assert.AreEqual(customerId, insight.CustomerId);
                Assert.AreNotEqual(string.Empty, insight.Channel);
            }
        }

        [TestMethod()]
        public void GetSubscribeList_NoExistingSubscription_DoubleOptIn_Account_Test()
        {
            var program1 = "Program1";
            //var insight3 = "BillToDate";
            //var insight5 = "AccountProjectedCost";
            var insight8 = "AccountLevelCostThreshold";

            double threshold = 200;

            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock client settings
            var mockClientSettings = TestHelpers.CreateMockClientSettings();
            _clientConfigFacadeMock.Setup(c => c.GetClientSettings(It.IsAny<int>())).Returns(mockClientSettings);
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock empty subscription
            IList<SubscriptionEntity> subList = new List<SubscriptionEntity>();
            _cassandraRepository.Setup(t => t.GetAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>())).Returns(Task.FromResult(subList));

            var subscribeList = new List<SubscriptionModel>();
            var subscribeInsight = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program1,
                InsightTypeName = insight8,
                CustomerId = customerId,
                ClientId = clientId,
                SubscriberThresholdValue = threshold,
                Channel = "Email"
            };
            subscribeList.Add(subscribeInsight);

            var result = _subscriptionFacadeManager.GetSubscribeList(clientId, customerId, accountId, subscribeList);


            Assert.IsNotNull(result);
            Assert.AreEqual(12, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            foreach (var program in subscribedProgram)
            {
                Assert.AreEqual(false, program.IsEmailOptInCompleted);
                Assert.AreEqual(false, program.IsSmsOptInCompleted);
            }
            foreach (var insight in insights)
            {
                Assert.AreEqual(true, insight.IsEmailOptInCompleted);
                Assert.AreEqual(true, insight.IsSmsOptInCompleted);
            }

        }


        [TestMethod()]
        public void GetSubscribeList_ExistingSubscription_DoubleOptInCompleted_Account_Test()
        {
            var program1 = "Program1";
            //var programInsight = "Program 1.AccountProjectedCost";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";

            double threshold = 200;

            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock client settings
            var mockClientSettings = TestHelpers.CreateMockClientSettings();
            _clientConfigFacadeMock.Setup(c => c.GetClientSettings(It.IsAny<int>())).Returns(mockClientSettings);
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, string.Empty, true, 0, true, true);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, insight1, false, 0, true, true);
            var mockInsight2 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, insight2, false, 0, true, true);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, insight4, false, 0, true, true);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program1, insight5, false, 0, true, true);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, insight6, false, 0, true, true);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, insight7, false, 0, true, true);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program1, insight3, false, 0, true, true);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, insight9, false, 0, true, true);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, insight10, false, 0, true, true);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, insight11, false, 0, true, true);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            insightList.Add(mockInsight2);
            insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));

            var subscribeList = new List<SubscriptionModel>();
            var subscribeInsight = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program1,
                InsightTypeName = insight8,
                PremiseId = string.Empty,
                ServiceContractId = string.Empty,
                ServicePointId = string.Empty,
                CustomerId = customerId,
                ClientId = clientId,
                SubscriberThresholdValue = threshold,
                Channel = "Email"
            };
            subscribeList.Add(subscribeInsight);

            var result = _subscriptionFacadeManager.GetSubscribeList(clientId, customerId, accountId, subscribeList);


            Assert.IsNotNull(result);
            Assert.AreEqual(12, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            foreach (var program in subscribedProgram)
            {
                Assert.AreEqual(true, program.IsSmsOptInCompleted);
                Assert.AreEqual(true, program.IsEmailOptInCompleted);
            }
            foreach (var insight in insights)
            {
                Assert.AreEqual(true, insight.IsSmsOptInCompleted);
                Assert.AreEqual(true, insight.IsEmailOptInCompleted);
            }
        }

        [TestMethod()]
        public void GetSubscribeList_ExistingSubscription_DoubleOptInIncompleted_Account_Test()
        {
            var program1 = "Program1";
            //var programInsight = "Program 1.AccountProjectedCost";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            //var commodity = "water";

            double threshold = 200;

            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId1 = "EM001";
            var rateClass = "RG-1";
            var servicePointId1 = "SP_BTD001a1";
            var serviceContractId1 = "BTD001a1_BTD001a1p1_1_EM001";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock client settings
            var mockClientSettings = TestHelpers.CreateMockClientSettings();
            _clientConfigFacadeMock.Setup(c => c.GetClientSettings(It.IsAny<int>())).Returns(mockClientSettings);
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity1);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, insight1, false, 0);
            var mockInsight2 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, insight2, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program1, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, insight7, false, 0);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program1, insight3, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program1, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            insightList.Add(mockInsight2);
            insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));

            var subscribeList = new List<SubscriptionModel>();
            var subscribeInsight = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program1,
                InsightTypeName = insight8,
                PremiseId = string.Empty,
                ServiceContractId = string.Empty,
                ServicePointId = string.Empty,
                CustomerId = customerId,
                ClientId = clientId,
                SubscriberThresholdValue = threshold,
                Channel = "Email"
            };
            subscribeList.Add(subscribeInsight);

            var result = _subscriptionFacadeManager.GetSubscribeList(clientId, customerId, accountId, subscribeList);


            Assert.IsNotNull(result);
            Assert.AreEqual(12, result.Count);
            var subscribedProgram = result.FindAll(i => string.IsNullOrEmpty(i.InsightTypeName));
            var insights = result.FindAll(i => !string.IsNullOrEmpty(i.InsightTypeName));
            foreach (var program in subscribedProgram)
            {
                Assert.AreEqual(false, program.IsSmsOptInCompleted);
                Assert.AreEqual(false, program.IsEmailOptInCompleted);
            }
            foreach (var insight in insights)
            {
                Assert.AreEqual(true, insight.IsSmsOptInCompleted);
                Assert.AreEqual(true, insight.IsEmailOptInCompleted);
            }
        }

        /// <summary>
        /// Success Test
        /// </summary>
        [TestMethod(), TestCategory("SubscriptionController"), TestCategory("BVT")]
        public void SubscriptionV1_ValidateInsightList_TestOne()
        {
            const string program = "Program1";

            const string insight1 = "ServiceLevelCostThreshold";

            const int clientId = 87;
            const string customerId = "VID001";
            const string accountId = "VID001a1";
            const string premiseId = "VID001a1p1";

            const string billCycleScheduleId = "Cycle01";
            const string meterId1 = "EM001";

            const string rateClass = "RG-1";

            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            const string servicePointId1 = "SP_BTD001a1p1s1";
            const string serviceContractId1 = "BTD001a1_BTD001a1p1s1_1_EM001";
            const string servicePointId2 = "SP_BTD001a1p1s2";
            const string serviceContractId2 = "BTD001a1_BTD001a1p1s2_1_EM002";

            // mock client settings
            var mockClientSettings = TestHelpers.CreateMockClientSettings();
            _clientConfigFacadeMock.Setup(c => c.GetClientSettings(It.IsAny<int>())).Returns(mockClientSettings);
            // mock billing entities data
            var mockBillingEntity1 = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId1,
                servicePointId1, meterId1, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity> { mockBillingEntity1 };
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);

            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity> { mockProgramSubscription };
            // mock subscription insight entity 
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);

            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>
            {
                mockInsight1
            };
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));

            // mock subscription list
            var subscribeList = new List<SubscriptionModel>();
            var subscribeInsight1 = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program,
                InsightTypeName = insight1,
                PremiseId = premiseId,
                ServiceContractId = serviceContractId1,
                ServicePointId = servicePointId1,
                CustomerId = customerId,
                ClientId = clientId,
                SubscriberThresholdValue = 10,
                Channel = "EmailandSMS",
                CommodityKey = "electric"
            };

            var subscribeInsight2 = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program,
                InsightTypeName = insight1,
                PremiseId = premiseId,
                ServiceContractId = serviceContractId2,
                ServicePointId = servicePointId2,
                CustomerId = customerId,
                ClientId = clientId,
                SubscriberThresholdValue = 10000,
                Channel = "File",
                CommodityKey = "electric"
            };

            var subscribeInsight3 = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program,
                InsightTypeName = insight1,
                PremiseId = premiseId,
                ServiceContractId = serviceContractId2,
                ServicePointId = servicePointId2,
                CustomerId = customerId,
                ClientId = clientId,
                SubscriberThresholdValue = 1,
                Channel = "Email",
                CommodityKey = "electric"
            };

            var subscribeInsight4 = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program,
                InsightTypeName = insight1,
                PremiseId = premiseId,
                ServiceContractId = serviceContractId2,
                ServicePointId = servicePointId2,
                CustomerId = customerId,
                ClientId = clientId,
                SubscriberThresholdValue = 0,
                Channel = "Email",
                CommodityKey = "electric"
            };

            subscribeList.Add(subscribeInsight1);
            subscribeList.Add(subscribeInsight2);
            subscribeList.Add(subscribeInsight3);
            subscribeList.Add(subscribeInsight4);

            // *** Validate the subscriptions list ***
            var insightErrMsg = string.Empty;
            var result = _subscriptionFacadeManager.ValidateInsightList(clientId, subscribeList, "test@email.com", "781-694-3201", ref insightErrMsg);

            Assert.IsNotNull(result);
            Assert.AreEqual(0, result.Count);
            Assert.AreEqual(insightErrMsg, string.Empty);
        }

        /// <summary>
        /// Failure Test for first invalid entry of threshold min - where many invalid entries are supplied
        /// </summary>
        [TestMethod(), TestCategory("SubscriptionController"), TestCategory("BVT")]
        public void SubscriptionV1_ValidateInsightList_TestTwo()
        {
            const string program = "Program1";

            const string insight1 = "ServiceLevelCostThreshold";

            const int clientId = 87;
            const string customerId = "VID001";
            const string accountId = "VID001a1";
            const string premiseId = "VID001a1p1";

            const string servicePointId1 = "SP_BTD001a1p1s1";
            const string serviceContractId1 = "BTD001a1_BTD001a1p1s1_1_EM001";
            const string servicePointId2 = "SP_BTD001a1p1s2";
            const string serviceContractId2 = "BTD001a1_BTD001a1p1s2_1_EM002";

            // mock client settings
            var mockClientSettings = TestHelpers.CreateMockClientSettings();
            _clientConfigFacadeMock.Setup(c => c.GetClientSettings(It.IsAny<int>())).Returns(mockClientSettings);

            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity> { mockProgramSubscription };

            // mock subscription insight entity 
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId1, program, insight1, false, 0);

            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>
            {
                mockInsight1
            };

            _cassandraRepository.SetupSequence(
                 t => t.GetAsync(It.IsAny<string>(),
                 It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                 .Returns(Task.FromResult(programList))
                 .Returns(Task.FromResult(insightList));

            // mock subscription list
            var subscribeList = new List<SubscriptionModel>();
            var subscribeInsight1 = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program,
                InsightTypeName = insight1,
                PremiseId = premiseId,
                ServiceContractId = serviceContractId1,
                ServicePointId = servicePointId1,
                CustomerId = customerId,
                ClientId = clientId,
                SubscriberThresholdValue = -1,
                Channel = "EmailandSMS"
            };

            var subscribeInsight2 = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program,
                InsightTypeName = insight1,
                PremiseId = premiseId,
                ServiceContractId = serviceContractId2,
                ServicePointId = servicePointId2,
                CustomerId = customerId,
                ClientId = clientId,
                SubscriberThresholdValue = 10000,
                Channel = "George"
            };

            var subscribeInsight3 = new SubscriptionModel
            {
                AccountId = accountId,
                IsSelected = true,
                ProgramName = program,
                InsightTypeName = insight1,
                PremiseId = premiseId,
                ServiceContractId = serviceContractId2,
                ServicePointId = servicePointId2,
                CustomerId = customerId,
                ClientId = clientId,
                SubscriberThresholdValue = 999999999,
                Channel = "Email"
            };

            subscribeList.Add(subscribeInsight1);
            subscribeList.Add(subscribeInsight2);
            subscribeList.Add(subscribeInsight3);

            // *** Validate the subscriptions list ***
            var insightErrMsg = string.Empty;
            var result = _subscriptionFacadeManager.ValidateInsightList(clientId, subscribeList, "test@aclara.com", "781-694-3201", ref insightErrMsg);

            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Count);
            Assert.AreEqual(insightErrMsg, "The subscriber has chosen a threshold that is less than the min allowed.");

        }


        [TestMethod()]
        public void GetSettingsWithoutSubscribedPrograms_UOM_Test()
        {
            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId = "EM001";
            var rateClass = "RG-1";
            var servicePointId = "SP_BTD001a1";
            var serviceContractId = "BTD001a1_BTD001a1p1_1_EM001";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight9 = "DayThreshold";

            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);

            // mock client settings
            var mockClientSettings = TestHelpers.CreateMockClientSettings();
            _clientConfigFacadeMock.Setup(c => c.GetClientSettings(It.IsAny<int>())).Returns(mockClientSettings);
            // mock billing entities data
            var mockBillingEntity = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId,
                servicePointId, meterId, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock empty subscription
            IList<SubscriptionEntity> subList = new List<SubscriptionEntity>();
            _cassandraRepository.Setup(
                t => t.GetAsync(It.IsAny<string>(), It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(subList));

            var result = _subscriptionFacadeManager.GetDefaultListWithSubscribedPrograms(clientId, customerId, accountId);
            foreach (var insight in result)
            {
                if (insight.InsightTypeName.ToLower() == insight2.ToLower() || insight.InsightTypeName.ToLower() == insight9)
                    Assert.AreEqual(0, insight.UomId);
            }

        }



        [TestMethod()]
        public void GetSettingsWithSubscribedPrograms_UOM_Test()
        {
            #region "test data"
            var clientId = 87;
            var customerId = "BTD001";
            var accountId = "BTD001a1";
            var premiseId = "BTD001a1p1";
            var billCycleScheduleId = "Cycle01";
            var meterId = "EM001";
            var rateClass = "RG-1";
            var servicePointId = "SP_BTD001a1";
            var serviceContractId = "BTD001a1_BTD001a1p1_1_EM001";
            var program = "Program1";
            var insight1 = "ServiceLevelCostThreshold";
            var insight2 = "ServiceLevelUsageThreshold";
            var insight3 = "BillToDate";
            var insight4 = "CostToDate";
            var insight5 = "AccountProjectedCost";
            var insight6 = "ServiceProjectedCost";
            var insight7 = "Usage";
            var insight8 = "AccountLevelCostThreshold";
            var insight9 = "DayThreshold";
            var insight10 = "ServiceLevelTieredThresholdApproaching";
            var insight11 = "ServiceLevelTieredThresholdExceed";
            var today = DateTime.Today.Date;
            var currentStart = today.AddDays(-6);
            //var currentEnd = currentStart.AddDays(30);
            var lastBillEndDate = currentStart.AddDays(-1);
            var lastBillStartDate = lastBillEndDate.AddDays(-29);


            // mock client settings
            var mockClientSettings = TestHelpers.CreateMockClientSettings();
            _clientConfigFacadeMock.Setup(c => c.GetClientSettings(It.IsAny<int>())).Returns(mockClientSettings);
            // mock billing entities data
            var mockBillingEntity = TestHelpers.CreateMockBillingEntityData(clientId, customerId, accountId, premiseId, serviceContractId,
                servicePointId, meterId, billCycleScheduleId, rateClass, 1000, lastBillStartDate, lastBillEndDate);
            var services = new List<BillingEntity>();
            services.Add(mockBillingEntity);
            _cassandraRepository.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<Expression<Func<BillingEntity, bool>>>()))
                .Returns(services);
            // mock subscription program entity
            var mockProgramSubscription = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, string.Empty, true, 0);
            IList<SubscriptionEntity> programList = new List<SubscriptionEntity>();
            programList.Add(mockProgramSubscription);
            // mock subscription insight entity
            var mockInsight1 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight1, false, 0);
            var mockInsight3 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight3, false, 0);
            var mockInsight4 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight4, false, 0);
            var mockInsight5 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight5, false, 0);
            var mockInsight6 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight6, false, 0);
            var mockInsight7 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight7, false, 0);
            var mockInsight8 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, string.Empty, program, insight8, false, 0);
            var mockInsight9 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight9, false, 0);
            var mockInsight10 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight10, false, 0);
            var mockInsight11 = TestHelpers.CreateMockSubscriptionEntityData(clientId, customerId, accountId, serviceContractId, program, insight11, false, 0);
            IList<SubscriptionEntity> insightList = new List<SubscriptionEntity>();
            insightList.Add(mockInsight1);
            insightList.Add(mockInsight3);
            insightList.Add(mockInsight4);
            insightList.Add(mockInsight5);
            insightList.Add(mockInsight6);
            insightList.Add(mockInsight7);
            insightList.Add(mockInsight8);
            insightList.Add(mockInsight9);
            insightList.Add(mockInsight10);
            insightList.Add(mockInsight11);
            _cassandraRepository.SetupSequence(
                t => t.GetAsync(It.IsAny<string>(),
                It.IsAny<Expression<Func<SubscriptionEntity, bool>>>()))
                .Returns(Task.FromResult(programList))
                .Returns(Task.FromResult(insightList));
            #endregion
            var result = _subscriptionFacadeManager.GetDefaultListWithSubscribedPrograms(clientId, customerId, accountId);

            foreach (var insight in result)
            {
                if (insight.InsightTypeName.ToLower() == insight2.ToLower() || insight.InsightTypeName.ToLower() == insight9)
                    Assert.AreEqual(0, insight.UomId);
            }
        }
    }
}