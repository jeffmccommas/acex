﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Http;
using AO.Business;
using AO.BusinessContracts;
using AO.Entities;
using AutoMapper;
using CE.AO.Models;
using CE.Insights.Controllers.Version1.DataCustodian.ESPI._1_1.Resource;
using CE.Insights.Test.Helpers;
using CE.Models;
using CE.Models.Insights;
using CE.Models.Insights.Types;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace CE.Insights.Test.Controllers.Version1.DataCustodian.ESPI._1_1.Resource
{
    [TestClass]
    public class LocalTimeParametersControllerTest
    {
        private UnityContainer _container;
        private Mock<ITallAMI> _tallAmi;
        private TestInsightsEntitiesMock _insightsEntitesMock;
        private Mock<InsightsEfRepository> _insightRepository;
        private string url = "https://www.greenbutton.com/CE.Insights/api/v1/Datacustodian/espi/1_1/resource/";


        public TestContext TestContext { get; set; }

        [TestInitialize]
        public void TestInit()
        {
            _container = new UnityContainer();
            SetupAutoMapper();

            _tallAmi = new Mock<ITallAMI>();

            _container.RegisterInstance(_tallAmi.Object);

            _insightsEntitesMock = new TestInsightsEntitiesMock();

            _insightRepository = new Mock<InsightsEfRepository>(_insightsEntitesMock.MockInsightsEntities.Object);

        }
        [TestMethod(), TestCategory("GreenButtonConnect"), TestCategory("BVT")]
        [DeploymentItem("TestData")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
"|DataDirectory|\\GetAllLocalTimeParamsTestData.xml", "LocalTimeParamController", DataAccessMethod.Sequential)]
        public void GetLocalTimeParamsTest()
        {
            try
            {
                var clientId = Convert.ToInt32(TestContext.DataRow["clientId"].ToString());
                var accountId = TestContext.DataRow["accountId"].ToString();
                var meterid = TestContext.DataRow["meterid"].ToString();
                var servicePointId = TestContext.DataRow["servicePointId"].ToString();
                var start = TestContext.DataRow["StartDate"].ToString();
                var end = TestContext.DataRow["EndDate"].ToString();
                DateTime? startDate = !string.IsNullOrEmpty(start) ? Convert.ToDateTime(start) : (DateTime?)null;
                DateTime? endDate = !string.IsNullOrEmpty(end) ? Convert.ToDateTime(end) : (DateTime?)null;
                var commodityType = Convert.ToInt32(TestContext.DataRow["commodityType"].ToString());
                var uomid = Convert.ToInt32(TestContext.DataRow["uomid"].ToString());
                var clientSecretCode = TestContext.DataRow["clientSecretCode"].ToString();
                var status = TestContext.DataRow["status"].ToString();

                var meters = new List<string>();
                meters.Add(TestContext.DataRow["meterid"].ToString());

                _insightRepository.Setup(x => x.GetMeterList(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<GreenButtonScope>())).Returns(() => meters);


                var mockAmiEnd = endDate ?? DateTime.UtcNow.Date;
                var mockAmiStart = startDate ?? DateTime.UtcNow.Date;
                var amiMock1 = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterid, servicePointId,
                    mockAmiStart, mockAmiEnd, mockAmiEnd.AddDays(10), commodityType, uomid);
                var readingMock = TestHelpers.CreateMockAmiHourlyReading(mockAmiEnd, mockAmiEnd);
                _tallAmi.Setup(
                    a => a.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<string>(), accountId, clientId))
                    .Returns(amiMock1);
                int testNoOfDays;
                _tallAmi.Setup(
                    m => m.GetReadings(It.IsAny<IEnumerable<TallAmiModel>>(), out testNoOfDays, It.IsAny<bool>()))
                    .Returns(readingMock);


                var controller = new  LocalTimeParametersController(_container, _insightRepository.Object);
                var request = new HttpRequestMessage();

                var authoriaztion = new AuthenticationHeaderValue("bearer", GenerateAccessToken(clientSecretCode));

                request.Headers.Authorization = authoriaztion;
                request.RequestUri = new Uri(url);


                controller.Request = request;
                controller.Request.SetConfiguration(new HttpConfiguration());

                var gbcRequest = new GreenButtonConnectRequest();

                var result = controller.GetLocalTimeParams(gbcRequest);
                Assert.IsNotNull(result);
                if (status == "NoError")
                {
                    Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
                    Assert.AreEqual("application/atom+xml", result.Content.Headers.ContentType.MediaType);
                }
                else
                {
                    Assert.AreEqual(HttpStatusCode.BadRequest, result.StatusCode);
                }
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod(), TestCategory("GreenButtonConnect"), TestCategory("BVT")]
        [DeploymentItem("TestData")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
"|DataDirectory|\\GetLocalTimeParamTestData.xml", "LocalTimeParamController", DataAccessMethod.Sequential)]
        public void GetLocalTimeParamTest()
        {
            try
            {
                var clientId = Convert.ToInt32(TestContext.DataRow["clientId"].ToString());
                var accountId = TestContext.DataRow["accountId"].ToString();
                var meterid = TestContext.DataRow["meterid"].ToString();
                var servicePointId = TestContext.DataRow["servicePointId"].ToString();
                var localTimeParamId = Convert.ToInt32(TestContext.DataRow["localtTimeParamId"].ToString());
                var start = TestContext.DataRow["StartDate"].ToString();
                var end = TestContext.DataRow["EndDate"].ToString();
                DateTime? startDate = !string.IsNullOrEmpty(start) ? Convert.ToDateTime(start) : (DateTime?)null;
                DateTime? endDate = !string.IsNullOrEmpty(end) ? Convert.ToDateTime(end) : (DateTime?)null;
                var commodityType = Convert.ToInt32(TestContext.DataRow["commodityType"].ToString());
                var uomid = Convert.ToInt32(TestContext.DataRow["uomid"].ToString());
                var clientSecretCode = TestContext.DataRow["clientSecretCode"].ToString();
                var status = TestContext.DataRow["status"].ToString();

                var mockAmiEnd = endDate ?? DateTime.UtcNow.Date;
                var mockAmiStart = startDate ?? DateTime.UtcNow.Date;
                var amiMock1 = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterid, servicePointId,
                    mockAmiStart, mockAmiEnd, mockAmiEnd.AddDays(10), commodityType, uomid);
                var readingMock = TestHelpers.CreateMockAmiHourlyReading(mockAmiEnd, mockAmiEnd);
                _tallAmi.Setup(
                    a => a.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<string>(), accountId, clientId))
                    .Returns(amiMock1);
                int testNoOfDays;
                _tallAmi.Setup(
                    m => m.GetReadings(It.IsAny<IEnumerable<TallAmiModel>>(), out testNoOfDays, It.IsAny<bool>()))
                    .Returns(readingMock);
                var meters = new List<string>();
                meters.Add(meterid);
                _insightRepository.Setup(x => x.GetMeterList(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<GreenButtonScope>())).Returns(() => meters);


                var controller = new LocalTimeParametersController(_container, _insightRepository.Object);
                var request = new HttpRequestMessage();

                var authoriaztion = new AuthenticationHeaderValue("bearer", GenerateAccessToken(clientSecretCode));

                request.Headers.Authorization = authoriaztion;
                request.RequestUri = new Uri(url);


                controller.Request = request;
                controller.Request.SetConfiguration(new HttpConfiguration());

                var gbcRequest = new GreenButtonConnectRequest();

                var result = controller.GetLocalTimeParam(gbcRequest, localTimeParamId);
                Assert.IsNotNull(result);
                if (status == "NoError")
                {
                    Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
                    Assert.AreEqual("application/atom+xml", result.Content.Headers.ContentType.MediaType);
                }
                else
                {
                    Assert.AreEqual(HttpStatusCode.BadRequest, result.StatusCode);
                }
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        private void SetupAutoMapper()
        {
            Mapper.CreateMap<RateModel.Reading, ConsumptionData>()
                 .ForMember(dest => dest.DateTime, opts => opts.MapFrom(src => src.Timestamp))
                 .ForMember(dest => dest.Value,
                     opts =>
                         opts.MapFrom(
                             src => src.TimeOfUse == RateModel.Enums.TimeOfUse.Undefined ? (decimal)src.Quantity : 0))
                 .ForAllUnmappedMembers(o => o.Ignore());

            Mapper.CreateMap<RateModel.Reading, ConsumptionTouData>()
                .ForMember(dest => dest.Value, opts => opts.MapFrom(src => (decimal)src.Quantity))
                .ForMember(dest => dest.TouKey,
                    opts =>
                        opts.MapFrom(
                            src =>
                                src.TimeOfUse == RateModel.Enums.TimeOfUse.Undefined
                                    ? TimeOfUseType.NoTou.ToString().ToLower()
                                    : src.TimeOfUse.ToString().ToLower()))
                .ForAllUnmappedMembers(o => o.Ignore());


            Mapper.CreateMap<ConsumptionByMeterEntity, TallAmiModel>()
    .ForAllUnmappedMembers(o => o.Ignore());

            Mapper.CreateMap<TallAmiModel, ConsumptionByMeterEntity>()
                .ForAllUnmappedMembers(o => o.Ignore());
        }

        private string GenerateAccessToken(string plainText)
        {
            //var plainText = "client_id,secret";
            var enrypted = GreenButtonConnect.Helper.Encrypt(plainText);
            var plainTextBytes = Encoding.UTF8.GetBytes(enrypted);
            return Convert.ToBase64String(plainTextBytes);
        }
    }
}
