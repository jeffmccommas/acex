﻿using AO.Business;
using AO.BusinessContracts;
using AO.Entities;
using AutoMapper;
using CE.AO.Models;
using CE.GreenButtonConnect;
using CE.Insights.Controllers.Version1;
using CE.Insights.Test.Helpers;
using CE.Models;
using CE.Models.Insights;
using CE.Models.Insights.Types;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace CE.Insights.Test.Controllers.Version1.DataCustodian.ESPI._1_1.Resource
{
    [TestClass()]
    public class UsagePointControllerTests
    {

        private UnityContainer _container;
        private Mock<ITallAMI> _tallAmi;
        private TestInsightsEntitiesMock _insightsEntitesMock;
        private Mock<InsightsEfRepository> _insightRepository;
        private string url = "https://www.greenbutton.com/CE.Insights/api/v1/Datacustodian/espi/1_1/resource/";


        public TestContext TestContext { get; set; }

        [TestInitialize]
        public void TestInit()
        {
            _container = new UnityContainer();
            SetupAutoMapper();

            _tallAmi = new Mock<ITallAMI>();

            _container.RegisterInstance(_tallAmi.Object);

            _insightsEntitesMock = new TestInsightsEntitiesMock();

            _insightRepository = new Mock<InsightsEfRepository>(_insightsEntitesMock.MockInsightsEntities.Object);

            var r = new CustomerInfoResponse();
            r.ClientId = 87;
            r.Customer = new CustomerInfo();
            r.Customer.Accounts = new List<CustomerInfoAccount>();

            //var cia = new CustomerInfoAccount();
            //cia.Id = "123";
            //r.Customer.Accounts.Add(cia);

            //_insightRepository.Setup(x => x.GetCustomerInfo(It.IsAny<int>(), It.IsAny<CustomerInfoRequest>())).Returns(() => r);

            var fb = new GreenButtonFunctionBlockType();
            fb = GreenButtonFunctionBlockType.IntervalElecMetering;
            var gbc = new GreenButtonScope();
            gbc.FunctionBlocks = new List<GreenButtonFunctionBlockType>();
            gbc.FunctionBlocks.Add(fb);

            var meters = new List<string>();
            meters.Add("meter1");
            meters.Add("meter2");

            _insightRepository.Setup(x => x.GetMeterList(It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<GreenButtonScope>())).Returns(() => meters);

        }

        [TestMethod(), TestCategory("GreenButtonConnect"), TestCategory("BVT")]

        public void GetSubscriptionUsagePointsTest()
        {
            try
            {
                var clientId = 87;
                var accountId = "account1";
                var meterid1 = "meter1";
                var meterid2 = "meter2";
                var servicePointId = "sp2";
                var commodityType = 1;
                var uomid = 0;
                var start = String.Empty;
                var end = String.Empty;
                var subscriptionId = 1;

                for (var i = 1; i < 4; i++)
                {
                    switch (i)
                    {
                        case 1:
                            start = String.Empty;
                            end = String.Empty;
                            break;
                        case 2:
                            start = "1/1/2017";
                            end = "1/1/2017";
                            break;
                        case 3:
                            start = "1/1/2014";
                            end = "1/1/2017";
                            break;

                    }

                    DateTime? startDate = !string.IsNullOrEmpty(start) ? Convert.ToDateTime(start) : (DateTime?) null;
                    DateTime? endDate = !string.IsNullOrEmpty(end) ? Convert.ToDateTime(end) : (DateTime?) null;
                    var mockAmiEnd = endDate ?? DateTime.UtcNow.Date;
                    var mockAmiStart = startDate ?? DateTime.UtcNow.Date;
                    var amiMock1 = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterid1, servicePointId,
                        mockAmiStart, mockAmiEnd, mockAmiEnd.AddDays(10), commodityType, uomid);
                    var amiMock2 = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterid2, servicePointId,
                        mockAmiStart, mockAmiEnd, mockAmiEnd.AddDays(10), commodityType, uomid);
                    var readingMock = TestHelpers.CreateMockAmiHourlyReading(mockAmiEnd, mockAmiEnd);
                    _tallAmi.Setup(a => a.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), meterid1, accountId, clientId))
                        .Returns(amiMock1)
                        .Verifiable();
                    _tallAmi.Setup(a => a.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), meterid2, accountId, clientId))
                       .Returns(amiMock2)
                       .Verifiable();
                    int testNoOfDays;
                    _tallAmi.Setup(
                        m => m.GetReadings(It.IsAny<IEnumerable<TallAmiModel>>(), out testNoOfDays, It.IsAny<bool>()))
                        .Returns(readingMock);

                    var controller = new UsagePointController(_container, _insightRepository.Object);
                    var request = new HttpRequestMessage();

                    var authoriaztion = new AuthenticationHeaderValue("bearer", GenerateAccessToken("client_id,secretEncrypted"));

                    request.Headers.Authorization = authoriaztion;
                    request.RequestUri = new Uri(url);


                    controller.Request = request;
                    var gbcRequest = new GreenButtonConnectRequest();

                    var result = controller.GetSubscriptionUsagePoints(gbcRequest, subscriptionId);

                    Assert.IsNotNull(result);
                    Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
                    Assert.AreEqual("application/atom+xml", result.Content.Headers.ContentType.MediaType);
                }

                
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod(), TestCategory("GreenButtonConnect"), TestCategory("BVT")]
        public void GetBatchSubscriptionUsagePointsTest()
        {
            try
            {
                var clientId = 87;
                var accountId = "account1";
                var meterid1 = "meter1";
                var meterid2 = "meter2";
                var servicePointId = "sp2";
                var commodityType = 1;
                var uomid = 0;
                var start = String.Empty;
                var end = String.Empty;
                var subscriptionId = 1;

                for (var i = 1; i < 4; i++)
                {
                    switch (i)
                    {
                        case 1:
                            start = String.Empty;
                            end = String.Empty;
                            break;
                        case 2:
                            start = "1/1/2017";
                            end = "1/1/2017";
                            break;
                        case 3:
                            start = "1/1/2014";
                            end = "1/1/2017";
                            break;

                    }

                    DateTime? startDate = !string.IsNullOrEmpty(start) ? Convert.ToDateTime(start) : (DateTime?) null;
                    DateTime? endDate = !string.IsNullOrEmpty(end) ? Convert.ToDateTime(end) : (DateTime?) null;

                    var mockAmiEnd = endDate ?? DateTime.UtcNow.Date;
                    var mockAmiStart = startDate ?? DateTime.UtcNow.Date;
                    var amiMock1 = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterid1, servicePointId,
                        mockAmiStart, mockAmiEnd, mockAmiEnd.AddDays(10), commodityType, uomid);
                    var amiMock2 = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterid2, servicePointId,
                        mockAmiStart, mockAmiEnd, mockAmiEnd.AddDays(10), commodityType, uomid);
                    var readingMock = TestHelpers.CreateMockAmiHourlyReading(mockAmiEnd, mockAmiEnd);
                    _tallAmi.Setup(a => a.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), meterid1, accountId, clientId))
                        .Returns(amiMock1)
                        .Verifiable();
                    _tallAmi.Setup(a => a.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), meterid2, accountId, clientId))
                        .Returns(amiMock2)
                        .Verifiable();
                    int testNoOfDays;
                    _tallAmi.Setup(
                        m => m.GetReadings(It.IsAny<IEnumerable<TallAmiModel>>(), out testNoOfDays, It.IsAny<bool>()))
                        .Returns(readingMock);

                    var controller = new UsagePointController(_container, _insightRepository.Object);
                    var request = new HttpRequestMessage();

                    var authoriaztion = new AuthenticationHeaderValue("bearer", GenerateAccessToken("client_id,secretEncrypted"));

                    request.Headers.Authorization = authoriaztion;
                    request.RequestUri = new Uri(url);


                    controller.Request = request;
                    var gbcRequest = new GreenButtonConnectRequest();

                    var result = controller.GetBatchSubscriptionUsagePoints(gbcRequest, subscriptionId);

                    Assert.IsNotNull(result);
                    _tallAmi.Verify();
                    Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
                    Assert.AreEqual("application/atom+xml", result.Content.Headers.ContentType.MediaType);
                }
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        
        }
        [TestMethod(), TestCategory("GreenButtonConnect"), TestCategory("BVT")]
        public void GetBatchSubscriptionUsagePointTest()
        {
            try
            {
                var clientId = 87;
                var accountId = "account1";
                var meterid = "meter1";
                var encrytedMeterid = Helper.ConvertStringToHex(meterid, Encoding.Unicode);
                var servicePointId = "sp2";
                var commodityType = 1;
                var uomid = 0;
                var start = String.Empty;
                var end = String.Empty;
                var subscriptionId = 1;

                for (var i = 1; i < 4; i++)
                {
                    switch (i)
                    {
                        case 1:
                            start = String.Empty;
                            end = String.Empty;
                            break;
                        case 2:
                            start = "1/1/2017";
                            end = "1/1/2017";
                            break;
                        case 3:
                            start = "1/1/2014";
                            end = "1/1/2017";
                            break;

                    }

                    DateTime? startDate = !string.IsNullOrEmpty(start) ? Convert.ToDateTime(start) : (DateTime?) null;
                    DateTime? endDate = !string.IsNullOrEmpty(end) ? Convert.ToDateTime(end) : (DateTime?) null;

                    var mockAmiEnd = endDate ?? DateTime.UtcNow.Date;
                    var mockAmiStart = startDate ?? DateTime.UtcNow.Date;
                    var amiMock = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterid, servicePointId,
                        mockAmiStart, mockAmiEnd, mockAmiEnd.AddDays(10), commodityType, uomid);
                    var readingMock = TestHelpers.CreateMockAmiHourlyReading(mockAmiEnd, mockAmiEnd);
                    _tallAmi.Setup(a => a.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), meterid, accountId, clientId))
                        .Returns(amiMock)
                        .Verifiable();
                    int testNoOfDays;
                    _tallAmi.Setup(
                        m => m.GetReadings(It.IsAny<IEnumerable<TallAmiModel>>(), out testNoOfDays, It.IsAny<bool>()))
                        .Returns(readingMock);

                    var controller = new UsagePointController(_container, _insightRepository.Object);
                    var request = new HttpRequestMessage();

                    var authoriaztion = new AuthenticationHeaderValue("bearer", GenerateAccessToken("client_id,secretEncrypted"));

                    request.Headers.Authorization = authoriaztion;
                    request.RequestUri = new Uri(url);


                    controller.Request = request;
                    var gbcRequest = new GreenButtonConnectRequest();

                    var result = controller.GetBatchSubscriptionUsagePoint(gbcRequest, subscriptionId, encrytedMeterid);

                    Assert.IsNotNull(result);
                    _tallAmi.Verify();
                    Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
                    Assert.AreEqual("application/atom+xml", result.Content.Headers.ContentType.MediaType);
                }
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod(), TestCategory("GreenButtonConnect"), TestCategory("BVT")]
        public void GetSubscriptionUsagePointTest()
        {
            try
            {
                var clientId = 87;
                var accountId = "account1";
                var meterid = "meter1";
                var encrytedMeterid = Helper.ConvertStringToHex(meterid, Encoding.Unicode);
                var servicePointId = "sp2";
                var commodityType = 1;
                var uomid = 0;
                var start = String.Empty;
                var end = String.Empty;
                var subscriptionId = 1;

                for (var i = 1; i < 4; i++)
                {
                    switch (i)
                    {
                        case 1:
                            start = String.Empty;
                            end = String.Empty;
                            break;
                        case 2:
                            start = "1/1/2017";
                            end = "1/1/2017";
                            break;
                        case 3:
                            start = "1/1/2014";
                            end = "1/1/2017";
                            break;

                    }

                    DateTime? startDate = !string.IsNullOrEmpty(start) ? Convert.ToDateTime(start) : (DateTime?) null;
                    DateTime? endDate = !string.IsNullOrEmpty(end) ? Convert.ToDateTime(end) : (DateTime?) null;

                    var mockAmiEnd = endDate ?? DateTime.UtcNow.Date;
                    var mockAmiStart = startDate ?? DateTime.UtcNow.Date;
                    var amiMock = TestHelpers.CreateMockAmiHourlyModel(clientId, accountId, meterid, servicePointId,
                        mockAmiStart, mockAmiEnd, mockAmiEnd.AddDays(10), commodityType, uomid);
                    var readingMock = TestHelpers.CreateMockAmiHourlyReading(mockAmiEnd, mockAmiEnd);
                    _tallAmi.Setup(a => a.GetAmiList(It.IsAny<DateTime>(), It.IsAny<DateTime>(), meterid, accountId, clientId))
                        .Returns(amiMock)
                        .Verifiable();
                    int testNoOfDays;
                    _tallAmi.Setup(
                        m => m.GetReadings(It.IsAny<IEnumerable<TallAmiModel>>(), out testNoOfDays, It.IsAny<bool>()))
                        .Returns(readingMock);

                    var controller = new UsagePointController(_container, _insightRepository.Object);
                    var request = new HttpRequestMessage();

                    var authoriaztion = new AuthenticationHeaderValue("bearer", GenerateAccessToken("client_id,secretEncrypted"));

                    request.Headers.Authorization = authoriaztion;
                    request.RequestUri = new Uri(url);


                    controller.Request = request;
                    var gbcRequest = new GreenButtonConnectRequest();

                    var result = controller.GetSubscriptionUsagePoint(gbcRequest, subscriptionId, encrytedMeterid);

                    Assert.IsNotNull(result);
                    Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
                    Assert.AreEqual("application/atom+xml", result.Content.Headers.ContentType.MediaType);
                }
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        
        private void SetupAutoMapper()
        {
            Mapper.CreateMap<RateModel.Reading, ConsumptionData>()
                 .ForMember(dest => dest.DateTime, opts => opts.MapFrom(src => src.Timestamp))
                 .ForMember(dest => dest.Value,
                     opts =>
                         opts.MapFrom(
                             src => src.TimeOfUse == RateModel.Enums.TimeOfUse.Undefined ? (decimal)src.Quantity : 0))
                 .ForAllUnmappedMembers(o => o.Ignore());

            Mapper.CreateMap<RateModel.Reading, ConsumptionTouData>()
                .ForMember(dest => dest.Value, opts => opts.MapFrom(src => (decimal)src.Quantity))
                .ForMember(dest => dest.TouKey,
                    opts =>
                        opts.MapFrom(
                            src =>
                                src.TimeOfUse == RateModel.Enums.TimeOfUse.Undefined
                                    ? TimeOfUseType.NoTou.ToString().ToLower()
                                    : src.TimeOfUse.ToString().ToLower()))
                .ForAllUnmappedMembers(o => o.Ignore());


            Mapper.CreateMap<ConsumptionByMeterEntity, TallAmiModel>()
    .ForAllUnmappedMembers(o => o.Ignore());

            Mapper.CreateMap<TallAmiModel, ConsumptionByMeterEntity>()
                .ForAllUnmappedMembers(o => o.Ignore());
        }

        private string GenerateAccessToken(string plainText)
        {
            //var plainText = "client_id,secret";
            var enrypted = Helper.Encrypt(plainText);
            var plainTextBytes = Encoding.UTF8.GetBytes(enrypted);
            return Convert.ToBase64String(plainTextBytes);
        }
    }
}