﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Http;
using AO.Business;
using AO.BusinessContracts;
using AO.Entities;
using AutoMapper;
using CE.AO.Models;
using CE.Insights.Controllers.Version1.DataCustodian.ESPI._1_1.Resource;
using CE.Insights.Test.Helpers;
using CE.Models;
using CE.Models.Insights;
using CE.Models.Insights.Types;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace CE.Insights.Test.Controllers.Version1.DataCustodian.ESPI._1_1.Resource
{
    [TestClass]
    public class ReadServiceStatusControllerTest
    {
        private UnityContainer _container;
        private Mock<ITallAMI> _tallAmi;
        private TestInsightsEntitiesMock _insightsEntitesMock;
        private InsightsEfRepository _insightRepository;
        private string url = "https://www.greenbutton.com/CE.Insights/api/v1/Datacustodian/espi/1_1/resource/";


        public TestContext TestContext { get; set; }

        [TestInitialize]
        public void TestInit()
        {
            _container = new UnityContainer();
            SetupAutoMapper();

            _tallAmi = new Mock<ITallAMI>();

            _container.RegisterInstance(_tallAmi.Object);

            _insightsEntitesMock = new TestInsightsEntitiesMock();

            _insightRepository = new InsightsEfRepository(_insightsEntitesMock.MockInsightsEntities.Object);

        }

        [TestMethod(), TestCategory("GreenButtonConnect"), TestCategory("BVT")]
        [DeploymentItem("TestData")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
"|DataDirectory|\\GetSerivceStatusTestData.xml", "ServiceStatus", DataAccessMethod.Sequential)]
        public void ReadServiceStatusTest()
        {
            try
            {
                var clientSecretCode = TestContext.DataRow["clientSecretCode"].ToString();

                var controller = new ReadServiceStatusController(_container, _insightRepository);
                var request = new HttpRequestMessage();

                var authoriaztion = new AuthenticationHeaderValue("bearer", GenerateAccessToken(clientSecretCode));

                request.Headers.Authorization = authoriaztion;
                request.RequestUri = new Uri(url);


                controller.Request = request;
                controller.Request.SetConfiguration(new HttpConfiguration());

                var gbcRequest = new GreenButtonConnectRequest();

                var result = controller.ReadServiceStatus(gbcRequest);
                Assert.IsNotNull(result);
                Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
                Assert.AreEqual("application/atom+xml", result.Content.Headers.ContentType.MediaType);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        private void SetupAutoMapper()
        {
            Mapper.CreateMap<RateModel.Reading, ConsumptionData>()
                 .ForMember(dest => dest.DateTime, opts => opts.MapFrom(src => src.Timestamp))
                 .ForMember(dest => dest.Value,
                     opts =>
                         opts.MapFrom(
                             src => src.TimeOfUse == RateModel.Enums.TimeOfUse.Undefined ? (decimal)src.Quantity : 0))
                 .ForAllUnmappedMembers(o => o.Ignore());

            Mapper.CreateMap<RateModel.Reading, ConsumptionTouData>()
                .ForMember(dest => dest.Value, opts => opts.MapFrom(src => (decimal)src.Quantity))
                .ForMember(dest => dest.TouKey,
                    opts =>
                        opts.MapFrom(
                            src =>
                                src.TimeOfUse == RateModel.Enums.TimeOfUse.Undefined
                                    ? TimeOfUseType.NoTou.ToString().ToLower()
                                    : src.TimeOfUse.ToString().ToLower()))
                .ForAllUnmappedMembers(o => o.Ignore());


            Mapper.CreateMap<ConsumptionByMeterEntity, TallAmiModel>()
    .ForAllUnmappedMembers(o => o.Ignore());

            Mapper.CreateMap<TallAmiModel, ConsumptionByMeterEntity>()
                .ForAllUnmappedMembers(o => o.Ignore());
        }

        private string GenerateAccessToken(string plainText)
        {
            //var plainText = "client_id,secret";
            var enrypted = GreenButtonConnect.Helper.Encrypt(plainText);
            var plainTextBytes = Encoding.UTF8.GetBytes(enrypted);
            return Convert.ToBase64String(plainTextBytes);
        }
    }
}
