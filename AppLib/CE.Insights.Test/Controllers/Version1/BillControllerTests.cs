﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CE.Insights.Controllers.Version1;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;
using CE.Infrastructure;
using CE.Insights.Test.Helpers;
using CE.Models;
using CE.Models.Insights;
using Moq;

// ReSharper disable once CheckNamespace
namespace CE.Insights.Test
{
    [TestClass()]
    public class BillControllerTests
    {
        private const string ClientSpace = "netnln2itvho";
        private const string ClientAccessToken = "c58ec8d495044174267c257f105914f7fb9567127a879b2419e1c3d405a54be2";

        private BillController _billController;
        private int _clientId = 87;
        private Mock<IInsightsEFRepository> _mockInsightsRepository;
        protected readonly MockRepository MockRepository = new MockRepository(MockBehavior.Strict);

        [TestInitialize]
        public void TestInitialize()
        {
            _mockInsightsRepository = MockRepository.Create<IInsightsEFRepository>();
            _billController = new BillController(_mockInsightsRepository.Object) { Request = CreateRequestMessage() };
        }
        [TestMethod()]
        public void Billv1_Get_Test()
        {
            var customerId = "getBill";
            var accountId = "getBill";
            var startDate = DateTime.Today.Date.AddDays(-60);
            var endDate = DateTime.Today.Date;
            var count = 1;
            var premiseId = "getBill";
            var serviceId = "getBill";
            var rateClass = "RG-1";
            var uomKey = "kwh";
            var commodityKey = "electric";
            var addr1 = "16 Laurel Ave";
            var city = "Wellesley";
            var state = "MA";
            var zip = "02481";
            var billDays = 30;
            var firstname = "TestFirst";
            var lastname = "TestLast";


            var billRequest = new BillRequest
            {
                CustomerId =  customerId,
                AccountId = accountId,
                Count = count,
                EndDate = endDate,
                StartDate = startDate,
                IncludeContent = false
            };

            var mockBillResponse = TestGreenButtonHelper.GetMockSingleServiceSinglePremiseSingleAccountData(_clientId, customerId, accountId,
                premiseId, serviceId, commodityKey, uomKey, billDays, rateClass, addr1, city, state, zip, firstname,
                lastname);
            _mockInsightsRepository.Setup(m => m.GetBills(_clientId, billRequest)).Returns(mockBillResponse);

            var result = _billController.Get(billRequest);

            Assert.IsNotNull(result);
            MockRepository.VerifyAll();
        }


        private ClientUser GetClientUser()
        {
            return new ClientUser
            {
                ClientID = _clientId,
                ClientProperties = new List<ClientProperties>
                {
                    new ClientProperties((PropertyType) Convert.ToInt32("0"), ClientAccessToken),
                    new ClientProperties((PropertyType) Convert.ToInt32("1"), ClientSpace)
                }
            };
        }

        private HttpRequestMessage CreateRequestMessage()
        {
            var request = new HttpRequestMessage();
            request.Properties["MS_HttpConfiguration"] = new HttpConfiguration();
            request.Properties[CEConfiguration.CEClientUser] = GetClientUser();

            return request;
        }
    }
}