﻿<?xml version="1.0" encoding="utf-8"?>
<configuration>
  <configSections>
    <section name="nlog" type="NLog.Config.ConfigSectionHandler, NLog" />
    <section name="azureRedisCache" type="CE.ContentModel.ContentCache.AzureRedisCacheSection, CE.ContentModel" />
  </configSections>
  <startup>
    <supportedRuntime version="v4.0" sku=".NETFramework,Version=v4.6" />
  </startup>
  <connectionStrings>
    <!--<add name="InsightsMetaDataConn" providerName="System.Data.SqlClient" connectionString="Server=tcp:acedsql1c0.database.windows.net,1433;Database=InsightsMetaData;User ID=Aclweb@acedsql1c0;Password=Acl@r@393;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;Min Pool Size=5;"/>-->

    <add name="InsightsMetaDataConn" providerName="System.Data.SqlClient" connectionString="Server=tcp:acepsql1c0.database.windows.net,1433;Database=InsightsMetaData;User ID=AclaraCEAdmin@acepsql1c0;Password=0S3att!30;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;Min Pool Size=5;"/>
    <!-- The format of the connection string is "DefaultEndpointsProtocol=https;AccountName=NAME;AccountKey=KEY" -->
    <!-- For local execution, the value can be set either in this config file or through environment variables -->
    <add name="AzureWebJobsDashboard" connectionString="" />
    <add name="AzureWebJobsStorage" connectionString="" />
  </connectionStrings>
  <appSettings>
    <add key="Environment" value="prod" />
    <add key="ProcessingEnabled" value="true" />
    <add key="ContentCacheProvider" value="RedisAzureCache" />
  </appSettings>
  <nlog xmlns="http://www.nlog-project.org/schemas/NLog.xsd"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <targets>
      <default-wrapper xsi:type="AsyncWrapper" overflowAction="Grow" queueLimit="50000">
      </default-wrapper>
      <target name="database" type="Database" connectionStringName="InsightsMetaDataConn" keepConnection="True" commandText="exec audit.WriteNotificationEvent @Type,@ClientId,@Severity,@Category,@RequestData,@ResponseData,@Message,@AdditionalInfo,@Source,@EnvironmentName">
        <parameter name="@Type" layout="${level}"/>
        <parameter name="@ClientId" layout="${event-context:item=ClientId}"/>
        <parameter name="@Severity" layout="${event-context:item=Severity}"/>
        <parameter name="@Category"	layout="${event-context:item=Category}"/>
        <parameter name="@RequestData" layout="${event-context:item=RequestData}"/>
        <parameter name="@ResponseData" layout="${event-context:item=ResponseData}"/>
        <parameter name="@Message"	layout="${event-context:item=Message}"/>
        <parameter name="@AdditionalInfo"	layout="${event-context:item=AdditionalInfo}"/>
        <parameter name="@Source" layout="${event-context:item=Source}"/>
        <parameter name="@EnvironmentName"	layout="${event-context:item=EnvironmentName}"/>
      </target>
    </targets>
    <targets>
      <target name="email"
  smtpServer="smtp.sendgrid.net"
  smtpPort="587"
  smtpAuthentication="Basic"
  smtpUserName="aclaraprod"
  smtpPassword="Xaclaraprod2311X"
  enableSsl="true"
  from="ACE_Prod@aclarax.com"
  to="CEInsightsNotifications@aclaratech.com"
  subject="${level} ${event-context:item=EnvironmentName} ${event-context:item=Severity}: ${event-context:item=Source}"
  body="ClientID: ${event-context:item=ClientId} ${newline} Category:${event-context:item=Category} Message: ${event-context:item=Message} ${newline} AdditionalInfo: ${event-context:item=AdditionalInfo}"
  xsi:type="Mail"
          />
    </targets>
    <rules>
      <logger name="database" minlevel="Trace" writeTo="database" />
      <logger name="email" minlevel="Trace" writeTo="email" />
    </rules>
  </nlog>
  <system.diagnostics>
    <trace autoflush="true" />
    <sources>
      <source name="System.Net" switchValue="Information, ActivityTracing">
        <listeners>
          <add name="System.Net"/>
        </listeners>
      </source>
      <source name="System.ServiceModel"
					switchValue="Information, ActivityTracing"
					propagateActivity="true" >
        <listeners>
          <add name="System.Net"/>
        </listeners>
      </source>
      <source name="System.ServiceModel.MessageLogging">
        <listeners>
          <add name="System.Net"/>
        </listeners>
      </source>
      <source name="myUserTraceSource" switchValue="Information, ActivityTracing">
        <listeners>
          <add name="System.Net"/>
        </listeners>
      </source>
      <source name="System.Net.HttpListener">
        <listeners>
          <add name="System.Net"/>
        </listeners>
      </source>
      <source name="System.Net.Sockets">
        <listeners>
          <add name="System.Net"/>
        </listeners>
      </source>
      <source name="System.Net.Cache">
        <listeners>
          <add name="System.Net"/>
        </listeners>
      </source>
    </sources>
    <sharedListeners>
      <add
       name="System.Net"
       type="System.Diagnostics.TextWriterTraceListener"
        initializeData="D:\home\data\jobs\triggered\SystemNet\System.Net.trace.log"
       traceOutputOptions="DateTime" />
    </sharedListeners>
    <switches>
      <add name="System.Net" value="Verbose" />
      <add name="System.Net.Sockets" value="Verbose" />
      <add name="System.Net.Cache" value="Verbose" />
      <add name="System.Net.HttpListener" value="Verbose" />
    </switches>
  </system.diagnostics>
  <system.serviceModel>
    <diagnostics wmiProviderEnabled="false">
      <messageLogging logEntireMessage="true" logMalformedMessages="true" logMessagesAtServiceLevel="true" logMessagesAtTransportLevel="true" maxMessagesToLog="3000"/>
    </diagnostics>
    <bindings>
      <basicHttpsBinding>
        <binding name="HTTPSEndPointBindingImplServiceSoapBinding" closeTimeout="00:02:00" openTimeout="00:02:00" receiveTimeout="00:02:00" sendTimeout="00:02:00">
          <security mode="Transport">
            <transport clientCredentialType="Basic" />
          </security>
        </binding>
      </basicHttpsBinding>
      <basicHttpBinding>
        <binding name="EPAPISoapBinding" closeTimeout="00:02:00" openTimeout="00:02:00" receiveTimeout="00:02:00" sendTimeout="00:02:00">
          <security mode="Transport" />
        </binding>
        <binding name="EPAPISoapBinding1" closeTimeout="00:02:00" openTimeout="00:02:00" receiveTimeout="00:02:00" sendTimeout="00:02:00" />
      </basicHttpBinding>
    </bindings>
    <client>
      <!--<endpoint address="https://lrstrain.pjm.com/ews" binding="basicHttpsBinding"
        bindingConfiguration="HTTPSEndPointBindingImplServiceSoapBinding"
        contract="ElrsServiceReference.HTTPEndPointBinding" name="HTTPEndPointBindingImplPort"/>-->
      <endpoint address="https://profiles.beta.vrli.com/WebService/EPAPI_1.2/soap.soap"
                binding="basicHttpBinding" bindingConfiguration="EPAPISoapBinding"
                contract="NuanceServiceReference.EPAPIPortType" name="EPAPI" />
    </client>
  </system.serviceModel>
  <azureRedisCache>
    <azureRedisCacheConfigurationOptions
      EndPoint="AclAceSharedCacheProd.redis.cache.windows.net"
      Password="ecrGBsXvF0cMzbMKwtxLcJsIGnZm48r9GR7XFmk+s38="
      Ssl="true"
      ConnectRetry="4"
      ConnectTimeout="30000"
      SyncTimeout="10000">
    </azureRedisCacheConfigurationOptions>
  </azureRedisCache>
  <startup>
    <supportedRuntime version="v4.0" sku=".NETFramework,Version=v4.5" />
  </startup>
  <runtime>
    <assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1">
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.WindowsAzure.Storage" publicKeyToken="31bf3856ad364e35" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-4.2.1.0" newVersion="4.2.1.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Newtonsoft.Json" publicKeyToken="30ad4fe6b2a6aeed" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-6.0.0.0" newVersion="6.0.0.0" />
      </dependentAssembly>
    </assemblyBinding>
  </runtime>
</configuration>