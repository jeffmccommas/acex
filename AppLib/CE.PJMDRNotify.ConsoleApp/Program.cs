﻿using System;
using System.ComponentModel;
using System.Configuration;
using System.Threading.Tasks;
using System.Threading;
namespace CE.PJMDRNotify.ConsoleApp
{
    class Program
    {
        private const string ConfigurationAppSettingKeyEnvironment = "Environment";
        private const string ConfigurationAppSettingKeyProcessingEnabled = "ProcessingEnabled";

        private const string MsgNotification = "Notification Process";
        private const string EventEnd = "Ends";

        // Please set the following connection strings in app.config for this WebJob to run:
        // AzureWebJobsDashboard and AzureWebJobsStorage
        static void Main()
        {
            string env;

           //try
           // {

                env = ConfigurationManager.AppSettings[ConfigurationAppSettingKeyEnvironment];

                if (AppSettings.Get<bool>(ConfigurationAppSettingKeyProcessingEnabled) == false)
                {
                    Console.WriteLine("Processing has been disabled for this environment. Exited without processing. (Environment: {0})", env);
                    return;
                }

            //await ProcessNotifications()
            //var processNote = ProcessNotifications();
            MainAsync();//.Wait(120000);
            //Console.WriteLine("Exiting");
            //}
            //catch (Exception)
            //{
               
                //throw;
            //}
        }

        static void MainAsync()
        {
            try { 
            //var cts = new CancellationTokenSource();
            //    Task task = Task.Run(async ()=> 
            //    {
                    ProcessNotifications();
                //},cts.Token);
                //cts.CancelAfter(50000);
             //await task;
                //await Task.Run(async () =>
                //{
                //    while (!cts.Token.IsCancellationRequested)
                //    {
                //        await ProcessNotifications();
                //    }

                //});
            // timeout in 50 secs so the next tesk is not blocked
            //var task = ProcessNotifications();//.ConfigureAwait(false);
            //try
            //{
            //    if (await Task.WhenAny(task, Task.Delay(timeout)) == task)
            //    {
            //        Console.WriteLine("success");
            //    }
            //    else
            //    {

            //        Console.WriteLine("timed out");
            //    }//ProcessNotifications().ConfigureAwait(false);
            //}
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
        public static void ProcessNotifications()
        {
            var function = 1;
            var clientId = 233; // hard coded?

            try
            {
                var manager = new DRMS.NotificationManager();

                if (function == 3)
                    manager.PingNuance(clientId);
                else if (function == 2)
                    System.Diagnostics.Debug.WriteLine("don't do anything");
                else
                {
                    manager.ProcessNotifications(clientId);
                    
                    //return true;
                }
            }
            catch (Exception)
            {
                Console.WriteLine("{0} {1}", MsgNotification, EventEnd);
            }
            //return true;
        }
    }
    /// <summary>
    /// Type safe app settings class.
    /// </summary>
    public static class AppSettings
    {
        public static T Get<T>(string key)
        {
            var appSetting = ConfigurationManager.AppSettings[key];
            if (string.IsNullOrWhiteSpace(appSetting)) throw new SettingsPropertyNotFoundException(key);

            var converter = TypeDescriptor.GetConverter(typeof(T));
            return (T)(converter.ConvertFromInvariantString(appSetting));
        }
    }
}
