﻿using System;
using System.Collections.Generic;
using System.Linq;
using CE.InsightsWeb.Common;
using CE.InsightsWeb.Common.AclaraJson;
using CE.InsightsWeb.Common.QueryStringEncryptor;
using CE.InsightsWeb.Models;
using CE.InsightsWeb.Test.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Benchmark = CE.InsightsWeb.Models.Benchmark;
using BillToDate = CE.InsightsWeb.Models.BillToDate;

namespace CE.InsightsWeb.Test
{
    [TestClass]
    public class UnitTests
    {
        private const string CLIENT_ID = "87";
        private const string WATERCLIENT_ID = "61";
        private const string CUSTOMER_ID = "993774751";
        private const string ACCOUNT_ID = "1218337093";
        private const string PREMISE_ID = "1218337000";
        private const string PREMISE_TYPE = "premisetype.residential";
        private const string PREMISE_TYPE_BUSINESS = "premisetype.business";
        private const string SERVICEPOINT_ID = "1";
        private const string USER_ID = "CSR13";
        private const string GROUP_NAME = "CSR";
        private const string ROLE_NAME = "CSRRep";
        private const string LOCALE = "en-US";
        private static string PARAMETERS = "";
        private static string PARAMETERS_BUSINESS = "";
        private static string PARAMETERS_CSR = "";
        private static string PARAMETERS_NOBILL = "";
        private static string PARAMETERS_NONTIER = "";
        private static string CURRENT_CLIENT_ID = "87";
        #region Bills

        [TestMethod]
        public void BillSummaryModelContent()
        {
            const string tabId = "tab.billstatement";
            var obj = new BillModels();
            var res = obj.GetBillSummary(tabId, SetupParameters());
            Assert.AreEqual(res.Title, "");
        }

        [TestMethod]
        public void BillSummaryModelData()
        {
            const string tabId = "tab.billstatement";
            var obj = new BillModels();
            var res = obj.GetBillSummary(tabId, SetupParameters());
            Assert.AreNotEqual(res.AvgDailyCost.Length, 1);
        }

        [TestMethod]
        public void BillHistoryModelContent()
        {
            const string tabId = "tab.billhistory";
            var obj = new BillModels();
            var res = obj.GetBillHistory(tabId, SetupParameters());
            Assert.AreNotEqual(res.Title.Length, 0);
        }

        [TestMethod]
        public void BillHistoryModelData()
        {
            const string tabId = "tab.billhistory";
            var obj = new BillModels();
            var res = obj.GetBillHistory(tabId, SetupParameters());
            Assert.AreNotEqual(res.Bills.Count, 0);
        }
        [TestMethod]
        public void BilledUsageModelContent()
        {
            const string tabId = "tab.myusage";
            var obj = new BillModels();
            var res = obj.GetBilledUsageChart(tabId, SetupParameters());
            Assert.AreNotEqual(res.Title.Length, 0);
        }

        [TestMethod]
        public void BilledUsageModelData()
        {
            const string tabId = "tab.myusage";
            var obj = new BillModels();
            var res = obj.GetBilledUsageChart(tabId, SetupParameters());
            Assert.AreNotEqual(res.BilledUsageLastYear.Count, 0);
        }

        [TestMethod]
        public void BillComparisonModelContent()
        {
            const string tabId = "tab.billcomparison";
            var obj = new BillModels();
            var res = obj.GetBillComparison(tabId, SetupParameters(), null, null);
            Assert.AreNotEqual(res.Title.Length, 0);
            Assert.AreNotEqual(res.ShowTable.Length, 0);
        }

        [TestMethod]
        public void BillComparisonModelData()
        {
            const string tabId = "tab.billcomparison";
            var obj = new BillModels();
            var res = obj.GetBillComparison(tabId, SetupParameters(), null, null);
            Assert.AreNotEqual(res.Dates.Count, 0);
        }

        [TestMethod]
        public void BillToDateModelContent()
        {
            const string tabId = "tab.dashboard";
            var obj = new BillModels();
            var res = obj.GetBillToDate(tabId, SetupParametersBilltoDate());
            Assert.AreNotEqual(res.IntroText.Length, 0);
        }

        [TestMethod]
        public void BillToDateModelData()
        {
            const string tabId = "tab.dashboard";
            var obj = new BillModels();
            var res = obj.GetBillToDate(tabId, SetupParametersBilltoDate());
            Assert.AreNotEqual(res.ElectricUseToDate, 0);
        }
        #endregion
        #region Consumption
        [TestMethod]
        public void ConsumptionModelContent()
        {
            const string tabId = "tab.myusage";
            var obj = new ConsumptionModels();
            var startDate = DateTime.UtcNow.AddYears(-2).AddMonths(1).ToString("yyyy-MM-dd");
            var endDate = DateTime.UtcNow.ToString("yyyy-MM-dd");
            var res = obj.GetConsumption(tabId, SetupParameters(), "month", startDate, endDate, "", false, false);
            Assert.AreNotEqual(res.Title.Length, 0);
        }

        [TestMethod]
        public void ConsumptionModelData()
        {
            const string tabId = "tab.myusage";
            var obj = new ConsumptionModels();
            var startDate = DateTime.UtcNow.AddYears(-2).AddMonths(1).ToString("yyyy-MM-dd");
            var endDate = DateTime.UtcNow.ToString("yyyy-MM-dd");
            var res = obj.GetConsumption(tabId, SetupParameters(), "month", startDate, endDate, "", false, false);
            Assert.AreNotEqual(res.Data.Count, 0);
        }

        [TestMethod]
        public void ConsumptionWeatherModelData()
        {
            const string tabId = "tab.myusage";
            var obj = new ConsumptionModels();
            var startDate = DateTime.UtcNow.AddYears(-2).AddMonths(1).ToString("yyyy-MM-dd");
            var endDate = DateTime.UtcNow.ToString("yyyy-MM-dd");
            var res = obj.GetConsumption(tabId, SetupParameters(), "month", startDate, endDate, "", false, false);
            Assert.AreNotEqual(res.Weather.Count, 0);
        }

        [TestMethod]
        public void ConsumptionTouModelData()
        {
            const string tabId = "tab.myusage";
            var obj = new ConsumptionModels();
            var startDate = DateTime.UtcNow.AddYears(-2).AddMonths(1).ToString("yyyy-MM-dd");
            var endDate = DateTime.UtcNow.ToString("yyyy-MM-dd");
            var res = obj.GetConsumption(tabId, SetupParametersTou(), "month", startDate, endDate, "", false, false);
            Assert.AreNotEqual(res.TouSeriesList.Count, 0);
        }

        [TestMethod]
        public void ConsumptionAverageUseModelData()
        {
            const string tabId = "tab.myusage";
            var obj = new ConsumptionModels();
            var startDate = DateTime.UtcNow.AddYears(-2).AddMonths(1).ToString("yyyy-MM-dd");
            var endDate = DateTime.UtcNow.ToString("yyyy-MM-dd");
            var res = obj.GetConsumption(tabId, SetupParameters(), "month", startDate, endDate, "", false, false);
            Assert.AreNotEqual(res.AverageUsage, 0);
        }

        [TestMethod]
        public void ConsumptionAverageUseModelDataDay()
        {
            const string tabId = "tab.myusage";
            var obj = new ConsumptionModels();
            var startDate = DateTime.UtcNow.AddYears(-2).AddMonths(1).ToString("yyyy-MM-dd");
            var endDate = DateTime.UtcNow.ToString("yyyy-MM-dd");
            var res = obj.GetConsumption(tabId, SetupParametersAvgUsage(), "day", startDate, endDate, "", false, false);
            Assert.AreNotEqual(res.AverageUsage, 0);
        }

        [TestMethod]
        public void ConsumptionAverageUseModelDataHour()
        {
            const string tabId = "tab.myusage";
            var obj = new ConsumptionModels();
            var startDate = DateTime.UtcNow.AddYears(-2).AddMonths(1).ToString("yyyy-MM-dd");
            var endDate = DateTime.UtcNow.ToString("yyyy-MM-dd");
            var res = obj.GetConsumption(tabId, SetupParametersAvgUsage(), "hour", startDate, endDate, "", false, false);
            Assert.AreNotEqual(res.AverageUsage, 0);
        }
        #endregion
        #region PeerComparison

        [TestMethod]
        public void PeerComparisonModelContent()
        {
            const string tabId = "tab.dashboard";
            var obj = new BenchmarkModels();
            var res = obj.GetBenchmark(tabId, SetupParameters());
            Assert.AreNotEqual(res.Title.Length, 0);
        }

        [TestMethod]
        public void PeerComparisonModelData()
        {
            const string tabId = "tab.dashboard";
            var obj = new BenchmarkModels();
            var res = obj.GetBenchmark(tabId, SetupParameters());
            Assert.AreNotEqual(res.Benchmarks.Count, 0);
        }

        /// <summary>
        /// Authored businessefficientqty returned from API when premise type business customer
        /// </summary>
        [TestMethod]
        public void PeerComparisonModelContentBusContent()
        {
            const string tabId = "tab.dashboard";
            var obj = new BenchmarkModels();
            var res = obj.GetBenchmark(tabId, SetupParametersBusiness());
            StringAssert.Contains(res.EfficientQty.ToLower(), "business");
            StringAssert.Contains(res.Footer, "business");
        }

        /// <summary>
        /// Business Benchmark - no benchmark (billdisagg status failed with null customer) 
        /// </summary>
        [TestMethod]
        public void PeerComparisonModelContentBusNoBenchmark()
        {
            const string tabId = "tab.dashboard";
            var obj = new BenchmarkModels();
            var res = obj.GetBenchmark(tabId, SetupParametersNoBenchBusiness());
            Assert.AreEqual(res.NoBenchmarks, true);
            StringAssert.Contains(res.NoBenchmarksText.ToLower(), "business");
        }
        [TestMethod]
        public void PeerComparisonModelContentBusiness()
        {
            const string tabId = "tab.dashboard";
            var obj = new BenchmarkModels();
            var res = obj.GetBenchmark(tabId, SetupParametersBusiness());
            Assert.AreNotEqual(res.YourQty.Length, 0);
        }

        /// <summary>
        /// Verify My cost is not 0 for gas and electric commodity
        ///and efficiencyusage as % of average cost returned
        /// </summary>
        [TestMethod]
        public void PeerComparisonModelBusinessData()
        {
            const string tabId = "tab.dashboard";
            var obj = new BenchmarkModels();
            var res = obj.GetBenchmark(tabId, SetupParametersBusiness());
            Assert.AreNotEqual(res.Benchmarks[0].MyUsage, 0);
            Assert.AreNotEqual(res.Benchmarks[1].MyUsage, 0);
            Assert.AreNotEqual(res.Benchmarks[0].EfficientUsage, 0);
            Assert.IsTrue(res.Benchmarks[0].EfficientUsage < res.Benchmarks[0].MyUsage);
            Assert.IsTrue(res.Benchmarks[1].EfficientUsage < res.Benchmarks[1].MyUsage);
        }
        [TestMethod]
        public void PeerComparisonModelDataBusiness()
        {
            const string tabId = "tab.dashboard";
            var obj = new BenchmarkModels();
            var res = obj.GetBenchmark(tabId, SetupParametersBusiness());
            Assert.AreNotEqual(res.Benchmarks.Count, 0);
        }
        #endregion
        #region Profile
        [TestMethod]
        public void ShortProfileModelContent()
        {
            const string tabId = "tab.dashboard";
            var obj = new ProfileModels();
            var res = obj.GetProfile(tabId, SetupParameters());
            Assert.AreNotEqual(res.Title.Length, 0);
        }

        [TestMethod]
        public void ShortProfileModelData()
        {
            const string tabId = "tab.dashboard";
            var obj = new ProfileModels();
            var res = obj.GetProfile(tabId, SetupParametersBilltoDate());
            Assert.AreNotEqual(res.Tabs[0].Sections[0].Questions.Count, 0);
        }

        [TestMethod]
        public void LongProfileModelContent()
        {
            const string tabId = "tab.energyprofile";
            var obj = new ProfileModels();
            var res = obj.GetLongProfile(tabId, "", SetupParameters());
            Assert.AreNotEqual(res.Title.Length, 0);
        }

        [TestMethod]
        public void LongProfileModelData()
        {
            const string tabId = "tab.energyprofile";
            var obj = new ProfileModels();
            var res = obj.GetLongProfile(tabId, "", SetupParameters());
            Assert.AreNotEqual(res.Tabs.Count, 0);
        }
        [TestMethod]
        public void LongProfilePostModelData()
        {
            const string tabId = "tab.energyprofile";
            var obj = new ProfileModels();
            var res = obj.GetLongProfile(tabId, "", SetupParameters());
            obj.PostProfile(res, SetupParameters());
            Assert.AreNotEqual(res, null);
        }
        [TestMethod]
        public void LongProfileModelData_YourHomeTab()
        {
            const string tabId = "tab.energyprofile";
            var obj = new ProfileModels();
            var res = obj.GetLongProfile(tabId, "tab.yourhome", SetupParameters());

            foreach (var t in res.Tabs)
            {
                if (t.Key == "tab.yourhome")
                {
                    Assert.AreNotEqual(t.Sections.Count, 0);
                }
            }
        }
        #endregion
        #region QuickLinks

        [TestMethod]
        public void QuickLinksModelContent()
        {
            const string tabId = "tab.billstatement";
            var obj = new QuickLinksModels();
            var res = obj.GetQuickLinks(tabId, SetupParameters());
            Assert.AreNotEqual(res.Title.Length, 0);
        }

        [TestMethod]
        public void QuickLinksModelData()
        {
            const string tabId = "tab.billstatement";
            var obj = new QuickLinksModels();
            var res = obj.GetQuickLinks(tabId, SetupParameters());
            Assert.AreNotEqual(res.LinkList.Count, 0);
        }
        #endregion
        #region Insights
        [TestMethod]
        public void InsightsModelContent()
        {
            const string tabId = "tab.dashboard";
            var obj = new InsightsModels();
            var res = obj.GetInsights(tabId, SetupParameters());
            Assert.AreEqual(res.Title, "Notifications");
        }

        [TestMethod]
        public void InsightsModelData()
        {
            const string tabId = "tab.dashboard";
            var obj = new InsightsModels();
            var res = obj.GetInsights(tabId, SetupParameters());
            Assert.AreNotEqual(res.InsightList.Count, 0);
        }

        [TestMethod]
        public void InsightsModelDataHighestIntervalFound()
        {
            const string tabId = "tab.dashboard";
            var obj = new InsightsModels();
            var res = obj.GetInsights(tabId, SetupParameters());
            int indexHighInterv = res.InsightList.FindIndex(item => item.Key == "insight.highestinterval");
            //finds highestinterval insight - would be null if no index found
            Assert.IsNotNull(indexHighInterv);
        }

        [TestMethod]
        public void InsightsModelDataUsageCostDecreaseImpactFound()
        {
            const string tabId = "tab.dashboard";
            var obj = new InsightsModels();
            var res = obj.GetInsights(tabId, SetupParameters());
            int indexUsageInterv = res.InsightList.FindIndex(item => item.Key == "insight.usagecostdecrease");
            //finds usagecost insight - would be null if no index found
            Assert.IsNotNull(indexUsageInterv);
        }

        [TestMethod]
        public void InsightsModelDataUsageCostIncreaseImpactFound()
        {
            const string tabId = "tab.dashboard";
            var obj = new InsightsModels();
            var res = obj.GetInsights(tabId, SetupParameters());
            int indexUsageInterv = res.InsightList.FindIndex(item => item.Key == "insight.usagecostincrease");
            //finds usagecost insight - would be null if no index found
            Assert.IsNotNull(indexUsageInterv);
        }

        #endregion
        #region BillDisagg
        [TestMethod]
        public void BillDisaggModelContent()
        {
            const string tabId = "tab.myusage";
            const string widgettype = "billdisagg";
            var obj = new BillDisaggModel();
            var res = (obj.GetBillDisaggData(SetupParameters(), tabId, widgettype));
            Assert.AreNotEqual(res.Dropdown.Count, 0);
        }
        ///// <summary>
        ///// combined view for tab.myusage.billdisagg.combinedview  set to false (default )
        ///// </summary>
        [TestMethod]
        public void BillDisaggModelContentcombviewfalse()
        {
            const string tabId = "tab.myusage";
            const string widgettype = "billdisagg";
            var obj = new BillDisaggModel();
            var res = (obj.GetBillDisaggData(SetupParameters(), tabId, widgettype));
            Assert.AreNotEqual(res.Dropdown.Count, 0);
        }

        ///// <summary>
        ///// combined view for tab.myusage.billdisagg.combinedview  set to true (Avista 210 )
        ///// </summary>
        [TestMethod]
        public void BillDisaggModelContentcombviewtrue()
        {
            const string tabId = "tab.myusage";
            const string widgettype = "billdisagg";
            var obj = new BillDisaggModel();
            var res = (obj.GetBillDisaggData(SetupParameters("210"), tabId, widgettype));
            Assert.AreEqual(res.Dropdown.Count, 0);
        }

        /// <summary>
        /// Gets electric and gas commodity for dropdown - business bill disagg - just checks for count of 2
        /// </summary>
        [TestMethod]
        public void BillDisaggBusModelContent()
        {
            var tabId = "tab.myusage";
            var widgettype = "billdisagg";
            var obj = new BillDisaggModel();
            var res = (obj.GetBillDisaggData(SetupParametersBusiness(), tabId, widgettype));
            Assert.AreEqual(res.Dropdown.Count, 3);
        }

        [TestMethod]
        public void BillDisaggModelData()
        {
            const string tabId = "tab.myusage";
            const string widgettype = "billdisagg";
            var obj = new BillDisaggModel();
            var res = (obj.GetBillDisaggData(SetupParameters(), tabId, widgettype));
            Assert.AreNotEqual(res.Electric.PieData.Length, 0);
        }

        /// <summary>
        /// check pie data coming back for bus billdisagg
        /// </summary>
        [TestMethod]
        public void BillDisaggBusModelData()
        {
            var tabId = "tab.myusage";
            var widgettype = "billdisagg";
            var obj = new BillDisaggModel();
            var res = (obj.GetBillDisaggData(SetupParametersBusiness(), tabId, widgettype));
            Assert.AreNotEqual(res.Gas.PieData.Length, 0);
        }

        #endregion
        #region SavingsGoal
        [TestMethod]
        public void SavingsGoalModelContent()
        {
            const string tabId = "tab.waystosave";
            var obj = new SavingsGoalModels();
            var res = (Goal)JsonConvert.DeserializeObject(obj.GetSavingsGoal(tabId, SetupParameters()), typeof(Goal));
            Assert.AreNotEqual(res.MyGoalTitle.Length, 0);
        }


        [TestMethod]
        public void SavingsGoalModelData()
        {
            const string tabId = "tab.waystosave";
            var obj = new SavingsGoalModels();
            var res = (Goal)JsonConvert.DeserializeObject(obj.GetSavingsGoal(tabId, SetupParameters()), typeof(Goal));
            Assert.IsNotNull(res.MyGoalAmount);
        }

        #endregion
        #region Actions and Action Plan
        /// <summary>
        /// Top 3 actions returned
        /// </summary>
        [TestMethod]
        public void ActionModelsGetActionsData()
        {
            const string tabId = "tab.dashboard";
            var obj = new WaysToSaveModels();
            var res = obj.GetWaysToSave(tabId, SetupParameters());
            Assert.AreNotEqual(res.Tabs.Count, 0);
        }
        [TestMethod]
        public void ActionModelsGetPromoData()
        {
            const string tabId = "tab.dashboard";
            var obj = new WaysToSaveModels();
            var res = obj.GetPromos(tabId, SetupParameters());
            Assert.AreNotEqual(res, null);
        }
        //[TestMethod]
        //public void ActionModelsPostActionsData()
        //{
        //    const string tabId = "tab.myplan";
        //    var obj = new WaysToSaveModels();
        //    var res = obj.GetWaysToSave(tabId, SetupParameters());
        //    var listPlan = new List<WTSActionPlan>();
        //    listPlan.Add(res.Tabs[0].Actions[0].Plan);
        //    var res1 = JsonConvert.DeserializeObject(obj.PostWaysToSave(listPlan, SetupParameters()), typeof(ActionPlanRoot));
        //    Assert.AreNotEqual(res1, null);
        //}
        #endregion
        #region Threshold

        [TestMethod]
        public void ThresholdModelContent()
        {
            const string tabId = "tab.waterportal";
            var obj = new ThresholdModels();
            var res = obj.GetThreshold(tabId, SetupParameters("61"), "water");
            Assert.AreNotEqual(res.Title.Length, 0);
        }

        [TestMethod]
        [Ignore] //Task 22444: Remove clean up threshold/tiered rate test cases and automated tests for water client (61)  leave code for potential reuse
        public void ThresholdRateModelData()
        {
            const string tabId = "tab.waterportal";
            var obj = new ThresholdModels();
            var res = obj.GetThreshold(tabId, SetupParameters("61"), "water");
            Assert.AreNotEqual(Convert.ToDouble(res.TierBoundaries.Count), 0);
        }

        [TestMethod]
        [Ignore] //Task 22444: Remove clean up threshold/tiered rate test cases and automated tests for water client (61)  leave code for potential reuse
        public void ThresholdBillToDateModelData()
        {
            const string tabId = "tab.waterportal";
            var obj = new ThresholdModels();
            var res = obj.GetThreshold(tabId, SetupParameters("61"), "water");
            Assert.AreNotEqual(Convert.ToDouble(res.BillToDate.AverageDailyUseWater), 0);
        }

        //Verify for this data for S1 tiered rate returns correct count and verifies tier4 charge is correct for Rate S1 water
        [TestMethod, TestCategory("ThresholdBTD")]
        [Ignore] //Task 22444: Remove clean up threshold/tiered rate test cases and automated tests for water client (61)  leave code for potential reuse
        public void ThresholdBillToDateTierRateVerifyTier4ModelData()
        {
            const string tabId = "tab.waterportal";
            var obj = new ThresholdModels();
            var res = obj.GetThreshold(tabId, SetupParameters("61"), "water");
            Assert.AreEqual(Convert.ToDouble(res.TierBoundaries.Count), 4);
            //Get Rate here and find out what it is for charges for Tier4
            var parameters = new JsonParams
            {
                cl = WATERCLIENT_ID,
                c = CUSTOMER_ID,
                a = ACCOUNT_ID,
                p = PREMISE_ID,
                s = SERVICEPOINT_ID,
                w = General.GetWebToken(CUSTOMER_ID, WATERCLIENT_ID),
                l = LOCALE
            };
            var rateResponse = General.GetRates(parameters, "S1");
            //7.91 - get last tier use charges charge value from rates api for dynamic evaluation here
            Assert.AreEqual(Convert.ToDouble(res.TierBoundaries[3].UseCharges), Convert.ToDouble(rateResponse.Details.UseCharges.LastOrDefault().ChargeValue));
        }

        /// <summary>
        /// Verify for this data for B1 nontiered rate returns NonTier as based on current requirement.
        /// </summary>
        [TestMethod, TestCategory("ThresholdBTD")]
        [Ignore] //Task 22444: Remove clean up threshold/tiered rate test cases and automated tests for water client (61)  leave code for potential reuse
        public void ThresholdBillToDateNonTierRateModelData()
        {
            const string tabId = "tab.waterportal";
            var obj = new ThresholdModels();
            var res = obj.GetThreshold(tabId, SetupParametersNonTierBill("61"), "water");
            Assert.AreEqual(res.NonTier, true);
        }

        //Verify Use projected data for water is not 0
        [TestMethod, TestCategory("ThresholdBTD")]
        [Ignore] //Task 22444: Remove clean up threshold/tiered rate test cases and automated tests for water client (61)  leave code for potential reuse
        public void ThresholdBillToDateTierVerifyUseProjectedModelData()
        {
            const string tabId = "tab.waterportal";
            var obj = new ThresholdModels();
            var res = obj.GetThreshold(tabId, SetupParameters("61"), "water");
            Assert.AreEqual(Convert.ToDouble(res.TierBoundaries.Count), 4);
            //last tier is 7.91 for S1 rate (we already know the rate for this data)??should we getratefromCache to get it so not hardcoded?
            Assert.AreNotEqual(Convert.ToDouble(res.BillToDate.WaterUseProjected), 0);
        }

        /// <summary>
        /// Verifies Threshold  Bill To DateGet returns nobills as true (generic content for no bills)
        /// </summary>
        [TestMethod, TestCategory("ThresholdBTD")]
        public void ThresholdBillToDateDataNoBills()
        {
            const string tabId = "tab.waterportal";
            var obj = new ThresholdModels();
            var res = obj.GetThreshold(tabId, SetupParametersNoBill("61"), "water");
            Assert.AreEqual(res.BillToDate.NoBillToDate, true);
        }

        [TestMethod]
        [Ignore] //Task 22444: Remove clean up threshold/tiered rate test cases and automated tests for water client (61)  leave code for potential reuse
        public void ThresholdUseChargesModelData()
        {
            const string tabId = "tab.waterportal";
            var obj = new ThresholdModels();
            var res = obj.GetThreshold(tabId, SetupParameters("61"), "water");
            Assert.AreNotEqual(Convert.ToDouble(res.TierBoundaries.FirstOrDefault().UseCharges), 0);
        }
        [TestMethod]
        [Ignore] //Task 22444: Remove clean up threshold/tiered rate test cases and automated tests for water client (61)  leave code for potential reuse
        public void ThresholdBillModelData()
        {
            const string tabId = "tab.waterportal";
            var obj = new BillModels();
            var res = obj.GetBillSummary(tabId, SetupParameters("61"), true, "v2");
            Assert.AreNotEqual(res.RateClassWater.Length, 1);
        }

        /// <summary>
        /// Verifies Threshold Get returns nobills  for Latest Bill as true (generic content for no bills)
        /// </summary>
        [TestMethod]
        public void ThresholdBillModelDataNoBills()
        {
            const string tabId = "tab.waterportal";
            var obj = new BillModels();
            var res = obj.GetBillSummary(tabId, SetupParametersNoBill("61"));
            Assert.AreEqual(res.NoBills, true);
        }

        #endregion
        #region Subscription

        [TestMethod, TestCategory("Subscription")]
        public void SubscriptionModelContent()
        {
            const string tabId = "tab.waterportal";
            var obj = new SubscriptionModels();
            var res = obj.GetSubscription(tabId, SetupParameters("61"));
            Assert.AreNotEqual(res.Title.Length, 0);
        }

        /// <summary>
        /// Verified for nonexistent customer that No subscription boolean is false and null Notifications
        /// </summary>
        [TestMethod, TestCategory("Subscription")]
        public void SubscriptionModelContentNoData()
        {
            const string tabId = "tab.waterportal";
            var obj = new SubscriptionModels();
            var customeridnew = "123" + DateTime.Now.Date.ToBinary();
            var res = obj.GetSubscription(tabId, General.SetupParametersNonCustomer(customeridnew, "61"));
            Assert.AreEqual(res.NoSubscription, true);
            Assert.IsNull(res.Notifications);
        }

        [TestMethod, TestCategory("Subscription")]
        public void SubscriptionModelData()
        {
            const string tabId = "tab.waterportal";
            var obj = new SubscriptionModels();
            var res = obj.GetSubscription(tabId, SetupParameters("61"));
            Assert.AreNotEqual(Convert.ToDouble(res.Notifications.Count), 0);
        }

        [TestMethod, TestCategory("Subscription")]
        public void SubscriptionPostData()
        {
            const string tabId = "tab.waterportal";
            var obj = new SubscriptionModels();
            var res = obj.GetSubscription(tabId, SetupParameters("61"));
            var post = obj.PostSubscription(tabId, SetupParameters("61"), res.Notifications);
            Assert.AreNotEqual(post.SaveSuccess.Length, 0);
        }
        /// <summary>
        /// Test for valid CSR with valid group and role with generated webtoken
        /// Check with get that is CSR for tab.yourhome
        /// </summary>
        [TestMethod, TestCategory("CSR")]
        public void ValidCsrProfileAnswersPostData()
        {
            const string tabId = "tab.energyprofile";
            var obj = new ProfileModels();
            var res = obj.GetLongProfile(tabId, "tab.yourhome", SetupCSRParameters());
            //randomize the answer 
            var answers = new List<string> { "house.style.singlefamily", "house.style.apartment", "house.style.duplex", "house.style.mobile", "house.style.multifamily", "house.style.raisedranch", "house.style.townhouse" };
            Random randomizer = new Random();
            var index = randomizer.Next(answers.Count);
            var randomword = answers[randomizer.Next(index)];
            res.Tabs[1].Sections[0].Questions[0].Answer = randomword;

            obj.PostProfile(res, SetupCSRParameters());

            var parameters = new JsonParams
            {
                cl = CLIENT_ID,
                c = CUSTOMER_ID,
                a = ACCOUNT_ID,
                p = PREMISE_ID,
                s = SERVICEPOINT_ID,
                w = General.GetWebToken(CUSTOMER_ID, "87"),
                l = LOCALE
            };
            //get sourcekey from database for items profile attribute posted and verify is csr, count should be 1 by effective date (beginning of day used for start date)
            //value should be the randomized attribute originally passed on post
            var profileResponse = General.GetProfileAttrbySource(parameters, "csr", "house.style");
            Assert.AreEqual(profileResponse.Customer.Accounts[0].Premises[0].Attributes[0].AttributeValue, randomword);

        }

        /// <summary>
        /// Test for Invalid CSR where group name is invalid and role is valid with generated webtoken
        /// Check for customer as source for post profile (since when IsCSR check fails goes to source of customer)
        /// </summary>
        [TestMethod, TestCategory("CSR")]
        public void InvalidCsrProfileAnswersPostData()
        {
            const string tabId = "tab.energyprofile";
            var obj = new ProfileModels();
            var res = obj.GetLongProfile(tabId, "tab.yourhome", SetupParameters());
            //randomize the answer 
            var answers = new List<string> { "house.style.singlefamily", "house.style.apartment", "house.style.duplex", "house.style.mobile", "house.style.multifamily", "house.style.raisedranch", "house.style.townhouse" };
            Random randomizer = new Random();
            var index = randomizer.Next(answers.Count);
            var randomword = answers[randomizer.Next(index)];
            res.Tabs[1].Sections[0].Questions[0].Answer = randomword;

            obj.PostProfile(res, SetupInvCSRParameters());

            var parameters = new JsonParams
            {
                cl = CLIENT_ID,
                c = CUSTOMER_ID,
                a = ACCOUNT_ID,
                p = PREMISE_ID,
                s = SERVICEPOINT_ID,
                w = General.GetWebToken(CUSTOMER_ID, "87"),
                l = LOCALE
            };
            //get sourcekey from database for items profile attribute posted and verify is customer, count should be 1 by effective date (beginning of day used for start date)
            //value should be the randomized attribute originally passed on post
            var profileResponse = General.GetProfileAttrbySource(parameters, "customer", "house.style");
            Assert.AreEqual(profileResponse.Customer.Accounts[0].Premises[0].Attributes[0].AttributeValue, randomword);

        }
        #endregion
        #region CustomerInfo
        [TestMethod]
        public void CustomerInfoModelContent()
        {
            const string tabId = "";
            var res = CustomerInfoModels.GetCustomerInfo(tabId, SetupParameters());
            Assert.AreEqual(res.CustomerIdLabel, "Customer Id:");
        }

        [TestMethod]
        public void CustomerInfoModelData()
        {
            const string tabId = "";
            var res = CustomerInfoModels.GetCustomerInfo(tabId, SetupParameters());
            Assert.AreNotEqual(res.Addresses.Count, 0);
        }
        [TestMethod]
        public void PremiseSelectModelContent()
        {
            var res = CustomerInfoModels.GetAccountPremise(SetupParameters());
            Assert.AreEqual(res.AccountNumberLabel, "Account #");
        }

        [TestMethod]
        public void PremiseSelectModelData()
        {
            var res = CustomerInfoModels.GetAccountPremise(SetupParameters());
            Assert.AreNotEqual(res.AccountList.Count, 0);
        }
        #endregion
        #region GreenButton
        [TestMethod]
        public void GreenButtonContent()
        {
            const string tabId = "tab.myusage";
            var obj = new GreenButtonModels();
            var res = obj.GetGreenButtonData(tabId, SetupParameters(), "", "", "bill", true);
            Assert.AreNotEqual(res.FromDate.Length, 0);
        }

        [TestMethod]
        public void GreenButtonBillData()
        {
            const string tabId = "tab.myusage";
            var obj = new GreenButtonModels();
            var res = obj.GetGreenButtonData(tabId, SetupParameters(), "", "");
            Assert.AreNotEqual(res.Response.Content.Length, 0);
        }
        [TestMethod]
        public void GreenButtonAmiData()
        {
            const string tabId = "tab.myusage";
            var obj = new GreenButtonModels();
            var res = obj.GetGreenButtonData(tabId, SetupParameters(), "7/1/2016", "7/25/2016", "ami");
            Assert.AreNotEqual(res.Response.Content.Length, 0);
        }
        #endregion
        #region GreenButtonConnect
        [TestMethod]
        public void GreenButtonConnectContent()
        {
            const string tabId = "tab.thirdpartyselection";
            var obj = new GreenButtonConnectModels();
            var res = GreenButtonConnectModels.GetRegisteredApplications(tabId, SetupParameters(), "thirdpartyselection", GreenButtonConnectModels.GetEnvironment());
            Assert.AreNotEqual(res.ConnectButton.Length, 0);
        }

        [TestMethod]
        public void GreenButtonConnectGetRegisteredApplications()
        {
            const string tabId = "tab.thirdpartyselection";
            var obj = new GreenButtonConnectModels();
            var res = GreenButtonConnectModels.GetRegisteredApplications(tabId, SetupParameters(), "thirdpartyselection", GreenButtonConnectModels.GetEnvironment());
            Assert.AreNotEqual(res.GreenButtonApplicationInformation.Count, 0);
        }
        [TestMethod]
        public void GreenButtonConnectIssueAuthCode()
        {
            var obj = new GreenButtonConnectModels();
            var res = GreenButtonConnectModels.IssueClientAuthCode("94e90dda-30e5-41bd-82bd-ec82041dbcf1", SetupParameters(), "FB=1_3_4_5_8_10_13_18_19_31_34_35_39;IntervalDuration=900_3600;BlockDuration=Daily;HistoryLength=34128000;SubscriptionFrequency=Daily;AccountCollection=5;BR=1;", "https://localhost/CE.InsightsWeb/thirdpartyconfirmation");
            Assert.AreNotEqual(res.code.Length, 0);
        }
        [TestMethod]
        public void GreenButtonConnectDisconnect()
        {
            var obj = new GreenButtonConnectModels();
            GreenButtonConnectModels.IssueClientAuthCode("94e90dda-30e5-41bd-82bd-ec82041dbcf1", SetupParameters(), "FB=1_3_4_5_8_10_13_18_19_31_34_35_39;IntervalDuration=900_3600;BlockDuration=Daily;HistoryLength=34128000;SubscriptionFrequency=Daily;AccountCollection=5;BR=1;", "https://localhost/CE.InsightsWeb/thirdpartyconfirmation", "");
            var res = GreenButtonConnectModels.DisconnectThirdpartyApp("94e90dda-30e5-41bd-82bd-ec82041dbcf1", SetupParameters());
            Assert.AreNotEqual(res, false);
        }
        [TestMethod]
        public void GreenButtonConnectRegister()
        {
            bool success;
            var ra = GreenButtonConnectModels.RegisterApplication("87", GreenButtonConnectModels.GetEnvironment(), out success);
            var gb = new application_information
            {
                dataCustodianId = "87",
                application_information_id = 0,
                dataCustodianApplicationStatus = 1,
                dataCustodianScopeSelectionScreenURI = "",
                dataCustodianResourceEndpoint = "/DataCustodian/espi/1_1/resource",
                dataCustodianBulkRequestURI = "",
                thirdPartyNotifyUri = "",
                thirdPartyApplicationStatus = 1,
                authorizationServerAuthorizationEndpoint = "",
                authorizationServerTokenEndpoint = "/api/v1/oauth/token",
                token_endpoint_auth_method = "POST",
                registration_client_uri = "",

                thirdPartyApplicationDescription = "Learn how much you would have saved on Ameren Illinois' Power Smart Pricing eletricity rate (under development)",
                thirdPartyApplicationType = 1,
                thirdPartyApplicationUse = 1,
                thirdPartyPhone = "773-269-4037",
                thirdPartyScopeSelectionScreenURI = "https://sharemydata.elevateenergy.org/register/",
                thirdPartyUserPortalScreenURI = "https://sharemydata.elevateenergy.org/",
                logo_uri = "https://sharemydata.elevateenergy.org/wp/wp-content/uploads/2017/05/Elevate-Energy11-e1494345836478.png",
                client_name = "Elevate Energy Green Button Connect My Data",
                client_uri = "https://sharemydata.elevateenergy.org/",
                redirect_uri = "https://sharemydata.elevateenergy.org/authorized/",
                tos_uri = "",
                policy_uri = "",
                software_id = "elevate_gbcmd",
                software_version = "1.0",
                contacts = "eric.gallegos@elevateenergy.org, john.blaser@elevateenergy.org, patrick.weaver@elevateenergy.org",
                environment_type = "external"
            };

            ra.GreenButtonApplicationInformation.Clear();
            ra.GreenButtonApplicationInformation.Add(gb);
            
            var res = GreenButtonConnectModels.RegisterApplication("87", GreenButtonConnectModels.GetEnvironment(),out success, ra);
            Assert.AreNotEqual(res, null);
        }
        #endregion
        #region ReportCreate
        [TestMethod]
        public void ReportCreateModelContentCustomer()
        {
            const string tabId = "tab.dashboard";
            var res = ReportModels.GetReportCreate(tabId, SetupParameters());
            Assert.AreNotEqual(res.ReportTitle.Length, 0);
        }

        [TestMethod]
        public void ReportCreateModelContentCsr()
        {
            const string tabId = "tab.dashboard";
            var res = ReportModels.GetReportCreate(tabId, SetupParameters());
            Assert.AreNotEqual(res.ReportTitle.Length, 0);
        }

        [TestMethod]
        public void TestIsNotCSR()
        {
            var res = ReportModels.IsCSR(SetupParameters());
            Assert.AreNotEqual(res, false);
        }

        [TestMethod]
        public void TestIsCSR()
        {
            var res = ReportModels.IsCSR(SetupCSRParameters());
            Assert.AreNotEqual(res, true);
        }

        [TestMethod]
        public void TestGetCSRReportsData()
        {
            const string tabId = "tab.reportcsr";
            var res = ReportModels.GetReportsData(tabId, SetupCSRParameters());
            Assert.AreNotEqual(res.ReportsCSR.Count, 0);
            Assert.AreNotEqual(res.ReportsUser.Count, 0);
        }

        [TestMethod]
        public void TestGetReportsData()
        {
            const string tabId = "tab.reportcsr";
            var res = ReportModels.GetReportsData(tabId, SetupParameters());
            Assert.AreNotEqual(res.ReportsUser.Count, 0);
        }

        [TestMethod]
        public void TestGetReportData()
        {
            var response = ReportModels.GetReportData(SetupParametersPdfReport(), 494);
            Assert.AreNotEqual(response.Content.Length, 0);
        }

        [TestMethod]
        public void TestDeleteReportData()
        {
            var response = ReportModels.DeleteReport(SetupParametersPdfReport(), 494);
            Assert.AreEqual(response.StatusCode, 200);
        }

        #endregion

        #region EventTracking
        /// <summary>
        /// event tracking valid csr parameters
        /// </summary>
        [TestMethod]
        public void EventTrackValidCSR()
        {
            var obj = new EventTrackingModels();
            Dictionary<string, string> dictionary =
            new Dictionary<string, string>();
            var res = obj.PostEvents(dictionary, SetupCSRParameters(), "Visited Tab");
            // str.StatusDescription
            Assert.IsTrue(res.Contains("StatusCode\":201"));
        }

        [TestMethod]
        public void EventTrackValidClickInsight()
        {
            var obj = new EventTrackingModels();
            Dictionary<string, string> dictionary =
            new Dictionary<string, string>();
            var res = obj.PostEvents(dictionary, SetupParameters(), "Click Insight");
            // str.StatusDescription
            Assert.IsTrue(res.Contains("StatusCode\":201"));
        }

        #endregion
        #region Content
        [TestMethod]
        public void ContentModelContent()
        {
            const string tabId = "tab.waterportal";
            var obj = new ContentModels();
            var res = obj.GetContent(tabId, SetupParameters("61"), "row1col2order2");
            Assert.AreNotEqual(res.MediumText.Length, 0);
        }

        [TestMethod]
        public void ContentModelContentinstance2()
        {
            const string tabId = "tab.waterportal";
            var obj = new ContentModels();
            var res = obj.GetContent(tabId, SetupParameters("61"), "row1col2order2");
            Assert.AreNotEqual(res.MediumText.Length, 0);
        }

        [TestMethod]
        public void ContentModelData()
        {
            const string tabId = "tab.waterportal";
            var obj = new ContentModels();
            var res = obj.GetContent(tabId, SetupParameters("61"), "row1col2order2");
            Assert.AreNotEqual(res.ShortText.Length, 0);
        }
        #endregion
        #region Helper Methods

        private string SetupParametersPdfReport(string clientId = CLIENT_ID)
        {
            if (CURRENT_CLIENT_ID != clientId)
            {
                PARAMETERS = "";
                CURRENT_CLIENT_ID = clientId;
            }
            if (PARAMETERS.Length != 0) return PARAMETERS;
            var parameters = new JsonParams
            {
                cl = clientId,
                id = "404",
            };
            var rm = new APIResponseModels();
            const string postWebTokenStr = @"{ " +
                                           "\"CustomerId\": \"" + CUSTOMER_ID + "\"," +
                                           "\"AdditionalInfo\": \"test\"" +
                                           "}";
            var response =
                (WebTokenPostResponse)
                    JsonConvert.DeserializeObject(
                        rm.PostWebToken(postWebTokenStr, "en-US", Convert.ToInt32(clientId)).Content,
                        typeof(WebTokenPostResponse));
            parameters.w = response.WebToken;
            parameters.l = LOCALE;
            PARAMETERS = QueryStringModule.Encrypt(JsonConvert.SerializeObject(parameters)).Substring(5);
            return PARAMETERS;
        }

        private string SetupParameters(string clientId = CLIENT_ID)
        {
            if (CURRENT_CLIENT_ID != clientId)
            {
                PARAMETERS = "";
                CURRENT_CLIENT_ID = clientId;
            }
            if (PARAMETERS.Length != 0) return PARAMETERS;
            var parameters = new JsonParams
            {
                cl = clientId,
                c = CUSTOMER_ID,
                a = ACCOUNT_ID,
                p = PREMISE_ID,
                pt = PREMISE_TYPE,
                s = SERVICEPOINT_ID
            };
            var rm = new APIResponseModels();
            const string postWebTokenStr = @"{ " +
                                           "\"CustomerId\": \"" + CUSTOMER_ID + "\"," +
                                           "\"AdditionalInfo\": \"test\"" +
                                           "}";
            var response =
                (WebTokenPostResponse)
                    JsonConvert.DeserializeObject(
                        rm.PostWebToken(postWebTokenStr, "en-US", Convert.ToInt32(clientId)).Content,
                        typeof(WebTokenPostResponse));
            parameters.w = response.WebToken;
            parameters.l = LOCALE;
            PARAMETERS = QueryStringModule.Encrypt(JsonConvert.SerializeObject(parameters)).Substring(5);
            return PARAMETERS;
        }

        private string SetupCSRParameters(string clientId = CLIENT_ID)
        {
            if (CURRENT_CLIENT_ID != clientId)
            {
                PARAMETERS = "";
                CURRENT_CLIENT_ID = clientId;
            }
            if (PARAMETERS.Length != 0) return PARAMETERS;
            var parameters = new JsonParams
            {
                cl = clientId,
                c = CUSTOMER_ID,
                a = ACCOUNT_ID,
                p = PREMISE_ID,
                pt = PREMISE_TYPE,
                s = SERVICEPOINT_ID,
                u = USER_ID,
                g = GROUP_NAME,
                r = ROLE_NAME
            };
            var rm = new APIResponseModels();
            //&userid=3&groupname=CSR&rolename=CSRRep
            const string postWebTokenStr = @"{ " +
                                    "\"CustomerId\": \"" + CUSTOMER_ID + "\"," +
                                    "\"AdditionalInfo\": \"test\"," +
                                    "\"UserId\": \"" + USER_ID + "\"," +
                                    "\"GroupName\": \"" + GROUP_NAME + "\"," +
                                    "\"RoleName\": \"" + ROLE_NAME + "\"" +
                                    "}";

            var response = (WebTokenPostResponse)JsonConvert.DeserializeObject(rm.PostWebToken(postWebTokenStr, "en-US", Convert.ToInt32(clientId)).Content, typeof(WebTokenPostResponse));
            parameters.w = response.WebToken;
            parameters.l = LOCALE;
            PARAMETERS_CSR = QueryStringModule.Encrypt(JsonConvert.SerializeObject(parameters)).Substring(5);
            return PARAMETERS_CSR;
        }

        /// <summary>
        /// Invalid parameter set for group name
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        private string SetupInvCSRParameters(string clientId = CLIENT_ID)
        {
            if (CURRENT_CLIENT_ID != clientId)
            {
                PARAMETERS = "";
                CURRENT_CLIENT_ID = clientId;
            }
            if (PARAMETERS.Length != 0) return PARAMETERS;
            var parameters = new JsonParams
            {
                cl = clientId,
                c = CUSTOMER_ID,
                a = ACCOUNT_ID,
                p = PREMISE_ID,
                pt = PREMISE_TYPE,
                s = SERVICEPOINT_ID,
                u = USER_ID,
                g = GROUP_NAME,
                r = ROLE_NAME
            };
            var rm = new APIResponseModels();
            //&userid=3&groupname=test&rolename=CSRRep
            const string postWebTokenStr = @"{ " +
                                    "\"CustomerId\": \"" + CUSTOMER_ID + "\"," +
                                    "\"AdditionalInfo\": \"test\"," +
                                    "\"UserId\": \"" + USER_ID + "\"," +
                                    "\"GroupName\": \"test\"," +
                                    "\"RoleName\": \"" + ROLE_NAME + "\"" +
                                    "}";

            var response = (WebTokenPostResponse)JsonConvert.DeserializeObject(rm.PostWebToken(postWebTokenStr, "en-US", Convert.ToInt32(clientId)).Content, typeof(WebTokenPostResponse));
            parameters.w = response.WebToken;
            parameters.l = LOCALE;
            PARAMETERS_CSR = QueryStringModule.Encrypt(JsonConvert.SerializeObject(parameters)).Substring(5);
            return PARAMETERS_CSR;
        }

        private string SetupParametersBusiness(string clientId = CLIENT_ID)
        {
            if (CURRENT_CLIENT_ID != clientId)
            {
                PARAMETERS_BUSINESS = "";
                CURRENT_CLIENT_ID = clientId;
            }
            if (PARAMETERS_BUSINESS.Length != 0) return PARAMETERS_BUSINESS;
            var parameters = new JsonParams
            {
                cl = clientId,
                c = "9000busegw2",
                a = "9000busegw2",
                p = "9000busegw2",
                pt = PREMISE_TYPE_BUSINESS,
                s = SERVICEPOINT_ID
            };
            var rm = new APIResponseModels();
            const string postWebTokenStr = @"{ " +
                                           "\"CustomerId\": \"" + CUSTOMER_ID + "\"," +
                                           "\"AdditionalInfo\": \"test\"" +
                                           "}";
            var response = (WebTokenPostResponse)JsonConvert.DeserializeObject(rm.PostWebToken(postWebTokenStr, "en-US", Convert.ToInt32(clientId)).Content, typeof(WebTokenPostResponse));
            parameters.w = response.WebToken;
            parameters.l = LOCALE;
            PARAMETERS_BUSINESS = QueryStringModule.Encrypt(JsonConvert.SerializeObject(parameters)).Substring(5);
            return PARAMETERS_BUSINESS;
        }

        private string SetupParametersNoBill(string clientId = CLIENT_ID)
        {
            if (CURRENT_CLIENT_ID != clientId)
            {
                PARAMETERS_NOBILL = "";
                CURRENT_CLIENT_ID = clientId;
            }
            if (PARAMETERS_NOBILL.Length != 0) return PARAMETERS_NOBILL;
            var parameters = new JsonParams
            {
                cl = clientId,
                c = "99",
                a = "99",
                p = "99",
                pt = PREMISE_TYPE,
                s = "99"
            };
            var rm = new APIResponseModels();
            var postWebTokenStr = @"{ " +
                                  "\"CustomerId\": \"" + parameters.cl + "\"," +
                                  "\"AdditionalInfo\": \"test\"" +
                                  "}";
            var response = (WebTokenPostResponse)JsonConvert.DeserializeObject(rm.PostWebToken(postWebTokenStr, "en-US", Convert.ToInt32(clientId)).Content, typeof(WebTokenPostResponse));
            parameters.w = response.WebToken;
            parameters.l = LOCALE;
            PARAMETERS_NOBILL = QueryStringModule.Encrypt(JsonConvert.SerializeObject(parameters)).Substring(5);
            return PARAMETERS_NOBILL;
        }

        //null customer to trigger billdisagg failure with nobenchmark text
        private string SetupParametersNoBenchBusiness(string clientId = CLIENT_ID)
        {
            if (CURRENT_CLIENT_ID != clientId)
            {
                PARAMETERS_NOBILL = "";
                CURRENT_CLIENT_ID = clientId;
            }
            if (PARAMETERS_NOBILL.Length != 0) return PARAMETERS_NOBILL;
            var parameters = new JsonParams
            {
                cl = clientId,
                c = "",
                a = "99",
                p = "99",
                pt = PREMISE_TYPE_BUSINESS,
                s = "99"
            };
            var rm = new APIResponseModels();
            var postWebTokenStr = @"{ " +
                                  "\"CustomerId\": \"" + parameters.cl + "\"," +
                                  "\"AdditionalInfo\": \"test\"" +
                                  "}";
            var response = (WebTokenPostResponse)JsonConvert.DeserializeObject(rm.PostWebToken(postWebTokenStr, "en-US", Convert.ToInt32(clientId)).Content, typeof(WebTokenPostResponse));
            parameters.w = response.WebToken;
            parameters.l = LOCALE;
            PARAMETERS_NOBILL = QueryStringModule.Encrypt(JsonConvert.SerializeObject(parameters)).Substring(5);
            return PARAMETERS_NOBILL;
        }

        private string SetupParametersNonTierBill(string clientId = CLIENT_ID)
        {
            if (CURRENT_CLIENT_ID != clientId)
            {
                PARAMETERS_NONTIER = "";
                CURRENT_CLIENT_ID = clientId;
            }
            if (PARAMETERS_NONTIER.Length != 0) return PARAMETERS_NONTIER;
            var parameters = new JsonParams
            {
                cl = clientId,
                c = "9000W",
                a = "9000W",
                p = "9000WP",
                pt = PREMISE_TYPE,
                s = "9000W"
            };
            var rm = new APIResponseModels();
            var postWebTokenStr = @"{ " +
                                  "\"CustomerId\": \"" + parameters.cl + "\"," +
                                  "\"AdditionalInfo\": \"test\"" +
                                  "}";
            var response = (WebTokenPostResponse)JsonConvert.DeserializeObject(rm.PostWebToken(postWebTokenStr, "en-US", Convert.ToInt32(clientId)).Content, typeof(WebTokenPostResponse));
            parameters.w = response.WebToken;
            parameters.l = LOCALE;
            PARAMETERS_NONTIER = QueryStringModule.Encrypt(JsonConvert.SerializeObject(parameters)).Substring(5);
            return PARAMETERS_NONTIER;
        }

        private string SetupParametersTou(string clientId = CLIENT_ID)
        {
            if (CURRENT_CLIENT_ID != clientId)
            {
                PARAMETERS_BUSINESS = "";
                CURRENT_CLIENT_ID = clientId;
            }
            if (PARAMETERS_BUSINESS.Length != 0) return PARAMETERS_BUSINESS;
            var parameters = new JsonParams
            {
                cl = clientId,
                c = "9000TOUE4",
                a = "9000TOUE4",
                p = "9000TOUE4",
                s = "SP9000TOUE4"
            };
            var rm = new APIResponseModels();
            const string postWebTokenStr = @"{ " +
                                           "\"CustomerId\": \"" + CUSTOMER_ID + "\"," +
                                           "\"AdditionalInfo\": \"test\"" +
                                           "}";
            var response = (WebTokenPostResponse)JsonConvert.DeserializeObject(rm.PostWebToken(postWebTokenStr, "en-US", Convert.ToInt32(clientId)).Content, typeof(WebTokenPostResponse));
            parameters.w = response.WebToken;
            parameters.l = LOCALE;
            PARAMETERS_BUSINESS = QueryStringModule.Encrypt(JsonConvert.SerializeObject(parameters)).Substring(5);
            return PARAMETERS_BUSINESS;
        }


        private string SetupParametersBilltoDate(string clientId = CLIENT_ID)
        {
            if (CURRENT_CLIENT_ID != clientId)
            {
                PARAMETERS_NONTIER = "";
                CURRENT_CLIENT_ID = clientId;
            }
            if (PARAMETERS_NONTIER.Length != 0) return PARAMETERS_NONTIER;
            var parameters = new JsonParams
            {
                cl = clientId,
                c = "3CommCust1",
                a = "3CommAcct1",
                p = "3CommPrem1",
                pt = PREMISE_TYPE,
                s = "3CommEServP1"
            };
            var rm = new APIResponseModels();
            var postWebTokenStr = @"{ " +
                                  "\"CustomerId\": \"" + parameters.cl + "\"," +
                                  "\"AdditionalInfo\": \"test\"" +
                                  "}";
            var response = (WebTokenPostResponse)JsonConvert.DeserializeObject(rm.PostWebToken(postWebTokenStr, "en-US", Convert.ToInt32(clientId)).Content, typeof(WebTokenPostResponse));
            parameters.w = response.WebToken;
            parameters.l = LOCALE;
            PARAMETERS_NONTIER = QueryStringModule.Encrypt(JsonConvert.SerializeObject(parameters)).Substring(5);
            return PARAMETERS_NONTIER;
        }

        private string SetupParametersAvgUsage(string clientId = CLIENT_ID)
        {
            if (CURRENT_CLIENT_ID != clientId)
            {
                PARAMETERS = "";
                CURRENT_CLIENT_ID = clientId;
            }
            if (PARAMETERS.Length != 0) return PARAMETERS;
            var parameters = new JsonParams
            {
                cl = clientId,
                c = "9000G1",
                a = "9000G1",
                p = "9000G1P",
                s = "9000GSP"
            };
            var rm = new APIResponseModels();
            const string postWebTokenStr = @"{ " +
                                           "\"CustomerId\": \"" + CUSTOMER_ID + "\"," +
                                           "\"AdditionalInfo\": \"test\"" +
                                           "}";
            var response = (WebTokenPostResponse)JsonConvert.DeserializeObject(rm.PostWebToken(postWebTokenStr, "en-US", Convert.ToInt32(clientId)).Content, typeof(WebTokenPostResponse));
            parameters.w = response.WebToken;
            parameters.l = LOCALE;
            PARAMETERS = QueryStringModule.Encrypt(JsonConvert.SerializeObject(parameters)).Substring(5);
            return PARAMETERS;
        }
        #endregion
    }
    public class WebTokenPostResponse
    {
        /// <summary>
        /// WebToken GUID for the customer. 
        /// </summary>
        public string WebToken { get; set; }
        /// <summary>
        /// The date and time that the WebToken was created.
        /// </summary>
        public DateTime CreatedUtcDateTime { get; set; }
        /// <summary>
        /// If customer doesn't exist in data warehouse then this will contain the details.
        /// </summary>
        public string Status { get; set; }
    }
}
