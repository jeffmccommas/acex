﻿using System;
using CE.InsightsWeb.Common;
using CE.InsightsWeb.Common.AclaraJson;
using CE.InsightsWeb.Common.QueryStringEncryptor;
using CE.InsightsWeb.Models;
using Newtonsoft.Json;
using System.Linq;
using System.Collections.Generic;

namespace CE.InsightsWeb.Test.Helpers
{
    public static class General
    {
        private const string LOCALE = "en-US";
        private const string COMMON_CATEGORY = "common";
        private const string SUBSCRIPTION_CATEGORY = "subscription";

        public static List<Common.TextContent> GetSubscriptionContent(string clientId)
        {
            var content = CMS.GetContent(int.Parse(clientId), LOCALE).Content;
            var subscriptionTextContent =
                content.TextContent.Where(x => x.category == SUBSCRIPTION_CATEGORY || x.category == COMMON_CATEGORY).ToList();
            return subscriptionTextContent;
        }

        public static RateRoot GetRates(JsonParams jp, string rateClass, string version = "v1")
        {
            var rm = new APIResponseModels();
                var date = DateTime.UtcNow.ToString("yyyy-MM-dd");
                var requestParams = "rateclass=" + rateClass + "&date=" + date + "&IncludeRateDetails=true&IncludeFinalizedTierBoundaries=true";
                var rateResponse = (RateRoot)JsonConvert.DeserializeObject(rm.GetRate(requestParams, jp.l, int.Parse(jp.cl), version).Content, typeof(RateRoot));
            return rateResponse;
        }

        /// <summary>
        /// Get profile attributes by source - used for CSR testing
        /// </summary>
        /// <param name="jp"></param>
        /// <param name="sourceKey"></param>
        /// <returns></returns>
        public static ProfileRoot GetProfileAttrbySource(JsonParams jp, string sourceKey, string attributeKey)
        {
            var pm = new APIResponseModels();
            var date = DateTime.UtcNow.ToString("yyyy-MM-dd");
            var requestParams = "CustomerId=" + jp.c + "&AccountId=" + jp.a + "&StartDate=" + date + "&SourceKeys=" + sourceKey + "" + "&ProfileAttributeKeys=" + attributeKey;
            var profileResponse = (ProfileRoot)JsonConvert.DeserializeObject(pm.GetProfile(requestParams, jp.l, int.Parse(jp.cl)).Content, typeof(ProfileRoot));
            return profileResponse;
        }

        public static string GetWebToken(string customerid, string clientid)
        {
            var rm = new APIResponseModels();
            var postWebTokenStr = @"{ " +
                                  "\"CustomerId\": \"" + customerid + "\"," +
                                  "\"AdditionalInfo\": \"test\"" +
                                  "}";
            var response = (WebTokenPostResponse)JsonConvert.DeserializeObject(rm.PostWebToken(postWebTokenStr, "en-US", Convert.ToInt32(clientid)).Content, typeof(WebTokenPostResponse));
            return response.WebToken;
        }

        public static string SetupParametersNonCustomer(string customerid, string clientid)
        {
            var parameters = new JsonParams
            {
                cl = clientid,
                c = customerid,
                a = customerid,
                p = customerid,
                s = customerid,
            };

        var rm = new APIResponseModels();
            var postWebTokenStr = @"{ " +
                                  "\"CustomerId\": \"" + customerid + "\"," +
                                  "\"AdditionalInfo\": \"test\"" +
                                  "}";
            var response = (WebTokenPostResponse)JsonConvert.DeserializeObject(rm.PostWebToken(postWebTokenStr, "en-US", Convert.ToInt32(clientid)).Content, typeof(WebTokenPostResponse));
            parameters.w = response.WebToken;
            parameters.l = LOCALE;
            return QueryStringModule.Encrypt(JsonConvert.SerializeObject(parameters)).Substring(5);

        }
    }
}
