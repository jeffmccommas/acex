﻿using CE.Infrastructure;
using CE.Insights.Common.Filters;
using CE.Insights.Models;
using CE.Models;
using CE.Models.Insights;
using Microsoft.Practices.Unity;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace CE.Insights.Controllers.Version2
{
    /// <summary>
    /// The Consumption API version 2.0 returns total usage for a customer, account, premise and service point. 
    /// The user specifies the time period and time interval resolution for the data. 
    /// </summary>
    [RequireHttpsAndClientCertAttribute]
    public class ConsumptionController : ApiController
    {
        private readonly ConsumptionModel model;

        public ConsumptionController()
        {
            var container = (IUnityContainer)GlobalConfiguration.Configuration.DependencyResolver.GetService(typeof(IUnityContainer));
            model = new ConsumptionModel(container);
        }

        public ConsumptionController(IUnityContainer unityContainer)
        {
            model = new ConsumptionModel(unityContainer);
        }

        /// <summary>
        /// Testability, passed in repository and unity container so not relying on data access through sql server and table storage
        /// </summary>
        /// <param name="unityContainer"></param>
        /// <param name="insightsEfRepository"></param>
        public ConsumptionController(IUnityContainer unityContainer, IInsightsEFRepository insightsEfRepository)
        {
            model = new ConsumptionModel(unityContainer, insightsEfRepository);
        }

        /// <summary>
        /// GET Consumption request with the parameters in an encoded string. 
        /// Returns total usage for a customer, account, premise and service point. 
        /// </summary>
        /// <param name="enc"></param>
        /// <returns>Total consumption for a customer, account, premise and service point.</returns>
        [ResponseType(typeof(ConsumptionResponse))]
        public HttpResponseMessage Get([FromUri] string enc)
        {
            string customerId = null;
            string accountId = null;
            string premiseId = null;
            DateTime? startDate = null;
            DateTime? endDate = null;
            string meterIds = null;
            string resolution = null;
            string commodity = null;
            bool includeContent = false;
            bool includeresolutions = false;
            bool includeWeather = false;
            bool includeAverageUsage = false;
            int pageIndex = 1;
            int count = 0;

            foreach (var aa in ActionContext.ActionArguments)
            {
                switch (aa.Key.ToLower())
                {
                    case "customerid":
                        customerId = aa.Value.ToString();
                        break;
                    case "accountid":
                        accountId = aa.Value.ToString();
                        break;
                    case "premiseid":
                        premiseId = aa.Value.ToString();
                        break;
                    case "startdate":
                        startDate = DateTime.Parse(aa.Value.ToString());
                        break;
                    case "enddate":
                        endDate = DateTime.Parse(aa.Value.ToString());
                        break;
                    case "meterids":
                        meterIds = aa.Value.ToString();
                        break;
                    case "count":
                        if (!(int.TryParse(aa.Value.ToString(), out count)))
                        {
                            count = 0;
                        }
                        break;
                    case "resolutionkey":
                        resolution = aa.Value.ToString();
                        break;
                    case "commoditykey":
                        commodity = aa.Value.ToString();
                        break;
                    case "includeresolutions":
                        Boolean.TryParse(aa.Value.ToString(), out includeresolutions);
                        break;
                    case "includecontent":
                        Boolean.TryParse(aa.Value.ToString(), out includeContent);
                        break;
                    case "pageindex":
                        if (!(int.TryParse(aa.Value.ToString(), out pageIndex)))
                        {
                            pageIndex = 1;
                        }
                        break;
                    case "includeweather":
                        Boolean.TryParse(aa.Value.ToString(), out includeWeather);
                        break;
                    case "includeaverageusage":
                        Boolean.TryParse(aa.Value.ToString(), out includeAverageUsage);
                        break;
                }
            }

            var request = new ConsumptionRequest();
            request.CustomerId = customerId;
            request.AccountId = accountId;
            request.PremiseId = premiseId;
            if (!(startDate == null)) { request.StartDate = startDate.Value; }
            if (!(endDate == null)) { request.EndDate = endDate.Value; }
            request.Count = count;
            request.MeterIds = meterIds;
            request.ResolutionKey = resolution;
            request.CommodityKey = commodity;
            request.IncludeContent = includeContent;
            request.IncludeResolutions = includeresolutions;
            request.PageIndex = pageIndex;
            request.IncludeWeather = includeWeather;
            request.IncludeAverageUsage = includeAverageUsage;

            // Validate model via model annotations.  This must execute here, since our enc is special. 
            bool success;
            var resultModelState = ValidateModels.Validate(request, out success);
            if (!success)
            {
                return (Request.CreateErrorResponse(HttpStatusCode.BadRequest, resultModelState));
            }

            return Get(request);
        }


        /// <summary>
        /// GET Consumption request with un-encoded parameters. 
        /// Returns total usage for a customer, account, premise and service point.
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Total consumption for a customer, account, premise and service point</returns>
        [ResponseType(typeof(ConsumptionResponse))]
        public HttpResponseMessage Get([FromUri] ConsumptionRequest request)
        {
            ConsumptionResponse response;
            try
            {

                var clientUser = ((ClientUser) ActionContext.Request.Properties[CEConfiguration.CEClientUser]);
                var locale = (ActionContext.Request.Properties.ContainsKey(CEConfiguration.CELocale))
                    ? ActionContext.Request.Properties[CEConfiguration.CELocale].ToString()
                    : "en-US";

                //model = new ConsumptionModel(container);
                if (request.MeterIds.Length == 0 && request.ServicePointId.Length == 0)
                {
                    response = new ConsumptionResponse { Message = CEConfiguration.Required_MeterId_ServicePointIds };
                }
                //if (request.MeterIds == null || (request.MeterIds != null && request.MeterIds == string.Empty))
                //{
                //    response = new ConsumptionResponse {Message = CEConfiguration.Required_MeterId_ServicePointIds };
                //}
                else
                {
                    response = model.GetConsumption(clientUser, request);

                    if (response != null)
                    {
                        if (request.IncludeContent)
                        {
                            model.ApplyContent(response, clientUser, locale);
                        }
                    }
                    else
                    {
                        response = new ConsumptionResponse {Message = CEConfiguration.Response_NoData};
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }

            return (Request.CreateResponse(HttpStatusCode.OK, response));
        }
    }
}