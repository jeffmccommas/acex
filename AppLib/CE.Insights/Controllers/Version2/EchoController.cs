﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using CE.Infrastructure;
using CE.Insights.Common.Filters;
using CE.Models.Insights;

namespace CE.Insights.Controllers.Version2
{

    /// <summary>
    /// Echo endpoint version 2.0. The purpose of the Echo endpoint to test if the service is up and binding correctly to various input.
    /// It simply echoes an input parameter back to the user.
    /// GET Echo can also be called to keep the application "alive" so that start up times are always fast. 
    /// 
    /// </summary>
    [RequireHttpsAndClientCertAttribute]
    public class EchoController : ApiController
    {
        public EchoController() { }
        /// <summary>
        /// GET Echo request with an encoded parameter string. Returns the value passed in the "Say" input parameter.
        /// Call in "Keep Alive" mode to ensure quick start-up times.
        /// </summary>
        /// <param name="enc"></param>
        /// <returns></returns>
        [ResponseType(typeof(EchoResponse))]
        public HttpResponseMessage Get([FromUri] string enc)
        {
            EchoRequest req = new EchoRequest();

            foreach (var aa in this.ActionContext.ActionArguments)
            {
                if (aa.Key.ToLower() == "say")
                {
                    req.Say = aa.Value.ToString();
                }
                else if (aa.Key.ToLower() == "name")
                {
                    req.Name = aa.Value.ToString();
                }
                else if (aa.Key.ToLower() == "number")
                {
                    req.Number = System.Convert.ToInt32(aa.Value);
                }
            }

            // Validate model via model annotations.  This must execute here, since our enc is special. 
            bool success = true;
            var resultModelState = ValidateModels.Validate(req, out success);
            if (!success)
            {
                return (Request.CreateErrorResponse(HttpStatusCode.BadRequest, resultModelState));
            }

            return (Get(req));
        }

        /// <summary>
        /// Get Echo called with complex, unencoded request object. Returns the value passed in the "Say" input parameter. 
        /// Call in "Keep Alive" mode to ensure quick start-up times.
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [ResponseType(typeof(EchoResponse))]
        public HttpResponseMessage Get([FromUri] EchoRequest req)
        {

            var response = new EchoResponse()
            {
                Echo = req.Say,
            };

            return (Request.CreateResponse(HttpStatusCode.OK, response));
        }


        /// <summary>
        /// Returns the value passed in the "Say" input parameter.
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [ResponseType(typeof(EchoResponse))]
        public HttpResponseMessage Post([FromBody] EchoRequest req)
        {
            var response = new EchoResponse()
            {
                Echo = req.Say,
            };

            return (Request.CreateResponse(HttpStatusCode.OK, response));
        }

    }
}
