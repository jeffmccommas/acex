﻿using CE.Infrastructure;
using CE.Insights.Common.Filters;
using CE.Insights.Models;
using CE.Models;
using CE.Models.Insights;
using Microsoft.Practices.Unity;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace CE.Insights.Controllers.Version2
{
    /// <summary>
    /// This is version 2.0 of the Bill endpoint. This version is different from version 1.0 in that it retrieves bill data from the Aclara One database instead of the Insights data warehouse. This is used only by clients who do not store bills in the Insights data warehouse. 
    ///
    /// The Bill endpoint version 2.0 returns bill summary information for a utility customer.
    /// </summary>
    [RequireHttpsAndClientCertAttribute]
    public class BillController : ApiController
    {
        private IUnityContainer container;

        public BillController()
        {
            container = (IUnityContainer)GlobalConfiguration.Configuration.DependencyResolver.GetService(typeof(IUnityContainer));
        }

        public BillController(IUnityContainer unityContainer)
        {
            container = unityContainer;
        }

        /// <summary>
        /// GET Bill request with the parameters in an encoded string. 
        /// 
        /// This is version 2.0 of the Bill endpoint. This version is different from version 1.0 in that it retrieves bill data from the Aclara One database instead of the Insights data warehouse. This is used only by clients who do not store bills in the Insights data warehouse. 
        /// </summary>
        /// <param name="enc"></param>
        /// <returns>The GET Bill response includes bill summary information for a utility customer.</returns>
        [ResponseType(typeof(BillResponse))]
        public HttpResponseMessage Get([FromUri] string enc)
        {
            string customerId = null;
            string accountId = null;
            DateTime? startDate = null;
            DateTime? endDate = null;
            int count = 1;
            bool includeContent = false;

            foreach (var aa in this.ActionContext.ActionArguments)
            {
                switch (aa.Key.ToLower())
                {
                    case "customerid":
                        customerId = aa.Value.ToString();
                        break;
                    case "accountid":
                        accountId = aa.Value.ToString();
                        break;
                    case "startdate":
                        startDate = DateTime.Parse(aa.Value.ToString());
                        break;
                    case "enddate":
                        endDate = DateTime.Parse(aa.Value.ToString());
                        break;
                    case "count":
                        if (!(int.TryParse(aa.Value.ToString(), out count)))
                        {
                            count = 1;
                        }
                        break;

                    case "includecontent":
                        Boolean.TryParse(aa.Value.ToString(), out includeContent);
                        break;
                }
            }

            var request = new BillRequest();
            request.CustomerId = customerId;
            request.AccountId = accountId;
            if (!(startDate == null)) { request.StartDate = startDate.Value; }
            if (!(endDate == null)) { request.EndDate = endDate.Value; }
            request.Count = count;
            request.IncludeContent = includeContent;

            // Validate model via model annotations.  This must execute here, since our enc is special. 
            bool success = true;
            var resultModelState = ValidateModels.Validate(request, out success);
            if (!success)
            {
                return (Request.CreateErrorResponse(HttpStatusCode.BadRequest, resultModelState));
            }

            return Get(request);
        }


        /// <summary>
        /// GET Bill request with un-encoded parameters. 
        /// 
        /// This is version 2.0 of the Bill endpoint. This version is different from version 1.0 in that it retrieves bill data from the Aclara One database instead of the Insights data warehouse. This is used only by clients who do not store bills in the Insights data warehouse. 
        /// </summary>
        /// <param name="request"></param>
        /// <returns>The GET Bill response includes bill summary information for a utility customer</returns>
        [ResponseType(typeof(BillResponse))]
        public HttpResponseMessage Get([FromUri] BillRequest request)
        {
            BillResponse response = null;

            var clientUser = ((ClientUser)this.ActionContext.Request.Properties[CEConfiguration.CEClientUser]);
            var locale = (ActionContext.Request.Properties.ContainsKey(CEConfiguration.CELocale))
                        ? ActionContext.Request.Properties[CEConfiguration.CELocale].ToString() : "en-US";

            var model = new BillModel(container);

            response = model.GetBills(clientUser.ClientID, request);

            if (response != null)
            {
                if (request.IncludeContent)
                {
                    var billModel = new BillModel();
                    billModel.ApplyContent(response, clientUser, locale);
                }
            }
            else
            {
                response = new BillResponse { Message = CEConfiguration.Response_NoData };
            }

            return (Request.CreateResponse(HttpStatusCode.OK, response));
        }
    }
}