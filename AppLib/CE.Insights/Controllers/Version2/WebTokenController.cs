﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Tracing;
using CE.Infrastructure;
using CE.Insights.Common.Filters;
using CE.Insights.Models;
using CE.Models;
using CE.Models.Insights;
using Microsoft.Practices.Unity;

namespace CE.Insights.Controllers.Version2
{
    /// <summary>
    /// This is version 2.0 of the WebToken endpoint. It is different from version 1.0 in that it only uses data that is stored in the Aclara One database. This is used only by clients who do not store bills in the Insights data warehouse. The WebToken API enables a utility to create and validate WebToken values for a customer.   
    /// </summary>
    [RequireHttpsAndClientCertAttribute]
    public class WebTokenController : ApiController
    {
        private const string StringApplication = "Application";
        private ClientUser _clientUser;
        private ITraceWriter _traceWriter;
        private IUnityContainer container;

        public WebTokenController()
        {
            container = (IUnityContainer)GlobalConfiguration.Configuration.DependencyResolver.GetService(typeof(IUnityContainer));
        }

        public WebTokenController(IUnityContainer unityContainer)
        {
            container = unityContainer;
        }


        public ClientUser InsightsUser
        {
            get { return _clientUser ?? (_clientUser = GetClientUser()); }
        }
        public ITraceWriter TraceWriter
        {
            get { return _traceWriter ?? (_traceWriter = Configuration.Services.GetTraceWriter()); }
        }



        /// <summary>
        /// GET WebToken request with the parameters in an encoded string. 
        /// Validates the input web token and customer id and returns the token's status, created date time and any additional information stored with the token.
        ///
        /// This is version 2.0 of the WebToken endpoint. It is different from version 1.0 in that it only uses data that is stored in the Aclara One database. This is used only by clients who do not store bills in the Insights data warehouse. 
        /// </summary>
        /// <param name="enc"></param>
        /// <returns>Returns token's status, created date time and any additional information stored with the token. </returns>
        [ResponseType(typeof(WebTokenResponse))]
        public HttpResponseMessage Get([FromUri] string enc)
        {
            const string stringCustomerId = "customerid";
            const string stringIncludeadditionalinfo = "includeadditionalinfo";
            const string stringWebToken = "webtoken";

            var request = new WebTokenRequest();

            foreach (var aa in ActionContext.ActionArguments)
            {
                switch (aa.Key.ToLower())
                {
                    case stringCustomerId:
                        request.CustomerId = aa.Value.ToString();
                        break;
                    case stringIncludeadditionalinfo:
                        bool includeAdditionalInfo;
                        Boolean.TryParse(aa.Value.ToString(), out includeAdditionalInfo);
                        request.IncludeAdditionalInfo = includeAdditionalInfo;
                        break;
                    case stringWebToken:
                        request.WebToken = aa.Value.ToString();
                        break;
                }
            }

            // Validate model via model annotations.  This must execute here, since our enc is special. 
            bool success;
            var resultModelState = ValidateModels.Validate(request, out success);
            return !success ? (Request.CreateErrorResponse(HttpStatusCode.BadRequest, resultModelState)) : Get(request);
        }



        /// <summary>
        /// GET WebToken request with un-encoded parameters. 
        /// Validates the input web token and customer id and returns the token's status, created date time and any additional information stored with the token.
        ///
        /// This is version 2.0 of the WebToken endpoint. It is different from version 1.0 in that it only uses data that is stored in the Aclara One database. This is used only by clients who do not store bills in the Insights data warehouse. 
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Returns the token's status, created date time and any additional information stored with the token. </returns>
        [ResponseType(typeof(WebTokenResponse))]
        public HttpResponseMessage Get([FromUri] WebTokenRequest request)
        {
            var response = new WebTokenResponse();

            var model = new WebTokenModel(container);

            try
            {
                var result = model.ValidateWebToken(InsightsUser, request, response);

                if (!result)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, string.Format(CEConfiguration.ErrorAccessDenied, request.CustomerId, request.WebToken));
                }

                if (!response.IsActive)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, string.Format(CEConfiguration.ErrorTokenExpired, request.WebToken, request.CustomerId));
                }
            }
            catch (Exception ex)
            {
                TraceWriter.Error(ActionContext.Request, StringApplication, ex, ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, CEConfiguration.Error_GetFailed);
            }

            return (Request.CreateResponse(HttpStatusCode.OK, response));
        }


        /// <summary>
        /// Creates a new web token for accessing a customer’s data.
        ///
        /// This is version 2.0 of the WebToken endpoint. It is different from version 1.0 in that it only uses data that is stored in the Aclara One database. This is used only by clients who do not store bills in the Insights data warehouse. 
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Returns a web token for accessing the customer's data.</returns>
        [ResponseType(typeof(WebTokenPostResponse))]
        public HttpResponseMessage Post([FromBody] WebTokenPostRequest request)
        {
            var response = new WebTokenPostResponse();

            if (request == null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, CEConfiguration.Error_PostEmpty);
            }

            try
            {
                var model = new WebTokenModel(container);

                model.CreateWebToken(InsightsUser, request, response);

            }
            catch (Exception ex)
            {
                TraceWriter.Error(ActionContext.Request, StringApplication, ex, ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, CEConfiguration.Error_PostFailed);
            }

            return (Request.CreateResponse(HttpStatusCode.OK, response));
        }


        /// <summary>
        /// Deletes web token for accessing a customer’s data.
        ///
        /// This is version 2.0 of the WebToken endpoint. It is different from version 1.0 in that it only uses data that is stored in the Aclara One database. This is used only by clients who do not store bills in the Insights data warehouse. 
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Returns Success=true after deleting the web token.</returns>
        [ResponseType(typeof(WebTokenDeleteResponse))]
        public HttpResponseMessage Delete([FromUri] WebTokenDeleteRequest request)
        {
            var response = new WebTokenDeleteResponse();

            if (request == null)
            {
                // No data was posted when data is expected.
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, CEConfiguration.Error_PostEmpty);
            }

            try
            {
                var model = new WebTokenModel(container);

                var result = model.DeleteWebToken(InsightsUser, request, response);

                if (!result)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, string.Format(CEConfiguration.ErrorNoDataAvailableToDelete, request.CustomerId, request.WebToken));
                }
            }
            catch (Exception ex)
            {
                TraceWriter.Error(ActionContext.Request, StringApplication, ex, ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, CEConfiguration.Error_PostFailed);
            }

            return (Request.CreateResponse(HttpStatusCode.OK, response));
        }


        /// <summary>
        /// Returns Client User object.
        /// </summary>
        /// <returns></returns>
        private ClientUser GetClientUser()
        {
            return ((ClientUser)ActionContext.Request.Properties[CEConfiguration.CEClientUser]);
        }


    }

}