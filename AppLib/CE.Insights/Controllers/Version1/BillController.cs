﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using CE.Infrastructure;
using CE.Insights.Common.Filters;
using CE.Insights.Models;
using CE.Models;
using CE.Models.Insights;

namespace CE.Insights.Controllers.Version1
{

    /// <summary>
    /// This is version 1.0 of the Bill endpoint. The Bill endpoint returns bill summary information for a utility customer.
    /// </summary>
    [RequireHttpsAndClientCertAttribute]
    public class BillController : ApiController {
        public static IInsightsEFRepository InsightsEfRepository;

        public BillController() {
            InsightsEfRepository = new InsightsEfRepository();
        }

        /// <summary>
        /// Testability, passed in repository so not relying on data access through sql server
        /// </summary>
        /// <param name="insightsEfRepository"></param>
        public BillController(IInsightsEFRepository insightsEfRepository)
        {
            if (insightsEfRepository == null)
            {
                throw new ArgumentNullException("", @"InsightsRepository is null!");
            }

            InsightsEfRepository = insightsEfRepository;

        }

        /// <summary>
        /// GET Bill request with the parameters in an encoded string. 
        /// </summary>
        /// <param name="enc"></param>
        /// <returns>The GET Bill response includes bill summary information for a utility customer.</returns>
        [ResponseType(typeof(BillResponse))]
        public HttpResponseMessage Get([FromUri] string enc)
        {
            string customerId = null;
            string accountId = null;
            DateTime? startDate = null;
            DateTime? endDate = null;
            var count = 1;
            var includeContent = false;

            foreach (var aa in ActionContext.ActionArguments)
            {
                switch (aa.Key.ToLower())
                {
                    case "customerid":
                        customerId = aa.Value.ToString();
                        break;
                    case "accountid":
                        accountId = aa.Value.ToString();
                        break;
                    case "startdate":
                        startDate = DateTime.Parse(aa.Value.ToString());
                        break;
                    case "enddate":
                        endDate = DateTime.Parse(aa.Value.ToString());
                        break;
                    case "count":
                        if (!(int.TryParse(aa.Value.ToString(), out count)))
                        {
                            count = 1;
                        }
                        break;

                    case "includecontent":
                        bool.TryParse(aa.Value.ToString(), out includeContent);
                        break;
                }
            }

            var request = new BillRequest
            {
                CustomerId = customerId,
                AccountId = accountId
            };

            if (!(startDate == null))
            {
                request.StartDate = startDate.Value;
            }

            if (!(endDate == null))
            {
                request.EndDate = endDate.Value;
            }

            request.Count = count;
            request.IncludeContent = includeContent;

            // Validate model via model annotations.  This must execute here, since our enc is special. 
            bool success;
            var resultModelState = ValidateModels.Validate(request, out success);
            return !success ? Request.CreateErrorResponse(HttpStatusCode.BadRequest, resultModelState) : Get(request);
        }


        /// <summary>
        /// GET Bill request with un-encoded parameters. 
        /// </summary>
        /// <param name="request"></param>
        /// <returns>The GET Bill response includes bill summary information for a utility customer</returns>
        [ResponseType(typeof(BillResponse))]
        public HttpResponseMessage Get([FromUri] BillRequest request)
        {
            var clientUser = (ClientUser)ActionContext.Request.Properties[CEConfiguration.CEClientUser];
            var locale = ActionContext.Request.Properties.ContainsKey(CEConfiguration.CELocale)
                        ? ActionContext.Request.Properties[CEConfiguration.CELocale].ToString() : "en-US";

            var billModel = new BillModel(InsightsEfRepository);
            var response = billModel.GetBills(clientUser, request, locale) ?? new BillResponse { Message = CEConfiguration.Response_NoData };

            return Request.CreateResponse(HttpStatusCode.OK, response);
        }
    }
}