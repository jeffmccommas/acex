﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using CE.Infrastructure;
using CE.Insights.Common.Filters;
using CE.Insights.Models;
using CE.Models;
using CE.Models.Insights;

namespace CE.Insights.Controllers.Version1
{
    /// <summary>
    /// The Benchmark API returns usage, cost and other measurements for one or more comparison groups.
    /// A utility can use this API to compare a customer's usage and costs to a peer group. 
    /// </summary>
    [RequireHttpsAndClientCertAttribute]
    public class BenchmarkController : ApiController {
        public static IInsightsEFRepository insightsRepository;

        /// <summary>
        /// Default constructor
        /// </summary>
        public BenchmarkController()
        {
            insightsRepository = new InsightsEfRepository();
        }

        /// <summary>
        /// Testability, passed in repository so not relying on data access through sql server
        /// </summary>
        /// <param name="InsightsRepository"></param>
        public BenchmarkController(InsightsEfRepository InsightsRepository)
        {
            if (InsightsRepository == null)
            {
                throw new ArgumentNullException("InsightsRepository is null!");
            }

            insightsRepository = InsightsRepository;
        }

        /// <summary>
        /// GET benchmark request with the parameters in an encoded string. Returns benchmark comparisons 
        /// between a utility customer and pre-defined comparison groups.
        /// </summary>
        /// <param name="enc"></param>
        /// /// <returns>The Benchmark response contains comparisons of cost, usage and other measurements for the specified benchmark groups.</returns>
        [ResponseType(typeof(BenchmarkResponse))]
        public HttpResponseMessage Get([FromUri] string enc)
        {
            string customerId = null;
            string accountId = null;
            string premiseId = null;
            DateTime? startDate = null;
            DateTime? endDate = null;
            string groups = null;
            string measurements = null;
            bool includeContent = false;
            int count = 1;

            foreach (var aa in this.ActionContext.ActionArguments)
            {
                if (aa.Key.ToLower() == "customerid")
                {
                    customerId = aa.Value.ToString();
                }
                else if (aa.Key.ToLower() == "accountid")
                {
                    accountId = aa.Value.ToString();
                }
                else if (aa.Key.ToLower() == "premiseid")
                {
                    premiseId = aa.Value.ToString();
                }
                else if (aa.Key.ToLower() == "startdate")
                {
                    startDate = DateTime.Parse(aa.Value.ToString());
                }
                else if (aa.Key.ToLower() == "enddate")
                {
                    endDate = DateTime.Parse(aa.Value.ToString());
                }
                else if (aa.Key.ToLower() == "groupkeys")
                {
                    groups = aa.Value.ToString();
                }
                else if (aa.Key.ToLower() == "measurementkeys")
                {
                    measurements = aa.Value.ToString();
                }
                else if (aa.Key.ToLower() == "includecontent")
                {
                    if (!(Boolean.TryParse(aa.Value.ToString(), out includeContent)))
                    {
                        includeContent = false;
                    }
                }
                else if (aa.Key.ToLower() == "count")
                {
                    if (!(int.TryParse(aa.Value.ToString(), out count)))
                    {
                        count = 1;
                    }
                }
            }

            var request = new BenchmarkRequest();

            request.CustomerId = customerId;
            request.AccountId = accountId;
            request.PremiseId = premiseId;
            if (!(startDate == null)) { request.StartDate = startDate.Value; }
            if (!(endDate == null)) { request.EndDate = endDate.Value; }
            request.GroupKeys = groups;
            request.MeasurementKeys = measurements;
            request.Count = count;
            request.IncludeContent = includeContent;

            // Validate model via model annotations.  This must execute here, since our enc is special. 
            bool success = true;
            var resultModelState = ValidateModels.Validate(request, out success);
            if (!success)
            {
                return (Request.CreateErrorResponse(HttpStatusCode.BadRequest, resultModelState));
            }

            return Get(request);
        }


        /// <summary>
        /// GET Benchmark request with un-encoded parameters. Returns benchmark comparisons 
        /// between a utility customer and pre-defined comparison groups.
        /// </summary>
        /// <param name="request"></param>
        /// <returns>The Benchmark response contains comparisons of cost, usage and other measurements for the specified benchmark groups.</returns>
        [ResponseType(typeof(BenchmarkResponse))]
        public HttpResponseMessage Get([FromUri] BenchmarkRequest request)
        {
            // Get ClientUser property
            var clientUser = ((ClientUser)this.ActionContext.Request.Properties[CEConfiguration.CEClientUser]);

            // finally make call
            BenchmarkResponse response = insightsRepository.GetBenchmark(clientUser.ClientID, request);

            if (response != null)
            {
                if (request.IncludeContent)
                {
                    var locale = (ActionContext.Request.Properties.ContainsKey(CEConfiguration.CELocale))
                        ? ActionContext.Request.Properties[CEConfiguration.CELocale].ToString() : "en-US";

                    // get and apply content to response
                    var benchmarkModel = new BenchmarkModel();
                    benchmarkModel.ApplyContent(response, clientUser, locale);
                }
            }
            else
            {
                response = new BenchmarkResponse { Message = CEConfiguration.Response_NoData };
            }

            return (Request.CreateResponse(HttpStatusCode.OK, response));
        }


    }
}
