﻿using CE.Infrastructure;
using CE.Insights.Common.Filters;
using CE.Insights.Models;
using CE.Models;
using CE.Models.Insights;
using CE.Models.Insights.Types;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Http;
using System.Web.Http.Description;

namespace CE.Insights.Controllers.Version1
{
    /// <summary>
    /// The Green Button Bill endpoint returns customer's bill in green button format.
    /// </summary>
    [RequireHttpsAndClientCert]
    public class GreenButtonBillController : ApiController
    {
        private ClientUser _clientUser;

        public ClientUser MyClientUser
        {
            get { return _clientUser ?? (_clientUser = GetClientUser()); }
            set { _clientUser = value; }
        }
        public GreenButtonBillController() { }

        /// <summary>
        /// GET GreenButtonBill request with the parameters in an encoded string. 
        /// Returns customer's bill in green button format as zip attachment or xml for single service.
        /// </summary>
        /// <param name="enc"></param>
        /// <returns></returns>
        [ResponseType(typeof(Status))]
        public HttpResponseMessage Get([FromUri] string enc)
        {
            string customerId = null;
            string accountId = null;
            string premiseId = null;
            string serviceContractId = null;

            foreach (var aa in ActionContext.ActionArguments)
            {
                switch (aa.Key.ToLower())
                {
                    case "customerid":
                        customerId = aa.Value.ToString();
                        break;
                    case "accountid":
                        accountId = aa.Value.ToString();
                        break;
                    case "premiseid":
                        premiseId = aa.Value.ToString();
                        break;
                    case "servicecontractid":
                        serviceContractId = aa.Value.ToString();
                        break;
                }
            }

            var request = new GreenButtonBillRequest
            {
                CustomerId = customerId,
                AccountId = accountId,
                PremiseId = premiseId,
                ServiceContractId = serviceContractId
            };

            // Validate model via model annotations.  This must execute here, since our enc is special. 
            bool success;
            var resultModelState = ValidateModels.Validate(request, out success);

            return !success ? Request.CreateErrorResponse(HttpStatusCode.BadRequest, resultModelState) : Get(request);
        }


        /// <summary>
        /// GET GreenButtonBill request with un-encoded parameters. 
        /// Returns customer's bill in green button format as zip attachment or xml for single service.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [ResponseType(typeof (Status))]
        public HttpResponseMessage Get([FromUri] GreenButtonBillRequest request)
        {
            HttpResponseMessage response;
            try
            {

                //var container = (IUnityContainer)Configuration.DependencyResolver.GetService(typeof(IUnityContainer));
                //var instance = container.Resolve<xxx>(name);
                var locale = ActionContext.Request.Properties.ContainsKey(CEConfiguration.CELocale)
                           ? ActionContext.Request.Properties[CEConfiguration.CELocale].ToString() : "en-US";

                var model = new GreenButtonBillModel(MyClientUser.ClientID, locale);


                var result = model.GetGreenButtonBill(MyClientUser, request);

                if (result != null)
                {
                    if (string.IsNullOrEmpty(result.ErrorMessage))
                    {
                        if (request.OutputFormat.Equals(GreenButtonOutputFormatType.Zip.ToString(),
                            StringComparison.InvariantCultureIgnoreCase))
                        {
                            response = new HttpResponseMessage(HttpStatusCode.OK);
                            result.File.Seek(0, SeekOrigin.Begin);
                            response.Content = new StreamContent(result.File);
                            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                            response.Content.Headers.ContentDisposition.FileName = result.FileName + ".zip";
                            response.Content.Headers.ContentLength = result.File.Length;

                        }
                        else
                        {

                            response = new HttpResponseMessage(HttpStatusCode.OK);
                            response.Content = new StringContent(result.FileXml, Encoding.UTF8, "application/xml");
                        }
                    }
                    else
                    {
                        var greenButtonBillResponse = new Status {Message = result.ErrorMessage };
                        response = Request.CreateResponse(HttpStatusCode.OK, greenButtonBillResponse);
                    }
                }
                else
                {
                    var greenButtonBillResponse = new Status { Message = CEConfiguration.Response_NoData };
                    response = Request.CreateResponse(HttpStatusCode.OK, greenButtonBillResponse);
                }

            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }


            return response;
        }
        


        /// <summary>
        /// Returns Client User object.
        /// </summary>
        /// <returns></returns>
        private ClientUser GetClientUser()
        {
            return (ClientUser)ActionContext.Request.Properties[CEConfiguration.CEClientUser];
        }
    }
}