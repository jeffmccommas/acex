﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using CE.Infrastructure;
using CE.Insights.Common.Filters;
using CE.Insights.Models;
using CE.Models;
using CE.Models.Insights;

namespace CE.Insights.Controllers.Version1
{
    /// <summary>
    /// The Rate API returns information about a rate, such as use charges, service charges, tier boundaries, 
    /// time of use boundaries, seasons, minimum charges and tax charges
    /// </summary>
    [RequireHttpsAndClientCertAttribute]
    public class RateController : ApiController
    {
        private ClientUser _clientUser;

        public ClientUser MyClientUser
        {
            get { return _clientUser ?? (_clientUser = GetClientUser()); }
            set { _clientUser = value; }
        }

        public RateController() { }

        /// <summary>
        /// GET Rate request with the parameters in an encoded string. 
        /// Returns rate information including use charges, service charges, tier boundaries, time of use boundaries, 
        /// seasons, minimum charges and tax charges.
        /// </summary>
        /// <param name="enc"></param>
        /// <returns>The GET Rate response includes use charges, service charges, tier boundaries, time of use boundaries, seasons, minimum charges and tax charges.</returns>
        [ResponseType(typeof(RateResponse))]
        public HttpResponseMessage Get([FromUri] string enc)
        {
            string rateClass = null;
            DateTime? startDate = null;
            DateTime? endDate = null;
            DateTime? projectedEndDate = null;
            DateTime? date = null;
            bool useProjectedEndDateToFinalizeTier = false;
            bool includeRateDetails = false;
            bool includeFinalizedTierBoundaries = false;
            bool includeContent = false;

            foreach (var aa in this.ActionContext.ActionArguments)
            {
                switch (aa.Key.ToLower())
                {
                    case "rateclass":
                        rateClass = aa.Value.ToString();
                        break;
                    case "date":
                        date = DateTime.Parse(aa.Value.ToString());
                        break;
                    case "startdate":
                        startDate = DateTime.Parse(aa.Value.ToString());
                        break;
                    case "enddate":
                        endDate = DateTime.Parse(aa.Value.ToString());
                        break;
                    case "projectedenddate":
                        projectedEndDate = DateTime.Parse(aa.Value.ToString());
                        break;
                    case "useprojectedenddatetofinalizetier":
                         Boolean.TryParse(aa.Value.ToString(), out useProjectedEndDateToFinalizeTier);
                        break;
                    case "includeratedetails":
                        Boolean.TryParse(aa.Value.ToString(), out includeRateDetails);
                        break;
                    case "includefinalizedtierboundaries":
                        Boolean.TryParse(aa.Value.ToString(), out includeFinalizedTierBoundaries);
                        break;
                    case "includecontent":
                        Boolean.TryParse(aa.Value.ToString(), out includeContent);
                        break;
                }
            }

            var request = new RateRequest();
            request.RateClass = rateClass;
            request.Date = date.Value;
            if (!(startDate == null)) { request.StartDate = startDate.Value; }
            if (!(endDate == null)) { request.EndDate = endDate.Value; }
            if (!(projectedEndDate == null)) { request.ProjectedEndDate = projectedEndDate.Value; }
            request.UseProjectedEndDateToFinalizeTier = useProjectedEndDateToFinalizeTier;
            request.IncludeRateDetails = includeRateDetails;
            request.IncludeFinalizedTierBoundaries = includeFinalizedTierBoundaries;
            request.IncludeContent = includeContent;

            // Validate model via model annotations.  This must execute here, since our enc is special. 
            bool success = true;
            var resultModelState = ValidateModels.Validate(request, out success);
            if (!success)
            {
                return (Request.CreateErrorResponse(HttpStatusCode.BadRequest, resultModelState));
            }

            return Get(request);
        }

        /// <summary>
        /// GET Rate request with un-encoded parameters. 
        /// Returns rate information including use charges, service charges, tier boundaries, time of use boundaries, 
        /// seasons, minimum charges and tax charges.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [ResponseType(typeof(RateResponse))]
        public HttpResponseMessage Get([FromUri] RateRequest request)
        {
            var response = new RateResponse();

            try
            {
                var locale = (ActionContext.Request.Properties.ContainsKey(CEConfiguration.CELocale))
                             ? ActionContext.Request.Properties[CEConfiguration.CELocale].ToString() : "en-US";

                var model = new RateClassModel();
                response = model.GetRate(MyClientUser, request);
                
                if (response != null)
                {
                    if (request.IncludeContent)
                    {
                        model.ApplyContent(response, MyClientUser, locale);
                    }
                }
                else
                {
                    return (Request.CreateErrorResponse(HttpStatusCode.InternalServerError, response.Message));
                }

            }
            catch (Exception ex)
            {
                return (Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message));
            }

            return (Request.CreateResponse(HttpStatusCode.OK, response));
        }

        /// <summary>
        /// Returns Client User object.
        /// </summary>
        /// <returns></returns>
        private ClientUser GetClientUser()
        {
            return ((ClientUser)ActionContext.Request.Properties[CEConfiguration.CEClientUser]);
        }
    }
}