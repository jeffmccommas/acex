﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using CE.Infrastructure;
using CE.Insights.Common.Filters;
using CE.Models;
using CE.Models.Insights;

namespace CE.Insights.Controllers.Version1
{
    /// <summary>
    /// The EndpointTracking endpoint gives accountants and program managers a view into how, when and how often
    /// a client is calling the Insights APIs. 
    /// </summary>
    [RequireHttpsAndClientCertAttribute]
    public class EndpointTrackingController : ApiController {
        public static IInsightsEFRepository insightsEFRepository;

        public EndpointTrackingController()
        {
            insightsEFRepository = new InsightsEfRepository();
        }

        /// <summary>
        /// Testability, passed in repository so not relying on data access through sql server
        /// </summary>
        /// <param name="InsightsEFRepository"></param>
        public EndpointTrackingController(IInsightsEFRepository InsightsEFRepository)
        {
            if (InsightsEFRepository == null)
            {
                throw new ArgumentNullException("InsightsRepository is null!");
            }
            insightsEFRepository = InsightsEFRepository;

        }

        /// <summary>
        /// Returns client tracking totals and details for a given date range, 
        /// optionally filtered by user, verb, endpoint and message Id.
        /// </summary>
        /// <param name="enc"></param>
        /// <returns></returns>
        [ResponseType(typeof(EndpointTrackingResponse))]
        public HttpResponseMessage Get([FromUri] string enc)
        {
            int endpointId = 0;
            string messageId = null;
            DateTime? startDate = null;
            DateTime? endDate = null;
            int pageIndex = 1;
            bool includeDetails = false;
            string verb = null;
            int userId = 0;

            foreach (var aa in this.ActionContext.ActionArguments)
            {
                switch (aa.Key.ToLower())
                {
                    case "endpointid":
                        if (!(int.TryParse(aa.Value.ToString(), out endpointId)))
                        {
                            endpointId = 0;
                        }
                        break;
                    case "messageid":
                        messageId = aa.Value.ToString();
                        break;
                    case "startdate":
                        startDate = DateTime.Parse(aa.Value.ToString());
                        break;
                    case "enddate":
                        endDate = DateTime.Parse(aa.Value.ToString());
                        break;
                    case "pageindex":
                        if (!(int.TryParse(aa.Value.ToString(), out pageIndex)))
                        {
                            pageIndex = 1;
                        }
                        break;

                    case "includedetails":
                        Boolean.TryParse(aa.Value.ToString(), out includeDetails);
                        break;
                    case "verb":
                        verb = aa.Value.ToString();
                        break;
                    case "userid":
                        if (!(int.TryParse(aa.Value.ToString(), out userId)))
                        {
                            userId = 0;
                        }
                        break;
                }
            }

            var request = new EndpointTrackingRequest();
            if (!(startDate == null)) { request.StartDate = startDate.Value; }
            if (!(endDate == null)) { request.EndDate = endDate.Value; }
            if ((endpointId > 0)) { request.EndpointId = endpointId; };
            request.IncludeDetails = includeDetails;
            request.MessageId = messageId;
            request.PageIndex = pageIndex;
            request.Verb = verb;
            if ((endpointId > 0)) { request.UserId = userId; }


            // Validate model via model annotations.  This must execute here, since our enc is special. 
            bool success = true;
            var resultModelState = ValidateModels.Validate(request, out success);
            if (!success)
            {
                return (Request.CreateErrorResponse(HttpStatusCode.BadRequest, resultModelState));
            }

            return Get(request);
        }

        /// <summary>
        /// Returns client tracking totals and details for a given date range, 
        /// optionally filtered by user, verb, endpoint and message Id.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [ResponseType(typeof(EndpointTrackingResponse))]
        public HttpResponseMessage Get([FromUri] EndpointTrackingRequest request)
        {
            // Get ClientUser property
            var clientUser = ((ClientUser)this.ActionContext.Request.Properties[CEConfiguration.CEClientUser]);

            // finally make call
            EndpointTrackingResponse response = insightsEFRepository.GetEndpointTracking(clientUser.ClientID, request);

            if (response != null)
            {
                // get and apply content to response               
            }
            else
            {
                response = new EndpointTrackingResponse { Message = CEConfiguration.Response_NoData };
            }

            return (Request.CreateResponse(HttpStatusCode.OK, response));
        }
    }
}
