﻿using CE.Infrastructure;
using CE.Insights.Common.Filters;
using CE.Insights.Models;
using CE.Models;
using CE.Models.Insights;
using Microsoft.Practices.Unity;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace CE.Insights.Controllers.Version1
{
    /// <summary>
    /// The BillToDate endpoint returns calculated bill data for the current bill period (from Cassandra) based on the customer’s AMI data.
    /// </summary>
    [RequireHttpsAndClientCertAttribute]
    public class BillToDateController : ApiController
    {
        private ClientUser _clientUser;

        private readonly IUnityContainer _container;

        public ClientUser MyClientUser
        {
            get { return _clientUser ?? (_clientUser = GetClientUser()); }
            set { _clientUser = value; }
        }

        public BillToDateController()
        {
            _container = (IUnityContainer)GlobalConfiguration.Configuration.DependencyResolver.GetService(typeof(IUnityContainer));
        }

        public BillToDateController(IUnityContainer unityContainer)
        {
            _container = unityContainer;
        }

        /// <summary>
        /// GET BillToDate request with the parameters in an encoded string. 
        /// Returns the usage and billed amounts for the current bill period based on the customer’s AMI data.
        /// </summary>
        /// <param name="enc"></param>
        /// <returns></returns>
        [ResponseType(typeof(BillToDateResponse))]
        public HttpResponseMessage Get([FromUri] string enc)
        {
            string customerId = null;
            string accountId = null;
            bool includeContent = false;
            bool forceCalc = false;
            bool showLog = false;

            foreach (var aa in ActionContext.ActionArguments)
            {
                switch (aa.Key.ToLower())
                {
                    case "customerid":
                        customerId = aa.Value.ToString();
                        break;
                    case "accountid":
                        accountId = aa.Value.ToString();
                        break;
                    case "includecontent":
                        bool.TryParse(aa.Value.ToString(), out includeContent);
                        break;
                    case "forcecalc":
                        bool.TryParse(aa.Value.ToString(), out forceCalc);
                        break;
                    case "showlog":
                        bool.TryParse(aa.Value.ToString(), out showLog);
                        break;
                }
            }

            var request = new BillToDateRequest
            {
                CustomerId = customerId,
                AccountId = accountId,
                IncludeContent = includeContent,
                ForceCalc = forceCalc,
                ShowLog = showLog
            };

            // Validate model via model annotations.  This must execute here, since our enc is special. 
            bool success;
            var resultModelState = ValidateModels.Validate(request, out success);

            return !success ? Request.CreateErrorResponse(HttpStatusCode.BadRequest, resultModelState) : Get(request);
        }

        /// <summary>
        /// GET BillToDate request with un-encoded parameters. 
        /// Returns the usage and billed amounts for the current bill period based on the customer’s AMI data.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [ResponseType(typeof(BillToDateResponse))]
        public HttpResponseMessage Get([FromUri] BillToDateRequest request)
        {
            BillToDateResponse response;
            try {
                var locale = ActionContext.Request.Properties.ContainsKey(CEConfiguration.CELocale)
                           ? ActionContext.Request.Properties[CEConfiguration.CELocale].ToString() : "en-US";

                //var container = (IUnityContainer)Configuration.DependencyResolver.GetService(typeof(IUnityContainer));
                //var instance = container.Resolve<xxx>(name);

                var model = new BillToDateModel(_container);

                
                response = model.GetBillToDate(MyClientUser, request);

                if (response != null)
                {
                    if (request.IncludeContent)
                    {
                        model.ApplyContent(response, MyClientUser, locale);
                    }
                }
                else
                {
                    response = new BillToDateResponse { Message = CEConfiguration.Response_NoData };
                }               

            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }


            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        /// <summary>
        /// Post BillToDate request, including the meter data, AMI data and settings. 
        /// The usage and billed amounts for the current bill period are based on the input data
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [ResponseType(typeof(BillToDateResponse))]
        public HttpResponseMessage Post([FromBody] BillToDatePostRequest request)
        {
            BillToDateResponse response;
            try
            {
                var clientUser = MyClientUser;

                BillToDateModel model;

                if (request.Configurations != null && request.Configurations.Count > 0)
                {
                    model = new BillToDateModel(_container, clientUser.ClientID, request.Configurations);
                }
                else
                {
                    model = new BillToDateModel(_container, clientUser.ClientID);
                }


                response = model.GetBillToDate(clientUser, request);

            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        /// <summary>
        /// Returns Client User object.
        /// </summary>
        /// <returns></returns>
        private ClientUser GetClientUser()
        {
            return (ClientUser)ActionContext.Request.Properties[CEConfiguration.CEClientUser];
        }
    }
}