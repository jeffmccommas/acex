﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using CE.Infrastructure;
using CE.Insights.Common.Filters;
using CE.Insights.Models;
using CE.Models;
using CE.Models.Insights;

namespace CE.Insights.Controllers.Version1
{

    /// <summary>
    /// The ActionPlan endpoint stores and retrieves action statuses for a particular customer, account and premise.
    /// </summary>
    [RequireHttpsAndClientCertAttribute]
    public class ActionPlanController : ApiController
    {
        public static IInsightsEFRepository InsightsEfRepository;
        private IActionPlanModel _actionPlanModel;

        public ActionPlanController()
        {
            InsightsEfRepository = new InsightsEfRepository();
        }

        /// <summary>
        /// Testability, passed in repository so not relying on data access through sql server
        /// </summary>
        /// <param name="insightsEfRepository"></param>
        public ActionPlanController(IInsightsEFRepository insightsEfRepository)
        {
            if (insightsEfRepository == null)
            {
                // ReSharper disable once NotResolvedInText
                throw new ArgumentNullException("InsightsRepository is null!");
            }
            InsightsEfRepository = insightsEfRepository;

        }

        /// <summary>
        /// Testability, passed in model
        /// </summary>
        /// <param name="insightsEfRepository"></param>
        /// <param name="actionPlanModel"></param>
        public ActionPlanController(IInsightsEFRepository insightsEfRepository, IActionPlanModel actionPlanModel)
        {
            if (insightsEfRepository == null)
            {
                // ReSharper disable once NotResolvedInText
                throw new ArgumentNullException("InsightsRepository is null!");
            }
            InsightsEfRepository = insightsEfRepository;
            _actionPlanModel = actionPlanModel;

        }

        /// <summary>
        /// GET ActionPlan request with the parameters in an encoded string. Returns a customer's action statuses.
        /// </summary>
        /// <param name="enc"></param>
        /// <returns>Action status for a customer.</returns>
        [ResponseType(typeof(ActionPlanResponse))]
        public HttpResponseMessage Get([FromUri] string enc)
        {
            string customerId = null;
            string accountId = null;
            string premiseId = null;

            foreach (var aa in ActionContext.ActionArguments)
            {
                switch (aa.Key.ToLower())
                {
                    case "customerid":
                        customerId = aa.Value.ToString();
                        break;
                    case "accountid":
                        accountId = aa.Value.ToString();
                        break;
                    case "premiseid":
                        premiseId = aa.Value.ToString();
                        break;
                }
            }

            var request = new ActionPlanRequest();

            request.CustomerId = customerId;
            request.AccountId = accountId;
            request.PremiseId = premiseId;

            // Validate model via model annotations.  This must execute here, since our enc is special. 
            bool success;
            var resultModelState = ValidateModels.Validate(request, out success);
            if (!success)
            {
                return (Request.CreateErrorResponse(HttpStatusCode.BadRequest, resultModelState));
            }

            return Get(request);
        }


        /// <summary>
        /// GET ActionPlan request with un-encoded parameters. Returns a customer's action statuses. 
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Action status for a customer.</returns>
        [ResponseType(typeof(ActionPlanResponse))]
        public HttpResponseMessage Get([FromUri] ActionPlanRequest request)
        {
            ActionPlanResponse response;

            var clientUser = ((ClientUser)ActionContext.Request.Properties[CEConfiguration.CEClientUser]);

            response = InsightsEfRepository.GetActionPlan(clientUser.ClientID, request);

            if (response != null)
            {


            }
            else
            {
                response = new ActionPlanResponse { Message = CEConfiguration.Response_NoData };
            }

            return (Request.CreateResponse(HttpStatusCode.OK, response));
        }


        /// <summary>
        /// Stores new action statuses for a customer.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [ResponseType(typeof(ActionPlanPostResponse))]
        public HttpResponseMessage Post([FromBody] ActionPlanPostRequest request)
        {
            ActionPlanPostResponse response;
            List<string> invalidActions = new List<string>();
            List<string> duplicateActions = new List<string>();
            List<string> invalidActionStatus = new List<string>();
            List<string> invalidActionSource = new List<string>();
            // Get ClientUser property
            var clientUser = ((ClientUser)ActionContext.Request.Properties[CEConfiguration.CEClientUser]);
            var locale = (ActionContext.Request.Properties.ContainsKey(CEConfiguration.CELocale))
                        ? ActionContext.Request.Properties[CEConfiguration.CELocale].ToString() : "en-US";

            if (request.Validate)
            {
                // Validate actions
                IActionPlanModel actionModel = new ActionPlanModel();
                if (_actionPlanModel != null)
                    actionModel = _actionPlanModel;
                var valid = actionModel.ValidateActions(clientUser, locale, clientUser.Environment.ToString(), request, invalidActions, duplicateActions, invalidActionStatus, invalidActionSource);

                if (valid)
                {
                    try
                    {
                        // Insert/Update attributes
                        response = InsightsEfRepository.PostActionPlan(clientUser.ClientID, request);
                    }
                    catch (Exception)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, CEConfiguration.Error_PostFailed);
                    }
                }
                else
                {
                    var msg = string.Empty;

                    if (invalidActions.Count > 0)
                    {
                        var invalidAct = String.Join(",", invalidActions);
                        msg = CEConfiguration.Invalid_Action + " --> " + invalidAct;
                    }
                    if (duplicateActions.Count > 0)
                    {
                        var duplicateAct = String.Join(",", duplicateActions);
                        if (invalidActions.Count > 0) msg += "; ";
                        msg += CEConfiguration.Invalid_DuplicateAction + " --> " + duplicateAct;
                    }
                    if (invalidActionStatus.Count > 0)
                    {
                        var invalidActionStat = String.Join(",", invalidActionStatus);
                        if (invalidActions.Count > 0 || duplicateActions.Count > 0) msg += "; ";
                        msg += CEConfiguration.Invalid_ActionStatus + " --> " + invalidActionStat;
                    }
                    if (invalidActionSource.Count > 0)
                    {
                        var invalidActionSour = String.Join(",", invalidActionSource);
                        if (invalidActions.Count > 0 || duplicateActions.Count > 0 || invalidActionStatus.Count > 0) msg += "; ";
                        msg += CEConfiguration.Invalid_ActionSource + " --> " + invalidActionSour;
                    }

                    // Invalid actions were detected, send 400 response with message containing listed invalid attributes
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, msg);
                }
            }
            else
            {
                try
                {
                    // Insert/Update attributes
                    response = InsightsEfRepository.PostActionPlan(clientUser.ClientID, request);
                }
                catch (Exception)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, CEConfiguration.Error_PostFailed);
                }
            }

            return (Request.CreateResponse(HttpStatusCode.OK, response));

        }
    }
}