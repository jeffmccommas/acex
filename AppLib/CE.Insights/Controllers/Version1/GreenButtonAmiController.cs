﻿using CE.Infrastructure;
using CE.Insights.Common.Filters;
using CE.Insights.Models;
using CE.Models;
using CE.Models.Insights;
using CE.Models.Insights.Types;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Http;
using System.Web.Http.Description;
using Microsoft.Practices.Unity;

namespace CE.Insights.Controllers.Version1
{
    /// <summary>
    /// The Green Button AMI endpoint returns customer's AMI in green button format.
    /// </summary>
    [RequireHttpsAndClientCert]
    public class GreenButtonAmiController : ApiController
    {
        private ClientUser _clientUser;
        private IUnityContainer _container;

        public ClientUser MyClientUser
        {
            get { return _clientUser ?? (_clientUser = GetClientUser()); }
            set { _clientUser = value; }
        }

        public GreenButtonAmiController()
        {
            _container = (IUnityContainer)GlobalConfiguration.Configuration.DependencyResolver.GetService(typeof(IUnityContainer));
        }

        public GreenButtonAmiController(IUnityContainer unityContainer)
        {
            _container = unityContainer;
        }

        /// <summary>
        /// GET GreenButtonAmi request with the parameters in an encoded string. 
        /// Returns customer's AMI in green button format as zip attachment or xml for single meter.
        /// </summary>
        /// <param name="enc"></param>
        /// <returns></returns>
        [ResponseType(typeof(Status))]
        public HttpResponseMessage Get([FromUri] string enc)
        {
            string customerId = null;
            string accountId = null;
            string premiseId = null;
            string meterids = null;
            DateTime? startDate = null;
            DateTime? endDate = null;
            string outputformat = null;

            foreach (var aa in ActionContext.ActionArguments)
            {
                switch (aa.Key.ToLower())
                {
                    case "customerid":
                        customerId = aa.Value.ToString();
                        break;
                    case "accountid":
                        accountId = aa.Value.ToString();
                        break;
                    case "premiseid":
                        premiseId = aa.Value.ToString();
                        break;
                    case "meterids":
                        meterids = aa.Value.ToString();
                        break;
                    case "startdate":
                        startDate = DateTime.Parse(aa.Value.ToString());
                        break;
                    case "enddate":
                        endDate = DateTime.Parse(aa.Value.ToString());
                        break;
                    case "outputformat":
                        outputformat = aa.Value.ToString();
                        break;
                }
            }

            var request = new GreenButtonAmiRequest
            {
                CustomerId = customerId,
                AccountId = accountId,
                PremiseId = premiseId,
                EndDate = endDate.Value,
                StartDate = startDate.Value,
                MeterIds = meterids
            };
            
            if (!string.IsNullOrEmpty(outputformat))
                request.OutputFormat = outputformat;
            // Validate model via model annotations.  This must execute here, since our enc is special. 
            bool success;
            var resultModelState = ValidateModels.Validate(request, out success);

            return !success ? Request.CreateErrorResponse(HttpStatusCode.BadRequest, resultModelState) : Get(request);
        }


        /// <summary>
        /// GET GreenButtonAMI request with un-encoded parameters. 
        /// Returns customer's AMI in green button format as zip attachment or xml for single meter.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [ResponseType(typeof(Status))]
        public HttpResponseMessage Get([FromUri] GreenButtonAmiRequest request)
        {
            HttpResponseMessage response;
            try
            {

                //var container = (IUnityContainer)Configuration.DependencyResolver.GetService(typeof(IUnityContainer));
                //var instance = container.Resolve<xxx>(name);
                var locale = ActionContext.Request.Properties.ContainsKey(CEConfiguration.CELocale)
                           ? ActionContext.Request.Properties[CEConfiguration.CELocale].ToString() : "en-US";
                
                var model = new GreenButtonAmiModel(MyClientUser.ClientID,locale,_container);


                var result = model.GetGreenButtonAmi(MyClientUser, request);

                if (result != null)
                {
                    if (string.IsNullOrEmpty(result.ErrorMessage))
                    {
                        if (request.OutputFormat.Equals(GreenButtonOutputFormatType.Zip.ToString(),
                            StringComparison.InvariantCultureIgnoreCase))
                        {
                            response = new HttpResponseMessage(HttpStatusCode.OK);
                            result.File.Seek(0, SeekOrigin.Begin);
                            response.Content = new StreamContent(result.File);
                            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                            response.Content.Headers.ContentDisposition.FileName = result.FileName;
                            response.Content.Headers.ContentLength = result.File.Length;

                        }
                        else
                        {

                            response = new HttpResponseMessage(HttpStatusCode.OK);
                            response.Content = new StringContent(result.FileXml, Encoding.UTF8, "application/xml");
                        }
                    }
                    else
                    {
                        var greenButtonAmiResponse = new Status { Message = result.ErrorMessage };
                        response = Request.CreateResponse(HttpStatusCode.OK, greenButtonAmiResponse);
                    }
                }
                else
                {
                    var greenButtonAmiResponse = new Status { Message = CEConfiguration.Response_NoData };
                    response = Request.CreateResponse(HttpStatusCode.OK, greenButtonAmiResponse);
                }

            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }


            return response;
        }



        /// <summary>
        /// Returns Client User object.
        /// </summary>
        /// <returns></returns>
        private ClientUser GetClientUser()
        {
            return (ClientUser)ActionContext.Request.Properties[CEConfiguration.CEClientUser];
        }
    }
}