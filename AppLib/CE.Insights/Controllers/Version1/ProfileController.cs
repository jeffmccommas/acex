﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Runtime.Remoting.Messaging;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Description;
using CE.Infrastructure;
using CE.Insights.Common.Filters;
using CE.Insights.Models;
using CE.MeasuresEngine;
using CE.Models;
using CE.Models.Insights;
using CM = CE.ContentModel;

namespace CE.Insights.Controllers.Version1
{
    /// <summary>
    /// The Profile API enables a utility to store and retrieve profile values for a customer, account and premise. 
    /// </summary>
    [RequireHttpsAndClientCertAttribute]
    public class ProfileController : ApiController {
        public static IInsightsEFRepository InsightsEfRepository;
        public static CM.ContentProviderType ContentType;
        private IProfileModel _profileModel;

        public ProfileController()
        {
            InsightsEfRepository = new InsightsEfRepository();
            ContentType = CM.ContentProviderType.Contentful;
        }
        /// <summary>
        /// Testability, passed in repository so not relying on data access through sql server and set contenttype to Mock enum so not using database for content provider
        /// </summary>
        /// <param name="insightsEfRepository"></param>
        public ProfileController(IInsightsEFRepository insightsEfRepository)
        {
            if (insightsEfRepository == null)
            {
                // ReSharper disable once NotResolvedInText
                throw new ArgumentNullException("InsightsRepository is null!");
            }
            InsightsEfRepository = insightsEfRepository;
            ContentType = CM.ContentProviderType.Mock;
        }


        /// <summary>
        /// Testability, passed in model
        /// </summary>
        /// <param name="insightsEfRepository"></param>
        /// <param name="profileModel"></param>
        public ProfileController(IInsightsEFRepository insightsEfRepository, IProfileModel profileModel)
        {
            if (insightsEfRepository == null)
            {
                // ReSharper disable once NotResolvedInText
                throw new ArgumentNullException("InsightsRepository is null!");
            }
            InsightsEfRepository = insightsEfRepository;
            ContentType = CM.ContentProviderType.Mock;
            _profileModel = profileModel;
        }

        /// <summary>
        /// GET Profile request with the parameters in an encoded string. Returns the current profile values for a customer, account and/or premise. 
        /// </summary>
        /// <param name="enc"></param>
        /// <returns>Returns the current profile values for a customer, account and/or premise. </returns>
        [ResponseType(typeof(ProfileResponse))]
        public HttpResponseMessage Get([FromUri] string enc)
        {
            string customerId = null;
            string accountId = null;
            string premiseId = null;
            string profileattributes = null;
            string sources = null;
            string enduses = null;
            string appliances = null;
            DateTime? startDate = null;
            DateTime? endDate = null;
            bool includeMissing = false;
            bool includeContent = false;
            string entityLevel = null;

            foreach (var aa in ActionContext.ActionArguments)
            {
                switch (aa.Key.ToLower())
                {
                    case "customerid":
                        customerId = aa.Value.ToString();
                        break;
                    case "accountid":
                        accountId = aa.Value.ToString();
                        break;
                    case "premiseid":
                        premiseId = aa.Value.ToString();
                        break;
                    case "profileattributekeys":
                        profileattributes = aa.Value.ToString();
                        break;
                    case "sourcekeys":
                        sources = aa.Value.ToString();
                        break;
                    case "appliancekeys":
                        appliances = aa.Value.ToString();
                        break;
                    case "endusekeys":
                        enduses = aa.Value.ToString();
                        break;
                    case "startdate":
                        startDate = DateTime.Parse(aa.Value.ToString());
                        break;
                    case "enddate":
                        endDate = DateTime.Parse(aa.Value.ToString());
                        break;
                    case "includemissing":
                        bool.TryParse(aa.Value.ToString(), out includeMissing);
                        break;
                    case "includecontent":
                        bool.TryParse(aa.Value.ToString(), out includeContent);
                        break;
                    case "entitylevel":
                        entityLevel = aa.Value.ToString();
                        break;
                }
            }

            var request = new ProfileRequest
            {
                CustomerId = customerId,
                AccountId = accountId,
                PremiseId = premiseId,
                ProfileAttributeKeys = profileattributes,
                SourceKeys = sources,
                ApplianceKeys = appliances,
                EndUseKeys = enduses
            };

            if (!(startDate == null))
            {
                request.StartDate = startDate.Value;
            }
            if (!(endDate == null))
            {
                request.EndDate = endDate.Value;
            }
            request.IncludeMissing = includeMissing;
            request.IncludeContent = includeContent;
            request.EntityLevel = entityLevel;

            // Validate model via model annotations.  This must execute here, since our enc is special. 
            bool success;
            var resultModelState = ValidateModels.Validate(request, out success);
            return !success ? Request.CreateErrorResponse(HttpStatusCode.BadRequest, resultModelState) : Get(request);
        }


        /// <summary>
        /// GET Profile request with un-encoded parameters. Returns the current profile values for a customer, account and/or premise
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Returns the current profile values for a customer, account and/or premise. </returns>
        [ResponseType(typeof(ProfileResponse))]
        public HttpResponseMessage Get([FromUri] ProfileRequest request)
        {
            var clientUser = (ClientUser)ActionContext.Request.Properties[CEConfiguration.CEClientUser];
            var locale = ActionContext.Request.Properties.ContainsKey(CEConfiguration.CELocale)
                        ? ActionContext.Request.Properties[CEConfiguration.CELocale].ToString() : "en-US";

            var response = InsightsEfRepository.GetProfile(clientUser.ClientID, request);

            if (response != null)
            {
                // Process attributes in response
                var profileModel = new ProfileModel();
                profileModel.ProcessAttributes(clientUser, locale, clientUser.Environment.ToString(), response, request);

                if (request.IncludeMissing)
                {
                    profileModel.IncludeMissingAttributes(clientUser, locale, clientUser.Environment.ToString(), response, request);
                }

                if (request.IncludeContent)
                {
                    profileModel.ApplyContent(response, clientUser, locale);
                }
            }
            else
            {
                response = new ProfileResponse { Message = CEConfiguration.Response_NoData };
            }

            return (Request.CreateResponse(HttpStatusCode.OK, response));
        }


        /// <summary>
        /// Stores new profile values for a customer and their accounts and premises.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [ResponseType(typeof(ProfilePostResponse))]
        public HttpResponseMessage Post([FromBody] ProfilePostRequest request)
        {
            ProfilePostResponse response;
            var invalidAttributes = new List<string>();
            var duplicateAttributes = new List<string>();
            var invalidValueAttributes = new List<string>();
            var invalidSourceAttributes = new List<string>();

            if (request == null)
            {
                // No data was posted when data is expected.
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, CEConfiguration.Error_PostEmpty);
            }

            // Get ClientUser property
            var clientUser = (ClientUser)ActionContext.Request.Properties[CEConfiguration.CEClientUser];
            var locale = ActionContext.Request.Properties.ContainsKey(CEConfiguration.CELocale)
                        ? ActionContext.Request.Properties[CEConfiguration.CELocale].ToString() : "en-US";

            if (request.Validate)
            {
                // Validate attribute list
                IProfileModel profileModel = new ProfileModel();
                if (_profileModel != null)
                    profileModel = _profileModel;
                var valid = profileModel.ValidateAttributes(clientUser, locale, clientUser.Environment.ToString(), request, invalidAttributes, duplicateAttributes, invalidValueAttributes, invalidSourceAttributes);

                if (valid)
                {
                    try
                    {
                        // Insert/Update attributes
                        response = InsightsEfRepository.PostProfile(clientUser.ClientID, request);

                        // recalculate measures, which includes both billdisagg & measures
                        ExecuteMe meas = ExecuteMeasures;
                        meas.BeginInvoke(clientUser.ClientID, request, MeasuresComplete, "This message is from Main() thread " + Thread.CurrentThread.ManagedThreadId);

                    }
                    catch (Exception)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, CEConfiguration.Error_PostFailed);
                    }
                }
                else
                {
                    var msg = string.Empty;

                    if (invalidAttributes.Count > 0)
                    {
                        var invalidAttr = string.Join(",", invalidAttributes);
                        msg = CEConfiguration.Invalid_ProfileAttribute + " --> " + invalidAttr;
                    }
                    if (duplicateAttributes.Count > 0)
                    {
                        var duplicateAttr = string.Join(",", duplicateAttributes);
                        if (invalidAttributes.Count > 0) msg += "; ";
                        msg += CEConfiguration.Invalid_DuplicateProfileAttributeKey + " --> " + duplicateAttr;
                    }

                    if (invalidValueAttributes.Count > 0)
                    {
                        var invalidValueAttr = string.Join(",", invalidValueAttributes);
                        if (invalidAttributes.Count > 0 || duplicateAttributes.Count > 0) msg += "; ";
                        msg += CEConfiguration.Invalid_ProfileAttributeValue + " --> " + invalidValueAttr;
                    }

                    if (invalidSourceAttributes.Count > 0)
                    {
                        var invalidSourceAttr = string.Join(",", invalidSourceAttributes);
                        if (invalidAttributes.Count > 0 || duplicateAttributes.Count > 0 || invalidValueAttributes.Count > 0) msg += "; ";
                        msg += CEConfiguration.Invalid_ProfileAttributeSource + " --> " + invalidSourceAttr;
                    }

                    // Invalid attributes were detected, send 400 response with message containing listed invalid attributes
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, msg);
                }
            }
            else
            {
                try
                {
                    // Insert/Update attributes
                    response = InsightsEfRepository.PostProfile(clientUser.ClientID, request);

                    // calculate measures, which includes both billdisagg & measures
                    ExecuteMe meas = ExecuteMeasures;
                    meas.BeginInvoke(clientUser.ClientID, request, MeasuresComplete, "This message is from Main() thread " + Thread.CurrentThread.ManagedThreadId);

                }
                catch (Exception)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, CEConfiguration.Error_PostFailed);
                }
            }

            return (Request.CreateResponse(HttpStatusCode.OK, response));
        }


        /// <summary>
        /// Use this to refresh/recalculate both disagg and measures.
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="req"></param>
        private static void ExecuteMeasures(int clientId, ProfilePostRequest req)
        {
            if (req?.Customer == null)
            {
                return;
            }

            var customerId = req.Customer.Id;

            foreach (var a in req.Customer.Accounts)
            {
                var accountId = a.Id;

                foreach (var p in a.Premises)
                {
                    var premiseId = p.Id;

                    var dalBd = new BillDisagg.DataAccess.DataAccessLayer();
                    var dalMe = new MeasuresEngine.DataAccess.DataAccessLayer();

                    var factory = new MeasuresEngineManagerFactory();
                    var manager = factory.CreateMeasuresEngineManager(clientId);

                    try
                    {
                        var result = manager.ExecuteMeasures(customerId, accountId, premiseId);

                        // save billDisagg and measures results to tables                          
                        dalBd.SaveBillDisagg(clientId, customerId, accountId, premiseId, result.BillDisaggFlatTableResult);
                        dalMe.SaveMeasures(clientId, customerId, accountId, premiseId, result.FlatResultTable);
                    }
                    catch
                    {
                        // for now, catch and dump errors here, we do not want profile post to fail because of this
                        // throw;
                    }

                }
            }
        }


        public delegate void ExecuteMe(int clientId, ProfilePostRequest req);

        private static void MeasuresComplete(IAsyncResult iftAr)
        {
            var ar = (AsyncResult)iftAr;
            var bp = (ExecuteMe)ar.AsyncDelegate;
            bp.EndInvoke(iftAr);
        }

    }

}