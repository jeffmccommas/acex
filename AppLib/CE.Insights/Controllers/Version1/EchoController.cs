﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Tracing;
using CE.Infrastructure;
using CE.Insights.Common.Filters;
using CE.Models;
using CE.Models.Insights;
using CM = CE.ContentModel;

namespace CE.Insights.Controllers.Version1
{
    /// <summary>
    /// The purpose of the Echo endpoint to test if the service is up and binding correctly to various input.
    /// It simply echoes an input parameter back to the user. 
    /// GET Echo can also be called to keep the application "alive" so that start up times are always fast.  
    /// </summary>
    [RequireHttpsAndClientCertAttribute]
    public class EchoController : ApiController {
        public static IInsightsEFRepository InsightsEfRepository;
        public static CM.ContentProviderType ContentType;


        public EchoController()
        {
            InsightsEfRepository = new InsightsEfRepository();
            ContentType = CM.ContentProviderType.Contentful;
            ;
        }
        /// <summary>
        /// Testability, passed in repository so not relying on data access through sql server and set contenttype to Mock enum so not using database for content provider
        /// </summary>
        /// <param name="insightsEfRepository"></param>
        public EchoController(IInsightsEFRepository insightsEfRepository)
        {
            if (insightsEfRepository == null)
            {
                throw new ArgumentNullException("insightsEfRepository");
            }
            InsightsEfRepository = insightsEfRepository;
            ContentType = CM.ContentProviderType.Mock;
        }

        /// <summary>
        /// GET Echo request with an encoded parameter string. Returns the value passed in the "Say" input parameter. 
        /// Call in "Keep Alive" mode to ensure quick start-up times.  
        /// </summary>
        /// <param name="enc"></param>
        /// <returns>The Echo response returns the value passed in the "Say" input parameter.</returns>
        [ResponseType(typeof(EchoResponse))]
        public HttpResponseMessage Get([FromUri] string enc)
        {

            var request = new EchoRequest();

            foreach (var aa in ActionContext.ActionArguments)
            {
                switch (aa.Key.ToLower())
                {
                    case "say":
                        request.Say = aa.Value.ToString();
                        break;
                    case "name":
                        request.Name = aa.Value.ToString();
                        break;
                    case "number":
                        request.Number = Convert.ToInt32(aa.Value);
                        break;
                }
            }

            // Validate model via model annotations.  This must execute here, since our enc is special. 
            bool success;
            var resultModelState = ValidateModels.Validate(request, out success);

            return !success ? (Request.CreateErrorResponse(HttpStatusCode.BadRequest, resultModelState)) : (Get(request));
        }




        /// <summary>
        /// GET Echo request called with complex, unencoded request object. 
        /// Returns the value passed in the "Say" input parameter.
        /// Call in "Keep Alive" mode to ensure quick start-up times.
        /// </summary>
        /// <param name="req"></param>
        /// <returns>The Echo response returns the value passed in the "Say" input parameter.</returns>
        [ResponseType(typeof(EchoResponse))]
        public HttpResponseMessage Get([FromUri] EchoRequest req)
        {
            var clientUser = ((ClientUser)ActionContext.Request.Properties[CEConfiguration.CEClientUser]);

            var sw = Stopwatch.StartNew();

            if (req.KeepAlive)
            {
                InsightsEfRepository.KeepAlive(clientUser.ClientID);

                //ApplyContent(clientUser);
            }

            //temporary test of connection strings to IaaS SQL Servers; need this to test connectivity
            if (req.Name.ToLower().Contains("testdb"))
            {
                CE.Insights.Models.BillDisaggModel model = null;
                bool isMetadata = false;

                switch (req.Name.ToLower())
                {
                    case "testdbdev":
                        model = new CE.Insights.Models.BillDisaggModel();
                        isMetadata = model.ExecuteTestDB(clientUser,
                                            "Data Source=192.168.100.21;Initial Catalog=InsightsMetadata;Persist Security Info=True;User ID=cesqluser;Password=Acl@r@694;Packet Size=32767;Min Pool Size=5;",
                                            "Data Source=192.168.100.21;Initial Catalog=InsightsDW;Persist Security Info=True;User ID=cesqluser;Password=Acl@r@694;Packet Size=32767;Min Pool Size=5;",
                                            "Server=tcp:acedsql1c0.database.windows.net,1433;Database=Insights;User ID=Aclweb@acedsql1c0;Password=Acl@r@393;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;Min Pool Size=5;");
                        break;
                    case "testdbqa":

                        break;
                    case "testdbuat":

                        break;
                    case "testdbprod":
                        model = new CE.Insights.Models.BillDisaggModel();
                        isMetadata = model.ExecuteTestDB(clientUser,
                                            "Data Source=192.168.101.5;Initial Catalog=InsightsMetadata;Persist Security Info=True;User ID=cesqluser;Password=Acl@r@694;Packet Size=32767;Min Pool Size=5;",
                                            "Data Source=192.168.101.5;Initial Catalog=InsightsDW;Persist Security Info=True;User ID=cesqluser;Password=Acl@r@694;Packet Size=32767;Min Pool Size=5;",
                                            "Server=tcp:acedsql1c0.database.windows.net,1433;Database=Insights;User ID=Aclweb@acedsql1c0;Password=Acl@r@393;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;Min Pool Size=5;");

                        break;
                }

                if (isMetadata)
                {
                    req.Say = "InsightsMetadataOKAY";
                }
            }


            sw.Stop();

            var response = new EchoResponse
            {
                Echo = req.Say + ", EF elapsed ms: " + sw.ElapsedMilliseconds.ToString(),
            };

            return (Request.CreateResponse(HttpStatusCode.OK, response));
        }



        /// <summary>
        /// Returns the value passed in the "Say" input parameter.
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [ResponseType(typeof(EchoResponse))]
        public HttpResponseMessage Post([FromBody] EchoRequest req)
        {
            var response = new EchoResponse
            {
                Echo = req.Say,
            };

            // sample warn trace message for logging
            ITraceWriter writer = Configuration.Services.GetTraceWriter();
            if (writer != null)
            {
                writer.Warn(ActionContext.Request, "Application", "Sample warning with argument: {0}", Environment.MachineName);
            }

            return (Request.CreateResponse(HttpStatusCode.OK, response));
        }


        /// <summary>
        /// Retrieve content.  Will come from cache or database.
        /// </summary>
        /// <param name="clientUser"></param>
        private static void ApplyContent(ClientUser clientUser)
        {
            // Local variables
            var cf = new CM.ContentModelFactoryContentful();
            var cp = cf.CreateContentProvider(clientUser.ClientID);
            cp.KeepContentCacheAlive();
        }




    }
}