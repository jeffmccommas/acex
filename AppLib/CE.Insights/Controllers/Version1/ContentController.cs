﻿using System;
using System.Data.SqlClient;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Tracing;
using CE.ContentModel;
using CE.Infrastructure;
using CE.Insights.Common.Filters;
using CE.Models;
using CE.Models.Insights;

namespace CE.Insights.Controllers.Version1
{
    /// <summary>
    /// The purpose of the Content endpoint is to allow operations to be made related to overall content.
    /// </summary>
    [RequireHttpsAndClientCertAttribute]
    public class ContentController : ApiController
    {
        private const string StringApplication = "Application";
        private ClientUser _clientUser;
        private ITraceWriter _traceWriter;

        public ClientUser InsightsUser
        {
            get { return _clientUser ?? (_clientUser = GetClientUser()); }
        }
        public ITraceWriter TraceWriter
        {
            get { return _traceWriter ?? (_traceWriter = Configuration.Services.GetTraceWriter()); }
        }

        public ContentController() { }

        /// <summary>
        /// GET Content request called with unencoded request object. 
        /// Can be used to retrieve content from the CMS or refresh the content database for the client.
        /// </summary>
        /// <param name="enc"></param>
        /// <returns>The Content response includes content data and a message indicating whether a refresh was performed. </returns>
        [ResponseType(typeof(ContentResponse))]
        public HttpResponseMessage Get([FromUri] string enc)
        {
            var refresh = false;
            string contentType = null;
            string contentCategory = null;
            string contentKeys = null;
            var includeContent = false;

            foreach (var aa in ActionContext.ActionArguments)
            {
                switch (aa.Key.ToLower())
                {
                    case "refresh":
                        Boolean.TryParse(aa.Value.ToString(), out refresh);
                        break;
                    case "type":
                        contentType = aa.Value.ToString();
                        break;
                    case "category":
                        contentCategory = aa.Value.ToString();
                        break;
                    case "keys":
                        contentKeys = aa.Value.ToString();
                        break;
                    case "includecontent":
                        if (!(Boolean.TryParse(aa.Value.ToString(), out includeContent)))
                        {
                            includeContent = false;
                        }
                        break;
                }
            }

            var req = new ContentRequest
            {
                Refresh = refresh,
                Type = contentType,
                Category = contentCategory,
                Keys = contentKeys,
                IncludeContent = includeContent
            };

            // Validate model via model annotations.  This must execute here, since our enc is special. 
            bool success;
            var resultModelState = ValidateModels.Validate(req, out success);
            return !success ? (Request.CreateErrorResponse(HttpStatusCode.BadRequest, resultModelState)) : (Get(req));
        }

        /// <summary>
        /// GET Content request called with unencoded request object. 
        /// Can be used to retrieve content from the CMS or refresh the content database for the client.
        /// </summary>
        /// <param name="request"></param>
        /// <returns>The Content response includes content data and a message indicating whether a refresh was performed. </returns>
        [ResponseType(typeof(ContentResponse))]
        public HttpResponseMessage Get([FromUri] ContentRequest request)
        {

            // Generic response.  Change this if required.
            var msg = request.Refresh ? "refresh true" : "refresh false";

            var response = new ContentResponse
            {
                Message = msg
            };

            try
            {
                var locale = (ActionContext.Request.Properties.ContainsKey(CEConfiguration.CELocale))
                    ? ActionContext.Request.Properties[CEConfiguration.CELocale].ToString()
                    : "en-US";

                // get and apply content to response
                ApplyContent(request, response, InsightsUser, locale);
            }
            catch (HttpException hex)
            {
                TraceWriter.Error(ActionContext.Request, StringApplication, hex, hex.Message);
                if (hex.GetHttpCode() == 400)
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, hex.Message);
                else
                    return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, hex.Message);

            }
            catch (SqlException mex)
            {
                TraceWriter.Error(ActionContext.Request, StringApplication, mex, mex.Message);
                if (mex.Number != 2627)
                    return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, mex.Message);
                const string templatePrimaryKeyError = "Looks like you have defined two entries with the same 'key' in contentful. To resolve this error, ensure that the 'key' in Contentful Entry is unique. Error:{0}";
                throw new ArgumentException(string.Format(templatePrimaryKeyError, mex.Message), mex);
            }
            catch (ArgumentException aex)
            {
                TraceWriter.Error(ActionContext.Request, StringApplication, aex, aex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, aex.Message);
            }
            catch (Exception ex)
            {
                TraceWriter.Error(ActionContext.Request, StringApplication, ex, ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, CEConfiguration.Error_GetFailed);
            }

            return (Request.CreateResponse(HttpStatusCode.OK, response));
        }

        /// <summary>
        /// Local Content grabber.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="response"></param>
        /// <param name="clientUser"></param>
        /// <param name="locale"></param>
        private void ApplyContent(ContentRequest request, ContentResponse response, ClientUser clientUser, string locale)
        {
            const string errorMsgInvalidType = "Content not authorized - Invalid content type '{0}'.";

            // Local variables
            var cf = new ContentModelFactoryContentful();
            var cp = cf.CreateContentProvider(clientUser.ClientID);

            var contentLocale = cp.GetContentLocale(locale);
            var contentType = ContentType.All;
            var contentCategory = "all";
            var contentKeys = string.Empty;
            var profileDefaultCollectionName = string.Empty;

            // sprint 4 - Get Profile default collection Name
            if (!string.IsNullOrEmpty(request.ProfileCollectionKey))
            {
                profileDefaultCollectionName = request.ProfileCollectionKey;
            }

            // Handle refresh request
            if (request.Refresh)
            {
                cp.RefreshContentCacheFromContentful(profileDefaultCollectionName);
            }

            // Handle Include Content request
            if (!request.IncludeContent) return;

            response.Content = new Content();

            // Get content type
            if (request.Type != null)
            {
                contentType = cp.GetContentType(request.Type);
            }

            // Get content category type
            if (request.Category != null)
            {
                contentCategory = request.Category;
            }

            // Get Content Keys
            if (!string.IsNullOrEmpty(request.Keys))
            {
                contentKeys = request.Keys;
            }


            // Do validation
            ValidateRequestContentType(request);
            ValidateRequestContentCategory(request, cp);

            // If category is passed and type is not passed, return all the content for the supported category
            if ((request.Category != null) && (request.Type == null))
            {
                response.Content.Condition = cp.GetContentCondition(contentCategory, contentKeys);
                response.Content.Configuration = cp.GetContentConfiguration(contentCategory, contentKeys);
                // TFS 598 Aug 2015 - configuration in bulk (json format)
                response.Content.ConfigurationBulk = cp.GetContentClientConfigurationBulk(contentCategory, contentKeys);
                response.Content.Enumeration = cp.GetContentEnumeration(contentCategory, contentKeys);
                response.Content.FileContent = cp.GetContentFileContent(contentCategory, contentLocale, contentKeys);
                response.Content.TextContent = cp.GetContentTextContent(contentCategory, contentLocale, contentKeys);
                return;
            }

            // Get Content from cache or database
            switch (contentType)
            {
                case ContentType.All:
                    response.Content.Action = cp.GetContentAction(contentKeys);
                    response.Content.ActionSavings = cp.GetContentActionSavings(contentKeys);
                    response.Content.Appliance = cp.GetContentAppliance(contentKeys);
                    response.Content.BenchmarkGroup = cp.GetContentBenchmarkGroup(contentKeys);
                    response.Content.Commodity = cp.GetContentCommodity(contentKeys);
                    response.Content.Condition = cp.GetContentCondition(contentCategory, contentKeys);
                    response.Content.Configuration = cp.GetContentConfiguration(request.Category, contentKeys);
                    // TFS 598 Aug 2015 - configuration in bulk (json format)
                    response.Content.ConfigurationBulk = cp.GetContentClientConfigurationBulk(contentCategory, contentKeys);
                    response.Content.Currency = cp.GetContentCurrency(contentKeys);
                    response.Content.EndUse = cp.GetContentEnduse(contentKeys);
                    response.Content.Enumeration = cp.GetContentEnumeration(contentCategory, contentKeys);
                    response.Content.Expression = cp.GetContentExpression(contentKeys);
                    response.Content.FileContent = cp.GetContentFileContent(contentCategory, contentLocale, contentKeys);
                    response.Content.Layout = cp.GetContentLayout(contentKeys);
                    response.Content.Measurement = cp.GetContentMeasurement(contentKeys);
                    response.Content.ProfileAttribute = cp.GetContentProfileAttribute(contentKeys);
                    // sprint 4 - if profile default collection name is presented in the request call getcontentProfileDefault with it otherwise don'l
                    response.Content.ProfileDefault = profileDefaultCollectionName == string.Empty ?
                        cp.GetContentProfileDefault(contentKeys) : cp.GetContentProfileDefault(contentKeys, profileDefaultCollectionName);
                    response.Content.ProfileDefaultCollection = cp.GetContentProfileDefaultCollection(contentKeys);
                    response.Content.ProfileOption = cp.GetContentProfileOption(contentKeys);
                    response.Content.ProfileSection = cp.GetContentProfileSection(contentKeys);
                    response.Content.Season = cp.GetContentSeason(contentKeys);
                    response.Content.Tab = cp.GetContentTab(contentKeys);
                    response.Content.TextContent = cp.GetContentTextContent(contentCategory, contentLocale, contentKeys);
                    response.Content.UOM = cp.GetContentUom(contentKeys);
                    response.Content.WhatIfData = cp.GetContentWhatIfData(contentKeys);
                    response.Content.Widget = cp.GetContentWidget(contentKeys);
                    break;
                case ContentType.Action:
                    response.Content.Action = cp.GetContentAction(contentKeys);
                    break;
                case ContentType.ActionSavings:
                    response.Content.ActionSavings = cp.GetContentActionSavings(contentKeys);
                    break;
                case ContentType.Appliance:
                    response.Content.Appliance = cp.GetContentAppliance(contentKeys);
                    break;
                case ContentType.BenchmarkGroup:
                    response.Content.BenchmarkGroup = cp.GetContentBenchmarkGroup(contentKeys);
                    break;
                case ContentType.Commodity:
                    response.Content.Commodity = cp.GetContentCommodity(contentKeys);
                    break;
                case ContentType.Condition:
                    response.Content.Condition = cp.GetContentCondition(contentCategory, contentKeys);
                    break;
                case ContentType.ConfigurationBulk:
                    // TFS 598 Aug 2015 - configuration in bulk (json format)
                    response.Content.ConfigurationBulk = cp.GetContentClientConfigurationBulk(contentCategory, contentKeys);
                    break;
                case ContentType.Configuration:
                    response.Content.Configuration = cp.GetContentConfiguration(request.Category, contentKeys);
                    break;
                case ContentType.Currency:
                    response.Content.Currency = cp.GetContentCurrency(contentKeys);
                    break;
                case ContentType.Enduse:
                    response.Content.EndUse = cp.GetContentEnduse(contentKeys);
                    break;
                case ContentType.Enumeration:
                    response.Content.Enumeration = cp.GetContentEnumeration(contentCategory, contentKeys);
                    break;
                case ContentType.Expression:
                    response.Content.Expression = cp.GetContentExpression(contentKeys);
                    break;
                case ContentType.FileContent:
                    response.Content.FileContent = cp.GetContentFileContent(contentCategory, contentLocale, contentKeys);
                    break;
                case ContentType.Layout:
                    response.Content.Layout = cp.GetContentLayout(contentKeys);
                    break;
                case ContentType.Measurement:
                    response.Content.Measurement = cp.GetContentMeasurement(contentKeys);
                    break;
                case ContentType.ProfileAttribute:
                    response.Content.ProfileAttribute = cp.GetContentProfileAttribute(contentKeys);
                    break;
                case ContentType.ProfileDefault:
                    // sprint 4 - if profile default collection name is presented in the request call getcontentProfileDefault with it otherwise don'l
                    response.Content.ProfileDefault = profileDefaultCollectionName == string.Empty ?
                        cp.GetContentProfileDefault(contentKeys) : cp.GetContentProfileDefault(contentKeys, profileDefaultCollectionName);
                    break;
                case ContentType.ProfileDefaultCollection:
                    response.Content.ProfileDefaultCollection = cp.GetContentProfileDefaultCollection(contentKeys);
                    break;
                case ContentType.ProfileOption:
                    response.Content.ProfileOption = cp.GetContentProfileOption(contentKeys);
                    break;
                case ContentType.ProfileSection:
                    response.Content.ProfileSection = cp.GetContentProfileSection(contentKeys);
                    break;
                case ContentType.Season:
                    response.Content.Season = cp.GetContentSeason(contentKeys);
                    break;
                case ContentType.Tab:
                    response.Content.Tab = cp.GetContentTab(contentKeys);
                    break;
                case ContentType.TextContent:
                    response.Content.TextContent = cp.GetContentTextContent(contentCategory, contentLocale, contentKeys);
                    break;
                case ContentType.Uom:
                    response.Content.UOM = cp.GetContentUom(contentKeys);
                    break;
                case ContentType.WhatIfData:
                    response.Content.WhatIfData = cp.GetContentWhatIfData(contentKeys);
                    break;
                case ContentType.Widget:
                    response.Content.Widget = cp.GetContentWidget(contentKeys);
                    break;
                default:
                    throw new HttpException(400, string.Format(errorMsgInvalidType, request.Type));
            }
        }

        /// <summary>
        /// Validate request content type.
        /// </summary>
        /// <param name="request"></param>
        private void ValidateRequestContentType(ContentRequest request)
        {
            const string errorMsgInvalidType = "Content not authorized - Content type cannot be null when keys parameter value is not empty.";
            const string errMsgInvalidRequest = "Request can't be null.";

            if (request == null) throw new ArgumentNullException(errMsgInvalidRequest);
            if ((request.Type == null) && (request.Keys != null))
            {
                throw new HttpException(401, string.Format(errorMsgInvalidType));
            }
        }

        /// <summary>
        /// Validate request content category.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cp"></param>
        private void ValidateRequestContentCategory(ContentRequest request, IContentProvider cp)
        {
            const string errorMsgInvalidCategory = "Content not authorized - Category '{0}' doesn't apply to type '{1}'.";

            if ((request.Type == null) && (request.Category == null))
                return;

            if ((request.Type == null) && (request.Category != null))
            {
                foreach (var c in request.Category.Split(','))
                {
                    cp.GetContentCategory(c);
                }
                return;
            }
            var contentType = cp.GetContentType(request.Type);

            if ((request.Category != null) && ((contentType != ContentType.Configuration)
                & (contentType != ContentType.TextContent)
                & (contentType != ContentType.FileContent)
                & (contentType != ContentType.Condition)
                & (contentType != ContentType.Enumeration)
                & (contentType != ContentType.ConfigurationBulk)))
            {
                throw new HttpException(400, string.Format(errorMsgInvalidCategory, request.Category, request.Type));
            }
        }

        /// <summary>
        /// Returns Client User object.
        /// </summary>
        /// <returns></returns>
        private ClientUser GetClientUser()
        {
            return ((ClientUser)ActionContext.Request.Properties[CEConfiguration.CEClientUser]);
        }

    }
}