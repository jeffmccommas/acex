﻿//using System;
//using System.Net;
//using System.Net.Http;
//using System.Web.Http;
//using System.Web.Http.Description;
//using System.Web.Http.Tracing;
//using CE.Infrastructure;
//using CE.Insights.Common.Filters;
//using CE.Insights.Models;
//using CE.Models;
//using CE.Models.Insights;

//namespace CE.Insights.Controllers.Version1
//{
//    /// <summary>
//    /// This is version 1.0 of the Authorize endpoint. 
//    /// </summary>
//    [RequireHttpsAndClientCertAttribute]
//    public class AuthorizeController : ApiController
//    {
//        private static readonly IInsightsEFRepository InsightsEfRepository = new InsightsEfRepository();
//        private const string StringApplication = "Application";
//        private ITraceWriter _traceWriter;

//        private ITraceWriter TraceWriter => _traceWriter ?? (_traceWriter = Configuration.Services.GetTraceWriter());

//        /// <summary>
//        ///Return's an auth code.
//        /// </summary>
//        /// <param name="request"></param>
//        /// <returns>Returns an auth code for a client.</returns>
//        [ResponseType(typeof(AuthorizePostResponse))]
//        public HttpResponseMessage Post([FromBody] AuthorizePostRequest request)
//        {
//            AuthorizePostResponse response;

//            if (request == null)
//            {
//                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, CEConfiguration.Error_PostEmpty);
//            }

//            try
//            {
//                //response = InsightsEfRepository.ValidateClientAuthorization(request);
//            }
//            catch (Exception ex)
//            {
//                TraceWriter.Error(ActionContext.Request, StringApplication, ex, ex.Message);
//                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, CEConfiguration.Error_PostFailed);
//            }
//            return Request.CreateResponse(response.Error != null ? HttpStatusCode.BadRequest : HttpStatusCode.OK, response);
//        }
//    }
//}