﻿using System;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Tracing;
using System.Xml;
using CE.Infrastructure;
using CE.Insights.Common.Filters;
using CE.Insights.Models;
using CE.Models;
using CE.Models.Insights;

namespace CE.Insights.Controllers.Version1
{
    /// <summary>
    /// This is version 1.0 of the oAuth Token endpoint. As part of a Green Button Connect implementation, the third-party vendor uses their client credentials and authorization code to request an access_token for a retail customer.
    /// </summary>
    [RequireHttpsAndClientCertAttribute]
    public class TokenController : ApiController {
        private static IInsightsEFRepository _insightsEfRepository;
        private const string StringApplication = "Application";
        private const string BaseUrlCeInsights = "CEInsightsAPIBaseURL";
        private ITraceWriter _traceWriter;

        public TokenController() {
            _insightsEfRepository = new InsightsEfRepository();
        }
        private ITraceWriter TraceWriter => _traceWriter ?? (_traceWriter = Configuration.Services.GetTraceWriter());

        /// <summary>
        /// Creates a token for accessing resources for a retail customer.
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Returns a access token.</returns>
        //[ResponseType(typeof(TokenPostResponse))]
        [ResponseType(typeof(Status))]
        public HttpResponseMessage Post([FromBody] TokenPostRequest request)
        {
            TokenPostResponse response;

            var locale = ActionContext.Request.Properties.ContainsKey(CEConfiguration.CELocale)
                           ? ActionContext.Request.Properties[CEConfiguration.CELocale].ToString() : "en-US";

            var headers = ActionContext.Request.Headers.Authorization.Parameter;

            //response = InsightsEfRepository.ValidateClientAuthentication1(request, headers);

            //var doc = new XmlDocument();
            //var el = (XmlElement)doc.AppendChild(doc.CreateElement("Response"));
            //el.SetAttribute("xmlns", "https://localhost/ce.insights/api/v1/oauth/Token");
            //el.AppendChild(doc.CreateElement("client_access_token")).InnerText = response.access_token;
            //el.AppendChild(doc.CreateElement("expires_in")).InnerText = response.expires_in.ToString();
            //el.AppendChild(doc.CreateElement("scope")).InnerText = "3";
            //el.AppendChild(doc.CreateElement("token_type")).InnerText = "Bearer";
            //el.AppendChild(doc.CreateElement("resourceURI")).InnerText = response.resourceUri;
            //el.AppendChild(doc.CreateElement("authorizationURI")).InnerText = response.authorizationUri;

            //return new HttpResponseMessage(HttpStatusCode.OK)
            //{
            //    Content = new StringContent(doc.OuterXml, Encoding.UTF8, "application/xml")
            //};

            var baseUrl = ConfigurationManager.AppSettings.Get(BaseUrlCeInsights);
            if (request == null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, CEConfiguration.Error_PostEmpty);
            }

            try
            {
                response = _insightsEfRepository.ValidateClientAuthentication(request, headers, baseUrl);
            }
            catch (Exception ex)
            {
                TraceWriter.Error(ActionContext.Request, StringApplication, ex, ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, CEConfiguration.Error_PostFailed);
            }

            return Request.CreateResponse(response.Error != null ? HttpStatusCode.BadRequest : HttpStatusCode.OK, response);
        }
    }
}