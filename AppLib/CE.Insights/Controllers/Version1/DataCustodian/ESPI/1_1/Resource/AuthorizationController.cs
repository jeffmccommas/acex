﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Tracing;
using CE.AO.Logging;
using CE.Infrastructure;
using CE.Insights.Common.Filters;
using CE.Insights.Helpers;
using CE.Insights.Models;
using CE.Models;
using CE.Models.Insights;
using CE.Models.Insights.Types;
using Microsoft.Practices.Unity;

namespace CE.Insights.Controllers.Version1.DataCustodian.ESPI._1_1.Resource
{
    /// <summary>
    /// This is version 1.0 of the Green Button Connect standard Authorization endpoint. 
    /// As part of a Green Button Connect implementation, the third-party vendor uses this 
    /// endpoint to retrieve information about their retail customer authorizations.
    ///  </summary>
    [RequireHttpsAndClientCert]
    [RoutePrefix("api/v{version}/datacustodian/espi/1_1/resource")]
    public class AuthorizationController : ApiController {
        private static IInsightsEFRepository _insightsEfRepository;
        private const string StringApplication = "Application";
        private ITraceWriter _traceWriter;
        private readonly IUnityContainer _container;
        //private const string ErrorCodeInvalidToken = "invalid_token";
        //private const string ErrorDescInvalidToken = "Token is invalid.";
        private const string ErrorCodeInternalApplication = "InternalApplicationError";
        private ITraceWriter TraceWriter => _traceWriter ?? (_traceWriter = Configuration.Services.GetTraceWriter());

        public AuthorizationController()
        {
            _insightsEfRepository = new InsightsEfRepository();
            _container = (IUnityContainer)GlobalConfiguration.Configuration.DependencyResolver.GetService(typeof(IUnityContainer));
        }

        /// <summary>
        /// Testability, passed in repository and unity container so not relying on data access through sql server and table storage
        /// </summary>
        /// <param name="unityContainer"></param>
        /// <param name="insightsEfRepository"></param>
        public AuthorizationController(IUnityContainer unityContainer, IInsightsEFRepository insightsEfRepository)
        {
            _container = unityContainer;
            _insightsEfRepository = insightsEfRepository;
        }

        /// <summary>
        /// Returns all the third-party vendor’s retail customer authorizations. The Green Button standard query parameters 
        /// are ignored: published-max, published-min, updated-max, updated-min, max-result, start-index, and depth.
        /// </summary>
        /// <param name="request"></param>
        [Route("Authorization")]
        [HttpGet]
        [ResponseType(typeof(StatusOAuth))]
        public HttpResponseMessage GetAuthorizations([FromUri] GreenButtonConnectRequest request)
        {
            var token = ActionContext.Request.Headers.Authorization.Parameter;

            var locale = ActionContext.Request.Properties.ContainsKey(CEConfiguration.CELocale) ? ActionContext.Request.Properties[CEConfiguration.CELocale].ToString() : "en-US";

            try
            {
                // to be removed once long term solution is in place
                var greenButtonRedirect = GreenButtonHelper.IsMidTermClient(token);
                if (greenButtonRedirect)
                {
                    // redirect to mid-term api
                    var api = new GreenButtonConnectMidTermApiResponseModel();
                    return api.GetAuthorizations(token);
                }
                var greenButtonDomain = GreenButtonHelper.GetGreenButtonDomain(ActionContext.Request.RequestUri);

                var model = new GreenButtonAuthorizationModel(token, locale, greenButtonDomain, _container,
                    _insightsEfRepository);
                var result = model.GetAllAuthorizationGreenButton(token);
                StatusOAuth sa;
                if (result != null)
                {
                    if (result.Status != GreenButtonErrorType.NoError)
                    {
                        sa = new StatusOAuth
                        {
                            Error = result.Status.ToString(),
                            Error_Description = result.Status.GetDescription()
                        };
                        return Request.CreateResponse(HttpStatusCode.BadRequest, sa);
                    }
                    var response = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(result.GreenButtonXml, Encoding.UTF8, "application/atom+xml")
                    };
                    return response;
                }
                sa = new StatusOAuth
                {
                    Error_Description = CEConfiguration.Response_NoData
                };
                return Request.CreateResponse(HttpStatusCode.BadRequest, sa);


            }
            catch (Exception ex)
            {

                TraceWriter.Error(ActionContext.Request, StringApplication, ex, ex.Message);
                var sa = new StatusOAuth
                {
                    Error = ErrorCodeInternalApplication,
                    Error_Description = ex.Message
                };
                return Request.CreateResponse(HttpStatusCode.BadRequest, sa);
            }


        }

        /// <summary>
        /// Returns a specific third-party authorization. The Green Button standard query parameters 
        /// are ignored: published-max, published-min, updated-max, updated-min, max-result, start-index, and depth.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="authorizationId">Unique identifier of the third-party authorization.</param>
        [Route("Authorization/{authorizationId}")]
        [HttpGet]
        [ResponseType(typeof(StatusOAuth))]
        public HttpResponseMessage GetAuthorization([FromUri] GreenButtonConnectRequest request, int authorizationId)
        {
            var token = ActionContext.Request.Headers.Authorization.Parameter;

            var locale = ActionContext.Request.Properties.ContainsKey(CEConfiguration.CELocale) ? ActionContext.Request.Properties[CEConfiguration.CELocale].ToString() : "en-US";

            try
            {
                // to be removed once long term solution is in place
                var greenButtonRedirect = GreenButtonHelper.IsMidTermClient(token);
                if (greenButtonRedirect)
                {
                    // redirect to mid-term api
                    var api = new GreenButtonConnectMidTermApiResponseModel();
                    return api.GetAuthorization(token, authorizationId);
                }
                var greenButtonDomain = GreenButtonHelper.GetGreenButtonDomain(ActionContext.Request.RequestUri);

                var model = new GreenButtonAuthorizationModel(token, locale, greenButtonDomain, _container,
                    _insightsEfRepository);
                var result = model.GetAuthorizationGreenButton(token, Convert.ToInt64(authorizationId));
                StatusOAuth sa;
                if (result != null)
                {
                    if (result.Status != GreenButtonErrorType.NoError)
                    {
                        sa = new StatusOAuth
                        {
                            Error = result.Status.ToString(),
                            Error_Description = result.Status.GetDescription()
                        };
                        return Request.CreateResponse(HttpStatusCode.BadRequest, sa);
                    }
                    var response = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(result.GreenButtonXml, Encoding.UTF8, "application/atom+xml")
                    };
                    return response;
                }
                sa = new StatusOAuth
                {
                    Error_Description = CEConfiguration.Response_NoData
                };
                return Request.CreateResponse(HttpStatusCode.BadRequest, sa);

            }
            catch (Exception ex)
            {

                TraceWriter.Error(ActionContext.Request, StringApplication, ex, ex.Message);
                var sa = new StatusOAuth
                {
                    Error = ErrorCodeInternalApplication,
                    Error_Description = ex.Message
                };
                return Request.CreateResponse(HttpStatusCode.BadRequest, sa);
            }


        }
    }
}
