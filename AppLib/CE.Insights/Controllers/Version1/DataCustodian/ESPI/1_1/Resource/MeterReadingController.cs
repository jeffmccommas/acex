﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Tracing;
using CE.AO.Logging;
using CE.Infrastructure;
using CE.Insights.Common.Filters;
using CE.Insights.Helpers;
using CE.Insights.Models;
using CE.Models;
using CE.Models.Insights;
using CE.Models.Insights.Types;
using Microsoft.Practices.Unity;

namespace CE.Insights.Controllers.Version1.DataCustodian.ESPI._1_1.Resource
{
    /// <summary>
    /// This is version 1.0 of the Green Button Connect standard MeterReading endpoint. As part of a Green Button Connect implementation, 
    /// the third-party vendor uses this endpoint to retrieve interval usage data for a retail customer.
    ///  </summary>
    [RequireHttpsAndClientCert]
    [RoutePrefix("api/v{version}/datacustodian/espi/1_1/resource")]
    public class MeterReadingController : ApiController {
        private static IInsightsEFRepository _insightsEfRepository;
        private const string StringApplication = "Application";
        private ITraceWriter _traceWriter;
        private readonly IUnityContainer _container;
        //private const string ErrorCodeInvalidToken = "invalid_token";
        //private const string ErrorDescInvalidToken = "Token is invalid.";
        private const string ErrorCodeInternalApplication = "InternalApplicationError";
        private ITraceWriter TraceWriter => _traceWriter ?? (_traceWriter = Configuration.Services.GetTraceWriter());

        public MeterReadingController()
        {
            _insightsEfRepository = new InsightsEfRepository();
            _container = (IUnityContainer)GlobalConfiguration.Configuration.DependencyResolver.GetService(typeof(IUnityContainer));
        }

        /// <summary>
        /// Testability, passed in repository and unity container so not relying on data access through sql server and table storage
        /// </summary>
        /// <param name="unityContainer"></param>
        /// <param name="insightsEfRepository"></param>
        public MeterReadingController(IUnityContainer unityContainer, IInsightsEFRepository insightsEfRepository)
        {
            _container = unityContainer;
            _insightsEfRepository = insightsEfRepository;
        }

        /// <summary>
        /// Returns meter reading ids for the subscription id and usage point id. Use the published-min and published-max parameters to specify the time period for the readings. 
        /// The other Green Button standard query parameters are ignored: updated-max, updated-min, max-result, start-index, and depth.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="subscriptionId">The Id of the subscription.</param>
        /// <param name="usagePointId">The Id of the subscription's usage point.</param>
        [Route("subscription/{subscriptionId}/usagepoint/{usagePointId}/meterreading")]
        [HttpGet]
        [GreenButtonConnectParameterNameMap(InboundParameterName = "published-min", ActionParameterName = "PublishedMin")]
        [GreenButtonConnectParameterNameMap(InboundParameterName = "published-max", ActionParameterName = "PublishedMax")]
        [GreenButtonConnectParameterNameMap(InboundParameterName = "updated-min", ActionParameterName = "UpdatedMin")]
        [GreenButtonConnectParameterNameMap(InboundParameterName = "updated-max", ActionParameterName = "UpdatedMax")]
        [ResponseType(typeof(StatusOAuth))]
        public HttpResponseMessage GeUsagePointMeterReadings([FromUri] GreenButtonConnectRequest request, int subscriptionId, string usagePointId)
        {
            var token = ActionContext.Request.Headers.Authorization.Parameter;

            var locale = ActionContext.Request.Properties.ContainsKey(CEConfiguration.CELocale) ? ActionContext.Request.Properties[CEConfiguration.CELocale].ToString() : "en-US";
            
            try
            {

                // to be removed once long term solution is in place
                var greenButtonRedirect = GreenButtonHelper.IsMidTermClient(token);
                if (greenButtonRedirect)
                {
                    // redirect to mid-term api
                    var api = new GreenButtonConnectMidTermApiResponseModel();
                    return api.GeUsagePointMeterReadings(token, subscriptionId, usagePointId, request.PublishedMin,
                        request.PublishedMax);
                }
                var greenButtonDomain = GreenButtonHelper.GetGreenButtonDomain(ActionContext.Request.RequestUri);
                DateTime? startDate = null;
                DateTime? endDate = null;

                if (request != null)
                {
                    startDate = !string.IsNullOrEmpty(request.PublishedMin)
                        ? Convert.ToDateTime(request.PublishedMin)
                        : (!string.IsNullOrEmpty(request.UpdatedMin)
                            ? Convert.ToDateTime(request.UpdatedMin)
                            : (DateTime?)null);
                    endDate = !string.IsNullOrEmpty(request.PublishedMax)
                        ? Convert.ToDateTime(request.PublishedMax)
                        : (!string.IsNullOrEmpty(request.UpdatedMax)
                            ? Convert.ToDateTime(request.UpdatedMax)
                            : (DateTime?)null);
                }
                var model = new GreenButtonMeterReadingModel(token, locale, greenButtonDomain, _container,
                    _insightsEfRepository);
                var result = model.GetAllMeterReadingsGreenButton(token, Convert.ToInt64(subscriptionId),
                    usagePointId,
                    startDate, endDate);
                StatusOAuth sa;
                if (result != null)
                {
                    if (result.Status != GreenButtonErrorType.NoError)
                    {
                        sa = new StatusOAuth
                        {
                            Error = result.Status.ToString(),
                            Error_Description = result.Status.GetDescription()
                        };
                        return Request.CreateResponse(HttpStatusCode.BadRequest, sa);
                    }
                    var response = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(result.GreenButtonXml, Encoding.UTF8, "application/atom+xml")
                    };
                    return response;
                }
                sa = new StatusOAuth
                {
                    Error_Description = CEConfiguration.Response_NoData
                };
                return Request.CreateResponse(HttpStatusCode.BadRequest, sa);
            }
            catch (Exception ex)
            {

                TraceWriter.Error(ActionContext.Request, StringApplication, ex, ex.Message);
                var sa = new StatusOAuth
                {
                    Error = ErrorCodeInternalApplication,
                    Error_Description = ex.Message
                };
                return Request.CreateResponse(HttpStatusCode.BadRequest, sa);
            }


        }

        /// <summary>
        /// Returns a specific meter reading record for the subscription id, usage point id and meter reading id. The Green Button standard query parameters 
        /// are ignored: published-max, published-min, updated-max, updated-min, max-result, start-index, and depth.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="subscriptionId">The Id of the subscription.</param>
        /// <param name="usagePointId">The Id of the subscription's usage point.</param>
        /// <param name="meterReadingId">The Id of the usage point's meter reading.</param>
        [Route("subscription/{subscriptionId}/usagepoint/{usagePointId}/meterreading/{meterReadingId}")]
        [HttpGet]
        [GreenButtonConnectParameterNameMap(InboundParameterName = "published-min", ActionParameterName = "PublishedMin")]
        [GreenButtonConnectParameterNameMap(InboundParameterName = "published-max", ActionParameterName = "PublishedMax")]
        [GreenButtonConnectParameterNameMap(InboundParameterName = "updated-min", ActionParameterName = "UpdatedMin")]
        [GreenButtonConnectParameterNameMap(InboundParameterName = "updated-max", ActionParameterName = "UpdatedMax")]
        [ResponseType(typeof(StatusOAuth))]
        public HttpResponseMessage GeUsagePointMeterReading([FromUri] GreenButtonConnectRequest request, int subscriptionId, string usagePointId, string meterReadingId)
        {
            var token = ActionContext.Request.Headers.Authorization.Parameter;

            var locale = ActionContext.Request.Properties.ContainsKey(CEConfiguration.CELocale) ? ActionContext.Request.Properties[CEConfiguration.CELocale].ToString() : "en-US";
            
            try
            {
                // to be removed once long term solution is in place
                var greenButtonRedirect = GreenButtonHelper.IsMidTermClient(token);
                if (greenButtonRedirect)
                {
                    // redirect to mid-term api
                    var api = new GreenButtonConnectMidTermApiResponseModel();
                    return api.GeUsagePointMeterReading(token, subscriptionId, usagePointId, meterReadingId);
                }
                var greenButtonDomain = GreenButtonHelper.GetGreenButtonDomain(ActionContext.Request.RequestUri);

                var model = new GreenButtonMeterReadingModel(token, locale, greenButtonDomain, _container,
                    _insightsEfRepository);
                var result = model.GetMeterReadingGreenButton(token, Convert.ToInt64(subscriptionId), usagePointId,
                    meterReadingId);
                StatusOAuth sa;
                if (result != null)
                {
                    if (result.Status != GreenButtonErrorType.NoError)
                    {
                        sa = new StatusOAuth
                        {
                            Error = result.Status.ToString(),
                            Error_Description = result.Status.GetDescription()
                        };
                        return Request.CreateResponse(HttpStatusCode.BadRequest, sa);
                    }
                    var response = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(result.GreenButtonXml, Encoding.UTF8, "application/atom+xml")
                    };
                    return response;
                }
                sa = new StatusOAuth
                {
                    Error_Description = CEConfiguration.Response_NoData
                };
                return Request.CreateResponse(HttpStatusCode.BadRequest, sa);
            }
            catch (Exception ex)
            {

                TraceWriter.Error(ActionContext.Request, StringApplication, ex, ex.Message);
                var sa = new StatusOAuth
                {
                    Error = ErrorCodeInternalApplication,
                    Error_Description = ex.Message
                };
                return Request.CreateResponse(HttpStatusCode.BadRequest, sa);
            }


        }


    }
}