﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using CE.Infrastructure;
using CE.Insights.Common.Filters;
using CE.Insights.Models;
using CE.Models;
using CE.Models.Insights;

namespace CE.Insights.Controllers.Version1
{
    /// <summary>
    /// The CustomerInfo API returns information about a utility customer such as name, address, accounts, premises, and services. 
    /// </summary>
    [RequireHttpsAndClientCertAttribute]
    public class CustomerInfoController : ApiController {
        public static IInsightsEFRepository insightsEFRepository;

        public CustomerInfoController()
        {
            insightsEFRepository = new InsightsEfRepository();
        }

        /// <summary>
        /// Testability, passed in repository so not relying on data access through sql server
        /// </summary>
        /// <param name="InsightsEFRepository"></param>
        public CustomerInfoController(IInsightsEFRepository InsightsEFRepository)
        {
            if (InsightsEFRepository == null)
            {
                throw new ArgumentNullException("InsightsRepository is null!");
            }
            insightsEFRepository = InsightsEFRepository;

        }

        /// <summary>
        /// GET CustomerInfo request with the parameters in an encoded string. Returns information about a utility customer
        /// </summary>
        /// <param name="enc"></param>
        /// <returns>Information about a utility customer</returns>
        [ResponseType(typeof(CustomerInfoResponse))]
        public HttpResponseMessage Get([FromUri] string enc)
        {
            string customerId = null;
            bool includeContent = false;

            foreach (var aa in this.ActionContext.ActionArguments)
            {
                switch (aa.Key.ToLower())
                {
                    case "customerid":
                        customerId = aa.Value.ToString();
                        break;
                }
            }

            var request = new CustomerInfoRequest();
            request.CustomerId = customerId;
            request.IncludeContent = includeContent;

            // Validate model via model annotations.  This must execute here, since our enc is special. 
            bool success = true;
            var resultModelState = ValidateModels.Validate(request, out success);
            if (!success)
            {
                return (Request.CreateErrorResponse(HttpStatusCode.BadRequest, resultModelState));
            }

            return Get(request);
        }


        /// <summary>
        /// GET Customer Info request with un-encoded parameters. Returns information about a utility customer.
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Returns customer info</returns>
        [ResponseType(typeof(CustomerInfoResponse))]
        public HttpResponseMessage Get([FromUri] CustomerInfoRequest request)
        {
            CustomerInfoResponse response = null;

            var clientUser = ((ClientUser)this.ActionContext.Request.Properties[CEConfiguration.CEClientUser]);
            var locale = (ActionContext.Request.Properties.ContainsKey(CEConfiguration.CELocale))
                        ? ActionContext.Request.Properties[CEConfiguration.CELocale].ToString() : "en-US";

            response = insightsEFRepository.GetCustomerInfo(clientUser.ClientID, request);

            if (response != null)
            {
                if (request.IncludeContent)
                {
                    var CustomerInfoModel = new CustomerInfoModel();
                    CustomerInfoModel.ApplyContent(response, clientUser, locale);
                }
            }
            else
            {
                response = new CustomerInfoResponse { Message = CEConfiguration.Response_NoData };
            }

            return (Request.CreateResponse(HttpStatusCode.OK, response));
        }
    }
}