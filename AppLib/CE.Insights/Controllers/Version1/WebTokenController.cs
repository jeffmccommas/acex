﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Tracing;
using CE.Infrastructure;
using CE.Insights.Common.Filters;
using CE.Insights.Models;
using CE.Models;
using CE.Models.Insights;
using System.Data.SqlClient;

namespace CE.Insights.Controllers.Version1
{
    /// <summary>
    /// This is version 1.0 of the WebToken endpoint. The WebToken API enables a utility to create and validate WebToken values for a customer.
    /// </summary>
    [RequireHttpsAndClientCertAttribute]
    public class WebTokenController : ApiController
    {
        private const string StringApplication = "Application";
        private ClientUser _clientUser;
        private ITraceWriter _traceWriter;

        public ClientUser InsightsUser => _clientUser ?? (_clientUser = GetClientUser());

        public ITraceWriter TraceWriter => _traceWriter ?? (_traceWriter = Configuration.Services.GetTraceWriter());

        public WebTokenController() { }
        /// <summary>
        /// GET WebToken request with the parameters in an encoded string. 
        /// Validates the input web token and customer id and returns the token's status, created date time and any additional information stored with the token.
        /// </summary>
        /// <param name="enc"></param>
        /// <returns>Returns token's status, created date time and any additional information stored with the token. </returns>
        [ResponseType(typeof(WebTokenResponse))]
        public HttpResponseMessage Get([FromUri] string enc)
        {
            const string stringCustomerId = "customerid";
            const string stringIncludeadditionalinfo = "includeadditionalinfo";
            const string stringWebToken = "webtoken";

            var request = new WebTokenRequest();

            foreach (var aa in ActionContext.ActionArguments)
            {
                switch (aa.Key.ToLower())
                {
                    case stringCustomerId:
                        request.CustomerId = aa.Value.ToString();
                        break;
                    case stringIncludeadditionalinfo:
                        bool.TryParse(aa.Value.ToString(), out var includeAdditionalInfo);
                        request.IncludeAdditionalInfo = includeAdditionalInfo;
                        break;
                    case stringWebToken:
                        request.WebToken = aa.Value.ToString();
                        break;
                }
            }

            // Validate model via model annotations.  This must execute here, since our enc is special. 
            var resultModelState = ValidateModels.Validate(request, out var success);
            return !success ? (Request.CreateErrorResponse(HttpStatusCode.BadRequest, resultModelState)) : Get(request);
        }

        /// <summary>
        /// GET WebToken request with un-encoded parameters. 
        /// Validates the input web token and customer id and returns the token's status, created date time and any additional information stored with the token.
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Returns the token's status, created date time and any additional information stored with the token. </returns>
        [ResponseType(typeof(WebTokenResponse))]
        public HttpResponseMessage Get([FromUri] WebTokenRequest request)
        {
            var response = new WebTokenResponse();
            var model = new WebTokenModel();
            try
            {
                var result = model.ValidateWebToken(InsightsUser, request, response);

                if (!result)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, string.Format(CEConfiguration.ErrorAccessDenied, request.CustomerId, request.WebToken));
                }

                if (!response.IsActive)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, string.Format(CEConfiguration.ErrorTokenExpired, request.WebToken, request.CustomerId));
                }
            }
            catch (Exception ex)
            {
                TraceWriter.Error(ActionContext.Request, StringApplication, ex, ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, CEConfiguration.Error_GetFailed);
            }

            return (Request.CreateResponse(HttpStatusCode.OK, response));
        }

        /// <summary>
        /// Creates a new web token for accessing a customer’s data.
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Returns a web token for accessing the customer's data.</returns>
        [ResponseType(typeof(WebTokenPostResponse))]
        public HttpResponseMessage Post([FromBody] WebTokenPostRequest request)
        {
            var response = new WebTokenPostResponse();
            var model = new WebTokenModel();
            if (request == null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, CEConfiguration.Error_PostEmpty);
            }
            if (string.IsNullOrEmpty(request.CustomerId) && string.IsNullOrEmpty(request.EmailAddress))
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, CEConfiguration.ErrorEitherEmailCustomerId);
            }
            if (!string.IsNullOrEmpty(request.CustomerId) && !string.IsNullOrEmpty(request.EmailAddress))
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, CEConfiguration.ErrorBothEmailCustomerId);
            }
            if (!string.IsNullOrEmpty(request.EmailAddress))
            {
                //Validate email format
                if (!model.IsEmail(request.EmailAddress))
                {
                    var webTokenModelErrorResponse = new Status { StatusType = CE.Models.Insights.Types.StatusType.ParameterErrors, Message = CEConfiguration.ErrorEmailNotValid };
                    return Request.CreateResponse(HttpStatusCode.OK, webTokenModelErrorResponse);
                }

                //Validate zip code
                if (!string.IsNullOrEmpty(request.ZipCode))
                {
                    if (!model.IsUsorCanadianZipCode(request.ZipCode))
                    {
                        var webTokenModelErrorResponse = new Status { StatusType = CE.Models.Insights.Types.StatusType.ParameterErrors, Message = CEConfiguration.ErrorZipCodeNotValid };
                        return Request.CreateResponse(HttpStatusCode.OK, webTokenModelErrorResponse);
                    }
                }
            }
            try
            {

                model.CreateWebToken(InsightsUser, request, response);
            }
            catch (SqlException se)
            {
                TraceWriter.Error(ActionContext.Request, StringApplication, se, se.Message);
                if (se.Errors[0].Class == 11)
                {
                    var webTokenModelErrorResponse = new Status { StatusType = CE.Models.Insights.Types.StatusType.ModelErrors, Message = se.Message };
                    return Request.CreateResponse(HttpStatusCode.OK, webTokenModelErrorResponse);
                }
                else
                    return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, CEConfiguration.Error_PostFailed);

            }
            catch (Exception ex)
            {
                TraceWriter.Error(ActionContext.Request, StringApplication, ex, ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, CEConfiguration.Error_PostFailed);
            }

            return (Request.CreateResponse(HttpStatusCode.OK, response));
        }

        /// <summary>
        /// Deletes web token for accessing a customer’s data.
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Returns Success=true after deleting the web token.</returns>
        [ResponseType(typeof(WebTokenDeleteResponse))]
        public HttpResponseMessage Delete([FromUri] WebTokenDeleteRequest request)
        {
            var response = new WebTokenDeleteResponse();

            if (request == null)
            {
                // No data was posted when data is expected.
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, CEConfiguration.Error_PostEmpty);
            }

            try
            {
                var model = new WebTokenModel();
                var result = model.DeleteWebToken(InsightsUser, request, response);
                if (!result)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, string.Format(CEConfiguration.ErrorNoDataAvailableToDelete, request.CustomerId, request.WebToken));
                }
            }
            catch (Exception ex)
            {
                TraceWriter.Error(ActionContext.Request, StringApplication, ex, ex.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, CEConfiguration.Error_PostFailed);
            }

            return (Request.CreateResponse(HttpStatusCode.OK, response));
        }

        /// <summary>
        /// Returns Client User object.
        /// </summary>
        /// <returns></returns>
        private ClientUser GetClientUser() => (ClientUser)ActionContext.Request.Properties[CEConfiguration.CEClientUser];
    }
}