﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using CE.Infrastructure;
using CE.Insights.Common.Filters;
using CE.Insights.Models;
using CE.Models;
using CE.Models.Insights;


namespace CE.Insights.Controllers.Version1
{

    /// <summary>
    /// BillDisagg breaks down a utility customer's bills into usage and costs for specific appliances and end uses. 
    /// </summary>
    [RequireHttpsAndClientCertAttribute]
    public class BillDisaggController : ApiController
    {
        private ClientUser _clientUser;

        public ClientUser MyClientUser
        {
            get { return _clientUser ?? (_clientUser = GetClientUser()); }
            set { _clientUser = value; }
        }
        public BillDisaggController() { }

        /// <summary>
        /// GET BillDisagg request with an encoded parameter string. Returns cost, usage and other measurements
        /// for a utility customer and bill period, disaggregated into appliances and end uses. 
        /// </summary>
        /// <param name="enc">Encoded string containing GET BillDisagg input parameters</param>
        /// <returns>The GET BillDisagg response shows how a bill's usage quantities and costs are distributed across appliaces and end uses</returns>
        [ResponseType(typeof(BillDisaggResponse))]
        public HttpResponseMessage Get([FromUri] string enc)
        {
            string customerId = null;
            string accountId = null;
            string premiseId = null;
            DateTime? startDate = null;
            DateTime? endDate = null;
            string measurements = null;
            var includeContent = false;

            foreach (var aa in ActionContext.ActionArguments)
            {
                switch (aa.Key.ToLower())
                {
                    case "customerid":
                        customerId = aa.Value.ToString();
                        break;
                    case "accountid":
                        accountId = aa.Value.ToString();
                        break;
                    case "premiseid":
                        premiseId = aa.Value.ToString();
                        break;
                    case "startdate":
                        startDate = DateTime.Parse(aa.Value.ToString());
                        break;
                    case "enddate":
                        endDate = DateTime.Parse(aa.Value.ToString());
                        break;
                    case "measurementkeys":
                        measurements = aa.Value.ToString();
                        break;
                    case "includecontent":
                        bool.TryParse(aa.Value.ToString(), out includeContent);
                        break;
                }
            }

            var request = new BillDisaggRequest
            {
                CustomerId = customerId,
                AccountId = accountId,
                PremiseId = premiseId
            };

            if (!(startDate == null)) { request.StartDate = startDate.Value; }
            if (!(endDate == null)) { request.EndDate = endDate.Value; }
            request.MeasurementKeys = measurements;
            request.IncludeContent = includeContent;

            // Validate model via model annotations.  This must execute here, since our enc is special. 
            bool success;
            var resultModelState = ValidateModels.Validate(request, out success);
            return !success ? Request.CreateErrorResponse(HttpStatusCode.BadRequest, resultModelState) : Get(request);
        }


        /// <summary>
        /// GET BillDisagg request where the parameters are not encoded. Returns cost, usage and other measurements
        /// for a utility customer disaggregated into appliances and end uses. 
        /// </summary>
        /// <param name="request"></param>
        /// <returns>The GET BillDisagg response shows how a bill's usage quantities and costs are distributed across appliances and end uses for an annual period</returns>
        [ResponseType(typeof(BillDisaggResponse))]
        public HttpResponseMessage Get([FromUri] BillDisaggRequest request)
        {
            var response = new BillDisaggResponse();

            try
            {
                var locale = ActionContext.Request.Properties.ContainsKey(CEConfiguration.CELocale)
                             ? ActionContext.Request.Properties[CEConfiguration.CELocale].ToString() : "en-US";

                var model = new BillDisaggModel();

                model.ExecuteBillDisaggModel(MyClientUser, request, response);

                if (request.IncludeContent)
                {
                    model.ApplyContent(response, MyClientUser, locale);
                }
            }
            catch (Exception)
            {
                return (Request.CreateErrorResponse(HttpStatusCode.InternalServerError, CEConfiguration.Error_BillDisaggFailed));
            }

            return Request.CreateResponse(HttpStatusCode.OK, response);

        }


        /// <summary>
        /// Returns Client User object.
        /// </summary>
        /// <returns></returns>
        private ClientUser GetClientUser()
        {
            return (ClientUser)ActionContext.Request.Properties[CEConfiguration.CEClientUser];
        }


    }


}
