﻿using System;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using CE.Infrastructure;
using CE.Insights.Common.Filters;
using CE.Insights.Models;
using CE.Models;
using CE.Models.Insights;
using CE.Models.Insights.Types;
using Microsoft.Practices.Unity;

namespace CE.Insights.Controllers.Version1
{

    /// <summary>
    /// The Action endpoint returns recommended actions, energy-savings measures and promotions based on profile values.
    /// </summary>
    [RequireHttpsAndClientCertAttribute]
    public class ActionController : ApiController
    {
        private ClientUser _clientUser;
        private IUnityContainer _container;

        public ClientUser MyClientUser
        {
            get { return _clientUser ?? (_clientUser = GetClientUser()); }
            set { _clientUser = value; }
        }

        public ActionController()
        {
            _container = (IUnityContainer)GlobalConfiguration.Configuration.DependencyResolver.GetService(typeof(IUnityContainer));
        }

        public ActionController(IUnityContainer unityContainer)
        {
            _container = unityContainer;
        }

        /// <summary>
        /// GET Action request with the parameters in an encoded string. Returns recommended actions.
        /// </summary>
        /// <param name="enc"></param>
        /// <returns>Recommended actions, energy-savings measures and promotions.</returns>
        [ResponseType(typeof(ActionResponse))]
        public HttpResponseMessage Get([FromUri] string enc)
        {
            string customerId = null;
            string accountId = null;
            string premiseId = null;
            string commodities = null;
            string types = null;
            string enduses = null;
            string appliances = null;
            bool includeContent = false;
            DateTime? latestBillDate = null;
            DateTime? previousBillDate = null;

            foreach (var aa in ActionContext.ActionArguments)
            {
                switch (aa.Key.ToLower())
                {
                    case "customerid":
                        customerId = aa.Value.ToString();
                        break;
                    case "accountid":
                        accountId = aa.Value.ToString();
                        break;
                    case "premiseid":
                        premiseId = aa.Value.ToString();
                        break;
                    case "commoditykeys":
                        commodities = aa.Value.ToString();
                        break;
                    case "types":
                        types = aa.Value.ToString();
                        break;
                    case "endusekeys":
                        enduses = aa.Value.ToString();
                        break;
                    case "appliancekeys":
                        appliances = aa.Value.ToString();
                        break;
                    case "includecontent":
                        bool.TryParse(aa.Value.ToString(), out includeContent);
                        break;
                    case "latestbilldate":
                        latestBillDate = DateTime.Parse(aa.Value.ToString());
                        break;
                    case "previousbilldate":
                        previousBillDate = DateTime.Parse(aa.Value.ToString());
                        break;
                }
            }

            var request = new ActionRequest
            {
                CustomerId = customerId,
                AccountId = accountId,
                PremiseId = premiseId,
                CommodityKeys = commodities,
                Types = types,
                EndUseKeys = enduses,
                ApplianceKeys = appliances,
                IncludeContent = includeContent
            };

            if (!(latestBillDate == null))
            {
                request.LatestBillDate = latestBillDate.Value;
            }

            if (!(previousBillDate == null))
            {
                request.PreviousBillDate = previousBillDate.Value;
            }
            // Validate model via model annotations.  This must execute here, since our enc is special. 
            bool success;
            var resultModelState = ValidateModels.Validate(request, out success);
            return !success ? Request.CreateErrorResponse(HttpStatusCode.BadRequest, resultModelState) : Get(request);
        }


        /// <summary>
        /// GET Action request with un-encoded parameters. Returns recommended actions.
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Recommended actions, energy-savings measures and promotions</returns>
        [ResponseType(typeof(ActionResponse))]
        public HttpResponseMessage Get([FromUri] ActionRequest request)
        {

            ActionResponse response = null;

            try
            {
                var locale = ActionContext.Request.Properties.ContainsKey(CEConfiguration.CELocale)
                    ? ActionContext.Request.Properties[CEConfiguration.CELocale].ToString()
                    : "en-US";

                var model = new ActionModel(_container);
                response = model.ValidateRequest(request);
                if (response.StatusType == StatusType.ParameterErrors)
                {
                    throw new HttpException(400, response.Message);

                }

                response = model.ExecuteActionModel(MyClientUser, request);

                if (string.IsNullOrEmpty(response.Message))
                {
                    if (request.IncludeContent)
                    {
                        model.ApplyContent(response, MyClientUser, locale);
                    }
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, response.Message);
                }
            }
            catch (HttpException hex)
            {
                if (hex.GetHttpCode() == 400)
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, hex.Message);
                else
                    return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, hex.Message);

            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }

            return Request.CreateResponse(HttpStatusCode.OK, response);
        }


        /// <summary>
        /// Returns Client User object.
        /// </summary>
        /// <returns></returns>
        private ClientUser GetClientUser()
        {
            return (ClientUser)ActionContext.Request.Properties[CEConfiguration.CEClientUser];
        }
    }

}