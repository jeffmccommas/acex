﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using CE.Infrastructure;
using CE.Insights.Common.Filters;
using CE.Insights.Models;
using CE.Models;
using CE.Models.Insights;

namespace CE.Insights.Controllers.Version1
{
    /// <summary>
    /// The Consumption API returns total usage for a customer, account, premise and service point. 
    /// The user specifies the time period and time interval resolution for the data. 
    /// </summary>
    [RequireHttpsAndClientCertAttribute]
    public class ConsumptionController : ApiController {
        public static IInsightsEFRepository insightsEFRepository;

        public ConsumptionController()
        {
            insightsEFRepository = new InsightsEfRepository();
        }

        /// <summary>
        /// Testability, passed in repository so not relying on data access through sql server
        /// </summary>
        /// <param name="InsightsEFRepository"></param>
        public ConsumptionController(IInsightsEFRepository InsightsEFRepository)
        {
            if (InsightsEFRepository == null)
            {
                throw new ArgumentNullException("InsightsRepository is null!");
            }
            insightsEFRepository = InsightsEFRepository;

        }

        /// <summary>
        /// GET Consumption request with the parameters in an encoded string. 
        /// Returns total usage for a customer, account, premise and service point. 
        /// </summary>
        /// <param name="enc"></param>
        /// <returns>Total consumption for a customer, account, premise and service point.</returns>
        [ResponseType(typeof(ConsumptionResponse))]
        public HttpResponseMessage Get([FromUri] string enc)
        {
            string customerId = null;
            string accountId = null;
            string premiseId = null;
            DateTime? startDate = null;
            DateTime? endDate = null;
            string servicePointId = null;
            string resolution = null;
            bool includeContent = false;
            int pageIndex = 1;

            foreach (var aa in this.ActionContext.ActionArguments)
            {
                switch (aa.Key.ToLower())
                {
                    case "customerid":
                        customerId = aa.Value.ToString();
                        break;
                    case "accountid":
                        accountId = aa.Value.ToString();
                        break;
                    case "premiseid":
                        premiseId = aa.Value.ToString();
                        break;
                    case "startdate":
                        startDate = DateTime.Parse(aa.Value.ToString());
                        break;
                    case "enddate":
                        endDate = DateTime.Parse(aa.Value.ToString());
                        break;
                    case "servicepointid":
                        servicePointId = aa.Value.ToString();
                        break;
                    case "resolutionkey":
                        resolution = aa.Value.ToString();
                        break;
                    case "includecontent":
                        Boolean.TryParse(aa.Value.ToString(), out includeContent);
                        break;
                    case "pageindex":
                        if (!(int.TryParse(aa.Value.ToString(), out pageIndex)))
                        {
                            pageIndex = 1;
                        }
                        break;
                }
            }

            var request = new ConsumptionRequest();
            request.CustomerId = customerId;
            request.AccountId = accountId;
            request.PremiseId = premiseId;
            if (!(startDate == null)) { request.StartDate = startDate.Value; }
            if (!(endDate == null)) { request.EndDate = endDate.Value; }
            request.ServicePointId = servicePointId;
            request.ResolutionKey = resolution;
            request.IncludeContent = includeContent;
            request.PageIndex = pageIndex;

            // Validate model via model annotations.  This must execute here, since our enc is special. 
            bool success = true;
            var resultModelState = ValidateModels.Validate(request, out success);
            if (!success)
            {
                return (Request.CreateErrorResponse(HttpStatusCode.BadRequest, resultModelState));
            }

            return Get(request);
        }


        /// <summary>
        /// GET Consumption request with un-encoded parameters. 
        /// Returns total usage for a customer, account, premise and service point.
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Total consumption for a customer, account, premise and service point</returns>
        [ResponseType(typeof(ConsumptionResponse))]
        public HttpResponseMessage Get([FromUri] ConsumptionRequest request)
        {
            ConsumptionResponse response = null;

            var clientUser = ((ClientUser)this.ActionContext.Request.Properties[CEConfiguration.CEClientUser]);
            var locale = (ActionContext.Request.Properties.ContainsKey(CEConfiguration.CELocale))
                        ? ActionContext.Request.Properties[CEConfiguration.CELocale].ToString() : "en-US";

            response = insightsEFRepository.GetConsumption(clientUser.ClientID, request);

            if (response != null)
            {
                if (request.IncludeContent)
                {
                    var consumptionModel = new ConsumptionModel();
                    consumptionModel.ApplyContent(response, clientUser, locale);
                }
            }
            else
            {
                response = new ConsumptionResponse { Message = CEConfiguration.Response_NoData };
            }

            return (Request.CreateResponse(HttpStatusCode.OK, response));
        }
    }
}