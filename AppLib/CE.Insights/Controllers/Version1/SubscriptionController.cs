﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using CE.Infrastructure;
using CE.Insights.Common.Filters;
using CE.Insights.Models;
using CE.Models;
using CE.Models.Insights;
using CE.Models.Insights.Types;
using Microsoft.Practices.Unity;

namespace CE.Insights.Controllers.Version1
{
    /// <summary>
    /// The Subscription API enables a utility to store and retrieve a customer's subscriptions to alerts and insights.
    /// </summary>
    [RequireHttpsAndClientCertAttribute]
    public class SubscriptionController : ApiController
    {
        private ClientUser _clientUser;
        private IUnityContainer container;

        public ClientUser MyClientUser
        {
            get { return _clientUser ?? (_clientUser = GetClientUser()); }
            set { _clientUser = value; }
        }

        public SubscriptionController()
        {
            container = (IUnityContainer)GlobalConfiguration.Configuration.DependencyResolver.GetService(typeof(IUnityContainer));
        }

        public SubscriptionController(IUnityContainer unityContainer)
        {
            container = unityContainer;
        }

        /// <summary>
        /// GET Subscription request with the parameters in an encoded string. 
        /// </summary>
        /// <param name="enc"></param>
        /// <returns>The GET Subscription response information for a utility customer.</returns>
        [ResponseType(typeof(SubscriptionResponse))]
        public HttpResponseMessage Get([FromUri] string enc)
        {
            string customerId = null;
            string accountId = null;
            bool includeContent = false;

            foreach (var aa in ActionContext.ActionArguments)
            {
                switch (aa.Key.ToLower())
                {
                    case "customerid":
                        customerId = aa.Value.ToString();
                        break;
                    case "accountid":
                        accountId = aa.Value.ToString();
                        break;
                    case "includecontent":
                        Boolean.TryParse(aa.Value.ToString(), out includeContent);
                        break;
                }
            }

            var request = new SubscriptionRequest();
            request.CustomerId = customerId;
            request.AccountId = accountId;
            request.IncludeContent = includeContent;

            // Validate model via model annotations.  This must execute here, since our enc is special. 
            bool success;
            var resultModelState = ValidateModels.Validate(request, out success);
            if (!success)
            {
                return (Request.CreateErrorResponse(HttpStatusCode.BadRequest, resultModelState));
            }

            return Get(request);
        }


        /// <summary>
        /// GET Subscription request with un-encoded parameters
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [ResponseType(typeof(SubscriptionResponse))]
        public HttpResponseMessage Get([FromUri] SubscriptionRequest request)
        {
            SubscriptionResponse response;

            try
            {
                var locale = (ActionContext.Request.Properties.ContainsKey(CEConfiguration.CELocale))
                             ? ActionContext.Request.Properties[CEConfiguration.CELocale].ToString() : "en-US";

                var model = new SubscriptionModel(container);
                response = model.GetSubscriptions(MyClientUser, request);

                if (response != null)
                {
                    if (request.IncludeContent)
                    {
                        model.ApplyContent(response, MyClientUser, locale);
                    }
                    else if(!string.IsNullOrEmpty(response.Message) && response.StatusType != StatusType.Undefined)
                    {
                        return (Request.CreateErrorResponse(HttpStatusCode.InternalServerError, response.Message));
                    }
                }

            }
            catch (Exception ex)
            {
                return (Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message));
            }

            return (Request.CreateResponse(HttpStatusCode.OK, response));
        }

        /// <summary>
        /// Stores new subscriptions to alerts and insights for the customer.
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        [ResponseType(typeof(SubscriptionPostResponse))]
        public HttpResponseMessage Post([FromBody] SubscriptionPostRequest post)
        {
            var response = new SubscriptionPostResponse();
            try
            {
                var clientUser = MyClientUser;
                var model = new SubscriptionModel(container);
                response = model.SubscribeInsights(clientUser, post);
            }
            catch (Exception ex)
            {
                response.StatusType = StatusType.ModelErrors;
                response.Message = ex.Message;
                return (Request.CreateResponse(HttpStatusCode.OK, response));
            }

            return (Request.CreateResponse(HttpStatusCode.OK, response));
        }

        /// <summary>
        /// Returns Client User object.
        /// </summary>
        /// <returns></returns>
        private ClientUser GetClientUser()
        {
            return ((ClientUser)ActionContext.Request.Properties[CEConfiguration.CEClientUser]);
        }
    }
}