﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using CE.Infrastructure;
using CE.Insights.Common.Filters;
using CE.Insights.Models;
using CE.Models;
using CE.Models.Insights;

namespace CE.Insights.Controllers.Version1
{
    /// <summary>
    /// The EnergyModel API enables a utility to execute energy modeling for appliances. 
    /// </summary>
    [RequireHttpsAndClientCertAttribute]
    public class EnergyModelController : ApiController
    {
        private ClientUser _clientUser;

        public ClientUser MyClientUser
        {
            get { return _clientUser ?? (_clientUser = GetClientUser()); }
            set { _clientUser = value; }
        }

        public EnergyModelController() { }

        /// <summary>
        /// Executes an energy model given the input payload.
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Returns an energy model.</returns>
        [ResponseType(typeof(EnergyModelPostResponse))]
        public HttpResponseMessage Post([FromBody] EnergyModelPostRequest request)
        {
            var response = new EnergyModelPostResponse();

            if (request == null)
            {
                return (Request.CreateErrorResponse(HttpStatusCode.BadRequest, CEConfiguration.Error_PostEmpty));
            }

            try
            {
                var model = new EnergyModelModel();

                model.ExecuteEnergyModel(MyClientUser, request, response);

                if (response.Data == null)
                {
                    return (Request.CreateErrorResponse(HttpStatusCode.InternalServerError, response.Message));
                }

            }
            catch (Exception)
            {
                return (Request.CreateErrorResponse(HttpStatusCode.InternalServerError, CEConfiguration.Error_EnergyModelFailed));
            }

            return (Request.CreateResponse(HttpStatusCode.OK, response));
        }


        /// <summary>
        /// Returns Client User object.
        /// </summary>
        /// <returns></returns>
        private ClientUser GetClientUser()
        {
            return ((ClientUser)ActionContext.Request.Properties[CEConfiguration.CEClientUser]);
        }
    }

}