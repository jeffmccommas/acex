﻿using System.Web.Mvc;

namespace CE.Insights.Controllers
{
    public class HomeController : Controller
    {
        public HomeController() { }
        /// <summary>
        /// Information page.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }
    }
}
