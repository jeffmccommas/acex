﻿using CE.ContentModel;
using CE.GreenButton;
using System;
//using System.Collections.Generic;
using System.IO;
using System.Linq;
//using System.Linq;
//using System.Web;
using System.Xml;

namespace CE.Insights.Helpers
{
    public class GreenButtonHelper
    {
        /// <summary>
        /// get green button configuration settings
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="locale"></param>
        /// <returns></returns>
        public static GreenButtonConfig GetGreenButtonSettings(int clientId, string locale)
        {
            var greenButtonSettings = new GreenButtonConfig();
            var cf = new ContentModelFactoryContentful();
            var cp = cf.CreateContentProvider(clientId);

            var settingsList = cp.GetContentConfiguration("greenbuttonengine", string.Empty);

            if (settingsList != null)
            {
                foreach (var item in settingsList)
                {
                    bool bvariable;
                    double dvariable;
                    switch (item.Key.ToLower())
                    {
                        case "greenbutton.billmonths":
                            int variable;
                            greenButtonSettings.BillMonths = int.TryParse(item.Value, out variable) ? variable : 0;
                            break;
                        case "greenbutton.convertgasuom":
                            greenButtonSettings.ConvertGasUom = bool.TryParse(item.Value, out bvariable) && bvariable;
                            break;
                        case "greenbutton.convertwateruom":
                            greenButtonSettings.ConvertWaterUom = bool.TryParse(item.Value, out bvariable) && bvariable;
                            break;
                        case "greenbutton.country":
                            greenButtonSettings.Country = Convert.ToString(item.Value);
                            break;
                        case "greenbutton.defaultgasconvfactor":
                            greenButtonSettings.DefaultGasConversionFactor = double.TryParse(item.Value, out dvariable) ? dvariable : 0;
                            break;
                        case "greenbutton.defaultwaterconvfactor":
                            greenButtonSettings.DefaultWaterConversionFactor = double.TryParse(item.Value, out dvariable) ? dvariable : 0;
                            break;
                        case "greenbutton.electricuom":
                            greenButtonSettings.ElectricDisplayUom = Convert.ToString(item.Value);
                            break;
                        case "greenbutton.enablelinks":
                            greenButtonSettings.EnableLinks = bool.TryParse(item.Value, out bvariable) && bvariable;
                            break;
                        case "greenbutton.gasuom":
                            greenButtonSettings.GasDisplayUom = Convert.ToString(item.Value);
                            break;
                        case "greenbutton.maxloadingtime":
                            greenButtonSettings.MaxLoadingTime = Convert.ToInt32(item.Value);
                            break;
                        case "greenbutton.timezoneoffset":
                            greenButtonSettings.TzOffsetValue = Convert.ToString(item.Value);
                            break;
                        case "greenbutton.wateruom":
                            greenButtonSettings.WaterDisplayUom = Convert.ToString(item.Value);
                            break;

                    }

                }
            }

            var contentLocale = cp.GetContentLocale(locale);
            var greentButtonContents = cp.GetContentTextContent("greenbutton", contentLocale, string.Empty);
            if (greentButtonContents != null)
            {
                foreach (var item in greentButtonContents)
                {
                    switch (item.Key.ToLower())
                    {
                        case "greenbutton.reportprename":
                            greenButtonSettings.ReportPreName = Convert.ToString(item.ShortText);
                            break;
                        case "greenbutton.reporttitle":
                            greenButtonSettings.ReportTitle = Convert.ToString(item.ShortText);
                            break;
                        case "greenbutton.usagepointtitle":
                            greenButtonSettings.UsagePointTitle = Convert.ToString(item.ShortText);
                            break;
                        case "greenbutton.intervalblocktitle":
                            greenButtonSettings.IntervalBlockTitle = Convert.ToString(item.ShortText);
                            break;
                        case "greenbutton.usagessummarytitle":
                            greenButtonSettings.UsageSummaryTitle = Convert.ToString(item.ShortText);
                            break;
                    }

                }
            }

            // get xmltemplate
            greenButtonSettings.XmlTemplates = GetXmlTemplate();
            // get xslt 
            string filename;
            greenButtonSettings.ProcessInfoFile = GetXsltStyleSheet(clientId, out filename);
            greenButtonSettings.ProcessorInfoFileName = filename;

            return greenButtonSettings;
        }

        public static CE.Models.Insights.GreenButtonConfig GetGreenButtonConnectSettings(int clientId, string locale)
        {
            CE.Models.Insights.GreenButtonConfig greenButtonSettings = new CE.Models.Insights.GreenButtonConfig();
            var cf = new ContentModelFactoryContentful();
            var cp = cf.CreateContentProvider(clientId);

            var settingsList = cp.GetContentConfiguration("greenbuttonengine", string.Empty);

            if (settingsList != null)
            {
                foreach (var item in settingsList)
                {
                    bool bvariable;
                    double dvariable;
                    switch (item.Key.ToLower())
                    {
                        case "greenbutton.billmonths":
                            int variable;
                            greenButtonSettings.BillMonths = int.TryParse(item.Value, out variable) ? variable : 0;
                            break;
                        case "greenbutton.convertgasuom":
                            greenButtonSettings.ConvertGasUom = bool.TryParse(item.Value, out bvariable) && bvariable;
                            break;
                        case "greenbutton.convertwateruom":
                            greenButtonSettings.ConvertWaterUom = bool.TryParse(item.Value, out bvariable) && bvariable;
                            break;
                        case "greenbutton.country":
                            greenButtonSettings.Country = Convert.ToString(item.Value);
                            break;
                        case "greenbutton.defaultgasconvfactor":
                            greenButtonSettings.DefaultGasConversionFactor = double.TryParse(item.Value, out dvariable) ? dvariable : 0;
                            break;
                        case "greenbutton.defaultwaterconvfactor":
                            greenButtonSettings.DefaultWaterConversionFactor = double.TryParse(item.Value, out dvariable) ? dvariable : 0;
                            break;
                        case "greenbutton.electricuom":
                            greenButtonSettings.ElectricDisplayUom = Convert.ToString(item.Value);
                            break;
                        case "greenbutton.gasuom":
                            greenButtonSettings.GasDisplayUom = Convert.ToString(item.Value);
                            break;
                        case "greenbutton.maxloadingtime":
                            greenButtonSettings.MaxLoadingTime = Convert.ToInt32(item.Value);
                            break;
                        case "greenbutton.timezoneoffset":
                            greenButtonSettings.TzOffsetValue = Convert.ToString(item.Value);
                            break;
                        case "greenbutton.wateruom":
                            greenButtonSettings.WaterDisplayUom = Convert.ToString(item.Value);
                            break;
                        case "greenbutton.maxquerydaterange":
                            greenButtonSettings.MaxQueryDateRange = Convert.ToInt32(item.Value);
                            break;
                    }

                }
            }

            var contentLocale = cp.GetContentLocale(locale);
            var greentButtonContents = cp.GetContentTextContent("greenbutton", contentLocale, string.Empty);
            if (greentButtonContents != null)
            {
                foreach (var item in greentButtonContents)
                {
                    switch (item.Key.ToLower())
                    {
                        case "greenbutton.reportprename":
                            greenButtonSettings.ReportPreName = Convert.ToString(item.ShortText);
                            break;
                        case "greenbutton.reporttitle":
                            greenButtonSettings.ReportTitle = Convert.ToString(item.ShortText);
                            break;
                        case "greenbutton.usagepointtitle":
                            greenButtonSettings.UsagePointTitle = Convert.ToString(item.ShortText);
                            break;
                        case "greenbutton.intervalblocktitle":
                            greenButtonSettings.IntervalBlockTitle = Convert.ToString(item.ShortText);
                            break;
                        case "greenbutton.usagessummarytitle":
                            greenButtonSettings.UsageSummaryTitle = Convert.ToString(item.ShortText);
                            break;
                    }

                }
            }
            
            // get xslt 
            string filename;
            greenButtonSettings.ProcessInfoFile = GetXsltStyleSheet(clientId, out filename);
            greenButtonSettings.ProcessorInfoFileName = filename;

            return greenButtonSettings;
        }

        /// <summary>
        /// convert output format from string to output format type
        /// </summary>
        /// <param name="outputFormat"></param>
        /// <returns></returns>
        public static OutputFormatType GetOutputFormat(string outputFormat)
        {
            OutputFormatType output;
            if (outputFormat.Equals(OutputFormatType.Xml.ToString(), StringComparison.InvariantCultureIgnoreCase))
                output = OutputFormatType.Xml;
            else
                output = OutputFormatType.Zip;

            return output;
        }
        
        /// <summary>
        /// get xml template 
        /// </summary>
        /// <returns></returns>
        private static string GetXmlTemplate()
        {
            string greenButtonXmlTemplateStr;
            try
            {
                var xmlDoc = new XmlDocument();
                var fullPath = System.Web.Hosting.HostingEnvironment.MapPath(@"~/Content/GreenButtonXmlTemplate.xml");
                if (fullPath != null)
                    xmlDoc.Load(fullPath);
                else
                {
                    xmlDoc.Load("Content\\GreenButtonXmlTemplate.xml");
                }
                //greenButtonXmlTemplateStr = ConvertXmltoString(xmlDoc);
                greenButtonXmlTemplateStr = xmlDoc.InnerXml;
            }
            catch (Exception)
            {
                greenButtonXmlTemplateStr = string.Empty;
            }
            return greenButtonXmlTemplateStr;
        }

        /// <summary>
        /// get xslt style sheet
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="filename"></param>
        /// <returns></returns>
        private static string GetXsltStyleSheet(int clientId, out string filename)
        {
            string xsltStyleSheetStr;
            var fullPath = System.Web.Hosting.HostingEnvironment.MapPath($"~\\Content\\Clients\\{clientId}\\Xslt");
            if (fullPath == null)
                fullPath = $"Content\\Clients\\{clientId}\\Xslt";

            if (Directory.Exists(fullPath))
            {
                var file = Directory.GetFiles(fullPath);
                {
                    var xmlDoc = new XmlDocument();
                    xmlDoc.Load(file[0]);

                    //xsltStyleSheetStr = ConvertXmltoString(xmlDoc);
                    xsltStyleSheetStr = xmlDoc.InnerXml;


                    var filepath = file[0].Split('\\');
                    filename = filepath[filepath.GetUpperBound(0)];
                }
            }
            else
            {
                fullPath = System.Web.Hosting.HostingEnvironment.MapPath($"~\\Content\\Clients\\0\\Xslt");
                if (fullPath == null)
                    fullPath = $"Content\\Clients\\0\\Xslt";

                var file = Directory.GetFiles(fullPath);
                {
                    var xmlDoc = new XmlDocument();
                    xmlDoc.Load(file[0]);

                    //xsltStyleSheetStr = ConvertXmltoString(xmlDoc);
                    xsltStyleSheetStr = xmlDoc.InnerXml;

                    var filepath = file[0].Split('\\');
                    filename = filepath[filepath.GetUpperBound(0)];
                }
            }

            return xsltStyleSheetStr;

        }

        public static string GetGreenButtonDomain(Uri requestUri)
        {
            var greenButtonDomain = $"{requestUri.Scheme}://{requestUri.Host}";


            var absolutePath = requestUri.AbsolutePath;
            var datacustodainPrefix = absolutePath.IndexOf("datacustodian", StringComparison.InvariantCultureIgnoreCase);
            if (datacustodainPrefix > -1)
            {
                greenButtonDomain = $"{greenButtonDomain}/{absolutePath.Substring(1, datacustodainPrefix - 2)}";
            }

            return greenButtonDomain;
        }

        // to be removed once long-term solution is in placed
        public static bool IsMidTermClient(string token)
        {
            var decVal = GreenButtonConnect.Helper.Base64Decode(token);
            decVal = GreenButtonConnect.Helper.Decrypt(decVal);
            var clientId = "0";
            var midtermClientIds =
                System.Configuration.ConfigurationManager.AppSettings.Get("GBCMidTermClientId");
            var redirectClients = midtermClientIds.Split(',').ToList();
            if (decVal.Split(',').GetUpperBound(0) > 2)
            {
                clientId = GreenButtonConnect.Helper.GetAclaraClientIdFromDataCustodianId(decVal.Split(',')[3])
                    .Split(':')[0];
            }
            if (redirectClients.Exists(c => c.Trim() == clientId))
            {
                return true;
            }
            return false;
        }

        //private string ConvertXmltoString(XmlDocument xmlDoc)
            //{
            //    StringBuilder sb = new StringBuilder();
            //    XmlWriterSettings settings = new XmlWriterSettings
            //    {
            //        Indent = true,
            //        IndentChars = "  ",
            //        NewLineChars = "\r\n",
            //        NewLineHandling = NewLineHandling.Replace
            //    };
            //    using (XmlWriter writer = XmlWriter.Create(sb, settings))
            //    {
            //        xmlDoc.Save(writer);
            //    }

            //    return sb.ToString();
            //}
        

    }
}