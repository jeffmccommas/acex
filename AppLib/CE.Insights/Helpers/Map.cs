﻿using CE.BillToDate;

namespace CE.Insights.Helpers
{
    public static class Map
    {

        public static FuelType ConvertFuelType(string commodityKey)
        {
            var fuel = FuelType.undefined;
            switch (commodityKey.ToLower())
            {
                case "electric":
                    fuel = FuelType.electric;
                    break;
                case "gas":
                    fuel = FuelType.gas;
                    break;
                case "water":
                    fuel = FuelType.water;
                    break;
            }
            return fuel;
        }

        public static UnitOfMeasureType ConvertUnitOfMeasureType(string uomKey)
        {
            var uom = UnitOfMeasureType.undefined;
            switch (uomKey.ToLower())
            {
                case "cf":
                    uom = UnitOfMeasureType.CF;
                    break;
                case "ccf":
                    uom = UnitOfMeasureType.CCF;
                    break;
                case "hcf":
                    uom = UnitOfMeasureType.HCF;
                    break;
                case "mcf":
                    uom = UnitOfMeasureType.MCF;
                    break;
                case "gal":
                    uom = UnitOfMeasureType.gal;
                    break;
                case "cgal":
                    uom = UnitOfMeasureType.CGal;
                    break;
                case "kgal":
                    uom = UnitOfMeasureType.kGal;
                    break;
                case "kwh":
                    uom = UnitOfMeasureType.kWh;
                    break;
                case "therms":
                    uom = UnitOfMeasureType.Therm;
                    break;
                case "other":
                    uom = UnitOfMeasureType.other;
                    break;
            }
            return uom;
        }


        public static string ConvertUomString(UnitOfMeasureType uom)
        {
            var uomString = string.Empty;
            switch (uom)
            {
                case UnitOfMeasureType.CCF:
                    uomString = CE.Models.Insights.Types.UnitOfMeasureType.CCF.ToString().ToLower();
                    break;
                case UnitOfMeasureType.HCF:
                    uomString = CE.Models.Insights.Types.UnitOfMeasureType.HCF.ToString().ToLower();
                    break;
                case UnitOfMeasureType.MCF:
                    uomString = CE.Models.Insights.Types.UnitOfMeasureType.MCF.ToString().ToLower();
                    break;
                case UnitOfMeasureType.gal:
                    uomString = CE.Models.Insights.Types.UnitOfMeasureType.Gal.ToString().ToLower();
                    break;
                case UnitOfMeasureType.kWh:
                    uomString = CE.Models.Insights.Types.UnitOfMeasureType.kWh.ToString().ToLower();
                    break;
                case UnitOfMeasureType.Therm:
                    uomString = CE.Models.Insights.Types.UnitOfMeasureType.Therms.ToString().ToLower();
                    break;
                case UnitOfMeasureType.CF:
                    uomString = CE.Models.Insights.Types.UnitOfMeasureType.CF.ToString().ToLower();
                    break;
                case UnitOfMeasureType.CGal:
                    uomString = CE.Models.Insights.Types.UnitOfMeasureType.CGal.ToString().ToLower();
                    break;
                case UnitOfMeasureType.kGal:
                    uomString = CE.Models.Insights.Types.UnitOfMeasureType.KGAL.ToString().ToLower();
                    break;
                case UnitOfMeasureType.other:
                    uomString = CE.Models.Insights.Types.UnitOfMeasureType.Other.ToString().ToLower();
                    break;
            }

            return uomString;
        }
        public static string ConvertCommodity(Meter meter)
        {
            var commodity = string.Empty;

            switch (meter.Fuel)
            {
                case FuelType.electric:
                    commodity = FuelType.electric.ToString();
                    break;
                case FuelType.gas:
                    commodity = FuelType.gas.ToString();
                    break;
                case FuelType.water:
                    if (meter.DerivedFuel == DerivedFuelType.sewer && meter.DerivedFuelIsPrimary)
                    {
                        commodity = DerivedFuelType.sewer.ToString();
                    }
                    else
                    {
                        commodity = FuelType.water.ToString();
                    }
                    break;
            }
            return commodity;

        }
    }
}