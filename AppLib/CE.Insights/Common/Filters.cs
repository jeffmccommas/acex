﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using CE.Infrastructure;
using CE.Models;
using CE.Models.Insights.EF;
using CE.GreenButtonConnect;
using CE.Models.Insights;
using ActionFilterAttribute = System.Web.Http.Filters.ActionFilterAttribute;


// ReSharper disable once CheckNamespace
namespace CE.Insights.Common.Filters
{
    public class RequireHttpsAndClientCertAttribute : AuthorizationFilterAttribute
    {
        private const int PropertyClientCertThumbprint = 2;

        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if (actionContext.Request.RequestUri.Scheme != Uri.UriSchemeHttps)
            {
                actionContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.Forbidden)
                {
                    ReasonPhrase = "HTTPS Required"
                };
            }
            else
            {
                if (actionContext.Request.RequestUri.AbsolutePath.IndexOf("oauth/token", 0, StringComparison.Ordinal) > 0)
                {
                    if (actionContext.Request.Headers.Authorization.Scheme == AuthType.Basic.ToString())
                    {
                        var decVal = string.Empty;
                        try
                        {
                            decVal = Helper.Base64Decode(actionContext.Request.Headers.Authorization.Parameter);

                        }
                        catch (Exception)
                        {
                            actionContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.BadRequest)
                            {
                                ReasonPhrase = "Basic token not valid."
                            };
                        }

                        if (decVal.Length > 0)
                        {
                            var clientId = decVal.Split(':')[0];
                            var secret = decVal.Split(':')[1];
                            //check client id and secret
                            List<application_information> appInfo;
                            using (var ent = new InsightsEntities())
                            {
                                appInfo = (from ai in ent.application_information
                                           where ai.client_id == clientId && ai.client_secret == secret
                                           select ai).ToList();
                            }

                            bool tokenValid;
                            if (appInfo.Count == 0)
                            {
                                tokenValid = false;
                            }
                            else
                            {
                                var applicationInfo = appInfo.FirstOrDefault();
                                if (applicationInfo != null)
                                {
                                    var timeValid =
                                        Helper.ConvertFromUnixTimestamp(applicationInfo.client_secret_expires_at);
                                    tokenValid = timeValid > DateTime.UtcNow;
                                }
                                else
                                    tokenValid = false;
                            }

                            if (!tokenValid)
                            {
                                actionContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.BadRequest)
                                {
                                    ReasonPhrase = "Authorization value not valid."
                                };
                            }
                        }
                    }
                    else
                    {
                        actionContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.BadRequest)
                        {
                            ReasonPhrase = "Authorization scheme not supported."
                        };
                    }

                    base.OnAuthorization(actionContext);
                }
                else if (actionContext.Request.RequestUri.AbsolutePath.IndexOf("resource", 0, StringComparison.Ordinal) > 0)
                {
                    if (actionContext.Request.Headers.Authorization.Scheme == AuthType.Bearer.ToString())
                    {

                        if (actionContext.Request.RequestUri.AbsolutePath.IndexOf("readservicestatus", 0, StringComparison.OrdinalIgnoreCase) == -1)
                        {
                            var decVal = string.Empty;
                            try
                            {
                                decVal = Helper.Base64Decode(actionContext.Request.Headers.Authorization.Parameter);

                            }
                            catch (Exception)
                            {
                                actionContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.BadRequest)
                                {
                                    ReasonPhrase = "Bearer token not valid."
                                };
                            }

                            if (decVal.Length > 0)
                            {
                                authorizations auths;
                                decVal = Helper.Decrypt(decVal);
                                var clientId = decVal.Split(',')[0];
                                var secret = decVal.Split(',')[1];
                                if (
                                    actionContext.Request.RequestUri.AbsolutePath.IndexOf("applicationinformation", 0,
                                        StringComparison.OrdinalIgnoreCase) > 0)
                                {
                                    using (var ent = new InsightsEntities())
                                    {
                                        var appInfo = (from ai in ent.application_information
                                            where ai.client_id == clientId && ai.client_secret == secret
                                            select ai).ToList();
                                        var appId = appInfo.FirstOrDefault()?.application_information_id;
                                        auths = (from ai in ent.application_information
                                            join au in ent.authorizations on ai.application_information_id equals
                                                au.application_information_id
                                            where
                                                ai.client_id == clientId && ai.client_secret == secret &&
                                                ai.enabled == true
                                                && au.application_information_id == appId
                                                && au.third_party == "REGISTRATION_third_party"
                                                && au.status == (int) Helper.AuthorizationStatus.Active
                                            select au).FirstOrDefault();

                                    }
                                }
                                else if (
                               actionContext.Request.RequestUri.AbsolutePath.IndexOf("authorization", 0,
                                   StringComparison.OrdinalIgnoreCase) > 0)
                                {
                                    using (var ent = new InsightsEntities())
                                    {
                                        var appInfo = (from ai in ent.application_information
                                                       where ai.client_id == clientId && ai.client_secret == secret
                                                       select ai).ToList();
                                        var appId = appInfo.FirstOrDefault()?.application_information_id;
                                        auths = (from ai in ent.application_information
                                                 join au in ent.authorizations on ai.application_information_id equals
                                                     au.application_information_id
                                                 where ai.client_id == clientId && ai.client_secret == secret && ai.enabled == true
                                                       && au.application_information_id == appId
                                                       && au.third_party == "third_party_admin"
                                                       && au.status == (int)Helper.AuthorizationStatus.Active
                                                 select au).FirstOrDefault();

                                    }
                                }
                                else
                                {
                                    var retailCustomerId = Convert.ToInt64(decVal.Split(',')[2]);
                                    using (var ent = new InsightsEntities())
                                    {
                                        var appInfo = (from ai in ent.application_information
                                            where ai.client_id == clientId && ai.client_secret == secret
                                            select ai).ToList();
                                        var appId = appInfo.FirstOrDefault()?.application_information_id;
                                        auths = (from ai in ent.application_information
                                            join au in ent.authorizations on ai.application_information_id equals
                                                au.application_information_id
                                            join ret in ent.retail_customers on au.retail_customer_id equals ret.id
                                            where
                                                ai.client_id == clientId && ai.client_secret == secret &&
                                                ai.enabled == true
                                                && au.application_information_id == appId
                                                && au.third_party == "third_party" &&
                                                au.retail_customer_id == retailCustomerId
                                                && au.status == (int) Helper.AuthorizationStatus.Active
                                            select au).FirstOrDefault();

                                    }
                                }



                                var tokenValid = false;
                                if (auths != null)
                                {
                                    var expIn = auths.expiresin ?? 0;
                                    var timeValid = Helper.ConvertFromUnixTimestamp(expIn);
                                    tokenValid = timeValid > DateTime.UtcNow;
                                }

                                if (!tokenValid)
                                {
                                    actionContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.BadRequest)
                                    {
                                        ReasonPhrase = "Token not valid."
                                    };
                                }
                                else
                                {
                                    //Check for enddate
                                    var endDate = auths.end_date;
                                    if (endDate != null)
                                    {
                                        if (endDate < DateTime.UtcNow)
                                        {
                                            actionContext.Response =
                                                new HttpResponseMessage(System.Net.HttpStatusCode.BadRequest)
                                                {
                                                    ReasonPhrase = "Token not valid."
                                                };
                                        }
                                    }
                                    else
                                    {
                                        var dur = auths.ap_duration ?? 0;
                                        var stDate = auths.ap_start ?? 0;
                                        var timeValid = Helper.ConvertFromUnixTimestamp(stDate + dur);
                                        if (!(timeValid > DateTime.UtcNow))
                                        {
                                            actionContext.Response =
                                                new HttpResponseMessage(System.Net.HttpStatusCode.BadRequest)
                                                {
                                                    ReasonPhrase = "Token not valid."
                                                };
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        actionContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.BadRequest)
                        {
                            ReasonPhrase = "Authorization scheme not supported."
                        };
                    }
                    base.OnAuthorization(actionContext);
                }
                else
                {
                    var clientUser = (ClientUser)actionContext.Request.Properties[CEConfiguration.CEClientUser];
                    var env = (int)Enum.Parse(typeof(EnvironmentType), clientUser.Environment.ToString(), true);
                    using (var ent = new InsightsEntities())
                    {
                        if (clientUser.Environment.ToString() != "localdev")
                        {
                            var thumbprint = (from cp in ent.ClientProperty
                                              where
                                                  cp.ClientID == clientUser.ClientID && cp.EnvID == env &&
                                                  cp.PropertyID == PropertyClientCertThumbprint
                                              select cp.PropValue).FirstOrDefault();

                            if (thumbprint != null)
                            {
                                if (actionContext.Request.GetClientCertificate() == null)
                                {
                                    actionContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.Forbidden)
                                    {
                                        ReasonPhrase = "Client Certificate Required"
                                    };
                                }
                                else if (actionContext.Request.GetClientCertificate().Thumbprint != thumbprint)
                                {
                                    actionContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.Forbidden)
                                    {
                                        ReasonPhrase = "Bad Client Certificate"
                                    };
                                }
                                else
                                {
                                    base.OnAuthorization(actionContext);
                                }
                            }
                            else
                            {
                                base.OnAuthorization(actionContext);
                            }
                        }
                        else
                        {
                            base.OnAuthorization(actionContext);
                        }
                    }
                }
            }
        }
    }

    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class GreenButtonConnectParameterNameMapAttribute : ActionFilterAttribute
    {
        public string InboundParameterName { get; set; }
        public string ActionParameterName { get; set; }


        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            GreenButtonConnectRequest request = (GreenButtonConnectRequest)actionContext.ActionArguments["request"];

            var query = actionContext.Request.RequestUri.Query;

            if (query.Length > 0)
            {
                query = query.Remove(0, 1);
                var parameters = query.Split('&').ToList();

                var param = parameters.Find(p => p.Contains(InboundParameterName.ToLower()));
                if (param != null)
                {
                    var value = param.Split('=')[1];

                    switch (ActionParameterName.ToLower())
                    {
                        case "publishedmax":
                            request.PublishedMax = value;
                            break;
                        case "publishedmin":
                            request.PublishedMin = value;
                            break;
                        case "updatedmax":
                            request.UpdatedMax = value;
                            break;
                        case "updatedmin":
                            request.UpdatedMin = value;
                            break;
                        case "maxresult":
                            request.MaxResult = value;
                            break;
                        case "startindex":
                            request.StartIndex = value;
                            break;
                        case "depth":
                            request.Depth = value;
                            break;
                        default:
                            throw new Exception("Parameter not found on controller: " + ActionParameterName);
                    }

                    actionContext.ActionArguments["request"] = request;
                }
            }

        }
    }
}
