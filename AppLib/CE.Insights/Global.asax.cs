﻿using CE.AO.Business;
using CE.Infrastructure;
using CE.Infrastructure.Encrypter;
using CE.Infrastructure.Filters;
using System;
using System.Configuration;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Dispatcher;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;


namespace CE.Insights
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            UnityConfig.RegisterComponents();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AutoMapperConfig.AutoMapperMappings();


            // enable API versioning
            GlobalConfiguration.Configuration.Services.Replace(typeof(IApiExplorer), new VersionedApiExplorer(GlobalConfiguration.Configuration));
            GlobalConfiguration.Configuration.Services.Replace(typeof(IHttpControllerSelector), new RouteVersionedControllerSelector(GlobalConfiguration.Configuration));

            //GlobalConfiguration.Configuration.Services.Replace(typeof(IDocumentationProvider), new XmlCommentDocumentationProvider( ));
            //GlobalConfiguration.Configuration.Services.Replace(typeof(IHttpControllerSelector), new AcceptHeaderVersionedControllerSelector(GlobalConfiguration.Configuration));

            //GlobalConfiguration.Configuration.Formatters.Add(new CE.Infrastructure.Formatter.JsonpMediaTypeFormatter());
            string root = HttpContext.Current.Server.MapPath("~");
            //GlobalConfiguration.Configuration.Formatters.Add(new CE.Infrastructure.Formatter.RazorFormatter(root));

            var formatters = GlobalConfiguration.Configuration.Formatters;
            //formatters.Remove(formatters.XmlFormatter);

            var json = GlobalConfiguration.Configuration.Formatters.JsonFormatter;
            json.SerializerSettings.Formatting = Newtonsoft.Json.Formatting.Indented;
            //json.SerializerSettings.DateFormatHandling = Newtonsoft.Json.DateFormatHandling.MicrosoftDateFormat;
            //json.SerializerSettings.DateTimeZoneHandling = Newtonsoft.Json.DateTimeZoneHandling.Utc;
            json.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
            json.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());

            GlobalConfiguration.Configuration.Formatters.Insert(0, json);

            // Encrypted Action Paramater
            GlobalConfiguration.Configuration.Filters.Add(new EncryptedActionParameterAttribute()); //ValidateClientParameterAttribute

            // Endpoint tracking
            GlobalConfiguration.Configuration.Filters.Add(new EndpointTrackingActionAttribute());

            // Model Validation
            GlobalConfiguration.Configuration.Filters.Add(new ValidateModelAttribute());

            //Exception handling
            var errorConfig = (CustomErrorsSection)ConfigurationManager.GetSection("system.web/customErrors");
            IncludeErrorDetailPolicy errorDetailPolicy;
            switch (errorConfig.Mode)
            {
                case CustomErrorsMode.RemoteOnly:
                    errorDetailPolicy = IncludeErrorDetailPolicy.LocalOnly;
                    break;
                case CustomErrorsMode.On:
                    errorDetailPolicy = IncludeErrorDetailPolicy.Never;
                    break;
                case CustomErrorsMode.Off:
                    errorDetailPolicy = IncludeErrorDetailPolicy.Always;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            GlobalConfiguration.Configuration.IncludeErrorDetailPolicy = errorDetailPolicy;

            if (errorDetailPolicy != IncludeErrorDetailPolicy.Always)
            {
                var exceptionHandler = new ExceptionHandlingAttribute();
                //exceptionHandler.Mappings.Add(typeof(NotAuthorizedException), HttpStatusCode.Forbidden);
                GlobalConfiguration.Configuration.Filters.Add(exceptionHandler);
            }

            // Additional JSON serialization support
            ValueProviderFactories.Factories.Add(new JsonValueProviderFactory());

            //var UnityContainer = UnityConfig.GetConfiguredContainer();
            //GlobalConfiguration.Configuration.DependencyResolver = new unitydepe

        }
    }
}
