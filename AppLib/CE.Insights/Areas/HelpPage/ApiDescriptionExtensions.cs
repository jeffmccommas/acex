using System;
using System.Text;
using System.Web;
using System.Web.Http.Description;

namespace CE.Insights.Areas.HelpPage
{
    public static class ApiDescriptionExtensions
    {
        /// <summary>
        /// Generates an URI-friendly ID for the <see cref="ApiDescription"/>. E.g. "Get-Values-id_name" instead of "GetValues/{id}?name={name}"
        /// </summary>
        /// <param name="description">The <see cref="ApiDescription"/>.</param>
        /// <returns>The ID as a string.</returns>
        public static string GetFriendlyId(this ApiDescription description)
        {
            string path = description.RelativePath;
            if (path.IndexOf("{folder}/Authorize", 0, StringComparison.Ordinal) > 0 || path.IndexOf("{folder}/Token", 0, StringComparison.Ordinal) > 0)
            {
                path = path.Replace("{folder}", "oauth");
            }
            else if (path.IndexOf("v1/UsagePoint", 0, StringComparison.Ordinal) > 0)
            {
                //do nothing
            }
            else if (path.IndexOf("{folder}", 0, StringComparison.Ordinal) > 0 || path.IndexOf("Resource", 0, StringComparison.Ordinal) > 0)
            {
                return "";
            }
            else if (path.IndexOf("Authorize", 0, StringComparison.Ordinal) > 0 || path.IndexOf("/Token", 0, StringComparison.Ordinal) > 0)
            {
                return "";
            }

            string[] urlParts = path.Split('?');
            string localPath = urlParts[0];
            string queryKeyString = null;
            if (urlParts.Length > 1)
            {
                string query = urlParts[1];
                string[] queryKeys = HttpUtility.ParseQueryString(query).AllKeys;
                queryKeyString = String.Join("_", queryKeys);
            }

            StringBuilder friendlyPath = new StringBuilder();
            friendlyPath.AppendFormat("{0}-{1}",
                description.HttpMethod.Method,
                localPath.Replace("/", "-").Replace("{", String.Empty).Replace("}", String.Empty));
            if (queryKeyString != null)
            {
                friendlyPath.AppendFormat("_{0}", queryKeyString);
            }
            return friendlyPath.ToString();
        }
    }
}