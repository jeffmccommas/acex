﻿using CE.Infrastructure;
using System;
using System.Configuration;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Tracing;

namespace CE.Insights
{
    public static class WebApiConfig
    {
        private const string ThrottleMaxRequests = "ThrottleMaxRequests";
        private const string ThrottleTimeSpanInHours = "ThrottleTimeSpanInHours";
        public static void Register(HttpConfiguration config)
        {
            config.MessageHandlers.Add(new HttpPersistDataHandler());
            config.MessageHandlers.Add(new HmacAuthenticationHandler(new CanonicalRepresentationBuilder(), new HmacSignatureCalculator()));

            var throttleMaxRequests = Convert.ToInt32(ConfigurationManager.AppSettings.Get(ThrottleMaxRequests));
            var throttleTimeSpanInHours = Convert.ToInt32(ConfigurationManager.AppSettings.Get(ThrottleTimeSpanInHours));
            config.MessageHandlers.Add(new ThrottlingHandlerWithFixedIdentifier(new InMemoryThrottleStore(), id => throttleMaxRequests, TimeSpan.FromHours(throttleTimeSpanInHours)));

            // Attribute routing.
            config.MapHttpAttributeRoutes();
            config.Routes.MapHttpRoute(
               name: "SubFolderApi",
               routeTemplate: "api/v{version}/{folder}/{controller}/{id}",
               constraints: null,
               defaults: new { id = RouteParameter.Optional }
           );

            config.Routes.MapHttpRoute(
               name: "DefaultApi",
               routeTemplate: "api/v{version}/{controller}/{id}",
               constraints: null,
               defaults: new { id = RouteParameter.Optional }
           );

            // Cors
            var cors = new EnableCorsAttribute("*", "*", "GET"); // origins, headers, methods
            config.EnableCors(cors);

            config.Services.Replace(typeof(ITraceWriter), new NLogTraceWriter());
        }
    }
}
