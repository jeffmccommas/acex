using AO.Registrar;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Dependencies;

namespace CE.Insights {
    public static class UnityConfig {
        public static void RegisterComponents() {
            try {
                var container = (new DataStorageRegistrar()).Initialize<HierarchicalLifetimeManager>();
                
                // register all your components with the container here
                // it is NOT necessary to register your controllers

                // e.g. container.RegisterType<ITestService, TestService>();
                //

                GlobalConfiguration.Configuration.DependencyResolver = new UnityResolver(container);
            }
            catch {
                // do nothing since there are api calls not using aclara one
            }
        }
    }

    public class UnityResolver : IDependencyResolver {
        protected IUnityContainer container;

        public UnityResolver(IUnityContainer container) {
            if (container == null) {
                throw new ArgumentNullException("container");
            }
            this.container = container;
        }

        public object GetService(Type serviceType) {
            try {
                return container.Resolve(serviceType);
            }
            catch (ResolutionFailedException) {
                return null;
            }
        }

        public IEnumerable<object> GetServices(Type serviceType) {
            try {
                return container.ResolveAll(serviceType);
            }
            catch (ResolutionFailedException) {
                return new List<object>();
            }
        }

        public IDependencyScope BeginScope() {
            var child = container.CreateChildContainer();
            return new UnityResolver(child);
        }

        public void Dispose() {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing) {
            container.Dispose();
        }
    }
}