﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using CE.BillDisagg;
using CE.EnergyModel;
using CE.EnergyModel.Enums;
using CE.Infrastructure;
using CE.Models;
using CE.Models.Insights;
using StatusType = CE.Models.Insights.Types.StatusType;

namespace CE.Insights.Models
{
    public class EnergyModelModel
    {
        /// <summary>
        /// Execute Energy Model given parameters and return a response with results.
        /// </summary>
        /// <param name="clientUser"></param>
        /// <param name="request"></param>
        /// <param name="response"></param>
        public void ExecuteEnergyModel(ClientUser clientUser, EnergyModelPostRequest request, EnergyModelPostResponse response)
        {
            EnergyModelResult result = null;
            string message = null;
            var statusType = StatusType.Undefined;
            BillDisaggResult bdResult = null;

            var sw = Stopwatch.StartNew();
            var swem = Stopwatch.StartNew();

            var factory = new EnergyModelManagerFactory();
            var mgr = factory.CreateEnergyModelManager(clientUser.ClientID);

            //Map region parameters (if provided)
            var regionParameters = MapRegionParameters(request.Data.RegionParameters);

            //Map customer parameters (if provided)
            var idParameters = MapIDParameters(request.Data.CustomerParameters);

            //Map appliance list (if provided)
            var appliances = MapAppliances(request.Data.Appliances, request.Data.StandAloneAppliances);

            //Map end uses (if provided)
            var endUses = MapEndUses(request.Data.EndUses, request.Data.StandAloneEndUses);

            //Map profile attributes (if provided)
            var profileAttributes = MapProfileAttributes(request.Data.ProfileAttributes, request.Data.StandAloneProfileAttributes);

            //Map bills (if provided - only for bill disagg)
            var bdBills = MapBills(request.Data.Bills, request.Data.ExecuteBillDisagg);

            //Map extra parameters (if provided)
            var extraParams = MapExtraParameters(request.Data.OutputMonthly, request.Data.OutputTrace, request.Data.ChunkSize, request.Data.ExecuteBillDisagg);

            if (regionParameters != null)
            {
                //First use region parameters
                result = mgr.ExecuteEnergyModel(regionParameters, idParameters, extraParams, profileAttributes, appliances, endUses);
            }
            else if (idParameters != null)
            {
                //Second use customer id parameters (this may just be zipcode)
                result = mgr.ExecuteEnergyModel(idParameters, extraParams, profileAttributes, appliances, endUses);
            }
            else
            {
                message = CEConfiguration.ErrorEMIllegalParameters;
                response.StatusType = StatusType.ParameterErrors;
            }

            swem.Stop();
            if (result != null)
            {
                result.Metrics.Add(new Metric("EnergyModel", swem.ElapsedMilliseconds));

                //do BILLDISAGG if indicated
                if (extraParams.ExecuteBillDisagg)
                {
                    var bdsw = Stopwatch.StartNew();

                    //use the emresult as an input
                    var bdInputParams = new DisaggInputParams
                    {
                        EMResultAsInput = result,
                        Bills = bdBills
                    };

                    //use bills from input if present

                    if (idParameters != null)
                    {
                        bdInputParams.CustomerId = idParameters.CustomerId;
                        bdInputParams.AccountId = idParameters.AccountId;
                        bdInputParams.PremiseId = idParameters.PremiseId;
                    }

                    var bdmFactory = new BillDisaggManagerFactory();
                    var bdMgr = bdmFactory.CreateBillDisaggManager(clientUser.ClientID);
                    bdResult = bdMgr.ExecuteBillDisagg(bdInputParams);

                    //reassign appliances and enduses from bdResult to result
                    result.Appliances = bdResult.Appliances;
                    result.EndUses = bdResult.EndUses;
                    result.TotalEnergyUses.Clear();

                    bdsw.Stop();
                    result.Metrics.Add(new Metric("BillDisagg", bdsw.ElapsedMilliseconds));
                }

                sw.Stop();
                result.Metrics.Add(new Metric("All", sw.ElapsedMilliseconds));

                // MAP RESPONSE HERE
                if (result.StatusList != null && result.StatusList.HasNoErrors())
                {
                    var emRegionParams = MapRegionParameters(result.RegionParams);
                    var emIdParams = MapIDParameters(result.IDParams);
                    var emAppliances = MapAppliances(result.Appliances);
                    var emEndUses = MapEndUses(result.EndUses);
                    var emTotalEnergyUses = MapTotalEnergyUses(result.TotalEnergyUses);
                    var emMetrics = MapMetrics(result.Metrics);

                    var data = new EMResponseContainer
                    {
                        ClientId = clientUser.ClientID,
                        Confidence = result.Confidence,
                        RegionParameters = emRegionParams,
                        CustomerParameters = emIdParams,
                        Appliances = emAppliances,
                        EndUses = emEndUses,
                        TotalEnergyUses = emTotalEnergyUses,
                        Metrics = emMetrics,
                        ModelType = MapModelType(result.ModelType)
                    };


                    //extra bill disaggregation data if applicable
                    if (extraParams.ExecuteBillDisagg && bdResult != null)
                    {
                        data.Bills = MapBills(bdResult.ActualBills);
                        data.ComputedBills = MapBills(bdResult.ComputedBills);
                        data.DisaggStatuses = MapDisaggStatuses(bdResult.DisaggStatuses);
                    }

                    //for verbose output
                    if (request.Data.ReturnProfileAttributes.HasValue && request.Data.ReturnProfileAttributes.Value)
                    {
                        var emProfileAttributes = MapProfileAttributes(result.ProfileAttributes);
                        data.ProfileAttributes = emProfileAttributes;
                    }

                    response.Data = data;

                }
                else
                {
                    if (statusType == StatusType.Undefined)
                    {
                        message = CEConfiguration.ErrorEMFailedExecution;
                        statusType = StatusType.ModelErrors;
                    }
                }
            }

            response.Message = message;
            response.StatusType = statusType;

        }


        /// <summary>
        /// Map to List of Energy Use.
        /// </summary>
        /// <param name="energyUseColl"></param>
        /// <returns></returns>
        private List<EMEnergyUse> MapTotalEnergyUses(EnergyUseCollection energyUseColl)
        {
            List<EMEnergyUse> energyUses = null;

            if (energyUseColl != null && energyUseColl.Count > 0)
            {
                energyUses = new List<EMEnergyUse>();

                foreach (var energyUse in energyUseColl)
                {
                    var emEU = new EMEnergyUse
                    {
                        Quantity = energyUse.Quantity,
                        CommodityName = ConvertCommodity(energyUse.Commodity),
                        UnitOfMeasureName = ConvertUnitOfMeasure(energyUse.UnitOfMeasure)
                    };

                    if (energyUse.EvaluationContext != null)
                    {
                        var ctx = new EMEvaluationContext
                        {
                            Expression = null,
                            ContextName = ConvertEvaluationType(energyUse.EvaluationContext.EvaluationType)
                        };
                        // energyUse.EvaluationContext.Expression; //do not send an expression back to caller
                        emEU.EvaluationContext = ctx;
                    }

                    // if there is period energy use (usually monthly if present)
                    if (energyUse.Periods != null)
                    {
                        emEU.Periods = new List<EMPeriod>();

                        foreach (var p in energyUse.Periods)
                        {
                            var per = new EMPeriod
                            {
                                Number = p.Number,
                                Name = p.Name,
                                Quantity = p.Quantity
                            };

                            if (per.Start.HasValue)
                            {
                                p.Start = per.Start;
                            }

                            if (per.End.HasValue)
                            {
                                p.End = per.End;
                            }

                            emEU.Periods.Add(per);
                        }

                    }

                    energyUses.Add(emEU);
                }
            }

            return (energyUses);
        }



        /// <summary>
        /// Map to ApplianceCollection.
        /// </summary>
        /// <param name="apps"></param>
        /// <param name="standAlone"></param>
        /// <returns></returns>
        private ApplianceCollection MapAppliances(IReadOnlyCollection<EMAppliance> apps, bool? standAlone)
        {
            ApplianceCollection appliances = null;

            if (apps != null && apps.Count > 0)
            {
                appliances = new ApplianceCollection();

                if (standAlone != null)
                {
                    appliances.IsStandAlone = standAlone.Value;
                }

                foreach (var a in apps)
                {
                    var appliance = new EnergyModel.Appliance {Name = a.Name};

                    if (a.EnergyUses != null)
                    {
                        foreach (var e in a.EnergyUses)
                        {
                            var energyuse = new EnergyUse
                            {
                                Quantity = e.Quantity,
                                Commodity = ConvertCommodityName(e.CommodityName),
                                UnitOfMeasure = ConvertUnitOfMeasureName(e.UnitOfMeasureName)
                            };

                            if (e.EvaluationContext != null)
                            {
                                var ctx = new EvaluationContext
                                {
                                    Expression = e.EvaluationContext.Expression,
                                    EvaluationType = ConvertEvaluationTypeName(e.EvaluationContext.ContextName)
                                };
                                energyuse.EvaluationContext = ctx;
                            }

                            appliance.EnergyUses.Add(energyuse);
                        }
                    }

                    appliances.Add(appliance);
                }
            }

            return (appliances);
        }

        /// <summary>
        /// Map to List of EMMetric.
        /// </summary>
        /// <param name="metricColl"></param>
        /// <returns></returns>
        private static List<EMMetric> MapMetrics(MetricCollection metricColl)
        {
            List<EMMetric> metrics = null;

            if (metricColl != null && metricColl.Count > 0)
            {
                metrics = metricColl.Select(metric => new EMMetric()
                {
                    Name = metric.Name, Duration = metric.Duration
                }).ToList();
            }

            return metrics;
        }


        /// <summary>
        /// ConvertCommodityName to CommodityType
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private static CommodityType ConvertCommodityName(string name)
        {
            CommodityType type;

            switch (name.ToLower())
            {
                case "electric":
                case "electricity":
                    type = CommodityType.Electric;
                    break;
                case "gas":
                    type = CommodityType.Gas;
                    break;
                case "oil":
                    type = CommodityType.Oil;
                    break;
                case "propane":
                    type = CommodityType.Propane;
                    break;
                case "water":
                    type = CommodityType.Water;
                    break;
                case "wood":
                    type = CommodityType.Wood;
                    break;
                case "hotwater":
                    type = CommodityType.HotWater;
                    break;
                case "otherwater":
                    type = CommodityType.OtherWater;
                    break;
                case "coal":
                    type = CommodityType.Coal;
                    break;
                default:
                    type = CommodityType.Unspecified;
                    break;
            }

            return type;
        }

        /// <summary>
        /// ConvertUnitOfMeasureName to UnitOfMeasureType.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private static UnitOfMeasureType ConvertUnitOfMeasureName(string name)
        {
            UnitOfMeasureType type;

            switch (name.ToLower())
            {
                case "kwh":
                    type = UnitOfMeasureType.kWh;
                    break;
                case "therms":
                case "therm":
                    type = UnitOfMeasureType.Therms;
                    break;
                case "ccf":
                    type = UnitOfMeasureType.CCF;
                    break;
                case "mcf":
                    type = UnitOfMeasureType.MCF;
                    break;
                case "hcf":
                    type = UnitOfMeasureType.HCF;
                    break;
                case "gallons":
                case "gal":
                    type = UnitOfMeasureType.Gal;
                    break;
                case "btu":
                case "btus":
                    type = UnitOfMeasureType.BTU;
                    break;
                default:
                    type = UnitOfMeasureType.Unspecified;
                    break;
            }

            return type;
        }

        /// <summary>
        /// ConvertEvaluationTypeName to EvaluationType.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private static EvaluationType ConvertEvaluationTypeName(string name)
        {
            EvaluationType type;

            switch (name.ToLower())
            {
                case "pending":
                    type = EvaluationType.Pending;
                    break;
                case "completed":
                    type = EvaluationType.Completed;
                    break;
                case "provided":
                    type = EvaluationType.Provided;
                    break;
                case "incomplete":
                    type = EvaluationType.Incomplete;
                    break;
                case "failed":
                    type = EvaluationType.Failed;
                    break;
                default:
                    type = EvaluationType.Failed;
                    break;
            }

            return type;
        }



        /// <summary>
        /// Map to List of EMAppliance.
        /// </summary>
        /// <param name="applianceColl"></param>
        /// <returns></returns>
        private List<EMAppliance> MapAppliances(ApplianceCollection applianceColl)
        {
            if (applianceColl == null || applianceColl.Count <= 0)
            {
                return null;
            }

            var appliances = new List<EMAppliance>();

            foreach (var a in applianceColl)
            {
                var appliance = new EMAppliance()
                {
                    Name = a.Name,
                    Confidence = a.Confidence
                };

                if (a.EnergyUses != null)
                {
                    appliance.EnergyUses = new List<EMEnergyUse>();

                    foreach (var e in a.EnergyUses)
                    {
                        var energyuse = new EMEnergyUse
                        {
                            Quantity = e.Quantity,
                            Cost = e.Cost,
                            CommodityName = ConvertCommodity(e.Commodity),
                            UnitOfMeasureName = ConvertUnitOfMeasure(e.UnitOfMeasure),
                            RecRatio = e.RecRatio,
                            WeatherAdjustmentFactor = e.WeatherAdjustmentFactor
                        };

                        if (e.EvaluationContext != null)
                        {
                            var ctx = new EMEvaluationContext
                            {
                                Expression = null,
                                ContextName = ConvertEvaluationType(e.EvaluationContext.EvaluationType)
                            };
                            // e.EvaluationContext.Expression; // do not send any expressions back out to the caller!
                            energyuse.EvaluationContext = ctx;
                        }

                        if (e.Periods != null)
                        {
                            energyuse.Periods = new List<EMPeriod>();

                            foreach (var p in e.Periods)
                            {
                                var per = new EMPeriod
                                {
                                    Number = p.Number,
                                    Name = p.Name,
                                    Quantity = p.Quantity
                                };

                                if (per.Start.HasValue)
                                {
                                    p.Start = per.Start;
                                }

                                if (per.End.HasValue)
                                {
                                    p.End = per.End;
                                }

                                energyuse.Periods.Add(per);
                            }

                        }

                        appliance.EnergyUses.Add(energyuse);
                    }
                }

                appliances.Add(appliance);
            }

            return appliances;
        }

        /// <summary>
        /// ConvertCommodity type to string
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private static string ConvertCommodity(CommodityType type)
        {
            string name;

            switch (type)
            {
                case CommodityType.Electric:
                    name = "Electric";
                    break;
                case CommodityType.Gas:
                    name = "Gas";
                    break;
                case CommodityType.Oil:
                    name = "Oil";
                    break;
                case CommodityType.Propane:
                    name = "Propane";
                    break;
                case CommodityType.Water:
                    name = "Water";
                    break;
                case CommodityType.Wood:
                    name = "Wood";
                    break;
                case CommodityType.HotWater:
                    name = "HotWater";
                    break;
                case CommodityType.OtherWater:
                    name = "OtherWater";
                    break;
                case CommodityType.Coal:
                    name = "Coal";
                    break;
                case CommodityType.Unspecified:
                    name = "Unspecified";
                    break;
                default:
                    name = "Unspecified";
                    break;
            }

            return (name);
        }

        /// <summary>
        /// ConvertUnitOfMeasure type to string.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private static string ConvertUnitOfMeasure(UnitOfMeasureType type)
        {
            string name;

            switch (type)
            {
                case UnitOfMeasureType.kWh:
                    name = "kWh";
                    break;
                case UnitOfMeasureType.Therms:
                    name = "Therms";
                    break;
                case UnitOfMeasureType.CCF:
                    name = "CCF";
                    break;
                case UnitOfMeasureType.MCF:
                    name = "MCF";
                    break;
                case UnitOfMeasureType.HCF:
                    name = "HCF";
                    break;
                case UnitOfMeasureType.Gal:
                    name = "Gallons";
                    break;
                case UnitOfMeasureType.BTU:
                    name = "BTU";
                    break;
                default:
                    name = "Unspecified";
                    break;
            }

            return name;
        }

        /// <summary>
        /// ConvertEvaluationType to string.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private static string ConvertEvaluationType(EvaluationType type)
        {
            string name;

            switch (type)
            {
                case EvaluationType.Pending:
                    name = "Pending";
                    break;
                case EvaluationType.Completed:
                    name = "Completed";
                    break;
                case EvaluationType.Provided:
                    name = "Provided";
                    break;
                case EvaluationType.Incomplete:
                    name = "Incomplete";
                    break;
                case EvaluationType.Failed:
                    name = "Failed";
                    break;
                default:
                    name = "Failed";
                    break;
            }

            return name;
        }





        /// <summary>
        /// Map to ProfileAttributeCollection.
        /// </summary>
        /// <param name="attrs"></param>
        /// <param name="standAlone"></param>
        /// <returns></returns>
        private static ProfileAttributeCollection MapProfileAttributes(List<EMProfileAttribute> attrs, bool? standAlone)
        {
            ProfileAttributeCollection profileAttributes = null;

            if (attrs != null && attrs.Count > 0)
            {
                profileAttributes = new ProfileAttributeCollection();

                if (standAlone != null)
                {
                    profileAttributes.IsStandAlone = standAlone.Value;
                }

                foreach (var p in attrs)
                {
                    var pa = new EnergyModel.ProfileAttribute()
                    {
                        Key = p.Key,
                        OptionKey = p.OptionKey,
                        OptionValue = p.OptionValue,
                        Source = "P"
                    };

                    profileAttributes.Add(pa);
                }
            }

            return profileAttributes;
        }

        /// <summary>
        /// Map to List of EMProfileAttribute.
        /// </summary>
        /// <param name="attributeColl"></param>
        /// <returns></returns>
        private static List<EMProfileAttribute> MapProfileAttributes(ProfileAttributeCollection attributeColl)
        {
            List<EMProfileAttribute> profileAttributes = null;

            if (attributeColl != null && attributeColl.Count > 0)
            {
                profileAttributes = new List<EMProfileAttribute>();

                foreach (var p in attributeColl)
                {
                    var pa = new EMProfileAttribute()
                    {
                        Key = p.Key,
                        OptionKey = p.OptionKey,
                        OptionValue = p.OptionValue,
                        Kind = p.Source
                    };

                    profileAttributes.Add(pa);
                }
            }

            return profileAttributes;
        }




        /// <summary>
        /// Map to EndUses.
        /// </summary>
        /// <param name="eus"></param>
        /// <param name="standAlone"></param>
        /// <returns></returns>
        private EndUseCollection MapEndUses(IReadOnlyCollection<EMEndUse> eus, bool? standAlone)
        {
            EndUseCollection endUses = null;

            if (eus != null && eus.Count > 0)
            {
                endUses = new EndUseCollection();

                if (standAlone != null)
                {
                    endUses.IsStandAlone = standAlone.Value;
                }

                foreach (var e in eus)
                {
                    var eu = new EnergyModel.EndUse
                    {
                        Name = e.Name,
                        Category = e.Category
                    };

                    if (e.ApplianceNames != null && e.ApplianceNames.Count > 0)
                    {
                        eu.ApplianceNames = new List<string>();

                        foreach (var i in e.ApplianceNames)
                        {
                            eu.ApplianceNames.Add(i);
                        }
                    }

                    endUses.Add(eu);
                }
            }

            return (endUses);
        }

        /// <summary>
        /// Map to List of EMEndUse.
        /// </summary>
        /// <param name="enduseColl"></param>
        /// <returns></returns>
        private List<EMEndUse> MapEndUses(EndUseCollection enduseColl)
        {
            List<EMEndUse> endUses = null;

            if (enduseColl != null && enduseColl.Count > 0)
            {
                endUses = new List<EMEndUse>();

                foreach (var e in enduseColl)
                {
                    var eu = new EMEndUse
                    {
                        Name = e.Name,
                        Category = e.Category,
                        Confidence = e.Confidence
                    };

                    if (e.ApplianceNames != null && e.ApplianceNames.Count > 0)
                    {
                        eu.ApplianceNames = new List<string>();

                        foreach (var i in e.ApplianceNames)
                        {
                            eu.ApplianceNames.Add(i);
                        }
                    }

                    // now map energy use to the output
                    if (e.EnergyUses != null)
                    {
                        eu.EnergyUses = new List<EMEnergyUse>();

                        foreach (var ee in e.EnergyUses)
                        {
                            var energyuse = new EMEnergyUse
                            {
                                Quantity = ee.Quantity,
                                Cost = ee.Cost,
                                CommodityName = ConvertCommodity(ee.Commodity),
                                UnitOfMeasureName = ConvertUnitOfMeasure(ee.UnitOfMeasure),
                                RecRatio = 1.0m,
                                WeatherAdjustmentFactor = 1.0m
                            };

                            // if there is period energy use (usually monthly if present)
                            if (ee.Periods != null)
                            {
                                energyuse.Periods = new List<EMPeriod>();

                                foreach (var p in ee.Periods)
                                {
                                    var per = new EMPeriod
                                    {
                                        Number = p.Number,
                                        Name = p.Name,
                                        Quantity = p.Quantity
                                    };

                                    if (per.Start.HasValue)
                                    {
                                        p.Start = per.Start;
                                    }
                                    if (per.End.HasValue)
                                    {
                                        p.End = per.End;
                                    }

                                    energyuse.Periods.Add(per);
                                }

                            }

                            eu.EnergyUses.Add(energyuse);
                        }
                    }

                    endUses.Add(eu);
                }
            }

            return endUses;
        }




        /// <summary>
        /// Map to RegionParameters.
        /// </summary>
        /// <param name="parms"></param>
        /// <returns></returns>
        private RegionParams MapRegionParameters(EMRegionParameters parms)
        {
            RegionParams regionParameters = null;

            if (parms != null)
            {
                regionParameters = new RegionParams
                {
                    TemperatureRegionId = parms.TemperatureRegionId,
                    SolarRegionId = parms.SolarRegionId,
                    ApplianceRegionId = parms.ApplianceRegionId,
                    ClimateId = parms.ClimateId
                };
                if (!string.IsNullOrEmpty(parms.State)) { regionParameters.State = parms.State; }
            }

            return regionParameters;
        }

        /// <summary>
        /// Map to EMRegionParameters.
        /// </summary>
        /// <param name="parms"></param>
        /// <returns></returns>
        private EMRegionParameters MapRegionParameters(RegionParams parms)
        {
            EMRegionParameters regionParameters = null;

            if (parms != null)
            {
                regionParameters = new EMRegionParameters
                {
                    TemperatureRegionId = parms.TemperatureRegionId,
                    SolarRegionId = parms.SolarRegionId,
                    ApplianceRegionId = parms.ApplianceRegionId,
                    ClimateId = parms.ClimateId
                };
                if (!string.IsNullOrEmpty(parms.State)) { regionParameters.State = parms.State; }
            }

            return regionParameters;
        }



        /// <summary>
        /// Map to IDParams.
        /// </summary>
        /// <param name="parms"></param>
        /// <returns></returns>
        private IDParams MapIDParameters(EMCustomerParameters parms)
        {
            IDParams idParameters = null;

            if (parms != null)
            {
                idParameters = new IDParams();
                if (!string.IsNullOrEmpty(parms.CustomerId)) { idParameters.CustomerId = parms.CustomerId; }
                if (!string.IsNullOrEmpty(parms.AccountId)) { idParameters.AccountId = parms.AccountId; }
                if (!string.IsNullOrEmpty(parms.PremiseId)) { idParameters.PremiseId = parms.PremiseId; }
                if (!string.IsNullOrEmpty(parms.Zipcode)) { idParameters.Zipcode = parms.Zipcode; }
            }

            return idParameters;
        }

        /// <summary>
        /// Map to EMCustomerParameters.
        /// </summary>
        /// <param name="parms"></param>
        /// <returns></returns>
        private EMCustomerParameters MapIDParameters(IDParams parms)
        {
            EMCustomerParameters idParameters = null;

            if (parms != null)
            {
                idParameters = new EMCustomerParameters();
                if (!string.IsNullOrEmpty(parms.CustomerId)) { idParameters.CustomerId = parms.CustomerId; }
                if (!string.IsNullOrEmpty(parms.AccountId)) { idParameters.AccountId = parms.AccountId; }
                if (!string.IsNullOrEmpty(parms.PremiseId)) { idParameters.PremiseId = parms.PremiseId; }
                if (!string.IsNullOrEmpty(parms.Zipcode)) { idParameters.Zipcode = parms.Zipcode; }
            }

            return idParameters;
        }



        /// <summary>
        /// Map Extra Parameters.
        /// </summary>
        /// <param name="outputMonthlyEnergyUse"></param>
        /// <param name="outputTraceDetails"></param>
        /// <param name="chunkSize"></param>
        /// <param name="executeBillDisagg"></param>/// 
        /// <returns></returns>
        private ExtraParams MapExtraParameters(bool? outputMonthlyEnergyUse, bool? outputTraceDetails, int? chunkSize, bool? executeBillDisagg)
        {
            var extraParams = new ExtraParams();

            if (outputMonthlyEnergyUse.HasValue)
            {
                extraParams.OutputMonthlyEnergyUse = outputMonthlyEnergyUse.Value;
            }

            if (outputTraceDetails.HasValue)
            {
                extraParams.OutputTraceDetails = outputTraceDetails.Value;
            }

            if (chunkSize.HasValue)
            {
                extraParams.ChunkSize = chunkSize.Value;
            }

            if (executeBillDisagg.HasValue)
            {
                extraParams.ExecuteBillDisagg = executeBillDisagg.Value;
            }

            return extraParams;
        }


        /// <summary>
        /// Map to list of BillDisagg.Bill.
        /// </summary>
        /// <param name="bills"></param>
        /// <param name="executeBillDisagg"></param>
        /// <returns></returns>
        private List<BillDisagg.Bill> MapBills(List<BDBill> bills, bool? executeBillDisagg)
        {
            List<BillDisagg.Bill> bdBills = null;

            if (bills != null && bills.Count > 0)
            {
                if (executeBillDisagg != null)
                {
                    bdBills = new List<BillDisagg.Bill>();

                    foreach (var b in bills)
                    {
                        var bdBill = new BillDisagg.Bill()
                        {
                            StartDate = b.StartDate,
                            EndDate = b.EndDate,
                            CommodityKey = b.CommodityKey,
                            UomKey = b.UomKey,
                            TotalServiceUse = b.Usage,
                            TotalServiceCost = b.Cost,
                            BillDays = b.BillDays
                        };

                        bdBills.Add(bdBill);
                    }
                }

            }

            return bdBills;
        }

        /// <summary>
        /// Map to list of BDBill.
        /// </summary>
        /// <param name="bdBills"></param>
        /// <returns></returns>
        private List<BDBill> MapBills(List<BillDisagg.Bill> bdBills)
        {
            List<BDBill> bills = null;

            if (bdBills != null && bdBills.Count > 0)
            {
                bills = new List<BDBill>();

                foreach (var b in bdBills)
                {
                    var bill = new BDBill
                    {
                        StartDate = b.StartDate,
                        EndDate = b.EndDate,
                        CommodityKey = b.CommodityKey,
                        UomKey = b.UomKey,
                        Usage = b.TotalServiceUse,
                        Cost = b.TotalServiceCost,
                        BillDays = b.BillDays
                    };

                    bills.Add(bill);
                }

            }

            return bills;
        }

        /// <summary>
        /// map to list of BDDisaggStatus
        /// 15.12 TFS 1489
        /// </summary>
        /// <param name="bdDisaggStatus"></param>
        /// <returns></returns>
        private List<BDDisaggStatus> MapDisaggStatuses(List<BillDisagg.DisaggStatus> bdDisaggStatus)
        {
            List<BDDisaggStatus> disaggStatuses = null;

            if (bdDisaggStatus != null && bdDisaggStatus.Count > 0)
            {
                disaggStatuses = bdDisaggStatus.Select(ds => new BDDisaggStatus
                {
                    CommodityKey = ds.Commodity.ToString(),
                    Status = ds.Status.ToString()
                }).ToList();
            }

            return disaggStatuses;
        }

        /// <summary>
        /// Map the ModelType.
        /// </summary>
        /// <param name="modeltype"></param>
        /// <returns></returns>
        private static string MapModelType(ModelType modeltype)
        {
            return modeltype == ModelType.Business ? "Business" : "Residential";
        }

    }
}