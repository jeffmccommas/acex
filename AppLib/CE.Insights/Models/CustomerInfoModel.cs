﻿using CE.ContentModel;
using CE.Models;
using CE.Models.Insights;

namespace CE.Insights.Models
{
    /// <summary>
    /// Insights model
    /// </summary>
    public class CustomerInfoModel
    {
        /// <summary>
        /// Local Content grabber.
        /// </summary>
        /// <param name="response"></param>
        /// <param name="clientUser"></param>
        /// <param name="locale"></param>
        public void ApplyContent(CustomerInfoResponse response, ClientUser clientUser, string locale)
        {
            //content
            var cf = new ContentModelFactoryContentful();
            var cp = cf.CreateContentProvider(clientUser.ClientID);
            var contentLocale = cp.GetContentLocale(locale);

            //get label content and configuration
            response.Content = new Content
            {
                TextContent = cp.GetContentTextContent("customerinfo", contentLocale, string.Empty),
                Configuration = cp.GetContentConfiguration("customerinfo", string.Empty),
                Enumeration = cp.GetContentEnumeration("customerinfo", string.Empty),
                Commodity = cp.GetContentCommodity(string.Empty),
                UOM = cp.GetContentUom(string.Empty)
            };
        }
    }
}