﻿using AutoMapper;
using AO.BusinessContracts;
using Enums = CE.AO.Utilities.Enums;
using CE.ContentModel;
using CE.Insights.Helpers;
using CE.Models;
using CE.Models.Insights;
using CE.Models.Insights.Types;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using CE.AO.Logging;
using CE.AO.Utilities;
using CE.ContentModel.Entities;
using CE.RateModel;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Extensions = AO.Business.Extensions;
using System.Threading.Tasks;
using AO.Entities;
using AO.Business;


namespace CE.Insights.Models
{
    /// <summary>
    /// Ami model
    /// </summary>
    public class ConsumptionModel : ConsumptionBaseModel
    {
        
        private readonly IInsightsEFRepository _insightsEfRepository;

        public ConsumptionModel()
        {
            _insightsEfRepository = new InsightsEfRepository();
        }

        public ConsumptionModel(IUnityContainer unityContainer) : base(unityContainer)
        {
            _insightsEfRepository = new InsightsEfRepository();
        }

        /// <summary>
        /// Testability, passed in repository and unity container so not relying on data access through sql server and table storage
        /// </summary>
        /// <param name="unityContainer"></param>
        /// <param name="insightsEfRepository"></param>
        public ConsumptionModel(IUnityContainer unityContainer, IInsightsEFRepository insightsEfRepository)
        {
            Container = unityContainer;
            if (insightsEfRepository == null)
            {
                throw new ArgumentNullException($"InsightsRepository is null!");
            }
            _insightsEfRepository = insightsEfRepository;
        }

        public ConsumptionResponse GetConsumption(ClientUser clientUser, ConsumptionRequest request) {
            var response = GetConsumptionAsync(clientUser, request);
            return response.Result;
        }

        public async Task<ConsumptionResponse> GetConsumptionAsync(ClientUser clientUser, ConsumptionRequest request)
        {
            bool isAdvAMi = !string.IsNullOrEmpty(request.AdvAmi);
            string miscRespInfo = "";
            double dataGetMs;
            ConsumptionResponse response;
            var pageSize = 500;
            int pageIndex;
            if (request.PageIndex == 0)
                pageIndex = 1;
            else
                pageIndex = request.PageIndex;

            if (request.IncludeWeather)
            {
                if(string.IsNullOrEmpty(request.ZipCode))
                {
                    // return errors
                    response = new ConsumptionResponse();
                    response.Message = "Zip Code is required for weather data";
                    return response;
                }
                else if (string.IsNullOrEmpty(request.Country))
                {
                    // return errors
                    response = new ConsumptionResponse();
                    response.Message = "Country is required for weather data, US/CA";
                    return response;
                }
            }

            if (string.IsNullOrEmpty(request.ResolutionKey))
                request.ResolutionKey = string.Empty;

            var meterList = request.MeterIds.Split(',').ToList();
            string availableResolution;
            int amiUomId;
            int amiCommodityId;

            AutoMapperSetup();
            
            if (request.StartDate.Kind == DateTimeKind.Unspecified)
                request.StartDate = DateTime.SpecifyKind(request.StartDate, DateTimeKind.Utc);

            if (request.EndDate.Kind == DateTimeKind.Unspecified)
                request.EndDate = DateTime.SpecifyKind(request.EndDate, DateTimeKind.Utc);

            request.StartDate = request.StartDate.ToUniversalTime();
            request.EndDate = request.EndDate.ToUniversalTime();

            List<Reading> readings;
            DateTime dtSttRoot = DateTime.Now;

            Task<WeatherReadings> weatherTask = null;
            List<ConsumptionWeather> weatherReadings = null;
            if (request.IncludeWeather) {
                //DateTime dtStt = DateTime.Now;
                weatherTask = GetWeatherData(clientUser.ClientID, request.StartDate, request.EndDate, request.ZipCode, request.Country,
                    request.ResolutionKey);
                ////weatherReadings = GetWeatherData(clientUser.ClientID, request.StartDate, request.EndDate, request.ZipCode, request.Country, request.ResolutionKey);
                //dataGetMs = DateTime.Now.Subtract(dtStt).TotalMilliseconds;
                //miscRespInfo += $"; getconsumption.GetWeatherData {dataGetMs} ms, {weatherReadings?.Count} weatherReadings";
            }

            bool cfgUseAdvAmi = false;
            try {
                var cf = new ContentModelFactoryContentful();
                var cp = cf.CreateContentProvider(clientUser.ClientID);
                //List<ClientConfigurationBulk> bulk = cp.GetContentClientConfigurationBulk("aclaraone.clientsettings", string.Empty);
                List<ClientConfigurationBulk> bulk =
                    cp.GetContentClientConfigurationBulk("system", "aclaraone.clientsettings");
                dataGetMs = DateTime.Now.Subtract(dtSttRoot).TotalMilliseconds;
                miscRespInfo += $"; getconsumption.getConfig {bulk?.Count} entries, {dataGetMs} ms";
                if (bulk?.Count > 1000) {
                    cfgUseAdvAmi = true;  // will never happen but gets rid of warning  (that's for Lee.  :) )
                }
                // todo: assign cfgUseAdvAmi based on new config settings.
            }
            catch (Exception exc) {
                miscRespInfo += $"; GetConsumption: exception in incomplete advami config {exc.Message}; inner= {exc.InnerException?.Message} ";
                cfgUseAdvAmi = false;
            }

            var filterValues = new Dictionary<string, string>();
            filterValues.Add(Constants.CassandraTableNames.AmiFilterByAccountID, request.AccountId);
            filterValues.Add(Constants.CassandraTableNames.AmiFilterByServicePointId, request.ServicePointId);
            DateTime dtStt = DateTime.Now;
            string minResolution = "60";
            //if (!isAdvAMi && !cfgUseAdvAmi) {
                
                
            var readingsTask = GetReadings(clientUser.ClientID, request, filterValues, isAdvAMi);
            var meterReadings = await readingsTask.ConfigureAwait(false);
            readings = meterReadings.Readings;
            amiCommodityId = meterReadings.AmiCommodityId;
            amiUomId = meterReadings.AmiUomId;
            availableResolution = meterReadings.AvailableResolultion;
            dataGetMs = DateTime.Now.Subtract(dtStt).TotalMilliseconds;
            int readingsCount = readings?.Count ?? 0;
            miscRespInfo += $"; GetConsumption.GetReadings Async {dataGetMs} ms, {readingsCount} readings{meterReadings.ExtraInfo}";
            //}
            //else {
            //    readings = new List<Reading>();
                
            //    dataGetMs = DateTime.Now.Subtract(dtStt).TotalMilliseconds;
            //    miscRespInfo += $";new advAmiDataGet GetConsumption.GetAmiData {dataGetMs} ms, {readings.Count} readings ";
            //    availableResolution = minResolution;
            //    amiUomId = -1;
            //    amiCommodityId = -2;
            //}

            if (request.ResolutionKey == string.Empty && availableResolution != string.Empty)
            {
                var resolutionList = availableResolution.Split(',');
                request.ResolutionKey = resolutionList.Any() ? resolutionList[resolutionList.Count() - 1] : availableResolution;
            }

            
            response = new ConsumptionResponse
            {
                ClientId = clientUser.ClientID,
                CustomerId = request.CustomerId,
                AccountId = request.AccountId,
                PremiseId = request.PremiseId,
                MeterIds = request.MeterIds,
                ResolutionKey = request.ResolutionKey,
                CommodityKey = request.CommodityKey,
                PageSize = pageSize,
                PageIndex = pageIndex
            };

            if (request.IncludeResolutions)
                response.ResolutionsAvailable = availableResolution;

            if (request.IncludeWeather && weatherTask != null) {
                var weather = await weatherTask;
                weatherReadings = weather.Weather;
                miscRespInfo += weather.ExtraInfo;
            }

            if ((readings != null && readings.Count > 0) || (weatherReadings != null && weatherReadings.Any()))
            {
                if(readings != null && readings.Count > 0)
                {
                    int skipRows = (pageIndex - 1) * pageSize;
                    response.UOMKey = Map.ConvertUomString(Map.ConvertUnitOfMeasureType(((Enums.UomType)amiUomId).ToString()));
                    if (string.IsNullOrEmpty(response.CommodityKey))
                        response.CommodityKey = ((Enums.CommodityType)amiCommodityId).ToString().ToLower();

                    dtStt = DateTime.Now;
                    var consumptionList = ConvertRateModelReadingsToConsumptionData(readings, skipRows, pageSize, request.Count);
                    dataGetMs = DateTime.Now.Subtract(dtStt).TotalMilliseconds;
                    miscRespInfo += $"; getconsumption.ConvertRateModelReadingsToConsumptionData {dataGetMs} ms, {readings?.Count} readinigs";

                    // average usage
                    if (request.IncludeAverageUsage)
                    {
                        var averageUsage = consumptionList.Average(x => x.Value);
                        response.AverageUsage = averageUsage;
                    }

                    response.Data = consumptionList;

                }

                
                if(weatherReadings != null && weatherReadings.Any())
                {
                    response.Weather = weatherReadings;
                }
            }
            else
            { // return errors
                response = new ConsumptionResponse();
                response.Message = $"No data found";
                //response.MiscInfo = miscRespInfo;
                if (request.IncludeResolutions)
                    response.ResolutionsAvailable = availableResolution;
            }

            response.MiscInfo = miscRespInfo;

            return response;

        }

        public Task<AmiReadings> GetReadings(int clientId, string accountId, List<string> meterIds,
            DateTime startDate, DateTime endDate, string requestedResolution)
        {
            var filterValues = new Dictionary<string,string>();
            filterValues.Add(Constants.CassandraTableNames.AmiFilterByAccountID, accountId);
            return GetReadings(clientId, meterIds, startDate, endDate, requestedResolution, filterValues);
        }

        public Task<AmiReadings> GetReadings(int clientId,ConsumptionRequest request, IDictionary<string, string> filterValues, Boolean isAdvAMIRead)
        {
            return GetReadingsAsync(clientId, request,filterValues,isAdvAMIRead);
        }

        public async Task<AmiReadings> GetReadings(int clientId, List<string> meterIds, DateTime startDate, DateTime endDate, string requestedResolution, IDictionary<string,string> filterValues)
        {
            List<Reading> readings = null;
            var extraInfo = string.Empty;
            var amiUomId = 99;
            var amiCommodityId = 0;
            var readingsList = new List<List<Reading>>();
            int previouisInterval = 0;
            var mixedInterval = false;
            var resolutionList = new List<string>();
            var availableResolution = string.Empty;
            var commaSeperator = ",";
            //var previousEndDate = DateTime.MinValue;

            var localStartDate = startDate;
            var localEndDate = endDate;

            var tasks = new List<Task<AmiReadings>>();
            AdvAmiRequest amiReq = new AdvAmiRequest();
            foreach (var meterId in meterIds)
            {
                var utcStartDate = startDate;
                var utcEndDate = endDate;

                
                tasks.Add(GetAmiReadings(clientId, meterId.Trim(), utcStartDate, utcEndDate, requestedResolution, filterValues));
                
            }

            await Task.WhenAll(tasks).ConfigureAwait(false);

            foreach (var task in tasks) {
                var amiReadings = task.Result;
                var intervalType = amiReadings.IntervalType;
                amiUomId = amiReadings.AmiUomId;
                amiCommodityId = amiReadings.AmiCommodityId;
                extraInfo += amiReadings.ExtraInfo;

                if (amiReadings.StartDate.Kind != DateTimeKind.Utc)
                    localStartDate = amiReadings.StartDate;
                if (amiReadings.EndDate.Kind != DateTimeKind.Utc)
                    localEndDate = amiReadings.EndDate;

                if (amiReadings.Readings != null && amiReadings.Readings.Any()) {
                    readingsList.Add(amiReadings.Readings);

                    if (previouisInterval == 0) {
                        previouisInterval = intervalType;
                    }

                    if (previouisInterval != intervalType) {
                        mixedInterval = true;
                    }
                }
            }

            if (meterIds.Count > 1)
            {
                if (readingsList.Any())
                {

                    switch (requestedResolution.ToLower())
                    {
                        case "day":
                            readings = AggregatedDailyReadings(readingsList, localStartDate.Date, localEndDate.Date.AddMinutes(-1));
                            break;
                        case "month":
                                readings = AggregatedMonthlyReadings(readingsList, localStartDate.Date, localEndDate.Date.AddMinutes(-1));
                            break;
                        default:
                            if (!mixedInterval)
                            {
                                readings = AggregatedIntervalReadings(readingsList, previouisInterval, localStartDate, localEndDate.AddMinutes(-1));
                            }
                            break;

                    }
                }

            }
            else
            {
                if (readingsList.Count > 0)
                {
                    if (readingsList[0].Count > 0)
                    {
                        readings = readingsList[0];
                    }
                }
            }

            switch (previouisInterval)
            {
                case (int)ResolutionType.fifteen:
                case (int)ResolutionType.thirty:
                case (int)ResolutionType.hour:
                    resolutionList.Add(ResolutionType.month.ToString());
                    resolutionList.Add(ResolutionType.day.ToString());
                    if (!mixedInterval)
                        resolutionList.Add(((ResolutionType)previouisInterval).ToString());
                    break;
                case (int)ResolutionType.day:
                    resolutionList.Add(ResolutionType.month.ToString());
                    if (!mixedInterval)
                        resolutionList.Add(ResolutionType.day.ToString());
                    break;
                case (int)ResolutionType.month:
                    if (!mixedInterval)
                        resolutionList.Add(ResolutionType.month.ToString());
                    break;
            }

            foreach (var resolution in resolutionList)
            {
                if (availableResolution == string.Empty)
                    availableResolution = resolution;
                else
                    availableResolution = availableResolution + commaSeperator + resolution;
            }


            var meterReadings = new AmiReadings {
                Readings = readings,
                AmiCommodityId = amiCommodityId,
                AmiUomId = amiUomId,
                ExtraInfo = extraInfo,
                AvailableResolultion = availableResolution
            };

            return meterReadings;
        }

        public async Task<AmiReadings> GetReadingsAsync(int clientId, ConsumptionRequest request, IDictionary<string, string> filterValues, Boolean isAdvAMIRead = false)
        {
            List<Reading> readings = null;
            var extraInfo = string.Empty;
            var amiUomId = 99;
            var amiCommodityId = 0;
            var readingsList = new List<List<Reading>>();
            int previouisInterval = 0;
            var mixedInterval = false;
            var resolutionList = new List<string>();
            var availableResolution = string.Empty;
            var commaSeperator = ",";
            //var previousEndDate = DateTime.MinValue;

            var startDate = request.StartDate.ToUniversalTime();
            var endDate = request.EndDate.ToUniversalTime();

            var tasks = new List<Task<AmiReadings>>();
            AdvAmiRequest amiReq = new AdvAmiRequest();
            ProcessingArgs pa = new ProcessingArgs();
            ContextData context = new ContextData();
            string miscRespInfo = string.Empty;
            string minResolution = "60";
            string[] collectionIds = new string[0];
            switch (pa.GetClientControlVal(clientId.ToString(), Constants.CassandraTableNames.AmiClientSpecificFilter,
                Constants.CassandraTableNames.AmiFilterByMeterId))
            {
                case Constants.CassandraTableNames.AmiFilterByMeterId:
                    collectionIds = request.MeterIds.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    break;
                case Constants.CassandraTableNames.AmiFilterByServicePointId:
                    collectionIds = request.ServicePointId.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    break;
                default:
                    collectionIds = request.MeterIds.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    break;
            }
            foreach (var collectionId in collectionIds)
            {
                var utcStartDate = request.StartDate;
                var utcEndDate = request.EndDate;

                if (!isAdvAMIRead)
                    tasks.Add(GetAmiReadings(clientId, collectionId.Trim(), utcStartDate, utcEndDate, request.ResolutionKey, filterValues));
                else
                {
                    tasks.Add(amiReq.GetAmiData(clientId,collectionId, request,miscRespInfo,minResolution));
                }

            }

            await Task.WhenAll(tasks).ConfigureAwait(false);

            foreach (var task in tasks)
            {
                var amiReadings = task.Result;
                var intervalType = amiReadings.IntervalType;
                amiUomId = amiReadings.AmiUomId;
                amiCommodityId = amiReadings.AmiCommodityId;
                extraInfo += amiReadings.ExtraInfo;

                if (amiReadings.StartDate.Kind != DateTimeKind.Utc)
                    startDate = amiReadings.StartDate;
                if (amiReadings.EndDate.Kind != DateTimeKind.Utc)
                    endDate = amiReadings.EndDate;

                if (amiReadings.Readings != null && amiReadings.Readings.Any())
                {
                    readingsList.Add(amiReadings.Readings);

                    if (previouisInterval == 0)
                    {
                        previouisInterval = intervalType;
                    }

                    if (previouisInterval != intervalType)
                    {
                        mixedInterval = true;
                    }
                }
            }

            if (collectionIds.Length > 1)
            {
                if (readingsList.Any())
                {

                    switch (request.ResolutionKey.ToLower())
                    {
                        case "day":
                            readings = AggregatedDailyReadings(readingsList, startDate.Date, endDate.Date.AddMinutes(-1));
                            break;
                        case "month":
                            readings = AggregatedMonthlyReadings(readingsList, startDate.Date, endDate.Date.AddMinutes(-1));
                            break;
                        default:
                            if (!mixedInterval)
                            {
                                readings = AggregatedIntervalReadings(readingsList, previouisInterval, startDate, endDate.AddMinutes(-1));
                            }
                            break;

                    }
                }

            }
            else
            {
                if (readingsList.Count > 0)
                {
                    if (readingsList[0].Count > 0)
                    {
                        readings = readingsList[0];
                    }
                }
            }

            switch (previouisInterval)
            {
                case (int)ResolutionType.fifteen:
                case (int)ResolutionType.thirty:
                case (int)ResolutionType.hour:
                    resolutionList.Add(ResolutionType.month.ToString());
                    resolutionList.Add(ResolutionType.day.ToString());
                    if (!mixedInterval)
                        resolutionList.Add(((ResolutionType)previouisInterval).ToString());
                    break;
                case (int)ResolutionType.day:
                    resolutionList.Add(ResolutionType.month.ToString());
                    if (!mixedInterval)
                        resolutionList.Add(ResolutionType.day.ToString());
                    break;
                case (int)ResolutionType.month:
                    if (!mixedInterval)
                        resolutionList.Add(ResolutionType.month.ToString());
                    break;
            }

            foreach (var resolution in resolutionList)
            {
                if (availableResolution == string.Empty)
                    availableResolution = resolution;
                else
                    availableResolution = availableResolution + commaSeperator + resolution;
            }


            var meterReadings = new AmiReadings
            {
                Readings = readings,
                AmiCommodityId = amiCommodityId,
                AmiUomId = amiUomId,
                ExtraInfo = extraInfo,
                AvailableResolultion = availableResolution
            };

            return meterReadings;
        }


        public async Task<AmiReadings> GetAmiReadings(int clientId, string meterId, DateTime startDate, DateTime endDate, string requestedResolution, IDictionary<string, string> filterValues)
        {
            ITallAMI amiManager = Container.Resolve<ITallAMI>();
            var extraInfo = string.Empty;
            var amiUomId = 99;
            var amiCommodityId = 0;
            var intervalType = 0;
            List<Reading> readings = null;

            if (requestedResolution.ToLower(CultureInfo.InvariantCulture) == "month")
            {
                startDate = new DateTime(startDate.Year, startDate.Month, 1, startDate.Hour, startDate.Minute, startDate.Second, DateTimeKind.Utc);
                if (endDate.Month != DateTime.Today.Month && endDate.Year != DateTime.Today.Year)
                {
                    endDate = new DateTime(endDate.AddMonths(1).Year, endDate.AddMonths(1).Month, 1, endDate.Hour, endDate.Minute, endDate.Second, DateTimeKind.Utc).AddDays(-1);
                }
            }
            DateTime localStartDate = startDate, localEndDate = endDate;
            DateTime dtStt = DateTime.Now;
            var tallAmiModels = await Task.Factory.StartNew(() => amiManager.GetAmiList(startDate, endDate, meterId, clientId, filterValues)?.ToList()).ConfigureAwait(false); 
            double dataGetMs = DateTime.Now.Subtract(dtStt).TotalMilliseconds;
            extraInfo += $"; GetAmiReadings Async.GetAmiList Async for meter {meterId} {dataGetMs} ms, {tallAmiModels?.Count} readings";
            
            if (tallAmiModels != null && tallAmiModels.Any())
            {
                var tallAmiModel = tallAmiModels.First();
                intervalType = tallAmiModel.IntervalType;
                amiUomId = tallAmiModel.UOMId;
                amiCommodityId = tallAmiModel.CommodityId;
                var timezone = tallAmiModels[0].Timezone;

                var timezoneEnum = (Enums.Timezone)Enum.Parse(typeof(Enums.Timezone), timezone.ToUpper(CultureInfo.InvariantCulture));
                var timezoneId = TimeZoneInfo.FindSystemTimeZoneById(timezoneEnum.GetDescriptionOfEnum());

                localStartDate = TimezoneHelper.ConvertToTimeZone(startDate, TimeZoneInfo.Utc, timezoneId);
                localEndDate = TimezoneHelper.ConvertToTimeZone(endDate, TimeZoneInfo.Utc, timezoneId);

                switch (requestedResolution.ToLower(CultureInfo.InvariantCulture))
                {
                    case "day":
                        dtStt = DateTime.Now;
                        readings = RollUpToDailyReadings(tallAmiModels, intervalType);
                        dataGetMs = DateTime.Now.Subtract(dtStt).TotalMilliseconds;
                        extraInfo += $"; GetAmiReadings Async.RollUpToDailyReadings meter {meterId} {dataGetMs} ms, {readings?.Count} readings";
                        break;

                    case "month":
                        dtStt = DateTime.Now;
                        readings = RollUpToMonthlyReadings(tallAmiModels, intervalType);
                        dataGetMs = DateTime.Now.Subtract(dtStt).TotalMilliseconds;
                        extraInfo += $"; GetAmiReadings Async.RollUpToMonthlyReadings meter {meterId} {dataGetMs} ms, {readings?.Count} readings";
                        break;

                    default:
                        bool getReading = (intervalType == 15 && requestedResolution == "fifteen") ||
                            (intervalType == 30 && requestedResolution == "thirty") ||
                            (intervalType == 60 && requestedResolution == "hour") || 
                            (requestedResolution == string.Empty);

                        if (getReading)
                        {
                            int noOfDays;
                            dtStt = DateTime.Now;
                            readings = amiManager.GetReadings(tallAmiModels, out noOfDays);
                            dataGetMs = DateTime.Now.Subtract(dtStt).TotalMilliseconds;
                            extraInfo += $"; GetAmiReadings Async.GetReadings meter {meterId} {dataGetMs} ms, {readings?.Count} readings";
                        }
                        break;
                }
            }

            var amiReadings = new AmiReadings {
                Readings = readings,
                AmiCommodityId =  amiCommodityId,
                AmiUomId = amiUomId,
                IntervalType = intervalType,
                ExtraInfo = extraInfo,
                EndDate = localEndDate,
                StartDate = localStartDate
            };

            return amiReadings;
        }

        /// <summary>
        /// To get the list of daily Reading model
        /// Timestamp for the reading will be in local time
        /// </summary>
        /// <param name="amiList">List of ami from which reading is to be fetched</param>
        /// <param name="intervalType"></param>
        /// <returns>List of Reading model</returns>
        private List<Reading> RollUpToDailyReadings(List<AO.Models.TallAmiModel> amiList, int intervalType)
        {
            ITallAMI amiManager = Container.Resolve<ITallAMI>();
            List<Reading> readings;
            // check if the ami is daily reading model
            // if it is, call GetReadings to convert it to daily ratemodel reading instead of summing all the interval readings up into daily
            if (intervalType == 24)
            {
                int noOfDays;
                readings = amiManager.GetReadings(amiList, out noOfDays);
            }
            else
            {
                readings = new List<Reading>();

                if (!amiList.Any()) return readings;
                var sortedAmiList = amiList.OrderByDescending(e => e.AmiTimeStamp).ToList();

                // Convert UTC timestamp into local timestamp
                ConvertToLocalTime(sortedAmiList);

                // NOTE:  This behavior needs to ultimately use the pre-aggregated consumption values that MDM is introducing.  Since that is not ready
                // yet for this migration, we will need to manually loop through interval readings and aggregate all the values for each day.  

                DateTime aggregatedDateTime = sortedAmiList[0].AmiTimeStamp; // Initialize with first ami reading in list.
                double aggregatedConsumption = 0;

                foreach (var amiRow in sortedAmiList)
                {
                    DateTime currentRowDateTime = amiRow.AmiTimeStamp;
                    double? currentRowConsumption = amiRow.Consumption;

                    if (currentRowDateTime.Date == aggregatedDateTime.Date) // We're in the same day, so we need to aggregate.
                    {
                        aggregatedConsumption += currentRowConsumption ?? 0;
                    }
                    else // We are in a new day.
                    {
                        //We must have hit a new day.  Add the last day to list.
                        readings.Add(new Reading(aggregatedDateTime.Date, aggregatedConsumption));

                        // Start the new days aggregation.
                        aggregatedConsumption = currentRowConsumption ?? 0;
                        aggregatedDateTime = currentRowDateTime;
                    }
                }

                // We'll always have one aggregated value left to add when exiting loop.
                readings.Add(new Reading(aggregatedDateTime.Date, aggregatedConsumption));
            }

            return readings;
        }

        /// <summary>
        /// To get the list of monthly Reading model.  Code copied from AMI.cs migrated by Saviant which is now obsolete.
        /// Timestamp in reading will be in local time
        /// </summary>
        /// <param name="amiList">List of ami from which reading is to be fetched</param>
        /// <param name="intervalType"></param>
        /// <returns>List of Reading model</returns>
        private List<Reading> RollUpToMonthlyReadings(List<AO.Models.TallAmiModel> amiList, int intervalType)
        {
            var readings = new List<Reading>();

            var timeStamp = DateTime.MinValue;
            var noTou = (double)0;
            var onPeak = (double)0;
            var offPeak = (double)0;
            var criticalPeak = (double)0;
            var shoulder1 = (double)0;
            var shoulder2 = (double)0;
            bool hasOnPeak = false, hasOffPeak = false, hasCriticalPeak = false, hasShoulder1 = false, hasShoulder2 = false;

            // Convert UTC timestamp into local timestamp
            ConvertToLocalTime(amiList);

            foreach (var amiRow in amiList.OrderByDescending(a => a.AmiTimeStamp))
            {

                if (timeStamp == DateTime.MinValue)
                {
                    timeStamp = new DateTime(amiRow.AmiTimeStamp.Year, amiRow.AmiTimeStamp.Month, 1);
                    noTou = 0;
                    onPeak = 0;
                    offPeak = 0;
                    criticalPeak = 0;
                    shoulder1 = 0;
                    shoulder2 = 0;
                }

                if (amiRow.AmiTimeStamp.Month != timeStamp.Month)
                {
                    var reading = new Reading(timeStamp, noTou);

                    readings.Add(reading);

                    // add monthly tou readings if exists
                    AddTimeOfUseReadings(timeStamp, readings,
                        hasOnPeak, onPeak, hasOffPeak, offPeak, hasShoulder1, shoulder1,
                        hasShoulder2, shoulder2, hasCriticalPeak, criticalPeak);

                    timeStamp = new DateTime(amiRow.AmiTimeStamp.Year, amiRow.AmiTimeStamp.Month, 1);
                    noTou = 0;
                    onPeak = 0;
                    offPeak = 0;
                    criticalPeak = 0;
                    shoulder1 = 0;
                    shoulder2 = 0;
                }

                var propertyList = amiRow.GetType().GetProperties();


                if (intervalType == 24)
                {
                    foreach (var property in propertyList)
                    {
                        if (!property.Name.Contains("TOU_")) continue;

                        var touId = Extensions.GetTouIdFromProperty(property);

                        var value = property.GetValue(amiRow);

                        if (value != null)
                        {
                            var quantity = Convert.ToDouble(value);
                            switch (touId)
                            {
                                case 0:
                                    noTou = noTou + quantity;
                                    break;
                                case 1:
                                    onPeak = onPeak + quantity;
                                    hasOnPeak = true;
                                    break;
                                case 2:
                                    offPeak = offPeak + quantity;
                                    hasOffPeak = true;
                                    break;
                                case 3:
                                    shoulder1 = shoulder1 + quantity;
                                    hasShoulder1 = true;
                                    break;
                                case 4:
                                    shoulder2 = shoulder2 + quantity;
                                    hasShoulder2 = true;
                                    break;
                                case 5:
                                    criticalPeak = criticalPeak + quantity;
                                    hasCriticalPeak = true;
                                    break;
                            }
                        }
                    }
                }
                else // Not a 24 hour interval.
                {
                    noTou += amiRow.Consumption ?? 0;
                }

            }
            // the last monthly reading
            readings.Add(new Reading(timeStamp, noTou));

            // add monthly tou readings if exists
            AddTimeOfUseReadings(timeStamp, readings,
                hasOnPeak, onPeak, hasOffPeak, offPeak, hasShoulder1, shoulder1,
                hasShoulder2, shoulder2, hasCriticalPeak, criticalPeak);

            return readings;
        }

        private List<Reading> AggregatedIntervalReadings(List<List<Reading>> readingsList, int intervalType, DateTime startDate, DateTime endDate)
        {
            var aggregatedIntervalReadings = new List<Reading>();
            var numOfDays = Convert.ToInt32((endDate.Date - startDate.Date).TotalDays + 1);
            int interval;
            switch (intervalType)
            {
                case 15:
                    interval = 96;
                    break;
                case 30:
                    interval = 48;
                    break;
                default:
                    interval = 24;
                    break;
            }

            for (var i = 0; i < numOfDays; i++)
            {
                var date = startDate.AddDays(i);
                for (var j = 0; j < interval; j++)
                {
                    var timestamp = date.AddMinutes(intervalType * j);
                    var total = (double)0;
                    foreach (var readings in readingsList)
                    {
                        var reading = readings.Find(r => r.Timestamp == timestamp);
                        if (reading != null)
                            total = reading.Quantity + total;
                    }
                    aggregatedIntervalReadings.Add(new Reading(timestamp, total));
                }

            }

            return aggregatedIntervalReadings;
        }
        
        private List<Reading> AggregatedDailyReadings(List<List<Reading>> readingsList, DateTime startDate, DateTime endDate)
        {
            var aggregatedDailyReadings = new List<Reading>();
            var numOfDays = Convert.ToInt32((endDate.Date - startDate.Date).TotalDays + 1);

            for (var i = 0; i < numOfDays; i++)
            {
                var date = startDate.AddDays(i);
                var total = (double)0;
                var onPeakTotal = (double) 0;
                var offPeakTotal = (double)0;
                var shoulder1Total = (double)0;
                var shoulder2Total = (double)0;
                var criticalPeakTotal = (double)0;
                bool hasOnPeak = false, hasOffPeak = false, hasShoulder1= false, hasShoulder2 = false, hasCriticalPeak = false;
                foreach (var readings in readingsList)
                {
                    var reading = readings.FindAll(r => r.Timestamp.Date == date.Date).Sum(s => s.Quantity);
                    total = reading + total;
                    for (var j = RateModel.Enums.TimeOfUse.OnPeak; j <= RateModel.Enums.TimeOfUse.CriticalPeak; j++)
                    {
                        if (readings.Exists(r => r.TimeOfUse == j))
                        {
                            var touReadings = readings.FindAll(r => r.Timestamp.Date == date.Date && r.TimeOfUse == j).Sum(s => s.Quantity);
                            switch (j)
                            {
                                case RateModel.Enums.TimeOfUse.OnPeak:
                                    onPeakTotal = touReadings + onPeakTotal;
                                    hasOnPeak = true;
                                    break;
                                case RateModel.Enums.TimeOfUse.OffPeak:
                                    offPeakTotal = touReadings + offPeakTotal;
                                    hasOffPeak = true;
                                    break;
                                case RateModel.Enums.TimeOfUse.CriticalPeak:
                                    criticalPeakTotal = touReadings + criticalPeakTotal;
                                    hasCriticalPeak = true;
                                    break;
                                case RateModel.Enums.TimeOfUse.Shoulder1:
                                    shoulder1Total = touReadings + shoulder1Total;
                                    hasShoulder1 = true;
                                    break;
                                case RateModel.Enums.TimeOfUse.Shoulder2:
                                    shoulder2Total = touReadings + shoulder2Total;
                                    hasShoulder2 = true;
                                    break;
                            }
                        }
                    }
                }
                aggregatedDailyReadings.Add(new Reading(date, total));
                if(hasOnPeak)
                    aggregatedDailyReadings.Add(new Reading(RateModel.Enums.TimeOfUse.OnPeak, date, onPeakTotal));
                if (hasOffPeak)
                    aggregatedDailyReadings.Add(new Reading(RateModel.Enums.TimeOfUse.OffPeak, date, offPeakTotal));
                if (hasShoulder1)
                    aggregatedDailyReadings.Add(new Reading(RateModel.Enums.TimeOfUse.Shoulder1, date, shoulder1Total));
                if (hasShoulder2)
                    aggregatedDailyReadings.Add(new Reading(RateModel.Enums.TimeOfUse.Shoulder2, date, shoulder2Total));
                if (hasCriticalPeak)
                    aggregatedDailyReadings.Add(new Reading(RateModel.Enums.TimeOfUse.CriticalPeak, date, criticalPeakTotal));
            }

            return aggregatedDailyReadings;
        }

        private List<Reading> AggregatedMonthlyReadings(List<List<Reading>> readingsList, DateTime startDate, DateTime endDate)
        {
            var aggregatedMonthlyReadings = new List<Reading>();
            var month = new DateTime(startDate.Year, startDate.Month, 1);
            var endMonth = new DateTime(endDate.Year, endDate.Month, 1);

            while (month <= endMonth)
            {
                var total = (double)0;
                var onPeakTotal = (double)0;
                var offPeakTotal = (double)0;
                var shoulder1Total = (double)0;
                var shoulder2Total = (double)0;
                var criticalPeakTotal = (double)0;
                bool hasOnPeak = false, hasOffPeak = false, hasShoulder1 = false, hasShoulder2 = false, hasCriticalPeak = false;
                foreach (var readings in readingsList)
                {
                    var reading = readings.FindAll(r => r.Timestamp == month).Sum(s => s.Quantity);
                    total = reading + total;
                    for (var j = RateModel.Enums.TimeOfUse.OnPeak; j <= RateModel.Enums.TimeOfUse.CriticalPeak; j++)
                    {
                        if (readings.Exists(r => r.TimeOfUse == j))
                        {
                            var touReadings = readings.FindAll(r => r.Timestamp == month && r.TimeOfUse == j).Sum(s => s.Quantity);
                            switch (j)
                            {
                                case RateModel.Enums.TimeOfUse.OnPeak:
                                    onPeakTotal = touReadings + onPeakTotal;
                                    hasOnPeak = true;
                                    break;
                                case RateModel.Enums.TimeOfUse.OffPeak:
                                    offPeakTotal = touReadings + offPeakTotal;
                                    hasOffPeak = true;
                                    break;
                                case RateModel.Enums.TimeOfUse.CriticalPeak:
                                    criticalPeakTotal = touReadings + criticalPeakTotal;
                                    hasCriticalPeak = true;
                                    break;
                                case RateModel.Enums.TimeOfUse.Shoulder1:
                                    shoulder1Total = touReadings + shoulder1Total;
                                    hasShoulder1 = true;
                                    break;
                                case RateModel.Enums.TimeOfUse.Shoulder2:
                                    shoulder2Total = touReadings + shoulder2Total;
                                    hasShoulder2 = true;
                                    break;
                            }
                        }
                    }
                }
                aggregatedMonthlyReadings.Add(new Reading(month, total));
                if (hasOnPeak)
                    aggregatedMonthlyReadings.Add(new Reading(RateModel.Enums.TimeOfUse.OnPeak, month, onPeakTotal));
                if (hasOffPeak)
                    aggregatedMonthlyReadings.Add(new Reading(RateModel.Enums.TimeOfUse.OffPeak, month, offPeakTotal));
                if (hasShoulder1)
                    aggregatedMonthlyReadings.Add(new Reading(RateModel.Enums.TimeOfUse.Shoulder1, month, shoulder1Total));
                if (hasShoulder2)
                    aggregatedMonthlyReadings.Add(new Reading(RateModel.Enums.TimeOfUse.Shoulder2, month, shoulder2Total));
                if (hasCriticalPeak)
                    aggregatedMonthlyReadings.Add(new Reading(RateModel.Enums.TimeOfUse.CriticalPeak, month, criticalPeakTotal));
                month = month.AddMonths(1);
            }

            return aggregatedMonthlyReadings;
        }
        
        private void ConvertToLocalTime(List<AO.Models.TallAmiModel> amiList)
        {
            if (amiList.Any())
            {
                var timezone = amiList[0].Timezone;

                var timezoneEnum = (Enums.Timezone)Enum.Parse(typeof(Enums.Timezone), timezone.ToUpper(CultureInfo.InvariantCulture));
                var timezoneId = TimeZoneInfo.FindSystemTimeZoneById(timezoneEnum.GetDescriptionOfEnum());
                foreach (var ami in amiList)
                {
                    ami.AmiTimeStamp = TimezoneHelper.ConvertToTimeZone(ami.AmiTimeStamp, TimeZoneInfo.Utc, timezoneId);
                }
            }
        }

        private static void AddTimeOfUseReadings(DateTime timeStamp, List<Reading> readings, bool hasOnPeak, double onPeak,
           bool hasOffPeak, double offPeak, bool hasShoulder1, double shoulder1, bool hasShoulder2, double shoulder2,
           bool hasCriticalPeak, double criticalPeak)
        {
            Reading reading;
            if (hasOnPeak)
            {
                reading = new Reading(timeStamp, onPeak);
                reading.TimeOfUse = RateModel.Enums.TimeOfUse.OnPeak;
                readings.Add(reading);
            }
            if (hasOffPeak)
            {
                reading = new Reading(timeStamp, offPeak);
                reading.TimeOfUse = RateModel.Enums.TimeOfUse.OffPeak;
                readings.Add(reading);
            }
            if (hasShoulder1)
            {
                reading = new Reading(timeStamp, shoulder1);
                reading.TimeOfUse = RateModel.Enums.TimeOfUse.Shoulder1;
                readings.Add(reading);
            }
            if (hasShoulder2)
            {
                reading = new Reading(timeStamp, shoulder2);
                reading.TimeOfUse = RateModel.Enums.TimeOfUse.Shoulder2;
                readings.Add(reading);
            }
            if (hasCriticalPeak)
            {
                reading = new Reading(timeStamp, criticalPeak);
                reading.TimeOfUse = RateModel.Enums.TimeOfUse.CriticalPeak;
                readings.Add(reading);
            }
        }

        /// <summary>
        /// Local Content grabber.
        /// </summary>
        /// <param name="response"></param>
        /// <param name="clientUser"></param>
        /// <param name="locale"></param>
        public void ApplyContent(ConsumptionResponse response, ClientUser clientUser, string locale)
        {
            //content
            var cf = new ContentModelFactoryContentful();
            var cp = cf.CreateContentProvider(clientUser.ClientID);
            var contentLocale = cp.GetContentLocale(locale);

            //get label content and configuration
            response.Content = new Content
            {
                TextContent = cp.GetContentTextContent("consumption", contentLocale, string.Empty),
                Configuration = cp.GetContentConfiguration("consumption", string.Empty),
                Enumeration = cp.GetContentEnumeration("consumption", string.Empty),
                Commodity = cp.GetContentCommodity(string.Empty),
                UOM = cp.GetContentUom(string.Empty)
            };
        }

        //private async Task<List<ConsumptionWeather>> GetWeatherDataAsync(int clientId, DateTime startDate, DateTime endDate, string zipCode, string country, string requestedResolution) {
        //    var task = await Task.Factory.StartNew(() => GetWeatherData(clientId, startDate, endDate, zipCode, country, requestedResolution))
        //        .ConfigureAwait(false);
        //    return task;
        //}

        /// <summary>
        /// Get the Weather Data based on the Zip Code for the request date range
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="zipCode"></param>
        /// <param name="country"></param>
        /// <param name="requestedResolution"></param>
        /// <returns></returns>
        public async Task<WeatherReadings> GetWeatherData(int clientId, DateTime startDate, DateTime endDate, string zipCode, string country, string requestedResolution)
        {
            DateTime dtStt = DateTime.Now;
            
            ////weatherReadings = GetWeatherData(clientUser.ClientID, request.StartDate, request.EndDate, request.ZipCode, request.Country, request.ResolutionKey);
            List<ConsumptionWeather> weatherDataList = null;

            if (zipCode.Length > 5 && country.Equals(CountryType.US.GetDescription(), StringComparison.CurrentCultureIgnoreCase))
            {
                zipCode = zipCode.Substring(0, 5);
            }
            else if (zipCode.Length > 6 && country.Equals(CountryType.CA.GetDescription(), StringComparison.CurrentCultureIgnoreCase))
            {
                zipCode = zipCode.Substring(0, 6);
            }
            var weatherRequest = new WeatherRequest
            {
                ZipCode =zipCode,
                StartDate = startDate,
                EndDate = endDate
            };
            var weatherDbList = await Task.Factory.StartNew(() => _insightsEfRepository.GetWeather(clientId, weatherRequest)).ConfigureAwait(false);

            //_insightsEfRepository.GetWeather(clientId, weatherRequest);

            if (weatherDbList != null && weatherDbList.Any())
            {
                weatherDataList = Mapper.Map<List<WeatherDb>, List<ConsumptionWeather>>(weatherDbList);

                if (requestedResolution.ToLower() == "month")
                {
                    weatherDataList = GetMonthlyWeather(weatherDataList.OrderBy(w => w.Date).ToList());

                }
            }

            var dataGetMs = DateTime.Now.Subtract(dtStt).TotalMilliseconds;
            var info = $"; getconsumption.GetWeatherData Async {dataGetMs} ms, {weatherDataList?.Count} weatherReadings";
            var weather = new WeatherReadings {
                Weather = weatherDataList,
                ExtraInfo = info
            };
            return weather;

        }

        private List<ConsumptionWeather> GetMonthlyWeather(List<ConsumptionWeather> weatherDataList)
        {
            var monthlyWeatherDataList = new List<ConsumptionWeather>();
            var timeStamp = DateTime.MinValue;
            var quantity = 0;
            var daysInMonth = 0;
            foreach (var weather in weatherDataList)
            {
                if (timeStamp == DateTime.MinValue)
                {
                    timeStamp = new DateTime(weather.Date.Year, weather.Date.Month, 1);
                    quantity = 0;
                    daysInMonth = 0;
                }

                if (weather.Date.Month != timeStamp.Month)
                {
                    var monthlyWeather = new ConsumptionWeather
                    {
                        Date = timeStamp,
                        AvgTemp =  quantity/daysInMonth
                    };
                    monthlyWeatherDataList.Add(monthlyWeather);
                    timeStamp =  new DateTime(weather.Date.Year, weather.Date.Month, 1);
                    quantity = 0;
                    daysInMonth = 0;
                }
                quantity = quantity + weather.AvgTemp;
                daysInMonth++;
            }

            // add last month weather
            monthlyWeatherDataList.Add(new ConsumptionWeather
            {
                Date = timeStamp,
                AvgTemp = quantity/daysInMonth
            });

            return monthlyWeatherDataList;
        }

        //private UnitOfMeasureType ConvertAmiUomType(int uomId)
        //{
        //    var uomType = UnitOfMeasureType.kWh;
        //    switch (uomId)
        //    {
        //        case 0:
        //            uomType = UnitOfMeasureType.kWh;
        //            break;
        //        case 1:
        //            uomType = UnitOfMeasureType.Therms;
        //            break;
        //        case 2:
        //            uomType = UnitOfMeasureType.CCF;
        //            break;
        //        case 3:
        //            uomType = UnitOfMeasureType.CF;
        //            break;
        //        case 4:
        //            uomType = UnitOfMeasureType.CGal;
        //            break;
        //        case 5:
        //            uomType = UnitOfMeasureType.hgal;
        //            break;
        //        case 6:
        //            uomType = UnitOfMeasureType.Gal;
        //            break;
        //        case 7:
        //            uomType = UnitOfMeasureType.KGAL;
        //            break;
        //        default:
        //            uomType = UnitOfMeasureType.kWh;
        //            break;
        //    }

        //    return uomType;
        //}
    }


    class ContextData {
    }

    class AdvAmiRequest {
        private string defAmiRoot = "http://127.0.0.1:8080";  // localhost with standalone service.
        public const string StandardTimeFormat = "yyyy-MM-ddTHH:mm:ss.fffzz00";


        private string _amiReq =
                "/api/current/ami/{0}?oper=recallseries&aclenv=perfd&dtStt={1}&dtStp={2}&timezone={3}&adjusttimezone={4}&seriesSort=desc&commodity={5}&clientId={6}&wantMetadata=1&wantCassinfo=1"
            ;

 

        //private const string CfgReq = "/api/current/config?custom=nyes&oversecj=2216";

        private void InitWebCall(WebClient client) {
            client.Headers["X-BIO-DiagUser"] = "AclaraDiag";
            client.Headers["Accept"] = "application/json";
            client.Headers["Content-Type"] = "text/plain";
        }

        private string GetResData(int clientId, WebClient client, ConsumptionRequest request, ref string miscRespInfo, string collectionId) {
            string root = defAmiRoot;
            //root = "https://aclaoimportbatchprocesscsvcdev.cloudapp.net:4443";

            //string collId = "011022625";
            string dtStt = "2017-01-01T00:00:00Z";
            string dtStp = "2017-06-25T00:00:00Z";
            string bucketsize = "24";
            string timezone = "EST";
            string adjusttimezone = "false";
            string bucketType = "nobucket";

            string[] meterIds = request.MeterIds.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            //collId = meterIds[0];
            //dtStt = request.StartDate.ToString(StandardTimeFormat);
            dtStt = request.StartDate.ToShortDateString();
            //dtStp = request.EndDate.ToString(StandardTimeFormat);
            dtStp = request.EndDate.Date.ToShortDateString();
            timezone = request.timezone?.ToUpper().Length > 0 ? request.timezone?.ToUpper(): "EST";
            adjusttimezone = request.adjusttimezone?.ToLower().Length > 0 ? request.adjusttimezone?.ToLower() : "true";
            bucketType = !string.IsNullOrEmpty(request.buckettype) ? request.buckettype : bucketType;
            bucketsize = !string.IsNullOrEmpty(request.bucketsize) ? request.bucketsize : bucketsize;
            string configQryParms = ConfigurationManager.AppSettings.Get("AdvAmiQueryParsOpts");
            var commodityId = (int)Enum.Parse(typeof(Enums.CommodityType), request.CommodityKey, true);
            string addlQryPars = "";
            if (!string.IsNullOrEmpty(configQryParms))
            {
                string[] qStgs = configQryParms.Split(';');
                addlQryPars = qStgs.Aggregate((current, next) => current + "&" + next);
                addlQryPars = "&" + addlQryPars;
            }

            string reqUriPars = string.Format(_amiReq, collectionId, dtStt, dtStp, timezone, adjusttimezone, commodityId, clientId, addlQryPars);
            ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;

            if (!string.IsNullOrEmpty(request.advAmiRoot)) {  // maybe override cloud portal
                switch (request.advAmiRoot) {
                    case "dev":  // don't really need this?
                        root = "https://aclaoimportbatchprocesscsvcdev.cloudapp.net:4443";
                        break;
                    default:
                        root = ConfigurationManager.AppSettings.Get(request.advAmiRoot);
                        if (string.IsNullOrEmpty(root)) {  // was not in config -- assume explicit
                            root = request.advAmiRoot;
                        }
                        break;
                }
            }
            else {
                root = ConfigurationManager.AppSettings.Get("AdvAmiPortalURL");
            }
            string cfgReq = $"{root}{reqUriPars}";
            string diags = $"; GetResData to {root}--- {cfgReq}";
            byte[] bytes;
            string res;
            try {
                bytes = client.DownloadData(cfgReq);
                diags += $", result len={bytes.Length}";
                res = Encoding.UTF8.GetString(bytes);
            }
            catch (Exception exc)
            {
                res = $"\"Exception\": \" {exc.Message}, {exc.InnerException?.Message} \"";
                res = "[ { " + res + " }] ";
            }
            miscRespInfo += diags;
            Debug.WriteLine(diags);
            return res;
        }


        public async Task<AmiReadings> GetAmiData(int clientId, string collectionId, ConsumptionRequest request, string miscRespInfo, string minResolution) {
            string jsonString;
            List<Reading> readings = new List<Reading>();
              using (WebClient client = new WebClient()) {
                InitWebCall(client);
                jsonString = await Task.Factory.StartNew(() => GetResData(clientId,client, request, ref miscRespInfo, collectionId)).ConfigureAwait(false);
            }
            //JArray json = JArray.Parse(jsonString);
            try
            {
                JArray json;
                using (JsonReader jsonReader = new JsonTextReader(new StringReader(jsonString)))
                {
                    jsonReader.DateParseHandling = DateParseHandling.DateTimeOffset;
                    json = JArray.Load(jsonReader);
                }
                bool gotInterval = false;
                foreach (JToken jToken in json)
                {
                    if (!gotInterval)
                    {
                        if (jToken["meta"] != null)
                        {
                            JObject jo = (JObject) jToken["meta"];
                            if (jo["interval"] != null)
                            {
                                minResolution = (string) jo["interval"];
                                gotInterval = true;
                            }
                        }
                    }
                    Reading reading =
                        new Reading(DateTime.Parse((string) jToken["ts"], null, DateTimeStyles.AssumeUniversal),
                            (double) jToken["val"]);
                    readings.Add(reading);
                }
                
            }
            catch (Exception exc)
            {
                string someData = (jsonString.Length < 120) ? jsonString : jsonString.Substring(0, 120);
                miscRespInfo +=
                    $";GetAmiData json to Reading list exception = {exc.Message}, inner={exc.InnerException?.Message}, someedata={someData} ";
            }
            var amiReadings = new AmiReadings
            {
                Readings = readings,
                AmiCommodityId =(int) Enum.Parse(typeof(Enums.CommodityType),request.CommodityKey,true),
                //IntervalType = minResolution,
                //ExtraInfo = extraInfo,
                EndDate = request.StartDate,
                StartDate = request.EndDate,
                ExtraInfo = miscRespInfo
            };

            return amiReadings;
        }

        //public void GetAmiData(List<Reading> readings, ConsumptionRequest request, ref string miscRespInfo, ref string minResolution) {
        //    AdvAmiRequest amiReq = new AdvAmiRequest();
        //    ProcessingArgs pa = new ProcessingArgs();
        //    ContextData context = new ContextData();
        //    amiReq.GetAmiData(readings, pa, context, request, ref miscRespInfo, ref minResolution);
        //}

    }

    public class AmiReadings {
        public int IntervalType { get; set; }
        public int AmiUomId { get; set; }
        public int AmiCommodityId { get; set; }
        public string ExtraInfo { get; set; }
        public List<Reading>  Readings { get; set; }
        public string AvailableResolultion { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime StartDate { get; set; }
    }

    public class WeatherReadings {
        public List<ConsumptionWeather> Weather { get; set; }
        public string ExtraInfo { get; set; }
    }
}
