﻿using CE.Models;
using CE.Models.Insights;
using CE.Models.Insights.GreenButtonConnect;
using CE.Models.Insights.Types;
using Microsoft.Practices.Unity;
using System.Collections.Generic;
using System.Linq;

namespace CE.Insights.Models
{
    public class GreenButtonReadingTypeModel : GreenButtonBaseModel
    {
        /// <summary>
        /// constructor for reading type model
        /// </summary>
        public GreenButtonReadingTypeModel(string accessToken, string locale, string greenButtonDomain, IUnityContainer container, IInsightsEFRepository insightsEfRepository) : base(accessToken, locale, greenButtonDomain, container, insightsEfRepository)
        {
        }

        /// <summary>
        /// Testability, passed in repository and unity container so not relying on data access through sql server and table storage
        /// </summary>
        /// <param name="greenButtonDomain"></param>
        /// <param name="authorizations"></param>
        /// <param name="greenButtonSettings"></param>
        /// <param name="unityContainer"></param>
        /// <param name="insightsEfRepository"></param>
        /// <param name="clientId"></param>
        public GreenButtonReadingTypeModel(int clientId, string greenButtonDomain, List<GreenButtonAuthorization> authorizations, GreenButtonConfig greenButtonSettings, IUnityContainer unityContainer, IInsightsEFRepository insightsEfRepository): base(clientId, greenButtonDomain, authorizations, unityContainer,insightsEfRepository, greenButtonSettings)
        {
        }

        public GreenButtonResult GetAllReadingTypesGreenButton(string accessToken)
        {
            if(Authorizations == null || Authorizations.FindAll(a => a.AccessToken == accessToken).Count == 0)
                return new GreenButtonResult { Status = GreenButtonErrorType.InvalidAuthorization };

            //check accessToken type
           var accessTokenType = Authorizations.Find(a => a.AccessToken == accessToken).AccessTokenType;

            if (accessTokenType == GreenButtonAccessTokenType.Customer)
                Authorizations = Authorizations.FindAll(a => a.AccessToken == accessToken);

            GreenButtonSubscription finalizedSubscription = null;
            foreach (var authorization in Authorizations)
            {
                var subscriptionId = authorization.SubscriptionId;

                var scope = Authorizations.Find(a => a.SubscriptionId == subscriptionId).Scope;

                // Get Subscription info
                var subscription = InsightsEfRepository.GetGreenButtonAmiSubscription(subscriptionId);

                if (subscription != null)
                {
                    if (finalizedSubscription == null)
                        finalizedSubscription = subscription;

                    // Get usage point
                    var usagePoints = InsightsEfRepository.GetGreenButtonAmiUsagePoints(subscriptionId, scope);

                    if (!scope.FunctionBlocks.Exists(fb => fb == GreenButtonFunctionBlockType.MulitpleUsagePoints) && usagePoints.Count > 1)
                        return new GreenButtonResult { Status = GreenButtonErrorType.MultipleUsageNotSupportErr };

                    if (usagePoints.Count > 0)
                    {
                        foreach (var usagePoint in usagePoints)
                        {
                            var meterId = usagePoint.MeterId;
                            var status = GreenButtonErrorType.NoError;
                            var readingTypes = GetReadingTypes(subscription.AccountId, meterId, null, null, scope, ref status);
                            if (status != GreenButtonErrorType.NoError)
                                return new GreenButtonResult { Status = status };
                            usagePoint.ReadingTypes = readingTypes;
                            if(finalizedSubscription.UsagePoints == null)
                                finalizedSubscription.UsagePoints = new List<GreenButtonUsagePoint> { usagePoint };
                            else
                                finalizedSubscription.UsagePoints.Add(usagePoint);
                        }
                    }
                    
                }

            }
            if(finalizedSubscription == null)
                return new GreenButtonResult { Status = GreenButtonErrorType.InvalidSubscriptionErr };
            return GreenButtonModel.GenerateGreenButton(finalizedSubscription, GreenButtonEntryType.ReadingType);
        }

        public GreenButtonResult GetReadingTypesGreenButton(string accessToken, string readingTypeId)
        {
            if (Authorizations == null || Authorizations.FindAll(a => a.AccessToken == accessToken).Count == 0)
                return new GreenButtonResult { Status = GreenButtonErrorType.InvalidAuthorization };

            // check accessToken type
            var accessTokenType = Authorizations.Find(a => a.AccessToken == accessToken).AccessTokenType;

            if (accessTokenType == GreenButtonAccessTokenType.Customer)
                Authorizations = Authorizations.FindAll(a => a.AccessToken == accessToken);

            GreenButtonSubscription finalizedSubscription = null;
            foreach (var authorization in Authorizations)
            {
                var subscriptionId = authorization.SubscriptionId;

                var scope = Authorizations.Find(a => a.SubscriptionId == subscriptionId).Scope;

                // Get Subscription info
                var subscription = InsightsEfRepository.GetGreenButtonAmiSubscription(subscriptionId);

                if (subscription != null)
                {
                    if (finalizedSubscription == null)
                        finalizedSubscription = subscription;

                    // Get usage point
                    var usagePoints = InsightsEfRepository.GetGreenButtonAmiUsagePoints(subscriptionId, scope);

                    if (!scope.FunctionBlocks.Exists(fb => fb == GreenButtonFunctionBlockType.MulitpleUsagePoints) && usagePoints.Count > 1)
                        return new GreenButtonResult { Status = GreenButtonErrorType.MultipleUsageNotSupportErr };

                    if (usagePoints.Count > 0)
                    {
                        foreach (var usagePoint in usagePoints)
                        {
                            var status = GreenButtonErrorType.NoError;
                            var readingType = GetReadingType(subscriptionId,usagePoint.MeterId,readingTypeId, ref status);
                            if (status == GreenButtonErrorType.NoError)
                            {
                                usagePoint.ReadingTypes = new List<GreenButtonAmiReadingType> { readingType };
                                if (finalizedSubscription.UsagePoints == null)
                                    finalizedSubscription.UsagePoints = new List<GreenButtonUsagePoint> { usagePoint };
                                else
                                    finalizedSubscription.UsagePoints.Add(usagePoint);
                            }
                        }
                    }

                }

            }
            if (finalizedSubscription == null)
                return new GreenButtonResult { Status = GreenButtonErrorType.InvalidSubscriptionErr };
            if (finalizedSubscription.UsagePoints == null || (finalizedSubscription.UsagePoints.SelectMany(up => up.ReadingTypes).ToList().Count == 0))
            {
                return new GreenButtonResult { Status = GreenButtonErrorType.NoReadingTypeErr };
            }
            return GreenButtonModel.GenerateGreenButton(finalizedSubscription, GreenButtonEntryType.ReadingType);
        }
    }
}