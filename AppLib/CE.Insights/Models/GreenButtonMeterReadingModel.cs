﻿using CE.GreenButtonConnect;
using CE.Models;
using CE.Models.Insights;
using CE.Models.Insights.GreenButtonConnect;
using CE.Models.Insights.Types;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Text;

namespace CE.Insights.Models
{
    public class GreenButtonMeterReadingModel : GreenButtonBaseModel
    {
        /// <summary>
        /// constructor for meter reading model
        /// </summary>
        public GreenButtonMeterReadingModel(string accessToken, string locale, string greenButtonDomain, IUnityContainer container, IInsightsEFRepository insightsEfRepository) : base(accessToken, locale, greenButtonDomain, container, insightsEfRepository)
        {
        }

        /// <summary>
        /// Testability, passed in repository and unity container so not relying on data access through sql server and table storage
        /// </summary>
        /// <param name="greenButtonDomain"></param>
        /// <param name="authorizations"></param>
        /// <param name="greenButtonSettings"></param>
        /// <param name="unityContainer"></param>
        /// <param name="insightsEfRepository"></param>
        /// <param name="clientId"></param>
        public GreenButtonMeterReadingModel(int clientId, string greenButtonDomain, List<GreenButtonAuthorization> authorizations, GreenButtonConfig greenButtonSettings, IUnityContainer unityContainer, IInsightsEFRepository insightsEfRepository): base(clientId, greenButtonDomain, authorizations, unityContainer,insightsEfRepository, greenButtonSettings)
        {
        }

        public GreenButtonResult GetAllMeterReadingsGreenButton(string accessToken, long subscriptionId, string encryptedMeterId, DateTime? startDate, DateTime? endDate)
        {
            // validate authorization
            if (!Authorizations.Exists(a => a.SubscriptionId == subscriptionId && a.SubscriptionId > 0 && a.AccessToken == accessToken))
                return new GreenButtonResult { Status = GreenButtonErrorType.InvalidSubscriptionAccessTokenErr };
            
            var scope = Authorizations.Find(a => a.SubscriptionId == subscriptionId).Scope;

            // Decrypt Meter Id
            var meterId = Helper.ConvertHexToString(encryptedMeterId, Encoding.Unicode);

            // Get Subscription info
            var subscription = InsightsEfRepository.GetGreenButtonAmiSubscription(subscriptionId);

            if (subscription != null)
            {
                // Get usage point
                var usagePoint = InsightsEfRepository.GetGreenButtonAmiUsagePoint(subscriptionId, meterId);

                if (usagePoint != null)
                {
                    var status = GreenButtonErrorType.NoError;
                    var readingTypes = GetReadingTypes(subscription.AccountId,meterId, startDate, endDate, scope, ref status);
                    if (status != GreenButtonErrorType.NoError)
                    {
                        if (status == GreenButtonErrorType.NoReadingTypeErr)
                            return new GreenButtonResult { Status = GreenButtonErrorType.NoMeterReadingErr };
                        return new GreenButtonResult { Status = status };
                    }
                    usagePoint.ReadingTypes = readingTypes;
                    subscription.UsagePoints = new List<GreenButtonUsagePoint> { usagePoint };
                    
                    // GetGreenButton
                    return GreenButtonModel.GenerateGreenButton(subscription, GreenButtonEntryType.MeterReading);
                }
                return new GreenButtonResult { Status = GreenButtonErrorType.InvalidSusbscriptionUsagePointErr };
            }
            return new GreenButtonResult {Status = GreenButtonErrorType.InvalidSubscriptionErr};
        }

        public GreenButtonResult GetMeterReadingGreenButton(string accessToken, long subscriptionId, string encryptedMeterId, string meterReadingId)
        {
            // validate authorization
            if (!Authorizations.Exists(a => a.SubscriptionId == subscriptionId && a.SubscriptionId > 0 && a.AccessToken == accessToken))
                return new GreenButtonResult { Status = GreenButtonErrorType.InvalidSubscriptionAccessTokenErr };
            
            // Decrypt Meter Id
            var meterId = Helper.ConvertHexToString(encryptedMeterId, Encoding.Unicode);

            // Get Subscription info
            var subscription = InsightsEfRepository.GetGreenButtonAmiSubscription(subscriptionId);

            if (subscription != null)
            {
                // Get usage point
                var usagePoint = InsightsEfRepository.GetGreenButtonAmiUsagePoint(subscriptionId, meterId);

                if (usagePoint != null)
                {
                    var status = GreenButtonErrorType.NoError;
                    var readingType = GetReadingType(subscriptionId,meterId, meterReadingId, ref status);
                    if (status != GreenButtonErrorType.NoError)
                    {
                        if(status == GreenButtonErrorType.NoReadingTypeErr)
                            return new GreenButtonResult { Status = GreenButtonErrorType.NoMeterReadingErr };
                        return new GreenButtonResult { Status = status };
                    }
                    usagePoint.ReadingTypes = new List<GreenButtonAmiReadingType> {readingType};
                    subscription.UsagePoints = new List<GreenButtonUsagePoint> { usagePoint };

                    // GetGreenButton
                    return GreenButtonModel.GenerateGreenButton(subscription, GreenButtonEntryType.MeterReading);
                }
                return new GreenButtonResult { Status = GreenButtonErrorType.InvalidSusbscriptionUsagePointErr };
            }
            return new GreenButtonResult { Status = GreenButtonErrorType.InvalidSubscriptionErr };
        }
    }
}