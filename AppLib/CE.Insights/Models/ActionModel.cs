﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using CE.BillHighlightsModel;
using CE.AmiHighlightsModel;
using CE.ContentModel;
using CE.Infrastructure;
using CE.MeasuresEngine;
using CE.Models;
using CE.Models.Insights;
using CE.Models.Insights.Types;
using Microsoft.Practices.Unity;

namespace CE.Insights.Models
{
    /// <summary>
    /// Action model
    /// </summary>
    public class ActionModel
    {
        private const string ActionSavingsEstQuality = "fair";
        private const string ActionPopularity = "very";
        private const string ActionCommodityElectric = "electric";
        private const string ActionCommodityGas = "gas";
        private const string ActionCommodityWater = "water";
        private const string ActionCommodityAll = "all";
        private const string ActionBillHighlight = "billhighlight";
        private const string ActionAmiHighlight = "amihighlight";
        private IUnityContainer _container;

        public ActionModel(IUnityContainer unityContainer)
        {
            _container = unityContainer;
        }

        /// <summary>
        /// MeasureModel is aka. ActionModel
        /// </summary>
        /// <param name="clientUser"></param>
        /// <param name="request"></param>
        public ActionResponse ExecuteActionModel(ClientUser clientUser, ActionRequest request)
        {
            ActionResponse response = ValidateRequest(request) ;

            if (response.StatusType == StatusType.ParameterErrors)
            {
                return response;

            }
            DataTable dataTable = null;
            var customerId = request.CustomerId;
            var accountId = request.AccountId;
            var premiseId = request.PremiseId;

            MeasuresEngineManagerFactory factory;
            IMeasuresEngine manager = null;


            // Get Bill Highlights
            var billHighlights = new List<BillHighlightModel>();
            if (!string.IsNullOrEmpty(request.Types) && request.Types.ToLower().Contains(ActionBillHighlight))
            {
                var billHighlightTask = GetBillHighlightAsync(clientUser.ClientID, clientUser.RateCompanyID, customerId, accountId,
                    premiseId, request.CommodityKeys, request.LatestBillDate, request.PreviousBillDate);
                billHighlights = billHighlightTask.Result;
            }

            // Get Ami Highlights
            var amiHighlights = new List<AmiHighlightModel>();
            if (!string.IsNullOrEmpty(request.Types) && request.Types.ToLower().Contains(ActionAmiHighlight))
            {
                //amiHighlights = CreateMockAmiHighlight();
                var amiHighlightTask = GetAmiHighlightAsync(clientUser, customerId, accountId, premiseId,
                    request.CommodityKeys);
                amiHighlights = amiHighlightTask.Result;
            }

            // Get the precalculated measures from the database.
            var dal = new MeasuresEngine.DataAccess.DataAccessLayer();
            if (!request.ForceCalc)
            {
                dataTable = dal.GetMeasures(clientUser.ClientID, customerId, accountId, premiseId, true);
            }

            //if there was nothing retrieved, execute measures/actions
            if (dataTable == null)
            {
                factory = new MeasuresEngineManagerFactory();
                manager = factory.CreateMeasuresEngineManager(clientUser.ClientID);
                var result = manager.ExecuteMeasures(customerId, accountId, premiseId);
                dataTable = result.FlatResultTable;
                dal.SaveMeasures(clientUser.ClientID, customerId, accountId, premiseId, dataTable);
            }

           // If nothing found return
            if (dataTable == null)
            {
                return response;
            }

            // Now process the data table and Map result to response
            var data = PopulateMeasureDataList(dataTable);

            // If no data found return.
            if (data == null)
            {
                return response;
            }

            // Apply basic filters here
            FilterByActionType(data, request);
            FilterByCommodity(data, request);

            if (manager == null)
            {
                factory = new MeasuresEngineManagerFactory();
                manager = factory.CreateMeasuresEngineManager(clientUser.ClientID);
            }

            // Apply advanced filters here; these are appliances and enduses filters; these require lookups via measures engine cached data
            if (!string.IsNullOrEmpty(request.ApplianceKeys) || !string.IsNullOrEmpty(request.EndUseKeys))
            {
                FilterByAppliance(data, request, manager.Configuration);
                FilterByEndUse(data, request, manager.Configuration);
            }


            // actions/measures are ready for mapping to response
            PopulateActionResponse(data, response, billHighlights, amiHighlights);
            return response;
        }


        private async Task<List<BillHighlightModel>> GetBillHighlightAsync(int clientId, int rateCompanyId, string customerId, string accountId, string premiseId,
                      string commodityKeys, DateTime latestBillDate, DateTime previousBillDate)
        {

            var billHighlightsModelFactory = new BillHighlightsModelFactory();

            var billHighlightManager =
                billHighlightsModelFactory.CreateBillHighlightsModelManager(clientId, rateCompanyId);
            //var calculationManager = _container.Resolve<ICalculation>();
            var task = await Task.Factory.StartNew(() => billHighlightManager.ExecuteBillHighlightModel(customerId, accountId, premiseId,
                      commodityKeys, latestBillDate, previousBillDate)).ConfigureAwait(false);
            return task;
        }

        private async Task<List<AmiHighlightModel>> GetAmiHighlightAsync(ClientUser clientUser, string customerId, string accountId, string premiseId,
                     string commodityKeys)
        {

            var amiHighlightsModelFactory = new AmiHighlightsModelFactory();

            var amiHighlightManager =
                amiHighlightsModelFactory.CreateAmiHighlightsModelManager(clientUser, _container);
            //var calculationManager = _container.Resolve<ICalculation>();
            var task = await Task.Factory.StartNew(() => amiHighlightManager.ExecuteAmiHighlightModel(customerId, accountId, premiseId,
                    commodityKeys)).ConfigureAwait(false);
            return task;
        }

        /// <summary>
        /// Determine what types of measures are requested.
        /// </summary>
        /// <param name="data">The measures data.</param>
        /// <param name="req">The current request</param>
        private void FilterByActionType(IList<MeasureRow> data, ActionRequest req)
        {
            // No types
            if (string.IsNullOrEmpty(req.Types))
            {
                // If no types remove the billhighlights and ami highlights so only Actions are returned.
                FilterByHighlight(data);

                return;
            }

            // Get the requested types.
            var keys = req.Types.ToLower().Split(CEConfiguration.ParameterListDelimiter).ToList();

            // iterate each measure row, and if it is not part of the inclusive filter list, remove it!
            for (var i = data.Count - 1; i >= 0; i--)
            {
                if (!keys.Contains(data[i].ActionTypeKey.ToLower()))
                {
                    data.RemoveAt(i);
                }
            }
        }

        /// <summary>
        /// Remove Bill and Ami Highlight Actions
        /// </summary>
        /// <param name="data">The measures data.</param>
        private void FilterByHighlight(IList<MeasureRow> data)
        {
            // iterate each measure row, and if it is not part of the inclusive filter list, remove it!
            for (var i = data.Count - 1; i >= 0; i--)
            {
                if (ActionBillHighlight.Equals(data[i].ActionTypeKey, StringComparison.InvariantCultureIgnoreCase) || ActionAmiHighlight.Equals(data[i].ActionTypeKey, StringComparison.InvariantCultureIgnoreCase))
                {
                    data.RemoveAt(i);
                }
            }
        }


        /// <summary>
        /// Determine what commodities should be measured
        /// </summary>
        /// <param name="data">The measures data.</param>
        /// <param name="req">The current request</param>
        private void FilterByCommodity(IList<MeasureRow> data, ActionRequest req)
        {
            // No commodities
            if (string.IsNullOrEmpty(req.CommodityKeys))
            {
                return;
            }

            // Get the commodities
            var keys = req.CommodityKeys.ToLower().Split(CEConfiguration.ParameterListDelimiter).ToList();

            // iterate each measure row, and if it is not part of the inclusive filter list, remove it!
            for (var i = data.Count - 1; i >= 0; i--)
            {
                if (!keys.Contains(data[i].CommodityKey.ToLower()) &&
                    !data[i].CommodityKey.Equals("all", StringComparison.InvariantCultureIgnoreCase))
                {
                    data.RemoveAt(i);
                }
            }
        }

        /// <summary>
        /// Determine what appliances should be measured
        /// </summary>
        /// <param name="data">The measures data.</param>
        /// <param name="req">The current request</param>
        /// <param name="config">The current measures configuration</param>
        private void FilterByAppliance(IList<MeasureRow> data, ActionRequest req, MeasuresConfiguration config)
        {
            // No appliances
            if (string.IsNullOrEmpty(req.ApplianceKeys))
            {
                return;
            }

            // Get the appliances
            var keys = req.ApplianceKeys.ToLower().Split(CEConfiguration.ParameterListDelimiter).ToList();

            // iterate each measure row, and if it is not part of the inclusive filter list, remove it!
            for (var i = data.Count - 1; i >= 0; i--)
            {
                // Appliances must be looked up using the appliances list from the action content
                var action = config.ClientActions[data[i].ActionKey];

                // If no action found.
                if (action == null)
                {
                    continue;
                }

                // If for billhighlight or ami highlight continue
                if (action.Type.Equals(ActionBillHighlight, StringComparison.InvariantCultureIgnoreCase) || action.Type.Equals(ActionAmiHighlight, StringComparison.InvariantCultureIgnoreCase))
                {
                    continue;
                } 

                if (!string.IsNullOrEmpty(action.ApplianceKeys))
                {
                    //if appliances do not intersect, then the item is filtered out
                    var appliances = action.ApplianceKeys.ToLower().Split(CEConfiguration.ParameterListDelimiter).ToList();
                    var results = keys.Intersect(appliances, StringComparer.OrdinalIgnoreCase);

                    if (!results.Any())
                    {
                        data.RemoveAt(i);
                    }
                }
                else
                {
                    data.RemoveAt(i);
                }
            }
        }

        /// <summary>
        /// Determine what end-uses should be measured
        /// </summary>
        /// <param name="data">The measures data.</param>
        /// <param name="req">The current request</param>
        /// <param name="config">The current measures configuration</param>
        private void FilterByEndUse(IList<MeasureRow> data, ActionRequest req, MeasuresConfiguration config)
        {
            // No end use keys
            if (string.IsNullOrEmpty(req.EndUseKeys))
            {
                return;
            }

            // Get the end uses
            var keys = req.EndUseKeys.ToLower().Split(CEConfiguration.ParameterListDelimiter).ToList();

            // iterate each measure row, and if it is not part of the inclusive filter list, remove it!
            for (var i = data.Count - 1; i >= 0; i--)
            {
                var intersect = false;

                // Enduses must be looked up using the appliances list from the action content
                var action = config.ClientActions[data[i].ActionKey];

                // If no action found.
                if (action == null)
                {
                    continue;
                }

                // If for billhighlight or amihighlight continue 
                if (action.Type.Equals(ActionBillHighlight, StringComparison.InvariantCultureIgnoreCase) || action.Type.Equals(ActionAmiHighlight, StringComparison.InvariantCultureIgnoreCase))
                {
                    continue;
                }

                // There have to be appliances to coorelate the end uses.
                if (!string.IsNullOrEmpty(action.ApplianceKeys))
                {
                    foreach (var endUseKey in keys)
                    {
                        var endUseAppliances = config.ClientEndUses.ContainsKey(endUseKey)
                            ? config.ClientEndUses[endUseKey].Appliances.ToLower().Split(';').ToList()
                            : new List<string>();

                        //if appliances intersect, then the item will be kept, since matching endUse
                        var actionAppliances =
                            action.ApplianceKeys.ToLower().Split(CEConfiguration.ParameterListDelimiter).ToList();

                        var results = actionAppliances.Intersect(endUseAppliances, StringComparer.OrdinalIgnoreCase);

                        if (results.Any())
                        {
                            intersect = true;
                            break;
                        }
                    }

                    if (!intersect)
                    {
                        data.RemoveAt(i);
                    }
                }
                else
                {
                    data.RemoveAt(i);
                }
            }
        }

        /// <summary>
        /// Populate the Action response.
        /// </summary>
        /// <param name="dataList">The measures data.</param>
        /// <param name="response">The current Action response.</param>
        /// <param name="billHighlights"></param>
        /// <param name="amiHighlights"></param>
        private void PopulateActionResponse(IReadOnlyList<MeasureRow> dataList, ActionResponse response, List<BillHighlightModel> billHighlights, List<AmiHighlightModel> amiHighlights)
        {
            var customer = new ActionCustomer();
            ActionAccount account = null;
            ActionPremise premise = null;
            CE.Models.Insights.Action action = null;

            var lastAccountId = string.Empty;
            var lastPremiseId = string.Empty;
            var lastActionKey = string.Empty;

            var accountAdded = false;
            var premiseAdded = false;
            //var actionAdded = false;

            if (dataList != null && dataList.Count > 0)
            {
                // For each row in the Measures lists. Action saving values stored in the Db.
                foreach (var row in dataList)
                {
                    // Do not repeat the last account processed.
                    if (lastAccountId != row.AccountId)
                    {
                        // account
                        lastAccountId = row.AccountId;

                        if (account != null)
                        {
                            if (premise != null)
                            {
                                premise.Actions.Add(action); //special
                                account.Premises.Add(premise); //special
                            }

                            customer.Accounts.Add(account);
                            accountAdded = true;
                        }

                        account = new ActionAccount
                        {
                            Id = row.AccountId,
                        };
                        
                    }

                    // Do not repeat the last premise processed.
                    if (lastPremiseId != row.PremiseId || accountAdded)
                    {
                        // premise
                        lastPremiseId = row.PremiseId;
                        if (!accountAdded && premise != null)
                        {
                            premise.Actions.Add(action); //special
                            account?.Premises.Add(premise);
                            premiseAdded = true;
                        }

                        premise = new ActionPremise
                        {
                            Id = row.PremiseId,
                        };
                    }

                    // Do not readd the last action.
                    if (lastActionKey == string.Empty ||
                        premiseAdded ||
                        accountAdded ||
                        (lastActionKey != string.Empty &&
                         !lastActionKey.Equals(row.ActionKey, StringComparison.InvariantCultureIgnoreCase)))
                    {

                        // action
                        lastActionKey = row.ActionKey;

                        if (!row.ActionTypeKey.Equals(ActionBillHighlight, StringComparison.InvariantCultureIgnoreCase) && !row.ActionTypeKey.Equals(ActionAmiHighlight, StringComparison.InvariantCultureIgnoreCase))
                        {
                            action = new CE.Models.Insights.Action
                            {
                                Key = row.ActionKey,
                                ActionTypeKey = row.ActionTypeKey,
                                AnnualCost = row.AnnualCost,
                                AnnualSavingsEstimate = row.AnnualSavingsEstimate,
                                AnnualSavingsEstimateCurrencyKey = row.AnnualSavingsEstimateCurrencyKey,
                                CostVariancePercent = row.AnnualCostVariancePercent,
                                PaybackTime = row.Payback,
                                ROI = row.Roi,
                                UpfrontCost = row.UpfrontCost,
                                SavingsEstimateQuality = ActionSavingsEstQuality, //hard coded, not dynamic
                                Popularity = ActionPopularity, //hard coded, not dynamic
                                Priority = row.Priority,
                                RebateAmount = row.RebateAmount
                            };

                            // if there is any save amount or usage quantity, init the list and add approprite commodities.
                            if (row.ElecSavEst != 0m
                                || row.ElecUsgSavEst != 0m
                                || row.GasSavEst != 0m
                                || row.GasUsgSavEst != 0m
                                || row.WaterSavEst != 0m
                                || row.WaterUsgSavEst != 0m)
                            {
                                action.ActionDetails = new List<ActionDetail>();
                            }

                            // If there is electrical savings 
                            if (row.ElecSavEst != 0m || row.ElecUsgSavEst != 0m)
                            {
                                var actionDetail = new ActionDetail()
                                {
                                    CommodityKey = ActionCommodityElectric,
                                    SavingsEstimate = row.ElecSavEst,
                                    SavingsEstimateCurrencyKey = row.ElecSavEstCurrencyKey,
                                    UsageSavingsEstimate = row.ElecUsgSavEst,
                                    UsageSavingsEstimateUomKey = row.ElecUsgSavEstUomKey
                                };
                                action.ActionDetails.Add(actionDetail);
                            }

                            // If there is gas savings.
                            if (row.GasSavEst != 0m || row.GasUsgSavEst != 0m)
                            {
                                var actionDetail = new ActionDetail()
                                {
                                    CommodityKey = ActionCommodityGas,
                                    SavingsEstimate = row.GasSavEst,
                                    SavingsEstimateCurrencyKey = row.GasSavEstCurrencyKey,
                                    UsageSavingsEstimate = row.GasUsgSavEst,
                                    UsageSavingsEstimateUomKey = row.GasUsgSavEstUomKey
                                };
                                action.ActionDetails.Add(actionDetail);
                            }

                            // If there is water savings.
                            if (row.WaterSavEst != 0m || row.WaterUsgSavEst != 0m)
                            {
                                var actionDetail = new ActionDetail()
                                {
                                    CommodityKey = ActionCommodityWater,
                                    SavingsEstimate = row.WaterSavEst,
                                    SavingsEstimateCurrencyKey = row.WaterSavEstCurrencyKey,
                                    UsageSavingsEstimate = row.WaterUsgSavEst,
                                    UsageSavingsEstimateUomKey = row.WaterUsgSavEstUomKey
                                };
                                action.ActionDetails.Add(actionDetail);
                            }

                            // remove any extra commodities if the row.commoditykey is specific, and there are unmatching details
                            if (row.CommodityKey.Equals(ActionCommodityAll, StringComparison.InvariantCultureIgnoreCase))
                            {
                                if (action.ActionDetails != null && action.ActionDetails.Count > 0)
                                {
                                    for (var i = action.ActionDetails.Count - 1; i >= 0; i--)
                                    {
                                        if (action.ActionDetails[i].CommodityKey != row.CommodityKey.ToLower())
                                        {
                                            action.ActionDetails.RemoveAt(i);
                                        }
                                    }

                                    // if nothing left, effectively nullify the detail list
                                    if (action.ActionDetails.Count == 0)
                                    {
                                        action.ActionDetails = null;
                                    }
                                }
                            }


                            premise?.Actions.Add(action);


                        }
                    }

                    // Reset the watch flags
                    if (accountAdded)
                    {
                        accountAdded = false;
                    }

                    if (premiseAdded)
                    {
                        premiseAdded = false;
                    }
                }

                // last items need to be added
                account?.Premises.Add(premise);
                customer.Accounts.Add(account);

                // add bill highlights
                if (billHighlights != null && billHighlights.Any())
                {
                    PopulateBillHighlights(billHighlights, ref customer);
                }

                // add ami highlights
                if (amiHighlights != null && amiHighlights.Any())
                {
                    PopulateAmiHighlights(amiHighlights,ref customer);
                }


                customer.Id = dataList[0].CustomerId;
                response.Customer = customer;
            }
        }

        /// <summary>
        /// Populate Bill Highlights to the response
        /// </summary>
        /// <param name="billHighlights"></param>
        /// <param name="customer"></param>
        private void PopulateBillHighlights(List<BillHighlightModel> billHighlights, ref ActionCustomer customer)
        {
            foreach (var account in customer.Accounts)
            {
                // add account level highlights
                foreach (var premise in account.Premises)
                {
                    premise.BillHighlights = new List<BillHighlight>();
                    foreach (var billHighlightModel in billHighlights)
                    {
                        premise.BillHighlights.Add(new BillHighlight
                        {
                            ServiceId = billHighlightModel.ServiceId,
                            ActionKey = billHighlightModel.ActionKey,
                            Commoditykey = billHighlightModel.Commoditykey,
                            CostImpact = billHighlightModel.CostImpact,
                            PercentageImpact = billHighlightModel.PercentageImpact,
                        });
                    }
                }
            }
        }

        /// <summary>
        /// Populate Ami Highlights to the response
        /// </summary>
        /// <param name="amiHighlights"></param>
        /// <param name="customer"></param>
        private void PopulateAmiHighlights(List<AmiHighlightModel> amiHighlights, ref ActionCustomer customer)
        {
            foreach (var account in customer.Accounts)
            {
                // add account level highlights
                foreach (var premise in account.Premises)
                {

                    premise.AmiHighlights = new List<AmiHighlight>();
                    foreach (var amihighlightModel in amiHighlights)
                    {
                        premise.AmiHighlights.Add(new AmiHighlight
                        {
                            ServiceId = amihighlightModel.ServiceId,
                            ActionKey = amihighlightModel.ActionKey,
                            Commoditykey = amihighlightModel.Commoditykey,
                            CostImpact = amihighlightModel.CostImpact,
                            PercentageImpact = amihighlightModel.PercentageImpact,
                            MeterId = amihighlightModel.MeterId,
                            TimeStamp =  amihighlightModel.HighlightTimeStamp
                            
                        });
                    }
                }
            }
            
            
        }

        /// <summary>
        /// Populate the Measure Data List
        /// </summary>
        /// <param name="tbl"></param>
        /// <returns></returns>
        private List<MeasureRow> PopulateMeasureDataList(DataTable tbl)
        {
            var rowList = new List<MeasureRow>();

            foreach (DataRow r in tbl.Rows)
            {
                var row = new MeasureRow
                {
                    ClientId = r["ClientId"] as int? ?? default(int),
                    CustomerId = r["CustomerId"] as string,
                    AccountId = r["AccountId"] as string,
                    PremiseId = r["PremiseId"] as string,
                    ActionKey = r["ActionKey"] as string,
                    ActionTypeKey = r["ActionTypeKey"] as string,
                    AnnualCost = r["AnnualCost"] as decimal? ?? default(decimal),
                    AnnualCostVariancePercent = r["AnnualCostVariancePercent"] as decimal? ?? default(decimal),
                    AnnualSavingsEstimate = r["AnnualSavingsEstimate"] as decimal? ?? default(decimal),
                    AnnualSavingsEstimateCurrencyKey = r["AnnualSavingsEstimateCurrencyKey"] as string,
                    Roi = r["Roi"] as decimal? ?? default(decimal),
                    Payback = r["Payback"] as decimal? ?? default(decimal),
                    UpfrontCost = r["UpfrontCost"] as decimal? ?? default(decimal),
                    CommodityKey = r["CommodityKey"] as string,
                    ElecSavEst = r["ElecSavEst"] as decimal? ?? default(decimal),
                    ElecSavEstCurrencyKey = r["ElecSavEstCurrencyKey"] as string,
                    ElecUsgSavEst = r["ElecUsgSavEst"] as decimal? ?? default(decimal),
                    ElecUsgSavEstUomKey = r["ElecUsgSavEstUomKey"] as string,
                    GasSavEst = r["GasSavEst"] as decimal? ?? default(decimal),
                    GasSavEstCurrencyKey = r["GasSavEstCurrencyKey"] as string,
                    GasUsgSavEst = r["GasUsgSavEst"] as decimal? ?? default(decimal),
                    GasUsgSavEstUomKey = r["GasUsgSavEstUomKey"] as string,
                    WaterSavEst = r["WaterSavEst"] as decimal? ?? default(decimal),
                    WaterSavEstCurrencyKey = r["WaterSavEstCurrencyKey"] as string,
                    WaterUsgSavEst = r["WaterUsgSavEst"] as decimal? ?? default(decimal),
                    WaterUsgSavEstUomKey = r["WaterUsgSavEstUomKey"] as string,
                    Priority = r["Priority"] as int? ?? default(int),
                    NewDate = r["NewDate"] as DateTime? ?? default(DateTime),
                    RebateAmount = r["RebateAmount"] as decimal? ?? default(decimal)
                };

                rowList.Add(row);
            }

            return rowList;
        }

        /// <summary>
        /// The Measure Row class
        /// </summary>
        private class MeasureRow
        {
            // ReSharper disable once UnusedAutoPropertyAccessor.Local
            public int ClientId { get; set; }

            public string CustomerId { get; set; }

            public string AccountId { get; set; }

            public string PremiseId { get; set; }

            public string ActionKey { get; set; }

            public string ActionTypeKey { get; set; }

            public decimal AnnualCost { get; set; }

            public decimal AnnualCostVariancePercent { get; set; }

            public decimal AnnualSavingsEstimate { get; set; }

            public string AnnualSavingsEstimateCurrencyKey { get; set; }

            public decimal Roi { get; set; }

            public decimal Payback { get; set; }

            public decimal UpfrontCost { get; set; }

            public string CommodityKey { get; set; }

            public decimal ElecSavEst { get; set; }

            public string ElecSavEstCurrencyKey { get; set; }

            public decimal ElecUsgSavEst { get; set; }

            public string ElecUsgSavEstUomKey { get; set; }
            public decimal GasSavEst { get; set; }
            public string GasSavEstCurrencyKey { get; set; }

            public decimal GasUsgSavEst { get; set; }

            public string GasUsgSavEstUomKey { get; set; }

            public decimal WaterSavEst { get; set; }

            public string WaterSavEstCurrencyKey { get; set; }

            public decimal WaterUsgSavEst { get; set; }

            public string WaterUsgSavEstUomKey { get; set; }

            public int Priority { get; set; }

            // ReSharper disable once UnusedAutoPropertyAccessor.Local
            public DateTime NewDate { get; set; }

            public decimal RebateAmount { get; set; } 
        }

        /// <summary>
        /// Local Content grabber.
        /// </summary>
        /// <param name="response">The current Action response</param>
        /// <param name="clientUser">The current user</param>
        /// <param name="locale">The current locale</param>
        public void ApplyContent(ActionResponse response, ClientUser clientUser, string locale)
        {
            //content
            var cf = new ContentModelFactoryContentful();
            var cp = cf.CreateContentProvider(clientUser.ClientID);
            var contentLocale = cp.GetContentLocale(locale);

            //get label content and configuration
            response.Content = new Content
            {
                TextContent = cp.GetContentTextContent("action", contentLocale, string.Empty),
                Configuration = cp.GetContentConfiguration("action", string.Empty),
                Commodity = cp.GetContentCommodity(string.Empty),
                Measurement = cp.GetContentMeasurement(string.Empty),
                UOM = cp.GetContentUom(string.Empty),
                Currency = cp.GetContentCurrency(string.Empty),
                Action = cp.GetContentAction(string.Empty)
            };
        }

        public ActionResponse ValidateRequest(ActionRequest request)
        {
            var response = new ActionResponse();
            ValidateRequestedBillDates(request, ref response);
            return response;
        }

        private void ValidateRequestedBillDates(ActionRequest request,ref ActionResponse response)
        {
            bool valid = true;
            if (request.PreviousBillDate == DateTime.MinValue && request.LatestBillDate != DateTime.MinValue)
            {
                valid = false;
            }
            else if(request.PreviousBillDate != DateTime.MinValue && request.LatestBillDate == DateTime.MinValue)
            {
                valid = false;
            }
            else if (request.PreviousBillDate > request.LatestBillDate)
            {
                valid = false;
            }
            //return true;

            if (!valid)
            {
                response.Message = "Invalid value for Bill Dates";
                response.StatusType = StatusType.ParameterErrors;
            }

        }

    }
}