﻿using System;
using System.Collections.Generic;
using System.Text;
using CE.Models;
using CE.Models.Insights;
using CE.Models.Insights.GreenButtonConnect;
using CE.Models.Insights.Types;
using Microsoft.Practices.Unity;
using CE.GreenButtonConnect;

namespace CE.Insights.Models
{
    public class GreenButtonIntervalBlockModel : GreenButtonBaseModel
    {

        /// <summary>
        /// constructor for IntervalBlock model
        /// </summary>
        public GreenButtonIntervalBlockModel(string accessToken, string locale, string greenButtonDomain, IUnityContainer container, IInsightsEFRepository insightsEfRepository) : base(accessToken, locale, greenButtonDomain, container, insightsEfRepository)
        {
        }

        /// <summary>
        /// Testability, passed in repository and unity container so not relying on data access through sql server and table storage
        /// </summary>
        /// <param name="greenButtonDomain"></param>
        /// <param name="authorizations"></param>
        /// <param name="greenButtonSettings"></param>
        /// <param name="unityContainer"></param>
        /// <param name="insightsEfRepository"></param>
        /// <param name="clientId"></param>
        public GreenButtonIntervalBlockModel(int clientId, string greenButtonDomain, List<GreenButtonAuthorization> authorizations, GreenButtonConfig greenButtonSettings, IUnityContainer unityContainer, IInsightsEFRepository insightsEfRepository): base(clientId, greenButtonDomain, authorizations, unityContainer,insightsEfRepository, greenButtonSettings)
        {
        }

        public GreenButtonResult GetAllIntervalBlocksGreenButton(string accessToken, long subscriptionId, string encryptedMeterId, string meterReadingId, DateTime? startDate, DateTime? endDate)
        {
            // validate authorization
            if (!Authorizations.Exists(a => a.SubscriptionId == subscriptionId && a.SubscriptionId > 0 && a.AccessToken == accessToken))
                return new GreenButtonResult { Status = GreenButtonErrorType.InvalidSubscriptionAccessTokenErr };

            // Decrypt Meter Id
            var meterId = Helper.ConvertHexToString(encryptedMeterId, Encoding.Unicode);

            var scope = Authorizations.Find(a => a.SubscriptionId == subscriptionId).Scope;

            // Get Subscription info
            var subscription = InsightsEfRepository.GetGreenButtonAmiSubscription(subscriptionId);

            if (subscription != null)
            {
                // Get usage point
                var usagePoint = InsightsEfRepository.GetGreenButtonAmiUsagePoint(subscriptionId, meterId);

                if (usagePoint != null)
                {
                    var status = GreenButtonErrorType.NoError;
                    var readingType = GetReadingTypeWithConsumption(subscriptionId, subscription.AccountId, meterId, meterReadingId, scope, startDate, endDate, ref status);
                    if (status != GreenButtonErrorType.NoError)
                    {
                        if (status == GreenButtonErrorType.NoReadingTypeErr)
                            return new GreenButtonResult { Status = GreenButtonErrorType.NoMeterReadingErr };
                        return new GreenButtonResult { Status = status };
                    }
                    
                    usagePoint.ReadingTypes = new List<GreenButtonAmiReadingType> { readingType };
                    subscription.UsagePoints = new List<GreenButtonUsagePoint> { usagePoint };

                    // GetGreenButton
                    return GreenButtonModel.GenerateGreenButton(subscription, GreenButtonEntryType.IntervalBlock);
                }
                return new GreenButtonResult { Status = GreenButtonErrorType.InvalidSusbscriptionUsagePointErr };

            }
            return new GreenButtonResult { Status = GreenButtonErrorType.InvalidSubscriptionErr };
        }


        public GreenButtonResult GetIntervalBlocksGreenButton(string accessToken, long subscriptionId, string encryptedMeterId, string meterReadingId, string intervalBlockId)
        {
            // decrypt interval block id
            var convertedIntervalBlockId = Helper.ConvertHexToString(intervalBlockId, Encoding.Unicode);


            // decrypt meter reading id
            var convertedMeterReadingId = Helper.ConvertHexToString(meterReadingId, Encoding.Unicode);

            var intervalBlockArray = convertedIntervalBlockId.Split('_');
            if(intervalBlockArray.GetUpperBound(0) != 1 || string.IsNullOrEmpty(intervalBlockArray[1]) || intervalBlockArray[0] != convertedMeterReadingId)
                return new GreenButtonResult { Status = GreenButtonErrorType.InvalidIntervalBlockErr };

            var date = Convert.ToDateTime(intervalBlockArray[1]);

            return GetAllIntervalBlocksGreenButton(accessToken, subscriptionId, encryptedMeterId, meterReadingId, date,
                date);
           
        }

    }
}