﻿using System;
using CE.Models.Insights;
using CE.Models.Insights.Types;
using System.Configuration;
using System.IO;
using Aclara.AO.AzureStorage;
using CE.Models;

namespace CE.Insights.Models
{

    /// <summary>
    /// File Model
    /// </summary>
    public class FileModel
    {
        private const string AzureStorageConnectionString = "AzureStorageConnectionString";
        private const string FileNotFoundError = "File not found";
        private static IInsightsEFRepository _insightsEfRepository;

        /// <summary>
        /// Testability, passed in repository so not relying on data access through sql server
        /// </summary>
        /// <param name="insightsEfRepository"></param>
        public FileModel(IInsightsEFRepository insightsEfRepository)
        {
            if (insightsEfRepository == null)
            {
                throw new ArgumentNullException("", @"InsightsRepository is null!");
            }

            _insightsEfRepository = insightsEfRepository;

        }

        /// <summary>
        /// Validate parameters for File Upload
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public FilePostResponse ValidateFileUpload(FilePostRequest request)
        {
            var response = new FilePostResponse();
            // validate filetype only if typeid is not passed

            request.Filedetail.TypeId =
                (int) ValidateFileType(request.Filedetail.TypeId, request.Filedetail.Name, ref response);
            ValidateRequiredField("AccountId", request.Filedetail.AccountId, ref response);
            ValidateRequiredField("CustomerId", request.Filedetail.CustomerId, ref response);
            ValidateRequiredField("PremiseId", request.Filedetail.PremiseId, ref response);
            ValidateRequiredField("File", request.Filedetail.Name, ref response);
            ValidateFileSource(request.Filedetail.FileSourceId, ref response);

            //if(request.Filedetail.SourceId )
            //validateRequiredField("AccountId", request.filedetail.AccountId, ref response);
            return response;
        }


        /// <summary>
        /// Validate File Source
        /// </summary>
        /// <param name="sourceId"></param>
        /// <param name="response"></param>
        private static void ValidateFileSource(int sourceId, ref FilePostResponse response)
        {
            if (!Enum.IsDefined(typeof(FileSource), sourceId) || sourceId == 0)
            {
                response.Message = "Invalid File Source";
                response.StatusType = StatusType.ParameterErrors;
            }

        }


        /// <summary>
        /// Validate File Type
        /// </summary>
        /// <param name="typeId"></param>
        /// <param name="fileName"></param>
        /// <param name="response"></param>
        /// <returns></returns>
        private static FileType ValidateFileType(int typeId, string fileName, ref FilePostResponse response)
        {
            FileType type;
            if (typeId == 0)
            {
                type = GetFileType(fileName);
            }
            else if (!Enum.IsDefined(typeof(FileType), typeId))
            {
                type = FileType.Unknown;
            }
            else
            {
                type = (FileType) typeId;
            }

            if (type == FileType.Unknown)
            {
                response.Message = "File type not supported";
                response.StatusType = StatusType.ParameterErrors;
            }
            return type;

        }

        /// <summary>
        /// Get filetype
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private static FileType GetFileType(string fileName)
        {
            FileType type;
            try
            {
                type =
                    (FileType)
                    Enum.Parse(typeof(FileType),
                        value: fileName.Remove(0, fileName.IndexOf(".", StringComparison.Ordinal) + 1), ignoreCase: true);
            }
            catch
            {
                type = FileType.Unknown;
            }
            return type;
        }

        private static void ValidateRequiredField(string fieldName, string fieldValue, ref FilePostResponse response)
        {
            var valid = string.IsNullOrWhiteSpace(fieldValue);

            if (valid)
            {
                response.Message = "Invalid value for : " + fieldName;
                response.StatusType = StatusType.ParameterErrors;
            }

        }

        private static string GetFileExtension(FileType fileType)
        {
            string extension = string.Empty;

            switch (fileType)
            {
                case FileType.Pdf:
                    extension = ".pdf";
                    break;
                case FileType.Csv:
                    extension = ".csv";
                    break;
                case FileType.Zip:
                    extension = ".zip";
                    break;
                case FileType.Unknown:
                    break;
            }
            return extension;

        }

        /// <summary>
        /// Saves File stream to Azure Storage
        /// </summary>
        /// <param name="request"></param>
        /// <param name="stream"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public string SaveFile(FilePostRequest request, Stream stream, int clientId)
        {
            //Post file to Azure storage
            var storageConnString = ConfigurationManager.AppSettings.Get(AzureStorageConnectionString);
            var containerName = "client-" + clientId;
            var mgr = new StorageManager();
            var result = mgr.CopyFileToBlobStorage(storageConnString, containerName, request.Filedetail.BlobName, stream);
            return result;
        }

        /// <summary>
        /// Delete single file from blob storage based on the file id
        /// </summary>
        /// <param name="request"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public FileResponse DeleteFile(FileRequest request, int clientId)
        {
            var response = new FileResponse();
            var storageConnString = ConfigurationManager.AppSettings.Get(AzureStorageConnectionString);
            var containerName = "client-" + clientId;

            //var blobName = request.CustomerId + "_" + request.FileId;
            //_insightsEfRepository.DeleteFile(request.FileId);
            var mgr = new StorageManager();

            var filedetail = _insightsEfRepository.GetFile(clientId, request.FileId);
            
            if (filedetail == null)
            {
                response.StatusType = StatusType.ParameterErrors;
                response.Message = FileNotFoundError;
                return response;
            }

            var extension = GetFileExtension((FileType) filedetail.TypeId);
            var blobName = filedetail.CustomerId + "_" + filedetail.Id + extension;

            // delete from blob storage
            var result = mgr.DeleteFileFromBlobStorage(storageConnString, containerName, blobName);

            // delete from database
            if (result.Equals("Success"))
            {
                _insightsEfRepository.DeleteFile(request.FileId);
            }
            else
            {
                response.Message = result;
            }
            return response;
        }


        /// <summary>
        /// Populate filedetail object
        /// </summary>
        /// <param name="formFieldName"></param>
        /// <param name="formFieldValue"></param>
        /// <param name="fileDetail"></param>
        public void SetFileUploadDetails(string formFieldName, string formFieldValue, FileDetail fileDetail)
        {
            switch (formFieldName)
            {
                case "CustomerId":
                    fileDetail.CustomerId = formFieldValue;
                    break;
                case "AccountId":
                    fileDetail.AccountId = formFieldValue;
                    break;
                case "PremiseId":
                    fileDetail.PremiseId = formFieldValue;
                    break;
                case "UserId":
                    fileDetail.UserId = formFieldValue;
                    break;
                case "RoleId":
                    fileDetail.RoleId = Convert.ToByte(formFieldValue);
                    break;
                case "Title":
                    fileDetail.Title = formFieldValue;
                    break;
                case "Description":
                    fileDetail.Description = formFieldValue;
                    break;
                case "SourceId":
                    fileDetail.FileSourceId = Convert.ToInt32(formFieldValue);
                    break;
                case "TypeId":
                    fileDetail.TypeId = Convert.ToInt32(formFieldValue);
                    break;
            }
        }


        /// <summary>
        /// Get file Data along with file content from Azure storage
        /// </summary>
        /// <param name="fileId"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public FileResponse GetFile(int fileId, int clientId)
        {
            var storageConnString = ConfigurationManager.AppSettings.Get(AzureStorageConnectionString);
            var response = new FileResponse();
            var containerName = "client-" + clientId;

            var mgr = new StorageManager();
            var filedetail = _insightsEfRepository.GetFile(clientId, fileId);
            if (filedetail == null)
            {
                response.StatusType = StatusType.ParameterErrors;
                response.Message = FileNotFoundError;
                return response;
            }
            var extension = GetFileExtension((FileType)filedetail.TypeId);
            var blobName = filedetail.CustomerId + "_" + fileId + extension;
            // retrieve file from blob storage
            MemoryStream memStream;
            var result = mgr.ExtractFileFromBlobStorage(storageConnString, containerName, blobName, out memStream);
            if (!result.Equals("Success"))
            {
                response.Message = FileNotFoundError;
                return response;
            }
            filedetail.FileContent = memStream;
            response.File = filedetail;

            return response;
        }

        /// <summary>
        /// Prepare and Returns FileResponse for GetFiles API
        /// </summary>
        /// <param name="request"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public FilesResponse GetFiles(FilesRequest request, int clientId)
        {
            FilesResponse response = new FilesResponse();
            response.Files = _insightsEfRepository.GetFiles(clientId, request);
            return response;
        }
    }
}