﻿using CE.ContentModel;
using CE.Models;
using CE.Models.Insights;
namespace CE.Insights.Models
{
    /// <summary>
    /// Benchmark model
    /// </summary>
    public class BenchmarkModel
    {
        /// <summary>
        /// Local Content grabber.
        /// </summary>
        /// <param name="response"></param>
        /// <param name="clientUser"></param>
        /// <param name="locale"></param>
        public void ApplyContent(BenchmarkResponse response, ClientUser clientUser, string locale)
        {
            //content
            var cf = new ContentModelFactoryContentful();
            var cp = cf.CreateContentProvider(clientUser.ClientID);
            var contentLocale = cp.GetContentLocale(locale);

            //get label content and configuration
            response.Content = new Content
            {
                TextContent = cp.GetContentTextContent("benchmark", contentLocale, string.Empty),
                Configuration = cp.GetContentConfiguration("benchmark", string.Empty),
                Commodity = cp.GetContentCommodity(string.Empty),
                BenchmarkGroup = cp.GetContentBenchmarkGroup(string.Empty),
                Measurement = cp.GetContentMeasurement(string.Empty),
                UOM = cp.GetContentUom(string.Empty),
                Currency = cp.GetContentCurrency(string.Empty)
            };
        }

    }
}