﻿using CE.GreenButton;
using CE.Insights.Helpers;
using CE.Models;
using CE.Models.Insights;
using CE.Models.Insights.Types;
using System;

namespace CE.Insights.Models
{
    public class GreenButtonBillModel
    {
        private GreenButton.GreenButtonConfig _greenButtonSettings;
        private GreenButtonUtil _greenButtonUtil;

        public GreenButtonBillModel(int clientId, string locale)
        {
            _greenButtonSettings = GreenButtonHelper.GetGreenButtonSettings(clientId, locale);
            _greenButtonUtil = new GreenButtonUtil(clientId, _greenButtonSettings);
        }

        /// <summary>
        /// Testability, passed in setting and repository so not relying on content configuration and data access through sql server
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="greenButtonSettings"></param>
        /// <param name="insightsEfRepository"></param>
        public GreenButtonBillModel(int clientId, GreenButton.GreenButtonConfig greenButtonSettings, IInsightsEFRepository insightsEfRepository)
        {
            _greenButtonSettings = greenButtonSettings;
            _greenButtonUtil = new GreenButtonUtil(clientId, _greenButtonSettings, insightsEfRepository);
        }
        
        /// <summary>
        /// get bill in green button format
        /// </summary>
        /// <param name="clientUser"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public ExportResult GetGreenButtonBill(ClientUser clientUser, GreenButtonBillRequest request)
        {
            // if output format is not provided, set it to zip if account id/premise id/service contract id is not available
            // otherwise xml
            if (string.IsNullOrEmpty(request.OutputFormat))
            {
                if (string.IsNullOrEmpty(request.AccountId) || string.IsNullOrEmpty(request.PremiseId) ||
                    string.IsNullOrEmpty(request.ServiceContractId))
                {
                    request.OutputFormat = GreenButtonOutputFormatType.Zip.ToString();
                }
                else
                {
                    request.OutputFormat = GreenButtonOutputFormatType.Xml.ToString();
                }
            }

            // validate the output format; if xml but account id/premise id/service contract id is not available, then throw error
            // otherwise continue
            if (
                request.OutputFormat.Equals(GreenButtonOutputFormatType.Xml.ToString(),
                    StringComparison.InvariantCultureIgnoreCase) &&
                (string.IsNullOrEmpty(request.AccountId) || string.IsNullOrEmpty(request.PremiseId) ||
                 string.IsNullOrEmpty(request.ServiceContractId)))
            {
                var result = new ExportResult
                {
                    ErrorMessage = "AccountId/PremiseId/ServiceContractId is required for xml output format"

                };
                return result;
            }
            else
            {
                var today = DateTime.UtcNow.Date;
                var startDate = today.AddMonths(_greenButtonSettings.BillMonths * -1);
                
                // generate green button for bill, set the start date based on configuration setting BillMonths
                var result = _greenButtonUtil.GenerateBillXml(request.CustomerId, request.AccountId, request.PremiseId,
                    request.ServiceContractId, startDate, today, GreenButtonHelper.GetOutputFormat(request.OutputFormat));
                return result;
                
            }
        }

        
    }
}