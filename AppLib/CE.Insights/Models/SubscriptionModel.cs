﻿using AutoMapper;
using AO.BusinessContracts;
using CE.AO.Models;
using CE.ContentModel;
using CE.Models;
using CE.Models.Insights;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AO.Business;
//using CE.AO.Logging;
using CE.Models.Insights.Types;
using AutoMapperConfig = CE.AO.Business.AutoMapperConfig;
using Enums = CE.AO.Utilities.Enums;


namespace CE.Insights.Models
{
    public class SubscriptionModel
    {
        private readonly IUnityContainer _container;
        private const string InsightKey = "{0}.{1}";
        private const string SmsBlockedNumberWarnings = "BlockedNumberWarnings";
        private const string SmsInvalidNumberFormatWarnings = "InvalidNumberFormatWarnings";
        private const string SmsInvalidSubscriptionWarnings = "InvalidSubscriptionWarnings";
        private const string SmsOtherWarnings = "OtherWarnings";

        //private const string InvalidInsightsMsg = "There is invalid insight(s)";

        public SubscriptionModel(IUnityContainer unityContainer)
        {
            _container = unityContainer;
        }

        public SubscriptionResponse GetSubscriptions(ClientUser clientUser, SubscriptionRequest request)
        {
            var response = new SubscriptionResponse();
            var insightPrograms = GetAllInsightPrograms(clientUser.ClientID, request.CustomerId, request.AccountId);

            if (insightPrograms != null && insightPrograms.Count > 0)
            {
                var customerInsight = MapSubscriptionsToCustomerInsight(insightPrograms, request.CustomerId);

                // get customer email and phone
                var customer = GetCustomerInfo(clientUser.ClientID, request.CustomerId);
                
                customerInsight.Email = customer?.EmailAddress;
                customerInsight.Phone = string.IsNullOrEmpty(customer?.Phone1) ? customer?.Phone2 : customer.Phone1;

                // Get the sms subscription status
                if (customer != null)
                {
                    string statusMessage;
                    var statusCode = GetSmsSubscriptionStatus(clientUser.ClientID, customer, out statusMessage);
                    if (!string.IsNullOrEmpty(statusCode))
                    {
                        response.Message = statusMessage;
                        response.SmsWarningCode = MapSmsStatusCode(statusCode);
                }
                    
                }
                response.Customer = customerInsight;

            }
            else
            {
                response.Message = "No data found";
            }

            return response;
        }
        
        
        public SubscriptionPostResponse SubscribeInsights(ClientUser clientUser, SubscriptionPostRequest post)
        {
            var logModel = new LogModel
            {
                Module = Enums.Module.SubscriptionApi,
                ClientId = Convert.ToString(clientUser.ClientID),
                CustomerId = post.Customer.Id,
                AccountId = post.Customer.Insights[0].AccountId
            };

            var response = new SubscriptionPostResponse();
            var valid = true;
            var insightErrorMsg = string.Empty;
            var subscribeList = GetSubscribeList(clientUser.ClientID, post.Customer.Id, post.Customer.Insights, ref valid, 
                post.Customer.Email, post.Customer.Phone, ref insightErrorMsg);

            if (valid)
            {
                if (!string.IsNullOrEmpty(insightErrorMsg))
                {
                    response.Message = insightErrorMsg;
                    response.StatusType = StatusType.ModelErrors;
                    return response;
                }

                if (subscribeList == null || subscribeList.Count <= 0)
                {
                    return response;
                }

                var hasSmsChannel = subscribeList.Exists(
                        s =>
                            !string.IsNullOrEmpty(s.Channel) &&
                        (string.Equals(s.Channel, Enums.GetDescriptionOfEnum(Enums.SubscriptionChannel.SMS),
                            StringComparison.CurrentCultureIgnoreCase) || string.Equals(s.Channel, Enums.GetDescriptionOfEnum(Enums.SubscriptionChannel.EmailAndSMS),
                            StringComparison.CurrentCultureIgnoreCase)));

                    // update customer info
                    var reOptIn = false;
                    UpdateCustomerInfo(clientUser.ClientID, post, hasSmsChannel, ref reOptIn);

                var subscriptionManager = _container.Resolve<ISubscription>();
                    var tasks =
                        subscribeList.Select(
                            subscription =>
                                Task.Run(
                                    () =>
                                        subscriptionManager.InsertSubscriptionAsync(subscription, false, false)
                                            .ConfigureAwait(false))).ToList();

                    Task.WhenAll(tasks);

                if (!subscribeList.Exists(s => s.IsDoubleOptIn) && !reOptIn)
                {
                    return response;
                }

                var subscriptionFacadeManager = _container.Resolve<ISubscriptionFacade>(new ParameterOverride("logModel", logModel));
                TrumpiaRequestDetailModel smsRequeststatus = null;
                if (!reOptIn)
                {
                    var doubleOptinList = subscribeList.FindAll(s => s.IsDoubleOptIn);

                    foreach (var subscription in doubleOptinList)
                    {
                        smsRequeststatus = subscriptionFacadeManager.DoubleOptin(subscription);
                    }
                }
                else
                {
                    var doubleOptinList = subscribeList.FindAll(s => string.IsNullOrEmpty(s.InsightTypeName));
                    foreach (var subscription in doubleOptinList)
                    {
                        smsRequeststatus = subscriptionFacadeManager.DoubleOptin(subscription);
                    }
                }

                if (smsRequeststatus != null && !string.IsNullOrEmpty(smsRequeststatus.StatusCode))
                {
                    response.SmsWarningCode = MapSmsStatusCode(smsRequeststatus.StatusCode);
                    response.Message = smsRequeststatus.ErrorMessage;
                }
            }
            else
            {
                response.Message = insightErrorMsg;
                response.StatusType = StatusType.ModelErrors;
            }

            return response;
        }

        public void UpdateCustomerInfo(int clientId, SubscriptionPostRequest post, bool hasSmsChannel, ref bool isReOptIn)
        {
            // get customer email and phone
            var updateCustomer = false;
            var customer = GetCustomerInfo(clientId, post.Customer.Id);

            var email = post.Customer.Email;
            var phone = post.Customer.Phone;

            string newPhoneNumber;
            if (Extensions.IsValidEmail(email) && Extensions.IsValidPhone(phone, out newPhoneNumber))
            { 

                var customerManager = _container.Resolve<ICustomer>();

                if (!string.IsNullOrEmpty(email) && 
                    !string.Equals(email, customer.EmailAddress, StringComparison.CurrentCultureIgnoreCase))
                {
                    customer.EmailAddress = email;
                    updateCustomer = true;
                }

                if (!string.IsNullOrEmpty(newPhoneNumber) &&
                     !customer.Phone1.Equals(newPhoneNumber, StringComparison.InvariantCultureIgnoreCase))
                {
                    customer.Phone1 = newPhoneNumber;
                    updateCustomer = true;
                    // clear the trumpia info for the new number
                    customer.TrumpiaRequestDetailPkRk = string.Empty;
                    customer.TrumpiaSubscriptionId = string.Empty;
                }

                if (hasSmsChannel)
                {
                    var clientConfigFacade = _container.Resolve<IClientConfigFacade>();
                    // sms settings
                    var settings = clientConfigFacade.GetClientSettings(clientId);
                    var smsApiKey = settings.TrumpiaApiKey;
                    var smsUserName = settings.TrumpiaUserName;
                    var smsContactList = settings.TrumpiaContactList;

                    if (string.IsNullOrEmpty(customer.TrumpiaSubscriptionId) &&
                    !string.IsNullOrEmpty(customer.Phone1))
                    {
                        customerManager.CreateSubscriptionAndUpdateCustomer(customer, smsApiKey, smsUserName, smsContactList);
                        updateCustomer = false;
                    }
                    else if (!string.IsNullOrEmpty(customer.TrumpiaSubscriptionId) && !string.IsNullOrEmpty(customer.Phone1))
                    {
                        // check if the customer is opt-out by text, if so, re-register 
                        customerManager.SmsSubscirptionCheckAndUpdate(customer, smsApiKey, smsUserName, smsContactList, ref isReOptIn);
                    }
                }

                if (updateCustomer)
                {
                    UpdateCustomerAsync(customer);
                }
            }
        }

        public List<AO.Models.SubscriptionModel> GetAllInsightPrograms(int clientId, string customerId, string accountId)
        {
            var logModel = new LogModel
            {
                Module = Enums.Module.SubscriptionApi,
                ClientId = Convert.ToString(clientId),
                CustomerId = customerId,
                AccountId = accountId
            };

            ISubscriptionFacade subscriptionManager = _container.Resolve<ISubscriptionFacade>(new ParameterOverride("logModel", logModel));
            var insights = subscriptionManager.GetDefaultListWithSubscribedPrograms(clientId, customerId, accountId);

            if (insights != null && insights.Count > 0)
                return insights;
            else
                return null;
            
        }
        /// <summary>
        /// get customer info for customer id from business layer
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public CustomerModel GetCustomerInfo(int clientId, string customerId)
        {

            CustomerModel customer = null;
            var customerTask = GetCustomerAsync(clientId, customerId);
            //bool task = customerTask.Wait(60000);
            Task.WaitAll(customerTask);
            if (customerTask.Status == TaskStatus.RanToCompletion)
            {
                customer = customerTask.Result;
            }
            return customer;
        }

        /// <summary>
        /// Get a list of Subscriptions
        /// </summary>
        /// <param name="clientId">The Id of the client.</param>
        /// <param name="customerId">The Id of the Customer.</param>
        /// <param name="insights">Known Insights associated with the Customer.</param>
        /// <param name="validInsight">True, of the Insights are valid.</param>
        /// <returns>A List of Subscriptions</returns>
        public List<AO.Models.SubscriptionModel> GetSubscribeList(int clientId, string customerId,
            List<SubscribedInsight> insights, ref bool validInsight)
        {
            var insightErrMsg = string.Empty;
            return GetSubscribeList(clientId, customerId, insights, ref validInsight, 
                string.Empty, string.Empty, ref insightErrMsg);
        }

        /// <summary>
        /// Get a list of Subscriptions
        /// </summary>
        /// <param name="clientId">The Id of the client.</param>
        /// <param name="customerId">The Id of the Customer.</param>
        /// <param name="insights">Known Insights associated with the Customer.</param>
        /// <param name="validInsight">True, of the Insights are valid.</param>
        /// <param name="custEmail">The Email Address of the Customer</param>
        /// <param name="custPhone">The Phone number of the Customer</param>
        /// <param name="insightErrMsg">The specific error found with the Insights.</param>
        /// <returns>A List of Subscriptions</returns>
        public List<AO.Models.SubscriptionModel> GetSubscribeList(int clientId, string customerId, List<SubscribedInsight> insights, 
            ref bool validInsight, string custEmail, string custPhone, ref string insightErrMsg)
        {
            var logModel = new LogModel
            {
                Module = Enums.Module.SubscriptionApi,
                ClientId = Convert.ToString(clientId),
                CustomerId = customerId,
                AccountId = insights[0].AccountId
            };
            InitializedSubscriptionMapper(clientId, customerId);

            List<AO.Models.SubscriptionModel> subscribeInsights = null;

            if (insights.Count <= 0)
            {
                return null;
            }

                // Convert the known insights into a list.
                var insightList = Mapper.Map<List<SubscribedInsight>, List<AO.Models.SubscriptionModel>>(insights);

                insightList.ForEach(i =>
                {
                    if (string.IsNullOrEmpty(i.Channel)) return;
                    if (i.Channel.Equals(Enums.GetDescriptionOfEnum(Enums.SubscriptionChannel.Email),
                        StringComparison.InvariantCultureIgnoreCase))
                    {
                        i.Channel = Enums.GetDescriptionOfEnum(Enums.SubscriptionChannel.Email);
                    }
                    else if (i.Channel.Equals(Enums.GetDescriptionOfEnum(Enums.SubscriptionChannel.SMS),
                        StringComparison.InvariantCultureIgnoreCase))
                    {
                        i.Channel = Enums.GetDescriptionOfEnum(Enums.SubscriptionChannel.SMS);
                    }
                    else if (i.Channel.Equals(Enums.GetDescriptionOfEnum(Enums.SubscriptionChannel.EmailAndSMS),
                        StringComparison.InvariantCultureIgnoreCase))
                    {
                        i.Channel = Enums.GetDescriptionOfEnum(Enums.SubscriptionChannel.EmailAndSMS);
                    }
                    else if (i.Channel.Equals(Enums.GetDescriptionOfEnum(Enums.SubscriptionChannel.File),
                        StringComparison.InvariantCultureIgnoreCase))
                    {
                        i.Channel = Enums.GetDescriptionOfEnum(Enums.SubscriptionChannel.File);
                    }
                });

                // Get the account associated with the insights
                var accountId = insightList[0].AccountId;

                // Get the Subscription Manager from dependency injection.
                var subscriptionManager = _container.Resolve<ISubscriptionFacade>(new ParameterOverride("logModel", logModel));

            // Validate the insights.
            var invalidInsights = subscriptionManager.ValidateInsightList(clientId, insightList, 
                    custEmail, custPhone, ref insightErrMsg);

                // remove the invalid subscription
                if (invalidInsights.Count > 0)
                {
                    insightList.RemoveAll(
                        s =>
                            invalidInsights.Exists(
                                i =>
                                    string.Equals(i.ProgramName, s.ProgramName, StringComparison.CurrentCultureIgnoreCase) &&
                                    string.Equals(i.InsightTypeName, s.InsightTypeName, StringComparison.CurrentCultureIgnoreCase)));

                    validInsight = false;
                }

                if (validInsight)
                {
                    subscribeInsights = subscriptionManager.GetSubscribeList(clientId, customerId, accountId,
                    insightList, ref insightErrMsg);
                }

            return subscribeInsights;
        }

        /// <summary>
        /// Local Content grabber.
        /// </summary>
        /// <param name="response"></param>
        /// <param name="clientUser"></param>
        /// <param name="locale"></param>
        public void ApplyContent(SubscriptionResponse response, ClientUser clientUser, string locale)
        {
            //content
            var cf = new ContentModelFactoryContentful();
            var cp = cf.CreateContentProvider(clientUser.ClientID);
            var contentLocale = cp.GetContentLocale(locale);

            //get label content and configuration
            response.Content = new Content
            {
                TextContent = cp.GetContentTextContent("subscription", contentLocale, string.Empty),
                Configuration = cp.GetContentConfiguration("subscription", string.Empty),
                ConfigurationBulk = cp.GetContentClientConfigurationBulk("system", string.Empty),
                Commodity = cp.GetContentCommodity(string.Empty),
                Measurement = cp.GetContentMeasurement(string.Empty),
                UOM = cp.GetContentUom(string.Empty),
                Currency = cp.GetContentCurrency(string.Empty),
                Action = cp.GetContentAction(string.Empty)
            };
        }

        /// <summary>
        /// return customer info async from business layer
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        private async Task<CustomerModel> GetCustomerAsync(int clientId, string customerId)
        {

            var customerManager = _container.Resolve<ICustomer>();
            var task = await Task.Factory.StartNew(() => customerManager.GetCustomerAsync(clientId, customerId)).ConfigureAwait(false);

            return task.Result;

        }

        private void UpdateCustomerAsync(CustomerModel customer)
        {
            var customerManager = _container.Resolve<ICustomer>();

            var customerTask =
                Task.Run(() => customerManager.InsertOrMergeCustomerAsync(customer).ConfigureAwait(false));
            Task.WhenAll(customerTask);
        }

        /// <summary>
        /// Get Sms subscription status
        /// Used by Get Action
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="customer"></param>
        /// <param name="statusMessage"></param>
        /// <returns></returns>
        private string GetSmsSubscriptionStatus(int clientId, CustomerModel customer,  out string statusMessage)
        {
            var checkSubscription = true;
            var statusCode = string.Empty;
            statusMessage = string.Empty;
            if (!string.IsNullOrEmpty(customer.TrumpiaRequestDetailId))
            {
                statusCode = GetTrumpiaRequestDetail(customer.TrumpiaRequestDetailId, out statusMessage);

                var customerManager = _container.Resolve<ICustomer>();
                var clientConfigFacade = _container.Resolve<IClientConfigFacade>();

                // sms settings
                var settings = clientConfigFacade.GetClientSettings(clientId);
                var smsApiKey = settings.TrumpiaApiKey;
                var smsUserName = settings.TrumpiaUserName;
                var smsContactList = settings.TrumpiaContactList;

                if (!string.IsNullOrEmpty(statusCode))
                {
                    if (string.IsNullOrEmpty(customer.TrumpiaSubscriptionId) &&
                        !string.IsNullOrEmpty(customer.Phone1))
                    {
                        checkSubscription = false;
                        var update = customerManager.CreateSubscriptionAndUpdateCustomer(customer, smsApiKey,
                            smsUserName, smsContactList);
                        if (update)
                        {
                            if (!string.IsNullOrEmpty(customer.TrumpiaRequestDetailId) &&
                                !string.IsNullOrEmpty(customer.TrumpiaSubscriptionId))
                            {
                                statusCode = string.Empty;
                                statusMessage = string.Empty;
                            }
                        }
                    }
                }
                if(checkSubscription)
                {
                    // check if user blocks the number before the next notification occurs
                    if (!string.IsNullOrEmpty(customer.TrumpiaSubscriptionId) && !string.IsNullOrEmpty(customer.Phone1))
                    {
                        var logModel = new LogModel
                        {
                            Module = Enums.Module.SubscriptionApi,
                            ClientId = Convert.ToString(clientId),
                            CustomerId = customer.CustomerId
                        };
                        // Get the Subscription Manager from dependency injection.
                        var subscriptionManager = _container.Resolve<ISubscriptionFacade>(new ParameterOverride("logModel", logModel));

                        // check if the subscription is still valid
                        var trumpiaSubscriptionStatus = subscriptionManager.TrumpiaSubscriptionCheck(customer.TrumpiaRequestDetailId,
                            customer.TrumpiaSubscriptionId, smsApiKey, smsUserName);

                        if (trumpiaSubscriptionStatus != null)
                        {
                            // if subscription is invalid, remove subscription id from customer
                            if (!string.IsNullOrEmpty(trumpiaSubscriptionStatus.StatusCode))
                            {
                                customer.TrumpiaSubscriptionId = string.Empty;
                                UpdateCustomerAsync(customer);
                            }
                            
                            statusCode = trumpiaSubscriptionStatus.StatusCode;
                            statusMessage = trumpiaSubscriptionStatus.ErrorMessage;
                        }
                    }
                }
            }
            
            return statusCode;
        }

        /// <summary>
        /// get the status of the trumpia request
        /// </summary>
        /// <param name="trumpiaRequestId"></param>
        /// <param name="smsMessage"></param>
        /// <returns></returns>
        private string GetTrumpiaRequestDetail(string trumpiaRequestId, out string smsMessage)
        {
            string smsStatusCode;
            var trumpiaRequestDetail = _container.Resolve<ITrumpiaRequestDetail>();
            var entity = trumpiaRequestDetail.GetTrumpiaRequestDetail(trumpiaRequestId);
            smsStatusCode = entity?.StatusCode;
            smsMessage = entity?.ErrorMessage;
            return smsStatusCode;
        }


        /// <summary>
        /// Map known subscriptions to a customer insight object.
        /// </summary>
        /// <param name="subscriptions">The known subscriptions.</param>
        /// <param name="customerId">The customer's id</param>
        /// <returns>The Customer Insight object.</returns>
        private static CustomerInsight MapSubscriptionsToCustomerInsight(List<AO.Models.SubscriptionModel> subscriptions, string customerId)
        {
            // Creater customer insight object.
            var customerInsight = new CustomerInsight
            {
                Id = customerId,
                Accounts = new List<AccountInsight>()
            };

            // map service level subscription
            var serviceSubscriptons = subscriptions.FindAll(s => !string.IsNullOrEmpty(s.ServiceContractId));
            
            foreach (var subscription in serviceSubscriptons)
            {

                var accountId = subscription.AccountId;
                var premiseId = subscription.PremiseId;

                // Create Account insight
                var accountInsight = customerInsight.Accounts.Find(a => a.Id == accountId);
                if (accountInsight == null)
                {
                    accountInsight = new AccountInsight
                    {
                        Id = accountId,
                        Premises = new List<PremiseInsight>()                   
                    };
                    customerInsight.Accounts.Add(accountInsight);
                }

                // Create Premise insight
                var premiseInsight = accountInsight.Premises.Find(p => p.Id == premiseId);
                if (premiseInsight == null)
                {
                    premiseInsight = new PremiseInsight
                    {
                        Id = premiseId,
                        Services = new List<ServiceInsight>()
                    };
                    accountInsight.Premises.Add(premiseInsight);
                }

                // Create Service insight
                var serviceInsight = premiseInsight.Services.Find(s => s.Id == subscription.ServiceContractId);
                if (serviceInsight == null)
                {
                    serviceInsight = new ServiceInsight
                    {
                        Id = subscription.ServiceContractId,
                        //ContractId = subscription.ServiceContractId,
                        CommodityKey = subscription.CommodityKey,
                        Insights = new List<Insight>()
                    };
                    premiseInsight.Services.Add(serviceInsight);
                }

                serviceInsight.Insights.Add(MapSubscriptionToInsight(subscription, SubscriptionLevelType.service));
            }

            // map account level subscripton
            var accountSubscriptons = subscriptions.FindAll(s => string.IsNullOrEmpty(s.ServiceContractId));
            foreach (var subscription in accountSubscriptons)
            {
                var accountInsight = customerInsight.Accounts.Find(a => a.Id == subscription.AccountId);
                if (accountInsight == null)
                {
                    accountInsight = new AccountInsight
                    {
                        Id = subscription.AccountId
                    };
                    customerInsight.Accounts.Add(accountInsight);
                }
                if (accountInsight.Insights == null)
                {
                    accountInsight.Insights = new List<Insight>();
                }

                accountInsight.Insights.Add(MapSubscriptionToInsight(subscription, SubscriptionLevelType.account));
            }

            return customerInsight;
        }

        /// <summary>
        /// Map a Subscription object to an Insights object.
        /// </summary>
        /// <param name="subscription"></param>
        /// <param name="subscriptionLevelType"></param>
        /// <returns></returns>
        private static Insight MapSubscriptionToInsight(AO.Models.SubscriptionModel subscription, SubscriptionLevelType subscriptionLevelType)
        {
            // Create the Insights object.
                var insight = new Insight
                {
                    IsSelected = subscription.IsSelected,
                    Name = string.Format(InsightKey, subscription.ProgramName.ToLower(), subscription.InsightTypeName.ToLower()),
                    Level = subscriptionLevelType.ToString(),
                    AllowedChannels = subscription.AllowedChannels.ToLower(),
                    Channel = subscription.Channel.ToLower(),
                UomKey = subscription.UomId.HasValue ? Enums.GetDescriptionOfEnum(((Enums.UomType)subscription.UomId)).ToLower() : null,
                    IsEmailOptInCompleted = subscription.IsEmailOptInCompleted,
                    IsSmsOptInCompleted = subscription.IsSmsOptInCompleted
                };

            // No threshold values then return insight.
            if (!subscription.SubscriberThresholdValue.HasValue)
            {
                return insight;
            }

            // Add threshold values.
            if (subscription.ThresholdMax.HasValue)
                {
                        insight.ThresholdMax = Convert.ToDecimal(subscription.ThresholdMax.Value);
            }
            if (subscription.ThresholdMin.HasValue)
            {
                        insight.ThresholdMin = Convert.ToDecimal(subscription.ThresholdMin.Value);
            }
            if (!string.IsNullOrEmpty(subscription.ThresholdType))
            {
                        insight.ThresholdType = subscription.ThresholdType.ToLower();
                }

            insight.Threshold = Convert.ToDecimal(subscription.SubscriberThresholdValue.Value);

            // Return completed insight
            return insight;
        }


        private static void InitializedSubscriptionMapper(int clientId, string customerId)
        {
            AutoMapperConfig.ForAllUnmappedMembers(Mapper.CreateMap<SubscribedInsight, AO.Models.SubscriptionModel>()
                    .ForMember(dest => dest.ClientId, opts => opts.UseValue(clientId))
                    .ForMember(dest => dest.CustomerId, opts => opts.UseValue(customerId))
                    .ForMember(dest => dest.AccountId, opts => opts.MapFrom(src => src.AccountId))
                    //.ForMember(dest => dest.ServicePointId, opts => opts.MapFrom(src => src.Level == SubscriptionLevelType.service.ToString() ? src.ServiceId : string.Empty))
                    .ForMember(dest => dest.ServiceContractId, opts => opts.MapFrom(src => src.Level == SubscriptionLevelType.service.ToString() ? src.ServiceId : string.Empty))
                    .ForMember(dest => dest.PremiseId, opts => opts.MapFrom(src => src.Level == SubscriptionLevelType.service.ToString() ? src.PremiseId : string.Empty))
                    .ForMember(dest => dest.IsSelected, opts => opts.MapFrom(src => src.IsSelected))
                    .ForMember(dest => dest.ProgramName, opts => opts.MapFrom(src => src.Name.Split('.')[0]))
                    .ForMember(dest => dest.InsightTypeName, opts => opts.MapFrom(src => src.Name.Split('.')[1]))
                    .ForMember(dest => dest.SubscriberThresholdValue, opts => opts.MapFrom(src => src.Threshold.HasValue ? (double?)src.Threshold.Value : null))
                    .ForMember(dest => dest.Channel, opts => opts.MapFrom(src => src.Channel)), o => o.Ignore());
        }
        
        /// <summary>
        /// map third party sms status code to API status code
        /// </summary>
        /// <param name="smsStatusCode"></param>
        /// <returns></returns>
        private static string MapSmsStatusCode(string smsStatusCode)
        {
            var status = string.Empty;
            if(!string.IsNullOrEmpty(smsStatusCode))
            {
                switch (smsStatusCode)
                {
                    case "MPSE0501":
                        status = SmsBlockedNumberWarnings;
                        break;
                    case "MPSE2201":
                        status = SmsInvalidNumberFormatWarnings;
                        break;
                    case "MPSE2303":
                        status = SmsInvalidSubscriptionWarnings;
                        break;
                    default:
                        status = SmsOtherWarnings;
                        break;
                }
            }
            return status;
        }
        
    }
}