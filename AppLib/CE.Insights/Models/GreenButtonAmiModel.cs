﻿//using CE.ContentModel;
using CE.GreenButton;
using CE.Insights.Helpers;
using CE.Models;
using CE.Models.Insights;
//using CE.Models.Insights.Types;
using Microsoft.Practices.Unity;
using System;
//using System.Collections;
using System.Collections.Generic;
using System.Linq;
using AO.BusinessContracts;
using AutoMapper;
using CE.AO.Business;
using CE.AO.Models;
//using CE.Infrastructure;
using CE.Models.Insights.Types;
using Enums = CE.AO.Utilities.Enums;

namespace CE.Insights.Models
{
    /// <summary>
    /// Ami model
    /// </summary>
    public class GreenButtonAmiModel
    {
        private GreenButton.GreenButtonConfig _greenButtonSettings;
        private GreenButtonUtil _greenButtonUtil;
        private IUnityContainer _container;

        public GreenButtonAmiModel(int clientId, string locale, IUnityContainer unityContainer)
        {
            _greenButtonSettings = GreenButtonHelper.GetGreenButtonSettings(clientId, locale);
            _greenButtonUtil = new GreenButtonUtil(clientId, _greenButtonSettings);
            _container = unityContainer;
        }

        /// <summary>
        /// Testability, passed in setting and repository so not relying on content configuration and data access through sql server
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="greenButtonSettings"></param>
        /// <param name="insightsEfRepository"></param>
        /// <param name="unityContainer"></param>
        public GreenButtonAmiModel(int clientId, GreenButton.GreenButtonConfig greenButtonSettings, IInsightsEFRepository insightsEfRepository, IUnityContainer unityContainer)
        {
            _greenButtonSettings = greenButtonSettings;
            _container = unityContainer;
            _greenButtonUtil = new GreenButtonUtil(clientId, _greenButtonSettings, insightsEfRepository);
        }

        /// <summary>
        /// get bill in green button format
        /// </summary>
        /// <param name="clientUser"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public ExportResult GetGreenButtonAmi(ClientUser clientUser, GreenButtonAmiRequest request)
        {
            ExportResult result = null;
            // if output format is not provided, set it to zip by default
            if (string.IsNullOrEmpty(request.OutputFormat))
            {
                request.OutputFormat = GreenButtonOutputFormatType.Zip.ToString();
            }

            var meterList = request.MeterIds.Split(',').ToList();

            // validate the output format; if xml but there're more than 1 meter in MeterIds list, then throw error
            // otherwise continue
            if (
                request.OutputFormat.Equals(GreenButtonOutputFormatType.Xml.ToString(),
                    StringComparison.InvariantCultureIgnoreCase) && meterList.Count > 1
                )
            {
                result = new ExportResult
                {
                    ErrorMessage = "Only 1 meter is allowed for xml output format"

                };
                return result;
            }
            else
            {
                var meterConsumptionList = new List<MeterConsumption>();

                // get ami consumption data for all intevals
                foreach (var meterId in meterList)
                {
                    int amiUomId;
                    int amiCommodityId;
                    var consumptionList = GetConsumptionDataList(clientUser.ClientID, request.AccountId, meterId, request.StartDate,
                   request.EndDate, out amiUomId, out amiCommodityId);
                    if (consumptionList != null && consumptionList.Any())
                    {
                        var commodityType = (Enums.CommodityType)amiCommodityId;
                        var uom = ((Enums.UomType)amiUomId).ToString();
                        var convertUom = false;
                        double defaultConversionFactor = 1;
                        var useDefaultConversionFactor = false;

                        // currently, ami green button only supoprt electric and gas
                        // no conversion settings for electric
                        if (commodityType == Enums.CommodityType.Gas)
                        {
                            convertUom = _greenButtonSettings.ConvertGasUom;
                            defaultConversionFactor = _greenButtonSettings.DefaultGasConversionFactor;
                            useDefaultConversionFactor = _greenButtonSettings.UseDefaultGasConversionFactor;
                        }
                        var meterConsumption = new MeterConsumption
                        {
                            CommodityId = amiCommodityId,
                            ConvertUom = convertUom,
                            DefaultConversioniFactor = defaultConversionFactor,
                            IntervalConsumptionList = consumptionList,
                            MeterId = meterId,
                            UomKey = uom,
                            UseDefaultConversionFactor = useDefaultConversionFactor
                        };
                        meterConsumptionList.Add(meterConsumption);

                    }
                }
                if (meterConsumptionList.Any())
                {
                    // generate ami xml result
                    result = _greenButtonUtil.GenerateAmiXml(request.CustomerId, request.AccountId, request.PremiseId,
                        meterConsumptionList, request.StartDate, request.EndDate,
                        GreenButtonHelper.GetOutputFormat(request.OutputFormat));
                }
                return result;
            }
            
        }

        /// <summary>
        /// Get Ami reading for all intervals if available
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="accountId"></param>
        /// <param name="meterId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="amiUomId"></param>
        /// <param name="amiCommodityId"></param>
        public List<IntervalConsumptionData> GetConsumptionDataList(int clientId, string accountId, string meterId,DateTime startDate,DateTime endDate, out int amiUomId, out int amiCommodityId)
        {
            var consumptionDataList = new List<IntervalConsumptionData>();
            amiUomId = 99;
            amiCommodityId = 0;
            Mapper.CreateMap<RateModel.Reading, ConsumptionData>()
                .ForMember(dest => dest.DateTime, opts => opts.MapFrom(src => src.Timestamp))
                .ForMember(dest => dest.Value, opts => opts.MapFrom(src => (decimal)src.Quantity))
                .ForAllUnmappedMembers(o => o.Ignore());

            var intervalId = 15;
            // 5-mins TBD when it's available
            // get 15 mins interval 
            var fifteenAmiConsumptionReadings = GetAmiConsumptionReadingsByInterval(clientId, accountId, meterId, startDate, endDate, intervalId, ref amiUomId, ref amiCommodityId);
            if (fifteenAmiConsumptionReadings != null && fifteenAmiConsumptionReadings.Any())
            {
               var fifteenConsumptionReadings = new IntervalConsumptionData
               {
                   ConsumptionDataList = fifteenAmiConsumptionReadings,
                   ResolutionKey = ResolutionType.fifteen.ToString()
               };
                consumptionDataList.Add(fifteenConsumptionReadings);
            }

            // get 30 mins interval 
            intervalId = 30;
            var thirtyAmiConsumptionReadings = GetAmiConsumptionReadingsByInterval(clientId, accountId, meterId, startDate, endDate, intervalId, ref amiUomId, ref amiCommodityId);
            if (thirtyAmiConsumptionReadings != null && thirtyAmiConsumptionReadings.Any())
            {
                var thirtyConsumptionReadings = new IntervalConsumptionData
                {
                    ConsumptionDataList = thirtyAmiConsumptionReadings,
                    ResolutionKey = ResolutionType.thirty.ToString()
                };
                consumptionDataList.Add(thirtyConsumptionReadings);
            }

            // get 60 mins interval 
            intervalId = 60;
            var hourAmiConsumptionReadings = GetAmiConsumptionReadingsByInterval(clientId, accountId, meterId, startDate, endDate, intervalId, ref amiUomId, ref amiCommodityId);
            if (hourAmiConsumptionReadings != null && hourAmiConsumptionReadings.Any())
            {
                var hourConsumptionReadings = new IntervalConsumptionData
                {
                    ConsumptionDataList = hourAmiConsumptionReadings,
                    ResolutionKey = ResolutionType.hour.ToString()
                };
                consumptionDataList.Add(hourConsumptionReadings);
            }

            return consumptionDataList;
        }

        /// <summary>
        /// Get Ami consumption by the interval
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="accountId"></param>
        /// <param name="meterId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="intervalId"></param>
        /// <param name="amiUomId"></param>
        /// <param name="amiCommodityId"></param>
        /// <returns></returns>
        public List<ConsumptionData> GetAmiConsumptionReadingsByInterval(int clientId, string accountId, string meterId, DateTime startDate, DateTime endDate, int intervalId, ref int amiUomId, ref int amiCommodityId)
        {
            ITallAMI amiManager = _container.Resolve<ITallAMI>();

            List<ConsumptionData> consumptionList = null;
            IEnumerable<TallAmiModel> tallAmiModels = amiManager.GetAmiList(startDate, endDate, meterId, accountId, clientId);

            if(tallAmiModels != null)
            {
                int noOfDays;
                var amiModels = tallAmiModels as IList<TallAmiModel> ?? tallAmiModels.ToList();
                List<RateModel.Reading> readings = amiManager.GetReadings(amiModels.Where(t => t.IntervalType == intervalId), out noOfDays);

                if (readings != null && readings.Any())
                {
                    amiUomId = amiModels[0].UOMId;
                    amiCommodityId = amiModels[0].CommodityId;

                    consumptionList = new List<ConsumptionData>();

                    foreach (var reading in readings.OrderBy(r => r.Timestamp))
                    {
                        var consumption = Mapper.Map<RateModel.Reading, ConsumptionData>(reading);
                        consumptionList.Add(consumption);
                    }
                }
            }

            return consumptionList;
        }
    }
}