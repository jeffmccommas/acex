﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using CE.BillDisagg;
using CE.ContentModel;
using CE.Infrastructure;
using CE.Models;
using CE.Models.Insights;
using CE.Models.Insights.Types;

namespace CE.Insights.Models
{
    /// <summary>
    /// BillDisagg model
    /// </summary>
    public class BillDisaggModel
    {
        public void ExecuteBillDisaggModel(ClientUser clientUser, BillDisaggRequest request, BillDisaggResponse response)
        {
            DataTable dataTable = null;
            var customerId = request.CustomerId;
            var accountId = request.AccountId;
            var premiseId = request.PremiseId;

            var dal = new CE.BillDisagg.DataAccess.DataAccessLayer();

            if (!request.ForceCalc && !request.ForceBusiness)
            {
                dataTable = dal.GetBillDisagg(clientUser.ClientID, customerId, request.EndDate, accountId, premiseId, true);
            }

            //if there was nothing retrieved, execute an actual billdisagg!
            if (dataTable == null)
            {
                BillDisaggManagerFactory factory = null;
                IBillDisagg manager = null;
                BillDisaggResult result = null;

                factory = new BillDisaggManagerFactory();
                manager = factory.CreateBillDisaggManager(clientUser.ClientID);

                result = manager.ExecuteBillDisagg(customerId, accountId, premiseId, request.ForceBusiness);
                dataTable = result.FlatResultTable;
                dal.SaveBillDisagg(clientUser.ClientID, customerId, accountId, premiseId, dataTable);
            }

            // Now process the data table and Map result to response
            if (dataTable != null)
            {
                var data = PopulateBillDisaggDataList(dataTable);

                if (data != null)
                {
                    PopulateBillDisaggResponse(data, response);

                    if (response != null)
                    {
                        //Add DissaggDetails at EndUse level, from sums of Appliance DisaggDetails
                        SumApplianceCommoditiesUpToEndUse(response.Customer);

                        // Execute Measurements for each DissAgg detail at Appliance & EndUse level
                        if (!string.IsNullOrEmpty(request.MeasurementKeys))
                        {
                            var arr = request.MeasurementKeys.Split(CEConfiguration.ParameterListDelimiter);
                            var items = arr.Select(a => System.Enum.Parse(typeof(MeasurementType), a)).OfType<MeasurementType>().ToList();

                            CreateMeasurementsWithinBillDisagg(response.Customer, items);
                        }

                    }

                }

            }

        }

        public bool ExecuteTestDB(ClientUser clientUser, string insightsMetadata, string insightsDW, string insights)
        {
            var dal = new CE.BillDisagg.DataAccess.DataAccessLayer(insightsMetadata, insightsDW, insights);
            var isInsightsMetadataReachable = dal.TestConnInsightsMetadata();

            return (isInsightsMetadataReachable);
        }


        private class BillDisaggApplianceRow
        {
            public int ClientID { get; set; }
            public string CustomerID { get; set; }
            public string AccountID { get; set; }
            public DateTime BillStartDate { get; set; }
            public DateTime BillEndDate { get; set; }
            public string PremiseID { get; set; }
            public string EndUseKey { get; set; }
            public string ApplianceKey { get; set; }
            public string CommodityKey { get; set; }
            public decimal Usage { get; set; }
            public string UomKey { get; set; }
            public decimal Cost { get; set; }
            public string CurrencyKey { get; set; }
            public int DisaggStatusId { get; set; }
            public int Confidence { get; set; }
            public decimal ReconciliationRatio { get; set; }
        }


        private List<BillDisaggApplianceRow> PopulateBillDisaggDataList(DataTable tbl)
        {
            List<BillDisaggApplianceRow> rowList = new List<BillDisaggApplianceRow>();
            BillDisaggApplianceRow row = null;

            //tclientId = reader["ClientId"] as int? ?? default(int);
            //tcustomerId = reader["CustomerId"] as string;
            //taccountId = reader["AccountId"] as string;
            //tpremiseId = reader["PremiseId"] as string;
            //tbillDate = reader["BillDate"] as DateTime? ?? default(DateTime);
            //enduseKey = reader["EndUseKey"] as string;
            //applianceKey = reader["ApplianceKey"] as string;
            //commodityKey = reader["CommodityKey"] as string;
            //periodId = reader["PeriodId"] as int? ?? default(int);
            //usage = reader["Usage"] as decimal? ?? default(decimal);
            //cost = reader["Cost"] as decimal? ?? default(decimal);
            //uomKey = reader["UomKey"] as string;
            //currencyKey = reader["CurrencyKey"] as string;
            //confidence = reader["Confidence"] as int? ?? default(int);
            //recRatio = reader["ReconciliationRatio"] as decimal? ?? default(decimal);
            //statusId = reader["StatusId"] as int? ?? default(int);
            //newDate = reader["NewDate"] as DateTime? ?? default(DateTime);

            foreach (DataRow r in tbl.Rows)
            {
                row = new BillDisaggApplianceRow()
                {
                    ClientID = r["ClientId"] as int? ?? default(int),
                    CustomerID = r["CustomerId"] as string,
                    AccountID = r["AccountId"] as string,
                    PremiseID = r["PremiseId"] as string,
                    BillStartDate = r["BillDate"] as DateTime? ?? default(DateTime),
                    BillEndDate = r["BillDate"] as DateTime? ?? default(DateTime),
                    EndUseKey = r["EndUseKey"] as string,
                    ApplianceKey = r["ApplianceKey"] as string,
                    CommodityKey = r["CommodityKey"] as string,
                    Usage = r["Usage"] as decimal? ?? default(decimal),
                    UomKey = r["UomKey"] as string,
                    Cost = r["Cost"] as decimal? ?? default(decimal),
                    CurrencyKey = r["CurrencyKey"] as string,
                    DisaggStatusId = r["StatusId"] as int? ?? default(int),
                    Confidence = r["Confidence"] as int? ?? default(int),
                    ReconciliationRatio = r["ReconciliationRatio"] as decimal? ?? default(decimal)
                };

                rowList.Add(row);

            }

            return (rowList);
        }




        private void PopulateBillDisaggResponse(List<BillDisaggApplianceRow> dataList, BillDisaggResponse response)
        {
            // Check and see if there is any data.
            if (!dataList.Any())
            {
                return;
            }

            var customer = new Customer();
            Account account = null;
            Premise premise = null;
            EndUse enduse = null;
            Appliance appliance = null;
            DisaggDetail detail = null;
            CE.Models.Insights.DisaggStatus disaggStatus = null;

            var lastAccountId = string.Empty;
            var lastPremiseId = string.Empty;
            var lastEndUseKey = string.Empty;
            var lastApplianceKey = string.Empty;
            var lastCommodityKey = string.Empty;

            var accountAdded = false;
            var premiseAdded = false;
            var endUseAdded = false;
            var applianceAdded = false;
            var commodityAdded = false;

            foreach (var row in dataList)
            {

                var status = MapDisaggStatus(row.DisaggStatusId);
                disaggStatus = new CE.Models.Insights.DisaggStatus
                {
                    CommodityKey = ConvertCommodities(row.CommodityKey),
                    Status = status
                };

                if (lastAccountId != row.AccountID)
                {
                    // account
                    lastAccountId = row.AccountID;
                    if (account != null)
                    {
                        appliance.DisaggDetails.Add(detail);  //special
                        enduse.Appliances.Add(appliance);  //special
                        premise.EndUses.Add(enduse); //special
                        if(!premise.DisaggStatuses.Exists(s => s.CommodityKey == disaggStatus.CommodityKey))
                        {
                            premise.DisaggStatuses.Add(disaggStatus);
                        }
                        account.Premises.Add(premise); //special
                        customer.Accounts.Add(account);
                        accountAdded = true;
                    }

                    account = new Account()
                    {
                        Id = row.AccountID,
                        BillStartDate = row.BillStartDate,
                        BillEndDate = row.BillEndDate
                    };
                }


                if (lastPremiseId != row.PremiseID || accountAdded)
                {
                    // premise
                    lastPremiseId = row.PremiseID;
                    if (!accountAdded && premise != null)
                    {
                        appliance.DisaggDetails.Add(detail);  //special
                        enduse.Appliances.Add(appliance);  //special
                        premise.EndUses.Add(enduse); //special
                        if (!premise.DisaggStatuses.Exists(s => s.CommodityKey == disaggStatus.CommodityKey))
                        {
                            premise.DisaggStatuses.Add(disaggStatus);
                        }
                        account.Premises.Add(premise);
                        premiseAdded = true;
                    }

                    premise = new Premise()
                    {
                        Id = row.PremiseID,
                        DisaggStatuses = new List<CE.Models.Insights.DisaggStatus>()
                    };
                }                

                if (lastEndUseKey == string.Empty || premiseAdded || accountAdded || (lastEndUseKey != string.Empty && (lastEndUseKey != row.EndUseKey)))
                {
                    // enduse
                    lastEndUseKey = row.EndUseKey;
                    if (!accountAdded && !premiseAdded && enduse != null)
                    {
                        appliance.DisaggDetails.Add(detail);  //special
                        enduse.Appliances.Add(appliance);  //special
                        premise.EndUses.Add(enduse);
                        if (!premise.DisaggStatuses.Exists(s => s.CommodityKey == disaggStatus.CommodityKey))
                        {
                            premise.DisaggStatuses.Add(disaggStatus);
                        }
                        endUseAdded = true;
                    }

                    enduse = new EndUse()
                    {
                        Key = row.EndUseKey
                    };
                }

                if (lastApplianceKey == string.Empty || endUseAdded || premiseAdded || accountAdded || (lastApplianceKey != string.Empty && (lastApplianceKey != row.ApplianceKey)))
                {
                    // appliance
                    lastApplianceKey = row.ApplianceKey;
                    if (!accountAdded && !premiseAdded && !endUseAdded && appliance != null)
                    {
                        appliance.DisaggDetails.Add(detail); //special
                        enduse.Appliances.Add(appliance);
                        if (!premise.DisaggStatuses.Exists(s => s.CommodityKey == disaggStatus.CommodityKey))
                        {
                            premise.DisaggStatuses.Add(disaggStatus);
                        }
                        applianceAdded = true;
                    }

                    appliance = new Appliance()
                    {
                        Key = row.ApplianceKey
                    };
                }


                if (lastCommodityKey == string.Empty || applianceAdded || endUseAdded || premiseAdded || accountAdded || (lastCommodityKey != string.Empty && (lastCommodityKey != row.CommodityKey)))
                {
                    // disaggDetail (via commodity)
                    lastCommodityKey = row.CommodityKey;
                    if (!accountAdded && !premiseAdded && !endUseAdded && !applianceAdded && detail != null)
                    {
                        appliance.DisaggDetails.Add(detail);
                        if (!premise.DisaggStatuses.Exists(s => s.CommodityKey == disaggStatus.CommodityKey))
                        {
                            premise.DisaggStatuses.Add(disaggStatus);
                        }
                        commodityAdded = true;
                    }

                    detail = new DisaggDetail()
                    {
                        CommodityKey = row.CommodityKey,
                        UsageQuantity = row.Usage,
                        UsageUOMKey = row.UomKey,
                        CostAmount = row.Cost,
                        CostCurrencyKey = row.CurrencyKey,
                        Confidence = row.Confidence,
                        ReconciliationRatio = row.ReconciliationRatio
                    };
                }

                // reset all of these
                if (accountAdded) accountAdded = false;
                if (premiseAdded) premiseAdded = false;
                if (endUseAdded) endUseAdded = false;
                if (applianceAdded) applianceAdded = false;
                if (commodityAdded) commodityAdded = false;

            }

            // last items need to be added
            appliance.DisaggDetails.Add(detail);
            enduse.Appliances.Add(appliance);
            premise.EndUses.Add(enduse);
            if (!premise.DisaggStatuses.Exists(s => s.CommodityKey == disaggStatus.CommodityKey))
            {
                premise.DisaggStatuses.Add(disaggStatus);
            }
            account.Premises.Add(premise);
            customer.Accounts.Add(account);

            customer.Id = dataList[0].CustomerID;
            response.Customer = customer;
            response.ClientId = dataList[0].ClientID;

        }




        private void SumApplianceCommoditiesUpToEndUse(Customer customer)
        {
            var commodityElectric = "electric";
            var commodityGas = "gas";
            var commodityWater = "water";

            foreach (var a in customer.Accounts)
            {
                foreach (var p in a.Premises)
                {
                    foreach (var e in p.EndUses)
                    {
                        // electric DisaggDetail total at endUse level if any exists at appliance level
                        var electricCount = e.Appliances.Sum(ap => ap.DisaggDetails.FindAll(f => f.CommodityKey == commodityElectric).Count());
                        if (electricCount > 0)
                        {
                            var cost = e.Appliances.Sum(ap => ap.DisaggDetails.FindAll(f => f.CommodityKey == commodityElectric).Sum(dd => dd.CostAmount));
                            var usage = e.Appliances.Sum(ap => ap.DisaggDetails.FindAll(f => f.CommodityKey == commodityElectric).Sum(dd => dd.UsageQuantity));
                            var confidence = e.Appliances.Max(ap => ap.DisaggDetails.FindAll(f => f.CommodityKey == commodityElectric).Sum(dd => dd.Confidence));
                            var reconRatio = e.Appliances.Max(ap => ap.DisaggDetails.FindAll(f => f.CommodityKey == commodityElectric).Sum(dd => dd.ReconciliationRatio));

                            var dissagDetail = new DisaggDetail()
                            {
                                CommodityKey = commodityElectric,
                                CostAmount = cost,
                                UsageQuantity = usage,
                                CostCurrencyKey = "usd",
                                UsageUOMKey = "kwh",
                                Confidence = (int)confidence,
                                ReconciliationRatio = reconRatio
                            };

                            if (e.DisaggDetails == null)
                            {
                                e.DisaggDetails = new List<DisaggDetail>();
                            }

                            e.DisaggDetails.Add(dissagDetail);
                        }

                        //gas DisaggDetail; get uomkey from a disagg detail, since it may be therms, ccf, or mcf
                        var gasCount = e.Appliances.Sum(ap => ap.DisaggDetails.FindAll(f => f.CommodityKey == commodityGas).Count());
                        if (gasCount > 0)
                        {
                            var cost = e.Appliances.Sum(ap => ap.DisaggDetails.FindAll(f => f.CommodityKey == commodityGas).Sum(dd => dd.CostAmount));
                            var usage = e.Appliances.Sum(ap => ap.DisaggDetails.FindAll(f => f.CommodityKey == commodityGas).Sum(dd => dd.UsageQuantity));
                            var confidence = e.Appliances.Max(ap => ap.DisaggDetails.FindAll(f => f.CommodityKey == commodityGas).Sum(dd => dd.Confidence));
                            var reconRatio = e.Appliances.Max(ap => ap.DisaggDetails.FindAll(f => f.CommodityKey == commodityGas).Sum(dd => dd.ReconciliationRatio));
                            var uomkey = e.Appliances.Find(ap => ap.DisaggDetails.Any(f => f.CommodityKey == commodityGas)).DisaggDetails.Find(dd => dd.CommodityKey == commodityGas).UsageUOMKey;

                            var dissagDetail = new DisaggDetail()
                            {
                                CommodityKey = commodityGas,
                                CostAmount = cost,
                                UsageQuantity = usage,
                                CostCurrencyKey = "usd",
                                UsageUOMKey = uomkey,
                                Confidence = (int)confidence,
                                ReconciliationRatio = reconRatio
                            };

                            if (e.DisaggDetails == null)
                            {
                                e.DisaggDetails = new List<DisaggDetail>();
                            }

                            e.DisaggDetails.Add(dissagDetail);

                        }

                        //water DisaggDetail; get uomkey from a disagg detail, since it may be gallons or ccf
                        var waterCount = e.Appliances.Sum(ap => ap.DisaggDetails.FindAll(f => f.CommodityKey == commodityWater).Count());
                        if (waterCount > 0)
                        {
                            var cost = e.Appliances.Sum(ap => ap.DisaggDetails.FindAll(f => f.CommodityKey == commodityWater).Sum(dd => dd.CostAmount));
                            var usage = e.Appliances.Sum(ap => ap.DisaggDetails.FindAll(f => f.CommodityKey == commodityWater).Sum(dd => dd.UsageQuantity));
                            var confidence = e.Appliances.Max(ap => ap.DisaggDetails.FindAll(f => f.CommodityKey == commodityWater).Sum(dd => dd.Confidence));
                            var reconRatio = e.Appliances.Max(ap => ap.DisaggDetails.FindAll(f => f.CommodityKey == commodityWater).Sum(dd => dd.ReconciliationRatio));
                            var uomkey = e.Appliances.Find(ap => ap.DisaggDetails.Any(f => f.CommodityKey == commodityWater)).DisaggDetails.Find(dd => dd.CommodityKey == commodityWater).UsageUOMKey;

                            var dissagDetail = new DisaggDetail()
                            {
                                CommodityKey = commodityWater,
                                CostAmount = cost,
                                UsageQuantity = usage,
                                CostCurrencyKey = "usd",
                                UsageUOMKey = uomkey,
                                Confidence = (int)confidence,
                                ReconciliationRatio = reconRatio
                            };

                            if (e.DisaggDetails == null)
                            {
                                e.DisaggDetails = new List<DisaggDetail>();
                            }

                            e.DisaggDetails.Add(dissagDetail);

                        }
                    }
                }
            }
        }

        private void CreateMeasurementsWithinBillDisagg(Customer customer, List<MeasurementType> items)
        {
            foreach (var a in customer.Accounts)
            {
                foreach (var p in a.Premises)
                {
                    foreach (var e in p.EndUses)
                    {
                        foreach (var ap in e.Appliances)
                        {
                            if (ap.DisaggDetails != null)
                            {
                                CreateMeasurements(ap.DisaggDetails, items);
                            }

                        }

                        CreateMeasurements(e.DisaggDetails, items);
                    }
                }
            }
        }

        private void CreateMeasurements(List<DisaggDetail> disaggDetails, List<MeasurementType> items)
        {

            foreach (var dd in disaggDetails)
            {
                if (dd.Measurements == null) dd.Measurements = new List<Measurement>();

                foreach (var mt in items)
                {
                    switch (mt)
                    {
                        case MeasurementType.CO2:
                            dd.Measurements.Add(new Measurement()
                            {
                                Key = MeasurementType.CO2.ToString().ToLower(),
                                Quantity = dd.UsageQuantity * 1.5M,
                                Units = MeasurementUnitType.lbs
                            });

                            break;

                        case MeasurementType.Points:
                            dd.Measurements.Add(new Measurement()
                            {
                                Key = MeasurementType.Points.ToString().ToLower(),
                                Quantity = dd.UsageQuantity * 0.25M,
                                Units = MeasurementUnitType.pts
                            });
                            break;
                    }
                }
            }

        }

        /// <summary>
        /// Disagg Status.
        /// </summary>
        /// <param name="disaggStatus"></param>
        /// <returns></returns>
        private string MapDisaggStatus(int disaggStatus)
        {
            var s = string.Empty;

            switch (disaggStatus)
            {
                case 0:
                    s = "Unspecified";
                    break;
                case 1:
                    s = "ActualBills";
                    break;
                case 2:
                    s = "ComputedBills";
                    break;
                case 3:
                    s = "ModelOnly";
                    break;
                case 4:
                    s = "Failed";
                    break;

            }

            return (s);
        }

        private string ConvertCommodities(string commodity)
        {
            string s = string.Empty;
            switch (commodity)
            {
                case "electric":
                case "electricity":
                    s = EnergyModel.Enums.CommodityType.Electric.ToString();
                    break;
                case "gas":
                    s = EnergyModel.Enums.CommodityType.Gas.ToString();
                    break;
                case "oil":
                    s = EnergyModel.Enums.CommodityType.Oil.ToString();
                    break;
                case "propane":
                    s = EnergyModel.Enums.CommodityType.Propane.ToString();
                    break;
                case "water":
                    s = EnergyModel.Enums.CommodityType.Water.ToString();
                    break;
                case "wood":
                    s = EnergyModel.Enums.CommodityType.Wood.ToString();
                    break;
                case "hotwater":
                    s = EnergyModel.Enums.CommodityType.HotWater.ToString();
                    break;
                case "otherwater":
                    s = EnergyModel.Enums.CommodityType.OtherWater.ToString();
                    break;
                case "coal":
                    s = EnergyModel.Enums.CommodityType.Coal.ToString();
                    break;
                default:
                    s = EnergyModel.Enums.CommodityType.Unspecified.ToString();
                    break;
            }
            return (s);
        }

        public void ApplyContent(BillDisaggResponse response, ClientUser clientUser, string locale)
        {
            //content
            var cf = new ContentModelFactoryContentful();
            var cp = cf.CreateContentProvider(clientUser.ClientID);
            var contentLocale = cp.GetContentLocale(locale);

            //get label content and configuration
            response.Content = new Content
            {
                TextContent = cp.GetContentTextContent("billdisagg", contentLocale, string.Empty),
                Configuration = cp.GetContentConfiguration("billdisagg", string.Empty),
                EndUse = cp.GetContentEnduse(string.Empty),
                Appliance = cp.GetContentAppliance(string.Empty),
                Commodity = cp.GetContentCommodity(string.Empty),
                Measurement = cp.GetContentMeasurement(string.Empty),
                UOM = cp.GetContentUom(string.Empty),
                Currency = cp.GetContentCurrency(string.Empty)
            };
        }

    }
}