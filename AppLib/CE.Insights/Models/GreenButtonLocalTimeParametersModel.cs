﻿using System.Collections.Generic;
using CE.Models;
using CE.Models.Insights;
using CE.Models.Insights.GreenButtonConnect;
using CE.Models.Insights.Types;
using Microsoft.Practices.Unity;

namespace CE.Insights.Models
{
    public class GreenButtonLocalTimeParametersModel : GreenButtonBaseModel
    {
        /// <summary>
        /// constructor for local time parameters model
        /// </summary>
        public GreenButtonLocalTimeParametersModel(string accessToken, string locale, string greenButtonDomain, IUnityContainer container, IInsightsEFRepository insightsEfRepository) : base(accessToken, locale, greenButtonDomain, container, insightsEfRepository)
        {
        }

        /// <summary>
        /// Testability, passed in repository and unity container so not relying on data access through sql server and table storage
        /// </summary>
        /// <param name="greenButtonDomain"></param>
        /// <param name="authorizations"></param>
        /// <param name="greenButtonSettings"></param>
        /// <param name="unityContainer"></param>
        /// <param name="insightsEfRepository"></param>
        /// <param name="clientId"></param>
        public GreenButtonLocalTimeParametersModel(int clientId, string greenButtonDomain, List<GreenButtonAuthorization> authorizations, GreenButtonConfig greenButtonSettings, IUnityContainer unityContainer, IInsightsEFRepository insightsEfRepository): base(clientId, greenButtonDomain, authorizations, unityContainer,insightsEfRepository, greenButtonSettings)
        {
        }

        public GreenButtonResult GetAllLocalTimeParamatersGreenButton(string accessToken)
        {
            if (Authorizations == null || Authorizations.FindAll(a => a.AccessToken == accessToken).Count == 0)
                return new GreenButtonResult { Status = GreenButtonErrorType.InvalidAuthorization };

            // check accessToken type
            var accessTokenType = Authorizations.Find(a => a.AccessToken == accessToken).AccessTokenType;

            if (accessTokenType == GreenButtonAccessTokenType.Customer)
                Authorizations = Authorizations.FindAll(a => a.AccessToken == accessToken);

            GreenButtonSubscription finalizedSubscription = null;
            foreach (var authorization in Authorizations)
            {
                var subscriptionId = authorization.SubscriptionId;

                var scope = Authorizations.Find(a => a.SubscriptionId == subscriptionId).Scope;

                // Get Subscription info
                var subscription = InsightsEfRepository.GetGreenButtonAmiSubscription(subscriptionId);

                if (subscription != null)
                {
                    if (finalizedSubscription == null)
                        finalizedSubscription = subscription;

                    // Get usage point
                    var usagePoints = InsightsEfRepository.GetGreenButtonAmiUsagePoints(subscriptionId, scope);

                    if (!scope.FunctionBlocks.Exists(fb => fb == GreenButtonFunctionBlockType.MulitpleUsagePoints) && usagePoints.Count > 1)
                        return new GreenButtonResult { Status = GreenButtonErrorType.MultipleUsageNotSupportErr };

                    if (usagePoints.Count > 0)
                    {
                        foreach (var usagePoint in usagePoints)
                        {
                            var status = GetUsagePoint(subscription.AccountId, scope, usagePoint, null, null, false);
                           if (status != GreenButtonErrorType.NoError)
                                return new GreenButtonResult { Status = status };
                            if (finalizedSubscription.UsagePoints == null)
                                finalizedSubscription.UsagePoints = new List<GreenButtonUsagePoint> { usagePoint };
                            else
                                finalizedSubscription.UsagePoints.Add(usagePoint);
                        }
                    }

                }

            }
            if (finalizedSubscription == null)
                return new GreenButtonResult { Status = GreenButtonErrorType.InvalidSubscriptionErr };
            return GreenButtonModel.GenerateGreenButton(finalizedSubscription, GreenButtonEntryType.LocalTimeParameters);
        }

        public GreenButtonResult GetLocalTimeParamaterGreenButton(string accessToken, int localTimeParamId)
        {
            if (Authorizations == null || Authorizations.FindAll(a => a.AccessToken == accessToken).Count == 0)
                return new GreenButtonResult { Status = GreenButtonErrorType.InvalidAuthorization };

            // check accessToken type
            var accessTokenType = Authorizations.Find(a => a.AccessToken == accessToken).AccessTokenType;

            if (accessTokenType == GreenButtonAccessTokenType.Customer)
                Authorizations = Authorizations.FindAll(a => a.AccessToken == accessToken);

            GreenButtonSubscription finalizedSubscription = null;
            foreach (var authorization in Authorizations)
            {
                var subscriptionId = authorization.SubscriptionId;

                var scope = Authorizations.Find(a => a.SubscriptionId == subscriptionId).Scope;

                // Get Subscription info
                var subscription = InsightsEfRepository.GetGreenButtonAmiSubscription(subscriptionId);

                if (subscription != null)
                {
                    if (finalizedSubscription == null)
                        finalizedSubscription = subscription;

                    // Get usage point
                    var usagePoints = InsightsEfRepository.GetGreenButtonAmiUsagePoints(subscriptionId, scope);

                    if (!scope.FunctionBlocks.Exists(fb => fb == GreenButtonFunctionBlockType.MulitpleUsagePoints) && usagePoints.Count > 1)
                        return new GreenButtonResult { Status = GreenButtonErrorType.MultipleUsageNotSupportErr };

                    if (usagePoints.Count > 0)
                    {
                        foreach (var usagePoint in usagePoints)
                        {
                            var status = GetUsagePoint(subscription.AccountId, scope, usagePoint, null, null, false);
                            if (status != GreenButtonErrorType.NoError)
                                return new GreenButtonResult { Status = status };
                            if (usagePoint.TimeConfigurationId == localTimeParamId)
                            {
                                if (finalizedSubscription.UsagePoints == null)
                                    finalizedSubscription.UsagePoints = new List<GreenButtonUsagePoint> { usagePoint };
                                else
                                    finalizedSubscription.UsagePoints.Add(usagePoint);
                            }
                        }
                    }

                }

            }
            if (finalizedSubscription == null)
                return new GreenButtonResult { Status = GreenButtonErrorType.InvalidSubscriptionErr };
            if(finalizedSubscription.UsagePoints == null)
                return new GreenButtonResult { Status = GreenButtonErrorType.InvalidLocalTimeParamErr };
            return GreenButtonModel.GenerateGreenButton(finalizedSubscription, GreenButtonEntryType.LocalTimeParameters);
        }
    }
}