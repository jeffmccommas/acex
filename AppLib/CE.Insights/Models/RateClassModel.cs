﻿using AutoMapper;
using CE.Models;
using MI = CE.Models.Insights;
using CE.ContentModel;
using CE.Infrastructure;
using CE.RateModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CE.Insights.Models
{
    /// <summary>
    /// Rate Model
    /// </summary>
    public class RateClassModel
    {
        private IRateModel rateModel;

        public RateClassModel()
        {
            rateModel = GetRateModel();
            InitializeRateMapper();
        }

        public RateClassModel(bool logOverride)
        {
            rateModel = GetRateModel(logOverride);
            InitializeRateMapper();
        }

        public MI.RateResponse GetRate(ClientUser clientUser, MI.RateRequest request)
        {
            var response = new MI.RateResponse();
            MI.RateClass rateClass = null;
            MI.RateDetails rateDetails = null;
            var startDate = DateTime.MinValue;
            var endDate = DateTime.MinValue;
            var projectedEndDate = DateTime.MinValue;
            var NoError = true;
            var useProjectedEnd = false;


            if (request.IncludeRateDetails)
            {
                rateDetails = GetDetailedRateInfo(clientUser.RateCompanyID, request.RateClass, request.Date, ref rateClass);

                if (rateDetails != null && rateClass != null)
                {
                    if (request.IncludeFinalizedTierBoundaries)
                    {
                        if (request.UseProjectedEndDateToFinalizeTier)
                        {
                            if (request.StartDate != DateTime.MinValue && request.EndDate != DateTime.MinValue && request.ProjectedEndDate != DateTime.MinValue)
                            {
                                startDate = request.StartDate;
                                endDate = request.EndDate;
                                projectedEndDate = request.ProjectedEndDate;
                                useProjectedEnd = true;
                            }
                            else
                            {
                                response.Message = CEConfiguration.Required_Start_End_ProjectedDate;
                                NoError = false;
                            }
                        }
                        else
                        {
                            if (request.StartDate != DateTime.MinValue && request.EndDate != DateTime.MinValue)
                            {
                                startDate = request.StartDate;
                                endDate = request.EndDate;
                            }
                        }
                        if (NoError)
                        {
                            var finalizedTierBoundaries = FinalizeTierBoundaries(clientUser.RateCompanyID, request.RateClass, request.Date, startDate, endDate, projectedEndDate, useProjectedEnd);
                            rateDetails.FinalizedTierBoundaries = finalizedTierBoundaries;
                        }
                    }
                }               
                
            }
            else
            {
                rateClass = GetBasicRateInfo(clientUser.RateCompanyID, request.RateClass, request.Date);
            }

            if(rateClass != null)
            {
                if (NoError)
                {
                    response.RateClass = rateClass;
                    response.Details = rateDetails;
                }
            }
            else
            {
                response.Message = "No data found";
            }


            
            return response;
        }


        public MI.RateClass GetBasicRateInfo(int rateCompanyId, string rateClass, DateTime date)
        {
            MI.RateClass basicRateInfo = null;
            var rateClassInfo = rateModel.GetRateClassInformation(rateCompanyId, rateClass, date);

            if(rateClassInfo != null)
            {

                basicRateInfo = new MI.RateClass();
                basicRateInfo = Mapper.Map<RateClass, MI.RateClass>(rateClassInfo);
            }
            return basicRateInfo;
        }

        public MI.RateDetails GetDetailedRateInfo(int rateCompanyId, string rateClass, DateTime date, ref MI.RateClass basicRateInfo)
        {
            MI.RateDetails detailedRateInfo = null;
            var rateClassInfo = rateModel.GetDetailsRateClassInformation(rateCompanyId, rateClass, date);

            if (rateClassInfo != null)
            {

                basicRateInfo = new MI.RateClass();
                basicRateInfo = Mapper.Map<DetailedRateClass, MI.RateClass>(rateClassInfo);

                detailedRateInfo = new MI.RateDetails();
                detailedRateInfo = Mapper.Map<DetailedRateClass, MI.RateDetails>(rateClassInfo);
            }
            return detailedRateInfo;
        }

        public List<MI.TierBoundary> FinalizeTierBoundaries(int rateCompanyId, string rateClass, DateTime date, DateTime startDate, DateTime endDate, DateTime projectedEndDate, bool useProjectedEndDate)
        {
            List<MI.TierBoundary> finalTierBoundaries = null;
            List<TierBoundary> tierBoudaries = null;
            if (startDate != DateTime.MinValue && endDate != DateTime.MinValue)
            {
                tierBoudaries = rateModel.FinalizedTierBoundariesByDateRange(rateCompanyId, rateClass, date, startDate, endDate, projectedEndDate, useProjectedEndDate);
            }
            else
            {
                tierBoudaries = rateModel.FinalizedTierBoundariesByDate(rateCompanyId, rateClass, date);
            }

            if(tierBoudaries != null && tierBoudaries.Count > 0)
            {
                finalTierBoundaries = new List<MI.TierBoundary>();

                finalTierBoundaries = Mapper.Map<List<TierBoundary>, List<MI.TierBoundary>>(tierBoudaries);
            }

            return finalTierBoundaries;
        }



        /// <summary>
        /// Local Content grabber.
        /// </summary>
        /// <param name="response"></param>
        /// <param name="clientUser"></param>
        /// <param name="locale"></param>
        public void ApplyContent(MI.RateResponse response, ClientUser clientUser, string locale)
        {
            //content
            var cf = new ContentModelFactoryContentful();
            var cp = cf.CreateContentProvider(clientUser.ClientID);
            var contentLocale = cp.GetContentLocale(locale);

            //get label content and configuration
            response.Content = new MI.Content
            {
                TextContent = cp.GetContentTextContent("threshold", contentLocale, string.Empty),
                Configuration = cp.GetContentConfiguration("threshold", string.Empty),
                Commodity = cp.GetContentCommodity(string.Empty),
                Measurement = cp.GetContentMeasurement(string.Empty),
                UOM = cp.GetContentUom(string.Empty),
                Currency = cp.GetContentCurrency(string.Empty)
            };
        }

        private IRateModel GetRateModel()
        {
            IRateModel rateModel;
            RateModelEPFactory RateModelEPFactory = new RateModelEPFactory();

            rateModel = RateModelEPFactory.CreateRateModel();

            return (rateModel);
        }


        private IRateModel GetRateModel(bool logOverride)
        {
            IRateModel rateModel;
            RateModelEPFactory RateModelEPFactory = new RateModelEPFactory();

            rateModel = RateModelEPFactory.CreateRateModel(logOverride);

            return (rateModel);
        }

        private void InitializeRateMapper()
        {
            Mapper.CreateMap<UseCharge, MI.UseCharge>()
                .ForMember(dest => dest.BaseOrTierKey, opts => opts.MapFrom(src => src.BaseOrTier.ToString()))
                .ForMember(dest => dest.ChargeValue, opts => opts.MapFrom(src => Convert.ToDecimal(src.ChargeValue)))
                .ForMember(dest => dest.CostKey, opts => opts.MapFrom(src => src.CostType))
                .ForMember(dest => dest.FcrGroup, opts => opts.MapFrom(src => src.FCRGroup))
                .ForMember(dest => dest.IsFuelCostRecovery, opts => opts.MapFrom(src => src.FuelCostRecovery))
                .ForMember(dest => dest.PartKey, opts => opts.MapFrom(src => src.PartType.ToString()))
                .ForMember(dest => dest.IsRealTimePricing, opts => opts.MapFrom(src => src.RealTimePricing))
                .ForMember(dest => dest.RtpGroup, opts => opts.MapFrom(src => src.RTPGroup))
                .ForMember(dest => dest.SeasonKey, opts => opts.MapFrom(src => src.Season.ToString()))
                .ForMember(dest => dest.TimeOfUseKey, opts => opts.MapFrom(src => src.TimeOfUse.ToString()));

            Mapper.CreateMap<ServiceCharge, MI.ServiceCharge>()
                .ForMember(dest => dest.CalculationKey, opts => opts.MapFrom(src => src.CalculationType.ToString()))
                .ForMember(dest => dest.ChargeValue, opts => opts.MapFrom(src => Convert.ToDecimal(src.ChargeValue)))
                .ForMember(dest => dest.CostKey, opts => opts.MapFrom(src => src.CostType))
                .ForMember(dest => dest.RebateClassKey, opts => opts.MapFrom(src => src.RebateClass.ToString()))
                .ForMember(dest => dest.StepKey, opts => opts.MapFrom(src => src.Step.ToString()))
                .ForMember(dest => dest.SeasonKey, opts => opts.MapFrom(src => src.Season.ToString()));

            Mapper.CreateMap<MinimumCharge, MI.MinimumCharge>()
                .ForMember(dest => dest.CalculationKey, opts => opts.MapFrom(src => src.CalculationType.ToString()))
                .ForMember(dest => dest.ChargeValue, opts => opts.MapFrom(src => Convert.ToDecimal(src.ChargeValue)))
                .ForMember(dest => dest.CostKey, opts => opts.MapFrom(src => src.CostType));

            Mapper.CreateMap<TaxCharge, MI.TaxCharge>()
                .ForMember(dest => dest.ChargeValue, opts => opts.MapFrom(src => Convert.ToDecimal(src.ChargeValue)))
                .ForMember(dest => dest.CostKey, opts => opts.MapFrom(src => src.CostType));

            Mapper.CreateMap<TierBoundary, MI.TierBoundary>()
                .ForMember(dest => dest.BaseOrTierKey, opts => opts.MapFrom(src => src.BaseOrTier.ToString()))
                .ForMember(dest => dest.Threshold, opts => opts.MapFrom(src => Convert.ToDecimal(src.Threshold)))
                .ForMember(dest => dest.SecondaryThreshold, opts => opts.MapFrom(src => Convert.ToDecimal(src.SecondaryThreshold)))
                .ForMember(dest => dest.SeasonKey, opts => opts.MapFrom(src => src.Season.ToString()))
                .ForMember(dest => dest.TimeOfUseKey, opts => opts.MapFrom(src => src.TimeOfUse.ToString()))
                .ForMember(dest => dest.SeasonFactor, opts => opts.MapFrom(src => Convert.ToDecimal(src.SeasonFactor)))
                .ForMember(dest => dest.DaysIntoSeason, opts => opts.MapFrom(src => src.DaysIntoSeason));

            Mapper.CreateMap<SeasonBoundary, MI.SeasonBoundary>()
                .ForMember(dest => dest.StartDay, opts => opts.MapFrom(src => src.StartDay))
                .ForMember(dest => dest.EndDay, opts => opts.MapFrom(src => src.EndDay))
                .ForMember(dest => dest.SeasonKey, opts => opts.MapFrom(src => src.Season.ToString()));

            Mapper.CreateMap<TouBoundary, MI.TouBoundary>()
                .ForMember(dest => dest.DayKey, opts => opts.MapFrom(src => src.DayType.ToString()))
                .ForMember(dest => dest.StartTime, opts => opts.MapFrom(src => src.StartTime))
                .ForMember(dest => dest.EndTime, opts => opts.MapFrom(src => src.EndTime))
                .ForMember(dest => dest.SeasonKey, opts => opts.MapFrom(src => src.Season.ToString()))
                .ForMember(dest => dest.TimeOfUseKey, opts => opts.MapFrom(src => src.TimeOfUse.ToString()));

            Mapper.CreateMap<DetailedRateClass, MI.RateDetails>()
                .ForMember(dest => dest.UseCharges, opts => opts.MapFrom(src => Mapper.Map<List<UseCharge>, List<MI.UseCharge>>(src.UseCharges)))
                .ForMember(dest => dest.ServiceCharges, opts => opts.MapFrom(src => Mapper.Map<List<ServiceCharge>, List<MI.ServiceCharge>>(src.ServiceCharges)))
                .ForMember(dest => dest.TaxCharges, opts => opts.MapFrom(src => Mapper.Map<List<TaxCharge>, List<MI.TaxCharge>>(src.TaxCharges)))
                .ForMember(dest => dest.MinimumCharges, opts => opts.MapFrom(src => Mapper.Map<List<MinimumCharge>, List<MI.MinimumCharge>>(src.MinimumCharges)))
                .ForMember(dest => dest.TierBoundaries, opts => opts.MapFrom(src => Mapper.Map<List<TierBoundary>, List<MI.TierBoundary>>(src.TierBoundaries)))
                .ForMember(dest => dest.TouBoundaries, opts => opts.MapFrom(src => Mapper.Map<List<TouBoundary>, List<MI.TouBoundary>>(src.TouBoundaries)))
                .ForMember(dest => dest.SeasonBoundaries, opts => opts.MapFrom(src => Mapper.Map<List<SeasonBoundary>, List<MI.SeasonBoundary>>(src.SeasonBoundaries)))
                .ForMember(dest => dest.FinalizedTierBoundaries, opts => opts.Ignore());

            Mapper.CreateMap<RateClass, MI.RateClass>()
                .ForMember(dest => dest.RateClassID, opts => opts.MapFrom(src => src.RateClassID))
                .ForMember(dest => dest.Name, opts => opts.MapFrom(src => src.Name))
                .ForMember(dest => dest.Description, opts => opts.MapFrom(src => src.Description))
                .ForMember(dest => dest.CustomerType, opts => opts.MapFrom(src => src.CustomerType.ToString()))
                .ForMember(dest => dest.CommodityKey, opts => opts.MapFrom(src => src.CommodityAsString))
                .ForMember(dest => dest.ContainsPTR, opts => opts.MapFrom(src => src.ContainsPTR))
                .ForMember(dest => dest.IsTiered, opts => opts.MapFrom(src => src.IsTiered))
                .ForMember(dest => dest.IsTimeOfUse, opts => opts.MapFrom(src => src.IsTimeOfUse))
                .ForMember(dest => dest.IsTimeOfUse, opts => opts.MapFrom(src => src.IsSeasonal))
                .ForMember(dest => dest.UomKey, opts => opts.MapFrom(src => src.UnitOfMeasureAsString));

            Mapper.CreateMap<DetailedRateClass, MI.RateClass>()
                .ForMember(dest => dest.RateClassID, opts => opts.MapFrom(src => src.RateClassID))
                .ForMember(dest => dest.Name, opts => opts.MapFrom(src => src.Name))
                .ForMember(dest => dest.Description, opts => opts.MapFrom(src => src.Description))
                .ForMember(dest => dest.CustomerType, opts => opts.MapFrom(src => src.CustomerType.ToString()))
                .ForMember(dest => dest.CommodityKey, opts => opts.MapFrom(src => src.CommodityAsString))
                .ForMember(dest => dest.ContainsPTR, opts => opts.MapFrom(src => src.ContainsPTR))
                .ForMember(dest => dest.IsTiered, opts => opts.MapFrom(src => src.IsTiered))
                .ForMember(dest => dest.IsTimeOfUse, opts => opts.MapFrom(src => src.IsTimeOfUse))
                .ForMember(dest => dest.IsTimeOfUse, opts => opts.MapFrom(src => src.IsSeasonal))
                .ForMember(dest => dest.UomKey, opts => opts.MapFrom(src => src.UnitOfMeasureAsString));
        }


    }
}