﻿using System;
using System.Text.RegularExpressions;
using CE.Infrastructure;
using CE.Models;
using CE.Models.Insights;
using CE.Models.Insights.DataAccess;
using Microsoft.Practices.Unity;
using AO.BusinessContracts;
using CE.AO.Models;
using System.Threading.Tasks;

namespace CE.Insights.Models
{
    public class WebTokenModel
    {
        private readonly DataAccessLayer _dataAccess;
        private readonly IUnityContainer _container;
        private static readonly IInsightsEFRepository InsightsEfRepository = new InsightsEfRepository();
        private const string UsZipRegEx = @"^\d{5}(?:[-\s]\d{4})?$";
        private const string CaZipRegEx = @"^([ABCEGHJKLMNPRSTVXY]\d[ABCEGHJKLMNPRSTVWXYZ])\ {0,1}(\d[ABCEGHJKLMNPRSTVWXYZ]\d)$";
        private const string EmailRegEx = @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z";

        /// <summary>
        /// Constructor added for unit testing
        /// </summary>
        public WebTokenModel(DataAccessLayer dataAccess)
        {
            _dataAccess = dataAccess;
        }
        /// <summary>
        /// Default Constructor
        /// </summary>
        public WebTokenModel()
        {
            _dataAccess = new DataAccessLayer();
        }

        public WebTokenModel(IUnityContainer unityContainer) : this()
        {
            _container = unityContainer;
        }

        /// <summary>
        /// Create WebToken for POST request
        /// </summary>
        /// <param name="clientUser"></param>
        /// <param name="request"></param>
        /// <param name="response"></param>
        public void CreateWebToken(ClientUser clientUser, WebTokenPostRequest request, WebTokenPostResponse response)
        {
            const string stringAllowMultipleWebtokens = "allowmultiplewebtokens";
            var webToken = Guid.NewGuid().ToString();
            var utcDateTimeCreated = DateTime.UtcNow;
            int customerIdExists;

            if (_container == null)
            {
                if (string.IsNullOrEmpty(request.CustomerId))
                {
                    //Assumptions for Anonymous user
                    // 1. if customer id is empty then its an anonymous user call
                    // 2. if zip code and email is present then create new anonymous user and return new customer id starting with X_
                    // 3. if only email is present then request is to validate and return exisitng customer id starting with X_

                    if (request.EmailAddress == "test@test.com" && request.ZipCode == "90221-4523")
                    {
                        response.CustomerId = "X_131333";
                        response.AccountId = "X_131333";
                        response.PremiseId = "X_131333";
                        response.ServicePointId = "X_131333";
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(request.ZipCode) && string.IsNullOrEmpty(request.EmailAddress))
                        {
                            response.Status = string.Format(CEConfiguration.ErrorEitherEmailCustomerId, request.ZipCode);
                            response.CreatedUtcDateTime = null;
                            return;
                        }
                        else
                        {
                            string customerId;
                            string accountId;
                            string premiseId;
                            if (_dataAccess.GetAnonymousUser(clientUser.ClientID, request.EmailAddress, request.ZipCode,out customerId,out accountId, out premiseId))
                            {
                                response.CustomerId = customerId;
                                response.AccountId = accountId;
                                response.PremiseId = premiseId;
                                response.ServicePointId = customerId;//same as customer id
                            }
                            else
                            {
                                response.Status = string.Format(CEConfiguration.Response_NoData, request.EmailAddress);
                                response.CreatedUtcDateTime = null;
                                return;
                            }
                        }
                    }
                    customerIdExists = 1;
                    request.CustomerId = response.CustomerId;
                }
                else
                {
                    // check the warehouse, caller is v1
                    customerIdExists = _dataAccess.ValidateCustomerIdInDataWarehouse(clientUser.ClientID, request.CustomerId);
                }
            }
            else
            {
                // check AclaraOne table storage, caller is v2
                customerIdExists = ValidateCustomerInAclaraOne(clientUser.ClientID, request.CustomerId);
            }

            if (customerIdExists == 0)
            {
                response.Status = string.Format(CEConfiguration.ErrorNoCustomerFound, request.CustomerId);
            }

            var allowmultiplewebtokens = GetConfigurationAsBoolean(clientUser, stringAllowMultipleWebtokens, false);

            var groupId = InsightsEfRepository.GetGroupId(request.GroupName);
            var roleId = InsightsEfRepository.GetRoleId(request.RoleName);

            if (groupId == null || roleId == null || request.UserId == null || request.UserId.Length == 0)
            {
                request.UserId = null;
                groupId = null;
                roleId = null;
            }
            if (customerIdExists > 0 && (!string.IsNullOrEmpty(request.GroupName) || !string.IsNullOrEmpty(request.RoleName)))
            {
                if (groupId == null)
                {
                    response.Status = string.Format(CEConfiguration.ErrorInvalidGroupOrRole);
                }
            }

            if (allowmultiplewebtokens)
            {
                _dataAccess.InsertCustomerWebTokenInfo(clientUser.ClientID, request.CustomerId, webToken, request.AdditionalInfo, utcDateTimeCreated, request.UserId, groupId, roleId);
            }
            else
            {
                _dataAccess.MergeCustomerWebTokenInfo(clientUser.ClientID, request.CustomerId, webToken, request.AdditionalInfo, utcDateTimeCreated, request.UserId, groupId, roleId);
            }

            response.WebToken = webToken;
            response.CreatedUtcDateTime = utcDateTimeCreated;
        }

        /// <summary>
        /// Validate WebToken for GET request.
        /// </summary>
        /// <param name="clientUser"></param>
        /// <param name="request"></param>
        /// <param name="response"></param>
        public bool ValidateWebToken(ClientUser clientUser, WebTokenRequest request, WebTokenResponse response)
        {
            const string stringWebTokenTimeout = "webtokentimeout";
            const int defaultWebTokenTimeout = 60;

            var groupId = InsightsEfRepository.GetGroupId(request.GroupName);
            var roleId = InsightsEfRepository.GetRoleId(request.RoleName);

            if (groupId == null || roleId == null || request.UserId == null || request.UserId.Length == 0)
            {
                request.UserId = null;
                groupId = null;
                roleId = null;
            }

            var webTokenTimeout = GetConfigurationAsInt(clientUser, stringWebTokenTimeout, defaultWebTokenTimeout);
            var result = _dataAccess.GetCustomerWebTokenInfo(clientUser.ClientID, request.CustomerId, request.WebToken, webTokenTimeout, out var isActive, out var additionalInfo, out DateTime utcDateTimeCreated, request.UserId, groupId, roleId);

            if (!result) return false;
            response.IsActive = isActive;
            response.CreatedUtcDateTime = utcDateTimeCreated;
            if (request.IncludeAdditionalInfo)
            {
                response.AdditionalInfo = additionalInfo;
            }
            return true;
        }

        /// <summary>
        /// Delete WebToken for DELETE request
        /// </summary>
        /// <param name="clientUser"></param>
        /// <param name="request"></param>
        /// <param name="response"></param>
        public bool DeleteWebToken(ClientUser clientUser, WebTokenDeleteRequest request, WebTokenDeleteResponse response)
        {
            var returnResult = _dataAccess.DeleteCustomerWebTokenInfo(clientUser.ClientID, request.CustomerId, request.WebToken);
            if (returnResult <= 0) return false;
            response.Success = true;
            return true;
        }

        /// <summary>
        /// Validate if the customer exists in the AclaraOne table storage.
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        private int ValidateCustomerInAclaraOne(int clientId, string customerId)
        {
            var result = 0;
            var custTask = GetCustAsync(clientId, customerId);
            var res = custTask.Wait(10000);

            if (!res || custTask.Status != TaskStatus.RanToCompletion) return result;
            var customerModel = custTask.Result;

            if (customerModel == null) return result;
            if (customerModel.CustomerId == customerId)
            {
                result = 1;
            }

            return result;
        }

        /// <summary>
        /// Get Customer Information using the ICustomer interface in table storage.
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        private async Task<CustomerModel> GetCustAsync(int clientId, string customerId)
        {
            var customerManager = _container.Resolve<ICustomer>();
            var task = await Task.Factory.StartNew(() => customerManager.GetCustomerAsync(clientId, customerId)).ConfigureAwait(false);

            return task.Result;
        }

        /// <summary>
        /// Returns CMS config value as integer
        /// </summary>
        /// <param name="clientUser"></param>
        /// <param name="configurationKey"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        private static int GetConfigurationAsInt(ClientUser clientUser, string configurationKey, int defaultValue)
        {
            var cf = new ContentModel.ContentModelFactoryContentful();
            var cp = cf.CreateContentProvider(clientUser.ClientID);
            var result = cp.GetContentConfigurationAsInt(configurationKey, defaultValue);

            return result;
        }

        /// <summary>
        /// Returns CMS config value as bool
        /// </summary>
        /// <param name="clientUser"></param>
        /// <param name="configurationKey"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        private static bool GetConfigurationAsBoolean(ClientUser clientUser, string configurationKey, bool defaultValue)
        {
            var cf = new ContentModel.ContentModelFactoryContentful();
            var cp = cf.CreateContentProvider(clientUser.ClientID);
            var result = cp.GetContentConfigurationAsBoolean(configurationKey, defaultValue);

            return result;
        }

        public bool IsUsorCanadianZipCode(string zipCode)
        {
            return !(!Regex.Match(zipCode, UsZipRegEx).Success && !Regex.Match(zipCode, CaZipRegEx).Success);
        }
        
        public bool IsEmail(string emailString)
        {
            return Regex.IsMatch(emailString, EmailRegEx, RegexOptions.IgnoreCase);
        }
    }
}