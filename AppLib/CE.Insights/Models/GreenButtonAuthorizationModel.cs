﻿using System.Collections.Generic;
using System.Linq;
using CE.Models;
using CE.Models.Insights;
using CE.Models.Insights.GreenButtonConnect;
using CE.Models.Insights.Types;
using Microsoft.Practices.Unity;

namespace CE.Insights.Models
{
    public class GreenButtonAuthorizationModel : GreenButtonBaseModel
    {
        /// <summary>
        /// constructor for reading type model
        /// </summary>
        public GreenButtonAuthorizationModel(string accessToken, string locale, string greenButtonDomain, IUnityContainer container, IInsightsEFRepository insightsEfRepository) : base(accessToken, locale, greenButtonDomain, container, insightsEfRepository)
        {
        }

        /// <summary>
        /// Testability, passed in repository and unity container so not relying on data access through sql server and table storage
        /// </summary>
        /// <param name="greenButtonDomain"></param>
        /// <param name="authorizations"></param>
        /// <param name="greenButtonSettings"></param>
        /// <param name="unityContainer"></param>
        /// <param name="insightsEfRepository"></param>
        /// <param name="clientId"></param>
        public GreenButtonAuthorizationModel(int clientId, string greenButtonDomain, List<GreenButtonAuthorization> authorizations, GreenButtonConfig greenButtonSettings, IUnityContainer unityContainer, IInsightsEFRepository insightsEfRepository): base(clientId, greenButtonDomain, authorizations, unityContainer,insightsEfRepository, greenButtonSettings)
        {
        }

        public GreenButtonResult GetAllAuthorizationGreenButton(string accessToken)
        {
            if (Authorizations == null || Authorizations.FindAll(a => a.AccessToken == accessToken).Count == 0)
                return new GreenButtonResult { Status = GreenButtonErrorType.InvalidAuthorization };

            // get application info id
            var applicationInfoId = Authorizations.Find(a => a.AccessToken == accessToken).ApplicationInformationId;

            // get all authorization with the same application info id (third party)
            var authorizations = Authorizations.Where(a => a.ApplicationInformationId == applicationInfoId && a.AccessTokenType != GreenButtonAccessTokenType.Registration).ToList();

            if(authorizations.Any())
                return GreenButtonModel.GenerateAuthorization(authorizations);
            return new GreenButtonResult { Status = GreenButtonErrorType.InvalidAuthorization };
        }

        public GreenButtonResult GetAuthorizationGreenButton(string accessToken, long authorizationId)
        {
            if (Authorizations == null || Authorizations.FindAll(a => a.AccessToken == accessToken).Count == 0)
                return new GreenButtonResult { Status = GreenButtonErrorType.InvalidAuthorization };
            
            // get authorization by authorization id
            var authorization = Authorizations.Find(a => a.AuthorizationId == authorizationId && a.AccessTokenType != GreenButtonAccessTokenType.Registration);

            if (authorization != null)
                return GreenButtonModel.GenerateAuthorization(new List<GreenButtonAuthorization> {authorization});
            return new GreenButtonResult { Status = GreenButtonErrorType.InvalidRequestAuthorization };
        }
    }
}