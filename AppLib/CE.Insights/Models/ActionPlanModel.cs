﻿using System;
using System.Collections.Generic;
using System.Linq;
using CE.Models;
using CE.Models.Insights;
using CM = CE.ContentModel;

namespace CE.Insights.Models
{
    /// <summary>
    /// ActionPlan model
    /// </summary>
    public class ActionPlanModel : IActionPlanModel
    {
        /// <summary>
        /// Get actions from content and check POSTED actions for any that are invalid, duplicates and invalid status.
        /// </summary>
        /// <param name="clientUser"></param>
        /// <param name="locale"></param>
        /// <param name="environment"></param>
        /// <param name="request"></param>
        /// <param name="invalidActions"></param>
        /// <param name="duplicateActions"></param> 
        /// <param name="invalidActionStatus"></param>
        /// <param name="invalidActionSource"></param>
        /// <returns></returns>
        public bool ValidateActions(ClientUser clientUser, string locale, string environment, ActionPlanPostRequest request, List<String> invalidActions, List<String> duplicateActions, List<String> invalidActionStatus, List<String> invalidActionSource)
        {
            bool valid = true;

            var cf = new CM.ContentModelFactoryContentful();
            var cp = cf.CreateContentProvider(clientUser.ClientID);

            var defautActions = cp.GetContentAction(string.Empty);

            var defautActionEnums = cp.GetContentEnumeration("action,common", string.Empty);

            var actionKeys = defautActions.Select(item => item.Key).ToList();

            var actionStatus = defautActionEnums.Where(item => item.EnumerationKey == "action.status").Select(item => item.Key).ToList();

            var actionSourceEnum = cp.GetContentEnumeration("action,common", string.Empty);

            var actionSources = actionSourceEnum.Where(item => item.EnumerationKey == "common.source").Select(item => item.Key).ToList();

            // compare with all lists in the posted request
            if (defautActions != null)
            {
                if (request.Customer != null)
                {
                    if (request.Customer.Accounts != null)
                    {
                        foreach (var a in request.Customer.Accounts)
                        {
                            if (a.Premises != null)
                            {
                                foreach (var p in a.Premises)
                                {
                                    foreach (var action in p.ActionStatuses)
                                    {
                                        //looking for invalid action
                                        if (!actionKeys.Contains(action.ActionKey))
                                        {
                                            valid = false;
                                            invalidActions.Add(action.ActionKey);
                                        }
                                        // check for invalid status
                                        if (!actionStatus.Contains(action.Status))
                                        {
                                            valid = false;
                                            invalidActionStatus.Add(action.ActionKey);
                                        }

                                        if (!actionSources.Contains(action.SourceKey))
                                        {
                                            valid = false;
                                            invalidActionSource.Add(action.ActionKey);
                                        }
                                    }

                                    // check for duplicates
                                    var itemList = new List<String>();
                                    foreach (var action in p.ActionStatuses)
                                    {
                                        itemList.Add(action.ActionKey + ";" + action.SubActionKey);
                                    }

                                    List<String> duplicates = itemList.GroupBy(x => x)
                                     .Where(g => g.Count() > 1)
                                     .Select(g => g.Key)
                                     .ToList();

                                    if (duplicates != null && duplicates.Count > 0)
                                    {
                                        valid = false;
                                        List<String> dups = new List<String>(); 
                                        foreach (var d in duplicates)
                                        {
                                            string[] dup = d.Split(';');
                                            dups.Add(dup[0]);
                                        }
                                        
                                        duplicateActions.AddRange(dups);
                                    }
                                }
                            }

                        }

                    }

                }

            }

            return (valid);
        }

    }

    public interface IActionPlanModel
    {
        bool ValidateActions(ClientUser clientUser, string locale, string environment, ActionPlanPostRequest request,
            List<String> invalidActions, List<String> duplicateActions, List<String> invalidActionStatus,
            List<String> invalidActionSource);
    }
}