﻿using AutoMapper;
using AO.BusinessContracts;
using CE.AO.Models;
using Enums = CE.AO.Utilities.Enums;
using CE.BillToDate;
using CE.ContentModel;
using CE.Insights.Helpers;
using CE.Models;
using CE.Models.Insights;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AO.Business;
using CE.AO.Utilities;
using AutoMapperConfig = CE.AO.Business.AutoMapperConfig;

namespace CE.Insights.Models
{
    /// <summary>
    /// Bill To Date Model
    /// </summary>
    public class BillToDateModel
    {
        private readonly IUnityContainer _container;
        private BillToDateSettings _btdSettings;
        private IBillToDate _billToDateCalc;

        public BillToDateModel(IUnityContainer unityContainer)
        {
            _container = unityContainer;
        }

        /// <summary>
        /// unit test
        /// </summary>
        /// <param name="unityContainer"></param>
        /// <param name="settings"></param>
        /// <param name="billToDateCalculator"></param>
        public BillToDateModel(IUnityContainer unityContainer, BillToDateSettings settings, IBillToDate billToDateCalculator)
        {
            _container = unityContainer;
            _btdSettings = settings;
            _billToDateCalc = billToDateCalculator;
        }
        
        public BillToDateModel(IUnityContainer unityContainer, int clientId)
        {
            var reader = new BillToDate.Configuration.SettingsReader(clientId);
            _btdSettings = reader.GetSettings();
            _container = unityContainer;
        }

        public BillToDateModel(IUnityContainer unityContainer, int clientId, List<ContentModel.Entities.ClientConfiguration> settings)
        {
            var reader = new BillToDate.Configuration.SettingsReader(clientId);
            _btdSettings = reader.GetSettings();
            reader.MergeSettings(_btdSettings, settings);
            _container = unityContainer;
        }

        /// <summary>
        /// unit test post
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="unityContainer"></param>
        /// <param name="billToDateSettings"></param>
        /// <param name="settings"></param>
        /// <param name="billToDateCalc"></param>
        public BillToDateModel(int clientId, IUnityContainer unityContainer, BillToDateSettings billToDateSettings, List<ContentModel.Entities.ClientConfiguration> settings, IBillToDate billToDateCalc)
        {
            var reader = new BillToDate.Configuration.SettingsReader(clientId);
            _btdSettings = billToDateSettings;
            reader.MergeSettings(_btdSettings, settings);
            _container = unityContainer;
            _billToDateCalc = billToDateCalc;
        }

    public BillToDateResponse GetBillToDate(ClientUser clientUser, BillToDateRequest request)
        {            
            var response = new BillToDateResponse();
            
                // get settings
                if (_btdSettings == null)
                {
                    var reader = new BillToDate.Configuration.SettingsReader(clientUser.ClientID);
                    _btdSettings = reader.GetSettings();
                }

                var forceCalc = request.ForceCalc;
                var bills = GetBillInfo(clientUser.ClientID, request.CustomerId, request.AccountId);
                if (bills != null && bills.Count > 0)
                {
                    CE.Models.Insights.BillToDate billToDateResult;
                    if (!forceCalc)
                {
                    billToDateResult = GetCalculatedBillToDate(bills, clientUser.ClientID, request.AccountId);
                        if (billToDateResult == null)
                            forceCalc = true;
                        else
                        {
                            if (Convert.ToDateTime(billToDateResult.BillEndDate) < DateTime.Now.Date)
                            {
                                forceCalc = true;
                            }
                            else
                            {
                                response.BillToDate = billToDateResult;
                            }

                        }
                    }

                    if (forceCalc)
                    {
                        // else calculate it

                        DateTime billDate;

                        // get metered services
                        var meteredServices = GetMeteredSericeList(bills, clientUser.ClientID, out billDate);

                        if (billDate == DateTime.MaxValue)
                        {
                            billDate = DateTime.Today.Date;
                        }

                        var bill = new BillToDate.Bill(clientUser.ClientID, billDate, request.CustomerId,
                            request.AccountId,
                            _btdSettings.RateCompanyId);

                        if (meteredServices != null && meteredServices.Count > 0)
                        {
                            bill.MeteredServices = meteredServices;
                        }

                        // get non metered services
                        var nonMeteredServices = GetNonMeteredServiceList(bills, clientUser.ClientID, billDate);

                        if (nonMeteredServices != null && nonMeteredServices.Count > 0)
                        {
                            bill.NonMeteredServices = nonMeteredServices;
                        }

                        var sewerCount =
                            meteredServices?.SelectMany(m => m.Meters)
                                .ToList()
                                .FindAll(m => !string.IsNullOrEmpty(m.RateClass2) &&
                                              m.DerivedFuel == DerivedFuelType.sewer)
                                .Count;

                        billToDateResult = bills.Count <=
                                           meteredServices?.SelectMany(m => m.Meters).Count() + sewerCount +
                                           nonMeteredServices?.Count
                            ? CalculateBillToDate(bill, request.ShowLog)
                            : new CE.Models.Insights.BillToDate {HasError = true, ErrorMessage = "NoAMIData"};

                        response.BillToDate = billToDateResult;
                    }
                }
                else
                {
                    // return errors
                    response.Message = "No data found";
                }

            return response;
        }

        public BillToDateResponse GetBillToDate(ClientUser clientUser, BillToDatePostRequest request)
        {
            var response = new BillToDateResponse();

            var billDate = DateTime.MaxValue;

            var bill = new BillToDate.Bill(billDate, clientUser.ClientID, request.CustomerId, request.AccountId, request.RateCompanyId);
            // get metered services
            foreach (var service in request.Services)
            {
                var meteredService = new MeteredService
                {
                    ServiceID = service.Id,
                    Fuel = Map.ConvertFuelType(service.CommodityKey)
                };

                if(service.Meters != null)
                {
                    foreach (var meter in service.Meters)
                    {

                        meteredService.Meters.Add(GetMeter(meter, service.CommodityKey));

                        var date = Convert.ToDateTime(meter.StartDate);
                        if (date < billDate)
                            billDate = date;
                    }
                }

                bill.MeteredServices.Add(meteredService);
            }

            // get non metered services
            if (request.NonMeteredServices != null)
            {
                foreach (var nonService in request.NonMeteredServices)
                {
                    var nonMeterService = new NonMeteredService(nonService.CommodityKey, nonService.Cost, nonService.Usage,
                        nonService.UomKey, Convert.ToDateTime(nonService.StartDate), Convert.ToDateTime(nonService.EndDate));

                    var date = Convert.ToDateTime(nonService.StartDate);
                    if (date < billDate)
                        billDate = date;

                    if (nonService.ProjectedEndDate != null && !_btdSettings.Settings.General.UseProjectedNumDaysforBilltoDate)
                        nonMeterService.ProjectedEndDate = Convert.ToDateTime(nonService.ProjectedEndDate);
                    bill.NonMeteredServices.Add(nonMeterService);
                }
            }

            bill.Datestamp = billDate;

            var billToDate = CalculateBillToDate(bill, true);
            response.BillToDate = billToDate;

            return response;
        }

        public CE.Models.Insights.BillToDate CalculateBillToDate(BillToDate.Bill bill, bool enableLogging)
        {
            CE.Models.Insights.BillToDate billToDateResult;
            IBillToDate billToDateCalculator;

            //main calc
            var billToDateFactory = new CalculationManagerFactory();


            if (_billToDateCalc == null)
            {

                if (enableLogging)
                {
                    billToDateCalculator = billToDateFactory.CreateCalculationManager(_btdSettings, true);
                    
                }
                else
                {
                    billToDateCalculator = billToDateFactory.CreateCalculationManager(_btdSettings);
                }
            }
            else
            {
                billToDateCalculator = _billToDateCalc;
            }
            var result = billToDateCalculator.CalculateBillToDate(bill);

            //// Make sure the UOM matches the bill.
            //if (bill != null && ctdCalculation.Unit.HasValue &&
            //    bill.UOMId.HasValue &&
            //    !ctdCalculation.Unit.Equals(bill.))
            //{
            //    var conversionFactor = Extensions.GetConversionFactor(clientId,
            //        ctdCalculation.Unit.Value, firstOrDefault.UOMId.Value);

            //    ctdCalculation.Unit = firstOrDefault.UOMId;

            //    ctdCalculation.ProjectedUsage = ctdCalculation.ProjectedUsage *
            //                                    conversionFactor;

            //    ctdCalculation.Usage = ctdCalculation.Usage * conversionFactor;

            //}

            if (!result.StatusList.HasStatusSeverity(StatusTypes.StatusSeverity.Error))
            {
                billToDateResult = MapBillToDateResult(result);

                if (enableLogging)
                {
                    billToDateResult.LogEntries = GetLoggingInfo(billToDateCalculator, result.StatusList);
                }
            }
            else
            {
                
                // return error
                var error = result.StatusList.FindAll(s => s.StatusSeverity == StatusTypes.StatusSeverity.Error).FirstOrDefault();
                
                billToDateResult = new CE.Models.Insights.BillToDate { HasError = true, ErrorMessage = error?.Exception.Message + error?.Exception.InnerException?.Message };
            }

            return billToDateResult;
        }

        /// <summary>
        /// Calculate the Bill To Date
        /// </summary>
        /// <param name="bills">A list of the client's bills.</param>
        /// <param name="clientId">The client Id.</param>
        /// <param name="accountId">The account Id of the client.</param>
        /// <returns>A BillToDate object.</returns>
        public CE.Models.Insights.BillToDate GetCalculatedBillToDate(List<BillingModel> bills, int clientId, string accountId)
        {
            CE.Models.Insights.BillToDate billToDateResult;
            CalculationModel btdCalculationResult = null;
            try
            {

                var btdTask = GetBtdAsync(clientId, accountId).ContinueWith(btd =>
                {
                    btdCalculationResult = btd.Result;
                });

                btdTask.Wait();

                if (btdCalculationResult != null)
                {
                    InitializeCalculationModelMapper();

                    billToDateResult = Mapper.Map<CalculationModel, CE.Models.Insights.BillToDate>(btdCalculationResult);
                    if (billToDateResult.TotalCost != 0m && billToDateResult.TotalProjectedCost != 0m)
                    {
                        billToDateResult.HasError = false;

                        // get caculated CTD for each service
                        billToDateResult.Services = new List<BillToDateService>();
                        var services =
                            bills.FindAll(s => s.MeterType.ToLower() ==
                                               "ami"); // since aclara one don't stored the non meter result in calculation table, it just adds to the BTD result || s.MeterType.ToLower() == "nonmetered");


                        // check if the calculated btd result is current result before move on to the cost to date
                        if (btdCalculationResult.AsOfCalculationDate >= Convert.ToDateTime(btdCalculationResult.BillCycleStartDate))
                        {
                            foreach (var service in services)
                            {
                                CalculationModel ctdCalculation = null;

                                var ctdTask = GetCtdAsync(clientId, accountId, service.ServiceContractId).ContinueWith(ctd =>
                                {
                                    ctdCalculation = ctd.Result;
                                });

                                ctdTask.Wait();
                                if (ctdCalculation != null)
                                {
                                    // add the calculated ctd result to the collection if it's current result
                                    if (ctdCalculation.AsOfCalculationDate >=
                                        Convert.ToDateTime(btdCalculationResult.BillCycleStartDate))
                                    {
                                        // Make sure the UOM matches the bill.
                                        var bill =
                                            bills.Find(
                                                b =>
                                                    b.MeterId == ctdCalculation.MeterId &&
                                                    b.ServiceContractId == ctdCalculation.ServiceContractId);
                                        if (bill != null && ctdCalculation.Unit.HasValue &&
                                            bill.UOMId.HasValue &&
                                            !ctdCalculation.Unit.Equals(bill.UOMId))
                                        {
                                            var conversionFactor = Extensions.GetConversionFactor(clientId,
                                                ctdCalculation.Unit.Value, bill.UOMId.Value);

                                            ctdCalculation.Unit = bill.UOMId;

                                            ctdCalculation.ProjectedUsage = ctdCalculation.ProjectedUsage *
                                                                            conversionFactor;

                                            ctdCalculation.Usage = ctdCalculation.Usage * conversionFactor;

                                        }

                                        var costToDate =
                                            Mapper.Map<CalculationModel, BillToDateService>(ctdCalculation);

                                        if (costToDate.CostToDate != 0m && costToDate.CostProjected != 0m)
                                        {
                                            billToDateResult.Services.Add(costToDate);
                                        }
                                    }
                                }

                            }

                            if ((billToDateResult.Services.Count == 0 ||
                                 billToDateResult.Services.Count != services.Count))
                            {
                                billToDateResult = null;
                            }
                        }
                        else
                        {
                            billToDateResult = null;
                        }

                    }
                    else
                    {
                        billToDateResult = null;
                    }
                }
                else
                {
                    billToDateResult = null;
                }


            }
            catch
            {
                billToDateResult = null;
            }
            return billToDateResult;
        }

        public CE.Models.Insights.BillToDate MapBillToDateResult(BillToDateResult result)
        {
            var hasErr = false;
            var errorList = new List<string>();
            var billToDate = new CE.Models.Insights.BillToDate {Services = new List<BillToDateService>()};

            // meter services mapping
            foreach (var meteredService in result.Bill.MeteredServices)
            {
                foreach (var meter in meteredService.Meters)
                {
                    if (meter.CostToDateResult.ErrCode == RateModel.Enums.ErrorCode.NoError)
                    {
                        var service = new BillToDateService
                        {
                            Id = meter.MeterID,
                            StartDate = meter.CostToDateResult.StartDate.ToShortDateString(),
                            EndDate = meter.CostToDateResult.ProjectedEndDate.ToShortDateString(),
                            CommodityKey = Map.ConvertCommodity(meter),
                            DaysIntoCycle =
                                (int)
                                    Math.Floor(
                                        (meter.CostToDateResult.EndDate - meter.CostToDateResult.StartDate).TotalDays +
                                        1),
                            ReadDate = meter.CostToDateResult.EndDate.ToShortDateString(),
                            CostToDate = Convert.ToDecimal(Math.Round(meter.CostToDateResult.Cost, 1)),
                            CostProjected = Convert.ToDecimal(Math.Round(meter.CostToDateResult.ProjectedCost, 1)),
                            UomKey = Map.ConvertUomString(meter.PrimaryUnitOfMeasure)
                        };

                        if (meter.Fuel == FuelType.water && meter.DerivedFuel == DerivedFuelType.sewer &&
                            meter.DerivedFuelIsPrimary)
                        {
                            service.RateClass = meter.RateClass2;
                        }
                        else
                        {
                            service.RateClass = meter.RateClass;
                        }

                        // Make sure the UOM matches the bill.
                        var rateClassModel = new RateClassModel();
                        var rateClass = rateClassModel.GetBasicRateInfo(meter.RateCompanyID, meter.RateClass,
                            meter.CostToDateResult.ProjectedEndDate);

                        var conversionFactor = 1.0;
                        if (!rateClass.UomKey.Equals(service.UomKey))
                        {

                            conversionFactor = Extensions.GetConversionFactor(result.Bill.ClientID,
                                (int) Enum.Parse(typeof(Enums.UomType), service.UomKey),
                                (int) Enum.Parse(typeof(Enums.UomType), rateClass.UomKey));

                            service.UomKey = rateClass.UomKey;
                        }

                        // usage to date
                        if (
                            meter.CostToDateResult.Usages.FindAll(
                                p => p.BaseOrTier == RateModel.Enums.BaseOrTier.TotalServiceUse).Count > 0)
                        {
                            service.UseToDate =
                                Convert.ToDecimal(
                                    Math.Round(
                                        meter.CostToDateResult.Usages.FindAll(
                                            p => p.BaseOrTier == RateModel.Enums.BaseOrTier.TotalServiceUse)
                                            .ToArray()
                                            .Sum(p => p.Quantity), 2))*(decimal) conversionFactor;

                        }
                        else if (
                            meter.CostToDateResult.Usages.FindAll(
                                p => p.BaseOrTier == RateModel.Enums.BaseOrTier.Undefined).Count > 0)
                        {
                            service.UseToDate =
                                Convert.ToDecimal(
                                    Math.Round(
                                        meter.CostToDateResult.Usages.FindAll(
                                            p => p.BaseOrTier == RateModel.Enums.BaseOrTier.Undefined)
                                            .ToArray()
                                            .Sum(p => p.Quantity), 2))*(decimal) conversionFactor;
                        }
                        else
                        {
                            service.UseToDate =
                                Convert.ToDecimal(Math.Round(meter.CostToDateResult.Usages.Sum(p => p.Quantity), 2))*
                                (decimal) conversionFactor;
                        }

                        // projected usage
                        if (
                            meter.CostToDateResult.ProjectedUsages.FindAll(
                                p => p.BaseOrTier == RateModel.Enums.BaseOrTier.TotalServiceUse).Count > 0)
                        {
                            service.UseProjected =
                                Convert.ToDecimal(
                                    Math.Round(
                                        meter.CostToDateResult.ProjectedUsages.FindAll(
                                            p => p.BaseOrTier == RateModel.Enums.BaseOrTier.TotalServiceUse)
                                            .ToArray()
                                            .Sum(p => p.Quantity), 2))*(decimal) conversionFactor;

                        }
                        else if (
                            meter.CostToDateResult.ProjectedUsages.FindAll(
                                p => p.BaseOrTier == RateModel.Enums.BaseOrTier.Undefined).Count > 0)
                        {
                            service.UseProjected =
                                Convert.ToDecimal(
                                    Math.Round(
                                        meter.CostToDateResult.ProjectedUsages.FindAll(
                                            p => p.BaseOrTier == RateModel.Enums.BaseOrTier.Undefined)
                                            .ToArray()
                                            .Sum(p => p.Quantity), 2))*(decimal) conversionFactor;
                        }
                        else
                        {
                            service.UseProjected =
                                Convert.ToDecimal(
                                    Math.Round(meter.CostToDateResult.ProjectedUsages.Sum(p => p.Quantity), 2))*
                                (decimal) conversionFactor;
                        }



                        billToDate.Services.Add(service);
                    }
                    else
                    {
                        errorList.Add(meter.CostToDateResult.ErrCode.ToString());
                        break;
                    }
                }
            }

            // non metered service mapping
            foreach (var nonMetered in result.Bill.NonMeteredServices)
            {
                if (!nonMetered.CostToDateResult.StatusList.HasStatusSeverity(StatusTypes.StatusSeverity.Error))
                {
                    var service = new BillToDateService
                    {
                        Id = nonMetered.Description,
                        UseToDate = Convert.ToDecimal(nonMetered.CostToDateResult.Usage),
                        UseProjected = Convert.ToDecimal(nonMetered.CostToDateResult.ProjectedUsage),
                        DaysIntoCycle = nonMetered.CostToDateResult.NumberOfDaysInCostToDate,
                        StartDate = nonMetered.StartDate?.ToShortDateString(),
                        EndDate = nonMetered.ProjectedEndDate?.ToShortDateString(),
                        ReadDate = nonMetered.EndDate?.ToShortDateString(),
                        CommodityKey = nonMetered.Name.ToLower(),
                        CostToDate = Convert.ToDecimal(Math.Round(nonMetered.CostToDateResult.Cost, 1)),
                        CostProjected = Convert.ToDecimal(Math.Round(nonMetered.CostToDateResult.ProjectedCost, 1)),
                        UomKey = nonMetered.Uom
                    };
                    billToDate.Services.Add(service);

                }
                else
                {
                    hasErr = true;
                    var firstOrDefault =
                        nonMetered.CostToDateResult.StatusList.FindAll(
                            s => s.StatusSeverity == StatusTypes.StatusSeverity.Error).FirstOrDefault();
                    if (
                        firstOrDefault != null)
                        errorList.Add(firstOrDefault.Exception?.Message);
                    break;
                }
            }

            if (!hasErr)
            {
                billToDate.TotalCost = Convert.ToDecimal(Math.Round(result.TotalCost, 1));
                billToDate.TotalProjectedCost = Convert.ToDecimal(Math.Round(result.TotalProjectedCost, 1));
                // get the largest days into cycle among all services
                var maxDaysIntoCycle = billToDate.Services.Max(s => s.DaysIntoCycle);
                // get the largest projected bill days among all services
                var maxProjectedBillDay = billToDate.Services.Max(s => Math.Floor((Convert.ToDateTime(s.EndDate) - Convert.ToDateTime(s.StartDate)).TotalDays + 1));
                // get the latest read date among all services
                var latestReadDate = billToDate.Services.Max(s => Convert.ToDateTime(s.ReadDate));
                // compute the start date for the bill usinig largest days into cycle and latest read date among all services
                var startDate = latestReadDate.Date.AddDays((-1 * maxDaysIntoCycle) + 1).Date;
                var endDate = startDate.AddDays(maxProjectedBillDay - 1).Date;
                billToDate.BillStartDate = startDate.ToShortDateString();
                billToDate.BillEndDate = endDate.ToShortDateString();
                billToDate.BillDays = (endDate - startDate).TotalDays + 1;
                billToDate.AverageDailyCost = Math.Round(billToDate.TotalCost / maxDaysIntoCycle, 2);
                billToDate.HasError = false;

            }
            else
            {
                billToDate = new CE.Models.Insights.BillToDate
                {
                    HasError = true,
                    ErrorMessage = errorList.FirstOrDefault()
                };
            }

            return billToDate;
        }

        private List<BillingModel> GetBillInfo(int clientId, string customerId, string accountId)
        {
            var billingManager = _container.Resolve<IBilling>();
            return billingManager.GetAllServicesFromLastBill(clientId, customerId, accountId).ToList();
        }

        public List<RateModel.Reading> GetAmiReadings(int clientId, string accountId, string meterId, DateTime startDate, DateTime endDate, out int noOfDays, out int amiUomId)
        {
            amiUomId = 99;
            var amiManager = _container.Resolve<ITallAMI>();
            var clientConfigFacade = _container.Resolve<IClientConfigFacade>();
            // sms settings
            var settings = clientConfigFacade.GetClientSettings(clientId);

            var clientTimezone = (Enums.Timezone)Enum.Parse(typeof(Enums.Timezone), Convert.ToString(settings.TimeZone));

            var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(clientTimezone.GetDescriptionOfEnum());

            var startDateTime = TimeZoneInfo.ConvertTimeToUtc(Convert.ToDateTime(startDate), timeZoneInfo);
            var endDateTime = TimeZoneInfo.ConvertTimeToUtc(new DateTime(endDate.Year, endDate.Month, endDate.Day, 23, 59, 59), timeZoneInfo);
            
            var amiList = amiManager.GetAmiList(startDateTime, endDateTime, meterId, accountId, clientId);

            TallAmiModel[] tallAmiModels = null;

            if (amiList != null)
            {
                tallAmiModels = amiList as TallAmiModel[] ?? amiList.ToArray();

                if (tallAmiModels.Any())
                {
                    amiUomId = tallAmiModels.First().UOMId;
                }
            }

            return amiManager.GetReadings(tallAmiModels, out noOfDays);
        }

        public MeteredServiceList GetMeteredSericeList(List<BillingModel> bills, int clientId, out DateTime billDate)
        {
            var rateCompanyId = _btdSettings.RateCompanyId;
            var meteredServiceList = new MeteredServiceList();
            billDate = DateTime.MaxValue;

            var meterBills = bills.FindAll(b => b.MeterType.Equals("ami",StringComparison.InvariantCultureIgnoreCase));

            foreach (var meterBill in meterBills)
            {
                DateTime currentStartDate;
                var currentEndDate = DateTime.Today.Date;
                BillCycleScheduleModel currentBillCycle;
                if (_btdSettings.Settings.General.UseProjectedNumDaysforBilltoDate)
                {
                    currentStartDate = meterBill.EndDate.AddDays(1);
                    var endDate = currentStartDate.AddDays(_btdSettings.Settings.General.ProjectedNumberOfDays - 1);

                    // exclude cost to date calculation if the computed cycle is in the past; bill to date/cost to date should always be current
                    if (endDate < DateTime.Today.Date)
                    {
                        break;
                    }

                    currentBillCycle = new BillCycleScheduleModel
                    {
                        BeginDate = currentStartDate.ToString("MM-dd-yyyy"),
                        EndDate = endDate.ToString("MM-dd-yyyy")
                    };
                    
                }
                else
                {
                    var billCycleScheduleManager = _container.Resolve<IBillingCycleSchedule>();
                    currentBillCycle = billCycleScheduleManager.GetBillingCycleScheduleByDateAsync(clientId,
                        meterBill.BillCycleScheduleId, currentEndDate);

                    // use bill cycle schedule to get start date of current bill cycle
                    // if bill cycle is not avaliable use the next day of last bill's end date as current bill cycle start
                    if (currentBillCycle == null)
                    {
                        currentStartDate = meterBill.EndDate.AddDays(1);
                        currentBillCycle = billCycleScheduleManager.GetBillingCycleScheduleByDateAsync(clientId,
                            meterBill.BillCycleScheduleId, currentStartDate);
                    }
                    else
                    {
                        currentStartDate = Convert.ToDateTime(currentBillCycle.BeginDate);
                    }
                }

                // next read date
                var currentProjectedEndDate = currentBillCycle?.EndDate;

                // if the projected end date/next read date is older than today, don't include the service
                if (Convert.ToDateTime(currentProjectedEndDate) < currentEndDate)
                {
                    break;
                }

                int noOfDays;
                int amiUomId; 
                var readings = GetAmiReadings(clientId, meterBill.AccountId, meterBill.MeterId, currentStartDate, currentEndDate,
                    out noOfDays, out amiUomId);

                if (noOfDays > 0)
                {
                    if (currentStartDate < billDate)
                    {
                        billDate = currentStartDate;
                    }

                    var fuel = (Enums.CommodityType) meterBill.CommodityId;

                    MeteredService meteredService;
                    if (fuel == Enums.CommodityType.Sewer) {
                        if (
                            meteredServiceList.Exists(
                                m => m.Meters.Exists(meter => meter.MeterID == meterBill.MeterId) &&
                                    //m.ServiceID == meterBill.ServiceContractId && 
                                    m.PremiseID == meterBill.PremiseId &&
                                    m.Fuel == FuelType.water))
                        {
                            meteredService =
                                meteredServiceList.Find(
                                    m =>
                                        m.PremiseID == meterBill.PremiseId &&
                                        m.Fuel == FuelType.water);
                            var meter =
                                meteredService?.Meters.Find(
                                    m => m.MeterID == meterBill.MeterId && m.Fuel == FuelType.water);
                            if (meter != null)
                            {
                                meter.RateClass2 = meterBill.RateClass;
                                meter.DerivedFuel = DerivedFuelType.sewer;
                                if (_btdSettings.Settings.Sewer.UseSewerMaximumMonthsUsageForResidential)
                                    meter.MaximumDerivedUsage = GetTotalSeriveUsageMonthsForSewer(clientId,
                                        meterBill.CustomerId, meterBill.AccountId, meterBill.MeterId,
                                        meterBill.ServiceContractId);
                            }
                            else
                            {
                                meter = new Meter
                                {
                                    MeterID = meterBill.MeterId,
                                    RateCompanyID = rateCompanyId,
                                    ServiceID = meterBill.ServiceContractId,
                                    RateClass = meterBill.RateClass,
                                    Fuel = FuelType.water,
                                    StartDate = currentStartDate,
                                    EndDate = readings.OrderByDescending(e => e.Timestamp).FirstOrDefault()?.Timestamp,
                                    Readings = readings,
                                    PrimaryUnitOfMeasure =
                                        Map.ConvertUnitOfMeasureType(
                                            ((Enums.UomType)amiUomId).ToString()),
                                    RateClass2 = meterBill.RateClass,
                                    DerivedFuel = DerivedFuelType.sewer
                                };

                                if (_btdSettings.Settings.Sewer.UseSewerMaximumMonthsUsageForResidential)
                                    meter.MaximumDerivedUsage = GetTotalSeriveUsageMonthsForSewer(clientId,
                                        meterBill.CustomerId, meterBill.AccountId, meterBill.MeterId,
                                        meterBill.ServiceContractId);

                                if (!_btdSettings.Settings.General.UseProjectedNumDaysforBilltoDate)
                                    meter.ProjectedEndDate = Convert.ToDateTime(currentProjectedEndDate);

                                meteredService?.Meters.Add(meter);
                            }
                        }
                        else
                        {
                            meteredService = new MeteredService(meterBill.ServiceContractId, meterBill.PremiseId,
                                FuelType.water);
                            var meter = new Meter
                            {
                                MeterID = meterBill.MeterId,
                                RateCompanyID = rateCompanyId,
                                ServiceID = meterBill.ServiceContractId,
                                RateClass = meterBill.RateClass,
                                Fuel = FuelType.water,
                                StartDate = currentStartDate,
                                EndDate = readings.OrderByDescending(e => e.Timestamp).FirstOrDefault()?.Timestamp,
                                Readings = readings,
                                PrimaryUnitOfMeasure =
                                    Map.ConvertUnitOfMeasureType(
                                        ((Enums.UomType)amiUomId)
                                            .ToString()),
                                RateClass2 = meterBill.RateClass,
                                DerivedFuel = DerivedFuelType.sewer
                            };

                            if (_btdSettings.Settings.Sewer.UseSewerMaximumMonthsUsageForResidential)
                                meter.MaximumDerivedUsage = GetTotalSeriveUsageMonthsForSewer(clientId,
                                    meterBill.CustomerId, meterBill.AccountId, meterBill.MeterId,
                                    meterBill.ServiceContractId);

                            if (!_btdSettings.Settings.General.UseProjectedNumDaysforBilltoDate &&
                                currentProjectedEndDate != null)
                                meter.ProjectedEndDate = Convert.ToDateTime(currentProjectedEndDate);

                            meteredService.Meters.Add(meter);
                            meteredServiceList.Add(meteredService);
                        }
                    }
                    else
                    {
                        var meter = new Meter
                        {
                            MeterID = meterBill.MeterId,
                            RateCompanyID = rateCompanyId,
                            ServiceID = meterBill.ServiceContractId,
                            RateClass = meterBill.RateClass,
                            Fuel = Map.ConvertFuelType(((Enums.CommodityType) meterBill.CommodityId).ToString()),
                            StartDate = currentStartDate,
                            EndDate = readings.OrderByDescending(e => e.Timestamp).FirstOrDefault()?.Timestamp,
                            Readings = readings,
                            PrimaryUnitOfMeasure =
                                Map.ConvertUnitOfMeasureType(
                                    ((Enums.UomType)amiUomId)
                                        .ToString())
                        };

                        if (!_btdSettings.Settings.General.UseProjectedNumDaysforBilltoDate &&
                            currentProjectedEndDate != null)
                            meter.ProjectedEndDate = Convert.ToDateTime(currentProjectedEndDate);

                        //multi meter in the same service with same premise and fuel
                        if (
                            meteredServiceList.Exists(
                                m => m.Meters.Exists(m2 => m2.MeterID == meterBill.MeterId) &&
                                     //m.ServiceID == meterBill.ServiceContractId && 
                                     m.PremiseID == meterBill.PremiseId &&
                                    m.Fuel ==
                                    Map.ConvertFuelType(((Enums.CommodityType) meterBill.CommodityId).ToString())))
                        {
                            meteredService =
                                meteredServiceList.Find(
                                    m =>
                                        //m.ServiceID == meterBill.ServiceContractId && 
                                        m.PremiseID == meterBill.PremiseId &&
                                        m.Fuel ==
                                        Map.ConvertFuelType(((Enums.CommodityType) meterBill.CommodityId).ToString()));
                            if (meteredService.Fuel == FuelType.water &&
                                meteredService.Meters.Exists(
                                    m =>
                                        m.MeterID == meterBill.MeterId && m.Fuel == FuelType.water &&
                                        m.DerivedFuel == DerivedFuelType.sewer))
                            {
                                meter =
                                    meteredService.Meters.Find(
                                        m => m.MeterID == meterBill.MeterId && m.Fuel == FuelType.water);
                                meter.RateClass = meterBill.RateClass;
                                meter.StartDate = currentStartDate;
                                meter.EndDate = readings.OrderByDescending(e => e.Timestamp).FirstOrDefault()?.Timestamp;
                                meter.Readings = readings;
                                meter.PrimaryUnitOfMeasure =
                                    Map.ConvertUnitOfMeasureType(
                                        ((Enums.UomType)amiUomId)
                                            .ToString());
                            }
                            else
                                meteredService.Meters.Add(meter);
                        }
                        else
                        {
                            meteredService = new MeteredService(meterBill.ServiceContractId, meterBill.PremiseId,
                                Map.ConvertFuelType(((Enums.CommodityType) meterBill.CommodityId).ToString()));
                            meteredService.Meters.Add(meter);
                            meteredServiceList.Add(meteredService);
                        }
                    }


                }
            }

            return meteredServiceList;
        }

        public NonMeteredServiceList GetNonMeteredServiceList(List<BillingModel> bills, int clientId, DateTime billDate)
        {
            var nonMeteredServiceList = new NonMeteredServiceList();
            var nonMeterBills =
                bills.FindAll(b => b.MeterType.Equals("nonmetered", StringComparison.InvariantCultureIgnoreCase));

            foreach (var nonMeteredBill in nonMeterBills)
            {
                // check if there's non meter service from last bill; if too old, exclude it
                var lastBillEndDate = billDate.AddDays(-1 * (nonMeteredBill.BillDays + 1));

                if (nonMeteredBill.EndDate > lastBillEndDate)
                {
                    var nonMeteredService =
                    new NonMeteredService(((Enums.CommodityType)nonMeteredBill.CommodityId).ToString(),
                        nonMeteredBill.TotalCost, nonMeteredBill.TotalUsage ?? 0,
                        nonMeteredBill.UOMId == null
                            ? Enums.UomType.Other.ToString().ToLower()
                            : ((Enums.UomType)nonMeteredBill.UOMId).ToString(), billDate, DateTime.Today.Date);

                    BillCycleScheduleModel currentBillCycle;
                    if (_btdSettings.Settings.General.UseProjectedNumDaysforBilltoDate)
                    {
                        var endDate = billDate.AddDays(_btdSettings.Settings.General.ProjectedNumberOfDays - 1);

                        // exclude cost to date calculation if the computed cycle is in the past; bill to date/cost to date should always be current
                        if (endDate < DateTime.Today.Date)
                        {
                            break;
                        }

                        currentBillCycle = new BillCycleScheduleModel
                        {
                            BeginDate = billDate.ToString("MM-dd-yyyy"),
                            EndDate = endDate.ToString("MM-dd-yyyy")
                        };
                    }
                    else
                    {
                        var billCycleScheduleManager = _container.Resolve<IBillingCycleSchedule>();
                        currentBillCycle = billCycleScheduleManager.GetBillingCycleScheduleByDateAsync(clientId,
                            nonMeteredBill.BillCycleScheduleId, billDate);
                    }

                    // next read date
                    var currentProjectedEndDate = currentBillCycle?.EndDate;

                    // if the projected end date/next read date is older than today, don't include the service
                    if (Convert.ToDateTime(currentProjectedEndDate) < DateTime.Today.Date)
                    {
                        break;
                    }

                    if (!_btdSettings.Settings.General.UseProjectedNumDaysforBilltoDate)
                    {
                        nonMeteredService.ProjectedEndDate = Convert.ToDateTime(currentProjectedEndDate);
                    }

                    nonMeteredServiceList.Add(nonMeteredService);
                }
            }
            return nonMeteredServiceList;
        }

        public double GetTotalSeriveUsageMonthsForSewer(int clientId, string customerId, string accountId,
            string meterId, string serviceContractId)
        {
            double totalServiceUsage = 0;
            var today = DateTime.Today.Date;

            var billingManager = _container.Resolve<IBilling>();
            var bills =
                billingManager.GetBillsForService(clientId, customerId, accountId, serviceContractId, today.AddYears(-3))
                    .ToList();

            bills.RemoveAll(b => b.IsFault.HasValue && (bool) b.IsFault);

            if (bills.Count > 1)
            {
                var sortedBills =
                    bills.FindAll(
                        b =>
                            b.EndDate.Month == 1 || b.EndDate.Month == 2 || b.EndDate.Month == 3 ||
                            b.EndDate.Month == 12).OrderByDescending(b => b.EndDate).Take(4);

                var billingModels = sortedBills as BillingModel[] ?? sortedBills.ToArray();

                if (billingModels.Length > 2)
                {
                    var totalUsage =
                        billingModels.OrderByDescending(b => b.TotalUsage ?? 0).ToList()[1]
                            .TotalUsage;

                    if (totalUsage != null)
                    {
                        totalServiceUsage = totalUsage.Value;
                    }
                }
            }


            return totalServiceUsage;
        }

        public void ApplyContent(BillToDateResponse response, ClientUser clientUser, string locale)
        {
            //content
            var cf = new ContentModelFactoryContentful();
            var cp = cf.CreateContentProvider(clientUser.ClientID);
            var contentLocale = cp.GetContentLocale(locale);

            //get label content and configuration
            response.Content = new Content
            {
                TextContent = cp.GetContentTextContent("billtodate", contentLocale, string.Empty),
                Configuration = cp.GetContentConfiguration("billtodate", string.Empty),
                Commodity = cp.GetContentCommodity(string.Empty),
                UOM = cp.GetContentUom(string.Empty),
                Currency = cp.GetContentCurrency(string.Empty)
            };
        }

        private Meter GetMeter(BillToDateMeter meter, string commodityKey)
        {
            var m = new Meter(meter.Id, _btdSettings.RateCompanyId, meter.RateClass, Map.ConvertFuelType(commodityKey), Convert.ToDateTime(meter.StartDate), Convert.ToDateTime(meter.EndDate));
            if (meter.ProjectedEndDate != null && !_btdSettings.Settings.General.UseProjectedNumDaysforBilltoDate)
            {
                m.ProjectedEndDate = Convert.ToDateTime(meter.ProjectedEndDate);
            }

            m.PrimaryUnitOfMeasure = Map.ConvertUnitOfMeasureType(meter.UomKey);

            if (!string.IsNullOrEmpty(meter.SewerRateClass))
            {
                m.DerivedFuel = DerivedFuelType.sewer;
                m.RateClass2 = meter.SewerRateClass;
            }

            m.Readings = new List<RateModel.Reading>();

            foreach (var reading in meter.Readings)
            {
                var quantity = reading.Reading;
                var timestamp = Convert.ToDateTime(reading.Timestamp);
                var tou = RateModel.Enums.TimeOfUse.Undefined;

                switch (reading.TouBin.ToLower())
                {
                    case "peak":
                        tou = RateModel.Enums.TimeOfUse.OnPeak;
                        break;
                    case "offpeak":
                        tou = RateModel.Enums.TimeOfUse.OffPeak;
                        break;
                    case "shoulder1":
                        tou = RateModel.Enums.TimeOfUse.Shoulder1;
                        break;
                    case "shoulder2":
                        tou = RateModel.Enums.TimeOfUse.Shoulder2;
                        break;
                    case "criticalpeak":
                        tou = RateModel.Enums.TimeOfUse.CriticalPeak;
                        break;
                }

                if (reading.Direction != null)
                {
                    var direction = reading.Direction;

                    if (direction.Equals("reserver", StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (quantity > 0)
                        {
                            quantity = quantity*-1;
                        }
                    }
                }

                //    'check if there's already a reading for the same time in the collection; if so, net it
                var addReading = true;
                if (m.Readings.Count > 0)
                {
                    var index = m.Readings.FindIndex(r => r.Timestamp == timestamp);
                    if (index != -1)
                    {
                        if (m.Readings[index].TimeOfUse.Equals(tou))
                        {
                            addReading = false;
                            m.Readings[index].Quantity = m.Readings[index].Quantity + quantity;
                        }
                    }
                }

                if (addReading)
                {
                    if (tou.Equals(RateModel.Enums.TimeOfUse.Undefined))
                    {
                        m.Readings.Add(new RateModel.Reading(RateModel.Enums.BaseOrTier.TotalServiceUse,
                            RateModel.Enums.TimeOfUse.Undefined, RateModel.Enums.Season.Undefined, timestamp, quantity,
                            false));
                    }
                    else
                    {
                        m.Readings.Add(new RateModel.Reading(RateModel.Enums.BaseOrTier.Undefined, tou,
                            RateModel.Enums.Season.Undefined, timestamp, quantity, false));
                    }
                }
            }

            return m;
        }

        private List<string> GetLoggingInfo(IBillToDate billToDateCalculator, StatusList statusList)
        {
            List<string> logEntries = null;
            var logSessionMessage = string.Empty;

            foreach (var status in statusList)
            {
                if (status.Exception.Message.Contains("RateModelLogSessionID"))
                {
                    logSessionMessage = status.Exception.Message;
                }
            }

            if (logSessionMessage != string.Empty)
            {
                var logSession = logSessionMessage.Split('=');
                var logSessionId = logSession[1].Trim();

                logEntries = billToDateCalculator.GetLoggingInfo(logSessionId);

            }

            return logEntries;
        }

        private static void InitializeCalculationModelMapper()
        {
            AutoMapperConfig.ForAllUnmappedMembers(Mapper.CreateMap<CalculationModel, CE.Models.Insights.BillToDate>()
                    .ForMember(dest => dest.TotalCost, opts => opts.MapFrom(src => src.Cost.HasValue ? Math.Round(src.Cost.Value, 1) : 0))
                    .ForMember(dest => dest.BillStartDate, opts => opts.MapFrom(src => src.BillCycleStartDate))
                    .ForMember(dest => dest.BillEndDate, opts => opts.MapFrom(src => src.BillCycleEndDate))
                    .ForMember(dest => dest.TotalProjectedCost, opts => opts.MapFrom(src => src.ProjectedCost == null ? 0 : Math.Round(src.ProjectedCost.Value, 1)))
                    .ForMember(dest => dest.AverageDailyCost, opts => opts.MapFrom(src => src.AverageDailyCost.HasValue ? Math.Round(src.AverageDailyCost.Value, 1) : 0))
                    .ForMember(dest => dest.BillDays, opts => opts.MapFrom(src => Math.Floor((Convert.ToDateTime(src.BillCycleEndDate) - Convert.ToDateTime(src.BillCycleStartDate)).TotalDays + 1))), o => o.Ignore());

            AutoMapperConfig.ForAllUnmappedMembers(Mapper.CreateMap<CalculationModel, BillToDateService>()
                    .ForMember(dest => dest.Id, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.MeterId) ? ((Enums.CommodityType)src.CommodityId).ToString().ToLower() : src.MeterId))
                    .ForMember(dest => dest.StartDate, opts => opts.MapFrom(src => src.BillCycleStartDate))
                    .ForMember(dest => dest.EndDate, opts => opts.MapFrom(src => src.BillCycleEndDate))
                    .ForMember(dest => dest.CommodityKey, opts => opts.MapFrom(src =>  src.CommodityId == null ? string.Empty : ((Enums.CommodityType)src.CommodityId).ToString().ToLower()))
                    .ForMember(dest => dest.ReadDate, opts => opts.MapFrom(src => src.AsOfAmiDate.HasValue ? src.AsOfAmiDate.ToString() : src.AsOfCalculationDate.ToShortDateString()))
                    .ForMember(dest => dest.DaysIntoCycle, opts => opts.MapFrom(src => Convert.ToInt32(src.BillDays ?? 0)))
                    .ForMember(dest => dest.UseToDate, opts => opts.MapFrom(src => src.Usage == null ? 0 : Convert.ToDecimal(Math.Round(src.Usage.Value, 2))))
                    .ForMember(dest => dest.UseProjected, opts => opts.MapFrom(src => src.ProjectedUsage == null ? 0 : Convert.ToDecimal(Math.Round(src.ProjectedUsage.Value,2))))
                    .ForMember(dest => dest.CostToDate, opts => opts.MapFrom(src => src.Cost == null ? 0 : Convert.ToDecimal(Math.Round(src.Cost.Value, 1))))
                    .ForMember(dest => dest.CostProjected, opts => opts.MapFrom(src => src.ProjectedCost == null ? 0 : Convert.ToDecimal(Math.Round(src.ProjectedCost.Value, 1))))
                    .ForMember(dest => dest.UomKey, opts => opts.MapFrom(src => src.Unit == null ? Enums.UomType.Other.ToString() : ((Enums.UomType)src.Unit).ToString()))
                    .ForMember(dest => dest.RateClass, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.RateClass) ? string.Empty : src.RateClass)), o => o.Ignore());
        }        


        private async Task<CalculationModel> GetBtdAsync(int clientId, string accountId)
        {
            var calculationManager = _container.Resolve<ICalculation>();
            var task = await Task.Factory.StartNew(() => calculationManager.GetBtdCalculationAsync(clientId, accountId)).ConfigureAwait(false);
            return task.Result;
        }
        
        private async Task<CalculationModel> GetCtdAsync(int clientId, string accountId, string serviceContractId)
        {
            var calculationManager = _container.Resolve<ICalculation>();
            var task = await Task.Factory.StartNew(() => calculationManager.GetCtdCalculationAsync(clientId, accountId, serviceContractId)).ConfigureAwait(false);
            return task.Result;
        }
    }
}