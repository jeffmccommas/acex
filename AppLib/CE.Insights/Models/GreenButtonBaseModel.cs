﻿using System;
using System.Collections.Generic;
using System.Linq;
//using System.Net.Http;
using AO.BusinessContracts;
using CE.AO.Models;
using CE.AO.Utilities;
using CE.GreenButtonConnect;
using CE.Insights.Helpers;
using CE.Models;
using CE.Models.Insights;
using CE.Models.Insights.GreenButtonConnect;
using CE.Models.Insights.Types;
using Microsoft.Practices.Unity;
using Enums = CE.AO.Utilities.Enums;

namespace CE.Insights.Models
{
    public class GreenButtonBaseModel : ConsumptionBaseModel
    {
        protected readonly int ClientId ;

        protected static IInsightsEFRepository InsightsEfRepository;
        protected readonly GreenButtonConfig GreenButtonSettings;
        protected readonly IGreenButtonModel GreenButtonModel;
        protected List<GreenButtonAuthorization> Authorizations;

        private const string DataCustodianLinkPrefix = "/Datacustodian/espi/1_1/resource";

        //private const int UomidNotApplicable = 0;
        private const int UomidCubicFeet = 119;
        private const int UomidTherm = 169;
        private const int UomidWattHours = 72;
        private const int UomidCubicMeter = 42;
        private const int UomidGal = 128;
        private const int KindGas = 1;
        //private const int KindWater = 2;

        public GreenButtonBaseModel(string accessToken, string locale, string greenButtonDomain, IUnityContainer unityContainer, IInsightsEFRepository insightsEfRepository) : base(unityContainer)
        {
            if (insightsEfRepository == null)
            {
                throw new ArgumentNullException($"InsightsRepository is null!");
            }
            InsightsEfRepository = insightsEfRepository;
            ClientId = GetClientId(accessToken);
            GreenButtonSettings = GreenButtonHelper.GetGreenButtonConnectSettings(ClientId, locale);
            GreenButtonModel = new GreenButtonModel(ClientId, greenButtonDomain, GreenButtonSettings, insightsEfRepository);
        }

        /// <summary>
        /// unit test
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="greenButtonDomain"></param>
        /// <param name="authorizations"></param>
        /// <param name="unityContainer"></param>
        /// <param name="insightsEfRepository"></param>
        /// <param name="config"></param>
        public GreenButtonBaseModel(int clientId, string greenButtonDomain, List<GreenButtonAuthorization> authorizations, IUnityContainer unityContainer, IInsightsEFRepository insightsEfRepository, GreenButtonConfig config) : base(unityContainer)
        {
            if (insightsEfRepository == null)
            {
                throw new ArgumentNullException($"InsightsRepository is null!");
            }
            ClientId = clientId;
            InsightsEfRepository = insightsEfRepository;
            GreenButtonSettings = config;
            GreenButtonModel = new GreenButtonModel(clientId, greenButtonDomain, GreenButtonSettings,insightsEfRepository);
            
            Authorizations = authorizations;
        }
        
        public GreenButtonResult GetSerivceStatus(string accessToken)
        {
            if (Authorizations == null || Authorizations.FindAll(a => a.AccessToken == accessToken).Count == 0)
                return new GreenButtonResult { Status = GreenButtonErrorType.InvalidAuthorization };

            //check accessToken type
            var authorization = Authorizations.Find(a => a.AccessToken == accessToken);

            if (authorization.AccessTokenType == GreenButtonAccessTokenType.Registration)
            {
                //Check for enddate
                var endDate = authorization.EndDate;
                if (endDate != null)
                {
                    if (endDate < DateTime.UtcNow)
                        return GreenButtonModel.GenerateServiceStatus(false);
                }
                // check for duration
                var dur = authorization.AuthorizationTimePeriod.Duration ?? 0;
                var stDate = authorization.AuthorizationTimePeriod.Start ?? 0;
                var timeValid = Helper.ConvertFromUnixTimestamp(stDate + dur);
                if (!(timeValid > DateTime.UtcNow))
                    return GreenButtonModel.GenerateServiceStatus(false);
                // check for active status
                if (!authorization.Active)
                    return GreenButtonModel.GenerateServiceStatus(false);

                return GreenButtonModel.GenerateServiceStatus(true);
            }
            return GreenButtonModel.GenerateServiceStatus(false);
        }

        public GreenButtonResult GetApplicationInfo(string accessToken, long applicationInfoId)
        {
            if (Authorizations == null || Authorizations.FindAll(a => a.AccessToken == accessToken).Count == 0)
                return new GreenButtonResult {Status = GreenButtonErrorType.InvalidAuthorization };

            var authorization = Authorizations.Find(a => a.AccessToken == accessToken);

            if (applicationInfoId != authorization.ApplicationInformationId)
                return new GreenButtonResult { Status = GreenButtonErrorType.InvalidApplicationInfoErr };

            var applicationInfo = InsightsEfRepository.GetGreenButtonApplicationInformation(applicationInfoId);
            if (applicationInfo == null)
                return new GreenButtonResult { Status = GreenButtonErrorType.InvalidApplicationInfoErr };

            return GreenButtonModel.GenerateApplicationInfo(applicationInfo);

        }

        public GreenButtonAmiReadingType GetReadingType(long subscriptionId, string meterId, string readingTypeId, ref GreenButtonErrorType status)
        {
            // call database to get readingTypes
            var readingType = InsightsEfRepository.GetGreenButtonAmiReadingType(subscriptionId,meterId,readingTypeId); // or search by reading type id

            if (readingType == null)
                status = GreenButtonErrorType.NoReadingTypeErr;

            return readingType;
        }


        public List<GreenButtonAmiReadingType> GetReadingTypes(string accountId, string meterId, DateTime? startDate, DateTime? endDate, GreenButtonScope scope,
            ref GreenButtonErrorType status)
        {
            // call database to get readingTypes
            var readingTypes = InsightsEfRepository.GetGreenButtonAmiReadingTypes(meterId); // or search by reading type id


            if (readingTypes.Count == 0)
            {
                var amiList = GetAmiData(accountId, meterId, startDate, endDate, scope, false, ref status);
                if (status == GreenButtonErrorType.NoError)
                {
                    if (amiList.Count > 0)
                    {
                        readingTypes = GetReadingTypes(amiList, readingTypes, scope);
                        if (readingTypes.Count == 0)
                            status = GreenButtonErrorType.CommodityOrIntervalNotSupportErr;
                    }
                    else
                    {
                        status = GreenButtonErrorType.NoAmiErr;
                    }
                }


            }

            foreach (var readingType in readingTypes)
            {
                readingType.BlockDuratioin = scope.BlockDuration ?? GreenButtonDurationType.Daily;
            }

            return readingTypes;
        }

        public List<GreenButtonAmiReadingType> GetReadingTypes(List<TallAmiModel> amiList, GreenButtonScope scope, bool isBatch)
        {
            var meterId = amiList[0].MeterId;

            // call database to get readingTypes
            var readingTypes = InsightsEfRepository.GetGreenButtonAmiReadingTypes(meterId); // or search by reading type id

            readingTypes = GetReadingTypes(amiList, readingTypes, scope);

            foreach (var readingType in readingTypes)
            {
                readingType.BlockDuratioin = scope.BlockDuration ?? GreenButtonDurationType.Daily;
            }


            // if batched, convert tall ami model list into IntervalConsumptionData foreach reading type
            if (isBatch)
                ConsumptionDataMapping(readingTypes, amiList);

            return readingTypes;
        }

        protected GreenButtonAmiReadingType GetReadingTypeWithConsumption(long subscriptionId, string accountId, string meterId, string readingTypeId, GreenButtonScope scope, DateTime? startDate, DateTime? endDate, ref GreenButtonErrorType status)
        {
            // call database to get readingTypes
            var readingType = InsightsEfRepository.GetGreenButtonAmiReadingType(subscriptionId, meterId, readingTypeId); // or search by reading type id

            if (readingType == null)
                status = GreenButtonErrorType.NoReadingTypeErr;
            else
                readingType.BlockDuratioin = scope.BlockDuration ?? GreenButtonDurationType.Daily;

            // Get ami Reading
            var amiModelList = GetAmiData(accountId, meterId, startDate, endDate, scope, true, ref status);
            if(status == GreenButtonErrorType.NoError)
            {
                if (amiModelList.Count > 0)
                {
                    ConsumptionDataMapping(readingType, amiModelList);

                }
                else
                {
                    status = GreenButtonErrorType.NoAmiErr;
                }
            }

            return readingType;
        }

        protected GreenButtonErrorType GetUsagePoint(string accountId, GreenButtonScope scope, GreenButtonUsagePoint usagePoint, DateTime? startDate, DateTime? endDate, bool isBatch)
        {
            if (isBatch || !usagePoint.TimeConfigurationId.HasValue)
            {
                var status = GreenButtonErrorType.NoError;
                // Get ami Reading
                var amiModelList = GetAmiData(accountId, usagePoint.MeterId, startDate, endDate, scope, false, ref status);
                if(status != GreenButtonErrorType.NoError)
                    return status;
                if (amiModelList.Count > 0)
                {

                    // if batch, gather reading types info 
                    if (isBatch)
                    {
                        var readingTypes = GetReadingTypes(amiModelList, scope, true);
                        if (readingTypes.Count == 0)
                            return GreenButtonErrorType.CommodityOrIntervalNotSupportErr;

                        readingTypes.RemoveAll(r => r.ConsumptionList == null);
                        if (readingTypes.Count == 0)
                            return GreenButtonErrorType.NoAmiErr;

                        usagePoint.ReadingTypes = readingTypes;
                    }

                    // check if time configuration is setup, if not, update it
                    if (usagePoint.TimeConfigurationId == null)
                    {
                        var timezone = amiModelList[0].Timezone;
                        var timezoneEnum = (Enums.Timezone)Enum.Parse(typeof(Enums.Timezone), timezone.ToUpper());
                        var timezoneId = TimeZoneInfo.FindSystemTimeZoneById(timezoneEnum.GetDescriptionOfEnum());

                        // update time configuration id
                        usagePoint.TimeConfigurationId = InsightsEfRepository.UpdateLocalTimeConfiguration(usagePoint.Id, timezoneId);
                    }
                }
                else
                {
                    return GreenButtonErrorType.NoAmiErr;
                }
            }

            // Get Usage Point Related Links
            var relatedLinks = InsightsEfRepository.GetGreenButtonAmiUsagePointRelatedLinks(usagePoint);
            usagePoint.Links.AddRange(relatedLinks);
           

            return GreenButtonErrorType.NoError;
        }
        
        private List<GreenButtonAmiReadingType> GetReadingTypes(List<TallAmiModel> amiList, List<GreenButtonAmiReadingType> readingTypes, GreenButtonScope scope)
        { 
            // if nothing, create reading type)
            if (readingTypes.Count == 0)
            {
                readingTypes = GenerateReadingTypes(amiList, scope);
            }
            else
            {
                var intervalList = amiList.Select(a => a.IntervalType).Distinct().ToList();
                var amiListMissingReadingType = new List<TallAmiModel>();
                foreach (var intervalId in intervalList)
                {
                    var resolution = (ResolutionType)intervalId;
                    var intervalLength = CE.Models.Insights.GreenButton.GetResolutionDuration(resolution);
                    if (IsIntervalLengthSupported(intervalLength, scope.IntervalDurations)
                        && !readingTypes.Exists(r => r.IntervalLength == Convert.ToInt32(intervalLength)))
                    {
                        var amiMissingReadingType = amiList.FindAll(a => a.IntervalType == intervalId);
                        amiListMissingReadingType.AddRange(amiMissingReadingType);
                    }
                }
                if (amiListMissingReadingType.Count > 0)
                {
                    var missingReadingType = GenerateReadingTypes(amiListMissingReadingType, scope);
                    readingTypes.AddRange(missingReadingType);
                }
            }
            return readingTypes;
        }

        private List<TallAmiModel> GetAmiData(string accountId, string meterId, DateTime? startDate, DateTime? endDate, GreenButtonScope scope, bool isIntervalBlockRequest, ref GreenButtonErrorType status)
        {
            ITallAMI amiManager = Container.Resolve<ITallAMI>();

            // hard-coded for now 
            var rangeMax = (int)scope.SubscriptionFrequency; 

            if (scope.FunctionBlocks.Exists(fb => fb == GreenButtonFunctionBlockType.QueryParmeters) || isIntervalBlockRequest)
            {
                if (startDate.HasValue || endDate.HasValue)
                {
                    rangeMax = GreenButtonSettings.MaxQueryDateRange;
                }
                endDate = endDate?.Date.AddDays(1) ?? DateTime.UtcNow.Date;

                startDate = startDate?.Date ?? endDate.Value.Date.AddDays(scope.HistoryLength * -1);
            }
            else
            {
                if (startDate.HasValue || endDate.HasValue)
                {
                    status = GreenButtonErrorType.QueryParameterNotSupportErr;
                    return new List<TallAmiModel>();
                }

                endDate = DateTime.UtcNow.Date;

                startDate = endDate.Value.Date.AddDays(scope.HistoryLength * -1);
            }


            // Get ami Reading
            IEnumerable<TallAmiModel> tallAmiModels = amiManager.GetAmiList(startDate.Value, endDate.Value,
                meterId, accountId, ClientId);

            if (tallAmiModels != null)
            {
                var amiModelList = tallAmiModels as List<TallAmiModel> ?? tallAmiModels.ToList();

                if(amiModelList.Count > 0)
                {
                    var daysOfData = (endDate.Value - startDate.Value).TotalDays;

                    // filter the ami list if the requested range is greater than max range
                    if (daysOfData > rangeMax)
                    {
                        endDate = amiModelList.Max(a => a.AmiTimeStamp);
                        startDate = endDate.Value.Date.AddDays(-1 * (rangeMax - 1));

                        amiModelList = amiModelList.Where(a => a.AmiTimeStamp >= startDate).ToList();
                    }

                    // filter out all interval readings if interval metering is not supported
                    if (!IsIntervalMeteringSupported(scope.FunctionBlocks))
                    {
                        amiModelList = amiModelList.Where(a => a.IntervalType == 24).ToList();

                        if (amiModelList.Count == 0)
                        {
                            status = GreenButtonErrorType.IntervalMeteringNotSupportErr;
                            return new List<TallAmiModel>();
                        }
                    }
                }
                
                return amiModelList;
            }
            return new List<TallAmiModel>();
        }

        private
            void ConsumptionDataMapping(List<GreenButtonAmiReadingType> readingTypes, List<TallAmiModel> amiList)
        {
            
            foreach (var readingType in readingTypes)
            {
                ConsumptionDataMapping(readingType, amiList);
            }
        }

        private void ConsumptionDataMapping(GreenButtonAmiReadingType readingType, List<TallAmiModel> amiList)
        {

            ITallAMI amiManager = Container.Resolve<ITallAMI>();

            var intervalId = ConvertIntervalLenghtToIntervalId(readingType.IntervalLength);

            var intervalAmiList = amiList.Where(a => a.IntervalType == intervalId).OrderBy(a => a.AmiTimeStamp).ToList();

            if (intervalAmiList.Count > 0)
            {
                AutoMapperSetup();
                var consumptionList = new IntervalConsumptionData
                {
                    ResolutionKey = ((ResolutionType)intervalId).ToString()
                };
                int numOfDays;
                var readings = amiManager.GetReadings(intervalAmiList, out numOfDays, false);
                var consumptionDataList = ConvertRateModelReadingsToConsumptionData(readings, null, null, null);
                consumptionList.ConsumptionDataList = consumptionDataList;
                readingType.ConsumptionList = consumptionList;
            }
        }
        private List<GreenButtonAmiReadingType> GenerateReadingTypes(List<TallAmiModel> amiList, GreenButtonScope scope)
        {
            var meterId = amiList[0].MeterId;
            var clientId = amiList[0].ClientId;
            var intervalList = amiList.Select(a => a.IntervalType).Distinct().ToList();
            var readingTypes = new List<GreenButtonAmiReadingType>();
            foreach (var intervalId in intervalList)
            {
                var intervalAmiList = amiList.Where(a => a.IntervalType == intervalId).ToList();
                
                var publishedDate = DateTime.UtcNow.Date;
                var updateDate = publishedDate;

                // default values
                var flowDirection = 1;
                var dataQualifier = 0;
                var behavior = 4;
                int phase = 0;

                var commodityId = intervalAmiList[0].CommodityId;
                var commodity = (Enums.CommodityType)commodityId;
                var fuelId = GetFuelId(commodity);
                var resolution = (ResolutionType)intervalId;
                var intervalLength = CE.Models.Insights.GreenButton.GetResolutionDuration(resolution);

                if(IsCommoditySupported(commodity, scope.FunctionBlocks) && IsIntervalLengthSupported(intervalLength, scope.IntervalDurations))
                {
                    var timeAttribute = GetTimeAttribute(resolution.ToString());
                    var uom = (Enums.UomType)intervalAmiList[0].UOMId;
                    var uomId = ConvertUOMNameToUomId(uom.ToString());
                    int kind = 0;
                    var powerOfTen = 0;
                    if (commodity == Enums.CommodityType.Gas)
                    {
                        kind = KindGas;
                        if (GreenButtonSettings.ConvertGasUom)
                        {
                            uomId = ConvertUOMNameToUomId(GreenButtonSettings.GasDisplayUom);
                            if (uomId != UomidCubicFeet)
                            {
                                powerOfTen = -3;
                            }
                        }
                        else
                        {
                            if (uomId == UomidTherm)
                            {
                                powerOfTen = -3;
                            }
                        }
                    }

                    var links = new List<GreenButtonLink>();
                    // self link
                    var link = $"{DataCustodianLinkPrefix}/{GreenButtonEntryType.ReadingType.ToString()}";
                    //var encryptedUuId = InsightsEfRepository.Encrypt(uuId);
                    var selfLink = CE.Models.Insights.GreenButton.GenerateGreenButtonLink(link, GreenButtonLinkRelationshipType.self.ToString());
                    links.Add(selfLink);
                    // up link
                    var upLink = CE.Models.Insights.GreenButton.GenerateGreenButtonLink(link, GreenButtonLinkRelationshipType.up.ToString());
                    links.Add(upLink);

                    // generate uuid in CE.Model
                    var readingType = new GreenButtonAmiReadingType
                    {
                        AccumulationBehavior = behavior,
                        Commodity = fuelId,
                        DataQualifier = dataQualifier,
                        FlowDirection = flowDirection,
                        IntervalLength = Convert.ToInt32(intervalLength),
                        Kind = kind,
                        Phase = phase,
                        TimeAttribute = timeAttribute,
                        PowerOfTenMultiplier = powerOfTen,
                        Uom = uomId,
                        PublishedDate = publishedDate,
                        UpdatedDate = updateDate,
                        MeterId = meterId,
                        Links = links
                    };
                    readingTypes.Add(readingType);
                }
                
            }

            // insert reading types
            if(readingTypes.Count > 0)
                InsightsEfRepository.InsertGreenButtonAmiReadingTypes(clientId, meterId,readingTypes);

            return readingTypes;
        }

        /// <summary>
        /// Convert uom name to uom identifier.
        /// </summary>
        /// <param name="uom"></param>
        /// <returns></returns>
        private int ConvertUOMNameToUomId(string uom)
        {

            int result;

            switch (uom.ToUpper())
            {
                case "CCF":
                    result = UomidCubicFeet;
                    break;
                case "THERMS":
                case "THERM":
                    result = UomidTherm;
                    break;
                case "KWH":
                    result = UomidWattHours;
                    break;
                case "CUBICMETER":
                case "CUBIC METER":
                    result = UomidCubicMeter;
                    break;
                case "GAL":
                case "KGAL":
                case "CGAL":
                    result = UomidGal;
                    break;
                default:
                    result = 0;
                    break;
            }

            return result;
        }
        private int ConvertIntervalLenghtToIntervalId(int intervalLength)
        {
            int intervalId = 0;

            switch (intervalLength)
            {
                case 300:
                    intervalId = 5;
                    break;

                case 900:
                    intervalId = (int)Enums.IntervalType.Fifteen;
                    break;

                case 1800:
                    intervalId = (int)Enums.IntervalType.Thirty;
                    break;

                case 3600:
                    intervalId = (int)Enums.IntervalType.Sixty;
                    break;

                case 21600:
                    intervalId = 360;
                    break;

                case 86400:
                    intervalId = (int)Enums.IntervalType.Daily;
                    break;
            }

            return intervalId;
        }
        private int GetFuelId(Enums.CommodityType resource)
        {
            int fuelId = 0;

            if (resource == Enums.CommodityType.Electric)
                fuelId = 0;
            else if (resource == Enums.CommodityType.Gas)
                fuelId = 1;
            else if (resource == Enums.CommodityType.Water)
                fuelId = 10;

            return fuelId;
        }
        
        private int GetTimeAttribute(string intervalName)
        {
            int value = 0;

            switch (intervalName)
            {
                case "day":
                    value = 11;
                    break;

                case "fifteen":
                    value = 2;
                    break;

                case "five":
                    value = 0;
                    break;

                case "thirty":
                    value = 5;
                    break;

                case "hour":
                    value = 7;
                    break;

                case "sixhour":
                    value = 0;
                    break;
            }

            return value;
        }

        private bool IsCommoditySupported(Enums.CommodityType commodity, List<GreenButtonFunctionBlockType> allowedFunctionBlocks)
        {
            if (commodity == Enums.CommodityType.Electric)
                return allowedFunctionBlocks.Exists(fb => fb == GreenButtonFunctionBlockType.IntervalElecMetering);
            else if(commodity == Enums.CommodityType.Gas)
                return allowedFunctionBlocks.Exists(fb => fb == GreenButtonFunctionBlockType.Gas);
            else if (commodity == Enums.CommodityType.Water)
                return allowedFunctionBlocks.Exists(fb => fb == GreenButtonFunctionBlockType.Water);
            return false;
        }

        private bool IsIntervalLengthSupported(long intervalLength, List<GreenButtonDurationType> allowedIntervals)
        {
            var interval = (GreenButtonDurationType) Convert.ToInt32(intervalLength);

            return allowedIntervals.Exists(i => i == interval);
        }

        private bool IsIntervalMeteringSupported(List<GreenButtonFunctionBlockType> allowedFunctionBlocks)
        {
            return allowedFunctionBlocks.Exists(fb => fb == GreenButtonFunctionBlockType.IntervalMetering);
        }

        private int GetClientId(string accessToken)
        {
            int id = 0;

            var decVal = Helper.Base64Decode(accessToken);

            if (decVal.Length > 0)
            {
                decVal = Helper.Decrypt(decVal);
                var clientId = decVal.Split(',')[0];
                var secret = decVal.Split(',')[1];
                //check client id and secret
                Authorizations = InsightsEfRepository.GetGreenButtonAuthorizations(clientId, secret);
                if(Authorizations.Count > 0)
                    id = Convert.ToInt32(Authorizations?[0].ClientId);
            }

            return id;
        }
    }
}