﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CE.GreenButtonConnect;
using CE.Models;
using CE.Models.Insights;
using CE.Models.Insights.GreenButtonConnect;
using CE.Models.Insights.Types;
using Microsoft.Practices.Unity;

namespace CE.Insights.Models
{
    public class GreenButtonUsagePointModel : GreenButtonBaseModel
    {
        /// <summary>
        /// constructor for Usage Point model
        ///  </summary>
        public GreenButtonUsagePointModel(string accessToken, string locale, string greenButtonDomain, IUnityContainer container, IInsightsEFRepository insightsEfRepository) : base(accessToken, locale, greenButtonDomain, container, insightsEfRepository)
        {
        }

        /// <summary>
        /// Testability, passed in repository and unity container so not relying on data access through sql server and table storage
        /// </summary>
        /// <param name="greenButtonDomain"></param>
        /// <param name="authorizations"></param>
        /// <param name="greenButtonSettings"></param>
        /// <param name="unityContainer"></param>
        /// <param name="insightsEfRepository"></param>
        /// <param name="clientId"></param>
        public GreenButtonUsagePointModel(int clientId, string greenButtonDomain,List<GreenButtonAuthorization> authorizations, GreenButtonConfig greenButtonSettings, IUnityContainer unityContainer, IInsightsEFRepository insightsEfRepository): base(clientId, greenButtonDomain, authorizations, unityContainer,insightsEfRepository, greenButtonSettings)
        {
        }

        public GreenButtonResult GetAllUsagePointsGreenButton(long subscriptionId, string accessToken, DateTime? startDate, DateTime? endDate, bool isBatch)
        {
            // validate authorization
            if (!Authorizations.Exists(a => a.SubscriptionId == subscriptionId && a.SubscriptionId > 0 && a.AccessToken == accessToken))
                return new GreenButtonResult { Status = GreenButtonErrorType.InvalidSubscriptionAccessTokenErr };

            var scope = Authorizations.Find(a => a.SubscriptionId == subscriptionId).Scope;

            // Get Subscription info
            var subscription = InsightsEfRepository.GetGreenButtonAmiSubscription(subscriptionId);

            if (subscription != null)
            {
                // Get usage point
                var usagePoints = InsightsEfRepository.GetGreenButtonAmiUsagePoints(subscriptionId, scope);

                if (!scope.FunctionBlocks.Exists(fb => fb == GreenButtonFunctionBlockType.MulitpleUsagePoints) && usagePoints.Count > 1)
                    return new GreenButtonResult { Status = GreenButtonErrorType.MultipleUsageNotSupportErr };

                if (usagePoints.Count > 0)
                {
                    var statusList = usagePoints.Select(usagePoint => GetUsagePoint(subscription.AccountId, scope, usagePoint, startDate, endDate, isBatch)).ToList();
                    if (!statusList.Exists(s => s == GreenButtonErrorType.NoError))
                        return new GreenButtonResult { Status = statusList[0] };

                    subscription.UsagePoints = usagePoints;

                    // GetGreenButton
                    if (isBatch)
                        return GreenButtonModel.GenerateGreenButton(subscription, GreenButtonEntryType.All);
                    else
                    {
                        var selfLink = subscription.Links.Find(l => l.Rel == "self");
                        selfLink.Href = usagePoints[0].Links.Find(l => l.Rel == "up").Href;
                    }
                    return GreenButtonModel.GenerateGreenButton(subscription, GreenButtonEntryType.UsagePoint);
                }
                return new GreenButtonResult { Status = GreenButtonErrorType.InvalidSusbscriptionUsagePointErr };
            }
            return new GreenButtonResult { Status = GreenButtonErrorType.InvalidSubscriptionErr };
        }

        public GreenButtonResult GetUsagePointGreenButton(long subscriptionId,  string accessToken, string encryptedMeterId, DateTime? startDate, DateTime? endDate, bool isBatch)
        {
            // validate authorization
            if (!Authorizations.Exists(a => a.SubscriptionId == subscriptionId && a.SubscriptionId > 0 && a.AccessToken == accessToken))
                return new GreenButtonResult { Status = GreenButtonErrorType.InvalidSubscriptionAccessTokenErr };

            var scope = Authorizations.Find(a => a.SubscriptionId == subscriptionId).Scope;

            // Decrypt Meter Id
            var meterId = Helper.ConvertHexToString(encryptedMeterId, Encoding.Unicode);

            // Get Subscription info
            var subscription = InsightsEfRepository.GetGreenButtonAmiSubscription(subscriptionId);

            if (subscription != null)
            {
                // Get usage point
                var usagePoint = InsightsEfRepository.GetGreenButtonAmiUsagePoint(subscriptionId, meterId);

                if (usagePoint != null)
                {
                    var status = GetUsagePoint(subscription.AccountId, scope, usagePoint, startDate, endDate, isBatch);
                    if (status != GreenButtonErrorType.NoError)
                        return new GreenButtonResult { Status = status };
                    subscription.UsagePoints = new List<GreenButtonUsagePoint> { usagePoint };
                    
                    var selfLink = subscription.Links.Find(l => l.Rel == "self");
                    selfLink.Href = usagePoint.Links.Find(l => l.Rel == "up").Href;

                    // GetGreenButton
                    if (isBatch)
                        return GreenButtonModel.GenerateGreenButton(subscription, GreenButtonEntryType.All);
                    return GreenButtonModel.GenerateGreenButton(subscription, GreenButtonEntryType.UsagePoint);
                }
                return new GreenButtonResult { Status = GreenButtonErrorType.InvalidSusbscriptionUsagePointErr };
            }
            return new GreenButtonResult { Status = GreenButtonErrorType.InvalidSubscriptionErr };
        }

}
}