﻿using RestSharp;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Text;

namespace CE.Insights.Models
{
    // mid-term solution  - to be removed once long term solution is in place
    public class GreenButtonConnectMidTermApiResponseModel
    {

        //private const string url = "https://localhost/CE.InsightsMidTerm/api/v1/datacustodian/espi/1_1/resource/";
        private readonly string _apiUrl = "https://aclaceapi1710wadev.azurewebsites.net/api/v1/datacustodian/espi/1_1/resource/";

        private const string GreenButtonConnectSubString = "api/v1/datacustodian/espi/1_1/resource/";

        public GreenButtonConnectMidTermApiResponseModel()
        {
            
            string u = ConfigurationManager.AppSettings.Get("AmerenMidTermApiBaseUrl");
            if (!string.IsNullOrEmpty(u))
            {
                _apiUrl = u + GreenButtonConnectSubString;
            }
        }
        
        public HttpResponseMessage GetApplicationInformation(string authorization, int applicationInformationId)
        {
            var url = _apiUrl + $"ApplicationInformation/{applicationInformationId}";
            return GetResponse(url, authorization);
        }

        public HttpResponseMessage GetAuthorizations(string authorization)
        {
            var url = _apiUrl + "Authorization";
            return GetResponse(url, authorization);
        }

        public HttpResponseMessage GetAuthorization(string authorization, int authorizationId)
        {
            var url = _apiUrl + $"Authorization/{authorizationId}";
            return GetResponse(url, authorization);
        }

        public HttpResponseMessage GetMeterReadingIntervalBlocks(string authorization, int subscriptionId, string usagePointId, string meterReadingId, string start, string end)
        {
            var url = _apiUrl +
                      $"subscription/{subscriptionId}/usagepoint/{usagePointId}/meterreading/{meterReadingId}/intervalblock";

            if (!string.IsNullOrEmpty(start) && !string.IsNullOrEmpty(end))
                url = url + $"?published-min={start}&published-max={end}";
            else if (!string.IsNullOrEmpty(start) && string.IsNullOrEmpty(end))
                url = url + $"?published-min={start}";
            else if(string.IsNullOrEmpty(start) && !string.IsNullOrEmpty(end))
                url = url + $"?published-max={end}";

            return GetResponse(url, authorization);
        }

        public HttpResponseMessage GetMeterReadingIntervalBlocksByIntervalBlockId(string authorization, int subscriptionId, string usagePointId, string meterReadingId, string intervalBlockId)
        {
            var url = _apiUrl +
                      $"subscription/{subscriptionId}/usagepoint/{usagePointId}/meterreading/{meterReadingId}/intervalblock/{intervalBlockId}";
            
            return GetResponse(url, authorization);
        }

        public HttpResponseMessage GetLocalTimeParams(string authorization)
        {
            var url = _apiUrl + "LocalTimeParameters";

            return GetResponse(url, authorization);
        }
        public HttpResponseMessage GetLocalTimeParam(string authorization, int localTimeParameterId)
        {
            var url = _apiUrl + $"LocalTimeParameters/{localTimeParameterId}";

            return GetResponse(url, authorization);
        }

        public HttpResponseMessage GeUsagePointMeterReadings(string authorization, int subscriptionId, string usagePointId, string start, string end)
        {
            var url = _apiUrl +
                      $"subscription/{subscriptionId}/usagepoint/{usagePointId}/meterreading";

            if (!string.IsNullOrEmpty(start) && !string.IsNullOrEmpty(end))
                url = url + $"?published-min={start}&published-max={end}";
            else if (!string.IsNullOrEmpty(start) && string.IsNullOrEmpty(end))
                url = url + $"?published-min={start}";
            else if (string.IsNullOrEmpty(start) && !string.IsNullOrEmpty(end))
                url = url + $"?published-max={end}";

            return GetResponse(url, authorization);
        }
        public HttpResponseMessage GeUsagePointMeterReading(string authorization, int subscriptionId, string usagePointId, string meterReadingId)
        {
            var url = _apiUrl +
                      $"subscription/{subscriptionId}/usagepoint/{usagePointId}/meterreading/{meterReadingId}";
            
            return GetResponse(url, authorization);
        }
        public HttpResponseMessage GeReadingTypes(string authorization)
        {
            var url = _apiUrl + "ReadingType";

            return GetResponse(url, authorization);
        }
        public HttpResponseMessage GeReadingType(string authorization, string readingTypeId)
        {
            var url = _apiUrl + $"ReadingType/{readingTypeId}";

            return GetResponse(url, authorization);
        }
        public HttpResponseMessage ReadServiceStatus(string authorization)
        {
            var url = _apiUrl + "ReadServiceStatus";

            return GetResponse(url, authorization);
        }
        public HttpResponseMessage GetSubscriptionUsagePoints(string authorization, int subscriptionId, string start, string end)
        {
            var url = _apiUrl + $"subscription/{subscriptionId}/usagepoint";

            if (!string.IsNullOrEmpty(start) && !string.IsNullOrEmpty(end))
                url = url + $"?published-min={start}&published-max={end}";
            else if (!string.IsNullOrEmpty(start) && string.IsNullOrEmpty(end))
                url = url + $"?published-min={start}";
            else if (string.IsNullOrEmpty(start) && !string.IsNullOrEmpty(end))
                url = url + $"?published-max={end}";

            return GetResponse(url, authorization);
        }
        public HttpResponseMessage GetSubscriptionUsagePoint(string authorization, int subscriptionId, string usagePointId, string start, string end)
        {
            var url = _apiUrl + $"subscription/{subscriptionId}/usagepoint/{usagePointId}";

            if (!string.IsNullOrEmpty(start) && !string.IsNullOrEmpty(end))
                url = url + $"?published-min={start}&published-max={end}";
            else if (!string.IsNullOrEmpty(start) && string.IsNullOrEmpty(end))
                url = url + $"?published-min={start}";
            else if (string.IsNullOrEmpty(start) && !string.IsNullOrEmpty(end))
                url = url + $"?published-max={end}";

            return GetResponse(url, authorization);
        }
        public HttpResponseMessage GetBatchSubscriptionUsagePoints(string authorization, int batchSubscriptionId, string start, string end)
        {
            var url = _apiUrl + $"batch/subscription/{batchSubscriptionId}";

            if (!string.IsNullOrEmpty(start) && !string.IsNullOrEmpty(end))
                url = url + $"?published-min={start}&published-max={end}";
            else if (!string.IsNullOrEmpty(start) && string.IsNullOrEmpty(end))
                url = url + $"?published-min={start}";
            else if (string.IsNullOrEmpty(start) && !string.IsNullOrEmpty(end))
                url = url + $"?published-max={end}";

            return GetResponse(url, authorization);
        }
        public HttpResponseMessage GetBatchSubscriptionUsagePoint(string authorization, int batchSubscriptionId, string batchUsagePointId, string start, string end)
        {
            var url = _apiUrl + $"batch/subscription/{batchSubscriptionId}/usagepoint/{batchUsagePointId}";

            if (!string.IsNullOrEmpty(start) && !string.IsNullOrEmpty(end))
                url = url + $"?published-min={start}&published-max={end}";
            else if (!string.IsNullOrEmpty(start) && string.IsNullOrEmpty(end))
                url = url + $"?published-min={start}";
            else if (string.IsNullOrEmpty(start) && !string.IsNullOrEmpty(end))
                url = url + $"?published-max={end}";

            return GetResponse(url, authorization);
        }
        private HttpResponseMessage GetResponse(string url, string authorization)
        {
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);

            request.AddHeader("Content-Type", "application/json");

            request.AddHeader("Authorization", "Bearer " + authorization);
            request.RequestFormat = DataFormat.Json;


            var result = client.Execute(request);
            
            if (result.StatusCode == HttpStatusCode.OK)
            {
                var response = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent(result.Content, Encoding.UTF8, "application/atom+xml")
                };
                return response;
            }
            else
            {
                var response = new HttpResponseMessage(result.StatusCode)
                {
                    Content = new StringContent(result.Content, Encoding.UTF8, "application/json")
                };
                return response;
            }
        }

    }
}