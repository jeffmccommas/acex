﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CE.AO.Business;
using CE.Models;
using CE.Models.Insights;
using CE.Models.Insights.Types;
using Microsoft.Practices.Unity;

namespace CE.Insights.Models
{
    public class ConsumptionBaseModel
    {

        protected IUnityContainer Container;

        public ConsumptionBaseModel()
        {
        }

        public ConsumptionBaseModel(IUnityContainer unityContainer)
        {
            Container = unityContainer;
        }

        /// <summary>
        /// convert rate model readings to consumption data list along with tou info
        /// </summary>
        /// <param name="readings"></param>
        /// <param name="skipRows"></param>
        /// <param name="pageSize"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        protected List<ConsumptionData> ConvertRateModelReadingsToConsumptionData(List<RateModel.Reading> readings, int? skipRows, int? pageSize, int? count)
        {
            var consumptionList = new List<ConsumptionData>();
            List<RateModel.Reading> sortedReadings;
            
                sortedReadings = readings.OrderBy(r => r.Timestamp).ToList();
            foreach (var reading in sortedReadings)
            {
                var touConsumption = Mapper.Map<RateModel.Reading, ConsumptionTouData>(reading);
                var consumption = consumptionList.Find(c => c.DateTime == reading.Timestamp);
                if (consumption == null)
                {
                    consumption = Mapper.Map<RateModel.Reading, ConsumptionData>(reading);
                    consumption.TouConsumptions = new List<ConsumptionTouData>();
                    consumption.TouConsumptions.Add(touConsumption);
                    consumptionList.Add(consumption);
                }
                else
                {
                    consumption.TouConsumptions.Add(touConsumption);
                }

            }
            foreach (var consumption in consumptionList)
            {
                var total = consumption.TouConsumptions.Where(t => !t.TouKey.Equals(TimeOfUseType.NoTou.ToString(),
                                    StringComparison.CurrentCultureIgnoreCase)).Sum(t => t.Value);
                if (consumption.Value == 0)
                {
                    consumption.Value = total;
                }
                else
                {
                    if (consumption.Value != total)
                    {
                        var notou =
                            consumption.TouConsumptions.Find(n => n.TouKey.Equals(TimeOfUseType.NoTou.ToString(),
                                StringComparison.CurrentCultureIgnoreCase));
                        if (notou != null)
                        {
                            if (notou.Value != total)
                                total = total + notou.Value;
                            else
                            {
                                consumption.TouConsumptions.RemoveAll(
                                    n => n.TouKey.Equals(TimeOfUseType.NoTou.ToString(),
                                        StringComparison.CurrentCultureIgnoreCase));
                            }
                        }
                        consumption.Value = total;
                    }
                    else
                    {
                        consumption.TouConsumptions.RemoveAll(
                                    n => n.TouKey.Equals(TimeOfUseType.NoTou.ToString(),
                                        StringComparison.CurrentCultureIgnoreCase));
                    }
                }
            }

            if(count.HasValue && count.Value > 0)
                consumptionList = consumptionList.OrderByDescending(r => r.DateTime).Take(count.Value).OrderBy(r => r.DateTime).ToList();

            if (pageSize.HasValue && skipRows.HasValue && pageSize.Value > 0)
                consumptionList = consumptionList.OrderBy(r => r.DateTime).Skip(skipRows.Value).Take(pageSize.Value).ToList();

            return consumptionList;
        }


        protected void AutoMapperSetup()
        {
            Mapper.CreateMap<RateModel.Reading, ConsumptionData>()
    .ForMember(dest => dest.DateTime, opts => opts.MapFrom(src => src.Timestamp))
    .ForMember(dest => dest.Value,
        opts =>
            opts.MapFrom(
                src => src.TimeOfUse == RateModel.Enums.TimeOfUse.Undefined ? (decimal)src.Quantity : 0))
    .ForAllUnmappedMembers(o => o.Ignore());

            Mapper.CreateMap<RateModel.Reading, ConsumptionTouData>()
                .ForMember(dest => dest.Value, opts => opts.MapFrom(src => (decimal)src.Quantity))
                .ForMember(dest => dest.TouKey,
                    opts =>
                        opts.MapFrom(
                            src =>
                                src.TimeOfUse == RateModel.Enums.TimeOfUse.Undefined
                                    ? TimeOfUseType.NoTou.ToString().ToLower()
                                    : src.TimeOfUse.ToString().ToLower()))
                .ForAllUnmappedMembers(o => o.Ignore());


            Mapper.CreateMap<WeatherDb, ConsumptionWeather>()
                .ForMember(dest => dest.Date, opts => opts.MapFrom(src => src.WeatherReadingDate))
                .ForMember(dest => dest.AvgTemp, opts => opts.MapFrom(src => src.AvgTemp))
                .ForAllUnmappedMembers(o => o.Ignore());
        }
    }
}