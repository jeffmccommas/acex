﻿using AutoMapper;
using CE.AO.Business;
using AO.BusinessContracts;
using CE.AO.Models;
using Enums = CE.AO.Utilities.Enums;
using CE.ContentModel;
using CE.Models;
using CE.Models.Insights;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CE.Insights.Models
{
    /// <summary>
    /// Bill model
    /// </summary>
    public class BillModel
    {
        private IUnityContainer container;
        public static IInsightsEFRepository InsightsEfRepository;

        /// <summary>
        /// constructor for bill controller V1
        /// </summary>
        public BillModel()
        {
            InsightsEfRepository = new InsightsEfRepository();
        }

        public BillModel(IInsightsEFRepository insightsEfRepository) {
            InsightsEfRepository = insightsEfRepository;
        }

        /// <summary>
        /// constructor for bill controller V2
        /// </summary>
        public BillModel(IUnityContainer unityContainer)
        {
            container = unityContainer;
        }

        public BillResponse GetBills(int clientId, BillRequest request)
        {
            var response = new BillResponse();
            response.ClientId = clientId;
            if (request.Count == 0)
                request.Count = 1;
            response.Customer = GetCustomerBill(clientId, request);
            if (response.Customer == null) {
                response.Message = "No data found";
            }
            return (response);
        }

        public BillCustomer GetCustomerBill(int clientId, BillRequest request)
        {
            BillCustomer billCustomer;
            var accountList = new List<string>();
            var customerModel = GetCustomerInfo(clientId, request.CustomerId);

            if (customerModel != null)
            {
                InitializeBillModelMapper();
                billCustomer = Mapper.Map<CustomerModel, BillCustomer>(customerModel);
                billCustomer.Accounts = new List<BillAccount>();

                var premiseModels = GetPremisesInfo(clientId, request.CustomerId);

                premiseModels.ForEach(p => { if (!accountList.Exists(a => a == p.AccountId)) { accountList.Add(p.AccountId); } });

                foreach (var accountId in accountList)
                {
                    var billAccount = new BillAccount { Id = accountId };
                    var serviceModels = GetServiceList(clientId, request.CustomerId, accountId);
                    var bills = new List<Bill>();
                    foreach(var serviceModel in serviceModels)
                    {
                        var billModels = GetBillsInfo(clientId, request.CustomerId, accountId, serviceModel.ServiceContractId, request.StartDate, request.EndDate);

                        var selectedBills = billModels.OrderByDescending(b => Convert.ToDateTime(b.EndDate)).Take(request.Count);

                        foreach(var billModel in selectedBills)
                        {
                            BillService billService;
                            billService = Mapper.Map<BillingModel, BillService>(billModel);
                            billService.AverageTemperature = 0;
                            billService.AdditionalServiceCost = 0;

                            var premiseModel = premiseModels.Find(p => p.PremiseId == billModel.PremiseId);
                            BillPremise billPremise;
                            billPremise = Mapper.Map<PremiseModel, BillPremise>(premiseModel);                            

                            Bill bill;
                            bill = Mapper.Map<BillingModel, Bill>(billModel);

                            if(bills.Exists(b => b.BillDate == bill.BillDate && b.BillFrequency == bill.BillFrequency))
                            {
                                var existingBill = bills.Find(b => b.BillDate == bill.BillDate && b.BillFrequency == bill.BillFrequency);
                                var services = existingBill.Premises.SelectMany(p => p.Service).ToList();
                                if(services.Exists(s => s.BillStartDate == billService.BillStartDate && s.BillEndDate == billService.BillEndDate))
                                {
                                    existingBill.TotalAmount = existingBill.TotalAmount + billService.CostofUsage;
                                    if (existingBill.Premises.Exists(p => p.Id == billPremise.Id))
                                    {
                                        var existingBillPremise = existingBill.Premises.Find(p => p.Id == billPremise.Id);
                                        existingBillPremise.Service.Add(billService);
                                    }
                                    else
                                    {
                                        billPremise.Service = new List<BillService>();
                                        billPremise.Service.Add(billService);
                                        existingBill.Premises.Add(billPremise);

                                    }
                                }
                                else
                                {
                                    billPremise.Service = new List<BillService>();
                                    billPremise.Service.Add(billService);
                                    bill.TotalAmount = bill.TotalAmount + billService.CostofUsage;
                                    bill.Premises = new List<BillPremise>();
                                    bill.Premises.Add(billPremise);
                                    bills.Add(bill);
                                }
                                
                            }
                            else
                            {
                                billPremise.Service = new List<BillService>();
                                billPremise.Service.Add(billService);
                                bill.TotalAmount = bill.TotalAmount + billService.CostofUsage;
                                bill.Premises = new List<BillPremise>();
                                bill.Premises.Add(billPremise);
                                bills.Add(bill);
                            }

                        }
                    }

                    billAccount.Bills = bills.OrderByDescending(b => Convert.ToDateTime(b.BillDate)).ToList();

                    billCustomer.Accounts.Add(billAccount);
                    
                }                
            }
            else
            {
                return null;
            }

            GetWeatherInfo(billCustomer);

            return billCustomer;

        }
        
        /// <summary>
        /// get weather info for each bill
        /// </summary>
        /// <param name="customer"></param>
        public void GetWeatherInfo(BillCustomer customer)
        {            
            var billWeathers = InsightsEfRepository.GetWeatherInfoForBill(customer.Accounts);

            if(billWeathers != null)
            {
                foreach(var account in customer.Accounts)
                {
                    foreach(var bill in account.Bills)
                    {
                        foreach(var premise in bill.Premises)
                        {
                            foreach(var service in premise.Service)
                            {
                                var weather = billWeathers.Find(w => w.AccountId == account.Id && w.PremiseId == premise.Id
                                && w.ServiceId == service.Id && w.BillDate == bill.BillDate);

                                service.AverageTemperature = weather.AvgTemp;
                            }
                        }
                    }
                }
            }

        }

        /// <summary>
        /// get customer info for customer id from business layer
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public CustomerModel GetCustomerInfo(int clientId, string customerId)
        {
            CustomerModel customer = null;
            var customerTask = GetCustomerAsync(clientId, customerId);
            bool task = customerTask.Wait(60000);
            if (task && customerTask.Status == TaskStatus.RanToCompletion)
            {
                customer = customerTask.Result;
            }
            return customer;
        }

        /// <summary>
        /// get all premises info for customerId from business layer
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public List<PremiseModel> GetPremisesInfo(int clientId, string customerId)
        {
            IPremise premiseManager = container.Resolve<IPremise>();
            var premises = premiseManager.GetPremises(clientId, customerId).ToList();
            return premises;
        }

        /// <summary>
        /// get all the services from last bill from business layer
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public List<BillingModel> GetServiceList(int clientId, string customerId, string accountId)
        {
            IBilling billManager = container.Resolve<IBilling>();
            var services = billManager.GetAllServicesFromLastBill(clientId, customerId, accountId).ToList();
            return services;
        }

        /// <summary>
        /// get all the bills for the service between start and end date
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <param name="serviceContractId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public List<BillingModel> GetBillsInfo(int clientId, string customerId, string accountId,
            string serviceContractId, DateTime startDate, DateTime endDate)
        {
            IBilling billManager = container.Resolve<IBilling>();
            var bills = billManager.GetBillsForService(clientId, customerId, accountId, serviceContractId, startDate, endDate).OrderByDescending(b => b.ReadDate).ToList();
            return bills;
        }

        /// <summary>
        /// Local Content grabber.
        /// </summary>
        /// <param name="response"></param>
        /// <param name="clientUser"></param>
        /// <param name="locale"></param>
        public void ApplyContent(BillResponse response, ClientUser clientUser, string locale)
        {
            //content
            var cf = new ContentModelFactoryContentful();
            var cp = cf.CreateContentProvider(clientUser.ClientID);
            var contentLocale = cp.GetContentLocale(locale);

            //get label content and configuration
            response.Content = new Content
            {
                TextContent = cp.GetContentTextContent("bill", contentLocale, string.Empty),
                Configuration = cp.GetContentConfiguration("bill", string.Empty),
                Currency = cp.GetContentCurrency(string.Empty),
                Commodity = cp.GetContentCommodity(string.Empty),
                UOM = cp.GetContentUom(string.Empty)
            };
        }



        public BillResponse GetBills(ClientUser clientUser, BillRequest request, string locale) {
            var response = GetBillsAsync(clientUser, request, locale);
            return response.Result;
        }

        private async Task<BillResponse> GetBillsAsync(ClientUser clientUser, BillRequest request, string locale) {
            Task<BillContent> billContentTask = null;
            if (request.IncludeContent) {
                billContentTask = GetContentAysnc(clientUser, locale);
            }

            var response = InsightsEfRepository.GetBills(clientUser.ClientID, request);

            if (request.IncludeContent && billContentTask != null && response != null) {
                var billContent = await billContentTask.ConfigureAwait(false); 
                response.Content = billContent.Content;
                response.MiscInfo += billContent.ExtraInfo;
            }

            return response;
        }
        

        private async Task<BillContent> GetContentAysnc(ClientUser clientUser, string locale) {
            string miscInfo = string.Empty;
            DateTime startTime = DateTime.Now;

            //content
            var cf = new ContentModelFactoryContentful();
            var cp = cf.CreateContentProvider(clientUser.ClientID);
            var contentLocale = cp.GetContentLocale(locale);

            //get label content and configuration
            var textContentTask = cp.GetContentTextContentAsync("bill", contentLocale, string.Empty);
            var configurationTask = cp.GetContentConfigurationAsync("bill", string.Empty);
            var currencyTask = cp.GetContentCurrencyAsync(string.Empty);
            var commodityTask = cp.GetContentCommodityAsync(string.Empty);
            var uomTask = cp.GetContentUomAsync(string.Empty);

            var textContent = await textContentTask.ConfigureAwait(false);
            var configuration = await configurationTask.ConfigureAwait(false);
            var currency = await currencyTask.ConfigureAwait(false);
            var commodity = await commodityTask.ConfigureAwait(false);
            var uom = await uomTask.ConfigureAwait(false);

            var content = new Content {
                TextContent = textContent,
                Configuration = configuration,
                Currency = currency,
                Commodity = commodity,
                UOM = uom
            };

            var performanceMs = DateTime.Now.Subtract(startTime).TotalMilliseconds;
            miscInfo += $"; BillModel.GetContentAysnc {performanceMs} ms";

            var billContent = new BillContent {
                Content = content,
                ExtraInfo = miscInfo
            };

            return billContent;
        }

        /// <summary>
        /// return customer info async from business layer
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        private async Task<CustomerModel> GetCustomerAsync(int clientId, string customerId)
        {

            ICustomer customerManager = container.Resolve<ICustomer>();
            var task = await Task.Factory.StartNew(() => customerManager.GetCustomerAsync(clientId, customerId)).ConfigureAwait(false);

            return task.Result;

        }

        private void InitializeBillModelMapper()
        {
            Mapper.CreateMap<CustomerModel, BillCustomer>()
               .ForMember(dest => dest.Id, opts => opts.MapFrom(src => src.CustomerId))
               .ForMember(dest => dest.FirstName, opts => opts.MapFrom(src => src.FirstName))
               .ForMember(dest => dest.LastName, opts => opts.MapFrom(src => src.LastName))
               .ForAllUnmappedMembers(o => o.Ignore());


            Mapper.CreateMap<PremiseModel, BillPremise>()
               .ForMember(dest => dest.Id, opts => opts.MapFrom(src => src.PremiseId))
               .ForMember(dest => dest.Addr1, opts => opts.MapFrom(src => src.AddressLine1))
               .ForMember(dest => dest.Addr2, opts => opts.MapFrom(src => src.AddressLine2))
               .ForMember(dest => dest.City, opts => opts.MapFrom(src => src.City))
               .ForMember(dest => dest.State, opts => opts.MapFrom(src => src.State))
               .ForMember(dest => dest.Zip, opts => opts.MapFrom(src => src.PostalCode))
               .ForAllUnmappedMembers(o => o.Ignore());

            Mapper.CreateMap<BillingModel, BillService>()
                .ForMember(dest => dest.Id, opts => opts.MapFrom(src => src.ServiceContractId))
                .ForMember(dest => dest.TotalServiceUse, opts => opts.MapFrom(src => src.TotalUsage.HasValue ? (decimal)src.TotalUsage.Value : 0))
                .ForMember(dest => dest.CostofUsage, opts => opts.MapFrom(src => (decimal)src.TotalCost))
                .ForMember(dest => dest.CommodityKey, opts => opts.MapFrom(src => ((Enums.CommodityType)src.CommodityId).ToString().ToLower()))
                .ForMember(dest => dest.UOMKey, opts => opts.MapFrom(src => src.UOMId.HasValue ? ((Enums.UomType)src.UOMId.Value).ToString() : Enums.UomType.Other.ToString().ToLower()))
                .ForMember(dest => dest.BillStartDate, opts => opts.MapFrom(src => src.StartDate.ToString("yyyy-MM-dd")))
                .ForMember(dest => dest.BillEndDate, opts => opts.MapFrom(src => src.EndDate.ToString("yyyy-MM-dd")))
                .ForMember(dest => dest.BillDays, opts => opts.MapFrom(src => src.BillDays))
                .ForMember(dest => dest.RateClass, opts => opts.MapFrom(src => src.RateClass))
                .ForAllUnmappedMembers(o => o.Ignore());

            Mapper.CreateMap<BillingModel, Bill>()
                .ForMember(dest => dest.BillDate, opts => opts.MapFrom(src => src.ReadDate.HasValue ? src.ReadDate.Value.ToString("yyyy-MM-dd") : src.EndDate.ToString("yyyy-MM-dd")))
                .ForMember(dest => dest.BillFrequency, opts => opts.MapFrom(src => ((Enums.BillPeriodType)src.BillPeriodType).ToString()))
                .ForAllUnmappedMembers(o => o.Ignore());
        }
    }

    public class BillContent {
        public Content Content { get; set; }
        public string ExtraInfo { get; set; }
    }
}