﻿using System;
using System.Collections.Generic;
using System.Linq;
using CE.Models;
using CE.Models.Insights;
using CM = CE.ContentModel;

namespace CE.Insights.Models
{
    /// <summary>
    /// Profile model
    /// </summary>
    public class ProfileModel : IProfileModel
    {
        public static CM.ContentProviderType contentType = CM.ContentProviderType.Contentful;

        /// <summary>
        /// Get attributes from content and check POSTED attributes for any that are invalid or duplicates.
        /// </summary>
        /// <param name="clientUser"></param>
        /// <param name="locale"></param>
        /// <param name="environment"></param>
        /// <param name="request"></param>
        /// <param name="invalidAttributes"></param>
        /// <param name="duplicateAttributes"></param> 
        /// <param name="invalidValueAttributes"></param>
        /// <param name="invalidSourceAttributes"></param>
        /// <returns></returns>
        public bool ValidateAttributes(ClientUser clientUser, string locale, string environment, ProfilePostRequest request, List<String> invalidAttributes, List<String> duplicateAttributes, List<String> invalidValueAttributes, List<String> invalidSourceAttributes)
        {
            bool valid = true;
            List<CM.ClientProfileAttributeDefault> profileAttributes = null;
            var customerAttributeKeys = new List<string>();
            var accountAttributeKeys = new List<string>();
            var premiseAttributeKeys = new List<string>();
            var cf = new CM.ContentModelFactoryContentful();
            var cp = cf.CreateContentProvider(clientUser.ClientID);

            // Get List from content here
            profileAttributes = cp.GetProfileAttributes(clientUser.ClientID, null, null, null, null);

            var profileSourceEnum = cp.GetContentEnumeration("profile,common", string.Empty);

            var profileSources = profileSourceEnum.Where(item => item.EnumerationKey == "common.source").Select(item => item.Key).ToList();

            // create level specific key lists
            foreach (var i in profileAttributes)
            {
                if (i.EntityLevel.ToLower() == "customer")
                {
                    customerAttributeKeys.Add(i.AttributeKey);
                }

                if (i.EntityLevel.ToLower() == "account")
                {
                    accountAttributeKeys.Add(i.AttributeKey);
                }

                if (i.EntityLevel.ToLower() == "premise")
                {
                    premiseAttributeKeys.Add(i.AttributeKey);
                }
            }

            // compare with all lists in the posted request
            if (profileAttributes != null)
            {
                if (request.Customer != null)
                {
                    if (request.Customer.Attributes != null)
                    {
                        //looking for invalid attributes
                        if (!request.Customer.Attributes.All(x => customerAttributeKeys.Contains(x.AttributeKey)))
                        {
                            valid = false;

                            //get keys that do not belong
                            var itemList = new List<String>();
                            foreach (var attr in request.Customer.Attributes)
                            {
                                itemList.Add(attr.AttributeKey);
                            }

                            //add to invalidAttributes
                            var res = itemList.Except(customerAttributeKeys);
                            invalidAttributes.AddRange(res);
                        }


                        {
                            // check for duplicates
                            var itemList = new List<String>();
                            foreach (var attr in request.Customer.Attributes)
                            {
                                itemList.Add(attr.AttributeKey);
                            }

                            List<String> duplicates = itemList.GroupBy(x => x)
                             .Where(g => g.Count() > 1)
                             .Select(g => g.Key)
                             .ToList();

                            if (duplicates != null && duplicates.Count > 0)
                            {
                                valid = false;
                                duplicateAttributes.AddRange(duplicates);
                            }

                        }

                        {
                            // check for invalid values & source
                            foreach (var attr in request.Customer.Attributes)
                            {
                                var cust = profileAttributes.Find(x => x.AttributeKey == attr.AttributeKey);

                                // list
                                if (cust != null)
                                {
                                    if (cust.MaxValue == null)
                                    {
                                        string[] options = cust.OptionKeys.Split(',');
                                        int pos = Array.IndexOf(options, attr.AttributeValue);
                                        if (pos > -1)
                                        {
                                            // the array contains the string and the pos variable
                                            // will have its position in the array
                                        }
                                        else
                                        {
                                            valid = false;
                                            invalidValueAttributes.Add(attr.AttributeKey);
                                        }
                                    }
                                    else // number
                                    {
                                        if (!(Convert.ToDecimal(attr.AttributeValue) >= cust.MinValue && Convert.ToDecimal(attr.AttributeValue) <= cust.MaxValue))
                                        {
                                            valid = false;
                                            invalidValueAttributes.Add(attr.AttributeKey);
                                        }

                                    }
                                }
                                if (!profileSources.Contains(attr.SourceKey))
                                {
                                    valid = false;
                                    invalidSourceAttributes.Add(attr.AttributeKey);
                                }
                            }
                        }

                    }

                    if (request.Customer.Accounts != null)
                    {
                        foreach (var a in request.Customer.Accounts)
                        {
                            if (a.Attributes != null)
                            {
                                //looking for invalid attributes
                                if (!a.Attributes.All(x => accountAttributeKeys.Contains(x.AttributeKey)))
                                {
                                    valid = false;

                                    //get keys that do not belong
                                    var itemList = new List<String>();
                                    foreach (var attr in a.Attributes)
                                    {
                                        itemList.Add(attr.AttributeKey);
                                    }

                                    //add to invalidAttributes
                                    var res = itemList.Except(accountAttributeKeys);
                                    invalidAttributes.AddRange(res);
                                }


                                {
                                    // check for duplicates
                                    var itemList = new List<String>();
                                    foreach (var attr in a.Attributes)
                                    {
                                        itemList.Add(attr.AttributeKey);
                                    }

                                    List<String> duplicates = itemList.GroupBy(x => x)
                                     .Where(g => g.Count() > 1)
                                     .Select(g => g.Key)
                                     .ToList();

                                    if (duplicates != null && duplicates.Count > 0)
                                    {
                                        valid = false;
                                        duplicateAttributes.AddRange(duplicates);
                                    }

                                }

                                {
                                    // check for invalid values
                                    foreach (var attr in a.Attributes)
                                    {
                                        var acct = profileAttributes.Find(x => x.AttributeKey == attr.AttributeKey);

                                        // list
                                        if (acct != null)
                                        {
                                            if (acct.MaxValue == null)
                                            {
                                                string[] options = acct.OptionKeys.Split(',');
                                                int pos = Array.IndexOf(options, attr.AttributeValue);
                                                if (pos > -1)
                                                {
                                                    // the array contains the string and the pos variable
                                                    // will have its position in the array
                                                }
                                                else
                                                {
                                                    valid = false;
                                                    invalidValueAttributes.Add(attr.AttributeKey);
                                                }
                                            }
                                            else // number
                                            {
                                                if (!(Convert.ToDecimal(attr.AttributeValue) >= acct.MinValue && Convert.ToDecimal(attr.AttributeValue) <= acct.MaxValue))
                                                {
                                                    valid = false;
                                                    invalidValueAttributes.Add(attr.AttributeKey);
                                                }

                                            }
                                        }
                                        if (!profileSources.Contains(attr.SourceKey))
                                        {
                                            valid = false;
                                            invalidSourceAttributes.Add(attr.AttributeKey);
                                        }
                                    }
                                }

                            }


                            if (a.Premises != null)
                            {
                                foreach (var p in a.Premises)
                                {
                                    if (p.Attributes != null)
                                    {
                                        //looking for invalid attributes
                                        if (!p.Attributes.All(x => premiseAttributeKeys.Contains(x.AttributeKey)))
                                        {
                                            valid = false;

                                            //get keys that do not belong
                                            var itemList = new List<String>();
                                            foreach (var attr in p.Attributes)
                                            {
                                                itemList.Add(attr.AttributeKey);
                                            }

                                            //add to invalidAttributes
                                            var res = itemList.Except(premiseAttributeKeys);
                                            invalidAttributes.AddRange(res);
                                        }


                                        {
                                            // check for duplicates
                                            var itemList = new List<String>();
                                            foreach (var attr in p.Attributes)
                                            {
                                                itemList.Add(attr.AttributeKey);
                                            }

                                            List<String> duplicates = itemList.GroupBy(x => x)
                                             .Where(g => g.Count() > 1)
                                             .Select(g => g.Key)
                                             .ToList();

                                            if (duplicates != null && duplicates.Count > 0)
                                            {
                                                valid = false;
                                                duplicateAttributes.AddRange(duplicates);
                                            }

                                        }

                                        {
                                            // check for invalid values
                                            foreach (var attr in p.Attributes)
                                            {
                                                var prem = profileAttributes.Find(x => x.AttributeKey == attr.AttributeKey);

                                                // list
                                                if (prem != null)
                                                {
                                                    if (prem.MaxValue == null)
                                                    {
                                                        string[] options = prem.OptionKeys.Split(',');
                                                        int pos = Array.IndexOf(options, attr.AttributeValue);
                                                        if (pos > -1)
                                                        {
                                                            // the array contains the string and the pos variable
                                                            // will have its position in the array
                                                        }
                                                        else
                                                        {
                                                            valid = false;
                                                            invalidValueAttributes.Add(attr.AttributeKey);
                                                        }
                                                    }
                                                    else // number
                                                    {
                                                        if (!(Convert.ToDecimal(attr.AttributeValue) >= prem.MinValue && Convert.ToDecimal(attr.AttributeValue) <= prem.MaxValue))
                                                        {
                                                            valid = false;
                                                            invalidValueAttributes.Add(attr.AttributeKey);
                                                        }

                                                    }
                                                }
                                                if (!profileSources.Contains(attr.SourceKey))
                                                {
                                                    valid = false;
                                                    invalidSourceAttributes.Add(attr.AttributeKey);
                                                }
                                            }
                                        }

                                    }

                                }

                            }

                        }

                    }


                }

            }

            return (valid);
        }


        /// <summary>
        /// Process attributes from content using filtering.
        /// </summary>
        /// <param name="clientUser"></param>
        /// <param name="locale"></param>
        /// <param name="environment"></param>
        /// <param name="response"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public void ProcessAttributes(ClientUser clientUser, string locale, string environment, ProfileResponse response, ProfileRequest request)
        {
            List<CM.ClientProfileAttributeDefault> profileAttributes = null;
            List<string> customerAttributeKeys = new List<string>();
            List<string> accountAttributeKeys = new List<string>();
            List<string> premiseAttributeKeys = new List<string>();

            var cf = new CM.ContentModelFactoryContentful();
            var cp = cf.CreateContentProvider(clientUser.ClientID);

            // Get List from content here
            profileAttributes = cp.GetProfileAttributes(clientUser.ClientID, request.ProfileAttributeKeys, request.EndUseKeys, request.ApplianceKeys, request.EntityLevel);

            // create level specific key lists
            foreach (var i in profileAttributes)
            {
                switch (i.EntityLevel.ToLower())
                {
                    case "customer":
                        customerAttributeKeys.Add(i.AttributeKey);
                        break;
                    case "account":
                        accountAttributeKeys.Add(i.AttributeKey);
                        break;
                    case "premise":
                        premiseAttributeKeys.Add(i.AttributeKey);
                        break;
                }
            }

            // look for invalid attributes in the response and remove
            if (profileAttributes != null)
            {
                if (response.Customer != null)
                {
                    if (response.Customer.Attributes != null)
                    {
                        //looking for invalid attributes
                        if (!response.Customer.Attributes.All(x => customerAttributeKeys.Contains(x.AttributeKey)))
                        {
                            //remove keys that do not belong
                            var itemList = response.Customer.Attributes.Select(attr => attr.AttributeKey).ToList();

                            //remove from list
                            var rem = itemList.Except(customerAttributeKeys);
                            response.Customer.Attributes.RemoveAll(x => rem.Contains(x.AttributeKey));
                        }
                    }

                    if (response.Customer.Accounts != null)
                    {
                        foreach (var a in response.Customer.Accounts)
                        {
                            if (a.Attributes != null)
                            {
                                //looking for invalid attributes
                                if (!a.Attributes.All(x => accountAttributeKeys.Contains(x.AttributeKey)))
                                {
                                    //remove keys that do not belong
                                    var itemList = a.Attributes.Select(attr => attr.AttributeKey).ToList();

                                    //remove from list
                                    var rem = itemList.Except(accountAttributeKeys);
                                    a.Attributes.RemoveAll(x => rem.Contains(x.AttributeKey));
                                }
                            }

                            if (a.Premises != null)
                            {
                                foreach (var p in a.Premises)
                                {
                                    if (p.Attributes != null)
                                    {
                                        //looking for invalid attributes
                                        if (!p.Attributes.All(x => premiseAttributeKeys.Contains(x.AttributeKey)))
                                        {
                                            //remove keys that do not belong
                                            var itemList = p.Attributes.Select(attr => attr.AttributeKey).ToList();

                                            //remove from list
                                            var rem = itemList.Except(premiseAttributeKeys);
                                            p.Attributes.RemoveAll(x => rem.Contains(x.AttributeKey));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


        /// <summary>
        ///  Add attributes at all levels if includeMissing is true.
        /// </summary>
        /// <param name="clientUser"></param>
        /// <param name="locale"></param>
        /// <param name="environment"></param>
        /// <param name="response"></param>
        /// <param name="request"></param>
        public void IncludeMissingAttributes(ClientUser clientUser, string locale, string environment, ProfileResponse response, ProfileRequest request)
        {
            List<CM.ClientProfileAttributeDefault> profileAttributes = null;
            var customerAttributeKeys = new List<string>();
            var accountAttributeKeys = new List<string>();
            var premiseAttributeKeys = new List<string>();

            var cf = new CM.ContentModelFactoryContentful();
            var cp = cf.CreateContentProvider(clientUser.ClientID);

            // Get List from content here
            profileAttributes = cp.GetProfileAttributes(clientUser.ClientID, request.ProfileAttributeKeys, request.EndUseKeys, request.ApplianceKeys, request.EntityLevel);

            // create level specific key lists
            foreach (var i in profileAttributes)
            {
                if (i.EntityLevel.ToLower() == "customer")
                {
                    customerAttributeKeys.Add(i.AttributeKey);
                }

                if (i.EntityLevel.ToLower() == "account")
                {
                    accountAttributeKeys.Add(i.AttributeKey);
                }

                if (i.EntityLevel.ToLower() == "premise")
                {
                    premiseAttributeKeys.Add(i.AttributeKey);
                }
            }

            // Need to include (add) missing attributes where applicable
            if (profileAttributes != null)
            {
                if (response.Customer != null)
                {
                    if (response.Customer.Attributes == null)
                    {
                        if (customerAttributeKeys.Any())
                        {
                            response.Customer.Attributes = new List<ProfileAttribute>();
                        }
                    }

                    foreach (var attr in profileAttributes)
                    {
                        if (attr.EntityLevel == "customer")
                        {
                            if (!response.Customer.Attributes.Any(x => x.AttributeKey == attr.AttributeKey))
                            {
                                response.Customer.Attributes.Add(new ProfileAttribute() { AttributeKey = attr.AttributeKey, AttributeValue = "", ConditionKeys = attr.ConditionKeys, SourceKey = "Missing" });
                            }
                        }

                    }

                    if (response.Customer.Accounts != null)
                    {
                        foreach (var a in response.Customer.Accounts)
                        {
                            if (a.Attributes == null)
                            {
                                if (accountAttributeKeys.Any())
                                {
                                    a.Attributes = new List<ProfileAttribute>();
                                }
                            }

                            foreach (var attr in profileAttributes)
                            {
                                if (attr.EntityLevel == "account")
                                {
                                    if (!a.Attributes.Any(x => x.AttributeKey == attr.AttributeKey))
                                    {
                                        a.Attributes.Add(new ProfileAttribute() { AttributeKey = attr.AttributeKey, AttributeValue = "", ConditionKeys = attr.ConditionKeys, SourceKey = "Missing" });
                                    }
                                }

                            }

                            if (a.Premises != null)
                            {
                                foreach (var p in a.Premises)
                                {

                                    if (p.Attributes == null)
                                    {
                                        if (accountAttributeKeys.Any())
                                        {
                                            p.Attributes = new List<ProfileAttribute>();
                                        }
                                    }

                                    foreach (var attr in profileAttributes)
                                    {
                                        if (attr.EntityLevel == "premise")
                                        {
                                            if (!p.Attributes.Any(x => x.AttributeKey == attr.AttributeKey))
                                            {
                                                p.Attributes.Add(new ProfileAttribute() { AttributeKey = attr.AttributeKey, AttributeValue = "", ConditionKeys = attr.ConditionKeys, SourceKey = "Missing" });
                                            }

                                        }

                                    }

                                }

                            }

                        }

                    }

                }

            }

        }

        /// <summary>
        /// Grab the PROFILE content
        /// </summary>
        /// <param name="response"></param>
        /// <param name="clientUser"></param>
        /// <param name="locale"></param>
        public void ApplyContent(ProfileResponse response, ClientUser clientUser, string locale)
        {
            //content
            var cf = new CM.ContentModelFactoryContentful();
            var cp = cf.CreateContentProvider(clientUser.ClientID);
            var contentLocale = cp.GetContentLocale(locale);

            //get label content and configuration; 
            response.Content = new Content
            {
                Condition = cp.GetContentCondition("all", string.Empty),
                Configuration = cp.GetContentConfiguration("profile", string.Empty),
                ProfileAttribute = cp.GetContentProfileAttribute(string.Empty),
                Enumeration = cp.GetContentEnumeration("profile", string.Empty),
                TextContent = cp.GetContentTextContent("profile", contentLocale, string.Empty),
            };
        }

    }

    public interface IProfileModel
    {
        bool ValidateAttributes(ClientUser clientUser, string locale, string environment, ProfilePostRequest request,
            List<String> invalidAttributes, List<String> duplicateAttributes, List<String> invalidValueAttributes,
            List<String> invalidSourceAttributes);
    }
}