﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using AO.TimeSeriesProcessing;
using Cassandra;
using Xunit;
using TimeZone = AO.TimeSeriesProcessing.TimeZone;

//using Xunit;

namespace AO.DiagnosticsConsole {
    public class UnitTest1 {

        /// <summary>
        /// run 'n' processing tests wich involve extracting series data.
        /// Validates the processing paths through the series management code and database.
        /// </summary>
        //[Fact]
        public void TestSeriesExtract()
        {
            foreach (string stg in testList) {
                string[] rVals = ServicesBroker.TestProcess(stg.Split(' '));
                BasicAsserts(rVals);
            }
        }

        // NOTE: in the below, change the numeric collid to one that exists in the system being tested.
        private string[] testList = {
            "oper=tseries procLimit=50  doAsyncBatch=1 aclenv=work collid=collectionStarterId dtStt=1/1/2017 dtstp=1/4/2017",
            "oper=tseries procLimit=50  doAsyncBatch=1 aclenv=work collid=011006280 dtStt=1/23/2017 dtstp=2/25/2017",
            "oper=tseries procLimit=50  doAsyncBatch=1 aclenv=work collid=011006280 dtStt=1/23/2017 dtstp=3/25/2017",
            "oper=recallseries aclenv=work dtStt=2017-01-01T00:00:00Z dtStp=2017-03-01T00:25:00Z collid=011051563 gasync=0 Accept=text/csv"
        };

        private const string timeSeriesTable = "value_series_meta";

        [Fact]
        public void TestStasticsExtract() {
            ProcessingArgs pa = new ProcessingArgs(testList[0].Split(' '));
            if (pa["sqlMeterMgtCxnStg"] == null) {
                pa["sqlMeterMgtCxnStg"] = ConfigurationManager.AppSettings.Get("sqlMeterMgt");
            }
            ContextData.InitContextStrings(timeSeriesTable, pa["aclenv"], "none", pa, null);
            ProcessingArgs.AccumulateRequestPars(timeSeriesTable, pa["aclenv"], "none", pa);
            
            SeriesCollectionSpecification collSpec = new SeriesCollectionSpecification(pa);
            collSpec.InitializeCache(pa);
            SeriesSpecification seriesSpec = new SeriesSpecification(pa, null as DataRow);
            string res = collSpec.Statistics(pa);
            bool isOk = res.IndexOf("count=", StringComparison.InvariantCultureIgnoreCase) >= 0;
            Assert.Equal(isOk, true);

            res = seriesSpec.Statistics(pa);
            isOk = res.IndexOf("count=", StringComparison.InvariantCultureIgnoreCase) >= 0;
            Assert.Equal(isOk, true);


        }

        /// <summary>
        /// Test importing and retrieving series data into cassandra.
        /// This validates the series management sql and cassandra access paths.
        /// ingests a record with estimate indication and verifies.
        /// Updates the record with no estimate indication and verifies.
        /// </summary>
        [Fact]
        public void TestCassImport() {

            CassConn1 cc1 = new CassConn1();
            ProcessingArgs pa = new ProcessingArgs(testList[0].Split(' '));
            DateTime dtStt = DateTime.Now;
            Console.WriteLine($"Starting at {dtStt}");
            if (pa["sqlMeterMgtCxnStg"] == null) {
                pa["sqlMeterMgtCxnStg"] = ConfigurationManager.AppSettings.Get("sqlMeterMgt");
            }
            ContextData.InitContextStrings(timeSeriesTable, pa["aclenv"], "none", pa, null);
            ProcessingArgs.AccumulateRequestPars(timeSeriesTable, pa["aclenv"], "none", pa);

            cc1.Connect(pa);

            InsertCassandraDataTest(cassImp1);
            var rs = RecallCassandraDataTest(pa, cc1);

            // now validate content
            foreach (Row row in rs) {
                double val = (double)row["val"];
                Assert.Equal(val,0);
                IDictionary<string, string> map = (IDictionary<string, string>) row["meta"];
                Assert.Equal(map["estInd"],"Yes");
            }

            InsertCassandraDataTest(cassImp2);
            rs = RecallCassandraDataTest(pa, cc1);

            // now validate content
            foreach (Row row in rs) {
                double val = (double)row["val"];
                Assert.Equal(val, 50);
                IDictionary<string, string> map = (IDictionary<string, string>)row["meta"];
                Assert.Equal(map["estInd"], "No");
            }

        }

        private string cassImp1 = "c,s,v\n10000000,201701,2017-01-01 00:00:00Z,0,0,(*( 'estDesc':'D2NO' (*)'estInd':'Yes' )*)";
        private string cassImp2 = "c,s,v\n10000000,201701,2017-01-01 00:00:00Z,50,0,(*( 'estDesc':'D2NO' (*)'estInd':'No' )*)";
/*
        private string cassQrySer = "";
*/



        [Theory]
        #region Test Data
        [InlineData("2016-03-13T00:00:00Z", "2016-03-14T00:00:00Z", "EST")]
        [InlineData("2016-03-13T00:00:00Z", "2016-03-14T00:00:00Z", "CST")]
        [InlineData("2016-03-13T00:00:00Z", "2016-03-14T00:00:00Z", "MDT")]
        [InlineData("2016-11-06T00:00:00Z", "2016-11-07T00:00:00Z", "EST")]
        [InlineData("2016-11-06T00:00:00Z", "2016-11-07T00:00:00Z", "CST")]
        [InlineData("2016-11-06T00:00:00Z", "2016-11-07T00:00:00Z", "MDT")]
        [InlineData("2016-03-15T00:00:00Z", "2016-03-16T00:00:00Z", "EST")]
        [InlineData("2016-12-01T00:00:00Z", "2016-12-02T00:00:00Z", "EST")]
        [InlineData("2016-03-15T00:00:00Z", "2016-03-16T00:00:00Z", "CST")]
        [InlineData("2016-12-01T00:00:00Z", "2016-12-02T00:00:00Z", "CST")]
        [InlineData("2016-03-15T00:00:00Z", "2016-03-16T00:00:00Z", "MDT")]
        [InlineData("2016-12-01T00:00:00Z", "2016-12-02T00:00:00Z", "MDT")]
        #endregion
        public void TestConvertTimeZone(string start, string end, string timezone)
        {
            var dtStart = DateTimeOffset.Parse(start).UtcDateTime;
            var dtend = DateTimeOffset.Parse(end).UtcDateTime;

            var dt = MockCassandraData(dtStart, dtend);

            ServicesBroker.ConvertTimeZone(dt, timezone);

            var timezoneDesc = TimeZone.GetTimeZoneDescription(timezone);
            var timezoneId = TimeZoneInfo.FindSystemTimeZoneById(timezoneDesc);

            Assert.NotNull(dt);
            Assert.Equal(TimeZoneInfo.ConvertTime(((DateTimeOffset)dt.Rows[0]["ts"]).UtcDateTime,timezoneId), ((DateTimeOffset)dt.Rows[0]["ts"]).DateTime);
            Assert.Equal(TimeZoneInfo.ConvertTime(((DateTimeOffset)dt.Rows[1]["ts"]).UtcDateTime, timezoneId), ((DateTimeOffset)dt.Rows[1]["ts"]).DateTime);
            Assert.Equal(TimeZoneInfo.ConvertTime(((DateTimeOffset)dt.Rows[2]["ts"]).UtcDateTime, timezoneId), ((DateTimeOffset)dt.Rows[2]["ts"]).DateTime);
            Assert.Equal(TimeZoneInfo.ConvertTime(((DateTimeOffset)dt.Rows[3]["ts"]).UtcDateTime, timezoneId), ((DateTimeOffset)dt.Rows[3]["ts"]).DateTime);
            Assert.Equal(TimeZoneInfo.ConvertTime(((DateTimeOffset)dt.Rows[4]["ts"]).UtcDateTime, timezoneId), ((DateTimeOffset)dt.Rows[4]["ts"]).DateTime);
            Assert.Equal(TimeZoneInfo.ConvertTime(((DateTimeOffset)dt.Rows[5]["ts"]).UtcDateTime, timezoneId), ((DateTimeOffset)dt.Rows[5]["ts"]).DateTime);
            Assert.Equal(TimeZoneInfo.ConvertTime(((DateTimeOffset)dt.Rows[6]["ts"]).UtcDateTime, timezoneId), ((DateTimeOffset)dt.Rows[6]["ts"]).DateTime);
            
        }

        [Theory]
        #region Test Data
        //EST bucket size hour 24 with offset
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-05T00:00:00-04:00 dtStp=2016-11-08T00:00:00-05:00 timezone=EST adjusttimezone=true collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 3, 96, 100, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-06T00:00:00-04:00 dtStp=2016-11-09T00:00:00-05:00 timezone=EST adjusttimezone=true collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 3, 100, 96, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-04T00:00:00-04:00 dtStp=2016-11-07T00:00:00-05:00 timezone=EST adjusttimezone=true collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 3, 96, 96, 100)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-05T00:00:00-04:00 dtStp=2016-11-08T00:00:00-05:00 timezone=EST collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 4, 96, 96, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-12T00:00:00-05:00 dtStp=2016-03-15T00:00:00-04:00 timezone=EST adjusttimezone=true collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 3, 96, 92, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-13T00:00:00-05:00 dtStp=2016-03-16T00:00:00-04:00 timezone=EST adjusttimezone=true collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 3, 92, 96, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-11T00:00:00-05:00 dtStp=2016-03-14T00:00:00-04:00 timezone=EST adjusttimezone=true collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 3, 96, 96, 92)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-11T00:00:00-05:00 dtStp=2016-03-14T00:00:00-04:00 timezone=EST collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 3, 96, 96, 92)]
        //CST bucket size hour 24 with offset
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-12T00:00:00-05:00 dtStp=2016-03-15T00:00:00-04:00 timezone=CST adjusttimezone=true collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 3, 96, 92, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-13T00:00:00-05:00 dtStp=2016-03-16T00:00:00-04:00 timezone=CST adjusttimezone=true collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 3, 92, 96, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-11T00:00:00-05:00 dtStp=2016-03-14T00:00:00-04:00 timezone=CST adjusttimezone=true collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 3, 96, 96, 92)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-11T00:00:00-05:00 dtStp=2016-03-14T00:00:00-04:00 timezone=CST collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 3, 96, 96, 92)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-05T00:00:00-05:00 dtStp=2016-11-08T00:00:00-06:00 timezone=CST adjusttimezone=true collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 3, 96, 100, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-06T00:00:00-05:00 dtStp=2016-11-09T00:00:00-06:00 timezone=CST adjusttimezone=true collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 3, 100, 96, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-04T00:00:00-05:00 dtStp=2016-11-07T00:00:00-06:00 timezone=CST adjusttimezone=true collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 3, 96, 96, 100)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-05T00:00:00-05:00 dtStp=2016-11-08T00:00:00-06:00 timezone=CST collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 4, 96, 96, 96)]
        //PST bucket size hour 24 with offset
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-05T00:00:00-07:00 dtStp=2016-11-08T00:00:00-08:00 timezone=PST adjusttimezone=true collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 3, 96, 100, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-06T00:00:00-07:00 dtStp=2016-11-09T00:00:00-08:00 timezone=PST adjusttimezone=true collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 3, 100, 96, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-04T00:00:00-07:00 dtStp=2016-11-07T00:00:00-08:00 timezone=PST adjusttimezone=true collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 3, 96, 96, 100)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-05T00:00:00-07:00 dtStp=2016-11-08T00:00:00-08:00 timezone=PST collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 4, 96, 96, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-12T00:00:00-05:00 dtStp=2016-03-15T00:00:00-04:00 timezone=PST adjusttimezone=true collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 3, 96, 92, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-13T00:00:00-05:00 dtStp=2016-03-16T00:00:00-04:00 timezone=PST adjusttimezone=true collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 3, 92, 96, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-11T00:00:00-05:00 dtStp=2016-03-14T00:00:00-04:00 timezone=PST adjusttimezone=true collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 3, 96, 96, 92)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-11T00:00:00-05:00 dtStp=2016-03-14T00:00:00-04:00 timezone=PST collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 3, 96, 96, 92)]
        //EST bucket size hour 24  without offset
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-12T00:00:00Z dtStp=2016-03-15T00:00:00Z timezone=EST collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 3, 96, 96, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-13T00:00:00Z dtStp=2016-03-16T00:00:00Z timezone=EST collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 3, 96, 96, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-11T00:00:00Z dtStp=2016-03-14T00:00:00Z timezone=EST collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 3, 96, 96, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-12T00:00:00Z dtStp=2016-03-15T00:00:00Z timezone=EST adjusttimezone=true collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 4, 96, 92, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-13T00:00:00Z dtStp=2016-03-16T00:00:00Z timezone=EST adjusttimezone=true collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 4, 92, 96, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-11T00:00:00Z dtStp=2016-03-14T00:00:00Z timezone=EST adjusttimezone=true collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 4, 96, 96, 92)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-05T00:00:00Z dtStp=2016-11-08T00:00:00Z timezone=EST collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 3, 96, 96, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-06T00:00:00Z dtStp=2016-11-09T00:00:00Z timezone=EST collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 3, 96, 96, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-04T00:00:00Z dtStp=2016-11-07T00:00:00Z timezone=EST collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 3, 96, 96, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-05T00:00:00Z dtStp=2016-11-08T00:00:00Z timezone=EST adjusttimezone=true collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 3, 96, 100, 92)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-06T00:00:00Z dtStp=2016-11-09T00:00:00Z timezone=EST adjusttimezone=true collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 3, 100, 96, 92)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-04T00:00:00Z dtStp=2016-11-07T00:00:00Z timezone=EST adjusttimezone=true collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 3, 96, 96, 96)]
        //CST bucket size hour 24  without offset
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-12T00:00:00Z dtStp=2016-03-15T00:00:00Z timezone=CST collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 3, 96, 96, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-13T00:00:00Z dtStp=2016-03-16T00:00:00Z timezone=CST collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 3, 96, 96, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-11T00:00:00Z dtStp=2016-03-14T00:00:00Z timezone=CST collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 3, 96, 96, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-12T00:00:00Z dtStp=2016-03-15T00:00:00Z timezone=CST adjusttimezone=true collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 4, 96, 92, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-13T00:00:00Z dtStp=2016-03-16T00:00:00Z timezone=CST adjusttimezone=true collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 4, 92, 96, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-11T00:00:00Z dtStp=2016-03-14T00:00:00Z timezone=CST adjusttimezone=true collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 4, 96, 96, 92)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-05T00:00:00Z dtStp=2016-11-08T00:00:00Z timezone=CST collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 3, 96, 96, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-06T00:00:00Z dtStp=2016-11-09T00:00:00Z timezone=CST collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 3, 96, 96, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-04T00:00:00Z dtStp=2016-11-07T00:00:00Z timezone=CST collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 3, 96, 96, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-05T00:00:00Z dtStp=2016-11-08T00:00:00Z timezone=CST adjusttimezone=true collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 3, 96, 100, 92)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-06T00:00:00Z dtStp=2016-11-09T00:00:00Z timezone=CST adjusttimezone=true collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 3, 100, 96, 92)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-04T00:00:00Z dtStp=2016-11-07T00:00:00Z timezone=CST adjusttimezone=true collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 3, 96, 96, 96)]
        //PST bucket size hour 24  without offset
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-05T00:00:00Z dtStp=2016-11-08T00:00:00Z timezone=PST collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 3, 96, 96, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-06T00:00:00Z dtStp=2016-11-09T00:00:00Z timezone=PST collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 3, 96, 96, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-04T00:00:00Z dtStp=2016-11-07T00:00:00Z timezone=PST collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 3, 96, 96, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-05T00:00:00Z dtStp=2016-11-08T00:00:00Z timezone=PST adjusttimezone=true collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 3, 96, 100, 92)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-06T00:00:00Z dtStp=2016-11-09T00:00:00Z timezone=PST adjusttimezone=true collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 3, 100, 96, 92)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-04T00:00:00Z dtStp=2016-11-07T00:00:00Z timezone=PST adjusttimezone=true collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 3, 96, 96, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-12T00:00:00Z dtStp=2016-03-15T00:00:00Z timezone=PST collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 3, 96, 96, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-13T00:00:00Z dtStp=2016-03-16T00:00:00Z timezone=PST collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 3, 96, 96, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-11T00:00:00Z dtStp=2016-03-14T00:00:00Z timezone=PST collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 3, 96, 96, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-12T00:00:00Z dtStp=2016-03-15T00:00:00Z timezone=PST adjusttimezone=true collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 4, 96, 92, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-13T00:00:00Z dtStp=2016-03-16T00:00:00Z timezone=PST adjusttimezone=true collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 4, 92, 96, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-11T00:00:00Z dtStp=2016-03-14T00:00:00Z timezone=PST adjusttimezone=true collid=test bucketSizeHours=24 gasync=0 Accept=text/csv", 4, 96, 96, 92)]
        //EST bucket size hour 2, 3, 4, 6, 8, 12  without offset
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-06T00:00:00Z dtStp=2016-11-09T00:00:00Z timezone=EST adjusttimezone=true collid=test bucketSizeHours=6 gasync=0 Accept=text/csv", 12, 28, 24, 24)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-06T00:00:00Z dtStp=2016-11-09T00:00:00Z timezone=EST collid=test bucketSizeHours=6 gasync=0 Accept=text/csv", 12, 24, 24, 24)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-06T00:00:00Z dtStp=2016-11-09T00:00:00Z timezone=EST adjusttimezone=true collid=test bucketSizeHours=3 gasync=0 Accept=text/csv", 24, 12, 16, 12)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-06T00:00:00Z dtStp=2016-11-09T00:00:00Z timezone=EST collid=test bucketSizeHours=3 gasync=0 Accept=text/csv", 24, 12, 12, 12)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-06T00:00:00Z dtStp=2016-11-09T00:00:00Z timezone=EST adjusttimezone=true collid=test bucketSizeHours=4 gasync=0 Accept=text/csv", 18, 16, 20, 16)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-06T00:00:00Z dtStp=2016-11-09T00:00:00Z timezone=EST collid=test bucketSizeHours=4 gasync=0 Accept=text/csv", 18, 16, 16, 16)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-06T00:00:00Z dtStp=2016-11-09T00:00:00Z timezone=EST adjusttimezone=true collid=test bucketSizeHours=8 gasync=0 Accept=text/csv", 9, 36, 32, 32)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-06T00:00:00Z dtStp=2016-11-09T00:00:00Z timezone=EST collid=test bucketSizeHours=8 gasync=0 Accept=text/csv", 9, 32, 32, 32)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-06T00:00:00Z dtStp=2016-11-09T00:00:00Z timezone=EST adjusttimezone=true collid=test bucketSizeHours=12 gasync=0 Accept=text/csv", 6, 52, 48, 48)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-06T00:00:00Z dtStp=2016-11-09T00:00:00Z timezone=EST collid=test bucketSizeHours=12 gasync=0 Accept=text/csv", 6, 48, 48, 48)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-06T00:00:00Z dtStp=2016-11-09T00:00:00Z timezone=EST adjusttimezone=true collid=test bucketSizeHours=2 gasync=0 Accept=text/csv", 36, 8, 8, 12)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-06T00:00:00Z dtStp=2016-11-09T00:00:00Z timezone=EST collid=test bucketSizeHours=2 gasync=0 Accept=text/csv", 36, 8, 8, 8)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-13T00:00:00Z dtStp=2016-03-16T00:00:00Z timezone=EST adjusttimezone=true collid=test bucketSizeHours=12 gasync=0 Accept=text/csv", 7, 44, 48, 48)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-13T00:00:00Z dtStp=2016-03-16T00:00:00Z timezone=EST collid=test bucketSizeHours=12 gasync=0 Accept=text/csv", 6, 48, 48, 48)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-13T00:00:00Z dtStp=2016-03-16T00:00:00Z timezone=EST adjusttimezone=true collid=test bucketSizeHours=8 gasync=0 Accept=text/csv", 10, 28, 32, 32)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-13T00:00:00Z dtStp=2016-03-16T00:00:00Z timezone=EST collid=test bucketSizeHours=8 gasync=0 Accept=text/csv", 9, 32, 32, 32)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-13T00:00:00Z dtStp=2016-03-16T00:00:00Z timezone=EST adjusttimezone=true collid=test bucketSizeHours=6 gasync=0 Accept=text/csv", 13, 24, 20, 24)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-13T00:00:00Z dtStp=2016-03-16T00:00:00Z timezone=EST collid=test bucketSizeHours=6 gasync=0 Accept=text/csv", 12, 24, 24, 24)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-13T00:00:00Z dtStp=2016-03-16T00:00:00Z timezone=EST adjusttimezone=true collid=test bucketSizeHours=4 gasync=0 Accept=text/csv", 19, 16, 12, 16)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-13T00:00:00Z dtStp=2016-03-16T00:00:00Z timezone=EST collid=test bucketSizeHours=4 gasync=0 Accept=text/csv", 18, 16, 16, 16)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-13T00:00:00Z dtStp=2016-03-16T00:00:00Z timezone=EST adjusttimezone=true collid=test bucketSizeHours=3 gasync=0 Accept=text/csv", 25, 12, 12, 8)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-13T00:00:00Z dtStp=2016-03-16T00:00:00Z timezone=EST collid=test bucketSizeHours=3 gasync=0 Accept=text/csv", 24, 12, 12, 12)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-13T00:00:00Z dtStp=2016-03-16T00:00:00Z timezone=EST adjusttimezone=true collid=test bucketSizeHours=2 gasync=0 Accept=text/csv", 37, 8, 8, 8)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-13T00:00:00Z dtStp=2016-03-16T00:00:00Z timezone=EST collid=test bucketSizeHours=2 gasync=0 Accept=text/csv", 36, 8, 8, 8)]
        // EST bucket size hour 2, 3, 4, 6, 8, 12 with offset
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-06T00:00:00-04:00 dtStp=2016-11-09T00:00:00-05:00 timezone=EST adjusttimezone=true collid=test bucketSizeHours=2 gasync=0 Accept=text/csv", 36, 12, 8, 8)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-06T00:00:00-04:00 dtStp=2016-11-09T00:00:00-05:00 timezone=EST collid=test bucketSizeHours=2 gasync=0 Accept=text/csv", 37, 8, 8, 8)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-06T00:00:00-04:00 dtStp=2016-11-09T00:00:00-05:00 timezone=EST adjusttimezone=true collid=test bucketSizeHours=3 gasync=0 Accept=text/csv", 24, 16, 12, 12)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-06T00:00:00-04:00 dtStp=2016-11-09T00:00:00-05:00 timezone=EST collid=test bucketSizeHours=3 gasync=0 Accept=text/csv", 25, 12, 12, 12)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-06T00:00:00-04:00 dtStp=2016-11-09T00:00:00-05:00 timezone=EST adjusttimezone=true collid=test bucketSizeHours=4 gasync=0 Accept=text/csv", 18, 20, 16, 16)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-06T00:00:00-04:00 dtStp=2016-11-09T00:00:00-05:00 timezone=EST collid=test bucketSizeHours=4 gasync=0 Accept=text/csv", 19, 16, 16, 16)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-06T00:00:00-04:00 dtStp=2016-11-09T00:00:00-05:00 timezone=EST adjusttimezone=true collid=test bucketSizeHours=6 gasync=0 Accept=text/csv", 12, 28, 24, 24)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-06T00:00:00-04:00 dtStp=2016-11-09T00:00:00-05:00 timezone=EST collid=test bucketSizeHours=6 gasync=0 Accept=text/csv", 13, 24, 24, 24)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-06T00:00:00-04:00 dtStp=2016-11-09T00:00:00-05:00 timezone=EST adjusttimezone=true collid=test bucketSizeHours=8 gasync=0 Accept=text/csv", 9, 36, 32, 32)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-06T00:00:00-04:00 dtStp=2016-11-09T00:00:00-05:00 timezone=EST collid=test bucketSizeHours=8 gasync=0 Accept=text/csv", 10, 32, 32, 32)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-06T00:00:00-04:00 dtStp=2016-11-09T00:00:00-05:00 timezone=EST adjusttimezone=true collid=test bucketSizeHours=12 gasync=0 Accept=text/csv", 6, 52, 48, 48)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-06T00:00:00-04:00 dtStp=2016-11-09T00:00:00-05:00 timezone=EST collid=test bucketSizeHours=12 gasync=0 Accept=text/csv", 7, 48, 48, 48)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-13T00:00:00-05:00 dtStp=2016-03-16T00:00:00-04:00 timezone=EST adjusttimezone=true collid=test bucketSizeHours=3 gasync=0 Accept=text/csv", 24, 8, 12, 12)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-13T00:00:00-05:00 dtStp=2016-03-16T00:00:00-04:00 timezone=EST collid=test bucketSizeHours=3 gasync=0 Accept=text/csv", 24, 12, 12, 12)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-13T00:00:00-05:00 dtStp=2016-03-16T00:00:00-04:00 timezone=EST adjusttimezone=true collid=test bucketSizeHours=6 gasync=0 Accept=text/csv", 12, 20, 24, 24)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-13T00:00:00-05:00 dtStp=2016-03-16T00:00:00-04:00 timezone=EST collid=test bucketSizeHours=6 gasync=0 Accept=text/csv", 12, 24, 24, 24)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-13T00:00:00-05:00 dtStp=2016-03-16T00:00:00-04:00 timezone=EST adjusttimezone=true collid=test bucketSizeHours=12 gasync=0 Accept=text/csv", 6, 44, 48, 48)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-13T00:00:00-05:00 dtStp=2016-03-16T00:00:00-04:00 timezone=EST collid=test bucketSizeHours=12 gasync=0 Accept=text/csv", 6, 48, 48, 48)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-13T00:00:00-05:00 dtStp=2016-03-16T00:00:00-04:00 timezone=EST adjusttimezone=true collid=test bucketSizeHours=2 gasync=0 Accept=text/csv", 36, 8, 4, 8)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-13T00:00:00-05:00 dtStp=2016-03-16T00:00:00-04:00 timezone=EST collid=test bucketSizeHours=2 gasync=0 Accept=text/csv", 36, 8, 8, 8)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-13T00:00:00-05:00 dtStp=2016-03-16T00:00:00-04:00 timezone=EST adjusttimezone=true collid=test bucketSizeHours=4 gasync=0 Accept=text/csv", 18, 12, 16, 16)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-13T00:00:00-05:00 dtStp=2016-03-16T00:00:00-04:00 timezone=EST collid=test bucketSizeHours=4 gasync=0 Accept=text/csv", 18, 16, 16, 16)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-13T00:00:00-05:00 dtStp=2016-03-16T00:00:00-04:00 timezone=EST adjusttimezone=true collid=test bucketSizeHours=8 gasync=0 Accept=text/csv", 9, 28, 32, 32)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-13T00:00:00-05:00 dtStp=2016-03-16T00:00:00-04:00 timezone=EST collid=test bucketSizeHours=8 gasync=0 Accept=text/csv", 9, 32, 32, 32)]
        //EST bucket size month with offset
        [InlineData("oper=recallseries aclenv=work dtStt=2016-10-01T00:00:00-04:00 dtStp=2017-01-01T00:00:00-05:00 timezone=EST collid=test bucketSizeMonths=1 gasync=0 Accept=text/csv", 4, 2960, 2880, 2976)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-10-01T00:00:00-04:00 dtStp=2017-01-01T00:00:00-05:00 timezone=EST adjusttimezone=true collid=test bucketSizeMonths=1 gasync=0 Accept=text/csv", 3, 2976, 2884, 2976)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-02-01T00:00:00-05:00 dtStp=2016-05-01T00:00:00-04:00 timezone=EST collid=test bucketSizeMonths=1 gasync=0 Accept=text/csv", 4, 2764, 2976, 2880)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-02-01T00:00:00-05:00 dtStp=2016-05-01T00:00:00-04:00 timezone=EST adjusttimezone=true collid=test bucketSizeMonths=1 gasync=0 Accept=text/csv", 3, 2784, 2972, 2880)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-10-10T00:00:00-04:00 dtStp=2017-01-15T00:00:00-05:00 timezone=EST collid=test bucketSizeMonths=1 gasync=0 Accept=text/csv", 4, 2096, 2880, 2976)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-10-10T00:00:00-04:00 dtStp=2017-01-15T00:00:00-05:00 timezone=EST adjusttimezone=true collid=test bucketSizeMonths=1 gasync=0 Accept=text/csv", 4, 2112, 2884, 2976)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-02-10T00:00:00-05:00 dtStp=2016-05-15T00:00:00-04:00 timezone=EST collid=test bucketSizeMonths=1 gasync=0 Accept=text/csv", 4, 1900, 2976, 2880)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-02-10T00:00:00-05:00 dtStp=2016-05-15T00:00:00-04:00 timezone=EST adjusttimezone=true collid=test bucketSizeMonths=1 gasync=0 Accept=text/csv", 4, 1920, 2972, 2880)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-05T00:00:00-04:00 dtStp=2017-02-15T00:00:00-05:00 timezone=EST collid=test bucketSizeMonths=1 gasync=0 Accept=text/csv", 4, 2480, 2976, 2976)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-05T00:00:00-04:00 dtStp=2017-02-15T00:00:00-05:00 timezone=EST adjusttimezone=true collid=test bucketSizeMonths=1 gasync=0 Accept=text/csv", 4, 2500, 2976, 2976)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-10T00:00:00-05:00 dtStp=2016-06-15T00:00:00-04:00 timezone=EST collid=test bucketSizeMonths=1 gasync=0 Accept=text/csv", 4, 2092, 2880, 2976)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-10T00:00:00-05:00 dtStp=2016-06-15T00:00:00-04:00 timezone=EST adjusttimezone=true collid=test bucketSizeMonths=1 gasync=0 Accept=text/csv", 4, 2108, 2880, 2976)]
        //EST bucket size month without offset
        [InlineData("oper=recallseries aclenv=work dtStt=2016-10-01T00:00:00Z dtStp=2017-01-01T00:00:00Z timezone=EST collid=test bucketSizeMonths=1 gasync=0 Accept=text/csv", 3, 2976, 2880, 2976)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-10-01T00:00:00Z dtStp=2017-01-01T00:00:00Z timezone=EST adjusttimezone=true collid=test bucketSizeMonths=1 gasync=0 Accept=text/csv", 4, 16, 2976, 2884)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-02-01T00:00:00Z dtStp=2016-05-01T00:00:00Z timezone=EST collid=test bucketSizeMonths=1 gasync=0 Accept=text/csv", 3, 2784, 2976, 2880)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-02-01T00:00:00Z dtStp=2016-05-01T00:00:00Z timezone=EST adjusttimezone=true collid=test bucketSizeMonths=1 gasync=0 Accept=text/csv", 4, 20, 2784, 2972)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-10-10T00:00:00Z dtStp=2017-01-15T00:00:00Z timezone=EST collid=test bucketSizeMonths=1 gasync=0 Accept=text/csv", 4, 2112, 2880, 2976)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-10-10T00:00:00Z dtStp=2017-01-15T00:00:00Z timezone=EST adjusttimezone=true collid=test bucketSizeMonths=1 gasync=0 Accept=text/csv", 4, 2128, 2884, 2976)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-02-10T00:00:00Z dtStp=2016-05-15T00:00:00Z timezone=EST collid=test bucketSizeMonths=1 gasync=0 Accept=text/csv", 4, 1920, 2976, 2880)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-02-10T00:00:00Z dtStp=2016-05-15T00:00:00Z timezone=EST adjusttimezone=true collid=test bucketSizeMonths=1 gasync=0 Accept=text/csv", 4, 1940, 2972, 2880)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-05T00:00:00Z dtStp=2017-02-15T00:00:00Z timezone=EST collid=test bucketSizeMonths=1 gasync=0 Accept=text/csv", 4, 2496, 2976, 2976)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-05T00:00:00Z dtStp=2017-02-15T00:00:00Z timezone=EST adjusttimezone=true collid=test bucketSizeMonths=1 gasync=0 Accept=text/csv", 4, 2516, 2976, 2976)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-10T00:00:00Z dtStp=2016-06-15T00:00:00Z timezone=EST collid=test bucketSizeMonths=1 gasync=0 Accept=text/csv", 4, 2112, 2880, 2976)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-10T00:00:00Z dtStp=2016-06-15T00:00:00Z timezone=EST adjusttimezone=true collid=test bucketSizeMonths=1 gasync=0 Accept=text/csv", 4, 2128, 2880, 2976)]
        //EST bucket size 2 month with offset
        [InlineData("oper=recallseries aclenv=work dtStt=2016-10-01T00:00:00-04:00 dtStp=2017-04-01T00:00:00-05:00 timezone=EST collid=test bucketSizeMonths=2 gasync=0 Accept=text/csv", 4, 5840, 5952, 5664)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-10-01T00:00:00-04:00 dtStp=2017-04-01T00:00:00-05:00 timezone=EST adjusttimezone=true collid=test bucketSizeMonths=2 gasync=0 Accept=text/csv", 4, 5860, 5952, 5660)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-10-10T00:00:00-04:00 dtStp=2017-04-15T00:00:00-05:00 timezone=EST collid=test bucketSizeMonths=2 gasync=0 Accept=text/csv", 4, 4976, 5952, 5664)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-10-10T00:00:00-04:00 dtStp=2017-04-15T00:00:00-05:00 timezone=EST adjusttimezone=true collid=test bucketSizeMonths=2 gasync=0 Accept=text/csv", 4, 4996, 5952, 5660)]
        //EST bucket size 2 month without offset
        [InlineData("oper=recallseries aclenv=work dtStt=2016-10-01T00:00:00Z dtStp=2017-04-01T00:00:00Z timezone=EST collid=test bucketSizeMonths=2 gasync=0 Accept=text/csv", 3, 5856, 5952, 5664)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-10-01T00:00:00Z dtStp=2017-04-01T00:00:00Z timezone=EST adjusttimezone=true collid=test bucketSizeMonths=2 gasync=0 Accept=text/csv", 4, 2992, 5860, 5664)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-10-10T00:00:00Z dtStp=2017-04-15T00:00:00Z timezone=EST collid=test bucketSizeMonths=2 gasync=0 Accept=text/csv", 4, 4992, 5952, 5664)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-10-10T00:00:00Z dtStp=2017-04-15T00:00:00Z timezone=EST adjusttimezone=true collid=test bucketSizeMonths=2 gasync=0 Accept=text/csv", 4, 5012, 5952, 5660)]
        //EST bucket size day 1 without offset
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-12T00:00:00Z dtStp=2016-03-15T00:00:00Z timezone=EST collid=test bucketSizeDays=1 gasync=0 Accept=text/csv", 3, 96, 96, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-13T00:00:00Z dtStp=2016-03-16T00:00:00Z timezone=EST collid=test bucketSizeDays=1 gasync=0 Accept=text/csv", 3, 96, 96, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-11T00:00:00Z dtStp=2016-03-14T00:00:00Z timezone=EST collid=test bucketSizeDays=1 gasync=0 Accept=text/csv", 3, 96, 96, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-12T00:00:00Z dtStp=2016-03-15T00:00:00Z timezone=EST adjusttimezone=true collid=test bucketSizeDays=1 gasync=0 Accept=text/csv", 4, 96, 92, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-13T00:00:00Z dtStp=2016-03-16T00:00:00Z timezone=EST adjusttimezone=true collid=test bucketSizeDays=1 gasync=0 Accept=text/csv", 4, 92, 96, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-11T00:00:00Z dtStp=2016-03-14T00:00:00Z timezone=EST adjusttimezone=true collid=test bucketSizeDays=1 gasync=0 Accept=text/csv", 4, 96, 96, 92)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-05T00:00:00Z dtStp=2016-11-08T00:00:00Z timezone=EST collid=test bucketSizeDays=1 gasync=0 Accept=text/csv", 3, 96, 96, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-06T00:00:00Z dtStp=2016-11-09T00:00:00Z timezone=EST collid=test bucketSizeDays=1 gasync=0 Accept=text/csv", 3, 96, 96, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-04T00:00:00Z dtStp=2016-11-07T00:00:00Z timezone=EST collid=test bucketSizeDays=1 gasync=0 Accept=text/csv", 3, 96, 96, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-05T00:00:00Z dtStp=2016-11-08T00:00:00Z timezone=EST adjusttimezone=true collid=test bucketSizeDays=1 gasync=0 Accept=text/csv", 3, 96, 100, 92)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-06T00:00:00Z dtStp=2016-11-09T00:00:00Z timezone=EST adjusttimezone=true collid=test bucketSizeDays=1 gasync=0 Accept=text/csv", 3, 100, 96, 92)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-04T00:00:00Z dtStp=2016-11-07T00:00:00Z timezone=EST adjusttimezone=true collid=test bucketSizeDays=1 gasync=0 Accept=text/csv", 3, 96, 96, 96)]
        //EST bucket size day 1 with offset
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-05T00:00:00-04:00 dtStp=2016-11-08T00:00:00-05:00 timezone=EST adjusttimezone=true collid=test bucketSizeDays=1 gasync=0 Accept=text/csv", 3, 96, 100, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-06T00:00:00-04:00 dtStp=2016-11-09T00:00:00-05:00 timezone=EST adjusttimezone=true collid=test bucketSizeDays=1 gasync=0 Accept=text/csv", 3, 100, 96, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-04T00:00:00-04:00 dtStp=2016-11-07T00:00:00-05:00 timezone=EST adjusttimezone=true collid=test bucketSizeDays=1 gasync=0 Accept=text/csv", 3, 96, 96, 100)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-11-05T00:00:00-04:00 dtStp=2016-11-08T00:00:00-05:00 timezone=EST collid=test bucketSizeDays=1 gasync=0 Accept=text/csv", 4, 96, 96, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-12T00:00:00-05:00 dtStp=2016-03-15T00:00:00-04:00 timezone=EST adjusttimezone=true collid=test bucketSizeDays=1 gasync=0 Accept=text/csv", 3, 96, 92, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-13T00:00:00-05:00 dtStp=2016-03-16T00:00:00-04:00 timezone=EST adjusttimezone=true collid=test bucketSizeDays=1 gasync=0 Accept=text/csv", 3, 92, 96, 96)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-11T00:00:00-05:00 dtStp=2016-03-14T00:00:00-04:00 timezone=EST adjusttimezone=true collid=test bucketSizeDays=1 gasync=0 Accept=text/csv", 3, 96, 96, 92)]
        [InlineData("oper=recallseries aclenv=work dtStt=2016-03-11T00:00:00-05:00 dtStp=2016-03-14T00:00:00-04:00 timezone=EST collid=test bucketSizeDays=1 gasync=0 Accept=text/csv", 3, 96, 96, 92)]
        #endregion
        public void TestBucketizeAmiData(string operation, int expectedCount, double expectedCol1Val, double expectedCol2Val, double expectedCol3Val)
        {
            //CassConn1 cc1 = new CassConn1();
            ProcessingArgs pa = new ProcessingArgs(operation.Split(' '));
            DateTime dtStart = pa.GetControlVal("dtstt", DateTime.Now);
            DateTime dtEnd = pa.GetControlVal("dtStp", DateTime.Now);
            string timezone = pa.GetControlVal("timezone", "EST");
            bool adjustTimezone = Convert.ToBoolean(pa.GetControlVal("adjusttimezone", "false"));
            Console.WriteLine($"Starting at {dtStart}");
            if (pa["sqlMeterMgtCxnStg"] == null)
            {
                pa["sqlMeterMgtCxnStg"] = ConfigurationManager.AppSettings.Get("sqlMeterMgt");
            }
            ContextData.InitContextStrings(timeSeriesTable, pa["aclenv"], "none", pa, null);
            ProcessingArgs.AccumulateRequestPars(timeSeriesTable, pa["aclenv"], "none", pa);
            ContextData context = new ContextData(pa);

            AmiAccumulator amiAccum = new AmiAccumulator(pa);

            var dt = MockCassandraData(dtStart.ToUniversalTime(), dtEnd.ToUniversalTime());

            if(adjustTimezone)
                ServicesBroker.ConvertTimeZone(dt, timezone);

            var result = amiAccum.BucketizeAmiData(context, pa, dt);
            
            Assert.NotNull(result);
            Assert.Equal(expectedCount, result.Rows.Count);
            Assert.Equal(expectedCol1Val, result.Rows[0]["val"]);
            Assert.Equal(expectedCol2Val, result.Rows[1]["val"]);
            Assert.Equal(expectedCol3Val, result.Rows[2]["val"]);
        }

        

        private DataTable MockCassandraData(DateTime start, DateTime end)
        {
            DataSet ds = new DataSet("noSetName");
            DataTable dt = new DataTable("casdata");
            ds.Tables.Add(dt);

            dt.Columns.Add("id", typeof(Int64));
            dt.Columns.Add("did", typeof(Int64)); 
            dt.Columns.Add("ts", typeof(DateTimeOffset));
            dt.Columns.Add("meta", typeof(string));
            dt.Columns.Add("val", typeof(double));
            
            var endTs = new TimeSpan(0,-15,0);
            end = end.Add(endTs);
            var ts = start;
            var initialized = false;
            while (ts < end)
            {
                var min = 15;
                if (!initialized)
                {
                    min = 0;
                    initialized = true;
                }
                var minTs = new TimeSpan(0,min,0);
                ts = ts.Add(minTs);

                var dtOffset = new DateTimeOffset(ts);

                var row = dt.NewRow();
                row["id"] = (Int64)1;
                row["did"] = (Int64)1;
                row["meta"] = "test";
                row["ts"] = dtOffset;
                row["val"] = (double)1;
                dt.Rows.Add(row);

            }

            return dt;
        }
        private static RowSet RecallCassandraDataTest(ProcessingArgs pa, CassConn1 cc1) {
            pa["qRows"] = "*";
            pa["reqVsrc"] = "value_series_meta";
            pa["maxrows"] = "2";
            pa["allowFilter"] = "";
            string selStg = "where id = 10000000 and did = 201701";
            pa["qSel"] = selStg;
            RowSet rs = cc1.PerformCasQuery(pa, ref selStg);
            Assert.Equal(rs.GetAvailableWithoutFetching(), 1);
            return rs;
        }

        // helper to ingest a file and validate the ingestion succeeded (doesn't validate data).
        private void InsertCassandraDataTest(string dat) {
            string tmpFile = ".\\EvcTestFile.csv";
            StreamWriter sw = new StreamWriter(tmpFile);
            sw.Write(dat);
            sw.Close();
            string stg = $"oper=cassimport procLimit=5000000 importfile={tmpFile} doAsyncBatch=1 aclenv=work";
            string[] rVals = ServicesBroker.TestProcess(stg.Split(' '));
            BasicAsserts(rVals);
        }



        /// <summary>
        /// Basic processing test checks against processing log.
        /// </summary>
        /// <param name="rVals"></param>
        // ReSharper disable once UnusedParameter.Local
        void BasicAsserts(string[] rVals) {
            // ReSharper disable once StringIndexOfIsCultureSpecific.1
            Assert.Equal(rVals[4].IndexOf("**error"), -1);  // look for any errors.
            // ReSharper disable once StringIndexOfIsCultureSpecific.1
            Assert.Equal(rVals[4].IndexOf("ctxFile") > 0, true);  // sanity check we actually have expected output
        }


    }
}
