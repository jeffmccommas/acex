﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AO.TimeSeriesProcessing;

//using AWSHostProvider;

namespace AO.DiagnosticsConsole {



    public delegate string ProcessDroppedFile(FilesProcessor src, string pathName);

    public class EventsConsole {

        private static ContextData mainContext;
        private static ProcessingArgs mainPa;


        public static void Main(string[] args) {
            Console.CancelKeyPress += myHandler;
            mainPa = new ProcessingArgs(args);
            string vsrc = mainPa.GetControlVal("reqVsrc", "evc");
            ContextData.InitContextStrings(vsrc, mainPa["aclenv"], "none", mainPa, null);
            ProcessingArgs.AccumulateRequestPars(vsrc, mainPa["aclenv"], "none", mainPa);
            mainContext = new ContextData(mainPa);
            MainProcess(args);
        }


        protected static void myHandler(object sender, ConsoleCancelEventArgs args) {
            string snagTxt = mainContext.Snag(true);
            string info = $"keyHandler  Key pressed: {args.SpecialKey}, {snagTxt}\n {mainContext.Counters()} ";
            Console.WriteLine("keyHandler {0}", info);
            //mainContext.ContextWrite(info);

            args.Cancel = true;  // assume we dont' want to exit
            if (args.SpecialKey == ConsoleSpecialKey.ControlBreak) {
                if (pa.GetControlVal("cntlBreak", "1") == "1") { // normally do want cntlBreak to end
                    args.Cancel = false;
                }
            }
            else {
                if (pa.GetControlVal("cntlCC", "0") == "1") {  // normally don't want cntlC to end
                    args.Cancel = false;
                }
            }
        }

        private static ProcessingArgs pa;

        public static int MainProcess(string[] args) {
            Console.Title += ": Running in '" + Environment.CurrentDirectory;
            pa = new ProcessingArgs(args);
            pa["bodyOverride"] = "yes";
            Console.WriteLine($"Starting at {DateTime.Now}");
            if (pa["sqlMeterMgtCxnStg"] == null) {
                pa["sqlMeterMgtCxnStg"] = ConfigurationManager.AppSettings.Get("sqlMeterMgt");
            }
            string vsrc = pa.GetControlVal("reqVsrc", "evc");
            ContextData.InitContextStrings(vsrc, pa["aclenv"], "none", pa, null);
            ProcessingArgs.AccumulateRequestPars(vsrc, pa["aclenv"], "none", pa);
            ContextData context = new ContextData(pa);
            DateTime dtStt = DateTime.Now;
            string todo = pa["oper"] ?? "nada";
            switch (todo) {
                case "cvt2Tall": {
                        ZipManager zm = new ZipManager(pa, context, args);
                        string zipSrc = pa.GetControlVal("zipSrc", "");
                        string dropLocation = pa.GetControlVal("dropLocation", "");
                        FileAttributes attr = File.GetAttributes(zipSrc);
                        if ((attr & FileAttributes.Directory) == FileAttributes.Directory) {
                            string consolidateFilter = pa.GetControlVal("consFilter", "*.zip");
                            IEnumerable<string> fnames = Directory.EnumerateFiles(zipSrc, consolidateFilter);
                            foreach (string fname in fnames) {
                                Console.WriteLine($"cvt2Tall {zipSrc} -> {dropLocation}");
                                context.ContextWrite("cvt2Tall", $" {zipSrc} -> {dropLocation}");
                                zm.Convert2TallAmi(fname, dropLocation);
                            }

                        }
                        else {
                            zm.Convert2TallAmi(zipSrc, dropLocation);
                        }
                    }
                    break;
                case "ziptest": {
                        ZipManager zm = new ZipManager(pa, context, args);
                        string zipSrc = pa.GetControlVal("zipSrc", "");
                        string dropLocation = pa.GetControlVal("dropLocation", "");
                        zm.ExtractAndDrop(zipSrc, dropLocation);
                    }
                    break;
                case "dumpconfig": {
                        Console.WriteLine(pa.AsJson());
                    }
                    break;
                case "hostservice": {
                        SimpleWebServerLocal ws = new SimpleWebServerLocal(pa);
                        for (;;) {
                            Console.WriteLine(" in hostservice and waiting ....");
                            Thread.Sleep(5000);
                        }
                    }
                    break;
                case "splitcsv": {
                        string srcFile = pa.GetControlVal("srcfile", "nada");
                        int maxLines = pa.GetControlVal("maxlines", 10000);
                        using (StreamReader sr = new StreamReader(srcFile)) {
                            string csvLine = sr.ReadLine();
                            int destIdx = 0;
                            int lineCnt = 0;
                            string destFile = $"{srcFile}.{destIdx:D3}.input.csv";
                            StreamWriter sw = new StreamWriter(destFile);
                            sw.WriteLine(csvLine);
                            while (!sr.EndOfStream) {
                                string line = sr.ReadLine();
                                sw.WriteLine(line);
                                if (++lineCnt >= maxLines) {
                                    sw.Flush();
                                    sw.Close();
                                    lineCnt = 0;
                                    destIdx++;
                                    destFile = $"{srcFile}.{destIdx:D3}.input.csv";
                                    sw = new StreamWriter(destFile);
                                    sw.WriteLine(csvLine);
                                }
                            }
                            sw.Close();

                        }
                    }
                    break;

                case "multicsv": {
                        ToolingRequest tr = new ToolingRequest();
                        tr.SubmitBulkDataRequests(pa, context);
                    }
                    break;

                case "watcher": {
                        InitializeWorkerProcessors(context);
                    }
                    break;
                case "validator": {
                    ValidateData(context);

                }
                    break;

                default: {
                        try {
                            context.ContextWrite("MainProcess call ProcessOptions", "Begin");
                            ServicesBroker.ProcessOptions(pa, context, dtStt);
                            context.ContextWrite("MainProcess call ProcessOptions", "End", dtStt);
                        }
                        finally {
                            Console.WriteLine(context.Snag());
                        }
                    }
                    break;
            }
            int errorCount = (int)context.Add2Counter("errors", 0);
            DateTime dtNow = DateTime.Now;
            context.Add2Counter("mainProcMs", (long)dtNow.Subtract(dtStt).TotalMilliseconds);
            Console.WriteLine($"mainprocess done at {dtNow}, errorCount={errorCount} delta = {dtNow.Subtract(dtStt).TotalMilliseconds} ms.");
            Console.WriteLine(context.Snag());
            context.End(mainContext);
            return errorCount;
        }


        private static void ValidateData(ContextData context) {
            List<string> resultsRecs = new List<string>();

            string srcDataRef = pa.GetControlVal("useRefFile", "");
            FileAttributes attr = File.GetAttributes(srcDataRef);
            List<string> srcFiles = new List<string>();
            if ((attr & FileAttributes.Directory) == FileAttributes.Directory) {
                int maxFiles = pa.GetControlVal("maxFiles", 20);
                bool justListData = pa.GetControlVal("justListData", "no").Equals("yes");
                int skipFiles = pa.GetControlVal("skipFiles", 0);
				string consolidateFilter = pa.GetControlVal("consFilter", "*.csv");
				IEnumerable<string> fnames = Directory.EnumerateFiles(srcDataRef, consolidateFilter)
                    .OrderBy(fileName => fileName);
                int doSkip = skipFiles; // want to snag the first one
                long totalRecsToProcess = 0;
                long totalFilesToProcess = 0;
                foreach (string fname in fnames) {
                    if (doSkip >= skipFiles) {
                        if (justListData) {
                            totalRecsToProcess += ShowProposedProcessInfo(fname);
                            totalFilesToProcess++;
                        }
                        else {
                            srcFiles.Add(fname);
                            totalFilesToProcess++;
                        }
                        doSkip = 0;
                    }
                    else {
                        doSkip++;
                    }
                    if (totalFilesToProcess >= maxFiles) break;
                }
                if (justListData) {
                    int lowEstimateRate = pa.GetControlVal("lowEstimateRate", 200);
                    int higEstimateRate = pa.GetControlVal("higEstimateRate", 400);
                    long lowRateSecs = totalRecsToProcess / lowEstimateRate;
                    long higRateSecs = totalRecsToProcess / higEstimateRate;
                    Console.WriteLine(
                        $"*** would process {totalFilesToProcess} with {totalRecsToProcess} total records, estimated time of {higRateSecs} to {lowRateSecs} seconds.");
                    context.Add2Counter("cntr_lowRateSecs", lowRateSecs);
                    context.Add2Counter("cntr_higRateSecs", higRateSecs);
                    context.Add2Counter("cntr_totalFilesToProcess", totalFilesToProcess);
                    context.Add2Counter("cntr_totalRecsToProcess", totalRecsToProcess);
                }
            }
            else {
                srcFiles.Add(srcDataRef);
            }
            foreach (string fname in srcFiles) {
                pa["useRefFile"] = fname;
                string res = ValidateDroppedFile(context);
                resultsRecs.Add(res);
                Console.WriteLine($"\n\n ***validateresults*** {res}");
            }

            Console.WriteLine($"\n\n **composite results*** ");
            foreach (string resLine in resultsRecs) {
                Console.WriteLine($" **compositevalidateresults*** {resLine}");
            }
        }

        private static long ShowProposedProcessInfo(string fileName) {
            long lineCount = 0;
            using (FileStream stream = File.OpenRead(fileName)) {
                using (StreamReader sr = new StreamReader(stream)) {
                    while (!sr.EndOfStream) {
                        sr.ReadLine();
                        lineCount++;
                    }
                }
            }
            long origProcLines = lineCount - 1;
            long procLines = origProcLines;
            int procLimit = pa.GetControlVal("procLimit", 5000000);
            int skipRows = pa.GetControlVal("skipRows", 0);
            if (procLines > procLimit) {
                procLines = procLimit;
            }
            if (skipRows > 0) {
                procLines /= (skipRows+1);
            }
            Console.WriteLine($"{fileName} , {procLines} of {lineCount - 1} records  would be processed. Skipping {skipRows} rows after processing one.  Possible {origProcLines} rows clamped at {procLimit} ");
            return procLines;
        }

        private static void InitializeWorkerProcessors(ContextData context) {
            string dropSourceDir = pa.GetControlVal("dropLocation", @"C:\_Test\drop");
            string dropRelocateDir = pa.GetControlVal("dropRelocate", @"C:\_Test\drop\snagged");
            string dropProcessedDir = pa.GetControlVal("dropProcessed", @"C:\_Test\drop\snagged");

            FilesProcessor inputProcessor = new FilesProcessor(pa, dropSourceDir, dropRelocateDir);
            if (pa.GetControlVal("enableProcessed", "no").Equals("yes")) {
                inputProcessor.SetMoveProcessor(ProcessDroppedFile);
            }
            //FilesProcessor processProcessor = new FilesProcessor(pa, dropRelocateDir, dropProcessedDir);
            //processProcessor.SetMoveProcessor(ProcessDroppedFile);

            if (pa.GetControlVal("dropFlushPendingAndExit", "no").Equals("yes")) {
                inputProcessor.FlushPending();
                inputProcessor.RelocateChanges(new List<string>(inputProcessor.WatchingDoneList));
                Console.WriteLine($" InitializeWorkerProcessors   dropFlushPendingAndExit  == bye bye {inputProcessor?.Status()}.");
                return;
            }
            if (pa.GetControlVal("dropFlushPending", "no").Equals("yes")) {
                inputProcessor.FlushPending();

            }


            int mainerrorTermCount = pa.GetControlVal("mainerrorTermCount", 100);
            int mainLoopDelay = pa.GetControlVal("mainLoopDelay", 5000);
            if (mainLoopDelay < 1000) mainLoopDelay = 1000;
            int dropHoldCount = pa.GetControlVal("dropHoldCount", 0);
            int watchBatchLimit = pa.GetControlVal("watchBatchLimit", 5);
            int mainErrorCount = 0;
            for (;;) {
                try {
                    int batchExec = 0;
                    int idle = 0;
                    int errored = 0;
                    if (inputProcessor.RequestTerminate) {
                        dropHoldCount = 0;
                    }
                    else {
                        if (!string.IsNullOrEmpty(inputProcessor.ConfigChangeData)) {
                            string configLine = inputProcessor.ConfigChangeData;
                            try {
                                Console.WriteLine($" **** Processing ConfigurationChange data: {configLine}  ");
                                pa.AddIfMissing(configLine, true);
                                mainerrorTermCount = pa.GetControlVal("mainerrorTermCount", 100);
                                mainLoopDelay = pa.GetControlVal("mainLoopDelay", 5000);
                                if (mainLoopDelay < 1000) mainLoopDelay = 1000;
                                dropHoldCount = pa.GetControlVal("dropHoldCount", 2);
                                watchBatchLimit = pa.GetControlVal("watchBatchLimit", 5);
                                if (configLine.Contains("dropLocation") || configLine.Contains("dropRelocate")) {
                                    inputProcessor.ResetWatchers(pa);
                                }
                            }
                            catch (Exception exc) {
                                Console.WriteLine(
                                    $" **** Processing ConfigurationChange exception '{configLine}' = {exc.Message} , {exc.InnerException?.Message} ");
                            }
                            finally {
                                inputProcessor.ConfigChangeData = "";
                            }
                        }
                        if (!inputProcessor.RequestPause) {
                            while (inputProcessor.QueuedList.Count > 0) { // allow a few to be dequed and executed.
                                int stat = inputProcessor.DoProcessOne();
                                switch (stat) {
                                    case 0:
                                        batchExec++; // a successful execution
                                        break;
                                    case -1:
                                        idle++; // a successful execution
                                        break;
                                    default:
                                        errored++;
                                        break;
                                }
                                if ((errored > 0) || (batchExec >= watchBatchLimit) ||
                                    inputProcessor.RequestTerminate || inputProcessor.RequestPause) {
                                    break;
                                }
                            }
                        }
                    }
                    string status = inputProcessor.Status();
                    int changeCount = inputProcessor.Count();
                    Console.WriteLine(
                        $" {DateTime.Now} in watcher and waiting   input : paused={inputProcessor.RequestPause} queued={inputProcessor.QueuedList.Count} count={changeCount} batchexec={batchExec} idle={idle} berr={errored} status={status} ....");
                    //Console.WriteLine($" in watcher and waiting process : count={processProcessor.Count()} status={processProcessor.Status()} ....");
                    if (changeCount > 0) {
                        List<string> changeList = inputProcessor.GetChangeList();
                        foreach (string change in changeList) {
                            Console.WriteLine(change);
                        }
                        if (changeCount > dropHoldCount) {
                            Console.WriteLine($" in watcher and removing  ....");
                            List<string>[] results = inputProcessor.RelocateChanges(changeList);
                            string resultsStat = "";
                            foreach (List<string> result in results) {
                                resultsStat += ((result != null) ? result.Count.ToString() : "[null]") + ",";
                            }
                            Console.WriteLine(
                                $" after remove : count={inputProcessor.Count()} status={inputProcessor.Status()} ; results status = {resultsStat} ....");
                            //foreach (string change in changeList) {
                            //    Console.WriteLine(change);
                            //}

                            List<string> errors = results[results.Length - 1];
                            foreach (string error in errors) {
                                Console.WriteLine($" notes/errors : {error}");
                            }
                        }
                    }
                }
                catch (Exception exc) {
                    mainErrorCount++;
                    Console.WriteLine($"InitializeWorkerProcessors: exception count={mainErrorCount} is {exc.Message}, inner={exc.InnerException?.Message}  {inputProcessor?.Status()}");
                }
                Thread.Sleep(mainLoopDelay);
                if (inputProcessor.RequestTerminate || (mainErrorCount >mainerrorTermCount)) {
                    Console.WriteLine($"InitializeWorkerProcessors: ending count={mainErrorCount} requestTerm={inputProcessor.RequestTerminate}  {inputProcessor?.Status()}");
                    break;
                }
            }
            context.Add2Counter("cntr_fpFileRetries", inputProcessor.FileRetries);
            context.Add2Counter("cntr_fpFileOpenRetries", inputProcessor.FileOpenRetries);
            context.Add2Counter("cntr_fpFileConfigRetries", inputProcessor.FileConfigRetries);
            context.Add2Counter("cntr_fpFilesDropped", inputProcessor.FilesDropped);
            context.Add2Counter("cntr_fpConfigDropped", inputProcessor.ConfigDropped);
            context.Add2Counter("cntr_fpFilesRelocated", inputProcessor.FilesRelocated);
            context.Add2Counter("cntr_fpFilesIgnored", inputProcessor.FilesIgnored);
        }


        public static string ProcessDroppedFile(FilesProcessor src, string pathName) {
            string result = "0";
            int procLimit = pa.GetControlVal("procLimit", 5000000);
            string useEnv = pa.GetControlVal("aclEnv", "perfd");
            if (pathName.ToLower().Contains("60min")) { // BUGBUG: don't like these assumptions but for now....
                useEnv = pa.GetControlVal("hourlyEnv", "perfh");
            }
            string impCmd = $"{useEnv}importFileCmd";
            string processCommand = pa.GetControlVal(impCmd, "");
            if (!string.IsNullOrEmpty(processCommand)) {
                string execCmd = string.Format(processCommand, pathName, useEnv,procLimit);
                string addlImportPars = pa.GetControlVal("addlImportPars", "");
                if (!string.IsNullOrEmpty(addlImportPars)) {  // if we want to pass along additional control parameters
                    string[] addlPars = addlImportPars.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string addlPar in addlPars) {
                        string addVal = pa.GetControlVal(addlPar, "");
                        switch ((addlPar)) {
                            case "outputPath": {  // special case
                                    string[] pathVals = addVal.Split('\\');
                                    if (pathVals.Length > 1) {
                                        addVal = $"{pathVals[0]}\\{pathVals[1]}";
                                    }
                                }
                                break;
                        }
                        string addThis = $" {addlPar}={addVal}";
                        execCmd += $" {addlPar}={addThis}";
                    }
                }


                string[] procArgs = execCmd.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                Console.WriteLine($"  wants to exec {execCmd}");
                try {
                    int rVal = MainProcess(procArgs);
                    result = rVal.ToString();
                }
                catch (Exception exc) {
                    Console.WriteLine($"MainProcess.ProcessDroppedFile: exception is {exc.Message}, inner={exc.InnerException?.Message}");
                }
            }
            else {

            }
            return result;
        }

        public static string Wide2TallProcessZipFile(FilesProcessor src, string srcName, string destName) {
            string result = "1";
            if (File.Exists(srcName)) {
                string destPath = Path.GetDirectoryName(destName);
                if (!Directory.Exists(destPath)) {
                    Directory.CreateDirectory(destPath);
                }
            }

            return result;
        }


        public static string ValidateDroppedFile(ContextData context) {
            string result = "0";
            int procLimit = pa.GetControlVal("procLimit", 50);
            string useEnv = pa.GetControlVal("aclEnv", "perfd");
            string overrideQry = pa.GetControlVal("overrideQry", "perfdvalQry");
            string useRefFile = pa.GetControlVal("useRefFile", @"C:\_Test\ameren\t104\174_ami_daily_20150703_001.csv");
            int skipRows = pa.GetControlVal("skipRows", 1000);
            string processCommand = pa.GetControlVal("validateFileCmd", "");
            if (!string.IsNullOrEmpty(processCommand) && !string.IsNullOrEmpty(useRefFile)){// && File.Exists(useRefFile)) {
                string execCmd = string.Format(processCommand, useRefFile, useEnv, procLimit, overrideQry, skipRows);
                pa.Override(execCmd);
                pa["excCmd"] = execCmd;
                // few vars for the older validate pipeline
                pa["srcfile"] = useRefFile;
                pa["bulkReqBody"] = "";
                pa["bulkReqBodyFile"] = "";
                pa["reqVenv"] = useEnv;
                pa["reqVval"] = "99";

                Console.WriteLine($"  submit cmd '{execCmd}'");
                try {
                    ToolingRequest tr = new ToolingRequest();
                    Task<string> t = tr.SubmitBulkDataRequests(pa, context);
                    result = t.Result;

                    //string[] procArgs = execCmd.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    //int rVal = MainProcess(procArgs);
                    //result = rVal.ToString();
                }
                catch (Exception exc) {
                    Console.WriteLine($"MainProcess.ProcessDroppedFile: exception is {exc.Message}, inner={exc.InnerException?.Message}");
                }
            }
            else {

            }
            string[] resRow = result.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
            foreach (string s in resRow) {
                if (s.IndexOf("**SnagCounters**")>0) {
                    return $"{useRefFile} {s}";
                }
            }
            return result;
        }

    }

    class ToolingRequest {
        //private string defToolRoot = "https://localhost/DevWt1/api/Data/consumption_by_meter/prod/4/validate?allowFilter=1&maxRows=300&procLimit=100&sortCol=ami_time_stamp&procResp=1&outputTag=test1&userPath=anon2&contextRecs=25&contextRet=1&expectRows=1&matchtype=get";  // localhost with standalone service.
        private string defToolRoot = "https://localhost/DevWt1/api/Data/consumption_by_meter/prod/4/validate?allowFilter=1&sortCol=ami_time_stamp&procResp=1&outputTag=test1&userPath=anon2&contextRecs=25&contextRet=1&expectRows=1&matchtype=get";  // localhost with standalone service.
        public const string StandardTimeFormat = "yyyy-MM-ddTHH:mm:ss.fffzz00";



        private string _amiReq =
                "/api/current/ami/{0}?dtStt={1}&dtStp={2}&bucketSizeHours={3}&wantMetadata=0&wantCassinfo=0&zmetaQualName=estInd&metaQualVal=yes&requireLogForce=no&seriesSort=desc&seriesValFmt=F3&logCassQueries=1&retdatapa=yes&zspecCassandraKeyspace=not-there&zallowcachecas=no&zresetcache=yes"
            ;

        //private const string CfgReq = "/api/current/config?custom=nyes&oversecj=2216";

        private void InitWebCall(WebClient client) {
            client.Headers["X-BIO-DiagUser"] = "AclaraDiag";
            client.Headers["Accept"] = "application/json";
            client.Headers["Content-Type"] = "text/plain";
        }

        public async Task<string> SubmitBulkDataRequests(ProcessingArgs paIn, ContextData context) {

            Task<string> resTask = null;
            string requestBody =

                "#C where client_id={0} and meter_id = '{1}' and ami_time_stamp>'2015-01-01 00:00:00' and ami_time_stamp<'2017-07-01 00:00:00'\n" +
                @"#F C:\_Test\uil_prod\files\uil_distinct_meters_20170529.csv.000.input.csv" + "\n" +
                "#P csv 1 meter_id,ami_time_stamp,consumption 1,DT,,yyyy-MM-ddTHH:mm:ss.fffzz00;\n";

            requestBody = paIn.GetControlVal("bulkReqBody", requestBody);


            string bodyOverrideFile = paIn.GetControlVal("bulkReqBodyFile", @".\data\BulkCassExtractBody.txt");
            if (!string.IsNullOrEmpty(bodyOverrideFile) && File.Exists(bodyOverrideFile)) {
                try {
                    using (StreamReader sr = new StreamReader(bodyOverrideFile)) {
                        requestBody = sr.ReadToEnd();
                        context.ContextWrite($"using query body file {bodyOverrideFile} is \n{requestBody}\n");
                    }
                }
                catch (Exception exc) {
                    context.ContextWrite(
                        $"exception getting body file {bodyOverrideFile} is {exc.Message}, inner={exc.InnerException?.Message}");
                }
            }


            //reqMsg.Headers.Add("Content-Type","text/plain");
            //string srcFileName = @"C:\_Test\uil_prod\files\uil_distinct_meters_20170529.csv.000.input.csv";
            string srcFileName = paIn.GetControlVal("srcfile", @"C:\_Test\uil_prod\files\uil_distinct_meters_20170529.csv.000.input.csv");
            System.Collections.Concurrent.ConcurrentBag<string> srcFiles = new System.Collections.Concurrent.ConcurrentBag<string>(new[] { srcFileName });  // start off assuming a file.

            if (File.Exists(srcFileName)) {  // might be a local directory with a file list.
                FileAttributes attr = File.GetAttributes(srcFileName);
                //detect whether its a directory or file
                if ((attr & FileAttributes.Directory) == FileAttributes.Directory) {
                    string consolidateFilter = paIn.GetControlVal("consFilter", "*.input.csv");
                    IEnumerable<string> fnames = Directory.EnumerateFiles(srcFileName, consolidateFilter);
                    srcFiles = new System.Collections.Concurrent.ConcurrentBag<string>();
                    foreach (string fname in fnames) {
                        srcFiles.Add(fname);
                    }
                }
            }

            bool doParallel = false;
            if (doParallel) {
                Parallel.ForEach(srcFiles, srcFil => {
                    ProcessingArgs pa = new ProcessingArgs(paIn);
                    Thread.Sleep(1000); // let things stabilize -- seems to be a little race somewhere.
                    resTask = PerformBulkRequestSegment(pa, srcFil, requestBody);
                });
            }
            else {
                foreach (string srcFil in srcFiles) {
                    ProcessingArgs pa = new ProcessingArgs(paIn);
                    //Thread.Sleep(1000); // let things stabilize -- seems to be a little race somewhere.
                    resTask = PerformBulkRequestSegment(pa, srcFil, requestBody);
                }
            }
            return resTask.Result;
        }

        private static string _consolePadlock = "ProcDataCachePadlock";


        private async Task<string> PerformBulkRequestSegment(ProcessingArgs pa, string srcFile, string requestBodyStg) {
            HttpRequestMessage reqMsg = new HttpRequestMessage();
            reqMsg.Headers.Add("X-BIO-DiagUser", "AclaraDiag");
            reqMsg.Headers.Add("Accept", "*/*");
            string[] requestBody = requestBodyStg.Split('\n');
            for (int i = 0; i < requestBody.Length; i++) {
                requestBody[i] = requestBody[i].Trim();
            }

            if (requestBody.Length > 1) {
                requestBody[1] = $"#F {srcFile}";
            }

            string srcDir = Path.GetDirectoryName(srcFile);

            string destFilename = srcFile + ".results.csv";
            if (File.Exists(destFilename)) {
                bool overwriteFiles = pa.GetControlVal("overwriteFiles", 0) > 0;
                if (overwriteFiles) {
                    Console.WriteLine($" SubmitBulkDataRequests '{destFilename}' exists, overwriteFiles={overwriteFiles}");
                    File.Delete(destFilename);
                }
                else {
                    string cTxt = $" ***** SubmitBulkDataRequests '{destFilename}' exists, overwriteFiles={overwriteFiles} so cancelling. ";
                    Console.WriteLine(cTxt);
                    return cTxt;
                }
            }

            string reqBody = requestBody.Aggregate((current, next) => current + Environment.NewLine + next);
            reqMsg.Content = new StringContent(reqBody);
            string excCmd = pa.GetControlVal("excCmd", "");
            string validateRestRoot = pa.GetControlVal("validateRestRoot", "");
            if (!string.IsNullOrEmpty(excCmd) && !string.IsNullOrEmpty(validateRestRoot)) {
                string validateRestPath = pa.GetControlVal("validateRestPath", "");
                string[] reqPars = excCmd.Split(new[] { ' ' },StringSplitOptions.RemoveEmptyEntries);
                string reqParamsStg = reqPars.Aggregate((current, next) => current + "&" + next);
                string uriPath = string.Format(validateRestPath, pa.GetControlVal("reqVsrc", "consumption_by_meter"), pa.GetControlVal("aclenv", "perfd"), pa.GetControlVal("reqVval", "88"));
                string uriStg = $"{validateRestRoot}/{uriPath}?{reqParamsStg}";
                reqMsg.RequestUri = new Uri(uriStg);
            }
            else {
                reqMsg.RequestUri = new Uri(defToolRoot);
            }

            HttpResponseMessage respMsg = new HttpResponseMessage();

            ContextData.InitContextStrings(pa["reqVsrc"], pa["aclenv"], pa["reqVval"], pa, reqMsg);
            pa["alreadyExtractedVars"] = "no";
            ProcessingArgs.AccumulateRequestPars(reqMsg, pa["reqVsrc"], pa["aclenv"], pa["reqVval"], pa);
            pa["bodyOverride"] = "yes";

            DateTime dtStt = DateTime.Now;
            ContextData contextData = new ContextData(pa);
            string result = "";
            try {

                if (pa.GetControlVal("showdetail", 0) > 0) {
                    contextData.ContextWriteSeg("Tooling request", true, dtStt);
                    contextData.ContextWriteBulk("config", pa.AsJson());
                }
                string doRemoteValidate = pa.GetControlVal("doRemoteValidate", "no");
                if (doRemoteValidate.Equals("yes")) {  // TODO: do this later
                    AnalysisUtils.AnalysisValidate(contextData, pa, reqMsg, respMsg);
                }
                else {
                    AnalysisUtils.AnalysisValidate(contextData, pa, reqMsg, respMsg);
                }

                Byte[] byteArray = await respMsg.Content.ReadAsByteArrayAsync();
                result = Encoding.UTF8.GetString(byteArray);
                string ctxOutputName = contextData.MainStream.FileName;
                bool overwriteFiles = pa.GetControlVal("overwriteFiles", 0) > 0;

                if (pa.GetControlVal("doMisFilesCopies", "yes").Equals("yes")) {
                    string csvP0Name = ctxOutputName + "_raw___P0_csv.csv";
                    csvP0Name = pa.GetControlVal("p0filename", csvP0Name);
                    if (File.Exists(destFilename)) {
                        Console.WriteLine(
                            $" SubmitBulkDataRequests '{destFilename}' exists, overwriteFiles={overwriteFiles}");
                        if (overwriteFiles) {
                            File.Delete(destFilename);
                        }
                    }
                    if (pa.GetControlVal("showdetail", 0) > 0) {
                        contextData.ContextWriteSeg("Tooling request", false, dtStt);
                    }
                    File.Copy(csvP0Name, destFilename);

                    string srcDirName = Path.GetDirectoryName(ctxOutputName);
                    string logDirName = Path.GetFileName(ctxOutputName);
                    string searchPat = $"{logDirName}*_pproc.csv";
                    IEnumerable<string> fnames = Directory.EnumerateFiles(srcDirName, searchPat, SearchOption.AllDirectories);
                    List<String> srcFiles = new List<string>();
                    foreach (string fname in fnames) {
                        srcFiles.Add(fname);
                        contextData.ContextWrite("found file name", fname);
                        string tName = Path.GetFileName(fname);
                        string tFrag = tName.Substring(logDirName.Length);
                        string newFileName = srcFile + tFrag;
                        contextData.ContextWrite("will use", newFileName);
                        if (overwriteFiles) {
                            File.Delete(newFileName);
                        }
                        File.Copy(fname, newFileName);
                    }
                }
            }
            catch (Exception exc) {
                contextData.ContextWrite($" exception in Tooling request  {exc.Message}; inner={exc.InnerException?.Message}");
            }
            contextData.End();

            lock (_consolePadlock) {
                Console.WriteLine($" SubmitBulkDataRequests response\n{result}\n");
            }
            return result;
        }
    }



    public class SimpleWebServerLocal : SimpleWebServer {
        public static void LocalLog(string msg) {
            Console.WriteLine(msg, null);
        }

        public SimpleWebServerLocal(ProcessingArgs pa) : base(pa) {
            SetLogFn(LocalLog);
            CheckEndpoints(pa);
        }


        public void CheckEndpoints(ProcessingArgs pa) {
            var epName = "AdvAmiEP";
            string hostAddr = pa.GetControlVal("hostIp", "127.0.0.1");
            string hostPort = pa.GetControlVal("hostPort", "443");
            string tmpStg = ConfigurationManager.AppSettings["AdvAmiHostAddress"];
            if (!string.IsNullOrEmpty(tmpStg)) {
                hostAddr = tmpStg;
            }
            tmpStg = ConfigurationManager.AppSettings["AdvAmiHostPort"];
            if (!string.IsNullOrEmpty(tmpStg)) {
                hostPort = tmpStg;
            }
            //bool cloudConfigSuccess = false;
            try {
                pa["hostIp"] = hostAddr;
                pa["port"] = hostPort;
                InitializeServer(pa);
            }
            catch (Exception e) {
                Trace.TraceError(
                    $"Caught exception in CheckEndpoint SimpleWebServer. {e.Message}, {e.InnerException?.Message} Details: {0}", e);
                Logit($"Event Process Worker  Caught exception in CheckEndpoint SimpleWebServer. {e.Message}, {e.InnerException?.Message}");
            }
        }


    }


    //static class Extensions {
    //    public static IList<T> Clone<T>(this IList<T> listToClone) where T : ICloneable {
    //        return listToClone.Select(item => (T)item.Clone()).ToList();
    //    }
    //}

    public class FilesProcessor {

        private List<FileSystemWatcher> _watchersList;
        private string _watchersListPadlock = "_watchersList";
        volatile int _changeHits;
        private ProcessDroppedFile _postMoveProcessor;
        public bool RequestTerminate { get; set; }
        public bool RequestPause { get; set; }
        public int FileRetries { get; set; }
        public int FileOpenRetries { get; set; }
        public int FileConfigRetries { get; set; }

        public int FilesDropped { get; set; }
         public int ConfigDropped { get; set; }
        public int FilesRelocated { get; set; }
       public int FilesIgnored { get; set; }



        public string ConfigChangeData { get; set; }

        public ProcessDroppedFile SetMoveProcessor(ProcessDroppedFile proc) {
            ProcessDroppedFile oldProc = _postMoveProcessor;
            _postMoveProcessor = proc;
            return oldProc;
        }

        public List<string> QueuedList { get; } = new List<string>();
        private string _queuedListPadlock = "_watchersList";
        public List<string> WatchingDoneList { get; } = new List<string>();
        private string _watchingPath = "[unitialized]";
        private string _destinationPath = "[unitialized]";

        public int Count() {
            return WatchingDoneList.Count;
        }
        public string Status() {
            return $" FilesProcessor.watching {WatchingDoneList.Count} in '{_watchingPath}', dest='{_destinationPath}' changehits={_changeHits} counts: cfg={ConfigDropped} drop={FilesDropped} relocate={FilesRelocated} *ignored={FilesIgnored} retries: file={FileRetries}  cfgr={FileConfigRetries} dropr={FileOpenRetries};";
        }


        /// <summary>
        /// hide default constructor
        /// </summary>
        private FilesProcessor() {
        }

        public FilesProcessor(ProcessingArgs pa, string srcPath, string destPath) {
            WatchFileChanges(pa, srcPath, destPath);
        }

        public void ResetWatchers(ProcessingArgs pa) {
            WatchFileChanges(pa, "","",true);
        }

        public List<string>[] RelocateChanges(List<string> toRelocateList, string relocateDir = null) {
            lock (_watchersListPadlock) {
                string movetoDir = _destinationPath;

                List<string>[] results = {
                    new List<string>(), new List<string>(), new List<string>(), new List<string>(), new List<string>(),
                    new List<string>()
                };
                if (relocateDir != null & Directory.Exists(relocateDir)) {
                    movetoDir = relocateDir;
                }
                foreach (string s in toRelocateList) {
                    if (WatchingDoneList.Contains(s)) {
                        if (File.Exists(s)) {
                            try {
                                results[1].Add(s);
                                string destFileName = Path.Combine(movetoDir, Path.GetFileName(s));
                                if (File.Exists(destFileName)) {
                                    string newFileName = destFileName + "." + DateTime.Now.Ticks;
                                    File.Move(destFileName, newFileName);
                                    results[5].Add($"FilesProcessor.RelocateChanges moved existing '{destFileName}' to '{newFileName}'");
                                }
                                File.Move(s, destFileName);
                                WatchingDoneList.Remove(s);
                            }
                            catch (Exception exc) {
                                results[5].Add($"FilesProcessor.RelocateChanges issue with {s} exception {exc.Message},{exc.InnerException?.Message}");
                            }
                        }
                        else {
                            results[2].Add(s);
                        }
                    }
                    else {
                        results[3].Add(s);
                    }
                    results[0] = new List<string>(WatchingDoneList);
                    List<string> unaccountedFor = Directory.GetFiles(_watchingPath).ToList();
                    foreach (string s1 in new List<string>(unaccountedFor)) {
                        if (WatchingDoneList.Contains(s1)) {
                            unaccountedFor.Remove(s1);
                        }
                    }
                    results[4] = unaccountedFor;
                }
                return results;
            }
        }


        public List<string> GetChangeList() {
            lock (_watchersListPadlock) {
                return new List<string>(WatchingDoneList);
            }
        }


        int _fileRetriesCount = 20;
        int _fileRetriesSleep = 50;

        /// <summary>
        /// Event indicating that some external configuration has changed and our cache may need a refresh
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private void OnChanged(object source, FileSystemEventArgs e) {
            _changeHits += 1;
            if (e.FullPath.Contains("PoisonTerminate")) {
                Console.WriteLine($" **** OnChanged PoisonTerminate {e.FullPath} {Status()} ");
                if (e.FullPath.Contains("Forced")) {
                    RequestTerminate = true;
                    Process process = Process.GetCurrentProcess();
                    process.Kill();
                }
                else if (e.FullPath.Contains("Graceful")) {
                    RequestTerminate = true;
                }
                else {
                    Console.WriteLine($" **** OnChanged PoisonTerminate no terminate ");
                }
            }
            else if (e.FullPath.Contains("PauseProcessing")) {
                Console.WriteLine($" **** OnChanged PauseProcessing {e.FullPath}  ");
                RequestPause = true;
            }
            else if (e.FullPath.Contains("ResumeProcessing")) {
                Console.WriteLine($" **** OnChanged ResumeProcessing {e.FullPath}  ");
                RequestPause = false;
            }
            else if (e.FullPath.Contains("ConfigurationChange")) {
                Console.WriteLine($" **** OnChanged ConfigurationChange {e.FullPath}  ");
                try {

                    int tries = TryOpenFile(e);
                    if (tries != _fileRetriesCount) {
                        FileConfigRetries += 1;
                        string addlInfo = "none";
                        if (tries <= 0) {
                            addlInfo = "possible config file open issue in future";
                        }
                        Console.WriteLine($" **** OnChanged config file '{e.FullPath}' drop ***RETRIED needed {_fileRetriesCount - tries} retires of {_fileRetriesCount} max to open file. addlInfo={addlInfo} ");
                    }

                    Thread.Sleep(200); // little delay since sometimes the file is not fully committed yet.
                    using (StreamReader sr = new StreamReader(e.FullPath)) {
                        string configLine = sr.ReadLine();
                        Console.WriteLine($" **** OnChanged ConfigurationChange data: {configLine}  ");
                        ConfigChangeData = configLine;
                        ConfigDropped++;
                    }
                }
                catch (Exception exc) {
                    Console.WriteLine($" **** OnChanged ConfigurationChange exception {e.FullPath} = {exc.Message} , {exc.InnerException?.Message} ");
                }
            }
            else {
                if (!RequestTerminate) {
                    lock (_watchersListPadlock) {
                        if (!WatchingDoneList.Contains(e.FullPath)) {
                            int tries = TryOpenFile(e);
                            if (tries != _fileRetriesCount) {
                                FileOpenRetries += 1;
                                string addlInfo = "none";
                                if (tries <= 0) {
                                    addlInfo = "possible file open issue in future";
                                }
                                Console.WriteLine($" **** OnChanged file '{e.FullPath}' drop ***RETRIED needed {_fileRetriesCount-tries} retires of {_fileRetriesCount} max to open file. addlInfo={addlInfo} ");
                            }
                            AddProcessFile(e.FullPath);
                            Console.WriteLine($" **** OnChanged file '{e.FullPath}' drop ");
                        }
                    }
                }
                else {
                    Console.WriteLine($" ****OnChanged requestTerminate so not queueing {e.FullPath} ");
                }
            }

        }

        private int TryOpenFile(FileSystemEventArgs e) {
            int tries = _fileRetriesCount;
            if (File.Exists(e.FullPath)) {
                Thread.Sleep(1);  // we often get exception 1'st time, give things a chanch to settle
                while (tries > 0) {
                    try {
                        using (FileStream stream = File.Open(e.FullPath, FileMode.Open, FileAccess.Read)) {
                            byte[] buf = new byte[2];
                            int len = stream.Read(buf, 0, 1);
                            if (len > 0) { // just testing read
                            }
                            break;
                        }
                    }
                    catch (IOException) {
                        FileRetries++;
                        Thread.Sleep(_fileRetriesSleep);
                        tries--;
                    }
                }
            }
            return tries;
        }

        private void AddProcessFile(string fullPath) {
            if (_postMoveProcessor != null) {
                lock (_queuedListPadlock) {
                    if (!QueuedList.Contains(fullPath)) {
                        QueuedList.Add(fullPath);
                        FilesDropped++;
                    }
                    else {
                        FilesIgnored++;
                    }
                }
            }
        }

        public int DoProcessOne() {
            int status = -1;
            if (QueuedList.Count > 0) {
                string one;
                lock (_queuedListPadlock) {
                    one = QueuedList[0];
                    QueuedList.RemoveAt(0);
                }
                if (!string.IsNullOrEmpty(one)) {
                    status = DoProcessFile(one);
                }
            }
            return status;
        }

        private int DoProcessFile(string fullPath) {
            int errorCount = 0;
            if (_postMoveProcessor != null) {
                if (fullPath.Contains(" ")) {
                    string oldPath = fullPath;
                    fullPath = fullPath.Replace(" ", "_");
                    File.Move(oldPath,fullPath);
                }
                string tVal = _postMoveProcessor(this, fullPath);
                int.TryParse(tVal, out errorCount);
            }
            if (errorCount == 0) {  // don't record file if any errors appeared
                WatchingDoneList.Add(fullPath);
            }
            return errorCount;
        }

        public void FlushPending() {
            List<string> unaccountedFor = Directory.GetFiles(_watchingPath).ToList();
            foreach (string s1 in new List<string>(unaccountedFor)) {
                if (WatchingDoneList.Contains(s1)) {
                    unaccountedFor.Remove(s1);
                }
                else {
                    AddProcessFile(s1);
                }
            }

        }


        /// <summary>
        /// Set up, if necessary, the watching of files in the path specified (or a part of) the call param.
        /// note: this is is set up for more but only handles a single so only do ONCE until _watchingPath is made a collection.
        /// </summary>
        /// <param name="pa"></param>
        /// <param name="path2Watch">path name for files to watch (can be a filename -- we will extract the path)</param>
        /// <param name="destinationPath"></param>
        /// <param name="forceInit"></param>
        public void WatchFileChanges(ProcessingArgs pa, string path2Watch, string destinationPath, bool forceInit = false) {

            if (_watchersList != null) {
                if (!forceInit) {
                    Console.WriteLine("FilesProcessor.WatchFileChanges: not null and not forced");
                    return;
                }
                else {
                    Console.WriteLine("FilesProcessor.WatchFileChanges force reinit: ");
                    foreach (FileSystemWatcher fileSystemWatcher in _watchersList) {
                        fileSystemWatcher.EnableRaisingEvents = false;
                    }
                    _watchersList = null;   // watch out -- this will leak a little (but shouldn't be called much if at all)
                }
            }

            _fileRetriesCount = pa.GetControlVal("fileRetriesCount", _fileRetriesCount);
            _fileRetriesSleep = pa.GetControlVal("fileRetriesSleep", _fileRetriesSleep);
            if (string.IsNullOrEmpty(path2Watch)) path2Watch = pa.GetControlVal("dropLocation", "");
            if (!Directory.Exists(path2Watch)) {
                Console.WriteLine($"FilesProcessor.WatchFileChanges: {path2Watch} does not exist");
                Directory.CreateDirectory(path2Watch);
                Console.WriteLine($"FilesProcessor.WatchFileChanges: created destination '{path2Watch}' ");
            }
            string path = path2Watch;
            if (_watchersList == null) {
                lock (_watchersListPadlock) {
                    // check again under lock
                    if (_watchersList == null) {
                        // this is how we see if the config file changes
                        _watchersList = new List<FileSystemWatcher>();
                    }
                }
            }

            if (File.Exists(path2Watch)) {
                // if a file get the path where the file exists
                path = Path.GetDirectoryName(path2Watch);
            }
            if (!Directory.Exists(path)) {
                Console.WriteLine($"FilesProcessor.WatchFileChanges: nonexistant watch path '{path}' ");
                return;
            }
            _watchingPath = path;
            if (string.IsNullOrEmpty(destinationPath)) destinationPath = pa.GetControlVal("dropRelocate", "");
            if (!string.IsNullOrEmpty(destinationPath)) {
                try {
                    if (!Directory.Exists(destinationPath)) {
                        Directory.CreateDirectory(destinationPath);
                        Console.WriteLine($"FilesProcessor.WatchFileChanges: created destination '{destinationPath}' ");
                    }
                    _destinationPath = destinationPath;
                }
                catch (Exception exc) {
                    Console.WriteLine($"FilesProcessor.WatchFileChanges: exception -- cannot use supplied path '{destinationPath}' {exc.Message}, {exc.InnerException?.Message} ");
                }
            }
            else {
                _destinationPath = Path.Combine(_watchingPath, pa.GetControlVal("dropLocationRelocate", "relocated"));
            }
            if (!Directory.Exists(_destinationPath)) {
                Directory.CreateDirectory(_destinationPath);
                Console.WriteLine($"FilesProcessor.WatchFileChanges: created destination '{_destinationPath}' ");
            }

            bool watching = false;
            foreach (FileSystemWatcher watcher in _watchersList) {
                if (watcher.Path == path) {
                    watching = true;
                    break;
                }
            }
            if (!watching) {
                lock (_watchersList) {
                    // check again under lock
                    foreach (FileSystemWatcher watcher in _watchersList) {
                        if (watcher.Path == path) {
                            watching = true;
                            break;
                        }
                    }
                    if (!watching) {
                        string watchFileTemplates = pa.GetControlVal("dropFiles2Watch", "*.csv");
                        string[] watchingFiles = watchFileTemplates.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string ft in watchingFiles) {
                            FileSystemWatcher configFileWatcher = new FileSystemWatcher();
                            configFileWatcher.Path = path;
                            //configFileWatcher.NotifyFilter = NotifyFilters.LastWrite;
                            configFileWatcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite
                                                             | NotifyFilters.FileName | NotifyFilters.DirectoryName;
                            configFileWatcher.Filter = ft;
                            //configFileWatcher.Created += OnChanged;
                            configFileWatcher.Changed += OnChanged;
                            configFileWatcher.Created += OnChanged;
                            configFileWatcher.EnableRaisingEvents = true;
                            _watchersList.Add(configFileWatcher);
                        }
                    }
                }
            }
        }


    }


    public class ZipManager {

        private ZipManager() {  // hide empty constructor
        }


        public void ExtractAndDrop(string zipSrc, string inDropLocation) {
            if (string.IsNullOrEmpty(inDropLocation)) {
                inDropLocation = pa.GetControlVal("dropLocation", @"c:\junk\t114");
            }
            try {
                bool failed = false;
                string errorInfo = "";
                try {
                    FileAttributes attr = File.GetAttributes(zipSrc);
                    int dropCounter = 0;  // used to load balance files across drop locations
                    if ((attr & FileAttributes.Directory) == FileAttributes.Directory) {
                        string consolidateFilter = pa.GetControlVal("consFilter", "*.zip");
                        IEnumerable<string> fnames = Directory.EnumerateFiles(zipSrc, consolidateFilter);
                        foreach (string fname in fnames) {
                            context.ContextWrite("ziptest process", fname);
                            dropCounter = TestFileExtract(fname, inDropLocation, dropCounter);
                            context.IncrementCounter("zipsProcessed");
                        }
                    }

                    else  {
                        TestFileExtract(zipSrc, inDropLocation,0);
                        context.IncrementCounter("zipsProcessed");
                    }
                }
                catch (Exception exc) {
                    failed = true;
                    errorInfo = $"ziptest exception {exc.Message}, {exc.InnerException?.Message}";
                    context.ContextWrite("ziptest exception", errorInfo);
                }
                if (failed) {
                    string errorTxt =
                        $"ziptest {errorInfo}, needs a zipSrc input file/directory and dropLocation path.  '{zipSrc}'  and '{inDropLocation}' .";
                    Console.WriteLine(errorTxt);
                    context.IncrementCounter("errors");
                }
            }
            catch (Exception exc) {
                context.IncrementCounter("ZipManagerErrors");
                context.IncrementCounter("errors");
                context.ContextWrite($"ZipManager.ExtractAndDrop",
                    $" exception {exc.Message}, {exc.InnerException?.Message}");
            }
        }

        ProcessingArgs pa;
        private ContextData context;
        private string[] args;
        public ZipManager(ProcessingArgs ipa, ContextData icontext, string[] iargs) {
            pa = ipa;
            context = icontext;
            args = iargs;
        }

        public const string StandardTimeFormatBaseTemp = "yyyy-MM-ddTHH:mm:ss{0}";

        public void Convert2TallAmiFS(string zipFile, string destPath) {
            if (string.IsNullOrEmpty(destPath)) {
                destPath = pa.GetControlVal("dropLocation", @"c:\junk\t114");
            }
            try {
                if (!Directory.Exists(destPath)) {
                    Directory.CreateDirectory(destPath);
                    context.ContextWrite("ZipManager.Convert2TallAmi", $" create drop location {destPath} ");
                    context.IncrementCounter("zipDropCreate");
                }
                string useHeaders = pa.GetControlVal("useHeaders", @"ClientId,MeterId,AccountNumber,ServicePointId,CommodityId,UOMId,VolumeFactor,Direction,TOUID,ProjectedReadDate,TimeStamp,Timezone,IntValue0000");
                string csvColTemp = pa.GetControlVal("csvColTemp", @"-,-,-,-,-,-,-,-,0,-,-,-,-");
                string csvColMap = pa.GetControlVal("csvColMap", @"0,1,2,3,4,5,6,7,-1,8,9,10,-1");
                int csvWideTimestampCol = pa.GetControlVal("csvTallTimestampCol", 9);
                int csvTallTimestampCol = pa.GetControlVal("csvTallTimestampCol", 10);
                int csvWideValCol = pa.GetControlVal("csvTallValCol", 11);
                int csvTallValCol = pa.GetControlVal("csvTallValCol", 12);
                string tsTimeZoneInfo = pa.GetControlVal("tsTimeZoneInfo", "Z");
                string StandardTimeFormatBase = string.Format(StandardTimeFormatBaseTemp, tsTimeZoneInfo);
                string[] csvColArray = csvColMap.Split(',');
                int[] csvIntMap = new int[csvColArray.Length];
                for (int i = 0; i < csvIntMap.Length; i++) {
                    csvIntMap[i] = int.Parse(csvColArray[i]);
                }

                bool retainTallSrc = pa.GetControlVal("retainTallSrc", "yes").Equals("yes");
                bool haveHeaders = pa.GetControlVal("haveCsvHeaders", "yes").Equals("yes");
                bool allowOverwrite = pa.GetControlVal("overwriteDrop", "no").Equals("yes");
                int procLimit = pa.GetControlVal("procLimit", 100);
                bool stripCsvQuotes = string.Equals(pa.GetControlVal("stripCsvQ", "no"), "yes");
                bool addCsvQuotes = string.Equals(pa.GetControlVal("addCsvQuotes", "yes"), "yes");
                using (ZipArchive archive = ZipFile.OpenRead(zipFile)) {
                    string tallZipFileName;
                    foreach (ZipArchiveEntry entry in archive.Entries) {
                        string tallFileName = $"{entry.FullName}.tall.csv";
                        string tallFileZipName = $"{entry.FullName}.tall.csv.zip";
                        tallZipFileName = Path.Combine(destPath, tallFileZipName);
                        string destFileName = Path.Combine(destPath, tallFileName);
                        if (File.Exists(destFileName)) {
                            if (allowOverwrite) {
                                File.Delete(destFileName);
                                context.IncrementCounter("deleteUnzipDest");
                                context.ContextWrite("ziptest extract overwrite", destFileName);
                            }
                            else {
                                string err = $" Error: ZipManager.Convert2TallAmi overwrite not allowed for {destFileName}";
                                context.ContextWrite(err);
                                throw new Exception(err);
                            }
                        }
                        string tempFilePath = Path.Combine(destPath, "temp");
                        if (!Directory.Exists(tempFilePath)) {
                            Directory.CreateDirectory(tempFilePath);
                            context.ContextWrite("ZipManager.Convert2TallAmi", $" create tempFilePath {tempFilePath} ");
                            context.IncrementCounter("zipTempCreate");
                        }
                        string tallFileTempName = Path.Combine(tempFilePath, tallFileName);
                        using (StreamWriter sw = new StreamWriter(tallFileTempName)) {
                            var lineCount = 0;
                            string headers = "";
                            using (var stream = entry.Open()) {
                                using (StreamReader sr = new StreamReader(stream)) {
                                    if (haveHeaders) {
                                        if (string.IsNullOrEmpty(headers)) {
                                            headers = sr.ReadLine(); // remove and save headers
                                            if (!string.IsNullOrEmpty(useHeaders)) {
                                                sw.WriteLine(useHeaders);
                                            }
                                            else {
                                                sw.WriteLine(headers);
                                            }
                                        }
                                        else {
                                            sr.ReadLine(); // remove headers
                                        }
                                    }
                                    while (!sr.EndOfStream) {
                                        string rec = sr.ReadLine();
                                        //sw.WriteLine($" will expand {rec}");
                                        if (stripCsvQuotes) {
                                            rec = rec.Replace("\"", "");
                                        }
                                        string[] cols = rec.Split(',');
                                        DateTime dt = DateTime.Parse(cols[csvWideTimestampCol]);
                                        int startSrcValsCol = csvWideValCol;
                                        for (int i = 0; i < 24; i++) {
                                            string[] newCsvVals = csvColTemp.Split(',');
                                            for (int cm = 0; cm < newCsvVals.Length; cm++) {
                                                if (csvIntMap[cm] >= 0) {
                                                    newCsvVals[cm] = cols[csvIntMap[cm]];
                                                }
                                            }
                                            newCsvVals[csvTallTimestampCol] = dt.ToString(StandardTimeFormatBase);
                                            newCsvVals[csvTallValCol] = cols[startSrcValsCol + i];
                                            if (addCsvQuotes) {
                                                for (int cm = 0; cm < newCsvVals.Length; cm++) {
                                                    //newCsvVals[cm] = $"\"{newCsvVals[cm]}\"";
                                                    //newCsvVals[cm] = string.Format("\"{0}\"", newCsvVals[cm]);
                                                    newCsvVals[cm] = '\"' + newCsvVals[cm] + '\"';  // significantly faster than others 1.5 (1.0 for none, 2.0 for interp)
                                                }
                                            }

                                            string newCsv = newCsvVals.Aggregate((current, next) => current + ',' + next);
                                            sw.WriteLine(newCsv);
                                            dt = dt.AddHours(1);
                                        }
                                        context.Add2Counter("unzip2TallRecCnt", 24);

                                        lineCount++;
                                        if (lineCount >= procLimit) {
                                            break;
                                        }
                                    }
                                }
                            }
                            if (lineCount > 0) {
                                context.Add2Counter("unzipCvtRecCnt", lineCount);
                            }
                        }
                        using (ZipArchive narchive = ZipFile.Open(tallZipFileName, ZipArchiveMode.Update)) {
                            narchive.CreateEntryFromFile(tallFileTempName, tallFileName);
                            context.ContextWrite("ZipManager.Convert2TallAmi", $"generated {tallZipFileName} from {tallFileName}");
                        }
                        FileInfo fit = new FileInfo(tallFileTempName);
                        FileInfo fiz = new FileInfo(tallZipFileName);
                        context.Add2Counter("unzipCvtRecTallSize", fit.Length);
                        context.Add2Counter("unzipCvtRecZipSize", fiz.Length);
                        context.ContextWrite("ZipManager.Convert2TallAmi", $"zipsize {fiz.Length} tallsize {fit.Length}");
                        if (!retainTallSrc) {
                            File.Delete(tallFileTempName);
                            context.IncrementCounter("unzipCvtRecDelSrcCnt");
                        }
                    }
                }
                string moveToPath = Path.Combine(Path.GetDirectoryName(zipFile), "extracted");
                if (!Directory.Exists(moveToPath)) {
                    Directory.CreateDirectory(moveToPath);
                    context.IncrementCounter("unzipCvtMove");
                }
                string destFile = Path.Combine(moveToPath, Path.GetFileName(zipFile));
                if (File.Exists(destFile) && pa.GetControlVal("overwriteDrop", "no").Equals("yes")) {
                    File.Delete(destFile);
                    context.IncrementCounter("unzipCvtDelete");
                    context.ContextWrite("ZipManager.Convert2TallAmi done overwrite", destFile);
                }
                File.Move(zipFile, destFile);
            }
            catch (Exception exc) {
                context.IncrementCounter("errors");
                context.ContextWrite($"ZipManager.Convert2TallAmi", $" exception {exc.Message}, {exc.InnerException?.Message}");
            }

        }

        public void Convert2TallAmi(string zipFile, string destPath) {
            if (string.IsNullOrEmpty(destPath)) {
                destPath = pa.GetControlVal("dropLocation", @"c:\junk\t114");
            }
            try {
                if (!Directory.Exists(destPath)) {
                    Directory.CreateDirectory(destPath);
                    context.ContextWrite("ZipManager.Convert2TallAmi", $" create drop location {destPath} ");
                    context.IncrementCounter("zipDropCreate");
                }
                string useHeaders = pa.GetControlVal("useHeaders", @"ClientId,MeterId,AccountNumber,ServicePointId,CommodityId,UOMId,VolumeFactor,Direction,TOUID,ProjectedReadDate,TimeStamp,Timezone,IntValue0000");
                string csvColTemp = pa.GetControlVal("csvColTemp", @"-,-,-,-,-,-,-,-,0,-,-,-,-");
                string csvColMap = pa.GetControlVal("csvColMap", @"0,1,2,3,4,5,6,7,-1,8,9,10,-1");
                int csvWideTimestampCol = pa.GetControlVal("csvTallTimestampCol", 9);
                int csvTallTimestampCol = pa.GetControlVal("csvTallTimestampCol", 10);
                int csvWideValCol = pa.GetControlVal("csvTallValCol", 11);
                int csvTallValCol = pa.GetControlVal("csvTallValCol", 12);
                string tsTimeZoneInfo = pa.GetControlVal("tsTimeZoneInfo", "Z");
                string StandardTimeFormatBase = string.Format(StandardTimeFormatBaseTemp, tsTimeZoneInfo);
                string[] csvColArray = csvColMap.Split(',');
                int[] csvIntMap = new int[csvColArray.Length];
                for (int i = 0; i < csvIntMap.Length; i++) {
                    csvIntMap[i] = int.Parse(csvColArray[i]);
                }

                bool retainTallSrc = pa.GetControlVal("retainTallSrc", "yes").Equals("yes");
                bool haveHeaders = pa.GetControlVal("haveCsvHeaders", "yes").Equals("yes");
                bool allowOverwrite = pa.GetControlVal("overwriteDrop", "no").Equals("yes");
                int procLimit = pa.GetControlVal("procLimit", 100);
                bool stripCsvQuotes = string.Equals(pa.GetControlVal("stripCsvQ", "no"), "yes");
                bool addCsvQuotes = string.Equals(pa.GetControlVal("addCsvQuotes", "yes"), "yes");
                using (ZipArchive archive = ZipFile.OpenRead(zipFile)) {
                    string tallZipFileName;
                    foreach (ZipArchiveEntry entry in archive.Entries) {
                        string tallFileName = $"{entry.FullName}.tall.csv";
                        string tallFileZipName = $"{entry.FullName}.tall.csv.zip";
                        tallZipFileName = Path.Combine(destPath, tallFileZipName);

                        string destFileName = Path.Combine(destPath, tallFileName);
                        if (File.Exists(destFileName)) {
                            if (allowOverwrite) {
                                File.Delete(destFileName);
                                context.IncrementCounter("deleteUnzipDest");
                                context.ContextWrite("ziptest extract overwrite", destFileName);
                            }
                            else {
                                string err = $" Error: ZipManager.Convert2TallAmi overwrite not allowed for {destFileName}";
                                context.ContextWrite(err);
                                throw new Exception(err);
                            }
                        }
                        //string tempFilePath = Path.Combine(dropLocation, "temp");
                        //if (!Directory.Exists(tempFilePath)) {
                        //    Directory.CreateDirectory(tempFilePath);
                        //    context.ContextWrite("ZipManager.Convert2TallAmi", $" create tempFilePath {tempFilePath} ");
                        //    context.IncrementCounter("zipTempCreate");
                        //}
                        //string tallFileTempName = Path.Combine(tempFilePath, tallFileName);

                        //using (StreamWriter sw = new StreamWriter(tallFileTempName)) {
                        using (var sw = new MemoryStream()) {
                            var lineCount = 0;
                            string headers = "";
                            using (var stream = entry.Open()) {
                                using (StreamReader sr = new StreamReader(stream)) {
                                    if (haveHeaders) {
                                        if (string.IsNullOrEmpty(headers)) {
                                            headers = sr.ReadLine(); // remove and save headers
                                            if (!string.IsNullOrEmpty(useHeaders)) {
                                                byte[] bytes = Encoding.UTF8.GetBytes(useHeaders + "\n");
                                                sw.Write(bytes, 0, bytes.Length);
                                                //sw.WriteLine(useHeaders);
                                            }
                                            else {
                                                byte[] bytes = Encoding.UTF8.GetBytes(headers + "\n");
                                                sw.Write(bytes, 0, bytes.Length);
                                                //sw.WriteLine(headers);
                                            }
                                        }
                                        else {
                                            sr.ReadLine(); // remove headers
                                        }
                                    }
                                    while (!sr.EndOfStream) {
                                        string rec = sr.ReadLine();
                                        //sw.WriteLine($" will expand {rec}");
                                        if (stripCsvQuotes) {
                                            rec = rec.Replace("\"", "");
                                        }
                                        string[] cols = rec.Split(',');
                                        DateTime dt = DateTime.Parse(cols[csvWideTimestampCol]);
                                        int startSrcValsCol = csvWideValCol;
                                        for (int i = 0; i < 24; i++) {
                                            string[] newCsvVals = csvColTemp.Split(',');
                                            for (int cm = 0; cm < newCsvVals.Length; cm++) {
                                                if (csvIntMap[cm] >= 0) {
                                                    newCsvVals[cm] = cols[csvIntMap[cm]];
                                                }
                                            }
                                            newCsvVals[csvTallTimestampCol] = dt.ToString(StandardTimeFormatBase);
                                            newCsvVals[csvTallValCol] = cols[startSrcValsCol + i];
                                            if (addCsvQuotes) {
                                                for (int cm = 0; cm < newCsvVals.Length; cm++) {
                                                    //newCsvVals[cm] = $"\"{newCsvVals[cm]}\"";
                                                    //newCsvVals[cm] = string.Format("\"{0}\"", newCsvVals[cm]);
                                                    newCsvVals[cm] = '\"' + newCsvVals[cm] + '\"';  // significantly faster than others 1.5 (1.0 for none, 2.0 for interp)
                                                }
                                            }

                                            string newCsv = newCsvVals.Aggregate((current, next) => current + ',' + next);
                                            byte[] bytes = Encoding.UTF8.GetBytes(newCsv + "\n");
                                            sw.Write(bytes, 0, bytes.Length);
                                            //sw.WriteLine(newCsv);
                                            dt = dt.AddHours(1);
                                        }
                                        context.Add2Counter("unzip2TallRecCnt", 24);

                                        lineCount++;
                                        if (lineCount >= procLimit) {
                                            break;
                                        }
                                    }
                                }
                            }
                            if (lineCount > 0) {
                                context.Add2Counter("unzipCvtRecCnt", lineCount);
                            }
                            using (ZipArchive narchive = ZipFile.Open(tallZipFileName, ZipArchiveMode.Update)) {
                                sw.Flush();
                                sw.Position = 0;
                                ZipArchiveEntry nentry = narchive.CreateEntry(tallFileName,CompressionLevel.Fastest);
                                using (Stream entryStream = nentry.Open()) {
                                    sw.CopyTo(entryStream);
                                }
                                //narchive.CreateEntryFromFile(tallFileTempName, tallFileName);
                                context.ContextWrite("ZipManager.Convert2TallAmi", $"generated {tallZipFileName} from {tallFileName}");
                            }
                            //FileInfo fit = new FileInfo(tallFileTempName);
                            FileInfo fiz = new FileInfo(tallZipFileName);
                            context.Add2Counter("unzipCvtRecTallSize", sw.Length);
                            context.Add2Counter("unzipCvtRecZipSize", fiz.Length);
                            context.ContextWrite("ZipManager.Convert2TallAmi", $"zipsize {fiz.Length} tallsize {sw.Length}");
                            //if (!retainTallSrc) {
                            //    File.Delete(tallFileTempName);
                            //    context.IncrementCounter("unzipCvtRecDelSrcCnt");
                            //}
                        }
                    }
                }
                string moveToPath = Path.Combine(Path.GetDirectoryName(zipFile), "extracted");
                if (!Directory.Exists(moveToPath)) {
                    Directory.CreateDirectory(moveToPath);
                    context.IncrementCounter("unzipCvtMove");
                }
                string destFile = Path.Combine(moveToPath, Path.GetFileName(zipFile));
                if (File.Exists(destFile) && pa.GetControlVal("overwriteDrop", "no").Equals("yes")) {
                    File.Delete(destFile);
                    context.IncrementCounter("unzipCvtDelete");
                    context.ContextWrite("ZipManager.Convert2TallAmi done overwrite", destFile);
                }
                File.Move(zipFile, destFile);
            }
            catch (Exception exc) {
                context.IncrementCounter("errors");
                context.ContextWrite($"ZipManager.Convert2TallAmi", $" exception {exc.Message}, {exc.InnerException?.Message}");
            }

        }




        public int TestFileExtract(string zipFile, string inDropLocation, int dropCounter) {


            string[] dropLocations = inDropLocation.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            try {

                using (ZipArchive archive = ZipFile.OpenRead(zipFile)) {
                    foreach (ZipArchiveEntry entry in archive.Entries) {
                        if (dropCounter >= dropLocations.Length) {
                            dropCounter = 0;
                        }
                        string dropLocation = dropLocations[dropCounter++];
                        string tmpLocation = Path.Combine(dropLocation, "zipTmp");
                        tmpLocation = pa.GetControlVal("zipTmpLocation", tmpLocation); // maybe override
                        if (!Directory.Exists(tmpLocation)) {
                            Directory.CreateDirectory(tmpLocation);
                            context.ContextWrite("ZipManager.ExtractAndDrop.TestFileExtract", $" create tmp location {tmpLocation} ");
                            context.IncrementCounter("zipTmpCreate");
                        }
                        if (!Directory.Exists(dropLocation)) {
                            Directory.CreateDirectory(dropLocation);
                            context.ContextWrite("ZipManager.ExtractAndDrop.TestFileExtract", $" create drop location {dropLocation} ");
                            context.IncrementCounter("zipDropCreate");
                        }

                        string destFileName = Path.Combine(dropLocation, entry.FullName);
                        if (File.Exists(destFileName) && pa.GetControlVal("overwriteDrop", "no").Equals("yes")) {
                            File.Delete(destFileName);
                            context.IncrementCounter("deleteUnzipDest");
                            context.ContextWrite("ziptest extract overwrite", destFileName);
                        }
                        var lineCount = 0;
                        if (pa.GetControlVal("cntUnzipRecs", "yes").Equals("yes")) {
                            DateTime dtSttCnt = DateTime.Now;
                            using (var stream = entry.Open()) {
                                using (StreamReader sr = new StreamReader(stream)) {
                                    while (!sr.EndOfStream) {
                                        sr.ReadLine();
                                        lineCount++;
                                    }
                                }
                            }
                            DateTime dtStPCnt = DateTime.Now;
                            context.Add2Counter("zipCntTime", (long)dtStPCnt.Subtract(dtSttCnt).TotalMilliseconds);
                        }
                        if (lineCount > 0) {
                            context.Add2Counter("unzipRecCnt", lineCount - 1);
                        }
                        if (pa.GetControlVal("useZipTmp", "yes").Equals("yes")) {
                            string tmpFile = Path.Combine(tmpLocation, "unzip.tmp");
                            DateTime dtSttUnzip = DateTime.Now;
                            entry.ExtractToFile(tmpFile,true); // watcher tries to grab viles while unzipping, so shorten window so can keep a short timeout
                            DateTime dtStpUnzip = DateTime.Now;
                            File.Move(tmpFile, destFileName);
                            DateTime dtStpMove = DateTime.Now;
                            context.Add2Counter("zipUnzipTime",(long) dtStpUnzip.Subtract(dtSttUnzip).TotalMilliseconds);
                            context.Add2Counter("zipMoveTime", (long) dtStpMove.Subtract(dtStpUnzip).TotalMilliseconds);
                            string info = $" w/ tmp extract {lineCount} lines from '{entry.Name}' to '{destFileName}'";
                            context.ContextWrite("ZipManager.TestFileExtract", info);
                            Console.WriteLine($"ZipManager.TestFileExtract {info}");
                        }
                        else {
                            DateTime dtSttUnzip = DateTime.Now;
                            entry.ExtractToFile(destFileName,true);
                            DateTime dtStpUnzip = DateTime.Now;
                            context.Add2Counter("zipUnzipTime", (long)dtStpUnzip.Subtract(dtSttUnzip).TotalMilliseconds);
                            string info = $" w/o tmp extract {lineCount} lines from '{entry.Name}' to '{destFileName}'";
                            context.ContextWrite("ZipManager.TestFileExtract", info);
                        }
                    }
                }
                if (pa.GetControlVal("moveAfterZip", "yes").Equals("yes")) {
                    string moveToPath = Path.Combine(Path.GetDirectoryName(zipFile), "extracted");
                    moveToPath = pa.GetControlVal("zipRelocatePath", moveToPath);
                    if (!Directory.Exists(moveToPath)) {
                        Directory.CreateDirectory(moveToPath);
                        context.IncrementCounter("createUnzipMove");
                        context.ContextWrite("createUnzipMove", moveToPath);
                    }
                    string destFile = Path.Combine(moveToPath, Path.GetFileName(zipFile));
                    if (File.Exists(destFile) && pa.GetControlVal("overwriteDrop", "no").Equals("yes")) {
                        File.Delete(destFile);
                        context.IncrementCounter("deleteUnzipMove");
                        context.ContextWrite("ziptest done overwrite", destFile);
                    }
                    File.Move(zipFile, destFile);
                    context.ContextWrite("createUnzipMove", $" from '{zipFile}' to '{destFile}'");
                }
            }
            catch (Exception exc) {
                context.IncrementCounter("errors");
                context.ContextWrite($"ZipManager.TestFileExtract", $" exception {exc.Message}, {exc.InnerException?.Message} src='{zipFile}', topath='{inDropLocation}'");
            }
            return dropCounter;
        }

        private void Read2Memory(string path) {
            using (var file = File.OpenRead(path))
            using (var zip = new ZipArchive(file, ZipArchiveMode.Read)) {
                foreach (var entry in zip.Entries) {
                    using (StreamReader sr = new StreamReader(entry.Open())) {
                        while (!sr.EndOfStream) {

                        }
                    }
                }
            }
        }


    }




}

