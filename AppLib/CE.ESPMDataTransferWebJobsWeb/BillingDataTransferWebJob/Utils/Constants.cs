﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillingDataTransferWebJob.Utils
{

    public class Constants
    {
        public string USERNAME = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes("UILWEBJOBUSER"));
        public string PASSWORD = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes("qwerty!1"));
        public const string AUTHTOKEN = "WebToken";
        public const string APIBASEURL = "APIBaseURL";
        public const string BILLINGPERIOD = "BillingPeriod";
        public const string COMMODITY = "Commodity";
        public const string USERCREDS = "WebjobCreds";
    }

    public class APIResourceURLS
    {
        public const string GET_METER_BILLING_DATA = "/api/PortalsApiMap/GetAllMeterBillingForMeter";
        public const string POST_METER_BILLING_DATA = "/api/EnergyStar/PostConsumptionData";
        public const string GET_USER_AUTH_TOKEN = "/api/PortalsApi/UserAuth";
        public const string POST_TRACE_LOG = "/api/PortalsApi/TraceLog";
        public const string GET_ALL_METER_IDS = "/api/PortalsApiMap/GetAllMeterIds";
        public const string GET_ACCEPTED_METER_END_DATE_URL = "/api/EnergyStar/MeterEndDate";
    }
}
