﻿using System;
using System.Runtime.Remoting.Messaging;
using BillingDataTransferWebJob.Handlers;
using BillingDataTransferWebJob.RestSharp;
using BillingDataTransferWebJob.Utils;
using CE.ESPMDataModel.ESPMModels;
using Microsoft.Azure.WebJobs;

namespace BillingDataTransferWebJob
{
    public class Function
    {
        private static RestSharpHelper restSharpHelper;
        //string authToken = "Jragpl4AqzzDE1i4ty-V2hwSozynTIT3lIX068BukVEzR7jCvuLVphX6wSd_PNFXgcQjTZubytEwVbK3ksZfYMLI8dVPVFhCAPegt4uNDSA";

        public Function()
        {
            restSharpHelper = new RestSharpHelper();
            //restSharpHelper.authToken = authToken;
        }
        [NoAutomaticTrigger]
        public void PostBillingData(string userCreds)
        {
            Console.WriteLine($"Starting Data Transfer Job at {DateTime.Now}");

            var authUser = UserAuthHandler.GetUserAuthToken(restSharpHelper, userCreds);

            if (authUser.User == null) return;

            UserAuthHandler.LogTrace(restSharpHelper, "WebJob PostBillingData - Start", "info");
            restSharpHelper.AuthToken = authUser.User;

            Console.WriteLine($"Getting Meter Ids at {DateTime.Now}");
            var meters = BillingDataHandler.GetAllMeterIds(restSharpHelper);
            UserAuthHandler.LogTrace(restSharpHelper, "WebJob PostBillingData - Meter Count: " + meters.Count, "info");
            foreach (var meter in meters)
            {
                UserAuthHandler.LogTrace(restSharpHelper, "WebJob PostBillingData - GetMeterStartDateForBilling: ", "debug");
                var meterStartdate =
                    ConsumptionDataHandler.GetMeterStartDateForBilling(restSharpHelper, $"?meterId={meter}");
                if (meterStartdate == null) { UserAuthHandler.LogTrace(restSharpHelper, "PostBillingData - meterStartdate is null: "+meter, "info"); continue;}
                Console.WriteLine($"Getting Billing data Meter Ids at {DateTime.Now} for meter id {meter} witn meter end date {meterStartdate}");
                var billingdata = BillingDataHandler.GetAllMeterBillingdata(restSharpHelper,
                    new BillDataRequest()
                    {
                        MeterId = meter,
                        StartDate = meterStartdate,
                        EndDate = DateTime.Now.ToString("yyyy-MM-dd")
                    });

                if (billingdata?.Count > 0)
                {
                    Console.WriteLine($"WebJob Posting Billing data Meter Ids at {DateTime.Now} for meter id {meter}");
                    var billingdataSent = BillingDataHandler.PostBillingDataToESPM(restSharpHelper, billingdata);
                }
            }

            UserAuthHandler.LogTrace(restSharpHelper, "WebJob PostBillingData - End", "info");

        }
    }
}
