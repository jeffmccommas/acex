﻿using System;
using System.Configuration;
using System.Text;
using BillingDataTransferWebJob.Utils;

namespace BillingDataTransferWebJob
{
    // To learn more about Microsoft Azure WebJobs SDK, please see https://go.microsoft.com/fwlink/?LinkID=320976
    class Program
    {
        // Please set the following connection strings in app.config for this WebJob to run:
        // AzureWebJobsDashboard and AzureWebJobsStorage
        static void Main()
        {
            var userCreds = ConfigurationManager.AppSettings[Constants.USERCREDS].Split(',');
            Console.WriteLine($"Starting WebJob to transfer billing data at {DateTime.Now}");
            foreach (var userInfo in userCreds)
            {
                Console.WriteLine($"Starting WebJob to transfer billing data to ESPM at {DateTime.Now} for userid: {Encoding.UTF8.GetString(Convert.FromBase64String(userInfo.Split(':')[0]))}");
                new Function().PostBillingData(userInfo);
            }
            
        }
    }
}
