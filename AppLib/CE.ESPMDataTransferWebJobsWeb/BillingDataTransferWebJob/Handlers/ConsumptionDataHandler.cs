﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using BillingDataTransferWebJob.RestSharp;
using BillingDataTransferWebJob.Utils;
using Newtonsoft.Json;

namespace BillingDataTransferWebJob.Handlers
{
    public class ConsumptionDataHandler
    {
        public static string GetMeterStartDateForBilling(RestSharpHelper restSharpHelper, string json)
        {
            var meterMapResponse = restSharpHelper.Get(APIResourceURLS.GET_ACCEPTED_METER_END_DATE_URL, json);
            if (meterMapResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var meterEndDate = (meterMapResponse.Content);
                return meterEndDate;
            }
            else if (meterMapResponse.StatusCode == HttpStatusCode.NotFound)
            {
                Console.WriteLine($"meter Id {json} not found");
                return null;
            }
            else if (meterMapResponse.StatusCode == HttpStatusCode.BadRequest)
            {
                Console.WriteLine($"meter Id {json} returned a bad request");
                return null;
            }
            return null;

        }
    }
}
