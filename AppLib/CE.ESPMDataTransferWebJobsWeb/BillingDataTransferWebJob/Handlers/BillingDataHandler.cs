﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BillingDataTransferWebJob.RestSharp;
using BillingDataTransferWebJob.Utils;
using CE.ESPMDataModel.ESPMModels;
using Newtonsoft.Json;

namespace BillingDataTransferWebJob.Handlers
{
    public class BillingDataHandler
    {
        public static List<String> GetAllMeterIds(RestSharpHelper restSharpHelper)
        {
            var meterIdResponse = restSharpHelper.Get(APIResourceURLS.GET_ALL_METER_IDS, null);
            if (meterIdResponse.StatusCode != System.Net.HttpStatusCode.OK) return null;
            var meterIds =
                (JsonConvert.DeserializeObject<List<string>>(meterIdResponse.Content));
            return meterIds;
        }

        public static List<MeterBillingData> GetAllMeterBillingdata(RestSharpHelper restSharpHelper, BillDataRequest billdataRequest)
        {
            var meterBillingDataResponse = restSharpHelper.Get(APIResourceURLS.GET_METER_BILLING_DATA, billdataRequest);
            if (meterBillingDataResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {

                var meterBillingData =
                    (JsonConvert.DeserializeObject<List<MeterBillingData>>(meterBillingDataResponse.Content));
                return meterBillingData;
            }
            return null;
        }

        public static Boolean PostBillingDataToESPM(RestSharpHelper restSharpHelper, List<MeterBillingData> meterBillingData)
        {
            var meterBillingDataResponse = restSharpHelper.Post(APIResourceURLS.POST_METER_BILLING_DATA, JsonConvert.SerializeObject(meterBillingData));
            if (meterBillingDataResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return true;

            }
            return false;
        }
    }
}
