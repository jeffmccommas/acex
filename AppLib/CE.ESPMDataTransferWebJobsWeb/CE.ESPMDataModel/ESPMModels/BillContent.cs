﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.ESPMDataModel.ESPMModels
{
    public class BillContent
    {
        public String ClientId { get; set; }

        public BillCustomer Customer { get; set; }
    }
}
