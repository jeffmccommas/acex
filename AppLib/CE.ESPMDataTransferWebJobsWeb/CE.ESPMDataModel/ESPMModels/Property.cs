﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.ESPMDataModel.ESPMModels
{
    public class Property
    {
        //[JsonProperty(PropertyName = "@id")]
        public string id { get; set; }
        //[JsonProperty(PropertyName = "@hint")]
        public string hint { get; set; }
        public string httpMethod { get; set; }
        public string link { get; set; }
        public string linkDescription { get; set; }
        public string name { get; set; }
        public Address address { get; set; }
        public string numberOfBuildings { get; set; }
        public string constructionStatus { get; set; }
        public string primaryFunction { get; set; }
        public string yearBuilt { get; set; }
        public GrossFloorArea grossFloorArea { get; set; }
        public string occupancyPercentage { get; set; }
        public string isFederalProperty { get; set; }
        public string notes { get; set; }
        public string accessLevel { get; set; }
        public Audit audit { get; set; }

    }

    public class Address
    {
        public string address1 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string postalCode { get; set; }
        public string country { get; set; }
    }

    public class GrossFloorArea
    {
        public string units { get; set; }
        public string temporary { get; set; }
        public string @default { get; set; }
        public string value { get; set; }
    }

    public class Audit
    {
        public string createdBy { get; set; }
        public string createdByAccountId { get; set; }
        public string createdDate { get; set; }
        public string lastUpdatedBy { get; set; }
        public string lastUpdatedByAccountId { get; set; }
        public string lastUpdatedDate { get; set; }
    }

}
