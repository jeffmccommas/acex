﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.ESPMDataModel.ESPMModels
{
    public class Metric
    {
        public string Name { get; set; }
        public string DataType { get; set; }
        public string DisplayText { get; set; }
        public string PrefixText { get; set; }
        public string SuffixText { get; set; }
        public bool IncludeInSummary { get; set; }
        public string Summarycalculation { get; set; }
        public string Value { get; set; }
        public string Change { get; set; }
        public string PercentChange { get; set; }
        public string BaseLineValue { get; set; }
        public string CurrentValue { get; set; }
        public string YearEnding { get; set; }
        public Boolean IsESPMMetric { get; set; }
        public String Type{ get; set; }
        public int Count { get; set; }
        public int PropertyReportOrder { get; set; }
        public int AccountReportOrder { get; set; }

    }
}
