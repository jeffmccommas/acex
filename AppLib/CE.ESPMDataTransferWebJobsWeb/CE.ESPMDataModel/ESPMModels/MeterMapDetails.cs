﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.ESPMDataModel.ESPMModels
{
    public class MeterMapDetails
    {
        public int? MapDetailId { get; set; }
        public int? MapId { get; set; }
        public string MapAccountId { get; set; }
        public string CustomerId { get; set; }
        public string MapPremiseId { get; set; }
        public bool Enabled { get; set; } = false;
        public string MeterType { get; set; }
    }
}
