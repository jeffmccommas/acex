﻿using System.Collections.Generic;


namespace CE.ESPMDataModel.ESPMModels
{
    public class BillCustomer
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public List<BillAccount> Accounts { get; set; }
    }
}
