﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.ESPMDataModel.ESPMModels
{
    public class Account
    {
        //[JsonProperty(PropertyName = "@id")]
        public string id { get; set; }
        //[JsonProperty(PropertyName = "@hint")]
        public string hint { get; set; }
        public string httpMethod { get; set; }
        public string link { get; set; }
        public string linkDescription { get; set; }
    }
}
