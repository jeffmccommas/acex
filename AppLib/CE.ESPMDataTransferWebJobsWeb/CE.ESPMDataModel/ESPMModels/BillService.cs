﻿using System.Collections.Generic;

namespace CE.ESPMDataModel.ESPMModels
{
    public class BillService
    {
        public string Id { get; set; }
        public decimal TotalServiceUse { get; set; }
        public decimal CostofUsage { get; set; }
        public decimal AdditionalServiceCost { get; set; }
        public string CommodityKey { get; set; }
        public string UOMKey { get; set; }
        public string BillStartDate { get; set; }
        public string BillEndDate { get; set; }
        public int BillDays { get; set; }
        public int AverageTemperature { get; set; }
        public string RateClass { get; set; }
        public List<BillDetail> CostDetails { get; set; }
    }
}
