﻿using System.Collections.Generic;

namespace CE.ESPMDataModel.ESPMModels
{
    public class BillPremise
    {
        public string Id { get; set; }
        public string Addr1 { get; set; }
        public string Addr2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public List<BillService> Service { get; set; }
    }
}
