﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.ESPMDataModel.ESPMModels
{
    public class PropertyMetrics
    {
        public string PropertyName { get; set; }
        public string PropertyId { get; set; }
        public string PortFolioName { get; set; }
        public string PortFolioId { get; set; }
        public string EnergyCurrentDate { get; set; }

        public string EnergyBaseLineDate { get; set; }

        public string NumberOfBuildings { get; set; }

        public string City { get; set; }

        public string Address { get; set; }

        public string ProperType { get; set; }

        public string PropertyGfa { get; set; }

        public List<Metric> Metrics { get; set; }
    }
}
