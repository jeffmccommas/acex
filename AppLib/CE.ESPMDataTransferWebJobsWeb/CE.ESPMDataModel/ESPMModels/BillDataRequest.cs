﻿using System;
using System.Configuration;
using System.Runtime.InteropServices;
using Microsoft.Win32.SafeHandles;

namespace CE.ESPMDataModel.ESPMModels
{
    public class BillDataRequest : IDisposable
    {
        private int count;
        public string CustomerId { get; set; }
        public string MeterId { get; set; }
        public string AccountId { get; set; }
        public string PremiseId { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Count { get; set; }
        public string MeterType { get; set; }
        bool disposed = false;
        SafeHandle handle = new SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
               
            }
            disposed = true;
        }
        ~BillDataRequest()
        {
            Dispose(false);
        }
    }
}
