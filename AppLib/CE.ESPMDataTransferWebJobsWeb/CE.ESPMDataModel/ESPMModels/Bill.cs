﻿using System.Collections.Generic;

// ReSharper disable once CheckNamespace
namespace CE.ESPMDataModel.ESPMModels
{
    public class Bill
    {
        public decimal TotalAmount { get; set; }
        public decimal AdditionalBillCost { get; set; }
        public string BillDate { get; set; }
        public string BillDueDate { get; set; }
        public string BillFrequency { get; set; }
        public List<BillDetail> CostDetails { get; set; }
        public List<BillPremise> Premises { get; set; }
    }
}
