﻿

// ReSharper disable once CheckNamespace
namespace CE.ESPMDataModel.ESPMModels
{
    public class BillDetail
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
