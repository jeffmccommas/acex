﻿using System.Collections.Generic;


namespace CE.ESPMDataModel.ESPMModels
{
    public class BillAccount
    {
        public string Id { get; set; }

        public List<Bill> Bills { get; set; }

    }
}
