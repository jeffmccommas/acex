﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CE.ESPMDataModel.ESPMModels
{
    public class MeterBillingData
    {
        public string MeterId { get; set; }
        public string Usage { get; set; }
        public string Cost { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int UpdatedBy {get ; set;}
        
    }
}
