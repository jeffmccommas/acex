﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Microsoft.Azure.WebJobs;
using CE.ESPMDataModel.ESPMModels;
using DataCollectionWebJob.Handlers;
using DataCollectionWebJob.RestSharp;
using DataCollectionWebJob.Utils;

namespace ImportDataWebJob
{
    public class Functions
    {
        private static RestSharpHelper restSharpHelper;
        //string authToken = "Jragpl4AqzzDE1i4ty-V2hwSozynTIT3lIX068BukVEzR7jCvuLVphX6wSd_PNFXgcQjTZubytEwVbK3ksZfYMLI8dVPVFhCAPegt4uNDSA";

        public Functions()
        {
            restSharpHelper = new RestSharpHelper();
            //restSharpHelper.authToken = authToken;
        }

        [NoAutomaticTrigger]
        public void UpdateBillingInformation(string userCreds)
        {
            var authUser = UserAuthHandler.GetUserAuthToken(restSharpHelper, userCreds);

            if (authUser.User == null) return;

            restSharpHelper.AuthToken = authUser.User;
            UserAuthHandler.LogTrace(restSharpHelper, "WebJob UpdateBillingInformation - Start", "info");


            if (!BillingHandler.DeleteBillingDataBeforeRun(restSharpHelper)) { UserAuthHandler.LogTrace(restSharpHelper, "UpdateBillingInformation - End - Table cleanup failed", "info"); return; }
            var acceptedAccounts = AccountHandler.GetAcceptedAccounts(restSharpHelper);

            if (acceptedAccounts == null) { UserAuthHandler.LogTrace(restSharpHelper, "WebJob UpdateBillingInformation - End - AcceptedAccounts is null", "info"); return; }

            UserAuthHandler.LogTrace(restSharpHelper, "WebJob UpdateBillingInformation - Accepted Account count = " + acceptedAccounts.Count, "info");
            foreach (var account in acceptedAccounts)
            {
                var acceptedProperties = Propertyhandler.GetAcceptedProperties(restSharpHelper, $"?accountId={account.id}");

                if (acceptedProperties == null) { UserAuthHandler.LogTrace(restSharpHelper, "WebJob UpdateBillingInformation - No properties found for accountId = " + account.id, "info"); continue; }
                foreach (var property in acceptedProperties)
                {
                    var acceptedmeters = MeterHandler.GetAcceptedMeters(restSharpHelper, $"?propertyId={property.id}");

                    if (acceptedmeters == null) { UserAuthHandler.LogTrace(restSharpHelper, "WebJob UpdateBillingInformation - No meters found for property = " + property.id, "info"); continue; }
                    foreach (var meter in acceptedmeters)
                    {
                        var meterMaps = MappingHandler.GetMeterMappings(restSharpHelper, $"?meterId={meter.id}");

                        if (meterMaps == null) { UserAuthHandler.LogTrace(restSharpHelper, "WebJob UpdateBillingInformation - No mapped meters found for meterid = " + meter.id, "info"); continue; }
                        foreach (MeterMapDetails meterMap in meterMaps)
                        {
                            using (var billdataRequest = new BillDataRequest()
                            {
                                AccountId = meterMap.MapAccountId,
                                PremiseId = meterMap.MapPremiseId,
                                CustomerId = meterMap.CustomerId,
                                EndDate = DateTime.Now.ToString("yyyy-MM-dd"),
                                StartDate = DateTime.Now.AddMonths(-12).ToString("yyyy-MM-dd"),
                                MeterId = meter.id,
                                MeterType = meterMap.MeterType,
                                Count = ConfigurationManager.AppSettings[Constants.BILLINGPERIOD]

                                //AccountId = "1218337093",
                                //PremiseId = null,
                                //CustomerId = "993774751",
                                //EndDate = "2015-07-01",
                                //StartDate = "2014-01-01",
                                //StartDate = "2014-01-01",
                                //MeterId = "1111",
                                //Count = ConfigurationManager.AppSettings[Constants.BILLINGPERIOD],
                                //MeterType = "Electric"
                            })
                            {
                                var billCustomer = BillingHandler.GetBillingData(restSharpHelper, billdataRequest);
                                if (billCustomer?.Accounts == null) continue;
                                var meterBillingData =
                                    BillingHandler.CalculateMeterBillingData(billCustomer, billdataRequest);

                                UserAuthHandler.LogTrace(restSharpHelper, $"WebJob UpdateBillingInformation - saving billing data accountId: {billdataRequest.AccountId} premiseId: {billdataRequest.PremiseId}", "debug");
                                BillingHandler.SaveBillingData(restSharpHelper, meterBillingData);
                            }
                        }
                    }
                }
            }
            UserAuthHandler.LogTrace(restSharpHelper, $"WebJob UpdateBillingInformation - End", "info");
        }
    }
}
