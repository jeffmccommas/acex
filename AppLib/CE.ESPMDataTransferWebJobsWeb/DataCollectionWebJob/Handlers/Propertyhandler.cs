﻿using CE.ESPMDataModel.ESPMModels;
using DataCollectionWebJob.RestSharp;
using DataCollectionWebJob.Utils;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace DataCollectionWebJob.Handlers
{
    public class Propertyhandler
    {
        public static List<Property> GetAcceptedProperties(RestSharpHelper restSharpHelper, string json)
        {
            var propertyResponse = restSharpHelper.Get(APIResourceURLS.GET_ACCEPTED_PROPERTY_LIST_URL, json);
            if (propertyResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
 
                var result1 = JObject.Parse(propertyResponse.Content);
                var links = result1["response"]["links"].HasValues == true ? (JObject)result1["response"]["links"] : null;
                if (links != null)
                {
                    var property = new List<Property>();
                    var token = JToken.Parse(JsonConvert.SerializeObject(links["link"]));
                    if (token is JArray)
                    {
                        foreach (var acceptedpropertylink in links["link"])
                        {
                            var acceptedProperty = acceptedpropertylink.ToObject<Property>();
                            property.Add(acceptedProperty);
                        }
                    }
                    else if (token is JObject)
                    {
                        property.Add(result1["response"]["links"]["link"].ToObject<Property>());
                    }

                    return property;
                }
            }
           return null;
           
        }
    }
}
