﻿using System.Collections.Generic;
using CE.ESPMDataModel.ESPMModels;
using DataCollectionWebJob.RestSharp;
using Newtonsoft.Json;
using DataCollectionWebJob.Utils;
using Newtonsoft.Json.Linq;


namespace DataCollectionWebJob.Handlers
{
    public class AccountHandler
    {
        public static List<Account>  GetAcceptedAccounts(RestSharpHelper restSharpHelper)
        {
            var accountsResponse = restSharpHelper.Get(APIResourceURLS.GET_ACCEPTED_ACCOUNT_LIST_URL, null);
            if (accountsResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {

                var result1 = JObject.Parse(accountsResponse.Content);
                var links = result1["response"]["links"].HasValues ? (JObject)result1["response"]["links"] : null;
                if (links != null)
                {
                    var accounts = new List<Account>();
                    var token = JToken.Parse(JsonConvert.SerializeObject(links["link"]));
                    if (token is JArray)
                    {
                        foreach (var acceptedaccountlink in links["link"])
                        {
                            var acceptedAccount = acceptedaccountlink.ToObject<Account>();
                            accounts.Add(acceptedAccount);
                        }
                    }
                    else if (token is JObject)
                    {
                        accounts.Add(result1["response"]["links"]["link"].ToObject<Account>());
                    }

                    return accounts;
                }
            }
            return null;
        }
    }
}
