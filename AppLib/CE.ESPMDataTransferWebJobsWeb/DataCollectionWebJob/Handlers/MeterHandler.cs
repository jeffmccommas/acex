﻿using CE.ESPMDataModel.ESPMModels;
using DataCollectionWebJob.RestSharp;
using DataCollectionWebJob.Utils;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace DataCollectionWebJob.Handlers
{
    public class MeterHandler
    {
        public static List<Meter> GetAcceptedMeters(RestSharpHelper restSharpHelper, string json)
        {
            var meterResponse = restSharpHelper.Get(APIResourceURLS.GET_ACCEPTED_METER_LIST_URL, json);
            if (meterResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result1 = JObject.Parse(meterResponse.Content);
                var links = result1["response"]["links"].HasValues == true ? (JObject)result1["response"]["links"] : null;
                if (links != null)
                {
                    var meters = new List<Meter>();
                    var token = JToken.Parse(JsonConvert.SerializeObject(links["link"]));
                    if (token is JArray)
                    {
                        foreach (var acceptedmeterlink in links["link"])
                        {
                            var acceptedMeter = acceptedmeterlink.ToObject<Meter>();
                            meters.Add(acceptedMeter);
                        }
                    }
                    else if (token is JObject)
                    {
                        meters.Add(result1["response"]["links"]["link"].ToObject<Meter>());
                    }

                    return meters;
                }
            }
            return null;
          
        }
    }
}
