﻿using CE.ESPMDataModel.ESPMModels;
using DataCollectionWebJob.RestSharp;
using DataCollectionWebJob.Utils;
using Newtonsoft.Json;
using System;

using System.Collections.Generic;


namespace DataCollectionWebJob.Handlers
{
    public class MappingHandler
    {
        public static List<MeterMapDetails> GetMeterMappings(RestSharpHelper restSharpHelper, string json)
        {
            var meterMapResponse = restSharpHelper.Get(APIResourceURLS.GET_ACCEPTED_METER_MAPS_URL, json);
            if (meterMapResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var meterMaps = JsonConvert.DeserializeObject<List<MeterMapDetails>>(meterMapResponse.Content);
                return meterMaps;
            }
            return null;

        }

        
    }
}
