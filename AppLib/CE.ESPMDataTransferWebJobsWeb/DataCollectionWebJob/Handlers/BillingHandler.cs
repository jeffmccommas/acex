﻿using CE.ESPMDataModel.ESPMModels;
using DataCollectionWebJob.RestSharp;
using DataCollectionWebJob.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using Newtonsoft.Json.Linq;

namespace DataCollectionWebJob.Handlers
{
    public class BillingHandler
    {
        public static BillCustomer GetBillingData(RestSharpHelper restSharpHelper, BillDataRequest billdataRequest)
        {

            var billingresponse = restSharpHelper.Get(APIResourceURLS.GET_BILLING_INFO_URL, billdataRequest);
            if (billingresponse.StatusCode == System.Net.HttpStatusCode.OK)
            {

                var result = JObject.Parse(billingresponse.Content);
                var billCustomer = (JsonConvert.DeserializeObject<BillContent>(result["content"].ToString())).Customer;
                return billCustomer;
            }
            return null;
        }

        public static Boolean DeleteBillingDataBeforeRun(RestSharpHelper restSharpHelper)
        {

            var billingresponse = restSharpHelper.Post(APIResourceURLS.DELETE_BILLING_INFO_BEFORE_RUN_URL,null);
            if (billingresponse.StatusCode == System.Net.HttpStatusCode.OK)
            {

                return true;
            }
            return false;
        }

        public static BillCustomer SaveBillingData(RestSharpHelper restSharpHelper, List<MeterBillingData> meterBillingData)
        {

            var billingresponse = restSharpHelper.Post(APIResourceURLS.SAVE_BILLING_INFO_URL, JsonConvert.SerializeObject(meterBillingData));
            if (billingresponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                Console.WriteLine($"Save Billing data{DateTime.Now}");
            }
            return null;
        }

        public static List<MeterBillingData> CalculateMeterBillingData(BillCustomer billCustomer, BillDataRequest billdataRequest)
        {
            if (billCustomer.Accounts[0].Bills != null)
            {
                var meterBillingDataCumulative = new List<MeterBillingData>();
                var billGroup = billCustomer.Accounts[0].Bills.GroupBy(x => x.BillDate).Select(grp => grp.ToList()).ToList();
                foreach(List<Bill> bills in billGroup)
                {
                    var service = GetAllBillServices(bills, billdataRequest.PremiseId);
                    var meterBilling = GetMeterBillingData(service, billdataRequest.MeterId, billdataRequest.MeterType);
                    if (meterBilling != null)
                    meterBillingDataCumulative.AddRange(meterBilling);
                }
            
                return meterBillingDataCumulative;
            }
            return null;
        }

       
        private static List<BillService> GetAllBillServices(List<Bill> bill, string billPremiseId)
        {
            List<BillPremise> premises = new List<BillPremise>();
            List<BillService> billService = new List<BillService>();

            bill.ForEach(x => { premises.AddRange(x.Premises);});
            if (billPremiseId?.Length >0 )
            {

                premises.Where(x => x.Id == billPremiseId).ToList().ForEach(x => { billService.AddRange(x.Service); });
                return billService;


            }
            premises.ForEach(x => { billService.AddRange(x.Service); });
            return billService;
         
        }

        private static List<MeterBillingData> GetMeterBillingData(List<BillService> billService, string meterId, string meterType)
        {
            var meterBillingData = new List<MeterBillingData>();
            //var commodity = ConfigurationManager.AppSettings[Constants.COMMODITY].Split(';');
            if (billService == null) return meterBillingData;
            var billServiceCommodity = billService.FindAll(f => CultureInfo.CurrentCulture.CompareInfo.IndexOf(meterType, f.CommodityKey, CompareOptions.IgnoreCase) >= 0);
            if (billServiceCommodity.Count > 0)
            {
                var totalCost = billServiceCommodity.Sum(x => x.CostofUsage);
                var totalUsage = billServiceCommodity.Sum(x => x.TotalServiceUse);
                var startDate = billServiceCommodity.Min(x => x.BillStartDate);
                var endDate = billServiceCommodity.Max(x => x.BillEndDate);

                meterBillingData.Add(new MeterBillingData()
                {
                    MeterId = meterId,
                    Cost = totalCost.ToString(CultureInfo.InvariantCulture),
                    Usage = totalUsage.ToString(CultureInfo.InvariantCulture),
                    EndDate = Convert.ToDateTime(endDate),
                    StartDate = Convert.ToDateTime(startDate)
                });

                return meterBillingData;
            }
            return null;

        }
    }
}
