﻿
namespace DataCollectionWebJob.Utils
{
    public class Constants
    {
        public string USERNAME = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes("UILWEBJOBUSER"));
        public string PASSWORD = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes("qwerty!1"));
        public const string AUTHTOKEN = "WebToken";
        public const string APIBASEURL = "APIBaseURL";
        public const string BILLINGPERIOD = "BillingPeriod";
        public const string USERCREDS = "WebjobCreds";
        public const string COMMODITY = "Commodity";
    }

    public class APIResourceURLS
    {
        public const string GET_ACCEPTED_ACCOUNT_LIST_URL = "/api/EnergyStar/AcceptedAccountsList";
        public const string GET_ACCEPTED_PROPERTY_LIST_URL = "/api/EnergyStar/AcceptedPropertyForAccount";
        public const string GET_ACCEPTED_METER_LIST_URL = "/api/EnergyStar/AcceptedMetersList";
        public const string GET_ACCEPTED_METER_MAPS_URL = "/api/PortalsApiMap/GetMappedAccountsForMeters";
        public const string GET_ACCEPTED_METER_END_DATE_URL = "/api/EnergyStar/MeterEndDate";
        public const string GET_BILLING_INFO_URL = "/api/PortalsApiMap/GetBillingData";
        public const string GET_USER_AUTH_TOKEN = "/api/PortalsApi/UserAuth";
        public const string POST_TRACE_LOG = "/api/PortalsApi/TraceLog";
        public const string SAVE_BILLING_INFO_URL = "/api/PortalsApiMap/SaveBillingData";
        public const string DELETE_BILLING_INFO_BEFORE_RUN_URL = "/api/PortalsApiMap/DeleteBillingDataWebJob";
    }
}
