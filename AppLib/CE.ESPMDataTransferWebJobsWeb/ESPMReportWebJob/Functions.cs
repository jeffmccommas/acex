﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using CE.ESPMDataModel.ESPMModels;
using ESPMReportWebJob.RestSharp;
using Microsoft.Azure.WebJobs;
using ESPMReportWebJob.Handlers;

namespace ESPMReportWebJob
{
    public class Functions
    {
        private static RestSharpHelper restSharpHelper;
        //string authToken = "Jragpl4AqzzDE1i4ty-V2hwSozynTIT3lIX068BukVEzR7jCvuLVphX6wSd_PNFXgcQjTZubytEwVbK3ksZfYMLI8dVPVFhCAPegt4uNDSA";

        public Functions()
        {
            restSharpHelper = new RestSharpHelper();
            //restSharpHelper.authToken = authToken;
        }

        [NoAutomaticTrigger]
        public void GenerateBillingReport(string userCreds)
        {
            var authUser = UserAuthhandler.GetUserAuthToken(restSharpHelper, userCreds);

            if (authUser.User == null) return;

            restSharpHelper.AuthToken = authUser.User;
            UserAuthhandler.LogTrace(restSharpHelper, "WebJob Get Metrics Report - Start", "info");

            var acceptedAccounts = AcountHandler.GetAcceptedAccounts(restSharpHelper);

            var configuration = Configurationhandler.GetConfigurationMetrics(restSharpHelper);

            var metricsList = string.Join(",",configuration.Where(x => x.IsESPMMetric).Select(x => x.Name));

            //var headertext = Configurationhandler.GetConfigurationHeadertext(restSharpHelper);

            if (acceptedAccounts == null)
            {
                UserAuthhandler.LogTrace(restSharpHelper, "WebJob  Get Metrics Report - End - AcceptedAccounts is null",
                    "info");
                return;
            }

            UserAuthhandler.LogTrace(restSharpHelper,
                "WebJob Get Metrics Report - Accepted Account count = " + acceptedAccounts.Count, "info");
            var propertyMetrics = new List<PropertyMetrics>();
            foreach (var account in acceptedAccounts)
            {
                var acceptedProperties =
                    PropertyHandler.GetAcceptedProperties(restSharpHelper, $"?accountId={account.id}");

                if (acceptedProperties == null) { UserAuthhandler.LogTrace(restSharpHelper, "WebJob Get Metrics Report - No properties found for accountId = " + account.id, "info"); continue; }
                foreach (var property in acceptedProperties)
                {
                    var propertyMetric = new PropertyMetrics();
                    propertyMetric.PropertyId = property.id;
                    propertyMetric.PropertyName = property.hint;
                    propertyMetric.PortFolioName = account.hint;
                    propertyMetric.PortFolioId = account.id;

                    var propertyDetails = PropertyHandler.GetPropertyDetails(restSharpHelper,$"?propertyId={property.id}");
                    if (propertyDetails == null) continue;
                    propertyMetric.City = propertyDetails.address.city;
                    propertyMetric.Address = propertyDetails.address.address1;
                    propertyMetric.NumberOfBuildings = propertyDetails.numberOfBuildings;
                    propertyMetric.ProperType = propertyDetails.primaryFunction;
                    propertyMetric.PropertyGfa = propertyDetails.grossFloorArea.value;

                    var dates = MetrisHandler.GetPropertyBaseLine_CurrentDates(restSharpHelper,
                        $"?propertyId={property.id}&month={DateTime.Now.Month}&year={DateTime.Now.Year}&metrics=energyBaselineDate,energyCurrentDate");
                    if (dates.Count == 0) continue;

                    propertyMetric.EnergyBaseLineDate = dates.First().ToString("yyyy-MMMM");
                    propertyMetric.EnergyCurrentDate = dates.Last().ToString("yyyy-MMMM");
                    var baseLineMEtrics = MetrisHandler.GetPropertyMetrics(restSharpHelper,
                        $"?propertyId={property.id}&month={dates.First().Month}&year={dates.First().Year}", metricsList);

                    var currentMetrics = MetrisHandler.GetPropertyMetrics(restSharpHelper,
                        $"?propertyId={property.id}&month={dates.Last().Month}&year={dates.Last().Year}", metricsList);

                    baseLineMEtrics.ForEach(x =>
                    {
                        var metric = configuration.FindLast(config => config.Name == x.Name);
                        x.IsESPMMetric = metric.IsESPMMetric;
                        x.PrefixText = metric.PrefixText;
                        x.DataType = metric.DataType;
                        x.DisplayText = metric.DisplayText;
                        x.IncludeInSummary = metric.IncludeInSummary;
                        x.SuffixText = metric.SuffixText;
                        x.Summarycalculation= metric.Summarycalculation;
                        x.Type = "Baseline";
                        x.YearEnding = new DateTime(dates.First().Year, 12, 31).ToString("MM-dd-yyyy");
                        } );

                    currentMetrics.ForEach(x =>
                    {
                        var metric = configuration.FindLast(config => config.Name == x.Name);
                        x.IsESPMMetric = metric.IsESPMMetric;
                        x.PrefixText = metric.PrefixText;
                        x.DataType = metric.DataType;
                        x.DisplayText = metric.DisplayText;
                        x.IncludeInSummary = metric.IncludeInSummary;
                        x.SuffixText = metric.SuffixText;
                        x.Summarycalculation = metric.Summarycalculation;
                        x.Type = "Current";
                        x.YearEnding = new DateTime(dates.Last().Year, 12, 31).ToString("MM-dd-yyyy");
                    });

                    configuration.FindAll(x => x.IsESPMMetric == false).ForEach(m =>
                    {
                        baseLineMEtrics.Add(new Metric()
                        {
                            IsESPMMetric = false,
                            Name = m.Name,
                            DisplayText = m.DisplayText,
                            DataType = m.DataType,
                            IncludeInSummary = m.IncludeInSummary,
                            Summarycalculation = m.Summarycalculation,
                            PrefixText = m.PrefixText,
                            SuffixText = m.SuffixText,
                            Type = "Baseline",
                            YearEnding = new DateTime(dates.First().Year, 12, 31).ToString("MM-dd-yyyy")
                    });

                        currentMetrics.Add(new Metric()
                        {
                            IsESPMMetric = false,
                            Name = m.Name,
                            DisplayText = m.DisplayText,
                            DataType = m.DataType,
                            IncludeInSummary = m.IncludeInSummary,
                            Summarycalculation = m.Summarycalculation,
                            PrefixText = m.PrefixText,
                            SuffixText = m.SuffixText,
                            Type = "Current",
                            YearEnding = new DateTime(dates.Last().Year, 12, 31).ToString("MM-dd-yyyy")
                        });
                    });

                    propertyMetric.Metrics = MetrisHandler.CalculateMetrics(baseLineMEtrics, currentMetrics);

                    propertyMetrics.Add(propertyMetric);
                    //FileHandler.GeneratePropertyFile(propertyMetrics);
                    //FileHandler.GenerateAccountSummary(propertyMetrics);
                    //break;

                }
                
            }
            FileHandler.GeneratePropertyFile(propertyMetrics);
            FileHandler.GenerateAccountSummary(propertyMetrics);
        }
    }
}
