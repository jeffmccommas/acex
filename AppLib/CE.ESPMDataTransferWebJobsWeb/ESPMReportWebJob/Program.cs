﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using System.Configuration;
using ESPMReportWebJob.Utils;

namespace ESPMReportWebJob
{
    // To learn more about Microsoft Azure WebJobs SDK, please see https://go.microsoft.com/fwlink/?LinkID=320976
    class Program
    {
        // Please set the following connection strings in app.config for this WebJob to run:
        // AzureWebJobsDashboard and AzureWebJobsStorage
        static void Main()
        {
            var userCreds = ConfigurationManager.AppSettings[Constants.USERCREDS].Split(',');
            Console.WriteLine($"Starting WebJob to Get Property Metrics Report {DateTime.Now}");
            foreach (var userInfo in userCreds)
            {
                Console.WriteLine($"Starting WebJob to Get Property Metrics Report at {DateTime.Now} for userid: {Encoding.UTF8.GetString(Convert.FromBase64String(userInfo.Split(':')[0]))}");
                new Functions().GenerateBillingReport(userInfo);
            }
        }
    }
}
