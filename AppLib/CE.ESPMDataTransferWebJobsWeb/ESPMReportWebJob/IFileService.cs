﻿using System.Collections.Generic;
using System.IO;

namespace ESPMReportWebJob
{
    public interface IFileService
    {
        string StorageName { get; set; }
        string StorageKey { get; set; }
        string ContainerName { get; set; }
        bool Exists(string path);
        void Delete(string path);
        void WriteAllText(string path, string text, bool overwrite);

        List<FileInfo> ListFilesInContainer();

        bool Save(string file, string saveas);
    }
}
