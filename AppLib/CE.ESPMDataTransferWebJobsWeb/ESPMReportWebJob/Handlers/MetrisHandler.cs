﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using CE.ESPMDataModel.ESPMModels;
using ESPMReportWebJob.RestSharp;
using ESPMReportWebJob.Utils;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ESPMReportWebJob.Handlers
{
    public class MetrisHandler
    {
        public static List<DateTime> GetPropertyBaseLine_CurrentDates(RestSharpHelper restSharpHelper, string json)
        {
            var datesResponse = restSharpHelper.Get(APIResourceURLS.GET_PROPERTY_METRICS,json);
            if (datesResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {

                var result1 = JObject.Parse(datesResponse.Content);
                var datesResponseObject = result1["propertyMetrics"]["metric"].HasValues == true ? (JArray)result1["propertyMetrics"]["metric"] : null;
                if (datesResponseObject != null)
                {
                    var dates = new List<DateTime>();
                    var token = JToken.Parse(JsonConvert.SerializeObject(result1["propertyMetrics"]["metric"]));
                    if (token is JArray)
                    {
                        foreach (var metric in result1["propertyMetrics"]["metric"])
                        {
                            DateTime result;
                            if (DateTime.TryParse(metric["value"].ToString(), out result))
                            dates.Add(DateTime.ParseExact(metric["value"].ToString(), "yyyy-MM-dd",
                                System.Globalization.CultureInfo.InvariantCulture));
                        }
                    }
                    else if (token is JObject)
                    {
                        DateTime result;
                        if (DateTime.TryParse(result1["propertyMetrics"]["metric"][0]["value"].ToString(), out result))
                            dates.Add(DateTime.ParseExact(result1["propertyMetrics"]["metric"][0]["value"].ToString(), "yyyy-MM-dd",
                            System.Globalization.CultureInfo.InvariantCulture));
                    }

                    return dates;
                }
            }
            return null;
        }

        public static List<Metric> GetPropertyMetrics(RestSharpHelper restSharpHelper, string json, string configuration)
        {
            List<string> strConfigurations = configuration.Split(',').ToList();
            var metrics = new List<Metric>();
            for (int i = 0; i < strConfigurations.Count; i = i + 10)
            {
                var metricList = String.Join(",",strConfigurations.Skip(i).Take(10));
                var queryParams = json + $"&metrics={metricList}";
                var metricsResponse = restSharpHelper.Get(APIResourceURLS.GET_PROPERTY_METRICS, queryParams);
                if (metricsResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    
                    var result1 = JObject.Parse(metricsResponse.Content);
                    var datesResponseObject = result1["propertyMetrics"]["metric"].HasValues == true
                        ? (JArray) result1["propertyMetrics"]["metric"]
                        : null;
                    if (datesResponseObject != null)
                    {
                        var token = JToken.Parse(JsonConvert.SerializeObject(result1["propertyMetrics"]["metric"]));

                        if (token is JArray)
                        {
                            List<Metric> metric = new List<Metric>();
                            foreach (var obj in token)
                            {
                                if (obj["value"] is JObject)
                                {

                                    metrics.Add(new Metric() {Name = obj["name"].ToString(), Value = ""});
                                }
                                else
                                {
                                    metrics.Add(JsonConvert.DeserializeObject<Metric>(obj.ToString()));

                                }
                            }
                            


                        }
                        else if (token is JObject)
                        {
                            return JsonConvert.DeserializeObject<List<Metric>>(result1["propertyMetrics"]["metric"][0]
                                .ToString());
                        }

                    }
                }
            }
            return metrics;
            
        }
        private string GetAggreGateMetricValue(Metric aggregateMetrics, List<Metric> metrics)
        {
            var listOfMetrics = aggregateMetrics.Name;
            var aggregatemetricsList = metrics.Where(u => aggregateMetrics.Name.Split(':').Contains(u.Name) && u.IsESPMMetric).ToList();
            float value = 0;
            switch (aggregateMetrics.Summarycalculation)
            {
                case Constants.CALCULATION_TEXT_SUM:
                    aggregatemetricsList.ForEach(x =>
                    {
                        if (!string.IsNullOrEmpty(x.Value))
                        {
                            value = value + float.Parse(x.Value);
                        }
                    });
                    break;

            }
            return value.ToString();
        }
        public static List<Metric> CalculateMetrics(List<Metric> baseLineMetrics, List<Metric> currentMetrics)
        {
            var metrics = new List<Metric>();
            MetrisHandler metricHandler = new MetrisHandler();
            baseLineMetrics.FindAll(x => x.IsESPMMetric == false).ForEach(
                xm =>
                {
                    xm.Value = metricHandler.GetAggreGateMetricValue(xm, baseLineMetrics);
                }
                );

            currentMetrics.FindAll(x => x.IsESPMMetric == false).ForEach(
                xm =>
                {
                    xm.Value = metricHandler.GetAggreGateMetricValue(xm, currentMetrics);
                }
            );

            metrics.AddRange(baseLineMetrics);
            metrics.AddRange(currentMetrics);
            using (var e1 = baseLineMetrics.GetEnumerator())
            using (var e2 = currentMetrics.GetEnumerator())
            {
                while (e1.MoveNext() && e2.MoveNext())
                {
                    var metric = new Metric();
                    var baselinemetric = e1.Current;
                    var currentmetric = e2.Current;
                    metric.Name = baselinemetric.Name;
                    metric.IncludeInSummary = baselinemetric.IncludeInSummary;
                    metric.Summarycalculation = baselinemetric.Summarycalculation;
                    metric.YearEnding = "Change";
                    metric.Type = "Change";
                    if ((baselinemetric?.Name == currentmetric?.Name) && baselinemetric?.Value != null && currentmetric?.Value != null)
                    {
                        float baselineValie,currentValue;
                        if (float.TryParse(baselinemetric.Value,out baselineValie) && float.TryParse(currentmetric.Value, out currentValue))
                        {
                            metric.Change = (float.Parse(currentmetric.Value) - float.Parse(baselinemetric.Value)).ToString("F2");
                            if (baselineValie != 0)
                                metric.PercentChange =
                                    ((float.Parse(metric.Change) / float.Parse(baselinemetric.Value)) * 100)
                                    .ToString("F1") + " %";
                            else
                                metric.PercentChange = "N/A";

                        }
                    }

                    metrics.Add(metric);
                }
               
            }
            

            return metrics;
        }
    }
}
