﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CE.ESPMDataModel.ESPMModels;
using ESPMReportWebJob.RestSharp;
using ESPMReportWebJob.Utils;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ESPMReportWebJob.Handlers
{
    public class PropertyHandler
    {
        public static List<Property> GetAcceptedProperties(RestSharpHelper restSharpHelper, string json)
        {
            var propertyResponse = restSharpHelper.Get(APIResourceURLS.GET_ACCEPTED_PROPERTY_LIST_URL, json);
            if (propertyResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {

                var result1 = JObject.Parse(propertyResponse.Content);
                var links = result1["response"]["links"].HasValues == true ? (JObject)result1["response"]["links"] : null;
                if (links != null)
                {
                    var property = new List<Property>();
                    var token = JToken.Parse(JsonConvert.SerializeObject(links["link"]));
                    if (token is JArray)
                    {
                        foreach (var acceptedpropertylink in links["link"])
                        {
                            var acceptedProperty = acceptedpropertylink.ToObject<Property>();
                            property.Add(acceptedProperty);
                        }
                    }
                    else if (token is JObject)
                    {
                        property.Add(result1["response"]["links"]["link"].ToObject<Property>());
                    }

                    return property;
                }
            }
            return null;

        }

        public static Property GetPropertyDetails(RestSharpHelper restSharpHelper, string json)
        {
            var propertyResponse = restSharpHelper.Get(APIResourceURLS.GET_ACCEPTED_PROPERTY_DETAILS_URL, json);
            if (propertyResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var result1 = JObject.Parse(propertyResponse.Content);
                var datesResponseObject = result1["property"].HasValues == true ? (JObject)result1["property"] : null;
                if(datesResponseObject != null)
                return result1["property"].ToObject<Property>();

            }
            return null;

        }
    }
}
