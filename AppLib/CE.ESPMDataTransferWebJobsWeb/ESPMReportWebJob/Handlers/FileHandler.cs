﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Text;
using System.Threading.Tasks;
using CE.ESPMDataModel.ESPMModels;
using ESPMReportWebJob.Utils;

namespace ESPMReportWebJob.Handlers
{
    public class FileHandler
    {
        private static string _filepathpropertyreport = ConfigurationManager.AppSettings[Constants.FILEPATH_PROPERTY_REPORT];
        private static string _filepathaccountreport = ConfigurationManager.AppSettings[Constants.FILEPATH_ACCOUNT_REPORT];
        private static string _delimiter = ConfigurationManager.AppSettings[Constants.DELIMITER];

        public static void GeneratePropertyFile(List<PropertyMetrics> propertyMetrics)
        {
            FileHandler fileHandler = new FileHandler();

            StringBuilder sb = new StringBuilder();
            var metricsheader = string.Join(",", propertyMetrics[0].Metrics.GroupBy(metricr => metricr.YearEnding).First().Select(x => $@"""{x.DisplayText}"""));
            sb.AppendLine(
                $"Portfolio Name, Property Id, Property Name, Year Ending, City, Address, Property GFA - Self Reported (sqft), Property Type - EPA Calculated,{metricsheader}");
            foreach (PropertyMetrics propertymetric in propertyMetrics)
            {
                propertymetric.Metrics.GroupBy(metricr => metricr.YearEnding).ToList().ForEach(r =>
                {

                    StringBuilder metrics = new StringBuilder();
                    var propertydetails = 
                        $"{propertymetric.PortFolioName},{propertymetric.PropertyId},{propertymetric.PropertyName},{r.First().YearEnding}," +
                        $"{propertymetric.City},{propertymetric.Address},{propertymetric.PropertyGfa},{propertymetric.ProperType},";
                    foreach (Metric m in r)
                    {

                        if (r.First().YearEnding != "Change")
                        {
                           
                            if (!string.IsNullOrEmpty(m.Value))
                            {
                                switch(m.DataType)
                                {
                                    case Constants.DATATYPE_NUMERIC: metrics.Append($@"""{m.PrefixText}{float.Parse(m.Value):n0}{m.SuffixText}"",");break;
                                    case Constants.DATATYPE_CURRENCY: metrics.Append($@"""{m.PrefixText}{float.Parse(m.Value):C}{m.SuffixText}"","); break;
                                    case Constants.DATATYPE_TEXT: metrics.Append($@"""{m.PrefixText}{m.Value}{m.SuffixText}"","); break;
                                    default: metrics.Append($@"""{m.PrefixText}{m.Value}{m.SuffixText}"","); break;
                                }
                                
                            }
                            else
                            {
                                metrics.Append($"{m.Value},");
                            }
                        }
                        else
                        {
                            metrics.Append($"{m.PercentChange},");
                        }
                    }
                    sb.AppendLine($"{propertydetails}{metrics.ToString()}");
                });
            }
            _filepathpropertyreport = _filepathpropertyreport + DateTime.Now.ToFileTimeUtc() + ".csv";
            File.WriteAllText(_filepathpropertyreport, sb.ToString());
        }

        public static void GenerateAccountSummary(List<PropertyMetrics> propertymetrics)
        {
            FileHandler fileHandler = new FileHandler();
            StringBuilder sb = new StringBuilder();
            var metricsheader = string.Join(",", propertymetrics[0].Metrics.GroupBy(metricr => metricr.YearEnding).First().Where(x => x.IncludeInSummary).Select(x => $@"""{x.DisplayText}"""));
            sb.AppendLine(
                $"Portfolio Name,Year Ending,Property GFA - Self Reported (sqft),{metricsheader}");
            propertymetrics.GroupBy(x => x.PortFolioId).ToList().ForEach(propertyMetric =>
            {
                var portfolioId = propertyMetric.First().PortFolioName;
                var propertyList = propertyMetric.ToList();
                var metricsAggregrateList = new List<Metric>();
                var metricsSummaryList = new List<Metric>();
                var metricSummary = new Metric();
                float propertysize = 0;
                foreach (PropertyMetrics propertymetric in propertyList)
                {
                    propertysize += float.Parse(propertymetric.PropertyGfa);
                    propertymetric.Metrics.GroupBy(metricr => metricr.Type).ToList().ForEach(r =>
                    {
                        if (r.First().Type != "Change")
                        {
                            metricsSummaryList.AddRange(r.ToList());
                        }
                       
                    }); 
                }
                var aggregateMetrics = new List<Metric>();
                foreach (var grouping in metricsSummaryList.GroupBy(list => list.Type).ToList())
                {
                    var metricTypeGroup = grouping.ToList();
                    metricsAggregrateList.AddRange(fileHandler.GetMetricAggregate(metricTypeGroup));

                }
                var aggreGrateMetricsBaseline = metricsAggregrateList.Where(x => x.Type == "Baseline").ToList();
                var aggreGrateMetricsCurrent = metricsAggregrateList.Where(x => x.Type == "Current").ToList();
                var aggregateChangeMetric =
                    fileHandler.CalculateChangeMetricsForAggregates(aggreGrateMetricsBaseline,
                        aggreGrateMetricsCurrent);
                var aggregatelists = aggreGrateMetricsBaseline.Concat(aggreGrateMetricsCurrent)
                    .Concat(aggregateChangeMetric);

                foreach (var grouping in aggregatelists.GroupBy(list => list.Type).ToList())
                {
                    sb.AppendLine(
                        $"{portfolioId},{grouping.First().Type},{propertysize},{fileHandler.MetricSummary(grouping.ToList())}");
                }

            });

            _filepathaccountreport = _filepathaccountreport + DateTime.Now.ToFileTimeUtc() + ".csv";
            File.WriteAllText(_filepathaccountreport, sb.ToString());

        }


        private List<Metric> CalculateChangeMetricsForAggregates(List<Metric> aggrgateBaseline,
            List<Metric> aggregateCurrent)
        {
            var aggregateChangeMetrics = new List<Metric>();
            using (var e1 = aggrgateBaseline.GetEnumerator())
            using (var e2 = aggregateCurrent.GetEnumerator())
            {
                while (e1.MoveNext() && e2.MoveNext())
                {
                    var metric = new Metric();
                    var baselinemetric = e1.Current;
                    var currentmetric = e2.Current;
                    metric.Name = baselinemetric.Name;
                    metric.IncludeInSummary = baselinemetric.IncludeInSummary;
                    metric.Summarycalculation = baselinemetric.Summarycalculation;
                    metric.YearEnding = "Change";
                    metric.Type = "Change";
                    if ((baselinemetric?.Name == currentmetric?.Name) && baselinemetric?.Value != null && currentmetric?.Value != null)
                    {
                        float baselineValie, currentValue;
                        switch (baselinemetric.Summarycalculation)
                        {
                            case Constants.CALCULATION_TEXT_SUM:
                                if (float.TryParse(baselinemetric.Value, out baselineValie) && float.TryParse(currentmetric.Value, out currentValue))
                                {
                                    metric.Change = (float.Parse(currentmetric.Value) - float.Parse(baselinemetric.Value)).ToString("F2");
                                    if (baselineValie != 0)
                                        metric.PercentChange =
                                            ((float.Parse(metric.Change) / float.Parse(baselinemetric.Value)) * 100)
                                            .ToString("F1") + " %";
                                    else
                                        metric.PercentChange = "N/A";
                                  

                                }
                                break;
                            case Constants.CALCULATION_TEXT_COUNT:
                                metric.Change =(currentmetric.Count - baselinemetric.Count).ToString();
                                metric.PercentChange = (currentmetric.Count - baselinemetric.Count).ToString();
                                break;

                        }
                        
                    }
                    aggregateChangeMetrics.Add(metric);
                }

            }
            return aggregateChangeMetrics;

        }
        private List<Metric> GetMetricAggregate(List<Metric> metric)
        {
            float value = 0;
            var aggregateMetrics = new List<Metric>();
            metric.GroupBy(x => x.Name).ToList().ForEach(metricGroup =>
            {
                if (metricGroup.First().IncludeInSummary && metricGroup.First() != null)
                {
                    var first = metricGroup.First();
                    var summaryCalculation = first.Summarycalculation;
                    var type = first.Type;
                    var summaryMetricForName = new Metric();
                    var dataType = first.DataType;
                    summaryMetricForName.Type = type;
                    summaryMetricForName.Summarycalculation = summaryCalculation;
                    summaryMetricForName.DataType = dataType;
                    summaryMetricForName.Name = first.Name;
                    summaryMetricForName.PrefixText = first.PrefixText;
                    summaryMetricForName.SuffixText = first.SuffixText;
                    switch (summaryCalculation)
                    {
                        case Constants.CALCULATION_TEXT_SUM:
                            value = 0;
                            foreach (var metric1 in metricGroup)
                            {

                                if (!string.IsNullOrEmpty(metric1.Value))
                                {
                                    value = value + float.Parse(metric1.Value);
                                }
                            }
                            break;
                        case Constants.CALCULATION_TEXT_COUNT:
                            value = 0;

                            foreach (var metric1 in metricGroup)
                            {
                                if (!string.IsNullOrEmpty(metric1.Value))
                                {
                                    value++;
                                }
                            }
                            summaryMetricForName.Count = int.Parse(value.ToString(CultureInfo.InvariantCulture));
                            break;
                    }
                    summaryMetricForName.Value = value.ToString();
                    //if (Math.Abs(value) > 0)
                    //{
                    //    switch (metricGroup.First().DataType)
                    //    {
                    //        case Constants.DATATYPE_NUMERIC:
                    //            summaryMetricForName.Value = string.Format(
                    //                $@"""{metricGroup.First().PrefixText}{float.Parse(value.ToString()):n0}{
                    //                        metricGroup.First().SuffixText
                    //                    }"",");
                    //            break;
                    //        case Constants.DATATYPE_CURRENCY:
                    //            summaryMetricForName.Value = string.Format(
                    //                $@"""{metricGroup.First().PrefixText}{float.Parse(value.ToString()):C}{
                    //                        metricGroup.First().SuffixText
                    //                    }"",");
                    //            break;
                    //        case Constants.DATATYPE_TEXT:
                    //            summaryMetricForName.Value = string.Format(
                    //                $@"""{metricGroup.First().PrefixText}{value.ToString()}{
                    //                        metricGroup.First().SuffixText
                    //                    }"",");
                    //            break;
                    //        default:
                    //            summaryMetricForName.Value = string.Format(
                    //                $@"""{metricGroup.First().PrefixText}{value.ToString()}{
                    //                        metricGroup.First().SuffixText
                    //                    }"",");
                    //            break;
                    //    }
                    //}
                    //else
                    //{
                    //    summaryMetricForName.Value = string.Format(
                    //        $@"""{metricGroup.First().PrefixText}  {
                    //                metricGroup.First().SuffixText
                    //            }"",");
                    //}
                    aggregateMetrics.Add(summaryMetricForName);
                }
            });
            return aggregateMetrics;
        }

        private string MetricSummary(List<Metric> metric)
        {
            StringBuilder valueList = new StringBuilder();
            var Type = metric.First().Type;
            string aggregateMetric = String.Empty;
            foreach(var m in metric)
            {
                if (m.Type != "Change")
                {
                    if (m.Summarycalculation == Constants.CALCULATION_TEXT_SUM)
                    {
                        switch (m.DataType)
                        {
                            case Constants.DATATYPE_NUMERIC:
                                valueList.Append(
                                    $@"""{m.PrefixText}{float.Parse(m.Value):n0}{m.SuffixText}"",");
                                break;
                            case Constants.DATATYPE_CURRENCY:
                                valueList.Append(
                                    $@"""{m.PrefixText}{float.Parse(m.Value):C}{m.SuffixText}"",");
                                break;
                            case Constants.DATATYPE_TEXT:
                                valueList.Append(
                                    $@"""{m.PrefixText}{m.Value}{m.SuffixText}"",");
                                break;
                            default:
                                valueList.Append(
                                    $@"""{m.PrefixText}{m.Value}{m.SuffixText}"",");
                                break;
                        }
                    }
                    else if (m.Summarycalculation == Constants.CALCULATION_TEXT_COUNT)
                    {
                        valueList.Append(
                            $@"""{m.Count}"",");
                    }
                }
                else
                {
                    valueList.Append(
                        $@"""{m.PercentChange}"",");
                }


            }
            return valueList.ToString();
        }
       
    }
}
