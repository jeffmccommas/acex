﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ESPMReportWebJob.RestSharp;
using ESPMReportWebJob.Utils;
using Newtonsoft.Json;
using CE.ESPMDataModel.ESPMModels;

namespace ESPMReportWebJob.Handlers
{
    public class Configurationhandler
    {
        public static List<Metric> GetConfigurationMetrics(RestSharpHelper restSharpHelper)
        {
            List<Metric> metric = new List<Metric>();
            var configResponse  = restSharpHelper.Get(APIResourceURLS.GET_CONFIGURATION_CONTENT, null);
            var configs = JsonConvert.DeserializeObject<List<string>>(configResponse.Content);
            foreach (string item in configs)
            {
                metric.Add(JsonConvert.DeserializeObject<Metric>(item));
            }

            return metric;
        }

        
    }
}
