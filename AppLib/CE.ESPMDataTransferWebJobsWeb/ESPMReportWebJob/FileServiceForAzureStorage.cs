﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;

namespace ESPMReportWebJob
{
    public class FileServiceForAzureStorage : IFileService
    {
        public string StorageName { get; set; }
        public string StorageKey { get; set; }
        public string ContainerName { get; set; }

        public bool Exists(string path)
        {
            bool result = false;
            StorageCredentials creds = new StorageCredentials(StorageName, StorageKey);
            CloudStorageAccount account = new CloudStorageAccount(creds, useHttps: true);
            CloudBlobClient client = account.CreateCloudBlobClient();

            CloudBlobContainer container = client.GetContainerReference(ContainerName);
            if (container.Exists())
            {
                CloudBlockBlob blob = container.GetBlockBlobReference(path);
                if (blob.Exists())
                    result = true;
            }

            return result;
        }

        public void Delete(string path)
        {
            throw new NotImplementedException();
        }

        public void WriteAllText(string path, string text, bool overwrite)
        {

            //var storageAccountName = ConfigurationManager.AppSettings["StorageAccountName"];
            //var storageAccountKey = ConfigurationManager.AppSettings["StorageAccountKey"];
            //var containerName = ConfigurationManager.AppSettings["StorageContainerName"];

            StorageCredentials creds = new StorageCredentials(StorageName, StorageKey);
            CloudStorageAccount account = new CloudStorageAccount(creds, useHttps: true);
            CloudBlobClient client = account.CreateCloudBlobClient();

            CloudBlobContainer container = client.GetContainerReference(ContainerName);
            container.CreateIfNotExists();
            CloudBlockBlob blob = container.GetBlockBlobReference(path);
            if (overwrite)
                blob.DeleteIfExists();
            else
            {
                if (blob.Exists())
                {
                    path = path + "_" + DateTime.Now.ToString("yyyyMMddhhmmss");
                    blob = container.GetBlockBlobReference(path);
                }

            }
            using (Stream file = GenerateStreamFromString(text))
            {
                blob.UploadFromStream(file);
            }


        }

        public bool Save(string file, string saveas)
        {
            StorageCredentials creds = new StorageCredentials(StorageName, StorageKey);
            CloudStorageAccount account = new CloudStorageAccount(creds, useHttps: true);
            CloudBlobClient client = account.CreateCloudBlobClient();

            CloudBlobContainer container = client.GetContainerReference(ContainerName);
            if (container.Exists())
            {
                CloudBlockBlob blob = container.GetBlockBlobReference(file);

                if (blob.Exists())
                {
                    blob = container.GetBlockBlobReference(file);
                }


                using (var fileStream = System.IO.File.OpenWrite(saveas))
                {
                    blob.DownloadToStream(fileStream);
                }
            }
            return true;
        }
        public AzureBlobInfo Download(string file)
        {
            AzureBlobInfo blobInfo = new AzureBlobInfo();
            StorageCredentials creds = new StorageCredentials(StorageName, StorageKey);
            CloudStorageAccount account = new CloudStorageAccount(creds, useHttps: true);
            CloudBlobClient client = account.CreateCloudBlobClient();

            CloudBlobContainer container = client.GetContainerReference(ContainerName);
            if (container.Exists())
            {
                CloudBlockBlob blob = container.GetBlockBlobReference(file);

                if (blob.Exists())
                {
                    blob = container.GetBlockBlobReference(file);
                }

                blobInfo.DataStream = new MemoryStream();
                blob.DownloadToStream(blobInfo.DataStream);
                blobInfo.ContentType = blob.Properties.ContentType;
                blobInfo.BlobLength = blob.Properties.Length.ToString();


                return blobInfo;
            }
            return null;
        }
        public List<FileInfo> ListFilesInContainer()
        {
            List<FileInfo> info = new List<FileInfo>();
            StorageCredentials creds = new StorageCredentials(StorageName, StorageKey);
            CloudStorageAccount account = new CloudStorageAccount(creds, useHttps: true);
            CloudBlobClient client = account.CreateCloudBlobClient();

            CloudBlobContainer container = client.GetContainerReference(ContainerName);
            if (container.Exists())
            {
                //List<string> blobs = new List<string>();

                foreach (IListBlobItem item in container.ListBlobs(null, false))
                {
                    if (item.GetType() == typeof(CloudBlockBlob))
                    {
                        CloudBlockBlob blob = (CloudBlockBlob)item;
                        info.Add(new FileInfo
                        {
                            Name = blob.Name,
                            Directory = blob.Container.Name,
                            CreateDateTime = blob.Properties.LastModified.ToString()
                        });
                    }
                    else if (item.GetType() == typeof(CloudPageBlob))
                    {
                        CloudPageBlob blob = (CloudPageBlob)item;
                        info.Add(new FileInfo
                        {
                            Name = blob.Name,
                            Directory = blob.Container.Name,
                            CreateDateTime = blob.Properties.LastModified.ToString()
                        });
                    }
                    //else if (item.GetType() == typeof(CloudBlobDirectory))
                    //{
                    //    CloudBlobDirectory dir = (CloudBlobDirectory)item;
                    //    blobs.Add(dir.Uri.ToString());
                    //}
                }
            }
            var orderedList = info.OrderBy(x => DateTime.Parse(x.CreateDateTime)).ToList();
            return orderedList;
        }

        private Stream GenerateStreamFromString(string s)
        {
            return new MemoryStream(Encoding.UTF8.GetBytes(s));
        }




    }
}
