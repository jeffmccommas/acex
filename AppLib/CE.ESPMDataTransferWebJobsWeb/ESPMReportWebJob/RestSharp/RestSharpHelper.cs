﻿using System.Collections.Generic;
using System.Linq;
using RestSharp;
using System.Reflection;
using ESPMReportWebJob.Utils;
using System.Configuration;

namespace ESPMReportWebJob.RestSharp
{
    public class RestSharpHelper
    {
        public string AuthToken;
        private readonly RestClient client;

        public RestSharpHelper()
        {
            var baseUrl = ConfigurationManager.AppSettings[Constants.APIBASEURL];
            client = new RestClient(baseUrl);
        }
        public IRestResponse Post(string resourceUrl, string json)
        {
            var request = new RestRequest(resourceUrl, Method.POST);
            request.AddParameter("text/json", json, ParameterType.RequestBody);
            request.AddHeader(Constants.AUTHTOKEN, AuthToken);
            request.RequestFormat = DataFormat.Json;
            return client.Execute(request);

        }

        public IRestResponse Get(string resourceUrl, string queryParams = null)
        {
            var request = new RestRequest(resourceUrl, Method.GET);
            if (queryParams != null)
            {
                foreach (var item in GenerateQueryParams(queryParams))
                {
                    request.AddQueryParameter(item.Key, item.Value);
                }
            }
            request.AddHeader(Constants.AUTHTOKEN, AuthToken);
            request.RequestFormat = DataFormat.Json;
            return client.Execute(request);
        }

        public IRestResponse Get(string resourceUrl, object queryParams = null)
        {
            var request = new RestRequest(resourceUrl, Method.GET);
            if (queryParams != null)
            {
                foreach (var item in GenerateQueryParamsFromObject(queryParams))
                {
                    request.AddQueryParameter(item.Name, item.GetValue(queryParams, null)?.ToString());
                }
            }
            request.AddHeader(Constants.AUTHTOKEN, AuthToken);
            request.RequestFormat = DataFormat.Json;
            return client.Execute(request);
        }

        private PropertyInfo[] GenerateQueryParamsFromObject(object queryParams)
        {
            //IEnumerable<PropertyInfo> propInfos = queryParams.GetType().GetRuntimeProperties();
            //var query = String.Empty;
            return queryParams.GetType().GetProperties();
            //var properties = from p in queryParams.GetType().GetProperties()
            //                 where p.GetValue(queryParams, null) != null
            //                 select p.Name + "=" + (p.GetValue(queryParams, null).ToString());

            //return properties.ToArray();
        }

        private Dictionary<string, string> GenerateQueryParams(string queryParams)
        {
            var queryParamsNameValuePair = new Dictionary<string, string>();
            if (!string.IsNullOrEmpty(queryParams))
            {
                //get after the ? on the first character
                var querystringParsed = queryParams.Substring(queryParams.IndexOf('?') + 1);
                //split into key values
                {
                    var p = querystringParsed.Split('&');
                    foreach (var temp in from s in p where s.IndexOf('=') > -1 select s.Split('='))
                    {
                        queryParamsNameValuePair.Add(temp[0], temp[1]);
                    }
                }
            }
            return queryParamsNameValuePair;
        }

    }
}
