﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CE.ESPMDataTransferWebJobsWeb.Startup))]
namespace CE.ESPMDataTransferWebJobsWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
