﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CE.EventTrackWebJobsWeb.Startup))]
namespace CE.EventTrackWebJobsWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
