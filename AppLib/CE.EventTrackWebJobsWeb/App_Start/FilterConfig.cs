﻿using System.Web;
using System.Web.Mvc;

namespace CE.EventTrackWebJobsWeb
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
