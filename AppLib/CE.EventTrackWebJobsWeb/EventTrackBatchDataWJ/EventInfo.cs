﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace EventTrackBatchDataWJ
{
    [DataContract]
    public class EventInfo
    {
        [DataMember]
        public int ClientId { get; set; }
        [DataMember]
        public string CustomerId { get; set; }
        [DataMember]
        public string AccountId { get; set; }
        [DataMember]
        public string PremiseId { get; set; }
        [DataMember(Name ="CEEnvironment")]
        public string Environment { get; set; }
        [DataMember]
        public string EventType { get; set; }
        [DataMember(Name = "ProfileAttributes")]
        public string EventData { get; set; }
        [DataMember]
        public DateTime DateCreated { get; set; }
    }
}
