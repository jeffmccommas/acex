﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace EventTrackBatchDataWJ
{
    public class SimulatedActivities
    {

        public void Execute(string env)
        {
            SimulatedActivityA(env, 300);
            SimulatedActivityB(env, 100);
            SimulatedActivityC(env, 600);
        }

        public void SimulatedActivityA(string env, int millisecondsTimeout)
        {
            Console.WriteLine(string.Format("Environment: {0}, Simulated activity A: BEGIN.",
                                            env));

            Thread.Sleep(millisecondsTimeout);

            Console.WriteLine(string.Format("Environment: {0}, Simulated activity A: COMPLETED SUCCESSFULLY",
                                            env));
        }

        public void SimulatedActivityB(string env, int millisecondsTimeout)
        {
            Console.WriteLine(string.Format("Environment: {0}, Simulated activity B: BEGIN.",
                                            env));

            Thread.Sleep(millisecondsTimeout);

            Console.WriteLine(string.Format("Environment: {0}, Simulated activity B: COMPLETED SUCCESSFULLY",
                                            env));
        }

        public void SimulatedActivityC(string env, int millisecondsTimeout)
        {
            Console.WriteLine(string.Format("Environment: {0}, Simulated activity C: BEGIN.",
                                            env));

            Thread.Sleep(millisecondsTimeout);

            Console.WriteLine(string.Format("Environment: {0}, Simulated activity C: COMPLETED SUCCESSFULLY",
                                            env));
        }

    }

}
