﻿using CE.NLogger;
using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventTrackBatchDataWJ
{
    public class EventTracker
    {
        private int _batchSize = 1;
        private string _eventHubName;
        private string _environmentName;

        private ASyncLogger _logger;

        private const int DataSizeCushion = 10;
        private const long MaxDataSizeInBytes = 262144;
        private const string NotificationSource = "Event Tracking Notification";

        public EventTracker()
        {
            _batchSize = Convert.ToInt16(ConfigurationManager.AppSettings["BatchSize"]);
            _eventHubName = ConfigurationManager.AppSettings["EventHubName"];
            _environmentName = ConfigurationManager.AppSettings["Environment"];

            _logger = new ASyncLogger(276, string.Empty);
        }

        public void ProcessEvents()
        {
            try
            {
                var random = new Random();

                List<EventData> events = null;
                var partitionKey = random.Next().ToString();

                while ((events = ReadEvents(partitionKey)).Count > 0)
                {
                    var startId = Convert.ToInt32(events[0].Properties[partitionKey]);
                    var endId = Convert.ToInt32(events[events.Count - 1].Properties[partitionKey]);

                    try
                    {
                        _logger.Info("Event Tracking", "Low", NotificationSource, "Process Begins", string.Format("StartId:{0} - EndId:{1}", startId, endId));

                        UpdateEventsStatus(startId, endId);
                        _logger.Info("Update Status", "Low", NotificationSource, "Status changed to 1");

                        DispatchEvents(events);
                        _logger.Info("Dispatch", "Low", NotificationSource, "Events dispatched successfully");

                        DeleteEvents(startId, endId);
                        _logger.Info("Purge", "Low", NotificationSource, "Events deleted");

                        _logger.Info("Event Tracking", "Low", NotificationSource, "Process Ends");
                    }
                    catch (Exception ex)
                    {
                        _logger.SendMail("Event Tracking Error", "Events:" + startId + "-" + endId + Environment.NewLine + ex.Message + ex.StackTrace);
                        _logger.ErrorException("Event Tracking Error", "High", NotificationSource, ex);

                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.ErrorException("Event Tracking Error", "High", NotificationSource, ex);
            }

        }

        private List<EventData> ReadEvents(string partitionKey)
        {
            var events = new List<EventData>();
            var sql = string.Format("SELECT TOP {0} ID ,ClientId ,AccountId ,PremiseId ,CustomerID ,EventData ,EventType FROM Track_Events WHERE   Status = 0 ORDER BY ID;", _batchSize);

            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["InsightsDWConn"].ToString()))
            {
                var command = new SqlCommand(sql, sqlConnection);

                sqlConnection.Open();
                var reader = command.ExecuteReader();

                long dataSizeInBytes = 0;

                while (reader.Read())
                {
                    var info = new EventInfo()
                    {
                        ClientId = reader.GetInt32(1),
                        CustomerId = reader.GetString(4).ToString(),
                        PremiseId = reader.GetString(3).ToString(),
                        EventData = reader.GetString(5).ToString(),
                        AccountId = reader.GetString(2).ToString(),
                        EventType = reader.GetString(6).ToString(),
                        DateCreated = DateTime.Today,
                        Environment = _environmentName
                    };

                    var serializedString = JsonConvert.SerializeObject(info);

                    
                    var data = new EventData(Encoding.UTF8.GetBytes(serializedString))
                    {
                        PartitionKey = partitionKey
                    };

                    data.Properties.Add(partitionKey, reader.GetInt32(0));

                    dataSizeInBytes += data.SerializedSizeInBytes + DataSizeCushion;

                    if (dataSizeInBytes > MaxDataSizeInBytes)
                        break;
                        
                    events.Add(data);
                }
            }

            return events;
        }

        private void UpdateEventsStatus(int beginId, int endId)
        {
            var sql = string.Format("UPDATE dbo.Track_Events SET Status = 1 WHERE ID >= {0} AND ID <= {1};", beginId, endId);
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["InsightsDWConn"].ToString()))
            {
                var command = new SqlCommand(sql, sqlConnection);

                sqlConnection.Open();

                command.ExecuteScalar();
            }
        }

        public void DispatchEvents(List<EventData> events)
        {
            var retry = false;
            var retryCount = 0;

            do
            {
                try
                {
                    retry = false;

                    var client = EventHubClient.Create(_eventHubName);

                    client.SendBatch(events);
                }
                catch (Exception)
                {
                    if (retryCount < 2)
                    {
                        System.Threading.Thread.Sleep((retryCount + 1) * 60 * 1000);
                        retry = true;
                        retryCount += 1; 
                    }
                    else
                        throw;
                }
            } while (retry);
        }

        public void DeleteEvents(int beginId, int endId)
        {
            var retry = false;
            var retryCount = 0;

            do
            {
                try
                {
                    retry = false;

                    var sql = string.Format("DELETE FROM dbo.Track_Events WHERE ID >= {0} AND ID <= {1};", beginId, endId);
                    using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["InsightsDWConn"].ToString()))
                    {
                        var command = new SqlCommand(sql, sqlConnection);

                        sqlConnection.Open();

                        command.ExecuteScalar();
                    }
                }
                catch (Exception)
                {
                    if (retryCount < 2)
                    {
                        System.Threading.Thread.Sleep((retryCount + 1) * 60 * 1000);
                        retry = true;
                        retryCount += 1;
                    }
                    else
                        throw;
                }
            } while (retry);
        }
    }
}
