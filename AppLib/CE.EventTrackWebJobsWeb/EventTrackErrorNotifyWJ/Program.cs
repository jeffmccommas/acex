﻿using Microsoft.Azure.WebJobs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventTrackErrorNotifyWJ
{
    // To learn more about Microsoft Azure WebJobs SDK, please see http://go.microsoft.com/fwlink/?LinkID=320976
    class Program
    {

        private const string Configuration_AppSettingKey_Environment = "Environment";
        private const string Configuration_AppSettingKey_ProcessingEnabled = "ProcessingEnabled";

        static void Main()
        {
            string env = string.Empty;

            try
            {

                env = System.Configuration.ConfigurationManager.AppSettings[Configuration_AppSettingKey_Environment];
                if (AppSettings.Get<bool>(Configuration_AppSettingKey_ProcessingEnabled) == false)
                {
                    Console.WriteLine(string.Format("Processing has been disabled for this environment. Exited without processing. (Environment: {0})",
                                                    env));
                    return;
                }

                var manager = new NotificationManager();

                manager.ProcessNotifications();

            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    /// <summary>
    /// Type safe app settings class.
    /// </summary>
    public static class AppSettings
    {
        public static T Get<T>(string key)
        {
            var appSetting = ConfigurationManager.AppSettings[key];
            if (string.IsNullOrWhiteSpace(appSetting)) throw new SettingsPropertyNotFoundException(key);

            var converter = TypeDescriptor.GetConverter(typeof(T));
            return (T)(converter.ConvertFromInvariantString(appSetting));
        }
    }
}
