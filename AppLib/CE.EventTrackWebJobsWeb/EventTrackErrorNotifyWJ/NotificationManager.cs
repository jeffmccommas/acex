﻿using CE.EventTrack.Utilities;
using CE.NLogger;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using System;
using System.Configuration;
using System.Linq;
using System.Text;

//using System.Threading.Tasks;

namespace EventTrackErrorNotifyWJ
{
    public class NotificationManager
    {
        #region Private Constants

        private const string DocumentDbEndPoint = "documentDB.endpoint";
        private const string DocumentDbAuthKey = "documentDB.authKey";
        private const string DocumentDbDatabase = "documentDB.database";
        private const string DocumentDbCollection = "documentDB.collection";
        private const string EmailCount = "Event Tracking: There are {0} error event(s).";
        private const string ErrorMessage =
            "ClientId: {0} CustomerId: {1} AccountId: {2} PremiseId: {3} DateCreated: {4} ErrorMessage: {5}";
        private const string ErrorMessageMax =
            "Please check documentBD for more details on the remaining error events";
        #endregion

        public void ProcessNotifications()
        {
            try
            {
                // get documentDB settings from appsetting
                var documentDbEndpoint = ConfigurationManager.AppSettings.Get(DocumentDbEndPoint);
                var documentDbAuthKey = ConfigurationManager.AppSettings.Get(DocumentDbAuthKey);
                var documentDbDatabase = ConfigurationManager.AppSettings.Get(DocumentDbDatabase);
                var documentDbCollection = ConfigurationManager.AppSettings.Get(DocumentDbCollection);

                // create new instance of document client
                var client = new DocumentClient(new Uri(documentDbEndpoint), documentDbAuthKey);

                // verify database exists
                Database database =  
                    client.CreateDatabaseQuery()
                        .Where(db => db.Id == documentDbDatabase)
                        .AsEnumerable()
                        .FirstOrDefault();
                if (database != null)
                {
                    Console.WriteLine("Database {0} exists", documentDbDatabase);
                    // verify collection exists
                    DocumentCollection documentCollection = client.CreateDocumentCollectionQuery("dbs/" + database.Id).Where(c => c.Id == documentDbCollection).AsEnumerable().FirstOrDefault();

                    if (documentCollection != null)
                    {
                        Console.WriteLine("Collection {0} exists", documentDbCollection);
                        var startTime = DateTime.Now.AddDays(-1).ToEpoch();

                        // get all error events created starting from yesterday
                        var errorEvents = client.CreateDocumentQuery<ErrorEvent>(documentCollection.DocumentsLink)
                            .Where(e => e.EpochDateCreated > startTime)
                            .ToList();

                        // email error event details if there's any
                        if (errorEvents.Any())
                        {
                            var logger = new ASyncLogger(0,string.Empty);
                            var count = errorEvents.Count();
                            var subject = (string.Format(EmailCount, count));
                            Console.WriteLine(subject);
                            Console.WriteLine("Details:");
                            var sbMessage  = new StringBuilder();
                            var errorCount = 0;
                            foreach (var errorEvent in errorEvents)
                            {
                                if (errorCount < 1000)
                                {
                                    var message = String.Format(ErrorMessage,
                                        string.IsNullOrEmpty(errorEvent.EventData.ClientId)
                                            ? string.Empty
                                            : errorEvent.EventData.ClientId,
                                        string.IsNullOrEmpty(errorEvent.EventData.CustomerId)
                                            ? string.Empty
                                            : errorEvent.EventData.CustomerId,
                                        string.IsNullOrEmpty(errorEvent.EventData.AccountId)
                                            ? string.Empty
                                            : errorEvent.EventData.AccountId,
                                        string.IsNullOrEmpty(errorEvent.EventData.PremiseId)
                                            ? string.Empty
                                            : errorEvent.EventData.PremiseId,
                                        string.IsNullOrEmpty(errorEvent.EventData.DateCreated)
                                            ? string.Empty
                                            : errorEvent.EventData.DateCreated,
                                        string.IsNullOrEmpty(errorEvent.ErrorMessage)
                                            ? string.Empty
                                            : errorEvent.ErrorMessage);
                                    Console.WriteLine(message);
                                    sbMessage.Append(message);
                                    sbMessage.AppendLine();
                                    errorCount++;
                                }
                                else
                                {
                                    Console.WriteLine(ErrorMessageMax);
                                    sbMessage.Append(ErrorMessageMax);
                                    break;
                                }
                            }
                            logger.SendMail(subject,sbMessage.ToString());
                        }

                     }
                    else
                    {
                        {
                            Console.WriteLine("Collection {0} doesn't exist", documentDbCollection);
                        }
                    }
                }
                else
                {
                    Console.WriteLine("Database {0} doesn't exist", documentDbDatabase);
                }
            }
            catch (DocumentClientException de)
            {
                Exception baseException = de.GetBaseException();
                Console.WriteLine("{0} error occurred: {1}, Message: {2}", de.StatusCode, de.Message, baseException.Message);
            }
            catch (Exception e)
            {
                Exception baseException = e.GetBaseException();
                Console.WriteLine("Error: {0}, Message: {1}", e.Message, baseException.Message);
            }
            //finally
            //{
            //    Console.WriteLine("End of notification process, press any key to exit.");
            //    Console.ReadKey();
            //}

        }
    }
    
    

    [Serializable]
    public class EventData
    {
        public string ClientId { get; set; }
        public string AccountId { get; set; }
        public string CustomerId { get; set; }
        public string PremiseId { get; set; }
        public string EventType { get; set; }
        public string PageId { get; set; }
        public string ActionId { get; set; }
        public string DateCreated { get; set; }
        public string EstimatedSavings { get; set; }
        public string CEEnvironment { get; set; }
        public string EstimatedCost { get; set; }
        public string EstimatedRebate { get; set; }
        public string TabId { get; set;  }
        public string SavinigsAmount { get; set; }
        public string SavingsPercentage { get; set; }
    }

    [Serializable]
    public class ErrorEvent
    {
        public string DateCreated { get; set; }
        public int EpochDateCreated { get; set; }
        public string ErrorMessage { get; set; }
        public EventData EventData { get; set; }
    }
}