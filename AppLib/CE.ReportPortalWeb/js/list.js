(function() {
  $(function() {
    var HtmlMode, editor;
    HtmlMode = window.ace.require("ace/mode/html").Mode;
    editor = window.ace.edit("code-preview-list");
    editor.getSession().setMode(new HtmlMode());
    editor.setTheme("ace/theme/github");
  });

}).call(this);
