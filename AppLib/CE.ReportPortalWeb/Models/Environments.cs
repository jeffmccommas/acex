﻿using System.Collections.Generic;
using System.Linq;

namespace CE.ReportPortalWeb.Models
{
    public class Environments
    {
        public int EnvID { get; set; }
        public string EnvName { get; set; }
        public string EnvDescription { get; set; }

        public List<Environments> GetEnvironments()
        {
            using (var ent=new InsightsEntities())
            {
                var envList = (from e in ent.Environments
                    select new
                    {
                        e.EnvID,
                        e.Name,
                        e.Description
                    }).ToList();
                var ev = new List<Environments>();
                foreach (var e in envList)
                {
                    var env = new Environments
                    {
                        EnvID = e.EnvID,
                        EnvName = e.Name,
                        EnvDescription = e.Description
                    };
                    ev.Add(env);
                }
                return ev;
            }
        }
    }
}