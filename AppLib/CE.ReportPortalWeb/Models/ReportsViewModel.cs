﻿using System;
using System.Collections.Generic;
using Microsoft.PowerBI.Api.V1.Models;



namespace CE.ReportPortalWeb.Models
{
    public class ReportsViewModel
    {
        //public List<Dictionary<Report,string>> Reports { get; set; }
        //public List<string> ReportsDetailsEnc { get; set; }
        public List<ReportDetails> ReportsDetails { get; set; }
    }
}