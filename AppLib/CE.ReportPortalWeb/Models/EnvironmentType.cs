﻿
namespace CE.ReportPortalWeb.Models
{
    public enum EnvironmentType
    {
        prod,
        uat,
        qa,
        dev,
        localdev
    }
}