﻿using System.Web.Mvc;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using CE.ReportPortalWeb.Common;

namespace CE.ReportPortalWeb.Models
{
    public class Clients
    {
        [Required(ErrorMessage = "Client ID is Required")]
        [Range(1, int.MaxValue, ErrorMessage = "Client ID must be a number")]
        public int ClientID { get; set; }

        [Required(ErrorMessage = "Client Name is Required")]
        public string ClientName{ get; set; }

        
        public string Description { get; set; }

        [Required(ErrorMessage = "Power BI Workspace ID is Required")]
        public string PBIWorkspaceID { get; set; }

        public int EnvID { get; set; }

        public string Environment { get; set; }
    }
}