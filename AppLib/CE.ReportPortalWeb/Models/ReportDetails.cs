﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.PowerBI.Api.V1.Models;

namespace CE.ReportPortalWeb.Models
{
    public class ReportDetails
    {
        public Report Reports { get; set; }
        public string encDetails { get; set; }
    }
}