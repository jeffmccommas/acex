﻿using System.Web.Mvc;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using CE.ReportPortalWeb.Common;

namespace CE.ReportPortalWeb.Models
{
    public class Logins
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        [Required(ErrorMessage = "User Name is Required")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Password is Required")]
        public string Password { get; set; }

        public int EnvID { get; set; }

        public bool Enabled { get; set; }

        public int ClientID { get; set; }
        
        public string UserType { get; set;}
       
        public string Client { get; set; }

        public List<SelectListItem> ClientList { get; set; }
    }
}