﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.PowerBI.Api.V1;
using Microsoft.PowerBI.Security;
using Microsoft.Rest;
using CE.ReportPortalWeb.Common;
using CE.ReportPortalWeb.Common.QueryStringEncryptor;
using CE.ReportPortalWeb.Models;

namespace CE.ReportPortalWeb.Controllers
{
    public class DashboardController : Controller
    {
        private readonly string _workspaceCollection;
        private string _workspaceId;
        private readonly string _accessKey;
        private readonly string _apiUrl;
        private readonly byte _envID;

        public DashboardController()
        {
            _workspaceCollection = ConfigurationManager.AppSettings["powerbi:WorkspaceCollection"];
            //_workspaceId = ConfigurationManager.AppSettings["powerbi:WorkspaceId"];
            _accessKey = ConfigurationManager.AppSettings["powerbi:AccessKey"];
            _apiUrl = ConfigurationManager.AppSettings["powerbi:ApiUrl"];
            _envID=Helpers.GetCurrentEnvironment();
        }

        public ActionResult Index()
        {
            if (Session[Constants.USER_SESSION_VARIABLE] == null)
            {
                Session.Abandon();
                return View("~/Views/Login/Index.cshtml");
            }

            return View();
        }

        [HttpPost]
        public ActionResult Logout()
        {
            Session.Abandon();
            return View("Index");
        }

        [ChildActionOnly]
        public ActionResult Reports()
        {
            if (Session[Constants.USER_SESSION_VARIABLE] == null)
            {
                return View("~/Views/Login/Index.cshtml");
            }
            var cliId = ((UserSessionVariables) Session[Constants.USER_SESSION_VARIABLE]).clientID; //Request["clientId"];
            var clientId=Int32.Parse(cliId);
            using (var ent = new InsightsEntities())
            {
                ent.Database.Connection.Open();
                string pbiWorkspaceId;
                //Get workspace ID according to the environment and client ID
                if (_envID==0)
                {
                     pbiWorkspaceId = (from u in ent.PowerBIPortalClients
                                          where u.ClientID == clientId && u.EnvID==0
                                          select u.PBIWorkspaceID).FirstOrDefault();
                }
                else
                {
                     pbiWorkspaceId = (from u in ent.PowerBIPortalClients
                                          where u.ClientID == clientId && (u.EnvID==1 || u.EnvID==2 || u.EnvID==3 || u.EnvID==4)
                                          select u.PBIWorkspaceID).FirstOrDefault();
                }
                _workspaceId = pbiWorkspaceId;
                ent.Database.Connection.Close();
            }
            //Retrieve all the reports under the user's client ID
            using (var client = CreatePowerBiClient())
            {
                try
                {
                    var reportsResponse = client.Reports.GetReports(_workspaceCollection, _workspaceId);
                    var reportsList = new List<ReportDetails>();

                    foreach (var rep in reportsResponse.Value.ToList())
                    {
                        var repDetvar = new ReportDetails();
                        repDetvar.Reports = rep;
                        repDetvar.encDetails = QueryStringModule.Encrypt("reportId=" + rep.Id + "&clientId=" + clientId);
                        reportsList.Add(repDetvar);
                    }

                    var viewModel = new ReportsViewModel
                    {
                        ReportsDetails = reportsList
                    };

                    return PartialView(viewModel);
                }
                catch (Exception)
                {

                    return PartialView();
                }
                
            }
            
        }

        private string ExtractString(string mainStr,string key,string end)
        {
            var startindex = mainStr.IndexOf(key) + 9;
            if (end!="")
            {
                var substring = mainStr.Substring(startindex);
                var endindex = substring.IndexOf("&");
                return substring.Substring(0, endindex);
            }
            return mainStr.Substring(startindex);
        }

        //Embed report through the report ID and client ID
        public async Task<ActionResult> Report(string enc)
        {
            if (Session[Constants.USER_SESSION_VARIABLE] == null)
            {
                Session.Abandon();
                return View("~/Views/Login/Index.cshtml");
            }
            string dec = QueryStringModule.Decrypt(enc);
            var reportId = ExtractString(dec, "reportId=","&");
            var clientId = ExtractString(dec, "clientId=","");
            var cliId = Int32.Parse(clientId);
            using (var ent = new InsightsEntities())
            {
                ent.Database.Connection.Open();
                var pbiWorkspaceId = (from u in ent.PowerBIPortalClients
                                      where u.ClientID == cliId
                                      select u.PBIWorkspaceID).FirstOrDefault();
                _workspaceId = pbiWorkspaceId;
               
            }
            
            using (var client = CreatePowerBiClient())
            {
                var reportsResponse = await client.Reports.GetReportsAsync(_workspaceCollection, _workspaceId);
                var report = reportsResponse.Value.FirstOrDefault(r => r.Id == reportId);
                if (report != null)
                {
                    var embedToken = PowerBIToken.CreateReportEmbedToken(_workspaceCollection, _workspaceId, report.Id);

                    var viewModel = new ReportViewModel
                    {
                        Report = report,
                        AccessToken = embedToken.Generate(_accessKey)
                    };

                    return View(viewModel);
                }
            }
            return View();
        }
        
        private IPowerBIClient CreatePowerBiClient()
        {
            var credentials = new TokenCredentials(_accessKey, "AppKey");
            var client = new PowerBIClient(credentials)
            {
                BaseUri = new Uri(_apiUrl)
            };

            return client;
        }
    }
}