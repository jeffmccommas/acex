﻿using CE.ReportPortalWeb.Common;
using CE.ReportPortalWeb.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;

namespace CE.ReportPortalWeb.Controllers
{
    public class LoginController : Controller
    {
        private const string AppSetting_CEEnvironment = "CEEnvironment";

        // GET: Login
        public ActionResult Index()
        {
            ViewData["CEEnvironment"] = ConfigurationManager.AppSettings.Get(AppSetting_CEEnvironment);

            return View();
        }


        [HttpPost]
        public JsonResult CheckUser(string user, string password, string clientId)
        {
            if (user.Length == 0 && password.Length == 0 && clientId.Length == 0)
            {
                return Json("false_Please enter all details.", JsonRequestBehavior.AllowGet);
            }
            if (user.Length == 0 && password.Length == 0)
            {
                return Json("false_Please enter the username and password.", JsonRequestBehavior.AllowGet);
            }
            if (password.Length == 0 && clientId.Length == 0)
            {
                return Json("false_Please enter the password and Client ID.", JsonRequestBehavior.AllowGet);
            }
            if (clientId.Length == 0 && user.Length == 0)
            {
                return Json("false_Please enter the username and client ID.", JsonRequestBehavior.AllowGet);
            }
            if (user.Length == 0)
            {
                return Json("false_Please enter the username.", JsonRequestBehavior.AllowGet);
            }
            if (password.Length == 0)
            {
                return Json("false_Please enter the password.", JsonRequestBehavior.AllowGet);
            }
            if (clientId.Length == 0)
            {
                return Json("false_Please enter the client ID.", JsonRequestBehavior.AllowGet);
            }
            if (!clientId.All(char.IsDigit))
            {
                return Json("false_Please enter valid client ID",JsonRequestBehavior.AllowGet);
            }
            var environmentID = Convert.ToInt32(Helpers.GetCurrentEnvironment());
            var cliID = Convert.ToInt32(clientId);
            using (var ent = new InsightsEntities())
            {
                ent.Database.Connection.Open();
                PowerBIPortalLogin loginUser;
                //For production
                if (environmentID == 0)
                {
                    var loginUsers = (from u in ent.PowerBIPortalLogins
                        where u.Username == user && u.EnvID == environmentID
                        select u).GetEnumerator();
                    loginUsers.MoveNext();
                    loginUser = loginUsers.Current;
                    do
                    {
                        if (loginUsers.Current!=null && loginUsers.Current.ClientID == cliID)
                        {
                            loginUser = loginUsers.Current;
                        }
                    } while (loginUsers.MoveNext());
                    if (loginUser == null)
                    {
                        return Json("false_User does not exist", JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    var loginUsers = (from u in ent.PowerBIPortalLogins
                                      where u.Username == user
                                      select u).GetEnumerator();
                    loginUsers.MoveNext();
                    loginUser = loginUsers.Current;
                    do
                    {
                        if (loginUsers.Current != null && loginUsers.Current.ClientID == cliID)
                        {
                            loginUser = loginUsers.Current;
                        }
                    } while (loginUsers.MoveNext());
                    
                    if (loginUser == null)
                    {
                        return Json("false_User does not exist", JsonRequestBehavior.AllowGet);
                    }
                    if (loginUser.EnvID==0)
                    {
                        return Json("false_Cannot login to non-production environment with production user",
                            JsonRequestBehavior.AllowGet);
                    }
                }
                
                if (loginUser.Enabled != null && (loginUser.Enabled.Value == false))
                {
                    return Json("false_Disabled user", JsonRequestBehavior.AllowGet);
                }
                if (loginUser.UserType.ToLower()=="admin")
                {
                    var clientsList = (from c in ent.PowerBIPortalClients orderby c.ClientID select c.ClientID).ToList();
                    var valid = false;
                    foreach (var client in clientsList)
                    {
                        if (cliID==client)
                        {
                            valid = true;
                        }
                    }
                    if (!valid)
                    {
                        return Json("false_Invalid Client ID",JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    if (loginUser.ClientID!=cliID)
                    {
                        return Json("false_Invalid Client ID",JsonRequestBehavior.AllowGet);
                    }
                }
                
                ent.Database.Connection.Close();
                var encryptor = new Encryptor(password);
                if (!encryptor.VerifyHash(password, loginUser.PasswordSalt, loginUser.PasswordHash))
                {
                    return Json("false_Bad username/password", JsonRequestBehavior.AllowGet);
                }

                
                    var usv = new UserSessionVariables
                    {
                        clientID = clientId,
                        UserName = user,
                        UserType = loginUser.UserType
                    };

                    Session[Constants.USER_SESSION_VARIABLE] = usv;
                
                return Json(clientId, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Create()
        {
            if (Session[Constants.USER_SESSION_VARIABLE] == null)
            {
                return View("Index");
            }
            //populate client IDs
            var user=new Logins();
            user.ClientList = PopulateClientList();
            return View(user);
        }

        private List<SelectListItem> PopulateClientList()
        {
            using (var ent = new InsightsEntities())
            {
                ent.Database.Connection.Open();
                var clients = (from c in ent.PowerBIPortalClients orderby c.ClientID select c.ClientID).ToList();
                var clientsList = new List<SelectListItem>();
                foreach (var t in clients)
                {
                    clientsList.Add(new SelectListItem {Value = t.ToString(), Text = t.ToString()});

                }
                return clientsList;
            }
        }

        public ActionResult Logout()
        {
            Session.Abandon();
            return View("Index");
        }

        // POST: 
        [HttpPost]
        public ActionResult Create(Logins login)
        {
            if (Session[Constants.USER_SESSION_VARIABLE] == null)
            {
                return View("Index");
            }
            if (ModelState.IsValid)
            {
                try
                {
                    using (var ent = new InsightsEntities())
                    {
                        
                        //Check if login exists
                        var lList = (from c in ent.PowerBIPortalLogins
                            where c.Username == login.UserName && c.ClientID == login.ClientID && c.EnvID == login.EnvID
                            select c.Username).ToList();
                        if (lList.Count > 0)
                        {
                            ModelState.AddModelError("UserName", "Duplicate Username");
                            //return Json("false", JsonRequestBehavior.AllowGet);
                            var user1 = new Logins();
                            user1.ClientList = PopulateClientList();
                            return View(user1);

                        }
                        //check for usertype
                        var userType = "user";
                        if (login.UserType == "true")
                        {
                            userType = "admin";
                        }
                        login.Enabled = true;
                        //Create login account
                        var newUserEnvID = (byte) login.EnvID;
                        var pbil = new PowerBIPortalLogin
                        {
                            Username = login.UserName,
                            FirstName = login.FirstName,
                            LastName = login.LastName,
                            ClientID = login.ClientID,
                            EnvID = newUserEnvID,
                            UpdateDate = DateTime.Now,
                            UserType = userType,
                            Enabled = true
                        };

                        //Encrypt password by hashing and using salt
                        var encryptor = new Encryptor("xv");
                        var bytes = new byte[16];
                        using (var rng = new System.Security.Cryptography.RNGCryptoServiceProvider())
                        {
                            rng.GetBytes(bytes);
                        }

                        // and if you need it as a string...
                        string hash = BitConverter.ToString(bytes).Replace("-", "");

                        var pass = encryptor.GenerateHashWithSalt(login.Password, hash);
                        pbil.PasswordHash = pass;
                        pbil.PasswordSalt = hash;
                        ent.PowerBIPortalLogins.Add(pbil);
                        ent.SaveChanges();
                        //Return to the dashboard
                        return Content("<script language='javascript' type='text/javascript'>alert('User created successfully');window.location.href=location.href.replace('/Login/Create','/Dashboard/Index');</script>");
                    }
                }
                catch (Exception ex)
                {
                    return Json(ex, JsonRequestBehavior.DenyGet);
                }
            }
            var user = new Logins();
            user.ClientList = PopulateClientList();
            return View(user);
        }

        //GET:
        public ActionResult List()
        {
            if (Session[Constants.USER_SESSION_VARIABLE] == null)
            {
                return View("Index");
            }
            using (var ent = new InsightsEntities())
            {
                Helpers.GetCurrentEnvironment();
                var loginList = (from l in ent.PowerBIPortalLogins
                    join c in ent.PowerBIPortalClients
                        on l.ClientID equals c.ClientID
                    //where l.EnvID == envID
                    orderby c.EnvID,c.ClientName
                    //&& l.ClientID == cliID
                    select new {l.FirstName, l.LastName, l.Username, l.UserType, l.Enabled, c.ClientName}).ToList();

                var ll = new List<Logins>();
                foreach (var c in loginList)
                {
                    var login = new Logins
                    {
                        FirstName = c.FirstName,
                        LastName = c.LastName,
                        UserName = c.Username,
                        UserType = c.UserType,
                        Enabled = c.Enabled.Value,
                        Client = c.ClientName
                    };

                    ll.Add(login);
                }

                return View(ll);
            }
        }

        public ActionResult Edit(string username, string client, bool enabled)
        {
            if (Session[Constants.USER_SESSION_VARIABLE] == null)
            {
                return View("Index");
            }
            using (var ent = new InsightsEntities())
            {
                ent.Database.Connection.Open();
                var user = (from ue in ent.PowerBIPortalLogins
                    join c in ent.PowerBIPortalClients on ue.ClientID equals c.ClientID
                    where ue.Username == username && c.ClientName == client
                    select ue).FirstOrDefault();
                if (user != null)
                {
                    user.Enabled = enabled;
                    user.UpdateDate = DateTime.Now;
                }
                ent.PowerBIPortalLogins.Attach(user);
                ent.Entry(user).Property(x => x.Enabled).IsModified = true;
                ent.Entry(user).Property(x => x.UpdateDate).IsModified = true;
                ent.SaveChanges();
                ent.Database.Connection.Close();
                return Json(true, JsonRequestBehavior.AllowGet);
            }
        }

        //GET
        public ActionResult Clients()
        {
            if (Session[Constants.USER_SESSION_VARIABLE] == null)
            {
                return View("Index");
            }

            using (var ent = new InsightsEntities())
            {
                Helpers.GetCurrentEnvironment();
                var clientList = (from c in ent.PowerBIPortalClients
                    join e in ent.Environments
                        on c.EnvID equals e.EnvID
                    orderby c.EnvID
                    select new {c.ClientID, c.ClientName, c.Description, c.PBIWorkspaceID, c.EnvID, e.Name}).ToList();

                var ll = new List<Clients>();
                foreach (var c in clientList)
                {
                    var client = new Clients()
                    {
                        ClientID = c.ClientID,
                        ClientName = c.ClientName,
                        Description = c.Description,
                        PBIWorkspaceID = c.PBIWorkspaceID,
                        EnvID = c.EnvID.Value,
                        Environment = c.Name
                    };

                    ll.Add(client);
                }

                return View(ll);
            }
        }

        public ActionResult CreateClient()
        {
            if (Session[Constants.USER_SESSION_VARIABLE] == null)
            {
                return View("Index");
            }

            return View();
        }

        // POST: 
        [HttpPost]
        public ActionResult CreateClient(Clients client)
        {
            if (Session[Constants.USER_SESSION_VARIABLE] == null)
            {
                return View("Index");
            }
            if (ModelState.IsValid)
            {
                try
                {
                    //var tempCliId = ((UserSessionVariables)Session[Constants.USER_SESSION_VARIABLE]).clientID;
                    //var cliId = Int32.Parse(tempCliId);
                    using (var ent = new InsightsEntities())
                    {
                        //login.EnvID = Convert.ToInt32(Helpers.GetCurrentEnvironment());
                        //Check if client exists
                        var lList = (from c in ent.PowerBIPortalClients
                            where c.ClientID == client.ClientID
                            select c.ClientID).ToList();
                        if (lList.Count > 0)
                        {
                            ModelState.AddModelError("ClientID", "Duplicate Client");
                            return View();
                        }
                        var envid = Convert.ToByte(client.EnvID);
                        //Create client
                        var cli = new PowerBIPortalClient()
                        {
                            ClientID = client.ClientID,
                            ClientName = client.ClientName,
                            Description = client.Description,
                            PBIWorkspaceID = client.PBIWorkspaceID,
                            EnvID = envid,
                        };
                        ent.PowerBIPortalClients.Add(cli);
                        ent.SaveChanges();
                        //Return to the client list
                        return Content("<script language='javascript' type='text/javascript'>alert('Client created successfully');window.location.href=location.href.replace('/CreateClient','/Clients');</script>");
                        //return RedirectToAction("Clients");
                    }
                }
                catch (Exception ex)
                {
                    return Json(ex, JsonRequestBehavior.DenyGet);
                }
            }

            return View();
        }

        
        public ActionResult DeleteClient(int clientID,string environment)
        {
            if (Session[Constants.USER_SESSION_VARIABLE] == null)
            {
                return View("Index");
            }
            using (var ent = new InsightsEntities())
            {
                var env= (int)System.Enum.Parse(typeof(EnvironmentType), environment, true);
                ent.Database.Connection.Open();
                var client = (from c in ent.PowerBIPortalClients
                            join e in ent.Environments on c.EnvID equals e.EnvID
                            where c.ClientID==clientID && e.EnvID==env
                            select c).FirstOrDefault();
                if (client != null)
                {
                    ent.PowerBIPortalClients.Remove(client);
                    ent.SaveChanges();
                }
                ent.Database.Connection.Close();
                return Json(true, JsonRequestBehavior.AllowGet);
            }
        }
        
        public ActionResult EditClient(string ClientID, string Environment)
        {
            if (Session[Constants.USER_SESSION_VARIABLE] == null)
            {
                return View("Index");
            }
            var env = (int)System.Enum.Parse(typeof(EnvironmentType), Environment, true);
            var clientID = Int32.Parse(ClientID);
            using (var ent=new InsightsEntities())
            {
                var cli = (from c in ent.PowerBIPortalClients
                    where c.ClientID == clientID && c.EnvID==env
                    select c).FirstOrDefault();
                if (cli==null)
                {
                    return HttpNotFound();
                }
                
                var client = new Clients()
                {
                    ClientID = cli.ClientID,
                    ClientName = cli.ClientName,
                    Description = cli.Description,
                    PBIWorkspaceID = cli.PBIWorkspaceID,
                    Environment = Environment,
                    EnvID = env
                };
                
                return View(client);
            }
        }

        [HttpPost]
        public ActionResult EditClient(Clients client)
        {
            if (Session[Constants.USER_SESSION_VARIABLE] == null)
            {
                return View("Index");
            }
            if (ModelState.IsValid)
            {

                using (var ent = new InsightsEntities())
                {
                    ent.Database.Connection.Open();
                    var cli = (from c in ent.PowerBIPortalClients
                                where c.ClientID==client.ClientID && c.EnvID==client.EnvID
                                select c).FirstOrDefault();
                    if (cli!=null)
                    {
                        cli.ClientName = client.ClientName;
                        cli.Description = client.Description;
                        cli.PBIWorkspaceID = client.PBIWorkspaceID;
                    }
                    ent.PowerBIPortalClients.Attach(cli);
                    ent.Entry(cli).Property(x => x.ClientName).IsModified = true;
                    ent.Entry(cli).Property(x => x.Description).IsModified = true;
                    ent.Entry(cli).Property(x => x.PBIWorkspaceID).IsModified = true;
                    ent.SaveChanges();
                    ent.Database.Connection.Close();
                    return Content("<script language='javascript' type='text/javascript'>alert('Client edited successfully');window.location.href=location.href.replace('/EditClient','/Clients');</script>");
                    //return RedirectToAction("Clients");
                }
            }
            return EditClient(client.ClientID.ToString(), client.Environment);
            //return View();
        }
    }
}