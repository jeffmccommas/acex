﻿using System;

namespace CE.ReportPortalWeb.Common
{

    [Serializable()]
    public class UserSessionVariables
    {
        public string UserName { get; set; }
        public string clientID { get; set; }
        public string UserType { get; set; }
    }
}