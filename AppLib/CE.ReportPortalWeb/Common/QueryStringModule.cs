﻿#region Using

using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Web;

#endregion

namespace CE.ReportPortalWeb.Common.QueryStringEncryptor
{

    /// <summary>
    /// Summary description for QueryStringModule
    /// </summary>
    public class QueryStringModule : IHttpModule
    {

        #region IHttpModule Members

        public void Dispose()
        {
            // Nothing to dispose
        }

        public void Init(HttpApplication context)
        {
            context.BeginRequest += new EventHandler(context_BeginRequest);
        }

        #endregion

        private const string PARAMETER_NAME = "enc=";
        private const string ENCRYPTION_KEY = "UFXAclaraSoftware";

        void context_BeginRequest(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;

            // if (context.Request.Url.OriginalString.Contains("aspx") && context.Request.RawUrl.Contains("?") && !context.Request.Url.OriginalString.Contains("WorkArea"))
            if (context.Request.RawUrl.Contains("?") && !context.Request.Url.OriginalString.Contains("WorkArea")
                 && !context.Request.ContentType.Contains("json")
                && !context.Request.Url.OriginalString.Contains("SolarCalcCustomization")
                && !context.Request.Url.OriginalString.Contains("FusionCharts"))
            {
                string query = ExtractQuery(context.Request.RawUrl);
                string path = GetVirtualPath();

                int indexOfEnc = query.IndexOf(PARAMETER_NAME, StringComparison.OrdinalIgnoreCase);

                if (indexOfEnc > -1)
                {
                    // Decrypts the query string and rewrites the path.                   
                    context.RewritePath(path, string.Empty, query.Substring(0, indexOfEnc) + Decrypt(query.Substring(indexOfEnc + PARAMETER_NAME.Length)));
                }
                else if (context.Request.HttpMethod == "GET" || context.Request.HttpMethod == "POST")
                {
                    // Encrypt the query string and redirects to the encrypted URL.
                    // Remove if you don't want all query strings to be encrypted automatically.
                    string encryptedQuery = Encrypt(query);
                    string tempRawUrl = context.Request.RawUrl.ToLower();
                    if (!(context.Request.HttpMethod == "POST" && tempRawUrl.Contains("ha")))
                    {
                        context.Response.Redirect(path + encryptedQuery);
                    }
                }
            }

            //context.Response.Write(context.Request.HttpMethod + context.Request.RawUrl);
        }


        public static string EncryptQueryString(string url)
        {
            HttpContext context = HttpContext.Current;
            string path;
            if (url.Contains("?"))
            {

                path = url.Substring(0, url.IndexOf("?"));
                string query = ExtractQuery(url);

                if (context.Request.HttpMethod == "GET" || context.Request.HttpMethod == "POST")
                {
                    // Encrypt the query string and redirects to the encrypted URL.
                    // Remove if you don't want all query strings to be encrypted automatically.
                    string encryptedQuery = Encrypt(query);
                    path = path + encryptedQuery;
                }
            }
            else
            {
                path = url;
            }

            return path;

        }

        /// <summary>
        /// Parses the current URL and extracts the virtual path without query string.
        /// </summary>
        /// <returns>The virtual path of the current URL.</returns>
        private static string GetVirtualPath()
        {
            string path = HttpContext.Current.Request.RawUrl;
            path = path.Substring(0, path.IndexOf("?"));
            path = path.Substring(path.LastIndexOf("/") + 1);
            return path;
        }

        private static string GetFullPath()
        {
            string path = HttpContext.Current.Request.RawUrl;
            path = path.Substring(0, path.IndexOf("?"));
            path = path.Substring(path.LastIndexOf("/") + 1);
            return path;
        }

        /// <summary>
        /// Parses a URL and returns the query string.
        /// </summary>
        /// <param name="url">The URL to parse.</param>
        /// <returns>The query string without the question mark.</returns>
        private static string ExtractQuery(string url)
        {
            int index = url.IndexOf("?") + 1;
            return url.Substring(index);
        }

        #region Encryption/decryption

        /// <summary>
        /// The salt value used to strengthen the encryption.
        /// </summary>
        private readonly static byte[] SALT = Encoding.ASCII.GetBytes(ENCRYPTION_KEY.Length.ToString());

        /// <summary>
        /// Encrypts any string using the Rijndael algorithm.
        /// </summary>
        /// <param name="inputText">The string to encrypt.</param>
        /// <returns>A Base64 encrypted string.</returns>
        public static string Encrypt(string inputText)
        {
            RijndaelManaged rijndaelCipher = new RijndaelManaged();
            byte[] plainText = Encoding.Unicode.GetBytes(inputText);
            PasswordDeriveBytes SecretKey = new PasswordDeriveBytes(ENCRYPTION_KEY, SALT);

            using (ICryptoTransform encryptor = rijndaelCipher.CreateEncryptor(SecretKey.GetBytes(32), SecretKey.GetBytes(16)))
            {
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                    {
                        cryptoStream.Write(plainText, 0, plainText.Length);
                        cryptoStream.FlushFinalBlock();
                        //return "?" + PARAMETER_NAME + Convert.ToBase64String(memoryStream.ToArray());
                        return Convert.ToBase64String(memoryStream.ToArray());
                    }
                }
            }
        }

        /// <summary>
        /// Decrypts a previously encrypted string.
        /// </summary>
        /// <param name="inputText">The encrypted string to decrypt.</param>
        /// <returns>A decrypted string.</returns>
        public static string Decrypt(string inputText)
        {
            RijndaelManaged rijndaelCipher = new RijndaelManaged();
            byte[] encryptedData = Convert.FromBase64String(inputText);
            PasswordDeriveBytes secretKey = new PasswordDeriveBytes(ENCRYPTION_KEY, SALT);

            using (ICryptoTransform decryptor = rijndaelCipher.CreateDecryptor(secretKey.GetBytes(32), secretKey.GetBytes(16)))
            {
                using (MemoryStream memoryStream = new MemoryStream(encryptedData))
                {
                    using (CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                    {
                        byte[] plainText = new byte[encryptedData.Length];
                        int decryptedCount = cryptoStream.Read(plainText, 0, plainText.Length);
                        return Encoding.Unicode.GetString(plainText, 0, decryptedCount);
                    }
                }
            }
        }

        #endregion

    }
}
