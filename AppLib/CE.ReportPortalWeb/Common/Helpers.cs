﻿using CE.ReportPortalWeb.Models;
using System.Configuration;

namespace CE.ReportPortalWeb.Common
{
    public static class Helpers
    {
        private static EnvironmentType _environment = EnvironmentType.localdev;    // default = 4
        private const string AppSetting_CEEnvironment = "CEEnvironment";
        //private const string BaseURL_CEInsightsLocalDev = "CEInsightsAPIBaseURL";

        //list of headers here with their respective checkboxes that if selected will be included in the request
        public const string DateTime = "X-DateTime";
        // this will always be included for now on rest client api request- on 87 CE authentication
        public const string UsernameHeader = "X-CE-AccessKeyId";
        public const string MessageIdHeader = "X-CE-MessageId";
        public const string ChannelHeader = "X-CE-Channel";
        public const string LocaleHeader = "X-CE-Locale";
        public const string MetaHeader = "X-CE-Meta";  ////on end also include "#cecp:1#" to simulate control panel caller so record would be excluded in reported results


        public static byte GetCurrentEnvironment()
        {
            string s = ConfigurationManager.AppSettings.Get(AppSetting_CEEnvironment);

            if (!string.IsNullOrEmpty(s))
            {
                _environment = (EnvironmentType)System.Enum.Parse(typeof(EnvironmentType), s, true);
            }
            return (byte)_environment;
        }
        
    }
}