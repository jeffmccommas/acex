profileloadtests for  
insert profilepostv1 post multiple keys with validation
update test maxattributes one account and one premise with validate api/v1/profile/post
To test the insert and update for profile webapi v1 post, respectively.


Run soap-ui test CEinsightsRestAPI_DynamicLoadTest_ProfilePost  for when testing an insert with csv file for the customer, account and premiseid
Test case of "insert profilepostv1 post multiple keys with validation"
Change values to unique values each time run (or delete and reinsert if using same files)
csv file is located under \loadtestdata in same directory: file name of \custaccountpremiseforprofileinsert.csv

Test case of "update profilepostv1 post multiple keys with validation", with Same csv file can be used without updating the customer, account and premiseid when testing the  profile webapi post update, where profile attribute values are randomly selected using a datagen step from list or numeric (depending on the type of content).