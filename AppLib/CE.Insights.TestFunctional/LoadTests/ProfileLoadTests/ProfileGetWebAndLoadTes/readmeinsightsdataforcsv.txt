Webtests are to exercise pulling data from insightsdw (datawarehouse) and this then inserts into the insights database .
To do this effectively, data would need to be generated from a sql for each time load test is done against desired environment being tested.


To generate the data for customers that are in insights dw (the pull) and also use for already existing customer in insights database (regular get) with this load test which does
80% of new customers in insightsdw versus 20% of existing customers in insights - get customer ids and generate (repaste into existing databound csv files here):

--NOTE can no longer use dblink since we switched to using Shard dbs without a dblink

Add the following data to insightsdwcustomeridsforgetprofilepull csv file - resulting from this query (include the customerid header):
USE InsightsDW_TEST
GO
SELECT  DISTINCT  TOP 5000  dc.CustomerId ,
					--		fpa.PremiseID,
					--		fpa.AccountID,
					--	ROW_NUMBER() OVER ( PARTITION BY fpa.PremiseKey, OptionKey ORDER BY fpa.EffectiveDate DESC ) AS SortId
					FROM    dbo.FactPremiseAttribute fpa  WITH (NOLOCK)
					INNER JOIN dbo.DimPremise dp ON dp.PremiseKey = fpa.PremiseKey
					INNER JOIN dbo.FactCustomerPremise fcp ON fcp.PremiseKey = dp.PremiseKey
					INNER JOIN dbo.DimCustomer dc ON dc.CustomerKey = fcp.CustomerKey
					INNER JOIN dbo.DimSource ds ON ds.SourceKey = dc.SourceKey
					WHERE     fpa.ClientID = 256
					ORDER BY CustomerID 
					GO


For existing customers use same query as above but as an 'IN' instead of the NOT IN - getting data already in insights.
Add data to (repaste into - insightscustomeridsforgetprofile.csv for the resultset from below sql) 
Note these will need to be continually adjusted depending on environment tests - as it pulls data from insights dw to insights database
		 
USE Insights
GO
 SELECT  DISTINCT  TOP 5000  CustomerId  
					FROM    wh.ProfilePremiseAttribute fpa WITH(NOLOCK)
					INNER JOIN wh.ProfileCustomerAccount acc ON acc.AccountId = fpa.AccountId AND acc.ClientId = fpa.ClientId
				    WHERE     fpa.ClientID = 256
		--WHERE   SortId = 1 
		ORDER BY CustomerID 

