﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CE.Test.Selenium;
using CE.Test.Selenium.PageFactoryTests;
using System.Configuration;

namespace CE.InsightsWebTests
{ 
    [TestClass]
    public class UnitTest1
    {
        //reset the environment on app.config before running test, if changed.
       private string strEnviron = GetConfigAppSetting("CEEnvironment");
       public const string LocaleEnglish = "en-US";
       public const string LocaleSpanish = "es-ES";
       public const string LocaleRussian = "ru-RU";

        [TestMethod, TestCategory("Firefox"), TestCategory("ShortForm"), TestCategory("Tab")]
        public void FF_Tab_ShortForm_Pool()
        {
            var strAccount = "QATestShortPoolCond";
           
            var strClientId = "256";

            LogonPage logonPage = new LogonPage();
            logonPage.Load(Common.GetEnvUrl(strEnviron));
            logonPage.LogonCustomer(strAccount, strEnviron, strClientId);

            //Look for tabkey = 'tab.dashboard'
            var isLoaded = logonPage.IsLoaded;
            Assert.IsTrue(isLoaded,"Dashboard did not load - tabKey tab.dashboard not found on page source");
            logonPage.Close();
        }

        [TestMethod, TestCategory("Chrome"), TestCategory("ShortForm"), TestCategory("Tab")]
        public void Chrome_Tab_ShortForm_Pool()
        {
            var strAccount = "QATestShortPoolCond";
            
            var strClientId = "256";
            LogonPage logonPage = new LogonPage(Common.Browser.Chrome);
            logonPage.Load(Common.GetEnvUrl(strEnviron));
            logonPage.LogonCustomer(strAccount, strEnviron, strClientId);

            //Look for tabkey = 'tab.dashboard'
            var isLoaded = logonPage.IsLoaded;
            Assert.IsTrue(isLoaded, "Dashboard did not load - tabKey tab.dashboard not found on page source");
            var errorTextFound = logonPage.ErrorText;
            logonPage.Close();
        }

        [TestMethod, TestCategory("IE"), TestCategory("ShortForm"), TestCategory("Tab")]
        public void IE_Tab_ShortForm_Pool()
        {
            var strAccount = "QATestShortPoolCond";
          
            var strClientId = "256";
            LogonPage logonPage = new LogonPage(Common.Browser.IE);
            logonPage.Load(Common.GetEnvUrl(strEnviron));
            logonPage.LogonCustomer(strAccount, strEnviron, strClientId);

            //Look for tabkey = 'tab.dashboard'
            var isLoaded = logonPage.IsLoaded;
            Assert.IsTrue(isLoaded, "Dashboard did not load - tabKey tab.dashboard not found on page source");
            logonPage.Close();
        }

        [TestMethod, TestCategory("Firefox"), TestCategory("ShortForm"), TestCategory("Tab")]
        public void FF_Tab_ShortForm_NoPool()
        {
            var strAccount = "QATestShortNoPool";
          
            var strClientId = "256";
            LogonPage logonPage = new LogonPage(Common.Browser.IE);
            logonPage.Load(Common.GetEnvUrl(strEnviron));
            logonPage.LogonCustomer(strAccount, strEnviron, strClientId);

            //Look for tabkey = 'tab.dashboard'
            var isLoaded = logonPage.IsLoaded;
            Assert.IsTrue(isLoaded, "Dashboard did not load - tabKey tab.dashboard not found on page source");
            logonPage.Close();
        }


        [TestMethod, TestCategory("Firefox"), TestCategory("ShortForm"), TestCategory("Tab")]
        public void FF_Widget_ShortForm_NoPool()
        {
            var strAccount = "QATestShortNoPool";
         
            var strClientId = "256";
            var strWidgetType = "dashboard-profile";
           
            Common.Logon(strAccount, strEnviron, strClientId, strWidgetType, LocaleEnglish);

        }

        [TestMethod, TestCategory("IE"), TestCategory("ShortForm"), TestCategory("Widget")]
        public void IE_Widget_ShortForm_NoPool()
        {
            var strAccount = "QATestShortNoPool";
         
            var strClientId = "256";
            var strWidgetType = "tab.dashboard-profile";

            Common.Logon(strAccount, strEnviron, strClientId, strWidgetType, LocaleEnglish);

        }

        [TestMethod, TestCategory("Firefox"), TestCategory("ShortForm"), TestCategory("Widget"), TestCategory("Locale")]
        public void FF_Widget_ShortForm_Spanish_NoPool()
        {
            var strAccount = "QATestShortNoPool";
            
            var strClientId = "256";
            var strWidgetType = "tab.dashboard-profile";

            Common.Logon(strAccount, strEnviron, strClientId, strWidgetType, LocaleSpanish);

        }

        [TestMethod, TestCategory("Firefox"), TestCategory("BillDisagg"), TestCategory("Widget")]
        public void FF_Widget_BillDisagg()
        {
            var strAccount = "QATestShortNoPool";
            
            var strClientId = "256";
            var strWidgetType = "tab.myusage-billdisagg";

            Common.Logon(strAccount, strEnviron, strClientId, strWidgetType, LocaleEnglish);

        }

        /// <summary>
        /// Tests if customer does not have any gas profile attributes , that the 'No gas etc. message displays' as configured in content.
        /// Precondition: customer with no bills at all (i.e. no bills in insightsdw for that premise/account/client)
        /// </summary>
        [TestMethod, TestCategory("Firefox"), TestCategory("BillDisagg"), TestCategory("Widget")]
        public void FF_Widget_BillDisagg_NoBillsNoGasProfileAttributes()
        {
            var strAccount = "QATestShortNoPool";

            var strClientId = "256";
            var strWidgetType = "tab.myusage-billdisagg";
            MyUsagePage myUsagePage = new MyUsagePage();
            myUsagePage.LoadWidget(strAccount, strEnviron, strClientId, strWidgetType, LocaleEnglish);

        }

        /// Tests if customer does not have any gas profile attributes , that the 'No gas etc. message displays' as configured in content.
        /// Precondition: customer with bill loaded into datawarehouse (see data folder and load 'AMConservation_201508251.tab' in preprocess/csvs folder for 256 if not already in the SUT)
        /// </summary>
        [TestMethod, TestCategory("Firefox"), TestCategory("BillDisagg"), TestCategory("Widget")]
        public void FF_Widget_BillDisagg_BillswithNoGasProfileAttributes()
        {
            var strAccount = "99999";

            var strClientId = "256";
            var strWidgetType = "tab.myusage-billdisagg";

            MyUsagePage myUsagePage = new MyUsagePage();
            myUsagePage.LoadWidget(strAccount, strEnviron, strClientId, strWidgetType, LocaleEnglish);

        }

        [TestMethod, TestCategory("Firefox"), TestCategory("BillDisagg"), TestCategory("Widget"), TestCategory("Locale")]
        public void FF_Widget_BillDisagg_Spanish()
        {
            var strAccount = "QATestShortNoPool";
            
            var strClientId = "256";
            var strWidgetType = "tab.myusage-billdisagg";

            Common.Logon(strAccount, strEnviron, strClientId, strWidgetType, LocaleSpanish);

            //go to billdisagg and do some test
        }

        [TestMethod, TestCategory("Firefox"), TestCategory("BillDisagg"), TestCategory("Widget"), TestCategory("Locale")]
        public void FF_Widget_BillDisagg_Russian()
        {
            var strAccount = "QATestShortNoPool";
           
            var strClientId = "256";
            var strWidgetType = "tab.myusage-billdisagg";

            Common.Logon(strAccount, strEnviron, strClientId, strWidgetType, LocaleRussian);

        }

        [TestMethod, TestCategory("IE"), TestCategory("LongForm"), TestCategory("Tab")]
        public void IE_Tab_LongForm()
        {
            var strAccount = "QATestLongPoolCond";

            var strClientId = "256";
            LogonPage logonPage = new LogonPage(Common.Browser.IE);
            logonPage.Load(Common.GetEnvUrl(strEnviron));
            logonPage.LogonCustomer(strAccount, strEnviron, strClientId);

            //Look for tabkey = 'tab.dashboard'
            var isLoaded = logonPage.IsLoaded;
            Assert.IsTrue(isLoaded, "Dashboard did not load - tabKey tab.dashboard not found on page source");
            //navigate to My Savings - Energy Profile , the "Long Form"
            LongFormPage longformPage = logonPage.LongFormLoad();
            longformPage.Load();
            var isLoaded2 = longformPage.IsLoadedSecTitle;
            Assert.AreEqual("Your Home", isLoaded2.ToString());
            //Look for Pool
            var textfound = longformPage.TextFoundPool;
            Assert.IsTrue(textfound, "Pool dropdown not found");
            longformPage.Close();
        }

        [TestMethod, TestCategory("Chrome"), TestCategory("LongForm"), TestCategory("Tab")]
        public void Chrome_Tab_LongForm()
        {
            var strAccount = "QATestLongPoolCond";

            var strClientId = "256";
            LogonPage logonPage = new LogonPage(Common.Browser.Chrome);
            logonPage.Load(Common.GetEnvUrl(strEnviron));
            logonPage.LogonCustomer(strAccount, strEnviron, strClientId);

            //Look for tabkey = 'tab.dashboard'
            var isLoaded = logonPage.IsLoaded;
            //navigate to My Savings - Energy Profile , the "Long Form"
            LongFormPage longformPage = logonPage.LongFormLoad();
            longformPage.Load();
            var isLoaded2 = longformPage.IsLoadedSecTitle;
            Assert.AreEqual("Your Home", isLoaded2.ToString());
            //Look for Pool
            var textfound = longformPage.TextFoundPool;
            Assert.IsTrue(textfound, "Pool dropdown not found");
            longformPage.Close();
        }

        [TestMethod, TestCategory("Opera"), TestCategory("LongForm"), TestCategory("Tab")]
        public void Opera_Tab_LongForm()
        {
            var strAccount = "QATestLongPoolCond";

            var strClientId = "256";
            LogonPage logonPage = new LogonPage(Common.Browser.Opera);
            logonPage.Load(Common.GetEnvUrl(strEnviron));
            logonPage.LogonCustomer(strAccount, strEnviron, strClientId);

            //Look for tabkey = 'tab.dashboard'
            var isLoaded = logonPage.IsLoaded;
            Assert.IsTrue(isLoaded, "Dashboard did not load - tabKey tab.dashboard not found on page source");
            //navigate to My Savings - Energy Profile , the "Long Form"
            LongFormPage longformPage = logonPage.LongFormLoad();
            longformPage.Load();
            var isLoaded2 = longformPage.IsLoadedSecTitle;
            Assert.AreEqual("Your Home", isLoaded2.ToString());
            //Look for Pool
            var textfound = longformPage.TextFoundPool;
            Assert.IsTrue(textfound, "Pool dropdown not found");
            longformPage.Close();
        }

        [TestMethod, TestCategory("Firefox"), TestCategory("LongForm"), TestCategory("Tab")]
        public void FF_Tab_LongForm()
        {
            var strAccount = "QATestLongPoolCond";

            var strClientId = "256";
            LogonPage logonPage = new LogonPage();
            logonPage.Load(Common.GetEnvUrl(strEnviron));
            logonPage.LogonCustomer(strAccount, strEnviron, strClientId);
            //Look for tabkey = 'tab.dashboard'
            var isLoaded = logonPage.IsLoaded;
            Assert.IsTrue(isLoaded, "Dashboard did not load - tabKey tab.dashboard not found on page source");
            //navigate to My Savings - Energy Profile , the "Long Form"  after logonPage/driver initialized
            // Logonpage longformLoad returns the LongForm PageObject
            LongFormPage longformPage = logonPage.LongFormLoad();
            longformPage.Load();
            var isLoaded2 = longformPage.IsLoadedSecTitle;
            Assert.AreEqual("Your Home", isLoaded2.ToString());
            
            longformPage.Close();
        }


        [TestMethod, TestCategory("Firefox"), TestCategory("LongForm"), TestCategory("Widget")]
        public void FF_Widget_LongForm()
        {
             var strAccount = "QATestShortPoolCond";
            
             var strClientId = "256";
             var strWidgetType = "tab.energyprofile-longprofile";
         
            LongFormPage longformPage = new LongFormPage();
            longformPage.LoadWidget(strAccount, strEnviron, strClientId, strWidgetType,LocaleEnglish);
 
            
            var isLoaded = longformPage.IsLoadedSecTitle;
            Assert.AreEqual("Your Home", isLoaded.ToString());
            //Look for Header under image for profile section your home : Your home's heaviest hitters'
            var isLoaded2 = longformPage.ProfileSectionSubtitle;
            Assert.AreEqual("Your home's heaviest hitters", isLoaded2.ToString());
             longformPage.Close();
        }

        [TestMethod, TestCategory("Firefox"), TestCategory("LongForm"), TestCategory("Widget"), TestCategory("Locale")]
        public void FF_Widget_LongForm_Spanish()
        {
            var strAccount = "QATestShortPoolCond";

            var strClientId = "256";
            var strWidgetType = "tab.energyprofile-longprofile";

            LongFormPage longformPage = new LongFormPage();
            longformPage.LoadWidget(strAccount, strEnviron, strClientId, strWidgetType, LocaleSpanish);


            var isLoaded = longformPage.IsLoadedSecTitle;
            Assert.AreEqual("Su casa", isLoaded.ToString());
            //Look for Header under image for profile section your home : Sabías?'
            var isLoaded2 = longformPage.ProfileSectionSubtitle;
            Assert.AreEqual("Sabías?", isLoaded2.ToString());
            longformPage.Close();
        }

        [TestMethod, TestCategory("IE"), TestCategory("LongForm"), TestCategory("Widget")]
        public void IE_Widget_LongForm()
        {
            var strAccount = "QATestLongPoolCond";

            var strClientId = "256";
            var strWidgetType = "tab.energyprofile-longprofile";
            LongFormPage longformPage = new LongFormPage(Common.Browser.IE);
            longformPage.LoadWidget(strAccount, strEnviron, strClientId, strWidgetType, LocaleEnglish);


            var isLoaded = longformPage.IsLoadedSecTitle;
            Assert.AreEqual("Your Home", isLoaded.ToString());
            //Look for Header under image for profile section your home : Your home's heaviest hitters'
            var isLoaded2 = longformPage.ProfileSectionSubtitle;
            Assert.AreEqual("Your home's heaviest hitters", isLoaded2.ToString());
            longformPage.Close();
        }

        [TestMethod, TestCategory("Opera"), TestCategory("LongForm"), TestCategory("Widget")]
        public void Opera_Widget_LongForm()
        {
            var strAccount = "QATestLongPoolCond";

            var strClientId = "256";
            var strWidgetType = "tab.energyprofile-longprofile";
            LongFormPage longformPage = new LongFormPage(Common.Browser.Opera);
            longformPage.LoadWidget(strAccount, strEnviron, strClientId, strWidgetType, LocaleEnglish);


            var isLoaded = longformPage.IsLoadedSecTitle;
            Assert.AreEqual("Your Home", isLoaded.ToString());
            //Look for Header under image for profile section your home : Your home's heaviest hitters'
            var isLoaded2 = longformPage.ProfileSectionSubtitle;
            Assert.AreEqual("Your home's heaviest hitters", isLoaded2.ToString());
            longformPage.Close();
        }


        [TestMethod, TestCategory("IE"), TestCategory("BillSummary"), TestCategory("Tab")]
        public void IE_Tab_BillSummary()
        {
            var strAccount = "QATestLongPoolCond";

            var strClientId = "256";
            LogonPage logonPage = new LogonPage(Common.Browser.IE);
            logonPage.Load(Common.GetEnvUrl(strEnviron));
            logonPage.LogonCustomer(strAccount, strEnviron, strClientId);

            //Look for tabkey = 'tab.dashboard'
            var isLoaded = logonPage.IsLoaded;
            Assert.IsTrue(isLoaded, "Dashboard did not load - tabKey tab.dashboard not found on page source");
            //navigate to My Bill -  the "BillSummary"
            MyBillPage mybillPage = logonPage.MyBillLoad();
            mybillPage.Load();
            var isLoaded2 = mybillPage.IsLoadedSecTitle;
           
            mybillPage.Close();
        }

        [TestMethod, TestCategory("FF"), TestCategory("BillSummary"), TestCategory("Tab")]
        public void FF_Tab_BillSummary()
        {
            var strAccount = "QATestLongPoolCond";

            var strClientId = "256";
            LogonPage logonPage = new LogonPage();
            logonPage.Load(Common.GetEnvUrl(strEnviron));
            logonPage.LogonCustomer(strAccount, strEnviron, strClientId);

            //Look for tabkey = 'tab.dashboard'
            var isLoaded = logonPage.IsLoaded;
            Assert.IsTrue(isLoaded, "Dashboard did not load - tabKey tab.dashboard not found on page source");
            //navigate to My Bill -  the "BillSummary"
            MyBillPage mybillPage = logonPage.MyBillLoad();
            mybillPage.Load();
            var isLoaded2 = mybillPage.IsLoadedSecTitle;

            mybillPage.Close();
        }

        [TestMethod, TestCategory("Firefox"), TestCategory("BillSummary"), TestCategory("Widget")]
        public void FF_Widget_BillSummary_NoPool()
        {
            var strAccount = "QATestShortNoPool";
           
            var strClientId = "256";
            var strWidgetType = "tab.billstatement-billsummary";

           
             MyBillPage myBillPage = new MyBillPage();
             myBillPage.LoadWidget(strAccount, strEnviron, strClientId, strWidgetType, LocaleEnglish);
        }

        [TestMethod, TestCategory("Firefox"), TestCategory("BillHistory"), TestCategory("Widget")]
        public void FF_Widget_BillHistory_AllCommodities()
        {
            var strAccount = "99999";

            var strClientId = "87";
            var strWidgetType = "tab.billhistory-billhistory";


            MyBillPage myBillPage = new MyBillPage();
            myBillPage.LoadWidget(strAccount, strEnviron, strClientId, strWidgetType, LocaleEnglish);

            Assert.IsTrue(myBillPage.IsLoadedCommoditySubhead.ToString().Contains("Electric"));
            Assert.IsTrue(myBillPage.IsLoadedCommoditySubhead.ToString().Contains("Gas"));
            Assert.IsTrue(myBillPage.IsLoadedCommoditySubhead.ToString().Contains("Water"));
            myBillPage.Close();
        }


        [TestMethod, TestCategory("Firefox"), TestCategory("BillHistory"), TestCategory("Widget")]
        public void FF_Widget_BillHistory_CustomerNoBill()
        {
            var strAccount = "QATestProfileNoBill";

            var strClientId = "256";
            var strWidgetType = "tab.billhistory-billhistory";


            MyBillPage myBillPage = new MyBillPage();
            myBillPage.LoadWidget(strAccount, strEnviron, strClientId, strWidgetType, LocaleEnglish);
            var strNoBill = myBillPage.IsLoadedNoBillText.ToString();
            Assert.AreEqual("No bills are available.", strNoBill);
            myBillPage.Close();
        }

        /// <summary>
        /// Verify Compare link where the content configuration is NOT set to display for Action column for clientid 256
        /// </summary>
        [TestMethod, TestCategory("Firefox"), TestCategory("BillHistory"), TestCategory("Widget")]
        public void FF_Widget_BillHistory_ActionsCompareLinkNotDisplayed()
        {
            var strAccount = "99999";

            var strClientId = "256";
            var strWidgetType = "tab.billhistory-billhistory";


            MyBillPage myBillPage = new MyBillPage();
            myBillPage.LoadWidget(strAccount, strEnviron, strClientId, strWidgetType, LocaleEnglish);
            var strNoCompareLink = myBillPage.IsLoadedActionHeadFirstCompareLink.ToString();
            Assert.IsFalse(strNoCompareLink.ToString().Contains("Compare"));
            myBillPage.Close();
        }



        /// <summary>
        /// Verify 15th (last row) Compare link not there where the content configuration is set to display for Action column for clientid 87
        /// </summary>
        [TestMethod, TestCategory("Firefox"), TestCategory("BillHistory"), TestCategory("Tab")]
        public void FF_Tab_BillHistory_ActionsLastRowCompareLinkNotDisplayed()
        {
            var strCustomer = "993774751";
            var strAccount = "1218337093";
            var strPremise = "1218337000";

            var strClientId = "87";
         
            LogonPage logonPage = new LogonPage();
            logonPage.Load(Common.GetEnvUrl(strEnviron));
            logonPage.LogonCustomer(strCustomer, strAccount, strPremise, "localdev" , strClientId);

            //Look for tabkey = 'tab.dashboard'
            var isLoaded = logonPage.IsLoaded;
            Assert.IsTrue(isLoaded, "Dashboard did not load - tabKey tab.dashboard not found on page source");
            //navigate to My Bill -  click on  "BillHistory subnav"
            MyBillPage mybillPage = logonPage.MyBillLoad();
            mybillPage.LoadBH();


            var strNoCompareLink = mybillPage.IsLoadedActionHeadLastCompareLink.ToString();
            Assert.IsFalse(strNoCompareLink.ToString().Contains("Compare"));
            mybillPage.Close();
        }
        /// <summary>
        /// Verify Compare link where the content configuration is set to display for Action column for clientid 87
        /// </summary>
        [TestMethod, TestCategory("Firefox"), TestCategory("BillHistory"), TestCategory("Tab")]
        public void FF_Tab_BillHistory_ActionsCompareLinkIsDisplayed()
        {
            var strAccount = "99999";

            var strClientId = "87";
            LogonPage logonPage = new LogonPage();
            logonPage.Load(Common.GetEnvUrl(strEnviron));
            logonPage.LogonCustomer(strAccount, strEnviron, strClientId);

            //Look for tabkey = 'tab.dashboard'
            var isLoaded = logonPage.IsLoaded;
            Assert.IsTrue(isLoaded, "Dashboard did not load - tabKey tab.dashboard not found on page source");
            //navigate to My Bill -  click on  "BillHistory subnav"
            MyBillPage mybillPage = logonPage.MyBillLoad();
            mybillPage.LoadBH();

            var strNoCompareLink = mybillPage.IsLoadedActionHeadFirstCompareLink.ToString();
            Assert.IsTrue(strNoCompareLink.ToString().Contains("Compare"));
            mybillPage.Close();
        }

        [TestMethod, TestCategory("Firefox"), TestCategory("BillHistory"), TestCategory("Widget")]
        public void FF_Widget_BillHistory_ElecOnlyCommodity()
        {
            var strCustomer = "LIN8987";
            var strAccount = "VAV29904";
            var strPremise = "HAX67067";

            var strClientId = "256";
            var strWidgetType = "tab.billhistory-billhistory";


            MyBillPage myBillPage = new MyBillPage();
            myBillPage.LoadWidget(strCustomer, strAccount, strPremise, strEnviron, strClientId, strWidgetType, LocaleEnglish);

            Assert.IsTrue(myBillPage.IsLoadedCommoditySubhead.ToString().Contains("Electric"));
            Assert.IsFalse(myBillPage.IsLoadedCommoditySubhead.ToString().Contains("Gas"));
            Assert.IsFalse(myBillPage.IsLoadedCommoditySubhead.ToString().Contains("Water"));
            myBillPage.Close();
        }

        [TestMethod, TestCategory("Firefox"), TestCategory("BillHistory"), TestCategory("Widget"), TestCategory("webtoken")]
        public void FF_Widget_BillHistory_Incorrectwebtoken()
        {
            var strCustomer = "993774751";
            var strAccount = "1218337093";
            var strPremise = "1218337000";

            var strClientId = "87";
            var strWidgetType = "tab.billhistory-billhistory";


            MyBillPage myBillPage = new MyBillPage();
            myBillPage.LoadWidget(strCustomer, strAccount, strPremise, strEnviron, strClientId, strWidgetType, LocaleEnglish);

            Assert.IsTrue(myBillPage.IsLoadedCommoditySubhead.ToString().Contains("Electric"));
           
            myBillPage.Close();
        }



        [TestMethod, TestCategory("FF"), TestCategory("BillHistory"), TestCategory("Tab")]
        public void FF_Tab_BillHistory_AllCommodities()
        {
            var strAccount = "99999";

            var strClientId = "87";
            LogonPage logonPage = new LogonPage();
            logonPage.Load(Common.GetEnvUrl(strEnviron));
            logonPage.LogonCustomer(strAccount, strEnviron, strClientId);

            //Look for tabkey = 'tab.dashboard'
            var isLoaded = logonPage.IsLoaded;
            Assert.IsTrue(isLoaded, "Dashboard did not load - tabKey tab.dashboard not found on page source");
            //navigate to My Bill -  click on  "BillHistory subnav"
            MyBillPage mybillPage = logonPage.MyBillLoad();
            mybillPage.LoadBH();
            Assert.IsTrue(mybillPage.IsLoadedBH);
            Assert.IsTrue(mybillPage.IsLoadedCommoditySubhead.ToString().Contains("Electric"));
            Assert.IsTrue(mybillPage.IsLoadedCommoditySubhead.ToString().Contains("Gas"));
            Assert.IsTrue(mybillPage.IsLoadedCommoditySubhead.ToString().Contains("Water"));
            mybillPage.Close();
        }

        [TestMethod, TestCategory("Opera"), TestCategory("BillHistory"), TestCategory("Tab")]
        public void Opera_Tab_BillHistory_AllCommodities()
        {
            var strAccount = "99999";

            var strClientId = "87";
            LogonPage logonPage = new LogonPage(Common.Browser.Opera);
            logonPage.Load(Common.GetEnvUrl(strEnviron));
            logonPage.LogonCustomer(strAccount, strEnviron, strClientId);

            //Look for tabkey = 'tab.dashboard'
            var isLoaded = logonPage.IsLoaded;
            Assert.IsTrue(isLoaded, "Dashboard did not load - tabKey tab.dashboard not found on page source");
            //navigate to My Bill -  click on  "BillHistory subnav"
            MyBillPage mybillPage = logonPage.MyBillLoad();
            mybillPage.LoadBH();
            var isLoaded2 = mybillPage.IsLoadedBH;
            Assert.IsTrue(isLoaded2);
            Assert.IsTrue(mybillPage.IsLoadedCommoditySubhead.ToString().Contains("Electric"));
            Assert.IsTrue(mybillPage.IsLoadedCommoditySubhead.ToString().Contains("Gas"));
            Assert.IsTrue(mybillPage.IsLoadedCommoditySubhead.ToString().Contains("Water"));
            mybillPage.Close();
        }
        

        [TestMethod, TestCategory("Firefox"), TestCategory("Actions"), TestCategory("Widget")]
        public void FF_Widget_WaystoSave_SavingsGoal() 
        {
            var strAccount = "QATestactionplanauto";

            var strClientId = "256";
            var strWidgetType = "tab.waystosave-setgoal";


            ActionsPage actionsPage = new ActionsPage();
            actionsPage.LoadWidget(strAccount, strEnviron, strClientId, strWidgetType, LocaleEnglish);
            //look for title set goal
            var isLoaded = actionsPage.ActionsSectiontitle;
            Assert.AreEqual("Set Goal", isLoaded.ToString());
            actionsPage.Close();
        }

        [TestMethod, TestCategory("Firefox"), TestCategory("Actions"), TestCategory("Widget"), TestCategory("Locale")]
        public void FF_Widget_WaystoSave_SavingsGoal_Spanish()
        {
            var strAccount = "QATestactionplanauto";

            var strClientId = "256";
            var strWidgetType = "tab.waystosave-setgoal";


            ActionsPage actionsPage = new ActionsPage();
            actionsPage.LoadWidget(strAccount, strEnviron, strClientId, strWidgetType, LocaleSpanish);
            var isLoaded = actionsPage.ActionsSectiontitle;
            Assert.AreEqual("Set Goal", isLoaded.ToString());
            actionsPage.Close();
        }
        [TestMethod, TestCategory("Firefox"), TestCategory("Actions"), TestCategory("Widget")]
        public void FF_Widget_WaystoSave_Actions()
        {
            var strAccount = "QATestactionplanauto";

            var strClientId = "256";
            var strWidgetType = "tab.waystosave-actions";


            ActionsPage actionsPage = new ActionsPage();
            actionsPage.LoadWidget(strAccount, strEnviron, strClientId, strWidgetType, LocaleEnglish);
           // actionsPage.Close();
        }

        [TestMethod, TestCategory("Firefox"), TestCategory("ActionPlan"), TestCategory("Widget")]
        public void FF_Widget_MyPlan_ActionPlan()
        {
            var strAccount = "QATestactionplanauto";

            var strClientId = "256";
            var strWidgetType = "tab.myplan-actionplan";


            ActionsPage actionsPage = new ActionsPage();
            actionsPage.LoadWidget(strAccount, strEnviron, strClientId, strWidgetType, LocaleEnglish);
            // actionsPage.Close();
        }


        [TestMethod, TestCategory("Firefox"), TestCategory("ActionPlan"), TestCategory("Widget"), TestCategory("Locale")]
        public void FF_Widget_MyPlan_ActionPlan_Spanish()
        {
            var strAccount = "QATestactionplanauto";

            var strClientId = "256";
            var strWidgetType = "tab.myplan-actionplan";


            ActionsPage actionsPage = new ActionsPage();
            actionsPage.LoadWidget(strAccount, strEnviron, strClientId, strWidgetType, LocaleSpanish);
            // actionsPage.Close();
        }

        [TestMethod, TestCategory("Firefox"), TestCategory("Promo"), TestCategory("Widget")]
        public void FF_Widget_DashboardPromo()
        {
            var strAccount = "QATestActionPromoCond";

            var strClientId = "256";
            var strWidgetType = "tab.dashboard-promo";

            Common.Logon(strAccount, strEnviron, strClientId, strWidgetType, LocaleEnglish);

        }

        [TestMethod, TestCategory("IE"), TestCategory("Promo"), TestCategory("Widget")]
        public void IE_Widget_DashboardPromo()
        {
            var strAccount = "QATestActionPromoCond";

            var strClientId = "256";
            var strWidgetType = "tab.dashboard-promo";
            LogonPage logonpage = new LogonPage(Common.Browser.IE);
            logonpage.LoadWidget(strAccount, strEnviron, strClientId, strWidgetType, LocaleEnglish);

        }

        [TestMethod, TestCategory("Opera"), TestCategory("Promo"), TestCategory("Widget")]
        public void Opera_Widget_DashboardPromo()
        {
            var strAccount = "QATestActionPromoCond";

            var strClientId = "256";
            var strWidgetType = "tab.dashboard-promo";
            LogonPage logonpage = new LogonPage(Common.Browser.Opera);
            logonpage.LoadWidget(strAccount, strEnviron, strClientId, strWidgetType, LocaleEnglish);

        }

        [TestMethod, TestCategory("Chrome"), TestCategory("Promo"), TestCategory("Widget")]
        public void Chrome_Widget_DashboardPromo()
        {
            var strAccount = "QATestActionPromoCond";

            var strClientId = "256";
            var strWidgetType = "tab.dashboard-promo";
            LogonPage logonpage = new LogonPage(Common.Browser.Chrome);
            logonpage.LoadWidget(strAccount, strEnviron, strClientId, strWidgetType, LocaleEnglish);


        }


        public static string GetConfigAppSetting(string keySetting)
        {
            var appSetting = ConfigurationManager.AppSettings[keySetting];
            return appSetting;
        }

    }
}
