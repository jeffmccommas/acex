﻿using System;
using System.Configuration;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Opera;
using OpenQA.Selenium.Safari;

namespace CE.Test.Selenium
{
    public static class Common
    {
     

        public enum Browser
        {
            Chrome,
            Firefox,
            IE,
            Opera,
            Safari
        }
 
        //when driver is already initiated as part of previous 'page' no need to init , otherwise go through normal driver, new Page process
        public enum DriverInit
        {
            InitNo,
            InitYes
        }


        public static void Logon(string AccountInfo, string TestEnvironment, string ClientId)
        {
            IWebDriver driver = new FirefoxDriver();
            //driver.FindElement(ByID)
            var _url = GetEnvUrl(TestEnvironment);
            driver.Navigate().GoToUrl(_url);
            driver.Manage().Window.Maximize();
            //send keys with specified customer/account/premise info (all same at this point) and locale
            var input = driver.FindElement(By.Id("txtCustomer"));
            input.Clear();
            input.SendKeys(AccountInfo);
            var input2 = driver.FindElement(By.Id("txtClient"));
            input2.Clear();
            input2.SendKeys(ClientId);
            var input3 = driver.FindElement(By.Id("txtAccount"));
            input3.Clear();
            input3.SendKeys(AccountInfo);
            var input4= driver.FindElement(By.Id("txtPremise"));
            input4.Clear();
            input4.SendKeys(AccountInfo);
            var btn = driver.FindElement(By.Id("btnLogin"));
            btn.Click();

        }

        public static void Logon(string AccountInfo, string TestEnvironment, string ClientId, string WidgetType, string Locale)
        {
            IWebDriver driver = new FirefoxDriver();
            var _url = GetEnvUrl(TestEnvironment, WidgetType);
            var _webtoken = GetWebToken(Convert.ToInt32(ClientId), AccountInfo);
            //sample url for widget: https://acewebsitedev.aclarax.com/Widget?id=tab.myusage-billdisagg&ClientId=87&CustomerId=993774751&AccountId=1218337093&PremiseId=1218337000&ServicePointId=&WebToken=ac0f5c55-b039-49ce-8aad-30d5d54b0cf1&Locale=en-US
            _url += "&ClientId=" + ClientId + "&CustomerId=" + AccountInfo + "&AccountId=" + AccountInfo + "&PremiseId=" + AccountInfo + "&ServicePointId=&WebToken=" + _webtoken + "&Locale=" + Locale;//if no locale then default is en-US
            driver.Navigate().GoToUrl(_url);
            driver.Manage().Window.Maximize();
            //send keys with specified customer/account/premise info (all same at this point) and locale
        }

        public static string GetEnvUrl(string TestEnvironment)
        {
            if (TestEnvironment == "dev")
            {
                string strUrl = "https://acewebsitedev.aclarax.com";
                return strUrl;
            }
            else if (TestEnvironment == "qa")
            {
                string strUrl = "https://acewebsiteqa.aclarax.com/Web/Login";
                return strUrl;
            }
            else if (TestEnvironment == "uat")
            {
                string strUrl = "https://acewebsiteuat.aclarax.com/Web/Login";
                return strUrl;
            }
            else if (TestEnvironment == "localdev")
            {
                string strUrl = "https://lefkowitz7.energyguide.com/CE.InsightsWeb/Login";
                return strUrl;
            }
            else
            {
                throw new ArgumentException("invalid environment");
            }
        }

        public static string GetEnvUrl(string TestEnvironment, String WidgetType)
        {
            if (TestEnvironment == "dev")
            {
                string strUrl = "https://acewebsitedev.aclarax.com/Widget?id=" + WidgetType;
                return strUrl;
            }
            else if (TestEnvironment == "qa")
            {
                string strUrl = "https://acewebsiteqa.aclarax.com/Widget?id=" + WidgetType;
                return strUrl;
            }
            else if (TestEnvironment == "uat")
            {
                string strUrl = "https://acewebsiteuat.aclarax.com/Widget?id=" + WidgetType;
                return strUrl;
            }
            else if (TestEnvironment == "localdev")
            {
                string strUrl = "https://lefkowitz7.energyguide.com/CE.InsightsWeb/Widget?id=" + WidgetType;
                return strUrl;
            }
            else
            {
                throw new ArgumentException("invalid environment");
            }
        }

        public static string GetWebToken(int ClientId, string Custid)
        {
            //call webtoken api and get back webtoken to use in widget
            return "257e4261-8512-47bd-aaf5-8d729c0d384d";
        }

        public static bool IsElementPresent(By by, IWebDriver driver)
        {
            try
            {
                driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }


        public static void QuitDriver(IWebDriver driver)
        {
            driver.Quit();
        }

        public static string TestNewWidget()
        {
         IWebDriver driver;
            driver = new FirefoxDriver();
            driver.Manage().Timeouts().ImplicitlyWait(new TimeSpan(0, 0, 0, 10)); // 10 seconds
            var urlto = "https://lefkowitz7.energyguide.com/CE.InsightsWeb/Widget?id=tab.energyprofile-longprofile&ClientId=256&CustomerId=QATestShortPoolCond&AccountId=QATestShortPoolCond&PremiseId=QATestShortPoolCond&ServicePointId=&WebToken=e8c7c62a-a4e7-4ec9-b2de-d8831ae159da&Locale=en-US";
            driver.Navigate().GoToUrl(urlto);

 
           
        
            // verifyText | id=iws_lp_tab-0 | Your Home
             var x = driver.FindElement(By.Id("iws_lp_tab-0")).Text;
            return x;

    }
        public static IWebDriver GetWebDriverForBrowser(Browser browser)
        {
            IWebDriver driver = null;
            var driverLocation = ReadSetting("SeleniumDriverLocation");
            switch (browser)
            {
                case Browser.Chrome:
                   // System.setProperty("webdriver.chrome.driver", "C:\Program Files (x86)\Google\Chrome\Application\chrome.exe");
                    driver = new ChromeDriver(@driverLocation + "\\chromedriver_win32");
                    break;

                case Browser.Firefox:
                    driver = new FirefoxDriver();
                    break;

                case Browser.IE:
                    driver = new InternetExplorerDriver(@driverLocation + "\\IEDriverServer_Win32_2.45.0");
                    break;

                case Browser.Opera:
                    driver = new OperaDriver(@driverLocation + "\\operadriver_win32");
                    break;

                case Browser.Safari:
                    driver = new SafariDriver();
                    break;

            }

            return driver;
        }

        public static string ReadSetting(string key)
        {
            try
            {
                var appSettings = ConfigurationManager.AppSettings;
                string result = appSettings[key] ?? "Not Found";
                return result;
            }
            catch (ConfigurationErrorsException)
            {
                throw;
            }
        }

    }
}
