﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Opera;
using OpenQA.Selenium.Chrome;

namespace CE.Test.Selenium.PageFactoryTests
{
    public class MyUsagePage
    {
        private static string tabKey = "tabKey = 'tab.billstatement'";
        private static string tabkeyBH = "tabKey = 'tab.billhistory'";
        private IWebDriver driver;

        public MyUsagePage()
        {
            driver = new FirefoxDriver();
            driver.Manage().Timeouts().ImplicitlyWait(new TimeSpan(0, 0, 0, 10)); // 10 seconds
        }

        public MyUsagePage(IWebDriver driver)
        {
            this.driver = driver;
            driver.Manage().Timeouts().ImplicitlyWait(new TimeSpan(0, 0, 0, 10)); // 10 seconds

        }


        public MyUsagePage(Common.Browser browser)
        {
            driver = Common.GetWebDriverForBrowser(browser);
            driver.Manage().Timeouts().ImplicitlyWait(new TimeSpan(0, 0, 0, 10)); // 10 seconds

        }
        public void Load()
        {
            // MyBill.Click();
            driver.Manage().Timeouts().ImplicitlyWait(new TimeSpan(0, 0, 0, 10)); // 10 seconds
            driver.FindElement(By.LinkText("My Usage")).Click();

        }


        public void LoadWidget(string AccountInfo, string TestEnvironment, string ClientId, string WidgetType, string Locale)
        {
            var _url = Common.GetEnvUrl(TestEnvironment, WidgetType);
            var _webtoken = Common.GetWebToken(Convert.ToInt32(ClientId), AccountInfo);
            _url += "&ClientId=" + ClientId + "&CustomerId=" + AccountInfo + "&AccountId=" + AccountInfo + "&PremiseId=" + AccountInfo + "&ServicePointId=&WebToken=" + _webtoken + "&Locale=" + Locale;//if no locale then default is en-US
            driver.Navigate().GoToUrl(_url);
        }

        public void LoadWidget(string Customer, string Account, string Premise, string TestEnvironment, string ClientId, string WidgetType, string Locale)
        {
            var _url = Common.GetEnvUrl(TestEnvironment, WidgetType);
            var _webtoken = Common.GetWebToken(Convert.ToInt32(ClientId), Customer);
            _url += "&ClientId=" + ClientId + "&CustomerId=" + Customer + "&AccountId=" + Account + "&PremiseId=" + Premise + "&ServicePointId=&WebToken=" + _webtoken + "&Locale=" + Locale;//if no locale then default is en-US
            driver.Navigate().GoToUrl(_url);
        }

        public void Close()
        {
            driver.Close();
        }

        public bool IsLoaded
        {
            get { return driver.PageSource.Contains(tabKey); }
        }


        public string IsLoadedSecTitle
        {
            get
            {
                {
                    return driver.FindElement(By.Id("iws_header_title")).Text; ;
                }
            }
        }


        public string IsFoundNoBillMessage
        {
            get
            {
                {
                    return driver.FindElement(By.Id("iws_bh_billtable")).Text; ;
                }
            }
        }
    }
}

