﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
 using OpenQA.Selenium.Firefox;
//using OpenQA.Selenium.IE;
//using OpenQA.Selenium.Chrome;
//using OpenQA.Selenium.Opera;
//using OpenQA.Selenium.Safari;

namespace CE.Test.Selenium.PageFactoryTests
{
    public class LogonPage
    {
        private static string tabKey = "tabKey = 'tab.dashboard'";

        [FindsBy(How = How.Id, Using = "txtCustomer")]
        private IWebElement CustomerIdField { get; set; }

        [FindsBy (How= How.Id, Using = "txtClient")]
        private IWebElement ClientIdField { get; set; }

        [FindsBy(How = How.Id, Using = "txtAccount")]
        private IWebElement AccountIdField { get; set; }

        [FindsBy(How = How.Id, Using = "txtPremise")]
        private IWebElement PremiseIdField { get; set; }

        [FindsBy(How = How.Id, Using = "txtLocale")]
        private IWebElement LocaleField { get; set; }

        [FindsBy(How = How.Id, Using = "btnLogin")]
        private IWebElement LoginBtnField { get; set; }

        [FindsBy(How = How.Id, Using = "iws_pr_error_text")]
        private IWebElement ErrorTextField { get; set; }

        private IWebDriver driver;

        public LogonPage()
        {
            driver = new FirefoxDriver();
            driver.Manage().Timeouts().ImplicitlyWait(new TimeSpan(0, 0, 0, 10)); // 10 seconds
            PageFactory.InitElements(driver, this);
        }


        public LogonPage(Common.Browser browser)
        {
            driver = Common.GetWebDriverForBrowser(browser);
            driver.Manage().Timeouts().ImplicitlyWait(new TimeSpan(0, 0, 0, 10)); // 10 seconds
            PageFactory.InitElements(driver, this);
        }
        public void Load(String urlLogon)
        {
            driver.Navigate().GoToUrl(urlLogon);
        }


        public void LoadWidget(string AccountInfo, string TestEnvironment, string ClientId, string WidgetType, string Locale)
        {
            var _url = Common.GetEnvUrl(TestEnvironment, WidgetType);
            var _webtoken = Common.GetWebToken(Convert.ToInt32(ClientId), AccountInfo);
            _url += "&ClientId=" + ClientId + "&CustomerId=" + AccountInfo + "&AccountId=" + AccountInfo + "&PremiseId=" + AccountInfo + "&ServicePointId=&WebToken=" + _webtoken + "&Locale=" + Locale;//if no locale then default is en-US
            driver.Navigate().GoToUrl(_url);

        }

        public void Close()
        {
            driver.Close();
        }

        public bool IsLoaded
        {
            get { return driver.PageSource.Contains(tabKey); }
        }

        public void LogonCustomer(String strAccount, String strEnviron, String strClientId)
        {
            //send keys and click submit button here   
            var _url = Common.GetEnvUrl(strEnviron);
            driver.Navigate().GoToUrl(_url);
            driver.Manage().Window.Maximize();
            //send keys with specified customer/account/premise info (all same at this point) and locale
            CustomerIdField.Clear();
            CustomerIdField.SendKeys(strAccount);
            ClientIdField.Clear();
            ClientIdField.SendKeys(strClientId);
            AccountIdField.Clear();
            AccountIdField.SendKeys(strAccount);
            PremiseIdField.Clear();
            PremiseIdField.SendKeys(strAccount);
            LoginBtnField.SendKeys(Keys.Enter);
           // LoginBtnField.Click();
         
        }

        public void LogonCustomer(String strCustomer, String strAccount, String strPremise, String strEnviron, String strClientId)
        {
            //send keys and click submit button here   
            var _url = Common.GetEnvUrl(strEnviron);
            driver.Navigate().GoToUrl(_url);
            driver.Manage().Window.Maximize();
            //send keys with specified customer/account/premise info (all same at this point) and locale
            CustomerIdField.Clear();
            CustomerIdField.SendKeys(strCustomer);
            ClientIdField.Clear();
            ClientIdField.SendKeys(strClientId);
            AccountIdField.Clear();
            AccountIdField.SendKeys(strAccount);
            PremiseIdField.Clear();
            PremiseIdField.SendKeys(strPremise);
            LoginBtnField.SendKeys(Keys.Enter);
            // LoginBtnField.Click();

        }

        // click on my savings then energy profile sub nav to get to long form
        public LongFormPage LongFormLoad()
        {
            return new LongFormPage(driver);
        }

        // click on my savings then energy profile sub nav to get to long form
        public MyBillPage MyBillLoad()
        {
            return new MyBillPage(driver);
        }

        // click on my savings then waystosave sub nav to get to setgoal and actions
        public ActionsPage ActionsLoad()
        {
            return new ActionsPage(driver);
        }

        public String CustomerId
        {
            get { return CustomerIdField.GetAttribute("value"); }
        }

        public String ErrorText
        {
            get { return ErrorTextField.GetAttribute("value"); }
        }

    }
}
