﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Opera;
using OpenQA.Selenium.Chrome;

namespace CE.Test.Selenium.PageFactoryTests
{
    public class MyBillPage
    {
        private static string tabKey = "tabKey = 'tab.billstatement'";
        private static string tabkeyBH = "tabKey = 'tab.billhistory'";
        private IWebDriver driver;

        public MyBillPage()
        {
            driver = new FirefoxDriver();
            driver.Manage().Timeouts().ImplicitlyWait(new TimeSpan(0, 0, 0, 10)); // 10 seconds
        }

        public MyBillPage(IWebDriver driver)
        {
            this.driver = driver;
            driver.Manage().Timeouts().ImplicitlyWait(new TimeSpan(0, 0, 0, 10)); // 10 seconds

        }


        public MyBillPage(Common.Browser browser)
        {
            driver = Common.GetWebDriverForBrowser(browser);
            driver.Manage().Timeouts().ImplicitlyWait(new TimeSpan(0, 0, 0, 10)); // 10 seconds

        }
        public void Load()
        {
            // MyBill.Click();
            driver.Manage().Timeouts().ImplicitlyWait(new TimeSpan(0, 0, 0, 10)); // 10 seconds
            driver.FindElement(By.LinkText("My Bills")).Click();

        }

        public void LoadBH()
        {
            driver.Manage().Timeouts().ImplicitlyWait(new TimeSpan(0, 0, 0, 120)); // 120 seconds
            // click | link=My Bills | 
            driver.FindElement(By.CssSelector("ul li a[title='" + "My Bills" + "']")).Click();
            // click | link=Bill History | 
            driver.Manage().Timeouts().ImplicitlyWait(new TimeSpan(0, 0, 0, 120)); // 120 seconds
           // driver.FindElement(By.CssSelector("ul li a[title='" + "BillHistory" + "']")).Click();
            driver.FindElement(By.CssSelector("#tab_110 > a:nth-child(1)")).Click();
           // driver.FindElement(By.LinkText("Bill History")).Click();


        }

        public void LoadWidget(string AccountInfo, string TestEnvironment, string ClientId, string WidgetType, string Locale)
        {
            var _url = Common.GetEnvUrl(TestEnvironment, WidgetType);
            var _webtoken = Common.GetWebToken(Convert.ToInt32(ClientId), AccountInfo);
            _url += "&ClientId=" + ClientId + "&CustomerId=" + AccountInfo + "&AccountId=" + AccountInfo + "&PremiseId=" + AccountInfo + "&ServicePointId=&WebToken=" + _webtoken + "&Locale=" + Locale;//if no locale then default is en-US
            driver.Navigate().GoToUrl(_url);
        }

        public void LoadWidget(string Customer, string Account, string Premise, string TestEnvironment, string ClientId, string WidgetType, string Locale)
        {
            var _url = Common.GetEnvUrl(TestEnvironment, WidgetType);
            var _webtoken = Common.GetWebToken(Convert.ToInt32(ClientId), Customer);
            _url += "&ClientId=" + ClientId + "&CustomerId=" + Customer + "&AccountId=" + Account + "&PremiseId=" + Premise + "&ServicePointId=&WebToken=" + _webtoken + "&Locale=" + Locale;//if no locale then default is en-US
            driver.Navigate().GoToUrl(_url);
        }

        public void Close()
        {
            driver.Close();
        }

        public bool IsLoaded
        {
            get { return driver.PageSource.Contains(tabKey); }
        }


        public bool IsLoadedBH
        {
            get { return driver.PageSource.Contains(tabkeyBH); }
        }

        public string IsLoadedSecTitle
        {
            get
            {
                {
                    return driver.FindElement(By.Id("iws_lp_tab-0")).Text; ;
                }
            }
        }

      
        public string IsLoadedCommoditySubhead
        {
            get
            {
                {
                    return driver.FindElement(By.Id("iws_bh_billtable")).Text; ;
                }
            }
        }

        //15th row, last item td should not contain compare
        public string IsLoadedActionHeadLastCompareLink
        {
            get
            {
                {
                    return driver.FindElement(By.CssSelector("#iws_bh_billtable > tbody:nth-child(2) > tr:nth-child(15) > td:nth-child(6)")).Text; ;
                }
            }
        }

        //compare link found
        public string IsLoadedActionHeadFirstCompareLink
        {
            get
            {
                {
                    return driver.FindElement(By.Id("iws_bh_billtable")).Text;
                }
            }
        }

        public string IsLoadedNoBillText
        {
            get
            {
                {
                    return driver.FindElement(By.Id("iws_bd_error_text")).Text; ;
                }
            }
        }
    }
}
