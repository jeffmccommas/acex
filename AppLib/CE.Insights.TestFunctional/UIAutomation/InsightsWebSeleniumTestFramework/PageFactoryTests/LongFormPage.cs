﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Opera;
using OpenQA.Selenium.Chrome;

namespace CE.Test.Selenium.PageFactoryTests
{
    public class LongFormPage
    {
        private static string tabKey = "tabKey = 'tab.energyprofile'";
        private IWebDriver driver;

        public LongFormPage()
        {
            driver = new FirefoxDriver();
            driver.Manage().Timeouts().ImplicitlyWait(new TimeSpan(0, 0, 0, 10)); // 10 seconds
        }

        public LongFormPage(IWebDriver driver)
        {
            this.driver = driver;
            driver.Manage().Timeouts().ImplicitlyWait(new TimeSpan(0, 0, 0, 10)); // 10 seconds
          

        }

       // public LongFormPage(Common.DriverInit driverInit)
       // {
       //     if (driverInit == Common.DriverInit.InitYes)
       //     {
       //         driver = new FirefoxDriver();
       //         driver.Manage().Timeouts().ImplicitlyWait(new TimeSpan(0, 0, 0, 10)); // 10 seconds
       //     }
       // }


        public LongFormPage(Common.Browser browser)
        {
            driver = Common.GetWebDriverForBrowser(browser);
            driver.Manage().Timeouts().ImplicitlyWait(new TimeSpan(0, 0, 0, 10)); // 10 seconds
          
        }
        public void Load()
        {
           // MySavingsField.Click();
            driver.Manage().Timeouts().ImplicitlyWait(new TimeSpan(0, 0, 0, 10)); // 10 seconds
            driver.FindElement(By.LinkText("My Savings")).Click();
  
        }

        public void LoadWidget(string AccountInfo, string TestEnvironment, string ClientId, string WidgetType, string Locale)
        {
            var _url = Common.GetEnvUrl(TestEnvironment, WidgetType);
            var _webtoken = Common.GetWebToken(Convert.ToInt32(ClientId), AccountInfo);
            _url += "&ClientId=" + ClientId + "&CustomerId=" + AccountInfo + "&AccountId=" + AccountInfo + "&PremiseId=" + AccountInfo + "&ServicePointId=&WebToken=" + _webtoken + "&Locale=" + Locale;//if no locale then default is en-US
            driver.Navigate().GoToUrl(_url);
        }

        public void Close()
        {
            driver.Close();
        }

        public bool IsLoaded
        {
            get { return driver.PageSource.Contains(tabKey); }
        }

       
        public string IsLoadedSecTitle
        {
            get
            {
                {
                    return driver.FindElement(By.Id("iws_lp_tab-0")).Text; ;
                }
            }
        }

        public string ProfileSectionSubtitle
        {
            get
            {
                //Your home's heaviest hitters subtitle
                var elementText = driver.FindElement(By.Id("iws_lp_section_subtitle"));
                return elementText.Text;
     
            }

        }

        public bool TextFoundPool
        {
            get { return driver.PageSource.Contains("Pool"); }
        }
    }
}
