When using chrome, make sure postman is enabled:  enter in the url: chrome://extensions/  see that the Postman - REST Client (Packaged App) checkbox is enabled. 
You may also need to allow/grant permissions to the urls being used if you are have an access error getting to that url.

explanation of automated tests for postman here:
http://tfssp.aclaratech.com/tfs/SWDefaultCollection/EnergyPrism/PM/Shared%20Documents/PAWS%20Project/Insights_API_AutomatedTests.docx

Information about Postman collections:  http://www.getpostman.com/docs/collections

backup.postman dump is backup of all the collections plus the environment settings. 
Collections are also saved individually which will not have the environment settings (anything with a {{ double bracket is a variable}})

To save any changes at the scenario level, check out the collection that is being tested.
1)  Under postman menu- go to the 'share collection' icon at the scenario level -> this brings up the 'Share Collection window'
2)  Select the 'Download as a File' button --> Save to the TFS location such as:  C:\SW\EnergyPrism\Branchversion\AppLib\CE.Insights.TestFunctional\Postman_TestCollections for that file . File will have following format: [nameofscenariotest].json.postman_collection
3) Check in to TFS


NOTE backup with caution:  To save all, backup the postman files regularly which can then easily be imported later. Only do this if you have gotten the latest previous to making the changes.
1)  Under Settings (top right menu - icon looks like a tool) from Postman: 
   a) Select 'Download' button under 'Download File'--> Save to the TFS location. This will save a file named 'Backup.postman_dump'
   b) Check in to TFS



To import a new collection:
1)  Under postman menu- go to the 'import collection' at the top level (level frame) -> this brings up the 'Import Collection' window
2) Import a published url or a file (either choose file or drop on the square output).
3) Once changes are made , export /save these off to TFS as a backup or as a collection.




Collection may also be published for others to access through url (see step 2 above). 
They will need to make sure they get or set the environment variables - in this case or use the backup postman dump.