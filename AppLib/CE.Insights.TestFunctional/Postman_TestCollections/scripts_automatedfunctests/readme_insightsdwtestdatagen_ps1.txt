NOTE: You must grant execute policy to unrestricted (or remote) to be able to run powershell scripts on your computer.
  
If you are getting an error like: File xxx cannot be loaded because the execution of scripts is disabled on this system. Please see get-help about_signing for more details.
See here for instructions:
https://technet.microsoft.com/en-us/library/hh849812.aspx

You should set to unrestricted for the execution policy. Or it might also work with RemoteSigned which is lower level
If running  scripts and creating folders from a different drive for fully qualified path C:\AclaraX for scripts and generated csv files - you will
also need to revise the powershell script to point the your folder structure.

insightsdwtestdatagen.ps1
This Generates test data for profile and action items for however many rows you want for clientid 256 or 87 in Dev, QA or UAT environments.
This copies over the generated file if successful to the preprocess file location for the bulkdata to load into the holding tables.
Default of 10 rows if argument is not supplied.
flag of 1 (default of 1 - true) to generate action item data . Note that one row for each customer with completed status is generated, and also an additional row with randomized status and action item for additional test data for that same customer.
flag of 1 (default of 1  - true) to generate profile data

Data is randomized based on current values that were put in requirements and min and max values that are set in the content system.
The seeddata may be changed to any name.  Datetimestamp is added with sequence to the seed for customer, premise and account. 

Note that the current raw data file generated for profile and actionitem is based on the suggested file format for testing with clientid 256.   Any differences in format for future (implemented) clients would need to have changes made on the current powershell script functions used.   
One could either
o	 Add different format on file generation in powershell scripts based on the input clientid, i.e. �clientid 224 would have different format than �clientid 256 when calling the powershell script  OR
o	 use different powershell script entirely with change in functions to accommodate this change.
o    We have added different format by changing functions for clientids 276 (extended billformat only, they don't do actions or profileattributes), 224, and 101


To use the powershell script
Create folder (or change path in the script to reflect your fully qualified path
C:\AclaraX
    \testing
                \scripts    Note: put powershell script , named "insightsdwtestdatagen.ps1" in this folder  
	\csvtestfilegen
                	actionitem
                	profile
					csvs
					billing

actionitem data will be generated in actionitem folder with datetimestamp.  File name starts with ActionItem_  . This is required naming convention of the file will not be picked up.
profile data will be generated in the profile folder.  File name starts with profiledata_.


#To run powershell script to generate data:
#argument passed to here from powershell command line:  
#c:\AclaraX\testing\scripts\insightsdwtestdatagen.ps1 -env Dev -clientid 256 -numberRows 20 -genaction 1 -genprofile 1 -genbill 1  -billformat basic
#sample call to generation bill in extended format - 
c:\AclaraX\Testing\scripts\insightsdwtestdatagen.ps1 -env Dev -clientid 256 -numberRows 20 -genbill 1 -billformat extend

#if CD to the directory above first
#can just enter  (parameter names are optional)
.\insightsdwtestdatagen.ps1 Dev 256 20 1 1

#sample script call here
PS C:\AclaraX\Testing\scripts>  c:\AclaraX\Testing\scripts\insightsdwtestdatagen.ps1 -env QA -clientid 256 -numberRows 100 -genaction 1 -genprofile 1
.copying file  C:\AclaraX\csvtestfilegen\actionitem\ActionItem_20150302095140.csv  to: \\azuqfil001\cloudfiledrop\256\preprocess\bi\actionitem\
copying file C:\AclaraX\csvtestfilegen\profile\profiledata_20150302095140.csv  to: \\azuqfil001\cloudfiledrop\256\preprocess\bi\profile\

Notes: Dev, QA, UAT are only acceptable values for -env
if numberRows not specified defaults to 10, 
if genaction is not specified it won't generate (needs to be 1, can also pass 0 if want to not generate but also ok to not include,
similar for -genprofile (needs to be 1)

For example above:
Dev is environment tested
Clientid is 256
where 20 is # of rows
1 is to create test data for actionitem
1 is to create test data for profile


#Other sample scripts 
#276: only extend bill format use, no actions, no profile attributes
c:\AclaraX\testing\scripts\insightsdwtestdatagen.ps1 -env Dev -clientid 276 -numberRows 1 -genbill 1  -billformat extend

#NOTE basic format is no longer supported
#101: only basic format - also can test in 87 or 256 - for bill. No profile attributes no actions.
c:\AclaraX\testing\scripts\insightsdwtestdatagen.ps1 -env Dev -clientid 101 -numberRows 1 -genbill 1  -billformat basic


 



