$timestamp = Get-Date -f yyyy-MM-dd
## Top of the script  environment name passed in  which determines which environment/ insights url is used for the postman tests when newman runs them.
#$envName = $args[0]
if ($args.Length -eq 0)
{
    $envName = "qa" 
}
else
{
  $envName = $args[0]
}
#or comment above and use below variable
#$envName = "localdev"
#fully qualified path name where test results are stored
$fqPath = "E:\AclaraX\Testing\"
#this is location for test results json file output from the postman tests. Holds all collections test result outputs
$resultPath = $fqPath + "testresults\"
#location for environment (qa,uat,localdev,dev,prod) global settings that contain the access keys and urls specific to each environment. One file for each environment is used.  
$envsettingsPath = $fqPath + "postmanenviron\"
#location for postman json collection tests when is stored - not shared on postman site for the run. Use either the TFS location on local computer or copy to the postmantests folder
$postmancollectionPath = $fqPath + "postmantests\"
#path for script to email script
$emailscriptfailuresPath =  $fqPath + "scripts\run_newman_CEInsightsAPI_countstatus.ps1"
 

#array of filenames for test results output
$arrayResultFiles = New-Object System.Collections.ArrayList
#name of output file which is appended with errors and successes count for each collection
$outputfiletally = $resultPath + "Fail\" + $envname + "_newman_testscount.txt"

#array of email addresses to whom the failed tests are sent
$emailTolist = "<llefkowitz@aclara.com>", "<lydia1002@verizon.net>", "<JMcNamara@aclara.com>", "<mjones@aclara.com>", "<pvictor@aclara.com>", "<RPensalfini@aclara.com>", "<IFaynstein@aclara.com>", "<mali@aclara.com>"

#function to run postman collection that is shared/published on the postman site
function runsharedpostmancollection 
{
Param(
   
  #envname from above + outfile + indicate the collectionname as the file name
  [Parameter(Mandatory=$true)]
  [string]
  $resultsFileName,
  
  [Parameter(Mandatory=$true)]
  [string]
  $collectionName 
)

Process 
{

    $outfilefn = $resultPath + $envName + $resultsFileName + $timestamp + ".json"
    $envfile = $envsettingsPath + $envName + ".postman_environment"
	$globalsfile = $envsettingsPath + "globals.postman_globals"

    #collection call from newman is slightly different if published as shared collection on postman site versus a file location
    #newman -u  https://www.getpostman.com/collections/cbb43d998c41ae80eec7 -e "C:\AclaraX\Testing\postmanenviron\qa.postman_environment" -o $outfile  
    newman -u  $collectionName -e $envfile -g $globalsfile -o $outfilefn 

    #add output file name to array for parsing later.
    $arrayResultFiles.Add($outfilefn)
 }
}
 
#function to run postman collection (json file) that is stored on drive , i.e. not shared on the postman site
function runstoredpostmancollection 
{
Param(
   
  #envname from above + outfile + indicate the collectionname as the file name
  [Parameter(Mandatory=$true)]
  [string]
  $resultsFileName,
  
  #name of json file to use. This is appended to fully qualified path where the json file is stored.
  [Parameter(Mandatory=$true)]
  [string]
  $collectionName 
)

Process 
{
    $outfilefn = $resultPath + $envName + $resultsFileName + $timestamp + ".json"
    $envfile = $envsettingsPath + $envName + ".postman_environment"
	$globalsfile = $envsettingsPath + "globals.postman_globals"
    $collectionLocation = $postmancollectionPath + $collectionName

    #collection call from newman is slightly different if published as shared collection on postman site versus a file location
    #newman -c  "E:\AclaraX\Testing\postmantests\GetandPostProfileScenarioTests.json.postman_collection" -e "E:\AclaraX\Testing\postmanenviron\qa.postman_environment" -o $outfilefn  
    newman -c $collectionLocation -e $envfile -g $globalsfile -o $outfilefn
    
    #add output file name to array for parsing later.
    $arrayResultFiles.Add($outfilefn)
 }
}


function parse_resultsforerrors 
{
Param 
 (
   [System.Collections.ArrayList]$resultsArray
 )

#repeating text for the test results file
$TestFail = "Tests Failed: "
$TestSuccess = "Tests Succeeded: "
$TestResults = "Test Results File: "
$countfailall = 0
  
  
 #each testresult from newman is output to a json test results which is used for test result file failures and may be imported into postman collection to see further details
 $timestampfull = Get-Date -format G
[string]$timestampfull + " Postman Run Date " + $envName + " Environment" | Out-File $outputfiletally
 #blank line before each of the counts of tests filed and succeeded for each test collection
 

 #loop through the resultfilesnames and look for failures and successes count
 #append to the output tally file of total successes and failures for each postman collection
 foreach ($element in $resultsArray) 
 {
 $xfailcount = 0
 $xsuccesscount = 0
 $xfailcounttext = ""
 $x = ""
 $element
 #blank line before each of the counts of tests filed and succeeded for each test collection
  Write-output `n | Out-File $outputfiletally -Append 
  $elementfilename = split-path $element  -leaf

  #$filename being evaluated print line for this
  $elementfilename | Out-File $outputfiletally -Append

  #count those that failed
  $x =  @(get-childitem $element -recurse | select-string -pattern '"fail": 1' -casesensitive )
  $xfailcount = "{0:N0}" -f $x.count

  If ($xfailcount) {
    $xfailcounttext =  $TestFail  + [string]$xfailcount
    $countfailall = $countfailall + $xfailcount
  }
  else {
    $xfailcounttext = $TestFail +  "0"
  }
   $x = ""
   #count those that passed
   $x1 =  @(get-childitem $element -recurse | select-string -pattern '"fail": 0' -casesensitive )
   $xsuccesscount = $TestSuccess + [string]$x1.count
    Write-output $xfailcounttext  $xsuccesscount | Out-File $outputfiletally -Append 
    
    #reset to 0 or null
     $xfailcount = 0
     $xsuccesscount = 0
     $xfailcounttext = ""
     $x = ""
 }
 
 #--------------tally of total failed counts from ALL postman collections tested above against desired environment
 $countfailalltext = "CE Insights API Functional Tests Failed (" + $envName + " Env)-total failed count: " + [string]$countfailall
 Write-output `n | Out-File $outputfiletally -Append 
 Write-output  $countfailalltext | Out-File $outputfiletally -Append
 
 #email if count fail tallies for all postman collections are greater than 0
 #COMMENT OUT THIS SECTION IF DON'T WANT TO EMAIL 
If ($countfailall -gt 0)
 {
 Send-MailMessage -from llefkowitz@aclara.com -subject $countfailalltext -to $emailTolist -Attachments $outputfiletally -Body "see output of counts on attachment" -Priority High -smtpServer 10.153.19.6
 }

}

#delete any files older than ? days. To do.

#----------Put all calls here or running the postman collections calling functions above----------------------------------------------------------------------------
#keep alive test  run in all environments except dev - always run this first before others to ensure hits initially for echo
if ($envName -eq "qa") 
{
#runsharedpostmancollection -resultsFileName "outfile-keepalive-" -collectionName "https://www.getpostman.com/collections/f6457c3adfd8e62a6192" 
runstoredpostmancollection -resultsFileName "outfile-keepalive-" -collectionName "KeepAliveEndpointsScenarioTests.json.postman_collection"
}

#put any calls to either runsharedpostmancollection or runstoredpostmancollection here
#runsharedpostmancollection -resultsFileName "outfile-exceptions-" -collectionName "https://www.getpostman.com/collections/0311b1ddb6d19a622661"
runstoredpostmancollection -resultsFileName "outfile-exceptions-" -collectionName "ExceptionHandlingTests.json.postman_collection"
#get bill scenarios
#runsharedpostmancollection -resultsFileName "outfile-getbill-" -collectionName "https://www.getpostman.com/collections/1142943bf31df8ff1c24"
runstoredpostmancollection -resultsFileName "outfile-getbill-" -collectionName "GetBillScenarioTests.postman_collection.json"

#basic authentication tests
#runsharedpostmancollection -resultsFileName "outfile-basicauth-"  -collectionName "https://www.getpostman.com/collections/d3c828716e13ff9e7e14"
runstoredpostmancollection -resultsFileName "outfile-basicauth-" -collectionName "BasicAuthenticationScheme_Tests.json.postman_collection"

#authorization endpoint scenarios
#runsharedpostmancollection -resultsFileName "outfile-authendpoint-" -collectionName "https://www.getpostman.com/collections/84842e5aa7eda2d02f9e"
runstoredpostmancollection -resultsFileName "outfile-authendpoint-" -collectionName "AuthorizationEndpointTests.json.postman_collection"

#get benchmark scenarios
#runsharedpostmancollection -resultsFileName "outfile-getbench-" -collectionName "https://www.getpostman.com/collections/df8cd1ee361a84da2868" 
runstoredpostmancollection -resultsFileName "outfile-getbench-" -collectionName "GetBenchmarkScenariosTests.postman_collection.json"

#get content scenarios
#runsharedpostmancollection -resultsFileName "outfile-getcontent-" -collectionName "https://www.getpostman.com/collections/0aea1af09b02c9e1f4da"
runstoredpostmancollection -resultsFileName "outfile-getcontent-" -collectionName "ContentScenarioTests.json.postman_collection"

#content profile defaults tests
#runsharedpostmancollection -resultsFileName "outfile-contentprofiledef-" -collectionName "https://www.getpostman.com/collections/7c3f62d18ebff9eed074"
runstoredpostmancollection -resultsFileName "outfile-contentprofiledef-" -collectionName "ContentProfileDefaultsTests.json.postman_collection"

#get and post profile scenarios - this is example of  using stored location for postman collection tests.
runstoredpostmancollection -resultsFileName "outfile-getandpostprofile-" -collectionName "GetandPostProfileScenarioTests.json.postman_collection"  

#get and post action plan scenarios 
#runsharedpostmancollection -resultsFileName "outfile-getandpostactplan-" -collectionName "https://www.getpostman.com/collections/e8f9a29cd07fe89d3379"
runstoredpostmancollection -resultsFileName "outfile-getandpostactplan-" -collectionName "ActionPlanScenarioTests.json.postman_collection" 

#get and post action plan subaction scenarios
#runsharedpostmancollection -resultsFileName "outfile-getandpostactplan-subact-" -collectionName  "https://www.getpostman.com/collections/605092a58aa210a26b78"
runstoredpostmancollection -resultsFileName "outfile-getandpostactplan-subact-" -collectionName "GetandPostActionPlan_SubActionKeyScenarioTests.json.postman_collection" 

#billdisagg tests collection
#runsharedpostmancollection -resultsFileName "outfile-billdisagg-" -collectionName "https://www.getpostman.com/collections/685a1b07dc45b63b18f8"
runstoredpostmancollection -resultsFileName "outfile-billdisagg-" -collectionName "BillDisagg.postman_collection.json" 

#get action scenarios
#runsharedpostmancollection -resultsFileName "outfile-getaction-" -collectionName "https://www.getpostman.com/collections/7818d7193d725faf694e"
runstoredpostmancollection -resultsFileName "outfile-getaction-" -collectionName "GetActionsScenarioTests.json.postman_collection" 

#customerinfo test collection from datawarehouse test shard
runstoredpostmancollection -resultsFileName "outfile-custinfo-" -collectionName "CustomerInfoTests.json.postman_collection" 

#aclaraone (ao) consumptionv2 as of 16.06 get scenarios not executed for tests for azure table storage in prod. This replaces consumptionv1 which was fakedata generator
#aclaraone (ao) billtodate v1 as of 16.06 get not executed for tests in azure table storage in prod 
#rateapi 16.05 does not exist yet on prod
#runsharedpostmancollection -resultsFileName "outfile-getconsump-" -collectionName "https://www.getpostman.com/collections/f9687ad8c04142ca5380" 
#webtoken scenarios - execute if QA environment
#eventtracking nonprod run once for nonprod environment for invalidactionid and invalidpremiseid
#runsharedpostmancollection -resultsFileName "outfile-getandpostwebtoken-" -collectionName "https://www.getpostman.com/collections/81e8384f1036931b0f38"

#not working from newman on weldapp001 commenting this out
#runstoredpostmancollection -resultsFileName "outfile-eventtracknonprod-" -collectionName "EventTracking.json.postman_collection"  

runstoredpostmancollection -resultsFileName "outfile-getandpostwebtoken-" -collectionName "GetandPostWebTokenScenarioTests.postman_collection.json"  
runstoredpostmancollection -resultsFileName "outfile-busprofile-" -collectionName "BusinessGetandPostProfileScenarioTests.json.postman_collection"
#runstoredpostmancollection -resultsFileName "outfile-busenergymodel-" -collectionName "BusinessEnergyModelAndBillDisagg.json.postman_collection"
runstoredpostmancollection -resultsFileName "outfile-busbilldisagg-" -collectionName "BusBillDisaggTests.postman_collection.json"
#runstoredpostmancollection -resultsFileName "outfile-billtodateao-" -collectionName "BillToDateScenarioTests.json.postman_collection"
#runstoredpostmancollection -resultsFileName "outfile-subscripao-" -collectionName "SubscriptionTests.json.postman_collection"
runstoredpostmancollection -resultsFileName "outfile-rate-" -collectionName "RateTests.json.postman_collection"
#runstoredpostmancollection -resultsFileName "outfile-getconsumpao-" -collectionName "GetConsumptionv2AO.json.postman_collection" 
#runstoredpostmancollection -resultsFileName "outfile-SubscriptUOM-" -collectionName "SubscriptionUOMtests.json.postman_collection"
#runstoredpostmancollection -resultsFileName "outfile-GreenbuttonBill-" -collectionName "GreenButtonBill.postman_collection.json"
#runstoredpostmancollection -resultsFileName "outfile-GreenbuttonAMI-" -collectionName "GreenbuttonAMI.postman_collection.json"

#add for new features here
if ($envName -eq "qa") 
{
runstoredpostmancollection -resultsFileName "outfile-EnergyStar-" -collectionName "EnergyStarAclara.postman_collection.json"
}

#content refresh true - execute if DEV environment
#runsharedpostmancollection -resultsFileName "outfile-contentrefreshtrue-" -collectionName "https://www.getpostman.com/collections/d67c2fc9701ca1600e96" 
#if ($envName -eq "dev") 
#{
#	runstoredpostmancollection -resultsFileName "outfile-contentrefreshtrue-" -collectionName "ContentRefreshTrue.json.postman_collection" 
 #   runstoredpostmancollection -resultsFileName "outfile-GreenbuttonAMI-" -collectionName "GreenbuttonAMI.postman_collection.json"
	#runstoredpostmancollection -resultsFileName "outfile-EnergyStar-" -collectionName "EnergyStarAclara.postman_collection.json"
#}

#eventtrack prod generate errors for invalid eventtype 
#not working from newman on weldapp001 commenting this out
#if ($envName -eq "prod") 
#{
#	runstoredpostmancollection -resultsFileName "outfile-eventtrackprod-" -collectionName "EventTrackingprod.json.postman_collection" 
#}

#call file parser function to get total count of how many tests failed for each of the collection. Looks at resultsfile for each collection . This writes to a file with tallies for each collection and total count of failures/successes.
#Write-Output "array results list" + $arrayResultFiles
#also invokes email powershell script if errors > 0
parse_resultsforerrors -resultsArray $arrayResultFiles
 
#clear out array
$arrayResultFiles.Clear()
