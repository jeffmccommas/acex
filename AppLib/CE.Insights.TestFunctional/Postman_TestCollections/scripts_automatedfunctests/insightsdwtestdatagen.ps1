#argument passed to here from powershell command line (Note: make sure have access to preprocess folders on aclarace domain for your users prior to executing the script):  c:\AclaraX\Testing\scripts\insightsdwtestdatagen.ps1 -env Dev -clientid 256 -numberRows 20 -genaction 1 -genprofile 1 -genbill 1 -billformat basic
#Dev, QA, UAT 
Param(
	[string]$env,
    [string]$clientid,
	[string]$numberRows,
	[string]$genaction, 
	[string]$genprofile,
	[string]$genbill,
	[string]$billformat
)

  $environment = $null
  $environment = $env
	
	if($environment -ne "Dev" -and $environment -ne "QA" -and $environment -ne "UAT" -and $environment -ne "PROD" )
	{
		Write-Error "Aclara CE BI env parameter not specified or invalid.  Should be -env 'Dev' for example."  -Category InvalidArgument
		Write-Host "Script aborted."
        Return
	}

	 
    if([string]::IsNullOrEmpty($environment) -eq $true)
    {
        Write-Error "Aclara CE BI environment (environment variable) not specified." -Category InvalidArgument
        Write-Host "Script aborted."
        Return
    }
	
$clientnum = $null
$clientnum = $clientid

$numRows = 0
$numRows = $numberRows
   if($numRows -lt 1)
   {
    $numRows = 10
	Write-Host "setting to default 10 rows"
   }

  if($clientnum -ne "87" -and $clientnum -ne "256" -and $clientnum -ne "224" -and $clientnum -ne "276" -and $clientnum -ne "101")
  {Write-Error "only 87 or 256 or 224 or 276 or 101 client id is allowed for generating test data in test environments"  -Category InvalidArgument
   Write-Host "Script Aborted."
   Return
   }

   #catch for PROD , only demo clients can run
   if ($environment -eq "PROD") 
   {
	if($clientnum -ne "87" -and $clientnum -ne "256")
	{Write-Error "only 87 or 256 client id is allowed for generating test data in prod environments"  -Category InvalidArgument
	Write-Host "Script Aborted."
	Return
	}
   }
   
   
$timestamp = Get-Date -f yyyyMMddHHmmss  
$shortdate = Get-Date -f yyyyMMdd

#environment locations for the preimport file that is created here for profile and/or actionitem, move to this location when file is created
switch ($environment)
{
    Dev
    {
    $fileServer = "\\azuDfil001.stg.aclarace.com";
	$inputpreprocessactionitempath = $fileserver + '\PaaS_CloudFileDrop\' + $clientId + '\preprocess\bi\actionitem\'
	$inputpreprocessprofiledatapath = $fileserver  + '\PaaS_CloudFileDrop\' + $clientId + '\preprocess\bi\profile\'
	$inputpreprocessbilldatapath = $fileserver  + '\PaaS_CloudFileDrop\' + $clientId + '\preprocess\bi\csvs\'
	$inputpreprocessextendbilldatapath = $fileserver  + '\PaaS_CloudFileDrop\' + $clientId + '\preprocess\bi\billing\'
    break;
    }
    QA
    {
    $fileServer = "\\azuqfil001.stg.aclarace.com";
	$inputpreprocessactionitempath = $fileserver + '\PaaS_CloudFileDrop\' + $clientId + '\preprocess\bi\actionitem\'
	$inputpreprocessprofiledatapath = $fileserver  + '\PaaS_CloudFileDrop\' + $clientId + '\preprocess\bi\profile\'
	$inputpreprocessbilldatapath = $fileserver  + '\PaaS_CloudFileDrop\' + $clientId + '\preprocess\bi\csvs\'
	$inputpreprocessextendbilldatapath = $fileserver  + '\PaaS_CloudFileDrop\' + $clientId + '\preprocess\bi\billing\'
    break;
    }
    UAT
    {
    $fileServer = "\\azuufil001.stg.aclarace.com";
	$inputpreprocessactionitempath = $fileserver + '\PaaS_CloudFileDrop\' + $clientId + '\preprocess\bi\actionitem\'
	$inputpreprocessprofiledatapath = $fileserver  + '\PaaS_CloudFileDrop\' + $clientId + '\preprocess\bi\profile\'
	$inputpreprocessbilldatapath = $fileserver  + '\PaaS_CloudFileDrop\' + $clientId + '\preprocess\bi\csvs\'
	$inputpreprocessextendbilldatapath = $fileserver  + '\PaaS_CloudFileDrop\' + $clientId + '\preprocess\bi\billing\'
    break;
    }
	PROD
    {
    $fileServer = "\\azupfil001.prd.aclarace.com";
	$inputpreprocessactionitempath = $fileserver + '\PaaS_CloudFileDrop\' + $clientId + '\preprocess\bi\actionitem\'
	$inputpreprocessprofiledatapath = $fileserver  + '\PaaS_CloudFileDrop\' + $clientId + '\preprocess\bi\profile\'
	$inputpreprocessbilldatapath = $fileserver  + '\PaaS_CloudFileDrop\' + $clientId + '\preprocess\bi\csvs\'
	$inputpreprocessextendbilldatapath = $fileserver  + '\PaaS_CloudFileDrop\' + $clientId + '\preprocess\bi\billing\'
    break;
    }
    default
    {
    write-host "Invalid Environment $environment";
    return;
    break;
    
    }
}


#or comment above and use below variable
#$envName = "localdev"
#fully qualified path name where csv generated file is stored
$fqPath = "C:\AclaraX\csvtestfilegen\"
#this is location for test results json file output from the postman tests. Holds all collections test result outputs
$resultPathprofile = $fqPath + "profile\"
$resultPathactionitem = $fqPath + "actionitem\"
$resultPathbill = $fqPath + "csvs\"
$resultPathextendbill = $fqPath + "billing\"
$seedprefix = "QAInttest"
$timestampprefix = Get-Date -Format "MMddyyyyhhmmss"
$seedprefix += "_" + $timestampprefix
#function to generate action item test data
function genactionitemtestdata
{
Param(
   
  #resultfilename
  [Parameter(Mandatory=$true)]
  [string]
  $resultsFileName,
  [string]
  $fileExt

)

Process 
{

    $outfilefn = $resultPathactionitem + $resultsFileName + $timestamp + $fileExt
  
    $psObject = $null
    $psObject = New-Object psobject
    # $numRows will be the ‘loop counter’ 
     [System.Collections.ArrayList]$seedArrayList = 1..$numRows
    $seeds=$seedArrayList
    foreach($datatext in $seeds)
    {
    $custid = "cust" + $seedprefix + "_" + $datatext
    $acct = "acct" + $seedprefix + "_" + $datatext
    $prem = "prem" + $seedprefix + "_" + $datatext
    #select to get all current measures - tailor for the client tested
    #SELECT STUFF((SELECT ',"' + actionkey  + '"'FROM [InsightsMetadata].[cm].[ClientAction]  ORDER BY actionkey asc FOR XML PATH('')) ,1,1,'') AS Txt 
    #for now use the mapped description on excel spreadsheet - the list that are in purple and background whie
    #this was from content - the actual key value $measurearray = "auditairsealing","auditappliance","auditblowerdoor","audithome","audithvacttest","auditsitevisit","auditsystems","avoidheatunoccupied","avoidoverdryingclothes","calkandweatherize","clotheswasherpartialload","customizedprogram","dishwashbyhand","dryfullloadsofclothes","fullloadlaundry","gethottubcover","getpoolcover","greenpower","homeheatingprotection","improveinsulation","install_leddownlight","install_ledgeneral","installcflgeneral","installcflspecial","installprogramthermostat","insulatebasement","insulateceiling","insulatecrawlspace","insulateductwork","insulatehotwaterpipes","insulatewall","liauditmf2to4","liauditmfcustom","liauditsinglefam","lowerheattemp","lowerhottubtemp","lowerpooltemp","lowerwatertempelec","lowerwatertempgas","lowflowfaucet","lowflowshower","maintainheatsystem","maintainwaterheater","maintainwaterheatergas","paperlessbilling","powermeter","recycleseondrefrigerator","replaceclothesdryer","replaceclotheswasher","replacedehumidifier","replacedishwasher","replacefreezer","replaceheatsystem","replacepoolpump","replacerefrigerator","replacetorchiere","replacewaterheater","replacewindow","runfulldishwasher","runpoolpumpless","savingsplan","sealairducts","shortershowers","showerstartupvalve","skipdrydishwasher","takeshortershowers","updateprofile","updatesavingsplan","usehottubcover","uselaptop","usepoolcover","washclothesincold"
    $measurearray = "BLOWER DOOR TEST","CLOTHES WASHER REPLACEMENT","CUSTOM, CUSTOM","DEHUMIDIFIER REPLACEMENT","DHW PACKAGE, PIPE WRAP","DHW PACKAGE, AERATOR","DHW PACKAGE, SHOWERHEAD","DHW PACKAGE, WATER SAVERS","DUCT SEALING","FREEZER REPLACEMENT","SITE VISIT, SINGLE FAMILY","HOME APPLIANCE AND REBATE AUDIT","HOME AUDIT","HOME SYSTEMS AUDIT","HOURLY AIR SEALING","HVAC TESTS","INSTALL BASEMENT INSULATION","INSTALL CEILING INSULATION","INSTALL WALL INSULATION","LIGHTING, CFL, GENERAL SERVICE","LIGHTING, CFL, NON-GENERAL SERVICE","LIGHTING, LED, DOWNLIGHT","LIGHTING, LED, DOWNLIGHT, COURTESY","LIGHTING, LED, GENERAL SERVICE","LIGHTING, LED, GENERAL SERVICE, COURTESY","REFRIGERATOR REPLACE (15 CU. FT.)","REFRIGERATOR REPLACE (18 CU. FT.)","REFRIGERATOR REPLACE (21 CU. FT.)","REFRIGERATOR REPLACEMENT","WINDOW REPLACEMENT, CUSTOM","LIGHTING, TORCHIERE","SITE VISIT, DOE","SITE VISIT, MULTIFAMILY (2-4 UNITS)"
	#these are currently not configured, so don't randomize until added to database configuration "SITE VISIT, MULTIFAMILY, CUSTOM","A/C, UNITARY AND SPLIT SYSTEMS","AC REPLACE - 0-7999 BTUH (WINDOW)","AC REPLACE - 8K-13999 BTUH (WINDOW)","AC REPLACE - 8K-20K BTUH (SLEEVE)","ADMINISTRATIVE ADJUSTMENT","APPLIANCE EVAL. AND REPLACE","APPLIANCE EVAL. AND REPLACE (HHI)","BOILER","BOILER GAS","BOILER OIL","BOILER PRO","BOILER CIRCULATOR PUMP","CUSTOM, DHW","FURNACE","FURNACE GAS","FURNACE OIL","FURNACE ELE","FURNACE PRO","HEAT PUMP, AIR TO AIR","HEAT PUMP, DUCTLESS","HEAT PUMP, GEOTHERMAL","INSULATE ATTIC OPENINGS","LIGHTING, CUSTOM","LIGHTING, STANDARD","SYSTEM ADJUSTMENT  - ENVELOPE","TRACK 1: HERS/REMRATE","TRACK 2: HIGH PERFORMANCE INSULATION","TRACK 3: DUCT & ENVELOPE TESTING, CUSTOM","WATER HEATER, GAS, CONDENSING","WATER HEATER, GAS, INDIRECT","WATER HEATER, GAS, INTEGRATED BOILER","WATER HEATER, GAS, STORAGE","WATER HEATER, GAS, TANKLESS","WATER HEATER, HEAT PUMP"
    $measure = $measurearray | Get-Random
    $programarray = "Proj2014"," "
    $program = $programarray | Get-Random
    #currently just do completed, change back to randomized from below values when decide to go with larger list
    $statusarray = "completed","cancelled","selected","rejected","presented","considering"
   
	$status = "completed"
    #do we want random dates - NOTE: for now use current date
     $statusdate = Get-Date -f o
	 $statusdateshort = Get-Date -f yyyyMMdd
     $source = "utility"
     $programname = "Home Energy Solutions"
     $incentiveamount =  Get-Random -minimum 1 -maximum 101
     $incentivetype = "Rebate"
     $upfrontcost = Get-Random -minimum 1 -maximum 80
     $annualsavingsestimate = Get-Random -minimum 0.00 -maximum 100.00
     $annualsavingsestimate = "{0:N2}" -f $annualsavingsestimate 

	$arraylistfinal = $custid + "|" + $acct + "|" + $prem + "|" + $measure  + "|" + $program + "|" +  $statusdateshort +  "|" + $programname + "|" + $incentiveamount   + "|" + $upfrontcost + "|" + $annualsavingsestimate + "`n" 
    #$arraylistfinal = $custid + "|" + $acct + "|" + $prem + "|" + $measure  + "|" + $program  + "|" + $status  + "|" +  $statusdate + "|" + $source + "|" + $programname + "|" + $incentiveamount   + "|" + $incentivetype + "|" + $upfrontcost + "|" + $annualsavingsestimate + "`n" 
	# difference in format for 224 here
	$arraylistfinal224 = $custid + "|" + $acct + "|" + $prem + "|" + $measure  + "|" + $program + "|" +  $statusdateshort +  "|" + $programname + "|" + $incentiveamount   + "|" + $upfrontcost + "|" + $annualsavingsestimate + "`n" 
    #properties
    #$prop = 'customerid|accountid|premiseid|measurename|projectid|status|statusdate|source|programname|incentiveamount|incentivetype|upfrontcost|annualsavingsestimate'
	$prop = 'customerid|accountid|premiseid|measurename|subactionkey|statusdate|programname|incentiveamount|upfrontcost|annualsavingsestimate' 
	#different properties for 224 clientid
	$prop224 = 'customerid|accountid|premiseid|measurename|subactionkey|statusdate|programname|incentiveamount|upfrontcost|annualsavingsestimate'
    #first row add the column name above - rest of time, no name needed or get error
    if ($datatext -eq 1) {
	    if ($clientnum -ne "224")
		{
        $psObject | add-member -MemberType NoteProperty -name $prop -value $arraylistfinal 
		}
		else {
		 $psObject | add-member -MemberType NoteProperty -name $prop224 -value $arraylistfinal224 
		}
    }
     else {
        if ($clientnum -ne "224")
		{
			$psObject.$prop += $arraylistfinal
		}
		else
		{
		  $psObject.$prop224 += $arraylistfinal224
		}
         
     }
      #add additional row actions for same customer with randomized action status
	   $status2 = $statusarray | Get-Random
	   $measure2  = $measurearray | Get-Random
	   $program2 = $programarray | Get-Random
	   $incentiveamount2 =  Get-Random -minimum 1 -maximum 101
	   $upfrontcost2 = Get-Random -minimum 1 -maximum 80
	   $annualsavingsestimate2 = Get-Random -minimum 0.00 -maximum 100.00
	   $annualsavingsestimate2 = "{0:N2}" -f $annualsavingsestimate 
	   if ($clientnum -ne "224")
	   {
	   #$arraylistfinal2 = $custid + "|" + $acct + "|" + $prem + "|" + $measure2  + "|" + $program2  + "|" + $status2  + "|" +  $statusdate + "|" + $source + "|" + $programname + "|" + $incentiveamount2   + "|" + $incentivetype + "|" + $upfrontcost2 + "|" + $annualsavingsestimate2 + "`n" 
	    $arraylistfinal2 = $custid + "|" + $acct + "|" + $prem + "|" + $measure2  + "|" + $program2 + "|" +  $statusdateshort +  "|" + $programname + "|" + $incentiveamount2   + "|" + $upfrontcost2 + "|" + $annualsavingsestimate2 + "`n" 
	   $psObject.$prop += $arraylistfinal2
	   }
	   else
	   {
	    $arraylistfinal2 = $custid + "|" + $acct + "|" + $prem + "|" + $measure2  + "|" + $program2 + "|" +  $statusdateshort +  "|" + $programname + "|" + $incentiveamount2   + "|" + $upfrontcost2 + "|" + $annualsavingsestimate2 + "`n" 
        $psObject.$prop224 += $arraylistfinal2
	   
	   }
    }
    $psObject  |Export-CSV $outfilefn  -notype -delimiter "|"
    #replace the quotes with nothing
    (Get-Content $outfilefn) | % {$_ -replace '"', ""} | out-file -FilePath $outfilefn -Force -Encoding ascii
	#take out blank lines at end
    (Get-Content $outfilefn) | ? {$_.trim() -ne "" } | set-content $outfilefn
	#if file exists copy it to the preprocess location
	if((Test-Path -Path $outfilefn))
	{   Write-Host "copying file " $outfilefn " to:"  $inputpreprocessactionitempath 
		Copy-Item $outfilefn $inputpreprocessactionitempath 
	}
 }
} 



#function to generate profile test data
function genprofiletestdata
{
Param(
   
  #resultfilename
  [Parameter(Mandatory=$true)]
  [string]
  $resultsFileName,
  [string]
  $fileExt

)

Process 
{

    $outfilefn = $resultPathprofile  + $resultsFileName + $timestamp +  $fileExt

    $psObject = $null
    $psObject = New-Object psobject
    # $numRows is the loop count, set to default or can override
     [System.Collections.ArrayList]$seedArrayList = 1..$numRows
    $seeds=$seedArrayList
    foreach($datatext in $seeds)
    {
    $custid = "cust" + $seedprefix + "_" + $datatext
    $acct = "acct" + $seedprefix + "_" + $datatext
    $prem = "prem" + $seedprefix + "_" + $datatext
    #select to get all current profile attributes - tailor for the client tested
    # SELECT STUFF((SELECT ',"' + profileoptionkey  + '"'FROM [InsightsMetadata].[cm].[vProfileOptions]  WHERE profileoptionkey LIKE 'waterheater.fuel%' ORDER BY profileoptionkey asc FOR XML PATH('')) ,1,1,'') AS Txt 
    $waterheaterfuelarray = "waterheater.fuel.electric","waterheater.fuel.gas","waterheater.fuel.gasandwood","waterheater.fuel.oil","waterheater.fuel.propane","waterheater.fuel.solar","waterheater.fuel.wood"
    $waterheaterfuel = $waterheaterfuelarray | Get-Random
    # SELECT STUFF((SELECT ',"' + profileoptionkey  + '"'FROM [InsightsMetadata].[cm].[vProfileOptions] WHERE profileoptionkey LIKE 'heatsystem.fuel%' ORDER BY profileoptionkey asc FOR XML PATH('')) ,1,1,'') AS Txt 
    $heatsystemfuelarray = "heatsystem.fuel.electric","heatsystem.fuel.gas","heatsystem.fuel.gasandwood","heatsystem.fuel.oil","heatsystem.fuel.propane","heatsystem.fuel.solar","heatsystem.fuel.wood"
    $heatsystemfuel = $heatsystemfuelarray | Get-Random
    # SELECT [MinValue],[MaxValue],[DoNotDefault] ,[Disable] FROM [InsightsMetadata].[cm].[vProfileAttributes] WHERE profileattributekey = 'centralac.count'
    $centralac_count = Get-Random -minimum 0 -maximum 1
    # SELECT [MinValue],[MaxValue],[DoNotDefault] ,[Disable] FROM [InsightsMetadata].[cm].[vProfileAttributes] WHERE profileattributekey = 'house.totalarea'
    $housetotalarea = Get-Random  -minimum 0 -maximum 100000
    #SELECT STUFF((SELECT ',"' + profileoptionkey  + '"'FROM [InsightsMetadata].[cm].[vProfileOptions] WHERE profileoptionkey LIKE 'house.style%' ORDER BY profileoptionkey asc FOR XML PATH('')) ,1,1,'') AS Txt 
    $housestylearray = "house.style.apartment","house.style.duplex","house.style.mobile","house.style.multifamily","house.style.raisedranch","house.style.singlefamily","house.style.townhouse"
    $housestyle = $housestylearray | Get-Random
    #SELECT STUFF((SELECT ',"' + profileoptionkey  + '"'FROM [InsightsMetadata].[cm].[vProfileOptions] WHERE profileoptionkey LIKE 'house.basementstyle%' ORDER BY profileoptionkey asc FOR XML PATH('')) ,1,1,'') AS Txt 
    $housebasementarray = "house.basementstyle.conditioned","house.basementstyle.slabongrade","house.basementstyle.unconditioned","house.basementstyle.unventedcrawl","house.basementstyle.ventedcrawl"
    $housebasementstyle = $housebasementarray | Get-Random
    #SELECT [MinValue],[MaxValue],[DoNotDefault] ,[Disable] FROM [InsightsMetadata].[cm].[vProfileAttributes] WHERE profileattributekey = 'house.people'
    $housepeople =  Get-Random -minimum 0 -maximum 20
    #SELECT [MinValue],[MaxValue],[DoNotDefault] ,[Disable] FROM [InsightsMetadata].[cm].[vProfileAttributes] WHERE profileattributekey = 'house.yearbuilt'
    #house.yearbuiltrange.after2010;house.yearbuiltrange.2010;house.yearbuiltrange.2005;house.yearbuiltrange.2000;house.yearbuiltrange.1995;house.yearbuiltrange.1985;house.yearbuiltrange.1975;house.yearbuiltrange.1965;house.yearbuiltrange.1955;house.yearbuiltrange.before1950
	$houseyearbuiltarry =  "house.yearbuiltrange.after2010","house.yearbuiltrange.2010","house.yearbuiltrange.2005","house.yearbuiltrange.2000","house.yearbuiltrange.1995","house.yearbuiltrange.1985","house.yearbuiltrange.1965","house.yearbuiltrange.1955","house.yearbuiltrange.before1950"
	$houseyearbuilttext = $houseyearbuiltarry | Get-Random
    #SELECT STUFF((SELECT ',"' + profileoptionkey  + '"'FROM [InsightsMetadata].[cm].[vProfileOptions] WHERE profileoptionkey LIKE 'heatsystem.style%' ORDER BY profileoptionkey asc FOR XML PATH('')) ,1,1,'') AS Txt 
    $heatsystemstylearray = "heatsystem.style.airsourceheatpump","heatsystem.style.baseboardresistance","heatsystem.style.basicfireplace","heatsystem.style.condensinggas","heatsystem.style.condensingoil","heatsystem.style.conventionalgas","heatsystem.style.conventionaloil","heatsystem.style.fireplaceinsert","heatsystem.style.forcedairfurnace","heatsystem.style.freestandingstove","heatsystem.style.geothermalheatpump","heatsystem.style.groundsourceheatpump","heatsystem.style.heatcircfireplace","heatsystem.style.heatingsystem","heatsystem.style.heatpumpgasbackup","heatsystem.style.heatpumpoilbackup","heatsystem.style.heatpumppropanebackup","heatsystem.style.highefficiencygas","heatsystem.style.highefficiencyoil","heatsystem.style.hotwaterradiant","heatsystem.style.radiant","heatsystem.style.steamboiler","heatsystem.style.waterboiler","heatsystem.style.woodheatingsystem"
    $heatsystemstyle = $heatsystemstylearray | Get-Random
    #SELECT [MinValue],[MaxValue],[DoNotDefault] ,[Disable] FROM [InsightsMetadata].[cm].[vProfileAttributes] WHERE profileattributekey = 'heatsystem.yearinstalled'
    $heatsystemyrarry = "heatsystem.yearinstalledrange.before1950","heatsystem.yearinstalledrange.1955","heatsystem.yearinstalledrange.1965","heatsystem.yearinstalledrange.1975","heatsystem.yearinstalledrange.1985","heatsystem.yearinstalledrange.1995","heatsystem.yearinstalledrange.2000","heatsystem.yearinstalledrange.2005","heatsystem.yearinstalledrange.2010","heatsystem.yearinstalledrange.after2010"
	$heatsystemyrinstall =   $heatsystemyrarry | Get-Random
    #SELECT STUFF((SELECT ',"' + profileoptionkey  + '"'FROM [InsightsMetadata].[cm].[vProfileOptions] WHERE profileoptionkey LIKE 'heatsystem.efficiency%' ORDER BY profileoptionkey asc FOR XML PATH('')) ,1,1,'') AS Txt 
    $heatsystemefficiencyarry = "heatsystem.efficiency.high","heatsystem.efficiency.standard"
    $heatsystemeff = $heatsystemefficiencyarry | Get-Random
    #SELECT [MinValue],[MaxValue],[DoNotDefault] ,[Disable] FROM [InsightsMetadata].[cm].[vProfileAttributes] WHERE profileattributekey = 'waterheater.year'
    $waterheateryeararray = "waterheater.yearrange.before1950","waterheater.yearrange.1955","waterheater.yearrange.1965","waterheater.yearrange.1975","waterheater.yearrange.1985","waterheater.yearrange.1995","waterheater.yearrange.2000","waterheater.yearrange.2005","waterheater.yearrange.after2010"
    $waterheateryear = $waterheateryeararray | Get-Random
	#SELECT STUFF((SELECT ',"' + profileoptionkey  + '"'FROM [InsightsMetadata].[cm].[vProfileOptions] WHERE profileoptionkey LIKE 'waterheater.efficiencyvalue%' ORDER BY profileoptionkey asc FOR XML PATH('')) ,1,1,'') AS Txt 
    #NOTE: note sure what this will be - not in system yet - providing 0 and 1 for value now
    $waterhtrefficiencyarry = "0","1"
    $waterhtreff = $waterhtrefficiencyarry | Get-Random
    #NOTE: new value - not in content yet
    $housewindowqualarry = "house.windowqualitypoor.yes", "house.windowqualitypoor.no"
    $housewindowqual = $housewindowqualarry | Get-Random
    #NOTE: new value not in content yet
    $houseinsulqualarry = "house.insulationqualitypoor.yes", "house.insulationqualitypoor.no"
    $houseinsulqual = $houseinsulqualarry | Get-Random
    #no min or max value defined
    $roomaccount = Get-Random -minimum 0 -maximum 1
	#when not using the range
	 $houseyearbuilt224 =  Get-Random -minimum 1700 -maximum 2015
     $heatsystemyrinstall224 =  Get-Random -minimum 1900 -maximum 2015
     $waterheateryear224 = Get-Random -minimum 1900 -maximum 2015
	 #is  value not a yes no boolean for 224 for the waterheater efficiency
	 $waterheaterefficiency224 = "0"

    $arraylistfinal = $custid + "|" + $acct + "|" + $prem + "|" + $waterheaterfuel  + "|" + $heatsystemfuel  + "|" + $centralac_count  + "|" +  $housetotalarea + "|" + $housestyle + "|" + $housebasementstyle + "|" + $housepeople   + "|" + $houseyearbuilt + "|" + $heatsystemstyle + "|" + $heatsystemyrinstall  + "|" + $heatsystemeff + "|" + $waterheateryear + "|" + $waterhtreff + "|" + $housewindowqual + "|" + $houseinsulqual + "|" + $roomaccount + "`n" 
	$arraylistfinal224 =  $custid + "|" + $acct + "|" + $prem + "|" + $waterheaterfuel  + "|" + $heatsystemfuel  + "|" + $centralac_count  + "|" +  $housetotalarea + "|" + $housestyle + "|" + $housebasementstyle + "|" + $housepeople   + "|" + $houseyearbuilt224 + "|" + $heatsystemstyle + "|" + $heatsystemyrinstall224  + "|" + $heatsystemeff + "|" + $waterheateryear224 + "|" + $waterheaterefficiency224  + "|" + $housewindowqual + "|" + $houseinsulqual + "|" + $roomaccount + "`n" 
    #properties
	$prop = 'customerID|accountID|premiseId|waterheater.fuel|heatsystem.fuel|centralac.count|house.totalarea|house.style|house.basementstyle|house.people|house.yearbuilt|heatsystem.style|heatsystem.yearinstalled|heatsystem.efficiencyvalue|waterheater.year|waterheater.efficiencyvalue|house.windowqualitypoor|house.insulationqualitypoor|roomac.count'
	$prop224 = 'customerid|accountid|premiseid|waterheater.fuel|heatsystem.fuel|centralac.count|house.totalarea|house.style|house.basementstyle|house.people|house.yearbuilt|heatsystem.style|heatsystem.yearinstalled|heatsystem.efficiencyvalue|waterheater.year|waterheater.efficiencyvalue|house.windowqualitypoor|house.insulationqualitypoor|roomac.count'
    #first row add the column name above - rest of time, no name needed or get error
    if ($datatext -eq 1) {
	    if ($clientnum -ne "224")
		{
         $psObject | add-member -MemberType NoteProperty -name $prop -value $arraylistfinal 
		}
		else 
		{
		 $psObject | add-member -MemberType NoteProperty -name $prop224 -value $arraylistfinal224 
		}
    }
     else {
	     if ($clientnum -ne "224")
         {
		  $psObject.$prop += $arraylistfinal
		  }
		 else
		 {
		  $psObject.$prop224 += $arraylistfinal224
		  }
     
     }
 
    }
    $psObject  |Export-CSV $outfilefn  -notype -delimiter "|"
    #replace the quotes with nothing
    (Get-Content $outfilefn) | % {$_ -replace '"', ""} | out-file -FilePath $outfilefn -Force -Encoding ascii
	#take out blank lines at end
    (Get-Content $outfilefn) | ? {$_.trim() -ne "" } | set-content $outfilefn
	#creation file completed move to preprocess location
	#if file exists copy it to the preprocess location
	if((Test-Path -Path $outfilefn))
	{   Write-Host "copying file" $outfilefn " to:"  $inputpreprocessprofiledatapath
		Copy-Item $outfilefn $inputpreprocessprofiledatapath
	}

 }
} 



#function to generate bill test data
function genbilltestdata
{
Param(
   
  #resultfilename
  [Parameter(Mandatory=$true)]
  [string]
  $resultsFileName
  

)

Process 
{

    $outfilefn = $resultPathbill  + $resultsFileName + $timestamp + ".tab"

    $psObject = $null
    $psObject = New-Object psobject
    # $numRows is the loop count, set to default or can override
     [System.Collections.ArrayList]$seedArrayList = 1..$numRows
    $seeds=$seedArrayList
    foreach($datatext in $seeds)
    {
    $custid = "cust" + $seedprefix + "_" + $datatext
    $acct = "acct" + $seedprefix + "_" + $datatext
    $prem = "prem" + $seedprefix + "_" + $datatext
	$serv = "serv" + $seedprefix + "_" + $datatext
    $mail_address_line_1 
	$mail_address_line_2  = ""
	$mail_address_line_3 = ""
	$mail_city = "LOS ANGELES"
	$mail_state = "CA"
	$mailziparry = "90022-2201", "90640-4405", "90022-2236"
	$mail_zip_code = $mailziparry | Get-Random
	$first_name = $seedprefix + $datatext
	$last_name = $seedprefix + $datatext
	$phone_1 = ""
	$phone_2 = ""
	$email = $seedprefix + $datatext + "@verizon.net"
	$customer_type = "RESIDENTIAL"
	$active_date = ""
	$inactive_date = ""
	$read_cycle = "1"
	$rate_code = "GR"
	$programsarry = "SMOKE081", "SMOKE03", "SMOKE04", "SMOKE05", "SMOKE06", " "
	$programs =  $programsarry | Get-Random
	$service_house_number  = Get-Random -minimum 0 -maximum 500
	$streetnamearry = "SMITH STREET", "MEANDERING RD", "VIA CORONA ST", "N 1ST ST", "N 2ND ST", "N 3RD ST", "N 4TH ST"
    $service_street_name = $streetnamearry | Get-Random
	$service_unit = ""
	$service_city = "LOS ANGELES"
	$service_state = "CA"
	$serviceziparry = "90022-2201", "90640-4405", "90022-2236"
	$service_zip_code = $serviceziparry | Get-Random
	$meter_type = "G"
    $meter_units = "T"
	$bldg_sq_foot = Get-Random  -minimum 0 -maximum 100000
	#for basic format 101, 256, 87 has to be 4 digit year, not the list range for other clientids
	$year_built  = Get-Random -minimum 1800 -maximum 2015
	$bedrooms = Get-Random -minimum 1 -maximum 20
	$assess_value = Get-Random -minimum 1 -maximum 50000
	$usage_value = Get-Random -minimum 1 -maximum 800
	$date_to  = "7/31/2015"
	$duration = "31"
	$is_estimate = "0"
	$usage_charge = Get-Random -minimum 80 -maximum 1000
    $usage_charge = "{0:N2}" -f $usage_charge 


    #properties
    $arraylistfinal = $custid + "`t" +  $prem  + "`t" +  $mail_address_line_1  + "`t" + $mail_address_line_2 + "`t"  +  $mail_address_line_3  + "`t" +   $mail_city + "`t"  +  $mail_state  + "`t" +  $mail_zip_code  + "`t" + $first_name  + "`t" +  $last_name  + "`t" +  $phone_1  + "`t" +  $phone_2  + "`t" +   $email  + "`t" + $customer_type + "`t" + $acct  + "`t" + $active_date + "`t" + $inactive_date + "`t" +  $read_cycle + "`t" +  $rate_code  + "`t" +  $programs + "`t"  + $serv + "`t" +  $service_house_number  + "`t" + $service_street_name  + "`t" +  $service_unit + "`t" + $service_city  + "`t" +  $service_state + "`t"  +  $service_zip_code + "`t" + $meter_type + "`t" + $meter_units  + "`t" + $bldg_sq_foot + "`t" +  $year_built + "`t" + $bedrooms + "`t" +  $assess_value + "`t"  +  $usage_value  + "`t" +  $date_to + "`t" + $duration + "`t" + $is_estimate  + "`t" + $usage_charge + "`n"
    #properties
    $prop = "customer_id" + "`t" + "premise_id" + "`t" + "mail_address_line_1" + "`t" + "mail_address_line_2" + "`t" + "mail_address_line_3" + "`t" + "mail_city" + "`t" + "mail_state" + "`t" + "mail_zip_code" + "`t" + "first_name" + "`t" +	"last_name" + "`t" + "phone_1" + "`t" +	"phone_2" + "`t" + "email"	+ "`t" + "customer_type" + "`t" + "account_id" + "`t"+ "active_date" + "`t" + "inactive_date" + "`t" + "read_cycle" + "`t" + "rate_code" + "`t" + "programs" + "`t" + "service_point_id" + "`t" + "service_house_number" + "`t" + "service_street_name" + "`t" + "service_unit"	+ "`t" + "service_city"	 + "`t" + "service_state" + "`t" + "service_zip_code" + "`t" + "meter_type" + "`t"  + "`t" + "meter_units"	+ "`t" + "bldg_sq_foot" + "`t" + "year_built" + "`t" +	"bedrooms" + "`t" +	"assess_value" + "`t" +	"usage_value" + "`t" +	"date_to" + "`t" +	"duration" + "`t" +	"is_estimate"+ "`t" + "usage_charge" + "`n"
   
   #first row add the column name above - rest of time, no name needed or get error
    if ($datatext -eq 1) {
        $psObject | add-member -MemberType NoteProperty -name $prop -value $arraylistfinal 
 
    }
     else {
        $psObject.$prop += $arraylistfinal
         
     }
 
    }
    $psObject  |Export-CSV $outfilefn  -notype -delimiter "`t"
    #replace the quotes with nothing
    (Get-Content $outfilefn) | % {$_ -replace '"', ""} | out-file -FilePath $outfilefn -Force -Encoding ascii
	#take out blank lines at end
    (Get-Content $outfilefn) | ? {$_.trim() -ne "" } | set-content $outfilefn
	#creation file completed move to preprocess location
	#if file exists copy it to the preprocess location
	if((Test-Path -Path $outfilefn))
	{   Write-Host "copying file" $outfilefn " to:"  $inputpreprocessbilldatapath
		Copy-Item $outfilefn $inputpreprocessbilldatapath
	}

 }
} 




#function to generate profile test data in Extended format pipe delimited
function genbillextend
{
Param(
   
  #resultfilename
  [Parameter(Mandatory=$true)]
  [string]
  $resultsFileName
)

Process 
{

    $outfilefn = $resultPathextendbill + $resultsFileName + $timestamp + ".txt"

    $psObject = $null
    $psObject = New-Object psobject
    # $numRows is the loop count, set to default or can override
     [System.Collections.ArrayList]$seedArrayList = 1..$numRows
    $seeds=$seedArrayList
    foreach($datatext in $seeds)
    {
    $custid = "cust" + $seedprefix + "_" + $datatext
    $acct = "acct" + $seedprefix + "_" + $datatext
    $prem = "prem" + $seedprefix + "_" + $datatext
	$serv = "serv" + $seedprefix + "_" + $datatext
	#optionalfield is now being constructed from serviceid
	$service_contract = ""
    $mail_address_line_1 
	$mail_address_line_2  = ""
	$mail_address_line_3 = ""
	$mail_city = "LOS ANGELES"
	$mail_state = "CA"
	$mailziparry = "90022-2201", "90640-4405", "90022-2236"
	$mail_zip_code = $mailziparry | Get-Random
	$first_name = $seedprefix + $datatext
	$last_name = $seedprefix + $datatext
	$phone_1 = ""
	$phone_2 = ""
	$email = $seedprefix + $datatext + "@verizon.net"
	$customer_type = "RESIDENTIAL"
	$active_date = ""
	$inactive_date = ""
	$read_cycle = "1"
	$rate_code = "GR"
	$programsarry = "BudgetBill", "Level Pay Plan", "My Energy", "MedicalBill", " ", "BudgetBill, MedicalBill", "BudgetBill, My Energy"
	$programs =  $programsarry | Get-Random
	$service_house_number  = Get-Random -minimum 0 -maximum 500
	$streetnamearry = "SMITH STREET", "MEANDERING RD", "VIA CORONA ST", "N 1ST ST", "N 2ND ST", "N 3RD ST", "N 4TH ST"
    $service_street_name = $streetnamearry | Get-Random
	$service_unit = ""
	$service_city = "ORANGE"
	$service_state = "NJ"
	$serviceziparry = "07006", "07007", "07006-224"
	$service_zip_code = $serviceziparry | Get-Random
	$meter_type = "AMR"
	#mapping between UIL meter_units to UOM (see DimUOM):    meter_units = '1' is (3) Water; meter_units = '2' is (2) Gal; meter_units '3' is (1) Kwh; meter_unit=6 is (4) trees; meter_units =4 is  (5) lbs; meter_units 5 is (6) therms
    $meter_units = "3"
	$bldg_sq_foot = Get-Random  -minimum 0 -maximum 100000
	$bedrooms = Get-Random -minimum 1 -maximum 20
	#$year_built = Get-Random -minimum 1700 -maximum 2015
	$houseyearbuiltarry =  "house.yearbuiltrange.after2010","house.yearbuiltrange.2010","house.yearbuiltrange.2005","house.yearbuiltrange.2000","house.yearbuiltrange.1995","house.yearbuiltrange.1985","house.yearbuiltrange.1965","house.yearbuiltrange.1955","house.yearbuiltrange.before1950"
	$year_built = $houseyearbuiltarry | Get-Random
	if ($clientnum -eq "224")
	{
	  $year_built = Get-Random -minimum 1700 -maximum 2015
	}
	$assess_value = Get-Random -minimum 1 -maximum 50000
	$usage_value = Get-Random -minimum 1 -maximum 800
	$bill_enddate  = "20150630"
	$bill_days = "31"
	$is_estimate = "0"
	$usage_charge = Get-Random -minimum 80 -maximum 1000
    $usage_charge = "{0:N2}" -f $usage_charge 
	#see DimCommodity CommodityKey	1=electric;2=gas;3=water; 4=sewer;5=garbage
    $service_commodity =  "1"
	$account_structure_type  = "1"
	$meter_id = Get-Random -minimum 76669999 -maximum 444454999
	$billperiod_type = "1"
	$meter_replacesmeterid = ""
    $service_read_date = "20150812"
	$service_read_startdate = "20150701"

    #properties
    $arraylistfinal = $custid + "|" +  $prem  +  "|" +  $mail_address_line_1  +  "|" + $mail_address_line_2 +  "|"  +  $mail_address_line_3  +  "|" +   $mail_city +  "|"  +  $mail_state  +  "|" +  $mail_zip_code  +  "|" + $first_name  +  "|" +  $last_name  +  "|" +  $phone_1  +  "|" +  $phone_2  +  "|" +   $email  +  "|" + $customer_type +  "|" + $acct  +  "|" + $active_date +  "|" + $inactive_date +  "|" +  $read_cycle +  "|" +  $rate_code  +  "|" +  $service_house_number  + "|" + $service_street_name  +  "|" +  $service_unit +  "|" + $service_city  +  "|" +  $service_state +  "|"  +  $service_zip_code +  "|" + $meter_type +  "|" + $meter_units  +  "|" + $bldg_sq_foot + "|" +  $year_built + "|" + $bedrooms + "|" +  $assess_value + "|"  +  $usage_value  + "|" +  $bill_enddate + "|" + $bill_days + "|" + $is_estimate + "|" + $usage_charge + "|" + $clientnum   + "|"  +  $programs  + "|" + $service_commodity  + "|"  + $account_structure_type  + "|" + $meter_id + "|" + $billperiod_type + "|" + $meter_replacesmeterid + "|"  +  $service_read_date +  "|"  +  $service_read_startdate + "`n"
    #properties
    $prop = "customer_id" + "|" + "premise_id" + "|" + "mail_address_line_1" + "|" + "mail_address_line_2" + "|" + "mail_address_line_3" + "|" + "mail_city" + "|" + "mail_state" + "|" + "mail_zip_code" + "|" + "first_name" + "|" +	"last_name" + "|" + "phone_1" + "|" +"phone_2" + "|" + "email"	+ "|" + "customer_type" + "|" + "account_id" + "|" + "active_date" + "|" + "inactive_date" + "|" + "read_cycle" + "|" + "rate_code"  + "|" + "service_house_number" + "|" + "service_street_name" + "|" + "service_unit"	+ "|" + "service_city"	 + "|" + "service_state" + "|" + "service_zip_code" + "|" + "meter_type" + "|"  + "meter_units" + "|" + "bldg_sq_foot" + "|" + "year_built" + "|" +	"bedrooms" + "|" +	"assess_value" + "|" +	"usage_value" + "|" +	"bill_enddate" + "|" +"bill_days" + "|" +"is_estimate"+ "|" + "usage_charge" + "|" + "ClientId" + "|" + "Programs" + "|" + "service_commodity"  + "|"  + "account_structure_type"  + "|" + "meter_id" + "|" + "billperiod_type" + "|" + "meter_replaces_meterid" + "|"  +  "service_read_date" + "|" + "service_read_startdate" + "`n"
    #difference in format for 276, so added different properties
	$prop276 = "Customer_id" + "|" + "Premise_id" + "|" + "Mail_address_line_1" + "|" + "Mail_address_line_2" + "|" + "Mail_address_line_3" + "|" + "Mail_city" + "|" + "Mail_state"  + "|" + "Mail_zip_code" + "|" + "First_name" + "|" + "Last_name" + "|" + "Phone_1" + "|" + "Phone_2" + "|" +  "Email" + "|" + "Customer_type" + "|" + "Account_id" + "|" + "Active_date" + "|" + "Inactive_date" + "|" + "Read_cycle" + "|" + "Rate_code" + "|" + "Service_point_id" + "|" + "Service_house_number" + "|" + "Service_street_name" + "|" + "Service_unit" + "|" + "Service_city" + "|" + "Service_state" + "|" + "Service_zip_code" + "|" + "Meter_type" + "|" + "Meter_units" + "|" +  "Bldg_sq_foot" + "|" + "Year_built" + "|" + "Bedrooms" + "|" + "Assess_value" + "|" + "Usage_value" + "|" + "Bill_enddate" + "|" + "Bill_days" + "|" + "Is_estimate" + "|" + "Usage_charge" + "|" + "Clientid" + "|" + "Programs" + "|" + "Service_commodity" + "|" + "Account_structure_type" + "|" + "Meter_id" + "|" + "Billperiod_type" + "|" + "Meter_replaces_meterid" + "|" + "Service_read_date" + "|" + "Service_contract" + "`n"
    $arraylistfinal276 = $custid + "|" +  $prem  +  "|" +  $mail_address_line_1  +  "|" + $mail_address_line_2 +  "|"  +  $mail_address_line_3  +  "|" +   $mail_city +  "|"  +  $mail_state  +  "|" +  $mail_zip_code  +  "|" + $first_name  +  "|" +  $last_name  +  "|" +  $phone_1  +  "|" +  $phone_2  +  "|" +   $email  +  "|" + "RES" +  "|" + $acct  +  "|" + $active_date +  "|" + $inactive_date +  "|" +  $read_cycle +  "|" +  "MGE-RSM"  +  "|" + $serv + "|" +  $service_house_number  + "|" + $service_street_name  +  "|" +  $service_unit +  "|" + $service_city  +  "|" +  $service_state +  "|"  +  $service_zip_code +  "|" + $meter_type +  "|" + "1"  +  "|" + $bldg_sq_foot + "|" +  $year_built + "|" + $bedrooms + "|" +  $assess_value + "|"  +  $usage_value  + "|" +  $bill_enddate + "|" + $bill_days + "|" + $is_estimate + "|" + $usage_charge + "|" + $clientnum   + "|"  +  ""  + "|" + "2"  + "|"  + $account_structure_type  + "|" + $meter_id + "|" + $billperiod_type + "|" + $meter_replacesmeterid + "|"  +  $service_read_date + "T00:00" + "|"  +  $service_contract + "`n"

	
	#first row add the column name above - rest of time, no name needed or get error, different format for 276 so base output on that
    if ($datatext -eq 1) {
	   if ($clientnum -ne "276") 
	   {
        $psObject | add-member -MemberType NoteProperty -name $prop -value $arraylistfinal 
	   }
	   else
	   {
	      $psObject | add-member -MemberType NoteProperty -name $prop276 -value $arraylistfinal276 
	   }

    }
     else {
	    if ($clientnum -ne "276") 
        {
		$psObject.$prop += $arraylistfinal
		}
	   else
       {
		$psObject.$prop276 += $arraylistfinal276
	   } 
     }
 
    }
    $psObject  |Export-CSV $outfilefn  -notype -delimiter "|"
    #replace the quotes with nothing
    (Get-Content $outfilefn) | % {$_ -replace '"', ""} | out-file -FilePath $outfilefn -Force -Encoding ascii
	#take out blank lines at end
    (Get-Content $outfilefn) | ? {$_.trim() -ne "" } | set-content $outfilefn
	#creation file completed move to preprocess location
	#if file exists copy it to the preprocess location
	if((Test-Path -Path $outfilefn))
	{   Write-Host "copying file" $outfilefn " to:"  $inputpreprocessextendbilldatapath
		Copy-Item $outfilefn $inputpreprocessextendbilldatapath
	}

 }
} 


#----------Put all calls for calling functions above----------------------------------------------------------------------------
#put any calls to either genaction test data or gen profile test data. Supply the desired file name which will be created in folder with datetimestamp
#genaction or genprofile must be set to 1 on powershell script for each one for it to generate data otherwise the file is not generated for the specified actions or profiledata

if ($genaction -eq 1)
{ 
	#changefile name based on the clientid
	if($clientnum -eq "224")
	{
	   genactionitemtestdata -resultsFileName "aclara_cdw_actions_" -fileExt ".txt"
	}
	elseif ($clientnum -eq "256")
	{
	   genactionitemtestdata -resultsFileName "actionitem_" -fileExt ".txt"
	}
	elseif ($clientnum -eq "87")
	{
	   genactionitemtestdata -resultsFileName "actionitem_" -fileExt ".txt"
	}
	else {
	  genactionitemtestdata -resultsFileName "actionitem_" -fileExt ".csv"
	}
}

if ($genprofile -eq 1)
{
	#changefile name based on the clientid
	if($clientnum -eq "224")
	{
	   genprofiletestdata -resultsFileName "aclara_cdw_profile_" -fileExt ".txt"
	}
	else {
	  genprofiletestdata -resultsFileName "profiledata_"  -fileExt ".csv"
	}

}

if ($genbill -eq 1)
{
	#if billformat is not supplied for the desired generated bill, set to basic otherwise 
	#use the billformat from input if valid (basic, extend)
	#We can call SCG format Basic Bill Format - format would be 'basic' for input to this script
	#And UIL  Extended Bill Format - format would be 'extend' for input to this script

	if([string]::IsNullOrEmpty($billformat) -eq $true)
    {
        $billformat = "basic"
        Write-Host "bill format set to default of basic"
    }


	switch ($billformat)
	{
	  basic 
	  {
		 genbilltestdata -resultsFileName "AMConservation_"
		 break;
	  }
	  extend 
	  {
	    if($clientnum -eq "224")
		{
		  genbillextend -resultsFileName "aclara_cdw_sap_" 
		}
		elseif ($clientnum -eq 276)
		{
		 genbillextend -resultsFileName "billing_"
		}
		else {
		  genbillextend -resultsFileName "bill_"
		}
		 break;
	  }
	  default 
	  {
	   write-host "Invalid bill file format $billformat";
		return;
		break;
	  }
	}

     
}