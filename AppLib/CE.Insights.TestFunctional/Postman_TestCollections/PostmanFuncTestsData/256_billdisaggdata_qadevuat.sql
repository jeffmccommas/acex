USE [Insights]
GO

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (256
           ,'1056927029'
           ,'64060526'
           ,'2014-09-01 00:00:00.000'
           ,'64060500'
           ,4
           ,16
           ,1
           ,78.1
           ,1
           ,12.65
           ,1
           ,'2014-09-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (256
           ,'1056927029'
           ,'64060526'
           ,'2014-09-01 00:00:00.000'
           ,'64060500'
           ,6
           ,20
           ,1
           ,92.3
           ,1
           ,14.95
           ,1
           ,'2014-09-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (256
           ,'1056927029'
           ,'64060526'
           ,'2014-09-01 00:00:00.000'
           ,'64060500'
           ,3
           ,18
           ,1
           ,170.4
           ,1
           ,27.6
           ,1
           ,'2014-09-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (256
           ,'1056927029'
           ,'64060526'
           ,'2014-09-01 00:00:00.000'
          ,'64060500'
           ,5
           ,23
           ,1
           ,0
           ,1
           ,0
           ,1
           ,'2014-09-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (256
           ,'1056927029'
           ,'64060526'
           ,'2014-09-01 00:00:00.000'
           ,'64060500'
           ,1
           ,3
           ,1
           ,205.9
           ,1
           ,33.35
           ,1
           ,'2014-09-01 00:00:00.000')
GO

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (256
           ,'1056927029'
           ,'64060526'
           ,'2014-09-01 00:00:00.000'
          ,'64060500'
           ,7
           ,21
           ,1
           ,42.6
           ,1
           ,6.9
           ,1
           ,'2014-09-01 00:00:00.000')
GO

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (256
           ,'1056927029'
           ,'64060526'
           ,'2014-09-01 00:00:00.000'
          ,'64060500'
           ,0
           ,19
           ,1
           ,71
           ,1
           ,11.5
           ,1
           ,'2014-09-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (256
           ,'1056927029'
           ,'64060526'
           ,'2014-09-01 00:00:00.000'
            ,'64060500'
           ,0
           ,6
           ,1
           ,49.7
           ,1
           ,8.05
           ,1
           ,'2014-09-01 00:00:00.000')
GO
--october
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (256
           ,'1056927029'
           ,'64060526'
           ,'2014-10-01 00:00:00.000'
          ,'64060500'
           ,4
           ,16
           ,1
           ,175.5
           ,1
           ,27.5
           ,1
           ,'2014-10-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (256
           ,'1056927029'
           ,'64060526'
           ,'2014-10-01 00:00:00.000'
           ,'64060500'
           ,3
           ,18
           ,1
           ,280.8
           ,1
           ,44
           ,1
           ,'2014-10-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (256
           ,'1056927029'
           ,'64060526'
           ,'2014-10-01 00:00:00.000'
          ,'64060500'
           ,5
           ,23
           ,1
           ,70.2
           ,1
           ,11
           ,1
           ,'2014-10-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (256
           ,'1056927029'
           ,'64060526'
           ,'2014-10-01 00:00:00.000'
 ,'64060500'
           ,7
           ,21
           ,1
           ,56.16
           ,1
           ,8.8
           ,1
           ,'2014-10-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (256
           ,'1056927029'
           ,'64060526'
           ,'2014-10-01 00:00:00.000'
         ,'64060500'
           ,0
           ,19
           ,1
           ,70.2
           ,1
           ,11
           ,1
           ,'2014-10-01 00:00:00.000')
GO

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (256
           ,'1056927029'
           ,'64060526'
           ,'2014-10-01 00:00:00.000'
       ,'64060500'
           ,0
           ,6
           ,1
           ,49.14
           ,1
           ,7.7
           ,1
           ,'2014-10-01 00:00:00.000')
GO

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (256
           ,'1056927029'
           ,'64060526'
           ,'2014-10-01 00:00:00.000'
          ,'64060500'
           ,6
           ,20
           ,1
           ,45
           ,1
           ,7.3
           ,1
           ,'2014-10-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (256
           ,'1056927029'
           ,'64060526'
           ,'2014-10-01 00:00:00.000'
         ,'64060500'
           ,1
           ,3
           ,1
           ,107
           ,1
           ,16.8
           ,1
           ,'2014-10-01 00:00:00.000')
GO
--end october
--november
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (256
           ,'1056927029'
           ,'64060526'
           ,'2014-11-01 00:00:00.000'
        ,'64060500'
           ,4
           ,16
           ,1
           ,167.5
           ,1
           ,21.25
           ,1
           ,'2014-11-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (256
           ,'1056927029'
           ,'64060526'
           ,'2014-11-01 00:00:00.000'
          ,'64060500'
           ,6
           ,20
           ,1
           ,0
           ,1
           ,0
           ,1
           ,'2014-11-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (256
           ,'1056927029'
           ,'64060526'
           ,'2014-11-01 00:00:00.000'
        ,'64060500'
           ,3
           ,18
           ,1
           ,268
           ,1
           ,34
           ,1
           ,'2014-11-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (256
           ,'1056927029'
           ,'64060526'
           ,'2014-11-01 00:00:00.000'
        ,'64060500'
           ,5
           ,23
           ,1
          , 67
           ,1
           ,8.5
           ,1
           ,'2014-11-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (256
           ,'1056927029'
           ,'64060526'
           ,'2014-11-01 00:00:00.000'
            ,'64060500'
           ,1
           ,3
           ,1
           ,0
           ,1
           ,0
           ,1
           ,'2014-11-01 00:00:00.000')
GO

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (256
           ,'1056927029'
           ,'64060526'
           ,'2014-11-01 00:00:00.000'
          ,'64060500'
           ,7
           ,21
           ,1
           ,53.6
           ,1
           ,6.8
           ,1
           ,'2014-11-01 00:00:00.000')
GO

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (256
           ,'1056927029'
           ,'64060526'
           ,'2014-11-01 00:00:00.000'
        ,'64060500'
           ,0
           ,19
           ,1
           ,67
           ,1
           ,8.5
           ,1
           ,'2014-11-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (256
           ,'1056927029'
           ,'64060526'
           ,'2014-11-01 00:00:00.000'
          ,'64060500'
           ,0
           ,6
           ,1
           ,46.9
           ,1
           ,5.95
           ,1
           ,'2014-11-01 00:00:00.000')
GO
--end november
--december
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (256
           ,'1056927029'
           ,'64060526'
           ,'2014-12-01 00:00:00.000'
    ,'64060500'
           ,4
           ,16
           ,1
           ,165
           ,1
           ,20
           ,1
           ,'2014-12-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (256
           ,'1056927029'
           ,'64060526'
           ,'2014-12-01 00:00:00.000'
           ,'64060500'
           ,6
           ,20
           ,1
           ,0
           ,1
           ,0
           ,1
           ,'2014-12-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (256
           ,'1056927029'
           ,'64060526'
           ,'2014-12-01 00:00:00.000'
     ,'64060500'
           ,3
           ,18
           ,1
           ,264
           ,1
           ,32
           ,1
           ,'2014-12-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (256
           ,'1056927029'
           ,'64060526'
           ,'2014-12-01 00:00:00.000'
        ,'64060500'
           ,5
           ,23
           ,1
          , 66
           ,1
           ,8
           ,1
           ,'2014-12-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (256
           ,'1056927029'
           ,'64060526'
           ,'2014-12-01 00:00:00.000'
           ,'64060500'
           ,1
           ,3
           ,1
           ,0
           ,1
           ,0
           ,1
           ,'2014-12-01 00:00:00.000')
GO

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (256
           ,'1056927029'
           ,'64060526'
           ,'2014-12-01 00:00:00.000'
       ,'64060500'
           ,7
           ,21
           ,1
           ,52.8
           ,1
           ,6.4
           ,1
           ,'2014-12-01 00:00:00.000')
GO

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (256
           ,'1056927029'
           ,'64060526'
           ,'2014-12-01 00:00:00.000'
           ,'64060500'
           ,0
           ,19
           ,1
           ,66
           ,1
           ,8
           ,1
           ,'2014-12-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (256
           ,'1056927029'
           ,'64060526'
           ,'2014-12-01 00:00:00.000'
         ,'64060500'
           ,0
           ,6
           ,1
           ,46.2
           ,1
           ,5.6
           ,1
           ,'2014-12-01 00:00:00.000')
GO
--end december
--Test data fr billdisagg for TC7174 for water - also use postman test for  profile section attributes : profilev1_post_responsesuccess_profilesectionhome

INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCus7174', 'TCAcct7174', '2014-12-01 00:00:00.000', 'TCPrem7174', 8, 35, 5, 3.23100, 2, 5.1023, 1, '2014-06-05 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-01-01 00:00:00.000', 'TCPrem7174', 2, 9, 5, 5.33000, 5, 16.3200, 1, '2014-06-05 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-01-01 00:00:00.000', 'TCPrem7174', 9, 35, 5, 1.34000, 5, 3.2300, 1, '2014-06-05 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-01-01 00:00:00.000', 'TCPrem7174', 9, 36, 5, 4.23000, 5, 7.1100, 1, '2014-06-05 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-02-01 00:00:00.000', 'TCPrem7174', 2, 9, 5, 5.24000, 5, 14.2100, 1, '2014-06-05 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-02-01 00:00:00.000', 'TCPrem7174', 9, 35, 5, 2.33000, 5, 4.5100, 1, '2014-06-05 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-02-01 00:00:00.000', 'TCPrem7174', 9, 36, 5, 3.10000, 5, 6.2300, 1, '2014-06-05 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-03-01 00:00:00.000', 'TCPrem7174', 2, 9, 5, 6.23000, 5, 15.9100, 1, '2014-06-05 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-03-01 00:00:00.000', 'TCPrem7174', 9, 35, 5, 2.10000, 5, 4.1430, 1, '2014-06-05 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-03-01 00:00:00.000', 'TCPrem7174', 9, 36, 5, 5.23000, 5, 8.4400, 1, '2014-06-05 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-04-01 00:00:00.000', 'TCPrem7174', 2, 9, 5, 4.31000, 5, 12.3900, 1, '2014-06-05 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-04-01 00:00:00.000', 'TCPrem7174', 2, 27, 5, 13.80000, 5, 20.7000, 1, '2014-06-05 13:19:41.083')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-04-01 00:00:00.000', 'TCPrem7174', 9, 35, 5, 3.39000, 5, 5.3298, 1, '2014-06-05 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-04-01 00:00:00.000', 'TCPrem7174', 9, 36, 5, 4.33000, 5, 7.5600, 1, '2014-06-05 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-05-01 00:00:00.000', 'TCPrem7174', 2, 8, 5, 12.44000, 5, 59.4300, 1, '2014-06-05 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-05-01 00:00:00.000', 'TCPrem7174', 2, 9, 5, 4.99000, 5, 13.9100, 1, '2014-06-05 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-05-01 00:00:00.000', 'TCPrem7174', 2, 27, 5, 4.60000, 5, 7.3600, 1, '2014-06-05 13:19:41.163')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-05-01 00:00:00.000', 'TCPrem7174', 9, 35, 5, 4.98000, 5, 6.8200, 1, '2014-06-05 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-05-01 00:00:00.000', 'TCPrem7174', 9, 36, 5, 6.87000, 5, 9.6700, 1, '2014-06-05 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-06-01 00:00:00.000', 'TCPrem7174', 2, 8, 5, 13.98000, 5, 61.4300, 1, '2014-06-05 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-06-01 00:00:00.000', 'TCPrem7174', 2, 9, 5, 3.45000, 5, 5.3400, 1, '2014-06-05 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-06-01 00:00:00.000', 'TCPrem7174', 2, 27, 5, 5.30000, 5, 8.9400, 1, '2014-06-05 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-06-01 00:00:00.000', 'TCPrem7174', 9, 35, 5, 3.72310, 5, 5.9230, 1, '2014-06-05 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-06-01 00:00:00.000', 'TCPrem7174', 9, 36, 5, 7.54000, 5, 10.2300, 1, '2014-06-05 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-07-01 00:00:00.000', 'TCPrem7174', 2, 8, 5, 16.23000, 5, 84.0000, 1, '2014-06-05 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-07-01 00:00:00.000', 'TCPrem7174', 2, 9, 5, 5.33000, 5, 4.3300, 1, '2014-06-05 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-07-01 00:00:00.000', 'TCPrem7174', 2, 27, 5, 6.14000, 5, 12.3300, 1, '2014-06-05 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-07-01 00:00:00.000', 'TCPrem7174', 9, 35, 5, 2.82380, 5, 4.6523, 1, '2014-06-05 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-07-01 00:00:00.000', 'TCPrem7174', 9, 36, 5, 8.99000, 5, 11.4300, 1, '2014-06-05 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-08-01 00:00:00.000', 'TCPrem7174', 2, 8, 5, 17.32000, 5, 96.2300, 1, '2014-06-05 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-08-01 00:00:00.000', 'TCPrem7174', 2, 9, 5, 4.87000, 5, 3.5400, 1, '2014-06-05 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-08-01 00:00:00.000', 'TCPrem7174', 2, 27, 5, 6.33000, 5, 11.5600, 1, '2014-06-05 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-08-01 00:00:00.000', 'TCPrem7174', 9, 35, 5, 3.98430, 5, 6.1320, 1, '2014-06-05 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-08-01 00:00:00.000', 'TCPrem7174', 9, 36, 5, 7.32000, 5, 9.9600, 1, '2014-06-05 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-09-01 00:00:00.000', 'TCPrem7174', 2, 8, 5, 18.23000, 5, 101.4300, 1, '2014-06-05 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-09-01 00:00:00.000', 'TCPrem7174', 2, 9, 5, 5.31000, 5, 4.2100, 1, '2014-06-05 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-09-01 00:00:00.000', 'TCPrem7174', 2, 27, 5, 5.44000, 5, 12.2300, 1, '2014-06-05 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-09-01 00:00:00.000', 'TCPrem7174', 9, 35, 5, 4.61000, 5, 6.2300, 1, '2014-06-05 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-09-01 00:00:00.000', 'TCPrem7174', 9, 36, 5, 8.32000, 5, 10.3200, 1, '2014-06-05 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-10-01 00:00:00.000', 'TCPrem7174', 2, 8, 5, 16.54000, 5, 89.5600, 1, '2014-06-05 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-10-01 00:00:00.000', 'TCPrem7174', 2, 9, 5, 4.33000, 5, 12.5600, 1, '2014-06-05 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-10-01 00:00:00.000', 'TCPrem7174', 2, 27, 5, 8.34000, 5, 14.3300, 1, '2014-06-05 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-10-01 00:00:00.000', 'TCPrem7174', 9, 35, 5, 4.21740, 5, 6.1490, 1, '2014-06-05 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-10-01 00:00:00.000', 'TCPrem7174', 9, 36, 5, 6.92000, 5, 8.7900, 1, '2014-06-05 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-11-01 00:00:00.000', 'TCPrem7174', 2, 8, 5, 16.34000, 5, 88.7500, 1, '2014-06-05 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-11-01 00:00:00.000', 'TCPrem7174', 2, 9, 5, 3.33000, 5, 11.3700, 1, '2014-06-06 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-11-01 00:00:00.000', 'TCPrem7174', 2, 27, 5, 9.20000, 5, 23.4400, 1, '2014-06-05 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-11-01 00:00:00.000', 'TCPrem7174', 9, 35, 5, 3.26000, 5, 5.6320, 1, '2014-06-05 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-11-01 00:00:00.000', 'TCPrem7174', 9, 36, 5, 7.18000, 5, 8.6500, 1, '2014-06-05 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-12-01 00:00:00.000', 'TCPrem7174', 2, 8, 5, 15.29000, 5, 78.6500, 1, '2014-06-05 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-12-01 00:00:00.000', 'TCPrem7174', 2, 9, 5, 3.12000, 5, 10.7800, 1, '2014-06-05 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-12-01 00:00:00.000', 'TCPrem7174', 2, 27, 5, 5.10000, 5, 10.4000, 1, '2014-06-05 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-12-01 00:00:00.000', 'TCPrem7174', 9, 36, 5, 9.23000, 5, 11.0200, 1, '2014-06-05 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-01-01 00:00:00.000', 'TCPrem7174', 10, 28, 5, 3.00000, 5, 1.2300, 1, '2014-01-01 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-01-01 00:00:00.000', 'TCPrem7174', 10, 31, 5, 3.10000, 5, 2.0300, 1, '2014-01-01 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-02-01 00:00:00.000', 'TCPrem7174', 10, 28, 5, 3.34000, 5, 1.5400, 1, '2014-01-01 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-02-01 00:00:00.000', 'TCPrem7174', 10, 31, 5, 4.30000, 5, 3.0100, 1, '2014-01-01 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-03-01 00:00:00.000', 'TCPrem7174', 10, 28, 5, 3.46000, 5, 1.3200, 1, '2014-01-01 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-03-01 00:00:00.000', 'TCPrem7174', 10, 31, 5, 1.10000, 5, 1.2300, 1, '2014-01-01 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-04-01 00:00:00.000', 'TCPrem7174', 10, 28, 5, 2.13000, 5, 2.5600, 1, '2014-01-01 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-04-01 00:00:00.000', 'TCPrem7174', 10, 31, 5, 1.23000, 5, 1.5400, 1, '2014-01-01 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-05-01 00:00:00.000', 'TCPrem7174', 10, 28, 5, 3.49000, 5, 4.5500, 1, '2014-01-01 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-05-01 00:00:00.000', 'TCPrem7174', 10, 31, 5, 1.33000, 5, 1.4130, 1, '2014-01-01 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-06-01 00:00:00.000', 'TCPrem7174', 10, 28, 5, 2.23000, 5, 3.2930, 1, '2014-01-01 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-06-01 00:00:00.000', 'TCPrem7174', 10, 31, 5, 1.49000, 5, 1.4930, 1, '2014-01-01 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-07-01 00:00:00.000', 'TCPrem7174', 10, 28, 5, 2.97000, 5, 3.9700, 1, '2014-01-01 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-07-01 00:00:00.000', 'TCPrem7174', 10, 31, 5, 1.68000, 5, 1.9300, 1, '2014-01-01 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-08-01 00:00:00.000', 'TCPrem7174', 10, 28, 5, 2.10000, 5, 3.1200, 1, '2014-01-01 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-08-01 00:00:00.000', 'TCPrem7174', 10, 31, 5, 1.01000, 5, 1.1000, 1, '2014-01-01 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-09-01 00:00:00.000', 'TCPrem7174', 10, 28, 5, 1.97000, 5, 2.3500, 1, '2014-01-01 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-09-01 00:00:00.000', 'TCPrem7174', 10, 31, 5, 4.33000, 5, 5.4300, 1, '2014-01-01 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-10-01 00:00:00.000', 'TCPrem7174', 10, 28, 5, 2.31000, 5, 3.5400, 1, '2014-01-01 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-10-01 00:00:00.000', 'TCPrem7174', 10, 31, 5, 3.23000, 5, 3.9100, 1, '2014-01-01 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-11-01 00:00:00.000', 'TCPrem7174', 10, 28, 5, 2.43000, 5, 3.4900, 1, '2014-01-01 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-11-01 00:00:00.000', 'TCPrem7174', 10, 31, 5, 2.12000, 5, 2.6300, 1, '2014-01-01 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-12-01 00:00:00.000', 'TCPrem7174', 10, 28, 5, 2.59000, 5, 3.8700, 1, '2014-01-01 00:00:00.000')
INSERT INTO [wh].[BillDisaggAppliance] ([ClientID], [CustomerID], [AccountID], [BillDate], [PremiseID], [EndUseID], [ApplianceID], [CommodityID], [UsageQuantity], [UsageUomID], [CostAmount], [CurrencyId], [NewDate]) VALUES (256, 'TCCust7174', 'TCAcct7174', '2014-12-01 00:00:00.000', 'TCPrem7174', 10, 31, 5, 2.78000, 5, 3.0200, 1, '2014-01-01 00:00:00.000')
GO
