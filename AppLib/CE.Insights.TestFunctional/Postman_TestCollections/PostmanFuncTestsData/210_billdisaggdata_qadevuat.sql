USE [Insights]
GO

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'36090001'
           ,'925237500'
           ,'2014-09-01 00:00:00.000'
           ,'925237500'
           ,4
           ,16
           ,1
           ,78.1
           ,1
           ,12.65
           ,1
           ,'2014-09-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'36090001'
           ,'925237500'
           ,'2014-09-01 00:00:00.000'
           ,'925237500'
           ,6
           ,20
           ,1
           ,92.3
           ,1
           ,14.95
           ,1
           ,'2014-09-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'36090001'
           ,'925237500'
           ,'2014-09-01 00:00:00.000'
           ,'925237500'
           ,3
           ,18
           ,1
           ,170.4
           ,1
           ,27.6
           ,1
           ,'2014-09-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'36090001'
           ,'925237500'
           ,'2014-09-01 00:00:00.000'
           ,'925237500'
           ,5
           ,23
           ,1
           ,33.1
           ,1
           ,4.5
           ,1
           ,'2014-09-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'36090001'
           ,'925237500'
           ,'2014-09-01 00:00:00.000'
           ,'925237500'
           ,1
           ,3
           ,1
           ,205.9
           ,1
           ,33.35
           ,1
           ,'2014-09-01 00:00:00.000')
GO

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'36090001'
           ,'925237500'
           ,'2014-09-01 00:00:00.000'
           ,'925237500'
           ,7
           ,21
           ,1
           ,42.6
           ,1
           ,6.9
           ,1
           ,'2014-09-01 00:00:00.000')
GO

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'36090001'
           ,'925237500'
           ,'2014-09-01 00:00:00.000'
           ,'925237500'
           ,0
           ,19
           ,1
           ,71
           ,1
           ,11.5
           ,1
           ,'2014-09-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'36090001'
           ,'925237500'
           ,'2014-09-01 00:00:00.000'
           ,'925237500'
           ,0
           ,6
           ,1
           ,49.7
           ,1
           ,8.05
           ,1
           ,'2014-09-01 00:00:00.000')
GO
--october
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'36090001'
           ,'925237500'
           ,'2014-10-01 00:00:00.000'
           ,'925237500'
           ,4
           ,16
           ,1
           ,175.5
           ,1
           ,27.5
           ,1
           ,'2014-10-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'36090001'
           ,'925237500'
           ,'2014-10-01 00:00:00.000'
           ,'925237500'
           ,3
           ,18
           ,1
           ,280.8
           ,1
           ,44
           ,1
           ,'2014-10-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'36090001'
           ,'925237500'
           ,'2014-10-01 00:00:00.000'
           ,'925237500'
           ,5
           ,23
           ,1
           ,70.2
           ,1
           ,11
           ,1
           ,'2014-10-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'36090001'
           ,'925237500'
           ,'2014-10-01 00:00:00.000'
           ,'925237500'
           ,7
           ,21
           ,1
           ,56.16
           ,1
           ,8.8
           ,1
           ,'2014-10-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'36090001'
           ,'925237500'
           ,'2014-10-01 00:00:00.000'
           ,'925237500'
           ,0
           ,19
           ,1
           ,70.2
           ,1
           ,11
           ,1
           ,'2014-10-01 00:00:00.000')
GO

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'36090001'
           ,'925237500'
           ,'2014-10-01 00:00:00.000'
           ,'925237500'
           ,0
           ,6
           ,1
           ,49.14
           ,1
           ,7.7
           ,1
           ,'2014-10-01 00:00:00.000')
GO

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'36090001'
           ,'925237500'
           ,'2014-10-01 00:00:00.000'
           ,'925237500'
           ,6
           ,20
           ,1
           ,45
           ,1
           ,7.3
           ,1
           ,'2014-10-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'36090001'
           ,'925237500'
           ,'2014-10-01 00:00:00.000'
           ,'925237500'
           ,1
           ,3
           ,1
           ,107
           ,1
           ,16.8
           ,1
           ,'2014-10-01 00:00:00.000')
GO
--end october
--november
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'36090001'
           ,'925237500'
           ,'2014-11-01 00:00:00.000'
           ,'925237500'
           ,4
           ,16
           ,1
           ,167.5
           ,1
           ,21.25
           ,1
           ,'2014-11-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'36090001'
           ,'925237500'
           ,'2014-11-01 00:00:00.000'
           ,'925237500'
           ,6
           ,20
           ,1
           ,20.2
           ,1
           ,3.5
           ,1
           ,'2014-11-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'36090001'
           ,'925237500'
           ,'2014-11-01 00:00:00.000'
           ,'925237500'
           ,3
           ,18
           ,1
           ,268
           ,1
           ,34
           ,1
           ,'2014-11-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'36090001'
           ,'925237500'
           ,'2014-11-01 00:00:00.000'
           ,'925237500'
           ,5
           ,23
           ,1
          , 67
           ,1
           ,8.5
           ,1
           ,'2014-11-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'36090001'
           ,'925237500'
           ,'2014-11-01 00:00:00.000'
           ,'925237500'
           ,1
           ,3
           ,1
           ,10.3
           ,1
           ,2.4
           ,1
           ,'2014-11-01 00:00:00.000')
GO

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'36090001'
           ,'925237500'
           ,'2014-11-01 00:00:00.000'
           ,'925237500'
           ,7
           ,21
           ,1
           ,53.6
           ,1
           ,6.8
           ,1
           ,'2014-11-01 00:00:00.000')
GO

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'36090001'
           ,'925237500'
           ,'2014-11-01 00:00:00.000'
           ,'925237500'
           ,0
           ,19
           ,1
           ,67
           ,1
           ,8.5
           ,1
           ,'2014-11-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'36090001'
           ,'925237500'
           ,'2014-11-01 00:00:00.000'
           ,'925237500'
           ,0
           ,6
           ,1
           ,46.9
           ,1
           ,5.95
           ,1
           ,'2014-11-01 00:00:00.000')
GO
--end november
--december
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'36090001'
           ,'925237500'
           ,'2014-12-01 00:00:00.000'
           ,'925237500'
           ,4
           ,16
           ,1
           ,165
           ,1
           ,20
           ,1
           ,'2014-12-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'36090001'
           ,'925237500'
           ,'2014-12-01 00:00:00.000'
           ,'925237500'
           ,6
           ,20
           ,1
           ,0
           ,1
           ,0
           ,1
           ,'2014-12-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'36090001'
           ,'925237500'
           ,'2014-12-01 00:00:00.000'
           ,'925237500'
           ,3
           ,18
           ,1
           ,264
           ,1
           ,32
           ,1
           ,'2014-12-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'36090001'
           ,'925237500'
           ,'2014-12-01 00:00:00.000'
           ,'925237500'
           ,5
           ,23
           ,1
          , 66
           ,1
           ,8
           ,1
           ,'2014-12-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'36090001'
           ,'925237500'
           ,'2014-12-01 00:00:00.000'
           ,'925237500'
           ,1
           ,3
           ,1
           ,9.1
           ,1
           ,1.8
           ,1
           ,'2014-12-01 00:00:00.000')
GO

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'36090001'
           ,'925237500'
           ,'2014-12-01 00:00:00.000'
           ,'925237500'
           ,7
           ,21
           ,1
           ,52.8
           ,1
           ,6.4
           ,1
           ,'2014-12-01 00:00:00.000')
GO

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'36090001'
           ,'925237500'
           ,'2014-12-01 00:00:00.000'
           ,'925237500'
           ,0
           ,19
           ,1
           ,66
           ,1
           ,8
           ,1
           ,'2014-12-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'36090001'
           ,'925237500'
           ,'2014-12-01 00:00:00.000'
           ,'925237500'
           ,0
           ,6
           ,1
           ,46.2
           ,1
           ,5.6
           ,1
           ,'2014-12-01 00:00:00.000')
GO
--end december
-------Customer2 sample data

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'695814151'
           ,'1994212837'
           ,'2014-09-01 00:00:00.000'
           ,'1994212800'
           ,4
           ,16
           ,1
           ,78.1
           ,1
           ,12.65
           ,1
           ,'2014-09-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'695814151'
           ,'1994212837'
           ,'2014-09-01 00:00:00.000'
          ,'1994212800'
           ,3
           ,18
           ,1
           ,170.4
           ,1
           ,27.6
           ,1
           ,'2014-09-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'695814151'
           ,'1994212837'
           ,'2014-09-01 00:00:00.000'
          ,'1994212800'
           ,1
           ,3
           ,1
           ,205.9
           ,1
           ,33.35
           ,1
           ,'2014-09-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'695814151'
           ,'1994212837'
           ,'2014-09-01 00:00:00.000'
          ,'1994212800'
           ,7
           ,21
           ,1
           ,42.6
           ,1
           ,6.9
           ,1
           ,'2014-09-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'695814151'
           ,'1994212837'
           ,'2014-09-01 00:00:00.000'
            ,'1994212800'
           ,0
           ,19
           ,1
           ,71
           ,1
           ,11.5
           ,1
           ,'2014-09-01 00:00:00.000')
GO

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'695814151'
           ,'1994212837'
           ,'2014-09-01 00:00:00.000'
            ,'1994212800'
           ,0
           ,6
           ,1
           ,49.7
           ,1
           ,8.05
           ,1
           ,'2014-09-01 00:00:00.000')
GO
--october
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'695814151'
           ,'1994212837'
           ,'2014-10-01 00:00:00.000'
          ,'1994212800'
           ,4
           ,16
           ,1
           ,175.5
           ,1
           ,27.5
           ,1
           ,'2014-10-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'695814151'
           ,'1994212837'
           ,'2014-10-01 00:00:00.000'
          ,'1994212800'
           ,3
           ,18
           ,1
           ,280.8
           ,1
           ,44
           ,1
           ,'2014-10-01 00:00:00.000')
GO
 
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'695814151'
           ,'1994212837'
           ,'2014-10-01 00:00:00.000'
         ,'1994212800'
           ,7
           ,21
           ,1
           ,56.16
           ,1
           ,8.8
           ,1
           ,'2014-10-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'695814151'
           ,'1994212837'
           ,'2014-10-01 00:00:00.000'
            ,'1994212800'
           ,0
           ,19
           ,1
           ,70.2
           ,1
           ,11
           ,1
           ,'2014-10-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'695814151'
           ,'1994212837'
           ,'2014-10-01 00:00:00.000'
            ,'1994212800'
           ,0
           ,6
           ,1
           ,49.14
           ,1
           ,7.7
           ,1
           ,'2014-10-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'695814151'
           ,'1994212837'
           ,'2014-10-01 00:00:00.000'
           ,'1994212800'
           ,1
           ,3
           ,1
           ,107
           ,1
           ,16.8
           ,1
           ,'2014-10-01 00:00:00.000')
GO
--November
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'695814151'
           ,'1994212837'
           ,'2014-11-01 00:00:00.000'
       ,'1994212800'
           ,4
           ,16
           ,1
           ,167.5
           ,1
           ,21.25
           ,1
           ,'2014-11-01 00:00:00.000')
GO

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'695814151'
           ,'1994212837'
           ,'2014-11-01 00:00:00.000'
          ,'1994212800'
           ,3
           ,18
           ,1
           ,268
           ,1
           ,34
           ,1
           ,'2014-11-01 00:00:00.000')
GO

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'695814151'
           ,'1994212837'
           ,'2014-11-01 00:00:00.000'
            ,'1994212800'
           ,1
           ,3
           ,1
           ,9
           ,1
           ,1.3
           ,1
           ,'2014-11-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'695814151'
           ,'1994212837'
           ,'2014-11-01 00:00:00.000'
           ,'1994212800'
           ,7
           ,21
           ,1
           ,53.6
           ,1
           ,6.8
           ,1
           ,'2014-11-01 00:00:00.000')
GO
 
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'695814151'
           ,'1994212837'
           ,'2014-11-01 00:00:00.000'
            ,'1994212800'
           ,0
           ,19
           ,1
           ,67
           ,1
           ,8.5
           ,1
           ,'2014-11-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'695814151'
           ,'1994212837'
           ,'2014-11-01 00:00:00.000'
            ,'1994212800'
           ,0
           ,6
           ,1
           ,46.9
           ,1
           ,5.95
           ,1
           ,'2014-11-01 00:00:00.000')
GO
--end november
--december
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'695814151'
           ,'1994212837'
           ,'2014-12-01 00:00:00.000'
            ,'1994212800'
           ,4
           ,16
           ,1
           ,165
           ,1
           ,20
           ,1
           ,'2014-12-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'695814151'
           ,'1994212837'
           ,'2014-12-01 00:00:00.000'
           ,'1994212800'
           ,3
           ,18
           ,1
           ,264
           ,1
           ,32
           ,1
           ,'2014-12-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'695814151'
           ,'1994212837'
           ,'2014-12-01 00:00:00.000'
           ,'1994212800'
           ,1
           ,3
           ,1
           ,8
           ,1
           ,1
           ,1
           ,'2014-12-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'695814151'
           ,'1994212837'
           ,'2014-12-01 00:00:00.000'
            ,'1994212800'
           ,7
           ,21
           ,1
          ,52.8
           ,1
           ,6.4
           ,1
           ,'2014-12-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'695814151'
           ,'1994212837'
           ,'2014-12-01 00:00:00.000'
            ,'1994212800'
           ,0
           ,19
           ,1
           ,66
           ,1
           ,8
           ,1
           ,'2014-12-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'695814151'
           ,'1994212837'
           ,'2014-12-01 00:00:00.000'
           ,'1994212800'
           ,0
           ,6
           ,1
           ,46.2
           ,1
           ,5.6
           ,1
           ,'2014-12-01 00:00:00.000')
GO
--end december
---endcustomer2 sample data
--begin customer 3 sample data
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'139078351'
           ,'66125400'
           ,'2014-09-01 00:00:00.000'
           ,'66125400'
           ,4
           ,16
           ,1
           ,78.1
           ,1
           ,12.65
           ,1
           ,'2014-09-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'139078351'
           ,'66125400'
           ,'2014-09-01 00:00:00.000'
          ,'66125400'
           ,6
           ,20
           ,1
           ,92.3
           ,1
           ,14.95
           ,1
           ,'2014-09-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'139078351'
           ,'66125400'
           ,'2014-09-01 00:00:00.000'
          ,'66125400'
           ,3
           ,18
           ,1
           ,170.4
           ,1
           ,27.6
           ,1
           ,'2014-09-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'139078351'
           ,'66125400'
           ,'2014-09-01 00:00:00.000'
          ,'66125400'
           ,5
           ,23
           ,1
           ,34.9
           ,1
           ,5.5
           ,1
           ,'2014-09-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'139078351'
           ,'66125400'
           ,'2014-09-01 00:00:00.000'
            ,'66125400'
           ,1
           ,3
           ,1
           ,205.9
           ,1
           ,33.35
           ,1
           ,'2014-09-01 00:00:00.000')
GO

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'139078351'
           ,'66125400'
           ,'2014-09-01 00:00:00.000'
            ,'66125400'
           ,7
           ,21
           ,1
          , 42.6
           ,1
           ,6.9
           ,1
           ,'2014-09-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'139078351'
           ,'66125400'
           ,'2014-09-01 00:00:00.000'
          ,'66125400'
           ,0
           ,19
           ,1
           ,71
           ,1
           ,11.5
           ,1
           ,'2014-09-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'139078351'
           ,'66125400'
           ,'2014-09-01 00:00:00.000'
          ,'66125400'
           ,0
           ,6
           ,1
           ,49.7
           ,1
           ,8.05
           ,1
           ,'2014-09-01 00:00:00.000')
GO
--october
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'139078351'
           ,'66125400'
           ,'2014-10-01 00:00:00.000'
          ,'66125400'
           ,4
           ,16
           ,1
           ,175.5
           ,1
           ,27.5
           ,1
           ,'2014-10-01 00:00:00.000')
GO
 
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'139078351'
           ,'66125400'
           ,'2014-10-01 00:00:00.000'
         ,'66125400'
           ,3
           ,18
           ,1
           ,280.8
           ,1
           ,44
           ,1
           ,'2014-10-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'139078351'
           ,'66125400'
           ,'2014-10-01 00:00:00.000'
            ,'66125400'
           ,5
           ,23
           ,1
           ,70.2
           ,1
           ,11
           ,1
           ,'2014-10-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'139078351'
           ,'66125400'
           ,'2014-10-01 00:00:00.000'
            ,'66125400'
           ,7
           ,21
           ,1
           ,56.16
           ,1
           ,8.8
           ,1
           ,'2014-10-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'139078351'
           ,'66125400'
           ,'2014-10-01 00:00:00.000'
           ,'66125400'
           ,0
           ,19
           ,1
           ,70.2
           ,1
           ,11
           ,1
           ,'2014-10-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'139078351'
           ,'66125400'
           ,'2014-10-01 00:00:00.000'
           ,'66125400'
           ,0
           ,6
           ,1
           ,49.14
           ,1
           ,7.7
           ,1
           ,'2014-10-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'139078351'
           ,'66125400'
           ,'2014-10-01 00:00:00.000'
           ,'66125400'
           ,6
           ,20
           ,1
           ,45
           ,1
           ,7.3
           ,1
           ,'2014-10-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'139078351'
           ,'66125400'
           ,'2014-10-01 00:00:00.000'
           ,'66125400'
           ,1
           ,3
           ,1
           ,60.2
           ,1
           ,10.3
           ,1
           ,'2014-10-01 00:00:00.000')
GO
--November
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'139078351'
           ,'66125400'
           ,'2014-11-01 00:00:00.000'
       ,'66125400'
           ,4
           ,16
           ,1
           ,167.5
           ,1
           ,21.25
           ,1
           ,'2014-11-01 00:00:00.000')
GO

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'139078351'
           ,'66125400'
           ,'2014-11-01 00:00:00.000'
          ,'66125400'
           ,6
           ,20
           ,1
           ,10
           ,1
           ,3
           ,1
           ,'2014-11-01 00:00:00.000')
GO

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'139078351'
           ,'66125400'
           ,'2014-11-01 00:00:00.000'
          ,'66125400'
           ,3
           ,18
           ,1
           ,268
           ,1
           ,34
           ,1
           ,'2014-11-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'139078351'
           ,'66125400'
           ,'2014-11-01 00:00:00.000'
          ,'66125400'
           ,5
           ,23
           ,1
           ,225.5
           ,1
           ,50.2
           ,1
           ,'2014-11-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'139078351'
           ,'66125400'
           ,'2014-11-01 00:00:00.000'
            ,'66125400'
           ,1
           ,3
           ,1
           ,10
           ,1
           ,2.1
           ,1
           ,'2014-11-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'139078351'
           ,'66125400'
           ,'2014-11-01 00:00:00.000'
           ,'66125400'
           ,7
           ,21
           ,1
           ,53.6
           ,1
           ,6.8
           ,1
           ,'2014-11-01 00:00:00.000')
GO
 
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'139078351'
           ,'66125400'
           ,'2014-11-01 00:00:00.000'
            ,'66125400'
           ,0
           ,19
           ,1
           ,67
           ,1
           ,8.5
           ,1
           ,'2014-11-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'139078351'
           ,'66125400'
           ,'2014-11-01 00:00:00.000'
            ,'66125400'
           ,0
           ,6
           ,1
           ,46.9
           ,1
           ,5.95
           ,1
           ,'2014-11-01 00:00:00.000')
GO
--end november
--december
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'139078351'
           ,'66125400'
           ,'2014-12-01 00:00:00.000'
            ,'66125400'
           ,4
           ,16
           ,1
           ,165
           ,1
           ,20
           ,1
           ,'2014-12-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'139078351'
           ,'66125400'
           ,'2014-12-01 00:00:00.000'
           ,'66125400'
           ,6
           ,20
           ,1
           ,9.1
           ,1
           ,2.5
           ,1
           ,'2014-12-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'139078351'
           ,'66125400'
           ,'2014-12-01 00:00:00.000'
           ,'66125400'
           ,3
           ,18
           ,1
           ,264
           ,1
           ,32
           ,1
           ,'2014-12-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'139078351'
           ,'66125400'
           ,'2014-12-01 00:00:00.000'
            ,'66125400'
           ,5
           ,23
           ,1
          ,362.7
           ,1
           ,50.33
           ,1
           ,'2014-12-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'139078351'
           ,'66125400'
           ,'2014-12-01 00:00:00.000'
            ,'66125400'
           ,1
           ,3
           ,1
           ,9
           ,1
           ,1
           ,1
           ,'2014-12-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'139078351'
           ,'66125400'
           ,'2014-12-01 00:00:00.000'
           ,'66125400'
           ,7
           ,21
           ,1
           ,52.8
           ,1
           ,6.4
           ,1
           ,'2014-12-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'139078351'
           ,'66125400'
           ,'2014-12-01 00:00:00.000'
           ,'66125400'
           ,0
           ,19
           ,1
           ,66
           ,1
           ,8
           ,1
           ,'2014-12-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'139078351'
           ,'66125400'
           ,'2014-12-01 00:00:00.000'
           ,'66125400'
           ,0
           ,6
           ,1
           ,46.2
           ,1
           ,5.6
           ,1
           ,'2014-12-01 00:00:00.000')
GO

--end customer 3 sample data
--customer4 billdisagg sample data 1 customer, 2 accounts - 3 premises (2 premises on 1 account, 1 premise on the other)

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-09-01 00:00:00.000'
           ,'796236900'
           ,4
           ,16
           ,1
           ,73.1
           ,1
           ,12.05
           ,1
           ,'2014-09-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-09-01 00:00:00.000'
           ,'796236900'
           ,6
           ,20
           ,1
           ,82.3
           ,1
           ,13.95
           ,1
           ,'2014-09-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-09-01 00:00:00.000'
           ,'796236900'
           ,3
           ,18
           ,1
           ,70.4
           ,1
           ,17.6
           ,1
           ,'2014-09-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-09-01 00:00:00.000'
           ,'796236900'
           ,5
           ,23
           ,1
           ,31.1
           ,1
           ,3.5
           ,1
           ,'2014-09-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-09-01 00:00:00.000'
           ,'796236900'
           ,1
           ,3
           ,1
           ,206.9
           ,1
           ,34.35
           ,1
           ,'2014-09-01 00:00:00.000')
GO

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-09-01 00:00:00.000'
           ,'796236900'
           ,7
           ,21
           ,1
           ,41.6
           ,1
           ,5.9
           ,1
           ,'2014-09-01 00:00:00.000')
GO

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-09-01 00:00:00.000'
           ,'796236900'
           ,0
           ,19
           ,1
           ,61
           ,1
           ,12.5
           ,1
           ,'2014-09-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-09-01 00:00:00.000'
           ,'796236900'
           ,0
           ,6
           ,1
           ,48.7
           ,1
           ,7.03
           ,1
           ,'2014-09-01 00:00:00.000')
GO
--october
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-10-01 00:00:00.000'
           ,'796236900'
           ,4
           ,16
           ,1
           ,174.5
           ,1
           ,26.5
           ,1
           ,'2014-10-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-10-01 00:00:00.000'
           ,'796236900'
           ,3
           ,18
           ,1
           ,270.8
           ,1
           ,34
           ,1
           ,'2014-10-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-10-01 00:00:00.000'
           ,'796236900'
           ,5
           ,23
           ,1
           ,69.1
           ,1
           ,12
           ,1
           ,'2014-10-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-10-01 00:00:00.000'
           ,'796236900'
           ,7
           ,21
           ,1
           ,54.16
           ,1
           ,6.8
           ,1
           ,'2014-10-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-10-01 00:00:00.000'
           ,'796236900'
           ,0
           ,18
           ,1
           ,69.1
           ,1
           ,11
           ,1
           ,'2014-10-01 00:00:00.000')
GO

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-10-01 00:00:00.000'
           ,'796236900'
           ,0
           ,6
           ,1
           ,48.14
           ,1
           ,6.5
           ,1
           ,'2014-10-01 00:00:00.000')
GO

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-10-01 00:00:00.000'
           ,'796236900'
           ,6
           ,20
           ,1
           ,25
           ,1
           ,5.3
           ,1
           ,'2014-10-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-10-01 00:00:00.000'
           ,'796236900'
           ,1
           ,3
           ,1
           ,108
           ,1
           ,17.8
           ,1
           ,'2014-10-01 00:00:00.000')
GO
--end october
--november
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-11-01 00:00:00.000'
           ,'796236900'
           ,4
           ,16
           ,1
           ,168.5
           ,1
           ,22.25
           ,1
           ,'2014-11-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-11-01 00:00:00.000'
           ,'796236900'
           ,6
           ,20
           ,1
           ,23.2
           ,1
           ,4.8
           ,1
           ,'2014-11-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-11-01 00:00:00.000'
           ,'796236900'
           ,3
           ,18
           ,1
           ,269
           ,1
           ,35
           ,1
           ,'2014-11-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-11-01 00:00:00.000'
           ,'796236900'
           ,5
           ,23
           ,1
          , 64
           ,1
           ,7.1
           ,1
           ,'2014-11-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-11-01 00:00:00.000'
           ,'796236900'
           ,1
           ,3
           ,1
           ,9.3
           ,1
           ,1.4
           ,1
           ,'2014-11-01 00:00:00.000')
GO

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-11-01 00:00:00.000'
           ,'796236900'
           ,7
           ,21
           ,1
           ,43.1
           ,1
           ,5.8
           ,1
           ,'2014-11-01 00:00:00.000')
GO

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-11-01 00:00:00.000'
           ,'796236900'
           ,0
           ,19
           ,1
           ,67
           ,1
           ,8.5
           ,1
           ,'2014-11-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-11-01 00:00:00.000'
           ,'796236900'
           ,0
           ,6
           ,1
           ,46.3
           ,1
           ,5.75
           ,1
           ,'2014-11-01 00:00:00.000')
GO
--end november
--december
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-12-01 00:00:00.000'
           ,'796236900'
           ,4
           ,16
           ,1
           ,165
           ,1
           ,20
           ,1
           ,'2014-12-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-12-01 00:00:00.000'
           ,'796236900'
           ,6
           ,20
           ,1
           ,0
           ,1
           ,0
           ,1
           ,'2014-12-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-12-01 00:00:00.000'
           ,'796236900'
           ,3
           ,18
           ,1
           ,264
           ,1
           ,32
           ,1
           ,'2014-12-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-12-01 00:00:00.000'
           ,'796236900'
           ,5
           ,23
           ,1
          , 66
           ,1
           ,8
           ,1
           ,'2014-12-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-12-01 00:00:00.000'
           ,'796236900'
           ,1
           ,3
           ,1
           ,9.1
           ,1
           ,1.8
           ,1
           ,'2014-12-01 00:00:00.000')
GO

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-12-01 00:00:00.000'
           ,'796236900'
           ,7
           ,21
           ,1
           ,52.8
           ,1
           ,6.4
           ,1
           ,'2014-12-01 00:00:00.000')
GO

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-12-01 00:00:00.000'
           ,'796236900'
           ,0
           ,19
           ,1
           ,66
           ,1
           ,8
           ,1
           ,'2014-12-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-12-01 00:00:00.000'
           ,'796236900'
           ,0
           ,6
           ,1
           ,46.2
           ,1
           ,5.6
           ,1
           ,'2014-12-01 00:00:00.000')
GO
--end december
--2nd premise on same account

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-09-01 00:00:00.000'
           ,'796236901'
           ,4
           ,16
           ,1
           ,78.1
           ,1
           ,12.65
           ,1
           ,'2014-09-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-09-01 00:00:00.000'
           ,'796236901'
           ,6
           ,20
           ,1
           ,92.3
           ,1
           ,14.95
           ,1
           ,'2014-09-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-09-01 00:00:00.000'
           ,'796236901'
           ,3
           ,18
           ,1
           ,170.4
           ,1
           ,27.6
           ,1
           ,'2014-09-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-09-01 00:00:00.000'
           ,'796236901'
           ,5
           ,23
           ,1
           ,33.1
           ,1
           ,4.5
           ,1
           ,'2014-09-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-09-01 00:00:00.000'
           ,'796236901'
           ,1
           ,3
           ,1
           ,205.9
           ,1
           ,33.35
           ,1
           ,'2014-09-01 00:00:00.000')
GO

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-09-01 00:00:00.000'
           ,'796236901'
           ,7
           ,21
           ,1
           ,42.6
           ,1
           ,6.9
           ,1
           ,'2014-09-01 00:00:00.000')
GO

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-09-01 00:00:00.000'
           ,'796236901'
           ,0
           ,19
           ,1
           ,71
           ,1
           ,11.5
           ,1
           ,'2014-09-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-09-01 00:00:00.000'
           ,'796236901'
           ,0
           ,6
           ,1
           ,49.7
           ,1
           ,8.05
           ,1
           ,'2014-09-01 00:00:00.000')
GO
--october
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-10-01 00:00:00.000'
           ,'796236901'
           ,4
           ,16
           ,1
           ,175.5
           ,1
           ,27.5
           ,1
           ,'2014-10-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-10-01 00:00:00.000'
           ,'796236901'
           ,3
           ,18
           ,1
           ,280.8
           ,1
           ,44
           ,1
           ,'2014-10-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-10-01 00:00:00.000'
           ,'796236901'
           ,5
           ,23
           ,1
           ,70.2
           ,1
           ,11
           ,1
           ,'2014-10-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-10-01 00:00:00.000'
           ,'796236901'
           ,7
           ,21
           ,1
           ,56.16
           ,1
           ,8.8
           ,1
           ,'2014-10-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-10-01 00:00:00.000'
           ,'796236901'
           ,0
           ,19
           ,1
           ,70.2
           ,1
           ,11
           ,1
           ,'2014-10-01 00:00:00.000')
GO

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-10-01 00:00:00.000'
           ,'796236901'
           ,0
           ,6
           ,1
           ,49.14
           ,1
           ,7.7
           ,1
           ,'2014-10-01 00:00:00.000')
GO

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-10-01 00:00:00.000'
           ,'796236901'
           ,6
           ,20
           ,1
           ,45
           ,1
           ,7.3
           ,1
           ,'2014-10-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-10-01 00:00:00.000'
           ,'796236901'
           ,1
           ,3
           ,1
           ,107
           ,1
           ,16.8
           ,1
           ,'2014-10-01 00:00:00.000')
GO
--end october
--november
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-11-01 00:00:00.000'
           ,'796236901'
           ,4
           ,16
           ,1
           ,167.5
           ,1
           ,21.25
           ,1
           ,'2014-11-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-11-01 00:00:00.000'
           ,'796236901'
           ,6
           ,20
           ,1
           ,20.2
           ,1
           ,3.5
           ,1
           ,'2014-11-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-11-01 00:00:00.000'
           ,'796236901'
           ,3
           ,18
           ,1
           ,268
           ,1
           ,34
           ,1
           ,'2014-11-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-11-01 00:00:00.000'
           ,'796236901'
           ,5
           ,23
           ,1
          , 67
           ,1
           ,8.5
           ,1
           ,'2014-11-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-11-01 00:00:00.000'
           ,'796236901'
           ,1
           ,3
           ,1
           ,10.3
           ,1
           ,2.4
           ,1
           ,'2014-11-01 00:00:00.000')
GO

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-11-01 00:00:00.000'
           ,'796236901'
           ,7
           ,21
           ,1
           ,53.6
           ,1
           ,6.8
           ,1
           ,'2014-11-01 00:00:00.000')
GO

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-11-01 00:00:00.000'
           ,'796236901'
           ,0
           ,19
           ,1
           ,67
           ,1
           ,8.5
           ,1
           ,'2014-11-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-11-01 00:00:00.000'
           ,'796236901'
           ,0
           ,6
           ,1
           ,46.9
           ,1
           ,5.95
           ,1
           ,'2014-11-01 00:00:00.000')
GO
--end november
--december
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-12-01 00:00:00.000'
           ,'796236901'
           ,4
           ,16
           ,1
           ,165
           ,1
           ,20
           ,1
           ,'2014-12-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-12-01 00:00:00.000'
           ,'796236901'
           ,6
           ,20
           ,1
           ,0
           ,1
           ,0
           ,1
           ,'2014-12-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-12-01 00:00:00.000'
           ,'796236901'
           ,3
           ,18
           ,1
           ,264
           ,1
           ,32
           ,1
           ,'2014-12-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-12-01 00:00:00.000'
           ,'796236901'
           ,5
           ,23
           ,1
          , 66
           ,1
           ,8
           ,1
           ,'2014-12-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-12-01 00:00:00.000'
           ,'796236901'
           ,1
           ,3
           ,1
           ,9.1
           ,1
           ,1.8
           ,1
           ,'2014-12-01 00:00:00.000')
GO

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-12-01 00:00:00.000'
           ,'796236901'
           ,7
           ,21
           ,1
           ,52.8
           ,1
           ,6.4
           ,1
           ,'2014-12-01 00:00:00.000')
GO

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-12-01 00:00:00.000'
           ,'796236901'
           ,0
           ,19
           ,1
           ,66
           ,1
           ,8
           ,1
           ,'2014-12-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796236999'
           ,'2014-12-01 00:00:00.000'
           ,'796236901'
           ,0
           ,6
           ,1
           ,46.2
           ,1
           ,5.6
           ,1
           ,'2014-12-01 00:00:00.000')
GO
--end second premise same account for customer
--start third premise for a second account for customer

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796237000'
           ,'2014-09-01 00:00:00.000'
           ,'796236902'
           ,4
           ,16
           ,1
           ,78.1
           ,1
           ,12.65
           ,1
           ,'2014-09-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796237000'
           ,'2014-09-01 00:00:00.000'
           ,'796236902'
           ,6
           ,20
           ,1
           ,92.3
           ,1
           ,14.95
           ,1
           ,'2014-09-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796237000'
           ,'2014-09-01 00:00:00.000'
           ,'796236902'
           ,3
           ,18
           ,1
           ,170.4
           ,1
           ,27.6
           ,1
           ,'2014-09-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796237000'
           ,'2014-09-01 00:00:00.000'
           ,'796236902'
           ,5
           ,23
           ,1
           ,33.1
           ,1
           ,4.5
           ,1
           ,'2014-09-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796237000'
           ,'2014-09-01 00:00:00.000'
           ,'796236902'
           ,1
           ,3
           ,1
           ,205.9
           ,1
           ,33.35
           ,1
           ,'2014-09-01 00:00:00.000')
GO

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796237000'
           ,'2014-09-01 00:00:00.000'
           ,'796236902'
           ,7
           ,21
           ,1
           ,42.6
           ,1
           ,6.9
           ,1
           ,'2014-09-01 00:00:00.000')
GO

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796237000'
           ,'2014-09-01 00:00:00.000'
           ,'796236902'
           ,0
           ,19
           ,1
           ,71
           ,1
           ,11.5
           ,1
           ,'2014-09-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796237000'
           ,'2014-09-01 00:00:00.000'
           ,'796236902'
           ,0
           ,6
           ,1
           ,49.7
           ,1
           ,8.05
           ,1
           ,'2014-09-01 00:00:00.000')
GO
--october
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796237000'
           ,'2014-10-01 00:00:00.000'
           ,'796236902'
           ,4
           ,16
           ,1
           ,175.5
           ,1
           ,27.5
           ,1
           ,'2014-10-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796237000'
           ,'2014-10-01 00:00:00.000'
           ,'796236902'
           ,3
           ,18
           ,1
           ,280.8
           ,1
           ,44
           ,1
           ,'2014-10-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796237000'
           ,'2014-10-01 00:00:00.000'
           ,'796236902'
           ,5
           ,23
           ,1
           ,70.2
           ,1
           ,11
           ,1
           ,'2014-10-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796237000'
           ,'2014-10-01 00:00:00.000'
           ,'796236902'
           ,7
           ,21
           ,1
           ,56.16
           ,1
           ,8.8
           ,1
           ,'2014-10-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796237000'
           ,'2014-10-01 00:00:00.000'
           ,'796236902'
           ,0
           ,19
           ,1
           ,70.2
           ,1
           ,11
           ,1
           ,'2014-10-01 00:00:00.000')
GO

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796237000'
           ,'2014-10-01 00:00:00.000'
           ,'796236902'
           ,0
           ,6
           ,1
           ,49.14
           ,1
           ,7.7
           ,1
           ,'2014-10-01 00:00:00.000')
GO

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796237000'
           ,'2014-10-01 00:00:00.000'
           ,'796236902'
           ,6
           ,20
           ,1
           ,45
           ,1
           ,7.3
           ,1
           ,'2014-10-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796237000'
           ,'2014-10-01 00:00:00.000'
           ,'796236902'
           ,1
           ,3
           ,1
           ,108
           ,1
           ,19.8
           ,1
           ,'2014-10-01 00:00:00.000')
GO
--end october
--november
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796237000'
           ,'2014-11-01 00:00:00.000'
           ,'796236902'
           ,4
           ,16
           ,1
           ,169.5
           ,1
           ,23.25
           ,1
           ,'2014-11-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796237000'
           ,'2014-11-01 00:00:00.000'
           ,'796236902'
           ,6
           ,20
           ,1
           ,20.2
           ,1
           ,3.5
           ,1
           ,'2014-11-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796237000'
           ,'2014-11-01 00:00:00.000'
           ,'796236902'
           ,3
           ,18
           ,1
           ,269
           ,1
           ,35
           ,1
           ,'2014-11-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796237000'
           ,'2014-11-01 00:00:00.000'
           ,'796236902'
           ,5
           ,23
           ,1
          , 67
           ,1
           ,8.5
           ,1
           ,'2014-11-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796237000'
           ,'2014-11-01 00:00:00.000'
           ,'796236902'
           ,1
           ,3
           ,1
           ,10.3
           ,1
           ,2.4
           ,1
           ,'2014-11-01 00:00:00.000')
GO

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796237000'
           ,'2014-11-01 00:00:00.000'
           ,'796236902'
           ,7
           ,21
           ,1
           ,53.6
           ,1
           ,6.8
           ,1
           ,'2014-11-01 00:00:00.000')
GO

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796237000'
           ,'2014-11-01 00:00:00.000'
           ,'796236902'
           ,0
           ,19
           ,1
           ,68
           ,1
           ,9.5
           ,1
           ,'2014-11-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796237000'
           ,'2014-11-01 00:00:00.000'
           ,'796236902'
           ,0
           ,6
           ,1
           ,46.9
           ,1
           ,5.95
           ,1
           ,'2014-11-01 00:00:00.000')
GO
--end november
--december
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796237000'
           ,'2014-12-01 00:00:00.000'
           ,'796236902'
           ,4
           ,16
           ,1
           ,169
           ,1
           ,30
           ,1
           ,'2014-12-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796237000'
           ,'2014-12-01 00:00:00.000'
           ,'796236902'
           ,6
           ,20
           ,1
           ,0
           ,1
           ,0
           ,1
           ,'2014-12-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796237000'
           ,'2014-12-01 00:00:00.000'
           ,'796236902'
           ,3
           ,18
           ,1
           ,264
           ,1
           ,32
           ,1
           ,'2014-12-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796237000'
           ,'2014-12-01 00:00:00.000'
           ,'796236902'
           ,5
           ,23
           ,1
          , 66
           ,1
           ,8
           ,1
           ,'2014-12-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796237000'
           ,'2014-12-01 00:00:00.000'
           ,'796236902'
           ,1
           ,3
           ,1
           ,9.1
           ,1
           ,1.8
           ,1
           ,'2014-12-01 00:00:00.000')
GO

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796237000'
           ,'2014-12-01 00:00:00.000'
           ,'796236902'
           ,7
           ,21
           ,1
           ,52.8
           ,1
           ,6.4
           ,1
           ,'2014-12-01 00:00:00.000')
GO

INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796237000'
           ,'2014-12-01 00:00:00.000'
           ,'796236902'
           ,0
           ,19
           ,1
           ,66
           ,1
           ,8
           ,1
           ,'2014-12-01 00:00:00.000')
GO
INSERT INTO [wh].[BillDisaggAppliance]
           ([ClientID]
           ,[CustomerID]
           ,[AccountID]
           ,[BillDate]
           ,[PremiseID]
           ,[EndUseID]
           ,[ApplianceID]
           ,[CommodityID]
           ,[UsageQuantity]
           ,[UsageUomID]
           ,[CostAmount]
           ,[CurrencyId]
           ,[NewDate])
     VALUES
           (210
           ,'1828274656'
           ,'796237000'
           ,'2014-12-01 00:00:00.000'
           ,'796236902'
           ,0
           ,6
           ,1
           ,46.2
           ,1
           ,5.6
           ,1
           ,'2014-12-01 00:00:00.000')
GO