﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using CE.ESPMDataModel.ESPMModels;
using CE.ESPMReport.ConsoleApp.Handlers;
using CE.ESPMReport.ConsoleApp.RestSharp;

namespace CE.ESPMReport.ConsoleApp
{
    public class Functions
    {
        //private static RestSharpHelper restSharpHelper;
        //string authToken = "Jragpl4AqzzDE1i4ty-V2hwSozynTIT3lIX068BukVEzR7jCvuLVphX6wSd_PNFXgcQjTZubytEwVbK3ksZfYMLI8dVPVFhCAPegt4uNDSA";

        public Functions()
        {

            //restSharpHelper.authToken = authToken;
        }

        public void GenerateBillingReport(string userCreds)
        {
            bool generateHeader = true;
            string fileNamePrefix = DateTime.UtcNow.ToString("yyyy-MM-dd HH-mm", CultureInfo.InvariantCulture);
            var restSharpHelper = new RestSharpHelper();
            var authUser = UserAuthhandler.GetUserAuthToken(restSharpHelper, userCreds);

            if (authUser.User == null) return;

            restSharpHelper.AuthToken = authUser.User;
            UserAuthhandler.LogTrace(restSharpHelper, "WebJob Get Metrics Report - Start", "info");

            var acceptedAccounts = AcountHandler.GetAcceptedAccounts(restSharpHelper);

            var configuration = Configurationhandler.GetConfigurationMetrics(restSharpHelper);

            var metricsList = string.Join(",", configuration.Where(x => x.IsESPMMetric).Select(x => x.Name));

            FileHandler.GetClientDetails(restSharpHelper);

            //var headertext = Configurationhandler.GetConfigurationHeadertext(restSharpHelper);

            if (acceptedAccounts == null)
            {
                UserAuthhandler.LogTrace(restSharpHelper, "WebJob  Get Metrics Report - End - AcceptedAccounts is null",
                    "info");
                return;
            }

            UserAuthhandler.LogTrace(restSharpHelper,
                "WebJob Get Metrics Report - Accepted Account count = " + acceptedAccounts.Count, "info");

            foreach (var account in acceptedAccounts)
            {
                var propertyMetrics = new List<PropertyMetrics>();
                var acceptedProperties =
                    PropertyHandler.GetAcceptedProperties(restSharpHelper, $"?accountId={account.id}");
                UserAuthhandler.LogTrace(restSharpHelper,
                    "WebJob Get Metrics Report - Get Properties for Account = " + account.id, "info");
                if (acceptedProperties == null)
                {
                    UserAuthhandler.LogTrace(restSharpHelper, "WebJob Get Metrics Report - No properties found for accountId = " + account.id, "info");
                    continue;
                }
                foreach (var property in acceptedProperties)
                {
                    var propertyMetric = new PropertyMetrics();
                    propertyMetric.PropertyId = property.id;
                    propertyMetric.PropertyName = property.hint;
                    propertyMetric.PortFolioName = account.hint;
                    propertyMetric.PortFolioId = account.id;

                    var propertyDetails = PropertyHandler.GetPropertyDetails(restSharpHelper, $"?propertyId={property.id}");
                    UserAuthhandler.LogTrace(restSharpHelper,
                        "WebJob Get Metrics Report - Got Property Details for Account " + account.id + " and Property " + property.id, "info");
                    if (propertyDetails == null)
                    {
                        UserAuthhandler.LogTrace(restSharpHelper,
                            "WebJob Get Metrics Report - No Property Details found for  Account " + account.id + " and Property " + property.id, "info");
                        continue;
                    }
                    propertyMetric.City = propertyDetails.address.city;
                    propertyMetric.Address = propertyDetails.address.address1;
                    propertyMetric.NumberOfBuildings = propertyDetails.numberOfBuildings;
                    propertyMetric.ProperType = propertyDetails.primaryFunction;
                    propertyMetric.PropertyGfa = propertyDetails.grossFloorArea.value;

                    var dates = MetrisHandler.GetPropertyBaseLine_CurrentDates(restSharpHelper,
                        $"?propertyId={property.id}&month={DateTime.Now.Month}&year={DateTime.Now.Year}&metrics=energyBaselineDate,energyCurrentDate");
                    if (dates.Count == 0)
                    {
                        UserAuthhandler.LogTrace(restSharpHelper,
                            "WebJob Get Metrics Report - No Metric BaseLine and Current Dates Found for  " + account.id + " and Property " + property.id, "info");
                        continue;
                    }

                    propertyMetric.EnergyBaseLineDate = dates.First().ToString("yyyy-MMMM");
                    propertyMetric.EnergyCurrentDate = dates.Last().ToString("yyyy-MMMM");
                    var baseLineMEtrics = MetrisHandler.GetPropertyMetrics(restSharpHelper,
                        $"?propertyId={property.id}&month={dates.First().Month}&year={dates.First().Year}", metricsList);

                    var currentMetrics = MetrisHandler.GetPropertyMetrics(restSharpHelper,
                        $"?propertyId={property.id}&month={dates.Last().Month}&year={dates.Last().Year}", metricsList);
                    if (baseLineMEtrics.Count == 0)
                    {
                        UserAuthhandler.LogTrace(restSharpHelper,
                            "WebJob Get Metrics Report - No BaseLine Metric Details found for Account ID    " + account.id + " and Property " + property.id, "info");

                    }
                    if (currentMetrics.Count == 0)
                    {
                        UserAuthhandler.LogTrace(restSharpHelper,
                            "WebJob Get Metrics Report - No Current Metric Details found for Account ID    " + account.id + " and Property " + property.id, "info");

                    }
                    baseLineMEtrics.ForEach(x =>
                    {
                        var metric = configuration.FindLast(config => config.Name == x.Name);
                        x.IsESPMMetric = metric.IsESPMMetric;
                        x.PrefixText = metric.PrefixText;
                        x.DataType = metric.DataType;
                        x.DisplayText = metric.DisplayText;
                        x.IncludeInSummary = metric.IncludeInSummary;
                        x.SuffixText = metric.SuffixText;
                        x.Summarycalculation = metric.Summarycalculation;
                        x.Type = "Baseline";
                        x.YearEnding = dates.First().ToString("MM-dd-yyyy");
                        x.AccountReportOrder = metric.AccountReportOrder;
                        x.PropertyReportOrder = metric.PropertyReportOrder;
                    });

                    currentMetrics.ForEach(x =>
                    {
                        var metric = configuration.FindLast(config => config.Name == x.Name);
                        x.IsESPMMetric = metric.IsESPMMetric;
                        x.PrefixText = metric.PrefixText;
                        x.DataType = metric.DataType;
                        x.DisplayText = metric.DisplayText;
                        x.IncludeInSummary = metric.IncludeInSummary;
                        x.SuffixText = metric.SuffixText;
                        x.Summarycalculation = metric.Summarycalculation;
                        x.Type = "Current";
                        x.YearEnding = dates.Last().ToString("MM-dd-yyyy");
                        x.AccountReportOrder = metric.AccountReportOrder;
                        x.PropertyReportOrder = metric.PropertyReportOrder;
                    });

                    configuration.FindAll(x => x.IsESPMMetric == false).ForEach(m =>
                    {
                        baseLineMEtrics.Add(new Metric()
                        {
                            IsESPMMetric = false,
                            Name = m.Name,
                            DisplayText = m.DisplayText,
                            DataType = m.DataType,
                            IncludeInSummary = m.IncludeInSummary,
                            Summarycalculation = m.Summarycalculation,
                            PrefixText = m.PrefixText,
                            SuffixText = m.SuffixText,
                            Type = "Baseline",
                            YearEnding = dates.First().ToString("MM-dd-yyyy"),
                            AccountReportOrder = m.AccountReportOrder,
                            PropertyReportOrder = m.PropertyReportOrder
                        });

                        currentMetrics.Add(new Metric()
                        {
                            IsESPMMetric = false,
                            Name = m.Name,
                            DisplayText = m.DisplayText,
                            DataType = m.DataType,
                            IncludeInSummary = m.IncludeInSummary,
                            Summarycalculation = m.Summarycalculation,
                            PrefixText = m.PrefixText,
                            SuffixText = m.SuffixText,
                            Type = "Current",
                            YearEnding = dates.Last().ToString("MM-dd-yyyy"),
                            AccountReportOrder = m.AccountReportOrder,
                            PropertyReportOrder = m.PropertyReportOrder
                        });
                    });
                    UserAuthhandler.LogTrace(restSharpHelper,
                        "WebJob Get Metrics Report - Calculating Metrics for Account Id   " + account.id + " and Property " + property.id, "info");
                    propertyMetric.Metrics = MetrisHandler.CalculateMetrics(baseLineMEtrics, currentMetrics);

                    propertyMetrics.Add(propertyMetric);

                }

                UserAuthhandler.LogTrace(restSharpHelper, "WebJob Get Metrics Report - Generating Property Metrics File", "info");
                FileHandler.GeneratePropertyFile(propertyMetrics, fileNamePrefix, generateHeader);

                UserAuthhandler.LogTrace(restSharpHelper, "WebJob Get Metrics Report - Generating Account Metrics File", "info");
                FileHandler.GenerateAccountSummary(propertyMetrics, fileNamePrefix, generateHeader);

                generateHeader = false;

            }

        }
    }
}
