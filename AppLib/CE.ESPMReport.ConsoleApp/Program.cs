﻿using System;
using System.Configuration;
using System.Text;
using CE.ESPMReport.ConsoleApp.Utils;

namespace CE.ESPMReport.ConsoleApp
{
    class Program
    {
        // Please set the following connection strings in app.config for this WebJob to run:
        // AzureWebJobsDashboard and AzureWebJobsStorage
        static void Main()
        {
            var userCreds = ConfigurationManager.AppSettings[Constants.USERCREDS].Split(',');
            Console.WriteLine($"Starting WebJob to Get Property Metrics Report {DateTime.Now}");
            foreach (var userInfo in userCreds)
            {
                Console.WriteLine($"Starting WebJob to Get Property Metrics Report at {DateTime.Now} for userid: {Encoding.UTF8.GetString(Convert.FromBase64String(userInfo.Split(':')[0]))}");
                new Functions().GenerateBillingReport(userInfo);
            }
        }
    }
}