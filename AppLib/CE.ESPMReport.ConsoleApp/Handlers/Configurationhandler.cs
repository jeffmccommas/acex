﻿using System.Collections.Generic;
using System.Linq;
using CE.ESPMReport.ConsoleApp.RestSharp;
using CE.ESPMReport.ConsoleApp.Utils;
using Newtonsoft.Json;
using CE.ESPMDataModel.ESPMModels;

namespace CE.ESPMReport.ConsoleApp.Handlers
{
    public class Configurationhandler
    {
        public static List<Metric> GetConfigurationMetrics(RestSharpHelper restSharpHelper)
        {
            List<Metric> metric = new List<Metric>();
            var configResponse = restSharpHelper.Get(APIResourceURLS.GET_CONFIGURATION_CONTENT, null);
            var configs = JsonConvert.DeserializeObject<List<string>>(configResponse.Content);
            foreach (string item in configs)
            {
                metric.Add(JsonConvert.DeserializeObject<Metric>(item));
            }

            return metric;
        }


    }
}
