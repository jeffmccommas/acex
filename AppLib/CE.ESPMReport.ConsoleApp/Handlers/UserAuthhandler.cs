﻿using CE.ESPMDataModel.ESPMModels;
using CE.ESPMReport.ConsoleApp.RestSharp;
using CE.ESPMReport.ConsoleApp.Utils;
using Newtonsoft.Json;

namespace CE.ESPMReport.ConsoleApp.Handlers
{
    public class UserAuthhandler
    {

        public static AuthenticatedUser GetUserAuthToken(RestSharpHelper restSharpHelper, string userCreds)
        {
            var userInfo = userCreds.Split(':');
            //var User = new { username = new Constants().USERNAME, password = new Constants().PASSWORD };
            var user = new { username = userInfo[0], password = userInfo[1] };
            var authUser = new AuthenticatedUser();

            var authUserResponse = restSharpHelper.Post(APIResourceURLS.GET_USER_AUTH_TOKEN, JsonConvert.SerializeObject(user));
            if (authUserResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                authUser = JsonConvert.DeserializeObject<AuthenticatedUser>(authUserResponse.Content);
            }
            return authUser;
        }

        public static void LogTrace(RestSharpHelper restSharpHelper, string message, string logType)
        {
            var trace = new { logLevel = logType, logMessage = message };
            restSharpHelper.Post(APIResourceURLS.POST_TRACE_LOG, JsonConvert.SerializeObject(trace));
        }
    }
}
