﻿
namespace CE.ESPMReport.ConsoleApp.Utils
{
    public class Constants
    {
        public string USERNAME = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes("UILWEBJOBUSER"));
        public string PASSWORD = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes("qwerty!1"));
        public const string AUTHTOKEN = "WebToken";
        public const string APIBASEURL = "APIBaseURL";
        public const string BILLINGPERIOD = "BillingPeriod";
        public const string USERCREDS = "WebjobCreds";
        public const string COMMODITY = "Commodity";
        public const string FILEPATH_PROPERTY_REPORT = "FilePathPropertyReport";
        public const string FILEPATH_ACCOUNT_REPORT = "FilePathAccountReport";
        public const string DELIMITER = "Delimiter";
        public const string DELIMITERCALCULATEDMETRIC = "DelimiterCalculateMetrics";
        public const string DATATYPE_NUMERIC = "Number";
        public const string DATATYPE_CURRENCY = "Currency";
        public const string DATATYPE_TEXT = "Text";

        public const string CALCULATION_TEXT_SUM = "Sum";
        public const string CALCULATION_TEXT_COUNT = "Count";
        public const string FILEPATH = "FilePath";
    }

    public class APIResourceURLS
    {
        public const string GET_ACCEPTED_ACCOUNT_LIST_URL = "/api/EnergyStar/AcceptedAccountsList";
        public const string GET_ACCEPTED_PROPERTY_LIST_URL = "/api/EnergyStar/AcceptedPropertyForAccount";
        public const string GET_CONFIGURATION_CONTENT = "/api/InsightsApi/ConfigurationContentForReport";
        public const string GET_USER_AUTH_TOKEN = "/api/PortalsApi/UserAuth";
        public const string POST_TRACE_LOG = "/api/PortalsApi/TraceLog";
        public const string GET_PROPERTY_METRICS = "/api/EnergyStar/GetPropertyMetrics";
        public const string GET_ACCEPTED_PROPERTY_DETAILS_URL = "/api/EnergyStar/PropertyDetails";
        public const string GET_ACCEPTED_ACCOUNT_DETAILS_URL = "/api/EnergyStar/AcceptedPropertyForAccount";
        public const string GET_CLIENT_DETAILS_URL = "/api/PortalsApi/GetClientDetails";
    }
}
