﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using AO.BusinessContracts;
using AO.Registrar;
using CE.AO.Logging;
using CE.AO.Models;
using CsvHelper;
using Microsoft.Azure.WebJobs;
using Microsoft.Practices.Unity;
using WinSCP;
using Enums = CE.AO.Utilities.Enums;

namespace MdmIntegrationForLUSWebJob
{
    public class Functions
    {
        /// <summary>
        /// This function will be triggered based on the schedule you have set for this WebJob
        /// This function will enqueue a message on an Azure Queue called queue
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="environment"></param>
        /// <param name="meterListForCustomersOfType"></param>
        /// <param name="createOutputPerCommodity"></param>
        /// <param name="maxRowsPerFile"></param>
        [NoAutomaticTrigger]
        public static void MdmIntegrationForLus(int clientId, string environment, string meterListForCustomersOfType,
            bool createOutputPerCommodity, int maxRowsPerFile)
        {
            // Setup the logging mechanism
            var logModel = new LogModel
            {
                ClientId = Convert.ToString(clientId),
                Source = "MdmIntegrationForLus Web Job",
                Module = Enums.Module.MdmIntegrationForLus,
                ProcessingType = Enums.ProcessingType.Schedule
            };

            //Setup the direct injection container
            IUnityContainer unityContainer = new UnityContainer();
            (new DataStorageRegistrar()).Initialize<TransientLifetimeManager>(unityContainer);

            try
            {
                // Load the direct injection container.
                var iAmi = unityContainer.Resolve<ITallAMI>(new ParameterOverride("logModel", logModel));

                var iBillingCycleSchedule = unityContainer.Resolve<IBillingCycleSchedule>(new ParameterOverride("logModel", logModel));

                var iBilling = unityContainer.Resolve<IBilling>(new ParameterOverride("logModel", logModel));

                var iClientAccount = unityContainer.Resolve<IClientAccount>(
                       new ParameterOverride("logModel", logModel));

                var iSubscription = unityContainer.Resolve<ISubscription>(
                    new ParameterOverride("logModel", logModel));

                var iClientConfigFacade = unityContainer.Resolve<IClientConfigFacade>();

                // Local variables
                var endDate = DateTime.Now.Date;
                var filePath = Environment.CurrentDirectory + @"\";
                var fileNameWithoutExtension = $"{DateTime.Now:MMddyy_hhmmss}_AMIRequest";
                
                // AMI Models
                var electricAmiRequests = new List<AmiRequestModel>();
                var gasAmiRequests = new List<AmiRequestModel>();
                var waterAmiRequests = new List<AmiRequestModel>();

                // Create a table token.
                byte[] token = null;

                do
                {
                    // The the accounts associated with the current clientId.
                    IEnumerable<ClientAccountModel> clientAccounts = iClientAccount.GetClientAccounts(clientId, 1000, ref token);

                    // For each account
                    foreach (var clientAccount in clientAccounts)
                    {

                        logModel.AccountId = clientAccount.AccountId;
                        logModel.CustomerId = clientAccount.CustomerId;

                        // Get active subscriptions if requested. If any exist then run only bills for those clientAccounts.
                        if (meterListForCustomersOfType.Equals("Subscribed", StringComparison.InvariantCultureIgnoreCase))
                        {
                            var activeSubscriptions =
                                iSubscription.GetCustomerSubscriptions(clientId, clientAccount.CustomerId,
                                    clientAccount.AccountId).Count(m => !string.IsNullOrEmpty(m.ProgramName)
                                                                        && string.IsNullOrWhiteSpace(m.InsightTypeName)
                                                                        && m.IsSelected);

                            if (activeSubscriptions == 0)
                            {
                                continue;
                            }
                        }

                        // Get the bills associated with an AMI Meter.
                        List<BillingModel> bills = iBilling.GetAllServicesFromLastBill(clientAccount.ClientId, clientAccount.CustomerId,
                            clientAccount.AccountId).ToList();

                        List<BillingModel> meteredBills =
                            bills.Where(b => b.MeterType == Convert.ToString(Enums.MeterType.ami)).ToList();

                        // For each bill
                        foreach (var meteredBill in meteredBills)
                        {
                            // Convert the bill to an AMI Request Model
                            var amiRequest = AutoMapper.Mapper.Map<BillingModel, AmiRequestModel>(meteredBill);

                            logModel.PremiseId = amiRequest.PremiseId;
                            logModel.ServiceContractId = amiRequest.ServiceId;
                            logModel.MeterId = amiRequest.MeterId;

                            // get btd setting for projected num days bill to date to determine if we should compute the start date or use bill cycle schedule
                            var btdSettings = iClientConfigFacade.GetBillToDateSettings(clientId);

                            BillCycleScheduleModel billCycleSchedule;
                            DateTime startDate;
                            if (btdSettings.Settings.General.UseProjectedNumDaysforBilltoDate)
                            {
                                startDate = meteredBill.EndDate.AddDays(1);
                                var billEndDate = startDate.AddDays(btdSettings.Settings.General.ProjectedNumberOfDays - 1);

                                billCycleSchedule = new BillCycleScheduleModel
                                {
                                    BeginDate = startDate.ToString("MM/dd/yyyy"),
                                    EndDate = billEndDate.ToString("MM/dd/yyyy")
                                };
                                Logger.Warn(
                                    $"Bill cycle schedule is overrided for meter Id {meteredBill.MeterId} Begin Date {billCycleSchedule.BeginDate} End Date{billCycleSchedule.EndDate}.",
                                    logModel);
                            }
                            else
                            {
                                // Get the current billing cycle schedule.
                                billCycleSchedule = iBillingCycleSchedule.GetBillingCycleScheduleByDateAsync(clientId,
                                    meteredBill.BillCycleScheduleId, endDate);

                                // Get all the AMI data for the current bill cycle.
                                DateTime.TryParse(billCycleSchedule.BeginDate, out startDate);
                            }
                            

                            // See if there is any AMI data for the start of the bill cycle till today.
                            IEnumerable<dynamic> amiEnumerable = iAmi.GetAmiList(startDate, endDate, amiRequest.MeterId, amiRequest.AccountNumber, amiRequest.ClientId);
                            List<dynamic> amiList = amiEnumerable?.ToList();

                            // Determine which row has the last full set of data.
                            DateTime? minStartDate = GetMinDateFromAmi(amiList);

                            // Set the AMI request dates.
                            amiRequest.EndDate = endDate.ToString("MM-dd-yyyy");
                            amiRequest.StartDate = minStartDate?.Date.ToString("MM-dd-yyyy") ?? startDate.ToString("MM-dd-yyyy");
                            amiRequest.BillingPeriodStartDate = billCycleSchedule.BeginDate;

                            AddAmiRequest(amiRequest, electricAmiRequests, gasAmiRequests, waterAmiRequests);
                        }
                    }
                } while (token != null);

                var fileNames = new Dictionary<string, string>();
                if (createOutputPerCommodity)
                {
                    WriteToFile(electricAmiRequests, filePath, $"{fileNameWithoutExtension}_electric", environment, maxRowsPerFile).ToList().ForEach(x => fileNames.Add(x.Key, x.Value));
                    WriteToFile(gasAmiRequests, filePath, $"{fileNameWithoutExtension}_gas", environment, maxRowsPerFile).ToList().ForEach(x => fileNames.Add(x.Key, x.Value));
                    WriteToFile(waterAmiRequests, filePath, $"{fileNameWithoutExtension}_water", environment, maxRowsPerFile).ToList().ForEach(x => fileNames.Add(x.Key, x.Value));
                }
                else
                {
                    var amiRequestList = new List<AmiRequestModel>();
                    amiRequestList.AddRange(electricAmiRequests);
                    amiRequestList.AddRange(gasAmiRequests);
                    amiRequestList.AddRange(waterAmiRequests);
                    WriteToFile(amiRequestList, filePath, fileNameWithoutExtension, environment, maxRowsPerFile).ToList().ForEach(x => fileNames.Add(x.Key, x.Value));
                }

                PutFilesToFtp(fileNames, logModel);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, logModel);
            }
        }

        private static SessionOptions _sessionOptions;

        private static SessionOptions GetSessionOptions()
        {
            if (_sessionOptions == null)
            {
                var sessionOptions = new SessionOptions
                {
                    Protocol = Protocol.Sftp,
                    HostName = ConfigurationManager.AppSettings["ftpHost"],
                    UserName = ConfigurationManager.AppSettings["ftpUserName"],
                    Password = ConfigurationManager.AppSettings["ftpPassword"],
                    FtpMode = FtpMode.Passive,
                    FtpSecure = FtpSecure.None,
                    SshHostKeyFingerprint = ConfigurationManager.AppSettings["HostCertificateFingerprint"]
                };
                _sessionOptions = sessionOptions;
            }
            return _sessionOptions;
        }

        private static void PutFilesToFtp(Dictionary<string, string> fileNames, LogModel logModel)
        {
            foreach (KeyValuePair<string, string> fileName in fileNames)
            {
                var trasnferOptions = new TransferOptions
                {
                    PreserveTimestamp = false,
                    FilePermissions = null
                };
                var sessionOptions = GetSessionOptions();
                using (var session = new Session())
                {
                    session.Open(sessionOptions);
                    var remoteFolderPath = ConfigurationManager.AppSettings["ftpDestinationPath"] + fileName.Key;
                    var result = session.PutFiles(fileName.Value, remoteFolderPath, true, trasnferOptions);
                    if (!result.IsSuccess)
                    {
                        Logger.Fatal("Unable to drop file to FTP location.", logModel);
                    }
                }
            }
        }

        private static Dictionary<string, string> WriteToFile(List<AmiRequestModel> amiRequests, string filePath, string fileNameWithoutExtension, string environment, int maxRowsPerFile)
        {
            var fileNames = new Dictionary<string, string>();
            if (amiRequests.Count > 0)
            {
                var environmentFile = string.IsNullOrWhiteSpace(environment) ? "" : $"{environment}";
                if (amiRequests.Count > maxRowsPerFile)
                {
                    var loopCount = Convert.ToInt32(amiRequests.Count / maxRowsPerFile);
                    var totalRequestsCreated = 0;
                    for (var i = 0; i <= loopCount - 1; i++)
                    {
                        List<AmiRequestModel> requests = amiRequests.Skip(i * maxRowsPerFile).Take(maxRowsPerFile).ToList();
                        var fileNameWithExtension = $"{fileNameWithoutExtension}_{i}{environmentFile}.csv";
                        var fileName = $"{filePath}{fileNameWithExtension}";
                        GenerateFile(fileName, requests);
                        fileNames.Add(fileNameWithExtension, fileName);
                        totalRequestsCreated = (i + 1) * maxRowsPerFile;
                    }

                    if (totalRequestsCreated != amiRequests.Count)
                    {
                        List<AmiRequestModel> requests = amiRequests.Skip(totalRequestsCreated).ToList();
                        var fileNameWithExtension = $"{fileNameWithoutExtension}_{loopCount}{environmentFile}.csv";
                        var fileName = $"{filePath}{fileNameWithExtension}";
                        GenerateFile(fileName, requests);
                        fileNames.Add(fileNameWithExtension, fileName);
                    }
                }
                else
                {
                    var fileNameWithExtension = $"{fileNameWithoutExtension}{environmentFile}.csv";
                    var fileName = $"{filePath}{fileNameWithExtension}";
                    GenerateFile(fileName, amiRequests);
                    fileNames.Add(fileNameWithExtension, fileName);
                }
            }
            return fileNames;
        }

        private static void GenerateFile(string fileName, List<AmiRequestModel> amiRequests)
        {
            using (var fileStream = new FileStream($"{fileName}", FileMode.Create, FileAccess.Write))
            {
                using (var streamWriter = new StreamWriter(fileStream))
                using (var csvWriter = new CsvWriter(streamWriter))
                {
                    csvWriter.WriteRecords(amiRequests.AsEnumerable());
                } // StreamWriter gets flushed here.                
            }
        }

        private static void AddAmiRequest(AmiRequestModel amiRequest, ICollection<AmiRequestModel> electricAmiRequests, ICollection<AmiRequestModel> gasAmiRequests, ICollection<AmiRequestModel> waterAmiRequests)
        {
            var amiCommodityTypeEnum = (Enums.CommodityType)Enum.Parse(typeof(Enums.CommodityType), Convert.ToString(amiRequest.FuelId));
            switch (amiCommodityTypeEnum)
            {
                case Enums.CommodityType.Electric:
                    electricAmiRequests.Add(amiRequest);
                    break;
                case Enums.CommodityType.Gas:
                    gasAmiRequests.Add(amiRequest);
                    break;
                case Enums.CommodityType.Water:
                    waterAmiRequests.Add(amiRequest);
                    break;
                case Enums.CommodityType.Undefined:
                    break;
            }
        }

        /// <summary>
        /// Look for  last date that has  a AMI reading.
        /// </summary>
        /// <param name="amiList">The AMI list.</param>
        /// <returns>The Date of the last AMI reading.</returns>
        private static DateTime? GetMinDateFromAmi(List<dynamic> amiList)
        {
            if (amiList != null)
            {
                amiList = amiList.OrderByDescending(o => o.AmiTimeStamp).ToList();

                // ReSharper disable once LoopCanBeConvertedToQuery
                foreach (var ami in amiList)
                {
                    // If the ami date for any row missing data.
                    if (GetCountOfNullColumns(ami, "IntValue") <= 0)
                    {
                        return ami.AmiTimeStamp;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Look over a Table date row columns to determine if any of them have a null value
        /// </summary>
        /// <typeparam name="T">The Data Type </typeparam>
        /// <param name="model">The data to be looked at.</param>
        /// <param name="filterColumnNames">The columns to be looked at.</param>
        /// <returns>The number null values found in the columns being reviewed.</returns>
        private static int GetCountOfNullColumns<T>(T model, string filterColumnNames)
        {
            // Get the columns
            List<PropertyInfo> properties = string.IsNullOrWhiteSpace(filterColumnNames) ?
                model.GetType().GetProperties().ToList() :
                model.GetType().GetProperties().Where(p => p.Name.Contains(filterColumnNames)).ToList();

            // Caclulate the number with null values.
            var nullColumns = 0;
            properties.ForEach(property => { nullColumns = nullColumns + (property.GetValue(model) == null ? 1 : 0); });
            return nullColumns;
        }
    }
}
