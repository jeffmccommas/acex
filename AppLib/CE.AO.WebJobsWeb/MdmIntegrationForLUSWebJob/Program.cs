﻿using System;
using System.Configuration;
using AO.Registrar;
using Microsoft.Azure.WebJobs;
using Microsoft.Practices.Unity;

namespace MdmIntegrationForLUSWebJob
{
    // To learn more about Microsoft Azure WebJobs SDK, please see http://go.microsoft.com/fwlink/?LinkID=320976
    internal class Program
    {
        // Please set the following connection strings in app.config for this WebJob to run:
        // AzureWebJobsDashboard and AzureWebJobsStorage
        public static void Main()
        {
            new DataStorageRegistrar().Initialize<TransientLifetimeManager>();


            new CE.ContentModel.AutoMapperBootStrapper().BootStrap();
            
            var storageConn = ConfigurationManager.AppSettings.Get(StaticConfig.AclaraOneStorageConnectionString);
            var config = new JobHostConfiguration
            {
                StorageConnectionString = storageConn,
                DashboardConnectionString = storageConn
            };

            var host = new JobHost(config);
            // The following code will invoke a function called ManualTrigger and 
            // pass in data (value in this case) to the function
            host.Call(typeof(Functions).GetMethod("MdmIntegrationForLus"),
                new
                {
                    clientId = Convert.ToInt32(ConfigurationManager.AppSettings.Get("ClientId")),
                    environment = ConfigurationManager.AppSettings.Get("Environment"),
                    meterListForCustomersOfType = ConfigurationManager.AppSettings.Get("MeterListForCustomersOfType"),
                    createOutputPerCommodity = ConfigurationManager.AppSettings.Get("CreateOutputPerCommodity"),
                    maxRowsPerFile = Convert.ToInt32(ConfigurationManager.AppSettings.Get("MaxRowsPerFile"))
                });
        }
    }
}
