﻿using System;
using System.ComponentModel;
using System.Configuration;
using AO.Registrar;
using Microsoft.Practices.Unity;

namespace ScheduleSMSStatusWebJob
{
    // To learn more about Microsoft Azure WebJobs SDK, please see http://go.microsoft.com/fwlink/?LinkID=320976
    class Program
    {
        // Please set the following connection strings in app.config for this WebJob to run:
        // AzureWebJobsDashboard and AzureWebJobsStorage
        private const string ConfigurationAppSettingKeyEnvironment = "Environment";
        private const string ConfigurationAppSettingKeyProcessingEnabled = "ProcessingEnabled";

        static void Main()
        {
            try
            {
                var env = ConfigurationManager.AppSettings[ConfigurationAppSettingKeyEnvironment];
                if (AppSettings.Get<bool>(ConfigurationAppSettingKeyProcessingEnabled) == false)
                {
                    Console.WriteLine(
                        $"Processing has been disabled for this environment. Exited without processing. (Environment: {env})");

                    return;
                }

                var container = (new DataStorageRegistrar()).Initialize<TransientLifetimeManager>();

                new CE.ContentModel.AutoMapperBootStrapper().BootStrap();

                var smsProcess = new SmsProcess();
                smsProcess.ProcessNotifications();

                /*var manager = new NotificationManager();

                manager.ProcessNotifications();
				 var host = new JobHost();
				// The following code will invoke a function called ManualTrigger and 
				// pass in data (value in this case) to the function
				host.Call(typeof(Functions).GetMethod("ManualTrigger"), new { value = 20 });
				*/
            }
            // ReSharper disable once RedundantCatchClause
            catch (Exception)
            {
                throw;
            }
        }
    }
    /// <summary>
    /// Type safe app settings class.
    /// </summary>
    public static class AppSettings
    {
        public static T Get<T>(string key)
        {
            var appSetting = ConfigurationManager.AppSettings[key];
            if (string.IsNullOrWhiteSpace(appSetting)) throw new SettingsPropertyNotFoundException(key);

            var converter = TypeDescriptor.GetConverter(typeof(T));
            return (T)(converter.ConvertFromInvariantString(appSetting));
        }
    }
}
