﻿using System;
using System.IO;
using Microsoft.Azure.WebJobs;

namespace ScheduleSMSStatusWebJob
{
    public class Functions
    {
        // This function will be triggered based on the schedule you have set for this WebJob
        // This function will enqueue a message on an Azure Queue called queue
        [NoAutomaticTrigger]
        public static void ManualTrigger(TextWriter log, int value, [Queue("queue")] out string message)
        {
            log.WriteLine($"Function is invoked with value={value}");
            message = value.ToString();
            log.WriteLine($"Following message will be written on the Queue={message}");
			Console.WriteLine("Inside function.cs");
        }
    }
}
