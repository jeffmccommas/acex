﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AO.BusinessContracts;
using AO.Registrar;
using CE.AO.Logging;
using Microsoft.Practices.Unity;
using CE.AO.Models;
using CE.ContentModel;

namespace ScheduleSMSStatusWebJob
{
    public class SmsProcess
    {
        public void ProcessNotifications()
        {
            var unityContainer = new UnityContainer();

            (new DataStorageRegistrar()).Initialize<TransientLifetimeManager>(unityContainer);

            new AutoMapperBootStrapper().BootStrap();
            var logModel = new LogModel();
            //Setup the direct injection container
            
            try
            {
                // Load the direct injection container.
                var iNotification = unityContainer.Resolve<INotification>(
                    new ParameterOverride("logModel", logModel));

                var iBilling = unityContainer.Resolve<IBilling>(new ParameterOverride("logModel", logModel));

                var iSubscription = unityContainer.Resolve<ISubscription>(
                   new ParameterOverride("logModel", logModel));

                var iCustomer = unityContainer.Resolve<ICustomer>(
                   new ParameterOverride("logModel", logModel));

                var iEvaluation = unityContainer.Resolve<IEvaluation>(new ParameterOverride("logModel", logModel));

                //Call Notification class method to fetch rows of Notification Table Storage
                var notifications = iNotification.GetScheduledNotifications().ToList();
                
                var clientConfigFacadeManager = unityContainer.Resolve<IClientConfigFacade>();

                //Calling GetSmsReport method for each notification
                foreach (var notification in notifications)
                {
                    InsightModel insightModel;
                    if (!string.IsNullOrEmpty(notification.ServiceContractId))
                    {
                        insightModel =
                            iEvaluation.GetServiceLevelEvaluationsAsync(notification.ClientId, notification.CustomerId,
                                notification.AccountId, notification.ProgramName, notification.ServiceContractId).Result;
                    }
                    else
                    {
                        insightModel = iEvaluation.GetAccountLevelEvaluationsAsync(notification.ClientId,
                            notification.CustomerId, notification.AccountId, notification.ProgramName).Result;
                    }
                    var clientSettings = clientConfigFacadeManager.GetClientSettings(notification.ClientId);
                    var apikey = clientSettings.TrumpiaApiKey;
                    var username = clientSettings.TrumpiaUserName;

                    List<SubscriptionModel> subscription = iSubscription.GetCustomerSubscriptions(notification.ClientId,
                    notification.CustomerId, notification.AccountId).ToList();
                    Task<CustomerModel> customer = iCustomer.GetCustomerAsync(notification.ClientId, notification.CustomerId);
                    

                    if (string.IsNullOrEmpty(notification.ServiceContractId))
                    {
                        //Call to GetSmsReport method
                        iNotification.GetSmsReport(null, subscription, notification, insightModel, customer.Result, null, apikey, username, DateTime.UtcNow);
                    }
                    else
                    {
                        Task<BillingModel> billing = Task.Run(() => iBilling.GetServiceDetailsFromBillAsync(notification.ClientId, notification.CustomerId, notification.AccountId, notification.ServiceContractId));
                        Task.WaitAll(billing);
                        //Call to GetSmsReport method
                        iNotification.GetSmsReport(billing.Result, subscription, notification, insightModel, customer.Result, null, apikey, username, DateTime.UtcNow);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, logModel);
            }
        }
    }
}
