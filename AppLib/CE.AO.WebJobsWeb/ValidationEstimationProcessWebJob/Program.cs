﻿using System.Configuration;
using Microsoft.Azure.WebJobs;

namespace ValidationEstimationProcessWebJob
{    
        static class Program
        {
            static void Main()
            {
                var storageConn = ConfigurationManager.ConnectionStrings[StaticConfig.AzureWebJobsStorage].ToString();
                var config = new JobHostConfiguration
                {
                    StorageConnectionString = storageConn,
                    DashboardConnectionString = storageConn
                };
                var host = new JobHost(config);
                host.RunAndBlock();
            }
        }    
}
