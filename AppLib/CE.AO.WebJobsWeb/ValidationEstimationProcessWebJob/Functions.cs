﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using Microsoft.Azure.WebJobs;
using Microsoft.Practices.Unity;
using AO.BusinessContracts;
using AO.VEE.DTO;
using AO.VEE.Registrar;
using CE.AO.Models;
using Enums = CE.AO.Utilities.Enums;

namespace ValidationEstimationProcessWebJob
{
	/// <summary>
	/// Function class will process AoVeeQueue message
	/// </summary>
	public static class Functions
    {
        private static readonly IUnityContainer UnityContainer = new UnityContainer();

        /// <summary>	
        /// This method will read messages from aoveequeue, extracts Client Id and Date from message and then pass the Client Id and Date to Trigger method of AO.VEE.Business.	
        /// </summary>
        /// <param name="message"></param>
        /// <param name="log"></param>
        public static void ProcessClientMeters([QueueTrigger(StaticConfig.AoVeeQueue)] string message, TextWriter log)
        {
            new CE.ContentModel.AutoMapperBootStrapper().BootStrap();
            new VeeRegistrar().Initialize<TransientLifetimeManager>(UnityContainer);
            var process = UnityContainer.Resolve<IProcessTrigger<VeeValidationEstimationTriggerDto>>();
            try
            {
                string[] parameter = message.Split(new[] { ":" }, StringSplitOptions.None).ToArray();
                var clientId = Convert.ToInt32(parameter[0]);
                var date = Convert.ToString(parameter[1]);
                DateTime amiDateTime;
                string[] formats = { "M-d-yyyy", "M-dd-yyyy", "MM-dd-yyyy", "M/d/yyyy", "M/dd/yyyy", "MM/dd/yyyy" };
                DateTime? amiDate = DateTime.TryParseExact(date, formats, CultureInfo.InvariantCulture, DateTimeStyles.None,
                    out amiDateTime) ? (DateTime?)amiDateTime : null;
                var veeValidationEstimationTrigger = new VeeValidationEstimationTriggerDto
                {
                    ClientId = clientId,
                    AmiDate = amiDate
                };
                process.LogModel = new LogModel { ClientId = Convert.ToString(clientId), Module = Enums.Module.ValidationEstimationProcess };
                process.Trigger(veeValidationEstimationTrigger);
            }
            catch (Exception ex)
            {
                log.WriteLine(ex.Message + ex.InnerException);
                throw;
            }
        }
    }
}
