﻿using System;
using System.Configuration;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using Quartz;
using Quartz.Impl;

namespace ScheduleCalculationsWebJob
{
    public class Common
    {
        private static CloudQueue _thumbnailRequestQueue;

        public static JobDetailImpl DailyMainCall(out ITrigger trigger, string jobName)
        {
            var hour = 00;
            var min = 05;

            var job = new JobDetailImpl(jobName, null, typeof(CreateSchedules));

            var dailyScheduleTime = ConfigurationManager.AppSettings["DailyScheduleTime"];
            string[] scheduletime = string.IsNullOrEmpty(dailyScheduleTime) ? null : dailyScheduleTime.Split(':');

            if (scheduletime != null)
            {
                hour = Convert.ToInt32(scheduletime[0]);
                min = Convert.ToInt32(scheduletime[1]);
            }

            trigger = TriggerBuilder.Create().StartNow().WithSchedule(CronScheduleBuilder.DailyAtHourAndMinute(hour, min)).Build();
            return job;
        }

        public static void SaveToQueue(string message, string queueName)
        {
            var storageAccount = CloudStorageAccount.Parse(ConfigurationManager.ConnectionStrings["AzureWebJobsStorage"].ToString());
            storageAccount.CreateCloudBlobClient();
            var queueClient = storageAccount.CreateCloudQueueClient();
            _thumbnailRequestQueue = queueClient.GetQueueReference(queueName);
            _thumbnailRequestQueue.AddMessage(new CloudQueueMessage(message));
            Console.WriteLine("Trigger Done in queue" + queueName);
        }

    }
}
