﻿using System;
using Microsoft.Azure.WebJobs;
using System.IO;
using Quartz;
using Quartz.Impl;

namespace ScheduleCalculationsWebJob
{
    public class Functions
    {
        #region unused
        //// This function will be triggered based on the schedule you have set for this WebJob
        //// This function will enqueue a message on an Azure Queue called queue
        //[NoAutomaticTrigger]
        // public static void ManualTrigger(TextWriter log, int value, [Queue("queue")] out string message)
        // {
        //    log.WriteLine("Function is invoked with value={0}", value);
        //    message = value.ToString();
        //    log.WriteLine("Following message will be written on the Queue={0}", message);
        // }
        #endregion

        [NoAutomaticTrigger]
        public static void ScheduleCalculation(TextWriter log)
        {
            try
            {
                Console.WriteLine("Test start calculation");
                ISchedulerFactory sf = new StdSchedulerFactory();
                var sched = sf.GetScheduler();

                ITrigger trigger;
                var job = Common.DailyMainCall(out trigger, "DailyScheduledCall");

                sched.ScheduleJob(job, trigger);
                sched.Start();

                Console.WriteLine("Test scheduledone");
            }
            catch (Exception ex)
            {
                log.WriteLine(ex.Message + ex.InnerException);
                throw;
            }
        }

        #region unused

        //Create Main Job Scheduling

        //public static void sendMessageTesting(string message)
        //{
        //    CloudQueue thumbnailRequestQueue;
        //    var storageAccount = CloudStorageAccount.Parse(ConfigurationManager.ConnectionStrings["AzureWebJobsStorage"].ToString());
        //    var blobClient = storageAccount.CreateCloudBlobClient();
        //    CloudQueueClient queueClient = storageAccount.CreateCloudQueueClient();
        //    thumbnailRequestQueue = queueClient.GetQueueReference("test");
        //    thumbnailRequestQueue.CreateIfNotExists();
        //    thumbnailRequestQueue.AddMessage(new CloudQueueMessage(message));
        //}

        //private static JobDetailImpl JobDetailImpl(out ITrigger trigger, string message, string jobName)
        //{
        //    var job = new JobDetailImpl(jobName, null, typeof(AddToQueueJob));
        //    job.JobDataMap["message"] = message;
        //    var timeZoneInfo = TimeZoneInfo.Local;
        //    int hour = 18;
        //    int min = 36;
        //    var cronScheduleBuilder = CronScheduleBuilder.DailyAtHourAndMinute(hour, min).InTimeZone(timeZoneInfo);
        //    trigger = TriggerBuilder.Create()
        //        .StartNow()
        //        .WithSimpleSchedule(x => x.WithRepeatCount(0).RepeatForever().WithIntervalInMinutes(1))
        //        .Build();
        //    return job;
        //}

        #endregion


    }
}
