﻿using System;
using Quartz;

namespace ScheduleCalculationsWebJob
{
    class AddToQueueJob : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            Console.WriteLine("Trigger Started");
            JobDataMap dataMap = context.JobDetail.JobDataMap;
            var message = dataMap.GetString("message");
            Console.WriteLine(message);
            Common.SaveToQueue(message, dataMap.GetString("queueName"));
        }

    }
}
