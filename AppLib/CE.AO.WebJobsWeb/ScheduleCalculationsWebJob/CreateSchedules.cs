﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using AO.BusinessContracts;
using AO.Registrar;
using CE.AO.Entities;
using CE.AO.Models;
using CE.AO.Utilities;
using Microsoft.Practices.Unity;
using Quartz;
using Quartz.Impl;
using Enums = CE.AO.Utilities.Enums;

namespace ScheduleCalculationsWebJob
{
    class CreateSchedules : IJob
    {
        private readonly IUnityContainer _uContainer = new UnityContainer();

        public void Execute(IJobExecutionContext context)
        {
            Console.WriteLine("Execution Started");
            (new DataStorageRegistrar()).Initialize<TransientLifetimeManager>(_uContainer);
            ISchedulerFactory sf = new StdSchedulerFactory();
            var clientsWithComma = GetAllClientsFromContentModel();
            var clients = string.IsNullOrEmpty(clientsWithComma) ? null : clientsWithComma.Split(',');

            var listOfClientSettings = new List<ClientSettings>();

            if (clients != null)
            {
                var clientConfigFacadeManager = _uContainer.Resolve<IClientConfigFacade>();
                listOfClientSettings.AddRange(clients.Select(clientId => clientConfigFacadeManager.GetClientSettings(Convert.ToInt32(clientId))).Where(y => y != null));
            }

            var sched = sf.GetScheduler();
            LoadJobs(sched, listOfClientSettings);
            sched.Start();
        }

        public void LoadJobs(IScheduler sched, List<ClientSettings> clientSettings)
        {
            Console.WriteLine("LoadJobs Started");

            foreach (var setting in clientSettings)
            {
                string message = setting.ClientId.ToString();
                
                var clientTimezoneEnum = (Enums.Timezone)Enum.Parse(typeof(Enums.Timezone), Convert.ToString(setting.TimeZone));

                var clientTimezone = TimeZoneInfo.FindSystemTimeZoneById(clientTimezoneEnum.GetDescriptionOfEnum());
                var utcToclientTimeZone = DateTime.UtcNow;

                ITrigger trigger;
                if (!string.IsNullOrEmpty(setting.CalculationScheduleTime))
                {
                    var array = setting.CalculationScheduleTime.Split(':').ToArray();

                    int hour = Convert.ToInt32(array[0]);
                    int min = Convert.ToInt32(array[1]);

                    var newdateTime = new DateTime(utcToclientTimeZone.Date.Year, utcToclientTimeZone.Date.Month, utcToclientTimeZone.Date.Day, hour, min, 0);
                    var dateForMessage = newdateTime.AddDays(-1);
                    var utcNewDateTime = TimeZoneInfo.ConvertTimeToUtc(newdateTime, clientTimezone);

                    StringBuilder newMessage = new StringBuilder();
                    string newrMessageForCal = newMessage.Append(message).Append("^^").Append(dateForMessage.ToShortDateString()).ToString();

                    var job = AddToQueueJobImpl(out trigger, newrMessageForCal, string.Concat(setting.ClientId, "CalculationSchedule"), true, Constants.QueueNames.AoScheduleCalculationQueue, utcNewDateTime);
                    sched.ScheduleJob(job, trigger);

                }

                if (!string.IsNullOrEmpty(setting.InsightReportScheduleTime))
                {
                    var array = setting.InsightReportScheduleTime.Split(':').ToArray();

                    int hour = Convert.ToInt32(array[0]);
                    int min = Convert.ToInt32(array[1]);

                    var newdateTime = new DateTime(utcToclientTimeZone.Date.Year, utcToclientTimeZone.Date.Month, utcToclientTimeZone.Date.Day, hour, min, 0);
                    var dateForMessage = newdateTime.AddDays(-1);
                    var utcNewDateTime = TimeZoneInfo.ConvertTimeToUtc(newdateTime, clientTimezone);

                    StringBuilder newMessage = new StringBuilder();
                    string newMessageForReport = newMessage.Append(message).Append("^^").Append(dateForMessage.ToShortDateString()).Append("^^").Append("1").ToString();

                    var job = AddToQueueJobImpl(out trigger, newMessageForReport, string.Concat(setting.ClientId, "InsightReportSchedule"), true, Constants.QueueNames.AoInsightReportQueue, utcNewDateTime);
                    sched.ScheduleJob(job, trigger);

                }

                if (string.IsNullOrEmpty(setting.NotificationReportScheduleTime)) continue;
                {
                    var array = setting.NotificationReportScheduleTime.Split(':').ToArray();

                    int hour = Convert.ToInt32(array[0]);
                    int min = Convert.ToInt32(array[1]);

                    var newdateTime = new DateTime(utcToclientTimeZone.Date.Year, utcToclientTimeZone.Date.Month, utcToclientTimeZone.Date.Day, hour, min, 0);
                    var dateForMessage = newdateTime.AddDays(-1);
                    var utcNewDateTime = TimeZoneInfo.ConvertTimeToUtc(newdateTime, clientTimezone);

                    StringBuilder newMessage = new StringBuilder();

                    string newrMessageForNotify = newMessage.Append(message).Append("^^").Append(dateForMessage.ToShortDateString()).Append("^^").Append("1").ToString();
                    var job = AddToQueueJobImpl(out trigger, newrMessageForNotify, string.Concat(setting.ClientId, "NotificationReportSchedule"), true, Constants.QueueNames.AoNotificationReportQueue, utcNewDateTime);
                    sched.ScheduleJob(job, trigger);

                }
            }

            sched.Start();
        }

        private static JobDetailImpl AddToQueueJobImpl(out ITrigger trigger, string message, string jobName, bool stop, string queueName, DateTime asOfDate)
        {
            var job = new JobDetailImpl(jobName, null, typeof(AddToQueueJob))
            {
                JobDataMap =
                {
                    ["queueName"] = queueName,
                    ["Stop"] = stop.ToString().ToLower(),
                    ["message"] = message
                }
            };

            #region previous Logic

            //string timeZone
            //trigger = TriggerBuilder.Create()
            //                        .StartNow()
            //                        //.WithSimpleSchedule(x => x.WithRepeatCount(0).RepeatForever().WithIntervalInMinutes(1))
            //                        .WithSchedule(CronScheduleBuilder.DailyAtHourAndMinute(hour, min).InTimeZone(clientTimezone))
            //                        //.WithSchedule(CronScheduleBuilder.DailyAtHourAndMinute(hour, min))
            //                        .Build();
            //Console.WriteLine("newdateTime:" + newdateTime.ToString());
            //Console.WriteLine("UtcNewDateTime:" + UtcNewDateTime.ToString());

            #endregion

            trigger = TriggerBuilder.Create()
                                        .StartAt(asOfDate)
                                        .Build();
            return job;
        }

        private static string GetAllClientsFromContentModel()
        {
            return ConfigurationManager.AppSettings["ClientList"];
        }

    }
}
