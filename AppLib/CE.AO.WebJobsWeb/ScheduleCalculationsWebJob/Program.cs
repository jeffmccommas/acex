﻿using System;
using Microsoft.Azure.WebJobs;
using System.Configuration;
using System.Threading.Tasks;

namespace ScheduleCalculationsWebJob
{
    // To learn more about Microsoft Azure WebJobs SDK, please see http://go.microsoft.com/fwlink/?LinkID=320976
    class Program
    {
        // Please set the following connection strings in app.config for this WebJob to run:
        // AzureWebJobsDashboard and AzureWebJobsStorage
        static void Main()
        {
            new CE.ContentModel.AutoMapperBootStrapper().BootStrap();            
            var storageConn = ConfigurationManager.ConnectionStrings[StaticConfig.AclaraOneStorageConnectionString].ToString();
            Console.WriteLine($"Connection String: {storageConn}");
            storageConn = ConfigurationManager.ConnectionStrings["AzureWebJobsStorage"].ToString();
            Console.WriteLine($"Connection String: {storageConn}");

            var config = new JobHostConfiguration
            {
                StorageConnectionString = storageConn,
                DashboardConnectionString = storageConn
            };

            var host = new JobHost(config);
            // The following code will invoke a function called ManualTrigger and 
            // pass in data (value in this case) to the function
            Task.Factory.StartNew(() => host.Call(typeof(Functions).GetMethod("ScheduleCalculation")));
            host.RunAndBlock();
        }
    }
}
