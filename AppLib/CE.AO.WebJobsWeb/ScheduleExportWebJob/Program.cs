﻿using System;
using System.Configuration;
using Microsoft.Azure.WebJobs;

namespace ScheduleExportWebJob
{
    
    class Program
    {
        // Please set the following connection strings in app.config for this WebJob to run:
        // AzureWebJobsStorage
        static void Main()
        {

            new CE.ContentModel.AutoMapperBootStrapper().BootStrap();
            var storageConn = ConfigurationManager.ConnectionStrings[StaticConfig.AclaraOneStorageConnectionString].ToString();
            Console.WriteLine($"Connection String: {storageConn}");
            storageConn = ConfigurationManager.ConnectionStrings["AzureWebJobsStorage"].ToString();
            Console.WriteLine($"Connection String: {storageConn}");

            var config = new JobHostConfiguration
            {
                StorageConnectionString = storageConn,
                DashboardConnectionString = storageConn
            };

            var host = new JobHost(config);
            host.RunAndBlock();
        }
    }
}
