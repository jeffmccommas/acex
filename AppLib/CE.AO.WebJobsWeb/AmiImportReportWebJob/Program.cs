﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CE.AO.AmiImportReport;
using Microsoft.Azure.WebJobs;

namespace AmiImportReportWebJob {
    // To learn more about Microsoft Azure WebJobs SDK, please see https://go.microsoft.com/fwlink/?LinkID=320976
    class Program {
        // Please set the following connection strings in app.config for this WebJob to run:
        // AzureWebJobsDashboard and AzureWebJobsStorage
        static void Main() {
            var report = new AmiImportReport();
            // no overwrite args when execute from webjob, pass in null, and use default setting
            report.GenerateAmiReports(null);
        }

        
    }
}
