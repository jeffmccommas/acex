﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CE.AO.NotificationStatsWebAPI.Startup))]
namespace CE.AO.NotificationStatsWebAPI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
