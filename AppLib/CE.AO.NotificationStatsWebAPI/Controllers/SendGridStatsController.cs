﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using AO.BusinessContracts;
using AO.Registrar;
using CE.AO.Logging;
using CE.AO.Models;
using Microsoft.Practices.Unity;
using Newtonsoft.Json;
using Enums = CE.AO.Utilities.Enums;

namespace CE.AO.NotificationStatsWebAPI.Controllers
{
    /// <summary>
    /// SendGridStatsController class is used to get push notification (like delivered, open, dropped etc.). Then according to event update the notification table.
    /// </summary>
    public class SendGridStatsController : Controller
    {
        private readonly IUnityContainer _uContainer = new UnityContainer();
        private const string KsnEmail = "Email";

        public SendGridStatsController() : this(new LogModel { Module = Enums.Module.NotificationStats })
        {
        }

        public SendGridStatsController(LogModel logModel)
        {
            (new DataStorageRegistrar()).Initialize<TransientLifetimeManager>(_uContainer);
            
            LogModel = logModel;
        }

        public LogModel LogModel { get; set; }

        /// <summary>
        /// Confirm email double opt in & update suscription with same.
        /// </summary>
        /// <param name="description">combination clientId, customerId, accountId, programName</param>
        /// <returns>view ConfirmEmailDoubleOptIn.cshtml</returns>
        [HttpGet]
        public ActionResult ConfirmEmailDoubleOptIn(string description)
        {
            var responseMsg = string.Empty;
            byte[] decodeDescription = Convert.FromBase64String(description);
            string[] columns = System.Text.Encoding.UTF8.GetString(decodeDescription).Split('|');

            if (columns.Length == 4)
            {
                try
                {
                    var clientId = columns[0];
                    var customerId = columns[1];
                    var accountId = columns[2];
                    var programName = columns[4];

                    var subscription = _uContainer.Resolve<ISubscription>();
                    var subscriptionDetails =
                        subscription.GetSubscriptionDetails(Convert.ToInt32(clientId), customerId, accountId, programName, "", "");

                    if (subscriptionDetails != null)
                    {
                        LogModel.AccountId = subscriptionDetails.AccountId;
                        LogModel.ClientId = Convert.ToString(subscriptionDetails.ClientId);
                        LogModel.CustomerId = subscriptionDetails.CustomerId;
                        LogModel.ServiceContractId = subscriptionDetails.ServiceContractId;
                        LogModel.Module = Enums.Module.NotificationStats;
                        LogModel.Source = "SendGrid";

                        subscriptionDetails.IsEmailOptInCompleted = true;
                        subscription.InsertSubscription(subscriptionDetails);

                        responseMsg = string.Format("You have successfully subscribed to " +
                                      subscriptionDetails.ProgramName + ".");
                    }
                    else
                        responseMsg = "Subscription not found.";
                }
                catch (Exception ex)
                {
                    Logger.Error(ex, LogModel);
                    responseMsg = "There is an error while confirming your subscription.";
                }
            }
            ViewBag.Message = responseMsg;
            return View();
        }

        /// <summary>
        /// Get push notification (like delivered, open, dropped etc.) & update notification with same.
        /// </summary>
        [HttpPost]
        public void Post()
        {
            try
            {
                var req = Request.InputStream;
                req.Seek(0, SeekOrigin.Begin);
                var json = new StreamReader(req).ReadToEnd();
                var emailStatsModels = JsonConvert.DeserializeObject<List<EmailStatsModel>>(json);

                foreach (EmailStatsModel emailStatsModel in emailStatsModels)
                {
                    try
                    {
                        string[] customKey = emailStatsModel.customKey.Split('|');
                        // check subscripton: if the custom key seperator is | it is subs, otherwise assume notify which uses _ (CJH ?!?!?)
                        if (customKey.Length > 1)
                        {
                            UpdateSubscriptionEmailStatus(emailStatsModel, customKey);
                        }
                        else
                        {
                            UpdateNotificationEmailStatus(emailStatsModel, customKey);
                        }
                    }
                    catch (Exception ix) {
                        // force some diagnostics data into log model rather than change log subsystem.
                        // in this case we've seen source is blank.
                        string ck = "ck="+ (emailStatsModel.customKey ?? "NoCustomKey");
                        ck += ":cl=" + emailStatsModel.ClientId;
                        ck += ":pn="+ (emailStatsModel.programName ?? "NoProgramName:");
                        if (LogModel.Source != null) LogModel.Source += $"[esm:{ck}]";
                        else LogModel.Source = $"[esm:{ck}]";
                        Logger.Error(ix, LogModel);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, LogModel);
            }
        }

        private void UpdateNotificationEmailStatus(EmailStatsModel emailStatsModel, string[] customKey)
        {
            var notification = _uContainer.Resolve<INotification>();
            var emailRequestDetail = _uContainer.Resolve<IEmailRequestDetail>();
            string[] customKeyParts = customKey[0].Split(';');

            var clientId = Convert.ToInt32(customKeyParts[0]);
            var customerId = customKeyParts[1];
            var accountId = customKeyParts[2];
            var notifiedDateTime = customKeyParts[3];
            var programName = customKeyParts[4];
            var serviceContractId = customKeyParts[5];
            var insightName = customKeyParts[6];
            var isLatest = Convert.ToBoolean(customKeyParts[7]);
            var defaultCommunicationChannel = customKeyParts[8];

            var notificationModel = notification.GetNotificationSync(clientId, customerId, accountId, programName,
                                                                     serviceContractId, insightName, defaultCommunicationChannel,
                                                                     isLatest, notifiedDateTime);

            if (notificationModel != null)
            {
                LogModel.ClientId = Convert.ToString(notificationModel.ClientId);
                LogModel.AccountId = notificationModel.AccountId;
                LogModel.CustomerId = notificationModel.CustomerId;
                LogModel.ServiceContractId = notificationModel.ServiceContractId;
                LogModel.Module = Enums.Module.NotificationStats;
                LogModel.Source = "SendGrid";

                var emailEventEnum =
                    (Enums.EmailEvent)
                        Enum.Parse(typeof(Enums.EmailEvent), Convert.ToString(emailStatsModel.@event));

                NotificationModel currentNotification = notification.GetNotificationSync(clientId, customerId, accountId, programName, serviceContractId, insightName, defaultCommunicationChannel, true, notifiedDateTime);

                bool bUpdateCurrentNotification = currentNotification != null && currentNotification.NotifiedDateTime.Date == Convert.ToDateTime(notifiedDateTime).ToUniversalTime().Date;
                
                EmailRequestDetailModel emailRequestDetailModel;
                switch (emailEventEnum)
                {
                    case Enums.EmailEvent.bounce:

                        // get email request detail
                        emailRequestDetailModel = emailRequestDetail.GetEmailRequestDetail(clientId, accountId, programName,
                            serviceContractId, insightName, defaultCommunicationChannel,
                            notifiedDateTime, emailEventEnum.ToString());

                        if (emailRequestDetailModel == null)
                            emailRequestDetailModel = new EmailRequestDetailModel
                            {
                                ClientId = clientId,
                                AccountId = notificationModel.AccountId,
                                Channel = notificationModel.Channel,
                                Insight = notificationModel.Insight,
                                ProgramName = notificationModel.ProgramName,
                                ServiceContractId = notificationModel.ServiceContractId,
                                NotifiedDateTime = notificationModel.NotifiedDateTime.Date
                            };

                        notificationModel.IsBounced = true;
                        notification.SaveNotification(notificationModel);
                        if (bUpdateCurrentNotification)
                        {
                            currentNotification.IsBounced = true;
                            notification.SaveNotification(currentNotification);
                        }
                        // insert email request detail info
                        emailRequestDetailModel.ErrorMessage = emailStatsModel.reason;
                        emailRequestDetailModel.Event = Enums.EmailEvent.bounce.ToString();
                        emailRequestDetail.InsertOrMergeEmailRequestDetail(emailRequestDetailModel);
                        break;
                    case Enums.EmailEvent.click:
                        // get email request detail
                        emailRequestDetailModel = emailRequestDetail.GetEmailRequestDetail(clientId, accountId, programName,
                            serviceContractId, insightName, defaultCommunicationChannel,
                            notifiedDateTime, emailEventEnum.ToString());

                        if (emailRequestDetailModel == null)
                            emailRequestDetailModel = new EmailRequestDetailModel
                            {
                                ClientId = clientId,
                                AccountId = notificationModel.AccountId,
                                Channel = notificationModel.Channel,
                                Insight = notificationModel.Insight,
                                ProgramName = notificationModel.ProgramName,
                                ServiceContractId = notificationModel.ServiceContractId,
                                NotifiedDateTime = notificationModel.NotifiedDateTime.Date
                            };
                        notificationModel.IsClicked = true;
                        notification.SaveNotification(notificationModel);
                        if (bUpdateCurrentNotification)
                        {
                            currentNotification.IsClicked = true;
                            notification.SaveNotification(currentNotification);
                        }
                        // insert email request detail info
                        emailRequestDetailModel.Url = emailStatsModel.url;
                        emailRequestDetailModel.Event = $"{Enums.EmailEvent.click.ToString()}|{emailStatsModel.url}";
                        emailRequestDetail.InsertOrMergeEmailRequestDetail(emailRequestDetailModel);
                        break;
                    case Enums.EmailEvent.delivered:
                        notificationModel.IsDelivered = true;
                        notification.SaveNotification(notificationModel);
                        if (bUpdateCurrentNotification)
                        {
                            currentNotification.IsDelivered = true;
                            notification.SaveNotification(currentNotification);
                        }
                        break;
                    case Enums.EmailEvent.open:
                        notificationModel.IsOpened = true;
                        notification.SaveNotification(notificationModel);
                        if (bUpdateCurrentNotification)
                        {
                            currentNotification.IsOpened = true;
                            notification.SaveNotification(currentNotification);
                        }
                        break;
                    case Enums.EmailEvent.dropped:
                        // get email request detail
                        emailRequestDetailModel = emailRequestDetail.GetEmailRequestDetail(clientId, accountId, programName,
                            serviceContractId, insightName, defaultCommunicationChannel,
                            notifiedDateTime, emailEventEnum.ToString());

                        if (emailRequestDetailModel == null)
                            emailRequestDetailModel = new EmailRequestDetailModel
                            {
                                ClientId = clientId,
                                AccountId = notificationModel.AccountId,
                                Channel = notificationModel.Channel,
                                Insight = notificationModel.Insight,
                                ProgramName = notificationModel.ProgramName,
                                ServiceContractId = notificationModel.ServiceContractId,
                                NotifiedDateTime = notificationModel.NotifiedDateTime.Date
                            };
                        notificationModel.IsDropped = true;
                        notification.SaveNotification(notificationModel);
                        if (bUpdateCurrentNotification)
                        {
                            currentNotification.IsDropped = true;
                            notification.SaveNotification(currentNotification);
                        }
                        // insert email request detail info
                        emailRequestDetailModel.ErrorMessage = emailStatsModel.reason;
                        emailRequestDetailModel.Event = Enums.EmailEvent.dropped.ToString();
                        emailRequestDetail.InsertOrMergeEmailRequestDetail(emailRequestDetailModel);
                        break;
                    case Enums.EmailEvent.unsubscribe:
                        var success = InsertorMergeForOptOut(notificationModel);
                        DeleteUnsubscribedEmailFromSendGrid(notificationModel.ClientId, emailStatsModel.email);
                        if (!success)
                            Logger.Fatal($@" Insertion failed in Notification for customKey: [{emailStatsModel.customKey}].", LogModel);
                        break;
                }

            }
            else
            {
                Logger.Fatal(
                    $@"PartitionKey ""{customKey[0]}"" not valid.", LogModel);
            }
        }

        private void UpdateSubscriptionEmailStatus(EmailStatsModel emailStatsModel, string[] customKey)
        {

            var emailRequestDetail = _uContainer.Resolve<IEmailRequestDetail>();
            var emailEventEnum =
                        (Enums.EmailEvent)
                            Enum.Parse(typeof(Enums.EmailEvent), Convert.ToString(emailStatsModel.@event));

            var subscription = _uContainer.Resolve<ISubscription>();
            string[] subscriptionKeyParts = customKey[1].Split(';');
            var subscriptionEntity = subscription.GetSubscriptionDetails(Convert.ToInt32(subscriptionKeyParts[0]), subscriptionKeyParts[1], subscriptionKeyParts[2],
                subscriptionKeyParts[3], string.Empty, string.Empty);

            if (subscriptionEntity != null)
            {
                var notificationModel = new NotificationModel
                {
                    ClientId = subscriptionEntity.ClientId,
                    AccountId = subscriptionEntity.AccountId,
                    CustomerId = subscriptionEntity.CustomerId,
                    ServiceContractId = subscriptionEntity.ServiceContractId,
                    Insight = subscriptionEntity.InsightTypeName,
                    ProgramName = subscriptionEntity.ProgramName,
                    NotifiedDateTime = Convert.ToDateTime(subscriptionKeyParts[6])
                };

                if (emailEventEnum == Enums.EmailEvent.unsubscribe)
                {
                    var success = InsertorMergeForOptOut(notificationModel);
                    DeleteUnsubscribedEmailFromSendGrid(notificationModel.ClientId, emailStatsModel.email);

                    if (!success)
                        Logger.Fatal($@" Insertion failed in Notification for customKey: [{emailStatsModel.customKey}].", LogModel);
                }
                else if (emailEventEnum == Enums.EmailEvent.bounce)
                {
                    // get email request detail
                    var emailRequestDetailModel = emailRequestDetail.GetEmailRequestDetail(notificationModel.ClientId, notificationModel.AccountId, notificationModel.ProgramName,
                                                                         notificationModel.ServiceContractId, notificationModel.Insight, KsnEmail,
                                                                         notificationModel.NotifiedDateTime.ToShortDateString(), emailEventEnum.ToString());

                    if (emailRequestDetailModel == null)
                        emailRequestDetailModel = new EmailRequestDetailModel
                        {
                            ClientId = notificationModel.ClientId,
                            AccountId = notificationModel.AccountId,
                            Channel = KsnEmail,
                            Insight = notificationModel.Insight,
                            ProgramName = notificationModel.ProgramName,
                            ServiceContractId = notificationModel.ServiceContractId,
                            NotifiedDateTime = notificationModel.NotifiedDateTime.Date
                        };

                    // insert email request detail info
                    emailRequestDetailModel.ErrorMessage = emailStatsModel.reason;
                    emailRequestDetailModel.Event = Enums.EmailEvent.bounce.ToString();
                    emailRequestDetail.InsertOrMergeEmailRequestDetail(emailRequestDetailModel);
                }
                else if (emailEventEnum == Enums.EmailEvent.click)
                {
                    // get email request detail
                    var emailRequestDetailModel = emailRequestDetail.GetEmailRequestDetail(notificationModel.ClientId, notificationModel.AccountId, notificationModel.ProgramName,
                                                                         notificationModel.ServiceContractId, notificationModel.Insight, KsnEmail,
                                                                         notificationModel.NotifiedDateTime.ToShortDateString(), emailEventEnum.ToString());

                    if (emailRequestDetailModel == null)
                        emailRequestDetailModel = new EmailRequestDetailModel
                        {
                            ClientId = notificationModel.ClientId,
                            AccountId = notificationModel.AccountId,
                            Channel = KsnEmail,
                            Insight = notificationModel.Insight,
                            ProgramName = notificationModel.ProgramName,
                            ServiceContractId = notificationModel.ServiceContractId,
                            NotifiedDateTime = notificationModel.NotifiedDateTime.Date
                        };

                    // insert email request detail info
                    emailRequestDetailModel.Url = emailStatsModel.url;
                    emailRequestDetailModel.Event = $"{Enums.EmailEvent.click.ToString()}|{emailStatsModel.url}";
                    emailRequestDetail.InsertOrMergeEmailRequestDetail(emailRequestDetailModel);
                }
                else if (emailEventEnum == Enums.EmailEvent.dropped)
                {
                    // get email request detail
                    var emailRequestDetailModel = emailRequestDetail.GetEmailRequestDetail(notificationModel.ClientId, notificationModel.AccountId, notificationModel.ProgramName,
                                                                         notificationModel.ServiceContractId, notificationModel.Insight, KsnEmail,
                                                                         notificationModel.NotifiedDateTime.ToShortDateString(), emailEventEnum.ToString());

                    if (emailRequestDetailModel == null)
                        emailRequestDetailModel = new EmailRequestDetailModel
                        {
                            ClientId = notificationModel.ClientId,
                            AccountId = notificationModel.AccountId,
                            Channel = KsnEmail,
                            Insight = notificationModel.Insight,
                            ProgramName = notificationModel.ProgramName,
                            ServiceContractId = notificationModel.ServiceContractId,
                            NotifiedDateTime = notificationModel.NotifiedDateTime.Date
                        };

                    // insert email request detail info
                    emailRequestDetailModel.ErrorMessage = emailStatsModel.reason;
                    emailRequestDetailModel.Event = Enums.EmailEvent.dropped.ToString();
                    emailRequestDetail.InsertOrMergeEmailRequestDetail(emailRequestDetailModel);
                }
            }
            else
            {
                Logger.Fatal(
                $@"PartitionKey ""{customKey[0]}"" or RowKey ""{customKey[1]}"" not valid.", LogModel);
            }
        }

        /// <summary>
        /// Set opt out completed for email and insert/update subscription with same.
        /// </summary>
        /// <param name="notificationModel">model to be inserted/updated in database</param>
        /// <returns>bool</returns>
        private bool InsertorMergeForOptOut(NotificationModel notificationModel)
        {
            var subscription = _uContainer.Resolve<ISubscription>();
            var programSubscriptionModel =
                subscription.GetSubscriptionDetails(notificationModel.ClientId,
                    notificationModel.CustomerId, notificationModel.AccountId,
                    notificationModel.ProgramName, string.Empty, string.Empty);
            List<SubscriptionModel> customerSubscriptions = subscription.GetCustomerSubscriptions(notificationModel.ClientId,
                notificationModel.CustomerId, notificationModel.AccountId).ToList();
            if (programSubscriptionModel != null)
            {
                // unsubcribe all insights in subscription table for the same program
                var insightSubscriptions =
                   customerSubscriptions.FindAll(
                       s =>
                           s.ProgramName.Equals(programSubscriptionModel.ProgramName,
                               StringComparison.InvariantCultureIgnoreCase));

                foreach (var insightSubscription in insightSubscriptions)
                {
                    if (insightSubscription.IsSelected)
                    {
                        if (insightSubscription.Channel == Convert.ToString(Enums.SubscriptionChannel.EmailAndSMS))
                        {
                            insightSubscription.Channel = Convert.ToString(Enums.SubscriptionChannel.SMS);
                            insightSubscription.IsEmailOptInCompleted = false;
                        }
                        else if (insightSubscription.Channel == Convert.ToString(Enums.SubscriptionChannel.Email))
                        {
                            insightSubscription.IsSelected = false;
                            insightSubscription.IsEmailOptInCompleted = false;
                        }

                        if (!subscription.InsertSubscription(insightSubscription))
                        {
                            Logger.Fatal(
                                @" Insertion failed for subscription optout.", LogModel);
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Delete unsubscribed email address from send grid account
        /// </summary>
        /// <param name="clientId">current client id</param>
        /// <param name="email">email to be deleted</param>
        private void DeleteUnsubscribedEmailFromSendGrid(int clientId, string email)
        {
            var clientConfigFacadeManager = _uContainer.Resolve<IClientConfigFacade>();
            var setting = clientConfigFacadeManager.GetClientSettings(clientId);

            var url = "https://api.sendgrid.com/api/unsubscribes.delete.json";
            var sendGridUserName = setting.OptInEmailUserName;//WebConfigurationManager.AppSettings["SendGridUserName"];
            var sendGridPassword = setting.OptInEmailPassword;//WebConfigurationManager.AppSettings["SendGridPassword"];

            url = string.Format(url + "?email=" + email + "&api_user=" + sendGridUserName + "&api_key=" + sendGridPassword);

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.Credentials = new NetworkCredential(sendGridUserName, sendGridPassword);
            httpWebRequest.ContentType = "text/json";
            httpWebRequest.Method = "POST";

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            // ReSharper disable once AssignNullToNotNullAttribute
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                streamReader.ReadToEnd();
            }
        }
    }
}
