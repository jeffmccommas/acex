﻿using CE.AO.Logging;
using CE.AO.Models;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml.Serialization;
using AO.BusinessContracts;
using AO.Registrar;
using WebGrease.Css.Extensions;
using Enums = CE.AO.Utilities.Enums;

namespace CE.AO.NotificationStatsWebAPI.Controllers
{
    /// <summary>
    /// TrumpiaStatsController class is used to get/post response. Then according to event update the subscription table.
    /// </summary>
    public class TrumpiaStatsController : ApiController
    {
        private readonly IUnityContainer _uContainer = new UnityContainer();

        public TrumpiaStatsController() : this(new LogModel { Module = Enums.Module.NotificationStats, Source = "Trumpia" })
        {
        }

        public TrumpiaStatsController(LogModel logModel)
        {
            (new DataStorageRegistrar()).Initialize<TransientLifetimeManager>(_uContainer);

            LogModel = logModel;
            LogModel.Module = Enums.Module.NotificationStats;
            LogModel.Source = "Trumpia";
        }

        public LogModel LogModel { get; set; }

        /// <summary>
        /// Get and post the response from/to trumpia
        /// </summary>
        /// <returns>HttpResponseMessage</returns>
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage PostStatistics()
        {
            var response = Request.CreateResponse(HttpStatusCode.OK);
            return response;
        }

        /// <summary>
        /// Get the sms statistics and update subscription
        /// </summary>
        /// <returns>HttpResponseMessage</returns>
        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage GetStatistics()
        {
            //var req = Request.InputStream;
            //req.Seek(0, SeekOrigin.Begin);
            //string json = new StreamReader(req).ReadToEnd();

            var json = string.Empty;
            if (Request.Method == HttpMethod.Get)
            {
                Dictionary<string, string> dictionary = Request.GetQueryNameValuePairs().ToDictionary(x => x.Key, x => x.Value);
                KeyValuePair<string, string> xmlKeyValue = dictionary.FirstOrDefault(k => k.Key == "xml");
                if (!string.IsNullOrWhiteSpace(Convert.ToString(xmlKeyValue.Value)))
                {
                    var xml = xmlKeyValue.Value;
                    dictionary.ForEach(pair =>
                    {
                        json = json + $"{pair.Key} {pair.Value} ";
                    });

                    var deserializer = new XmlSerializer(typeof(TrumpiaInboundMessageModel));
                    var strReader = new StringReader(xml);
                    var trumpiaInboundMessageModel = (TrumpiaInboundMessageModel)deserializer.Deserialize(strReader);
                    if (trumpiaInboundMessageModel != null)
                    {
                        var trumpiaKeywordConfiguration = _uContainer.Resolve<ITrumpiaKeywordConfiguration>(new ParameterOverride("logModel", LogModel));
                        var trumpiaKeywordConfigurationModel = trumpiaKeywordConfiguration.GetTrumpiaKeywordConfiguration(trumpiaInboundMessageModel.KEYWORD);
                        if (trumpiaKeywordConfigurationModel != null)
                        {
                            string[] contents = trumpiaInboundMessageModel.CONTENTS.Split(' ');
                            if (contents.Length > 0 && contents.Length <= 2)
                            {
                                var action = Convert.ToString(contents[0]);
                                var accountId = contents.Length > 1 ? Convert.ToString(contents[1]) : null;

                                var customerMobileNumberLookup = _uContainer.Resolve<ICustomerMobileNumberLookup>(
                               new ParameterOverride("logModel", LogModel));
                                var customerMobileNumberLookupModel = customerMobileNumberLookup
                                    .GetAllCustomerMobileNumberLookup(
                                        Convert.ToInt32(trumpiaKeywordConfigurationModel.ClientId),
                                        Convert.ToString(trumpiaInboundMessageModel.PHONENUMBER)).FirstOrDefault();

                                if (!string.IsNullOrWhiteSpace(customerMobileNumberLookupModel?.CustomerId))
                                {
                                    var clientConfigFacadeManager = _uContainer.Resolve<IClientConfigFacade>();
                                    var clientSettings = clientConfigFacadeManager.GetClientSettings(Convert.ToInt32(trumpiaKeywordConfigurationModel.ClientId));
                                    if (clientSettings != null)
                                    {
                                        var program =
                                            clientSettings.Programs.FirstOrDefault(
                                                p => p.TrumpiaKeyword == trumpiaInboundMessageModel.KEYWORD);
                                        var isProgram = program != null;
                                        //InsightSettings insight = null;
                                        //var insightProgram =
                                        //    clientSettings.Programs.FirstOrDefault(
                                        //        p => p.Insights.Any(i => i.TrumpiaKeyword == trumpiaInboundMessageModel.KEYWORD));
                                        //if (insightProgram != null)
                                        //{
                                        //    insight =
                                        //        insightProgram.Insights.FirstOrDefault(
                                        //            i => i.TrumpiaKeyword == trumpiaInboundMessageModel.KEYWORD);
                                        //}
                                        //var isInsight = insight != null;

                                        var subscription =
                                            _uContainer.Resolve<ISubscription>(
                                                new ParameterOverride("logModel", LogModel));
                                        if (isProgram)
                                        {
                                            ProgramLevelSubscriptionActions(action, accountId, customerMobileNumberLookupModel.CustomerId, clientSettings,
                                                trumpiaInboundMessageModel, program,
                                                subscription);
                                        }
                                        //else if (isInsight)
                                        //{
                                        //    InsightLevelSubscriptionActions(action, accountId, clientSettings, subscription,
                                        //        trumpiaInboundMessageModel, customerMobileNumberLookupModel, insightProgram,
                                        //        insight);
                                        //}
                                        else
                                        {
                                            Logger.Fatal($@"Keyword is not configured for any program or insight. Keyword: ""{trumpiaInboundMessageModel.KEYWORD}""", LogModel);
                                        }
                                    }
                                    else
                                    {
                                        Logger.Fatal("Client Settings are not configured for this client. Cannot proceed futher.", LogModel);
                                    }
                                }
                                else
                                {
                                    Logger.Fatal($"Customer is not registered for Mobile Number: {trumpiaInboundMessageModel.PHONENUMBER}.", LogModel);
                                }
                            }
                            else
                            {
                                //var programOrInsightMessage = isProgram
                                //    ? $@"Program Name: ""{program.ProgramName}"""
                                //    : isInsight ? $@"Program Name: ""{insightProgram.UtilityProgramName}"". Insight Name: ""{insight.UtilityInsightName}"" ." : "";
                                //Logger.Fatal($@"SMS is not valid. {programOrInsightMessage}Contents: ""{trumpiaInboundMessageModel.CONTENTS}"". Keyword: ""{trumpiaInboundMessageModel.KEYWORD}""", LogModel);
                                Logger.Fatal(
                                    $@"SMS is not valid. Mobile Number: ""{trumpiaInboundMessageModel.PHONENUMBER
                                        }"". Contents: ""{trumpiaInboundMessageModel.CONTENTS}"". Keyword: ""{
                                        trumpiaInboundMessageModel.KEYWORD}""", LogModel);
                            }
                        }
                        else
                        {
                            Logger.Fatal($@"Trumpia Keyword Configuration not found for Keyword ""{trumpiaInboundMessageModel.KEYWORD}"".", LogModel);
                        }
                    }
                }
            }
            //else
            //{
            //    json = Request.Content.ReadAsStringAsync().Result;
            //}

            //var sms = new SmsStatsEntity
            //{
            //    Response = "MobileSubscriptions " + json,
            //    PartitionKey = Guid.NewGuid().ToString(),
            //    RowKey = Guid.NewGuid().ToString()
            //};
            //InsertOrMergeAsync(sms, "SmsStats");

            var response = Request.CreateResponse(HttpStatusCode.OK);
            return response;
        }

        ////We are doing program level opt out no insight level
        //private void InsightLevelSubscriptionActions(string action, string accountId, ClientSettings clientSettings,
        //    ISubscription subscription, TrumpiaInboundMessageModel trumpiaInboundMessageModel,
        //    CustomerMobileNumberLookupModel customerMobileNumberLookupModel, ProgramSettings insightProgram,
        //    InsightSettings insight)
        //{
        //    if (action == "STOP")
        //    {
        //        var subscriptionEntity = subscription.GetSubscriptionDetails(clientSettings.ClientId,
        //                    customerMobileNumberLookupModel.CustomerId, accountId,
        //                    insightProgram.UtilityProgramName, insight.UtilityInsightName);
        //        if (subscriptionEntity == null)
        //        {
        //            subscriptionEntity = new SubscriptionEntity
        //            {
        //                ClientId = clientSettings.ClientId,
        //                CustomerId = customerMobileNumberLookupModel.CustomerId,
        //                AccountId = accountId,
        //                ProgramName = insightProgram.UtilityProgramName,
        //                InsightTypeName = insight.UtilityInsightName,
        //                ServiceContractId = "",
        //                IsSelected = false,
        //                Channel = Convert.ToString(Utilities.Enums.SubscriptionChannel.SMS)
        //            };
        //            var billing = _uContainer.Resolve<IBilling>("LogEnableBilling", new ParameterOverride("logModel", LogModel));
        //            var bills = billing.GetAllServicesFromLastBill(clientSettings.ClientId,
        //                customerMobileNumberLookupModel.CustomerId, accountId).ToList();
        //            if (bills.Any())
        //            {
        //                var commodityEnum = Enum.Parse(typeof(Utilities.Enums.CommodityType), insightProgram.CommodityType);
        //                var bill = bills.FirstOrDefault(
        //                    b => b.CommodityId == Convert.ToInt32(commodityEnum));
        //                if (bill != null)
        //                {
        //                    subscriptionEntity.ServiceContractId = bill.ServiceContractId;
        //                    subscriptionEntity.IsSelected = false;
        //                    var model =
        //                        Mapper.Map<SubscriptionEntity, SubscriptionModel>(
        //                            subscriptionEntity);
        //                    subscription.InsertSubscription(model);
        //                }
        //                else
        //                {
        //                    Logger.Fatal(
        //                        $@"Subscription not found. Mobile Number: ""{
        //                            trumpiaInboundMessageModel.PHONENUMBER
        //                            }"". Contents: ""{trumpiaInboundMessageModel.CONTENTS
        //                            }"". Keyword: ""{
        //                            trumpiaInboundMessageModel.KEYWORD}""", LogModel);
        //                }
        //            }
        //            else
        //            {
        //                Logger.Fatal(
        //                    $@"Subscription not found. Mobile Number: ""{
        //                        trumpiaInboundMessageModel.PHONENUMBER
        //                        }"". Contents: ""{trumpiaInboundMessageModel.CONTENTS
        //                        }"". Keyword: ""{
        //                        trumpiaInboundMessageModel.KEYWORD}""", LogModel);
        //            }
        //        }
        //        else
        //        {
        //            subscriptionEntity.IsSelected = false;
        //            var model =
        //                Mapper.Map<SubscriptionEntity, SubscriptionModel>(
        //                    subscriptionEntity);
        //            subscription.InsertSubscription(model);
        //        }
        //    }
        //    else
        //    {
        //        Logger.Fatal(
        //                $@"Action not supported. Mobile Number: ""{
        //                    trumpiaInboundMessageModel.PHONENUMBER
        //                    }"". Contents: ""{trumpiaInboundMessageModel.CONTENTS
        //                    }"". Keyword: ""{
        //                    trumpiaInboundMessageModel.KEYWORD}""", LogModel);
        //    }
        //}

        /// <summary>
        /// Program level opt out for sms
        /// </summary>
        /// <param name="action">action to take i.e STOP or CONFIRM</param>
        /// <param name="accountId">accountId for which opt out </param>
        /// <param name="customerId">customerId for which opt out </param>
        /// <param name="clientSettings">used to provide client id</param>
        /// <param name="trumpiaInboundMessageModel">to provide trumpia details</param>
        /// <param name="program">program for which opt out </param>
        /// <param name="subscription">subscription to be inserted/updated to database</param>
        private void ProgramLevelSubscriptionActions(string action, string accountId, string customerId, ClientSettings clientSettings,
            TrumpiaInboundMessageModel trumpiaInboundMessageModel, ProgramSettings program, ISubscription subscription)
        {
            switch (action)
            {
                //case "START":
                //    {
                //        var model = new SubscriptionModel
                //        {
                //            ClientId = clientSettings.ClientId,
                //            CustomerId = customerMobileNumberLookupModel.CustomerId,
                //            AccountId = accountId,
                //            ProgramName = program.UtilityProgramName,
                //            IsEmailDoubleOptInCompleted = true,
                //            IsSmsDoubleOptInCompleted = true,
                //            IsSelected = true
                //        };
                //        subscription.InsertSubscription(model);
                //        break;
                //    }
                case "STOP":
                    {
                        //Get all Accounts
                        var iPremise = _uContainer.Resolve<IPremise>(
                            new ParameterOverride("logModel", LogModel));
                        var billing = _uContainer.Resolve<IBilling>(new ParameterOverride("logModel", LogModel));
                        var iCustomerMobileNumberLookup = _uContainer.Resolve<ICustomerMobileNumberLookup>(
                               new ParameterOverride("logModel", LogModel));
                        IEnumerable<PremiseModel> premises = iPremise.GetPremises(clientSettings.ClientId, customerId);

                        foreach (var premise in premises)
                        {
                            List<BillingModel> bills = billing.GetAllServicesFromLastBill(clientSettings.ClientId,
                                customerId, premise.AccountId).ToList();
                            var customerMobileNumberLookupModel =
                                iCustomerMobileNumberLookup.GetCustomerMobileNumberLookup(clientSettings.ClientId,
                                Convert.ToString(trumpiaInboundMessageModel.PHONENUMBER), premise.AccountId);
                            if (bills.Any() && customerMobileNumberLookupModel != null)
                            {
                                    var programSubscriptionEntity =
                                        subscription.GetSubscriptionDetails(clientSettings.ClientId,
                                            customerMobileNumberLookupModel.CustomerId, premise.AccountId,
                                        program.UtilityProgramName, null, null);
                                    if (programSubscriptionEntity != null)
                                    {
                                        foreach (var insight in program.Insights)
                                        {
                                        //var commodityEnum = Enum.Parse(typeof(Utilities.Enums.CommodityType), insight.CommodityType);
                                        //var sameCommodityBills = bills.Where(
                                        //    b => b.CommodityId == Convert.ToInt32(commodityEnum)).ToList();

                                        string[] comodities = insight.CommodityType.Split(',');
                                        var sameCommodityBills = new List<BillingModel>();
                                        foreach (var commodity in comodities)
                                        {
                                            var commodityEnum = Enum.Parse(typeof(Enums.CommodityType), commodity);
                                            sameCommodityBills.AddRange(bills.Where(
                                            b => b.CommodityId == Convert.ToInt32(commodityEnum)).ToList());
                                        }

                                        foreach (var bill in sameCommodityBills)
                                        {
                                            var insightSubscriptionModel =
                                                subscription.GetSubscriptionDetails(clientSettings.ClientId,
                                                customerMobileNumberLookupModel.CustomerId, premise.AccountId,
                                                    program.UtilityProgramName, bill.ServiceContractId,
                                                    insight.UtilityInsightName);
                                            if (insightSubscriptionModel != null)
                                            {
                                                if (insightSubscriptionModel.IsSelected)
                                                {
                                                    if (insightSubscriptionModel.Channel == Convert.ToString(Enums.SubscriptionChannel.EmailAndSMS))
                                                        insightSubscriptionModel.Channel = Convert.ToString(Enums.SubscriptionChannel.Email);
                                                    else if (insightSubscriptionModel.Channel == Convert.ToString(Enums.SubscriptionChannel.SMS))
                                                        insightSubscriptionModel.IsSelected = false;
                                                    }
                                                }
                                            else
                                            {
                                                if (insight.DefaultCommunicationChannel.Contains(
                                                    Convert.ToString(Enums.SubscriptionChannel.EmailAndSMS))
                                                    &&
                                                    (programSubscriptionEntity.Channel ==
                                                     Convert.ToString(Enums.SubscriptionChannel.EmailAndSMS) ||
                                                     string.IsNullOrWhiteSpace(programSubscriptionEntity.Channel)))
                                                {
                                                    insightSubscriptionModel = new SubscriptionModel
                                                    {
                                                        ClientId = clientSettings.ClientId,
                                                        CustomerId = customerMobileNumberLookupModel.CustomerId,
                                                        AccountId = premise.AccountId,
                                                        ProgramName = program.UtilityProgramName,
                                                        InsightTypeName = insight.UtilityInsightName,
                                                        ServiceContractId = bill.ServiceContractId,
                                                        IsSelected = true,
                                                        Channel = Convert.ToString(Enums.SubscriptionChannel.Email)
                                                    };
                                                }
                                                else if (insight.DefaultCommunicationChannel.Contains(
                                                    Convert.ToString(Enums.SubscriptionChannel.SMS))
                                                    &&
                                                    (programSubscriptionEntity.Channel ==
                                                     Convert.ToString(Enums.SubscriptionChannel.SMS) ||
                                                     string.IsNullOrWhiteSpace(programSubscriptionEntity.Channel)))
                                                    insightSubscriptionModel = new SubscriptionModel
                                                    {
                                                        ClientId = clientSettings.ClientId,
                                                        CustomerId = customerMobileNumberLookupModel.CustomerId,
                                                        AccountId = premise.AccountId,
                                                        ProgramName = program.UtilityProgramName,
                                                        InsightTypeName = insight.UtilityInsightName,
                                                        ServiceContractId = bill.ServiceContractId,
                                                        IsSelected = false,
                                                        Channel = Convert.ToString(Enums.SubscriptionChannel.SMS)
                                                    };
                                            }

                                            if (insightSubscriptionModel != null)
                                            {
                                                subscription.InsertSubscription(insightSubscriptionModel);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        break;
                    }
                case "CONFIRM":
                    {
                        var subscriptionModel = subscription.GetSubscriptionDetails(clientSettings.ClientId,
                            customerId, accountId,
                            program.UtilityProgramName, null, null);
                        if (subscriptionModel != null)
                        {
                            subscriptionModel.IsSmsOptInCompleted = true;
                            subscription.InsertSubscription(subscriptionModel);
                        }
                        else
                        {
                            Logger.Fatal(
                                $@"Subscription not found. Mobile Number: ""{
                                    trumpiaInboundMessageModel.PHONENUMBER
                                    }"". Contents: ""{trumpiaInboundMessageModel.CONTENTS
                                    }"". Keyword: ""{
                                    trumpiaInboundMessageModel.KEYWORD}""", LogModel);
                        }
                        break;
                    }
                default:
                    {
                        Logger.Fatal(
                                $@"Action not supported. Mobile Number: ""{
                                    trumpiaInboundMessageModel.PHONENUMBER
                                    }"". Contents: ""{trumpiaInboundMessageModel.CONTENTS
                                    }"". Keyword: ""{
                                    trumpiaInboundMessageModel.KEYWORD}""", LogModel);
                        break;
                    }
            }
        }

        //private void InsertOrMergeAsync<T>(T tableModel, string tableName)
        //{
        //    var connectionString = ConfigurationManager.AppSettings.Get("AzureStorageConnectionString");
        //    var storageAccount = CloudStorageAccount.Parse(connectionString);
        //    var tableClient = storageAccount.CreateCloudTableClient();

        //    var tableBatchOperation = new TableBatchOperation();
        //    var table = tableClient.GetTableReference(tableName);

        //    tableBatchOperation.InsertOrMerge(tableModel as ITableEntity);

        //    table.ExecuteBatch(tableBatchOperation);
        //}
    }
}
