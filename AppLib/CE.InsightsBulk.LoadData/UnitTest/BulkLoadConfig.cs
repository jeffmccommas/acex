﻿using CE.InsightsBulk.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml;
using System.IO;
using System.Reflection;

namespace UnitTest
{
    class BulkLoadConfig : IConfig
    {
        private string _fileOutputExtension;
        const string XML_CUSTOMER_FILESEARCHPATTERN = "CustomerFileSearchPattern";
        const string XML_CUSTOMER_FILEPATH = "CustomerFilePath";
        const string XML_BILL_FILESEARCHPATTERN = "BillFileSearchPattern";
        const string XML_BILL_FILEPATH = "BillFilePath";
        const string XML_CUSTOMER_ARCHIVE_PATH = "CustomerArchivePath";
        const string XML_BILL_ARCHIVE_PATH = "BillArchivePath";
        const string XML_PROFILE_FILESEARCHPATTERN = "ProfileFileSearchPattern";
        const string XML_PROFILE_FILEPATH = "ProfileFilePath";
        const string XML_PROFILE_ARCHIVE_PATH = "ProfileArchivePath";
        const string XML_ACTIONITEM_FILESEARCHPATTERN = "ActionItemFileSearchPattern";
        const string XML_ACTIONITEM_FILEPATH = "ActionItemFilePath";
        const string XML_ACTIONITEM_ARCHIVE_PATH = "ActionItemArchivePath";
        const string XML_ACTIONITEM_XSD = "ActionItemXSD";
        const string XML_FILE_OUTPUT_EXTENSION = "FileOutputExtension";
        const string CONFIG_ENVIRONMENT = "Environment";
        const string MAX_BATCHSIZE = "MaxBatchSize";
        const string XML_PROFILE_XSD = "ProfileXSD";
        const string XML_CUSTOMER_XSD = "CustomerXSD";
        const string XML_BILLING_XSD = "BillingXSD";
        const string xmlRoot = "BulkLoad";

        private string _generalErrorLog;

        public string GeneralErrorLog
        {
            get { return _generalErrorLog; }
            set { _generalErrorLog = value; }
        }
    
        public string FileOutputExtension
        {
            get { return _fileOutputExtension; }
            set { _fileOutputExtension = value; }
        }

        private string _inputFilePath;

        public string InputFilePath
        {
            get { return _inputFilePath; }
            set { _inputFilePath = value; }
        }
        private string _archivePath;

        public string ArchivePath
        {
            get { return _archivePath; }
            set { _archivePath = value; }
        }

        private string _fileSearchPattern;

        public string FileSearchPattern
        {
            get { return _fileSearchPattern; }
            set { _fileSearchPattern = value; }
        }

        private string _fileWorkingExtension;

        public string FileWorkingExtension
        {
            get { return _fileWorkingExtension; }
            set { _fileWorkingExtension = value; }
        }
        private int _maxBatchSize;

        public int MaxBatchSize
        {
            get { return _maxBatchSize; }
            set { _maxBatchSize = value; }
        }

        private string _fileDoneExtension;

        public string FileDoneExtension
        {
            get { return _fileDoneExtension; }
            set { _fileDoneExtension = value; }
        }

        private string _inputXSD;

        public string InputXSD
        {
            get { return _inputXSD; }
            set { _inputXSD = value; }
        }

        private string _fileErrorExtension;

        public string FileErrorExtension
        {
            get { return _fileErrorExtension; }
            set { _fileErrorExtension = value; }
        }
        private string _fileErrorLog;

        public string FileErrorLog
        {
            get { return _fileErrorLog; }
            set { _fileErrorLog = value; }
        }
        private bool _importSpecificLoggingOn;

        public bool ImportSpecificLoggingOn
        {
            get { return _importSpecificLoggingOn; }
            set { _importSpecificLoggingOn = value; }
        }
        private string configFileName = "TestConfig_87.xml";

        public void LoadBillingConfiguration()
        {
            XDocument xdoc = XDocument.Load(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\testdata\\" + configFileName);
            _inputFilePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\testdata\\";
            _fileSearchPattern = "87bill_12012011110000.xml";
            _fileOutputExtension = (from x in xdoc.Elements(xmlRoot)
                                    select x.Element(XML_FILE_OUTPUT_EXTENSION).Value).ToList().FirstOrDefault();
            _archivePath = (from x in xdoc.Elements(xmlRoot)
                            select x.Element(XML_BILL_ARCHIVE_PATH).Value).ToList().FirstOrDefault();
            _inputXSD = (from x in xdoc.Elements(xmlRoot)
                         select x.Element(XML_BILLING_XSD).Value).ToList().FirstOrDefault();

        }

        public void LoadCustomerConfiguration()
        {

            XDocument xdoc = XDocument.Load(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\testdata\\" + configFileName);
            _inputFilePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\testdata\\" ;

            _fileSearchPattern = "87customer_12012011110000.xml";
            _fileOutputExtension = (from x in xdoc.Elements(xmlRoot)
                                    select x.Element(XML_FILE_OUTPUT_EXTENSION).Value).ToList().FirstOrDefault();
            _archivePath = (from x in xdoc.Elements(xmlRoot)
                            select x.Element(XML_CUSTOMER_ARCHIVE_PATH).Value).ToList().FirstOrDefault();
            _inputXSD = (from x in xdoc.Elements(xmlRoot)
                         select x.Element(XML_CUSTOMER_XSD).Value).ToList().FirstOrDefault();
        }

        public void LoadProfileConfiguration()
        {

            XDocument xdoc = XDocument.Load(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\testdata\\" + configFileName);
            _inputFilePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\testdata\\";
            _fileSearchPattern = "87profile_12012011110000.xml";
            _fileOutputExtension = (from x in xdoc.Elements(xmlRoot)
                                    select x.Element(XML_FILE_OUTPUT_EXTENSION).Value).ToList().FirstOrDefault();
            _archivePath = (from x in xdoc.Elements(xmlRoot)
                            select x.Element(XML_PROFILE_ARCHIVE_PATH).Value).ToList().FirstOrDefault();
            _inputXSD = (from x in xdoc.Elements(xmlRoot)
                         select x.Element(XML_PROFILE_XSD).Value).ToList().FirstOrDefault();
        }

        public void LoadActionItemConfiguration()
        {

            XDocument xdoc = XDocument.Load(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\testdata\\" + configFileName);
            _inputFilePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\testdata\\";
            _fileSearchPattern = "87ActionItem_12012011110000.xml";
            _fileOutputExtension = (from x in xdoc.Elements(xmlRoot)
                                    select x.Element(XML_FILE_OUTPUT_EXTENSION).Value).ToList().FirstOrDefault();
            _archivePath = (from x in xdoc.Elements(xmlRoot)
                            select x.Element(XML_ACTIONITEM_ARCHIVE_PATH).Value).ToList().FirstOrDefault();
            _inputXSD = (from x in xdoc.Elements(xmlRoot)
                         select x.Element(XML_ACTIONITEM_XSD).Value).ToList().FirstOrDefault();
        }


        
    }
}
