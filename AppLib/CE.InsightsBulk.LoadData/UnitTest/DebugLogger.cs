﻿using CE.InsightsBulk.Infrastructure.Interfaces;
using NLog;
using NLog.Targets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTest
{
    public class DebugLogger : ILogger
    {
        private static Logger _logger = LogManager.GetCurrentClassLogger();
        public string trackingId;
        private const string propTrackingId = "TrackingId";
        private const string propClientId = "ClientId";
        public int clientId = 0;


        public DebugLogger(string logFileName, string trackingId, int clientId)
        {
                FileTarget target = LogManager.Configuration.FindTargetByName("textFile") as FileTarget;
                target.FileName = logFileName;
                _trackingId = trackingId;
                _clientId = clientId;
                 
            
	     }
        private string _trackingId;

        public string TrackingId
        {
            get { return _trackingId; }
            set { _trackingId = value; }
        }

        private int _clientId;

        public int ClientId
        {
            get { return _clientId; }
            set { _clientId = value; }
        }
        public void Info(string trackingid, string message)
        {
         //   throw new NotImplementedException();
        }

        public void Warn(string trackingid, string message)
        {
           // throw new NotImplementedException();
        }

        public void Debug(string trackingid, string message)
        {
          //  throw new NotImplementedException();
        }

        public void Error(string trackingid, string message)
        {
           // throw new NotImplementedException();
        }

        public void ErrorException(string trackingid, Exception ex)
        {
           // throw new NotImplementedException();
        }

        public void Fatal(string trackingid, string message)
        {
            //throw new NotImplementedException();
        }
        public void SetName(string logFileName)
        {
            FileTarget target = LogManager.Configuration.FindTargetByName("textFile") as FileTarget;
            target.FileName = logFileName;

        }

        private void LogEvent(LogLevel logLevel, string message)
        {
            LogEventInfo theEvent = new LogEventInfo(logLevel, "", message);
            theEvent.Properties[propTrackingId] = _trackingId;
            theEvent.Properties[propClientId] = _clientId;
            _logger.Log(theEvent);
        }

        private void LogEventException(LogLevel logLevel, Exception ex)
        {
            LogEventInfo theEvent = new LogEventInfo(logLevel, "", ex.ToString());
            theEvent.Exception = ex;
            theEvent.Properties[propTrackingId] = _trackingId;
            theEvent.Properties[propClientId] = _clientId;
            _logger.Log(theEvent);
        }

        //private void LogEventException(LogLevel logLevel, Exception ex,string format, params object[] args)
        //{
        //    LogEventInfo theEvent = new LogEventInfo(logLevel, "", ex.ToString());
        //    theEvent.Exception = ex;
        //    theEvent.Properties[propTrackingId] = _trackingId;
        //    theEvent.Message = ex.ToString();
        //    _logger.Log(theEvent);
        //}

        
    }
}
