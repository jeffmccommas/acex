﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CE.InsightsBulk.Infrastructure.SharedClasses;
using CE.InsightsBulk.Infrastructure.Interfaces;
using CE.InsightsBulk.LoadData;
using System.IO;
using Aclara.UFx.StatusManagement;

namespace UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        private TestContext testContextInstance;

        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }

        }


        [TestMethod()]
        public void ExtractElementCustomerId()
        {
            //string cust = @"<Account xmlns=""Aclara:Insights"" AccountId=""115318410""><Premise xmlns=""Aclara:Insights"" PremiseId=""115318400"" PremiseStreet1=""69542 CAMINO BUENAVIDA"" PremiseCity=""CATHEDRAL CTY""";
            string cust = @"PremiseId=""12355""";
            string result = Helpers.ExtractElement(cust,"PremiseId");

            Assert.AreEqual("12355", result);

        }
                [TestMethod()]
        public void ExtractElementCustomerIdNoQuotes()
        {
            //string cust = @"<Account xmlns=""Aclara:Insights"" AccountId=""115318410""><Premise xmlns=""Aclara:Insights"" PremiseId=""115318400"" PremiseStreet1=""69542 CAMINO BUENAVIDA"" PremiseCity=""CATHEDRAL CTY""";
            string cust = @"PremiseId=12355";
            string result = Helpers.ExtractElement(cust, "PremiseId");

            Assert.AreEqual("12355", result);

        }

        [TestMethod()]
        public void TestIsETLRunningTrue()
        {
            CE.InsightsBulk.Infrastructure.MetaDataAccess da = new CE.InsightsBulk.Infrastructure.MetaDataAccess();
            bool result;
            result = da.IsETLRunning(87);
            Assert.IsTrue(result);


        }
        [TestMethod()]
        public void TestIsETLRunningFalse()
        {
            CE.InsightsBulk.Infrastructure.MetaDataAccess da = new CE.InsightsBulk.Infrastructure.MetaDataAccess();
            bool result;
            result = da.IsETLRunning(87);
            Assert.IsFalse(result);


        }
        [TestMethod()]
        [DeploymentItem("UnitTest\\TestData\\TestAllFileTypes.csv")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\TestAllFileTypes.csv", "TestAllFileTypes#csv", DataAccessMethod.Sequential)]
        public void RunCustomer()
        {
            try
            {

         
            Options o = new Options();
            int clientId = Convert.ToInt32(TestContext.DataRow["ClientId"].ToString());
            string customerFileName = TestContext.DataRow["CustomerFileName"].ToString();
            o.ClientId = clientId;
            o.FileType = "cust";
            IConfig config;
            ILogger log;

            log = new DebugLogger("debugrun.txt","1",clientId);
            config = new BulkLoadConfig();
            config.LoadCustomerConfiguration();
            IBulkFile bulkFile = new CustomerInputFile(o, config,log);
            FileInfo file = new FileInfo(config.InputFilePath + customerFileName);
            StatusList statusList = new StatusList();
            string trackingid = bulkFile.ProcessFile(file,out statusList);
            if (statusList.HasStatusSeverity(StatusTypes.StatusSeverity.Error))
                Assert.Fail();
            }
            catch (Exception)
            {

                Assert.Fail();
            }
          
        }
        [TestMethod()]
        [DeploymentItem("UnitTest\\TestData\\TestActionItemTypes.csv")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\TestActionItemTypes.csv", "TestActionItemTypes#csv", DataAccessMethod.Sequential)]
        public void RunActionItem()
        {
            try
            {


                Options o = new Options();
                int clientId = Convert.ToInt32(TestContext.DataRow["ClientId"].ToString());
                string customerFileName = TestContext.DataRow["CustomerFileName"].ToString();
                o.ClientId = clientId;
                o.FileType = "act";
                IConfig config;
                ILogger log;

                log = new DebugLogger("debugrun.txt", "1", clientId);
                config = new BulkLoadConfig();
                config.LoadActionItemConfiguration();
                IBulkFile bulkFile = new ActionItemFile(o, config, log);
                FileInfo file = new FileInfo(config.InputFilePath + customerFileName);
                StatusList statusList = new StatusList();
                string trackingid = bulkFile.ProcessFile(file, out statusList);
                if (statusList.HasStatusSeverity(StatusTypes.StatusSeverity.Error))
                    Assert.Fail();
            }
            catch (Exception)
            {

                Assert.Fail();
            }

        }
        public void RunOneCustomer()
        {
            try
            {


                Options o = new Options();
                int clientId = 87;
                string customerFileName = "87cust_12012011110000.xml";
                o.ClientId = clientId;
                o.FileType = "cust";
                IConfig config;
                ILogger log;

                log = new DebugLogger("debugrun.txt", "1", clientId);
                config = new BulkLoadConfig();
                config.LoadCustomerConfiguration();
                IBulkFile bulkFile = new CustomerInputFile(o, config, log);
                FileInfo file = new FileInfo(config.InputFilePath + customerFileName);
                StatusList statusList = new StatusList();
                string trackingid = bulkFile.ProcessFile(file, out statusList);
                if (statusList.HasStatusSeverity(StatusTypes.StatusSeverity.Error))
                    Assert.Fail();
            }
            catch (Exception)
            {

                Assert.Fail();
            }

        }
        public void RunBilling87()
        {
            try
            {


                Options o = new Options();
                o.ClientId = 87;
                o.FileType = "bill";
                IConfig config;
                ILogger log;
                log = new DebugLogger("debugrun.txt", "1", 87);
                config = new BulkLoadConfig();
                config.LoadBillingConfiguration();
                IBulkFile bulkFile = new BillInputFile(o, config, log);
                FileInfo file = new FileInfo(config.InputFilePath + config.FileSearchPattern);
                StatusList statusList = new StatusList();
                string trackingid = bulkFile.ProcessFile(file, out statusList);
                if (statusList.HasStatusSeverity(StatusTypes.StatusSeverity.Error))
                    Assert.Fail();
            }
            catch (Exception)
            {

                Assert.Fail();
            }

        }
               [TestMethod]
               public void RunProfile87()
               {
                   try
                   {


                       Options o = new Options();
                       o.ClientId = 87;
                       o.FileType = "prof";
                       IConfig config;
                       ILogger log;
                       log = new DebugLogger("debugrun.txt", "1", 87);
                       config = new BulkLoadConfig();
                       config.LoadProfileConfiguration();
                       IBulkFile bulkFile = new ProfileInputFile(o, config, log);
                       FileInfo file = new FileInfo(config.InputFilePath + config.FileSearchPattern);
                       StatusList statusList = new StatusList();
                       string trackingid = bulkFile.ProcessFile(file, out statusList);
                       if (statusList.HasStatusSeverity(StatusTypes.StatusSeverity.Error))
                           Assert.Fail();
                   }
                   catch (Exception)
                   {

                       Assert.Fail();
                   }

               }
    }
}
