﻿using Aclara.UFx.StatusManagement;
using CE.InsightsBulk.Infrastructure;
using CE.InsightsBulk.Infrastructure.DataAccess;
using CE.InsightsBulk.Infrastructure.Interfaces;
using CE.InsightsBulk.Infrastructure.SharedClasses;
using CE.InsightsBulk.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;

namespace CE.InsightsBulk.LoadData
{
    public class CustomerInputFile : IBulkFile
    {

        private readonly int _clientId;
        private readonly IConfig _config;
        private readonly ILogger _log;

        private const string NODE_CustomerId = "CustomerId";
        private const string NODE_City = "City";
        private const string NODE_Street2 = "Street2";
        private const string NODE_Street1 = "Street1";
        private const string NODE_State = "State";
        private const string NODE_MobileNumber = "MobilePhoneNumber";
        private const string NODE_PhoneNumber = "PhoneNumber";
        private const string NODE_LastName = "LastName";
        private const string NODE_IsBusiness = "IsBusiness";
        private const string NODE_IsAuthenticated = "IsAuthenticated";
        private const string NODE_FirstName = "FirstName";
        private const string NODE_EmailAddress = "EmailAddress";
        private const string NODE_Country = "Country";
        private const string NODE_AlternateEmailAddress = "AlternateEmailAddress";
        private const string NODE_Source = "Source";
        private const string NODE_PostalCode = "PostalCode";
        private const string ACCOUNTNODE_AccountId = "AccountId";
        private const string PREMISENODE_PremiseId = "PremiseId";
        private const string PREMISENODE_Street2 = "PremiseStreet2";
        private const string PREMISENODE_Street1 = "PremiseStreet1";
        private const string PREMISENODE_City = "PremiseCity";
        private const string PREMISENODE_State = "PremiseState";
        private const string PREMISENODE_PostalCode = "PremisePostalCode";
        private const string PREMISENODE_Latitude = "Latitude";
        private const string PREMISENODE_Longitude = "Longitude";
        private const string PREMISENODE_HasGasService = "HasGasService";
        private const string PREMISENODE_HasElectricService = "HasElectricService";
        private const string PREMISENODE_HasWaterService = "HasWaterService";
        private const string PREMISENODE_Country = "PremiseCountry";


        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="o"></param>
        /// <param name="config"></param>
        /// <param name="log"></param>
        public CustomerInputFile(Options o, IConfig config, ILogger log)
        {
            _clientId = Convert.ToInt32(o.ClientId);
            _config = config;
            _log = log;
        }

        /// <summary>
        /// Process the Customer file
        /// </summary>
        /// <param name="theFile">The file to be processed</param>
        /// <param name="statusList">The Status</param>
        /// <returns>The tracking id</returns>
        public string ProcessFile(FileInfo theFile, out StatusList statusList)
        {
           string trackingid  = "";
            statusList = new StatusList();
            var nodeCounter = 0;
            var totalNodeCounter = 0;
            var processedNodeCounter = 0;
            var customerTable = new List<Customer>();
            var premiseTable = new List<Premise>();
            var da = new BulkDataAccess();
            var daMeta = new MetaDataAccess();

            try
            {
                statusList.Add(new Status(DateTime.Now.ToString(CultureInfo.InvariantCulture) +  " Processing File " + theFile.FullName, StatusTypes.StatusSeverity.Informational));
                _log.Info(trackingid, $"Processing File{theFile.FullName} ");

                DateTime trackingIdDate;
                Helpers.SetTrackingData(theFile.Name, out trackingid, out trackingIdDate);

                var xsdFile = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\" + _config.InputXSD;
                var schemas = new XmlSchemaSet();
                schemas.Add(Constants.FileNamespace, XmlReader.Create(new StreamReader(xsdFile)));

                // This logic sets-up a custom iteration using linq and yield.  Each element is traversed forward-only, greatly saving resources.
                var getCustomerElements =InputFileXMLStream.StreamData(theFile.FullName).Select(el => el);
                
                foreach (var element in getCustomerElements)
                {
                    //Validate the node
                    var errors = false;
                    var errStatusList = new StatusList();
                    var doc = new XDocument(new XElement(Constants.RootNode));
                    var xElement = doc.Element(Constants.RootNode);
                    xElement?.Add(element);

                    doc.Validate(schemas, (o, e) =>
                    {
                        errStatusList.Add (new Status(e.Message,StatusTypes.StatusSeverity.Error));

                        string elementData = string.Concat(element.Elements());

                        daMeta.LogErrorsByInputType(Helpers.ExtractElements(elementData, BulkLoadConstants.RunType.Customer, _clientId, trackingid), e.Message, BulkLoadConstants.RunType.Customer,_config.ImportSpecificLoggingOn);

                        _log.ErrorException(trackingid, new InsightsBulkLoadException(
                            $"Validation Error for file {theFile.Name} data: {elementData}",e.Exception));

                        errors = true;

                    }, true);


                    if (nodeCounter == 0)
                    {
                        //Create the datatable for storage
                         customerTable = new List<Customer>();
                         premiseTable = new List<Premise>();
                    }

                    //Parse the tracking id out of the file name.  If we don't have one, then use a datetime stamp
                    if (!errors)
                    {
                        nodeCounter++;
                        var xmlNode = element.GetXmlNode();
                        ProcessNode(trackingid, trackingIdDate, xmlNode, customerTable, premiseTable);
                    }
                    else
                    {
                        statusList.AddRange(errStatusList);
                        errors = false;
                        errStatusList.Clear();
                    }

                    totalNodeCounter++;
                    if (customerTable.Count >= _config.MaxBatchSize)
                    {
                        processedNodeCounter += nodeCounter;
                        nodeCounter = 0;

                        var results = da.WriteCustomerData(customerTable, premiseTable, ref statusList);

                        if (results != customerTable.Count)
                        { 
                            statusList.Add(new Status("Not all data was inserted", StatusTypes.StatusSeverity.Error));
                            _log.ErrorException(trackingid, new InsightsBulkLoadException("Not all data was inserted"));
                        }

                        customerTable.Clear();
                        premiseTable.Clear();
                    }
                }

                if (nodeCounter > 0 )
                {
                    //Less than max rows, so write it out
                    processedNodeCounter += nodeCounter;
                    int results =da.WriteCustomerData(customerTable, premiseTable, ref statusList);

                    if (results != customerTable.Count)
                    { 
                        statusList.Add(new Status("Not all data was inserted", StatusTypes.StatusSeverity.Error));
                        _log.ErrorException(trackingid, new InsightsBulkLoadException("Not all data was inserted"));
                    }
                }
                    
                statusList.Add(new Status(DateTime.Now.ToString(CultureInfo.InvariantCulture) + " Processed " + processedNodeCounter + " customers out of " + totalNodeCounter, StatusTypes.StatusSeverity.Informational));
                _log.Info(trackingid, $"Processed {processedNodeCounter} customers out of {totalNodeCounter}");

            }
            catch (Exception ex)
            {
                statusList.Add(new Status(ex, StatusTypes.StatusSeverity.Error));
                _log.ErrorException(trackingid, ex);
            }
            
            return trackingid;

        }

        /// <summary>
        /// Process the XML Node
        /// </summary>
        /// <param name="trackingid">The tracking Id</param>
        /// <param name="trackingdate">The tracking date</param>
        /// <param name="rootNode">The xml node</param>
        /// <param name="customerTable">The customer data table</param>
        /// <param name="premiseTable">The premise data table</param>
        private void ProcessNode(string trackingid, DateTime trackingdate, XmlNode rootNode, 
            ICollection<Customer> customerTable ,   ICollection<Premise> premiseTable)
        {
            //For some reason, customer is coming in as a child node, so just get all children
            var cNode = rootNode.FirstChild;
            if (!cNode.HasChildNodes)
            {
                return;
            }

            var cust = new Customer
            {
                City = Helpers.GetText(cNode, NODE_City),
                CustomerID = Helpers.GetText(cNode, NODE_CustomerId),
                Street2 = Helpers.GetText(cNode, NODE_Street2),
                Street1 = Helpers.GetText(cNode, NODE_Street1),
                State = Helpers.GetText(cNode, NODE_State),
                postalcode = Helpers.GetText(cNode, NODE_PostalCode),
                ClientID = _clientId,
                MobilePhoneNumber = Helpers.GetText(cNode, NODE_MobileNumber),
                PhoneNumber = Helpers.GetText(cNode, NODE_PhoneNumber),
                LastName = Helpers.GetText(cNode, NODE_LastName),
                FirstName = Helpers.GetText(cNode, NODE_FirstName),
                EmailAddress = Helpers.GetText(cNode, NODE_EmailAddress),
                Country = Helpers.GetText(cNode, NODE_Country),
                AlternateEmailAddress = Helpers.GetText(cNode, NODE_AlternateEmailAddress),
                TrackingId = trackingid,
                TrackingDate = trackingdate,
                SourceId = Helpers.ConvertSourceToId(Helpers.GetText(cNode, NODE_Source))
            };

            bool flag;
            if (bool.TryParse(Helpers.GetText(cNode, NODE_IsBusiness), out flag))
            {
                cust.IsBusiness = flag;
            }
            else
            {
                cust.IsBusiness = null;
            }

            if (bool.TryParse(Helpers.GetText(cNode, NODE_IsAuthenticated), out flag))
            {
                cust.IsAuthenticated = flag;
            }
            else
            {
                cust.IsAuthenticated = null;
            }

            customerTable.Add(cust);

            //Load the Premise Table
            foreach (XmlElement accountNode in cNode.ChildNodes)
            {
                if (!accountNode.HasChildNodes)
                {
                    continue;
                }

                foreach (XmlElement premiseNode in accountNode.ChildNodes)
                {
                    var premise = new Premise
                    {
                        City = Helpers.GetText(premiseNode, PREMISENODE_City),
                        ClientID = _clientId,
                        Country = Helpers.GetText(premiseNode, PREMISENODE_Country),
                        CustomerID = cust.CustomerID,
                        AccountID = Helpers.GetText(accountNode, ACCOUNTNODE_AccountId),

                        ElectricService =
                            Convert.ToInt32(
                                Convert.ToBoolean(Helpers.GetText(premiseNode, PREMISENODE_HasElectricService))),

                        GasService =
                            Convert.ToInt32(Convert.ToBoolean(Helpers.GetText(premiseNode, PREMISENODE_HasGasService))),

                        WaterService =
                            Convert.ToInt32(Convert.ToBoolean(Helpers.GetText(premiseNode, PREMISENODE_HasWaterService))),

                        postalcode = Helpers.GetText(premiseNode, PREMISENODE_PostalCode),
                        PremiseID = Helpers.GetText(premiseNode, PREMISENODE_PremiseId),

                        SourceId = cust.SourceId,

                        State = Helpers.GetText(premiseNode, PREMISENODE_State),
                        Street1 = Helpers.GetText(premiseNode, PREMISENODE_Street1),
                        Street2 = Helpers.GetText(premiseNode, PREMISENODE_Street2),

                        Latitude =
                            Helpers.GetText(premiseNode, PREMISENODE_Latitude) == string.Empty
                                ? (double?) null
                                : Convert.ToDouble(Helpers.GetText(premiseNode, PREMISENODE_Latitude)),

                        Longitude =
                            Helpers.GetText(premiseNode, PREMISENODE_Longitude) == string.Empty
                                ? (double?) null
                                : Convert.ToDouble(Helpers.GetText(premiseNode, PREMISENODE_Longitude)),

                        TrackingDate = trackingdate,
                        TrackingID = trackingid
                    };

                    premiseTable.Add(premise);
                }
            }
        }
    }
}
