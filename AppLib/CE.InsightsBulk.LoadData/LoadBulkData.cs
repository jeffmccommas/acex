﻿using System;
using System.Globalization;
using System.Linq;
using CommandLine;
using System.IO;
using CE.InsightsBulk.Infrastructure.Interfaces;
using Aclara.UFx.StatusManagement;
using CE.InsightsBulk.Infrastructure.NLog;

namespace CE.InsightsBulk.LoadData
{
    internal class LoadBulkData
    {
        /// <summary>
        /// Main 
        /// </summary>
        /// <param name="args"></param>
        private static void Main(string[] args)
        {
            var options = new Options();
            var parser = new Parser(with => with.HelpWriter = Console.Error);

            if (parser.ParseArgumentsStrict(args, options, () => Environment.Exit(-2)))
            {
                options.Verbose = true;
                Run(options);
            }
        }

        /// <summary>
        /// Run
        /// </summary>
        /// <param name="options">The program options</param>
        private static void Run(Options options)
        {
            var currrentlyWorkingFileName = string.Empty;
            var originalFileName = string.Empty;
            var logName = string.Empty;

            IConfig config = null;
            var statusList = new StatusList();
            BulkLoadLogger bulkLoadLogger = null;

            try
            {
                if (options.Verbose)
                {
                    Console.WriteLine("CreateConfig" + DateTime.Now.ToString(CultureInfo.InvariantCulture));
                }

                //Loop through all the files that we have
                if (options.Verbose)
                {
                    Console.WriteLine("starting" + DateTime.Now.ToString(CultureInfo.InvariantCulture));
                }

                //Check to see if ETL is running.  If it is, then terminate 
                var cf = new BulkProcessingClassFactory();
                var md = cf.CreateMetaDataAccess();
                config = cf.CreateConfig(options);

                var isEtlRunning = !options.Ignore && md.IsETLRunning(options.ClientId, options.Verbose);

                if (isEtlRunning)
                {
                    md.LogDBEvent($"etl is currently running.  No processing will occur for client {options.ClientId}",
                        options.ClientId);

                    if (options.Verbose)
                    {
                        Console.WriteLine("etl is currently running.  No processing will occur");
                    }
                }
                else
                {
                    if (options.Verbose)
                    {
                        Console.WriteLine("Searching " + config.InputFilePath);
                    }

                    var dir = new DirectoryInfo(config.InputFilePath);
                    foreach (
                        var file in
                        dir.GetFiles(config.FileSearchPattern, SearchOption.TopDirectoryOnly).OrderBy(f => f.Name))
                    {
                        originalFileName = file.FullName;
                        logName = Path.ChangeExtension(originalFileName, config.FileOutputExtension);

                        var trackingId = Helpers.GetTrackingId(file.Name);
                        if (options.Verbose)
                        {
                            Console.WriteLine("Tracking Id is " + trackingId);
                        }

                        if (bulkLoadLogger == null)
                        {
                            bulkLoadLogger = new BulkLoadLogger(logName, trackingId, options.ClientId);
                        }
                        else
                        {
                            bulkLoadLogger.SetName(logName);
                            bulkLoadLogger.ClientId = options.ClientId;
                            bulkLoadLogger.TrackingId = trackingId;
                        }

                        var loader = cf.CreateBulkFileLoader(options, config, bulkLoadLogger);

                        statusList = new StatusList();

                        currrentlyWorkingFileName = MarkForProcessing(file, config.FileWorkingExtension);

                        trackingId = loader.ProcessFile(file, out statusList);

                        statusList.Add(
                            new Status(
                                DateTime.Now.ToString(CultureInfo.InvariantCulture) + " Processing File " +
                                file.FullName + " complete",
                                StatusTypes.StatusSeverity.Informational));

                        bulkLoadLogger.Info(trackingId, $"File {file.FullName} complete");

                        //Mark file as done and move it to archive
                        if (statusList.HasStatusSeverity(StatusTypes.StatusSeverity.Error))
                        {
                            bulkLoadLogger.Error(trackingId,
                                statusList.GetStatusListAsExceptionString(StatusTypes.StatusSeverity.Error));

                            MarkForError(originalFileName, currrentlyWorkingFileName, logName);
                        }
                        else
                        {
                            MarkForArchival(file, config.ArchivePath, config.FileDoneExtension);
                        }

                        currrentlyWorkingFileName = string.Empty;
                        originalFileName = string.Empty;
                        logName = string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                if (options.Verbose)
                {
                    Console.WriteLine("exception : " + ex.Message + " => " + ex.StackTrace);
                }

                statusList.Add(new Status(DateTime.Now.ToString(CultureInfo.InvariantCulture) + ex.Message,
                    StatusTypes.StatusSeverity.Error));

                if (config != null)
                {
                    Helpers.WriteLogFile(DateTime.Now, options.ClientId, "exception " + ex.Message,
                        config.GeneralErrorLog);
                }

                MarkForError(originalFileName, currrentlyWorkingFileName, logName);

                if (ex is System.Configuration.ConfigurationErrorsException)
                {
                    Environment.Exit(3);
                }

                Environment.Exit(2);
            }

            // Finish
            if (options.Verbose)
            {
                Console.WriteLine("ending" + DateTime.Now.ToString(CultureInfo.InvariantCulture));
            }
        }

        /// <summary>
        /// Mark the file as being processed
        /// </summary>
        /// <param name="file">The filename</param>
        /// <param name="workingExtension">The extension that marks it as being processed</param>
        private static string MarkForProcessing(FileInfo file, string workingExtension)
        {
            //Mark file for processing
            var processingName = Path.ChangeExtension(file.FullName, workingExtension);

            if (File.Exists(processingName))
            {
                File.Delete(processingName);
            }

            file.MoveTo(processingName);

            return processingName;

        }

        /// <summary>
        /// Mark the file for archival
        /// </summary>
        /// <param name="file">The filename</param>
        /// <param name="archivePath">The path for archiving</param>
        /// <param name="doneExtension">The extension that marks the file as done.</param>
        private static void MarkForArchival(FileInfo file, string archivePath, string doneExtension)
        {
            var archiveName = archivePath + Path.ChangeExtension(file.Name, doneExtension);

            if (File.Exists(archiveName))
            {
                File.Delete(archiveName);
            }

            file.MoveTo(archiveName);

        }


        /// <summary>
        /// Mark the file as being processed
        /// </summary>
        /// <param name="fileName">The data  filename</param>
        /// <param name="currentlyWorkingFileName"></param>
        /// <param name="logFileName">The log filename</param>
        private static void MarkForError(string fileName, string currentlyWorkingFileName, string logFileName)
        { 
            //Mark file for processing
            var errorFileName = fileName + ".ERROR";

            if (File.Exists(errorFileName))
            {
                File.Delete(errorFileName);
            }

            File.Move(currentlyWorkingFileName, errorFileName);

            if (!string.IsNullOrEmpty(logFileName))
            {
                var errorLogFileName = logFileName + ".ERROR";
                if (File.Exists(errorLogFileName))
                {
                    File.Delete(errorLogFileName);
                }

                File.Move(logFileName, errorLogFileName);
            }
        }
    }
}
