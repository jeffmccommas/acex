﻿using System.Text;
using CommandLine;

namespace CE.InsightsBulk.LoadData
{
    public class Options
    {
        [Option("c", HelpText = "ClientID Id", Required = true)]
        public int ClientId { get; set; }

        [Option("t", HelpText = "File Type: cust = customer, bill = billing,prof = profile,act=actionItem",
            Required = true)]
        public string FileType { get; set; }


        [Option("v", HelpText = "Print details during execution.")]
        public bool Verbose { get; set; }

        [Option("i", HelpText = "Ignore ETL Execution.")]
        public bool Ignore { get; set; }

        [HelpOption]
        public string GetUsage()
        {
            // this without using CommandLine.Text
            var usage = new StringBuilder();
            usage.AppendLine("Load Customer Bulk Data usage:  LoadCustomer -c87 -tcust ");
            return usage.ToString();
        }
    }
}
