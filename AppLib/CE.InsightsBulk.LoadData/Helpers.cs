﻿using Aclara.UFx.StatusManagement;
using CE.InsightsBulk.Infrastructure.SharedClasses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;

namespace CE.InsightsBulk.LoadData
{
    public class Helpers
    {
        private const string Dateformat = "yyyyMMddHHmmss";

        //Create a logfile
        public static void CreateLogFile(StatusList sList, string fileName)
        {
            // delete the last error file if it exists
            if (File.Exists(fileName))
            {
                File.Delete(fileName);
            }

            // Create new log file
            using (var sw = File.CreateText(fileName))
            {
                foreach (var sItem in sList)
                {
                    var message = "";
                    if (!string.IsNullOrEmpty(sItem.StatusDesc))
                    {
                        message = sItem.StatusDesc;
                    }
                    else
                    {
                        if (sItem.Exception != null)
                        {
                            message = sItem.Exception.Message;
                        }
                    }
                    sw.WriteLine(message);
                }
            }
        }

        /// <summary>
        /// Converts Source to SourceID type
        /// </summary>
        /// <param name="theSource">The source</param>
        /// <returns>A SourceID type</returns>
        public static int ConvertSourceToId(string theSource)
        {
            int convertedSource;
            switch (theSource)
            {
                case "aclara":
                    convertedSource = 1;
                    break;
                case "auditor":
                    convertedSource = 2;
                    break;
                case "csr":
                    convertedSource = 3;
                    break;
                case "customer":
                    convertedSource = 4;
                    break;
                case "customermobile":
                    convertedSource = 5;
                    break;
                case "customerweb":
                    convertedSource = 6;
                    break;
                case "thirdparty":
                    convertedSource = 7;
                    break;
                case "utility":
                    convertedSource = 8;
                    break;
                default:
                    convertedSource = -1;
                    break;
            }
            return convertedSource;
        }

        /// <summary>
        /// Converts Status to StatusID type
        /// </summary>
        /// <param name="theStatus">The status</param>
        /// <returns>A StatusID type</returns>
        public static int ConvertStatusToId(string theStatus)
        {
            int convertedStatus;
            switch (theStatus)
            {
                case "cancelled":
                    convertedStatus = 1;
                    break;
                case "canceled":
                    convertedStatus = 1;
                    break;
                case "completed":
                    convertedStatus = 2;
                    break;
                case "considering":
                    convertedStatus = 3;
                    break;
                case "presented":
                    convertedStatus = 4;
                    break;
                case "rejected":
                    convertedStatus = 5;
                    break;
                case "selected":
                    convertedStatus = 6;
                    break;
                default:
                    convertedStatus = -1;
                    break;
            }
            return convertedStatus;
        }

        /// <summary>
        /// Get the value of an attribute on an XML node.
        /// </summary>
        /// <param name="xnode">The XML node</param>
        /// <param name="attrName">The attribute name</param>
        /// <param name="returnNull"></param>
        /// <returns>The value</returns>
        public static string GetText(XmlNode xnode, string attrName, bool returnNull = false)
        {
            var returnVal = string.Empty;
            if (returnNull)
            {
                returnVal = null;
            }

            if (xnode.Attributes == null)
            {
                return returnVal;
            }

            return xnode.Attributes[attrName]?.Value ?? returnVal;
        }

        /// <summary>
        /// Get the value of an attribute on an XML node.
        /// </summary>
        /// <param name="xnode">The XML node</param>
        /// <param name="attrName">The attribute name</param>
        /// <returns>The value</returns>
        public static DateTime? GetDate(XmlNode xnode, string attrName)
        {
            var valueStr = GetText(xnode, attrName);

            if (string.IsNullOrEmpty(valueStr))
            {
                return null;
            }

            return Convert.ToDateTime(valueStr);
        }

        /// <summary>
        /// Set the tracking data
        /// </summary>
        /// <param name="fileName">The file being processed</param>
        /// <param name="trackingid">The tracking Id</param>
        /// <param name="trackingIdDate">The tracking is date</param>
        public static void SetTrackingData(string fileName, out string trackingid, out DateTime trackingIdDate)
        {
            trackingid = GetTrackingId(fileName);
            var trackingdate = DateTime.ParseExact(trackingid.Substring(0, 14), Dateformat, CultureInfo.InvariantCulture);
            trackingIdDate = trackingdate.ToUniversalTime();
        }

        /// <summary>
        /// Get the tracking Id from the file being processed
        /// </summary>
        /// <param name="fileName">The name of the file being processed.</param>
        /// <returns>The tracking Id</returns>
        public static string GetTrackingId(string fileName)
        {
            string trackingId;
            var trackingIdDate = DateTime.Now;
            var filenameParts = fileName.Split('_', '.');

            if (filenameParts.Length <= 1)
            {
                trackingId = trackingIdDate.ToString(Dateformat);
            }
            else
            {
                DateTime trackingIdDateTest;
                trackingId = DateTime.TryParseExact(filenameParts[filenameParts.Count() -1], Dateformat, null, DateTimeStyles.None, out trackingIdDateTest) ? 
                    filenameParts[filenameParts.Count() - 1] :
                    trackingIdDate.ToString(Dateformat);
            }

            return trackingId;
        }

        /// <summary>
        /// Write to the log file. Create if needed.
        /// </summary>
        /// <param name="errorTime">The time of the error</param>
        /// <param name="clientId">The clientId</param>
        /// <param name="message">The error message</param>
        /// <param name="fileName">The log file name</param>
        public static void WriteLogFile(DateTime errorTime, int clientId, string message, string fileName)
        {
            if (File.Exists(fileName))
            {
                using (var sw = new StreamWriter(fileName, true))
                {
                    sw.WriteLine($"{errorTime},{clientId},{message}");
                }
            }
            else
            {
                using (var sw = File.CreateText(fileName))
                {
                    sw.WriteLine($"{errorTime},{clientId},{message}");
                }
            }
        }

        /// <summary>
        /// Extract the data out of the XML elements and put it into a dictionary
        /// </summary>
        /// <param name="xmlnode"></param>
        /// <param name="importType"></param>
        /// <param name="clientId"></param>
        /// <param name="trackingId"></param>
        /// <returns></returns>
        public static Dictionary<string, string> ExtractElements(string xmlnode, BulkLoadConstants.RunType importType, int clientId, string trackingId)
        {
            // Create the dictionary
            var dict = new Dictionary<string, string>
            {
                {BulkLoadConstants.CUSTOMERID, ExtractElement(xmlnode, BulkLoadConstants.CUSTOMERID)},
                {BulkLoadConstants.PREMISEID, ExtractElement(xmlnode, BulkLoadConstants.PREMISEID)},
                {BulkLoadConstants.ACCOUNTID, ExtractElement(xmlnode, BulkLoadConstants.ACCOUNTID)},
                {BulkLoadConstants.TRACKINGID, trackingId},
                {BulkLoadConstants.CLIENTID, clientId.ToString()}
            };

            // Add specfic import type data
            switch (importType)
            {
                case BulkLoadConstants.RunType.Customer:
                    break;
                case BulkLoadConstants.RunType.Bill:
                    dict.Add(BulkLoadConstants.SERVICEPOINTID, ExtractElement(xmlnode, BulkLoadConstants.SERVICEPOINTID));
                    dict.Add(BulkLoadConstants.STARTDATE, ExtractElement(xmlnode, BulkLoadConstants.STARTDATE));
                    dict.Add(BulkLoadConstants.ENDDATE, ExtractElement(xmlnode, BulkLoadConstants.ENDDATE));
                    break;
                case BulkLoadConstants.RunType.Profile:
                    dict.Add(BulkLoadConstants.ATTRIBUTEKEY, ExtractElement(xmlnode, BulkLoadConstants.ATTRIBUTEKEY));
                    dict.Add(BulkLoadConstants.ATTRIBUTEVALUE, ExtractElement(xmlnode, BulkLoadConstants.ATTRIBUTEVALUE));
                    break;
                case BulkLoadConstants.RunType.ActionItem:
                    dict.Add(BulkLoadConstants.ACTIONKEY, ExtractElement(xmlnode, BulkLoadConstants.ACTIONKEY));
                    break;
            }
            return dict;
        }

        /// <summary>
        /// Extract an attribute value from an XML node
        /// </summary>
        /// <param name="xmlnode">The XML node</param>
        /// <param name="searchString">The attribute</param>
        /// <returns>The value</returns>
        public static string ExtractElement(string xmlnode, string searchString)
        {
            var retstring = "Unknown";
            searchString = searchString + "=" +  "(?:[\"']?([^\"'>=]*)[\"']?)";

           var matches = Regex.Matches(xmlnode,searchString );

            if (matches.Count <= 0)
            {
                return retstring;
            }

            retstring = matches[0].Value;
            retstring = retstring.Replace("\"", "");
            var values = retstring.Split('=');

            if (values.Length> 0)
            {
                retstring = values[1];
            }
            return retstring;
        }
    }
   
}
