﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace CE.InsightsBulk.LoadData
{
    static public class BillInputFileXMLStream
    {


        private const string CustomerNode = "Customer";
        public static IEnumerable<XElement> StreamData(string uri)
        {
            using (XmlReader reader = XmlReader.Create(uri))
            {
                reader.MoveToContent();

                // Parsing for the Customer nodes
                while (!reader.EOF)
                {
                    if (reader.NodeType == XmlNodeType.Element && (reader.Name == CustomerNode))
                    {
                        XElement el = XElement.ReadFrom(reader) as XElement;
                        if (el != null)
                            yield return el;
                    }
                    else
                    {
                        reader.Read();
                    }

                }


            }
        }
       
    }
}

   