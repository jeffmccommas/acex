﻿using Aclara.UFx.StatusManagement;
using CE.InsightsBulk.Infrastructure;
using CE.InsightsBulk.Infrastructure.DataAccess;
using CE.InsightsBulk.Infrastructure.Interfaces;
using CE.InsightsBulk.Infrastructure.SharedClasses;
using CE.InsightsBulk.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;

namespace CE.InsightsBulk.LoadData
{

    public class BillInputFile : IBulkFile
    {
        private readonly int _clientId;
        private readonly IConfig _config;
        private readonly ILogger _log;

        private const string BillingNode_Customerid = "CustomerId";
        private const string BillingNode_Accountid = "AccountId";
        private const string BillingNode_PremiseId = "PremiseId";
        private const string BillingNode_BillDate = "StartDate";
        private const string BillingNode_BillEndDate = "EndDate";
        private const string BillingNode_TotalUnits = "TotalUsage";
        private const string BillingNode_TotalCost = "TotalCost";
        private const string BillingNode_CommodityId = "Commodity";
        private const string BillingNode_BillPeriodTypeId = "BillPeriodType";
        private const string BillingNode_BillCycleScheduleId = "BillCycleScheduleId";
        private const string BillingNode_Source = "Source";
        private const string BillingNode_UOM = "UOM";
        private const string BillingNode_ServicePointId = "ServicePointId";
        private const string BillingNode_ServiceContractId = "ServiceContractId";
        private const string BillingNode_MeterId = "MeterId";
        private const string BillingNode_AMIStartDate = "AMIStartDate";
        private const string BillingNode_AMIEndDate = "AMIEndDate";
        private const string BillingNode_RateClass = "RateClass";
        private const string BillingNode_MeterType = "MeterType";
        private const string BillingNode_ReplacedMeterId = "ReplacedMeterId";
        private const string BillingNode_ReadQuality = "ReadQuality";
        private const string BillingNode_ReadDate = "ReadDate";
        private const string BillingNode_DueDate = "DueDate";

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="o"></param>
        /// <param name="config"></param>
        /// <param name="log"></param>
        public BillInputFile(Options o, IConfig config, ILogger log)
        {
            _clientId = Convert.ToInt32(o.ClientId);
            _config = config;
            _log = log;
        }

        /// <summary>
        /// Process the Billing file
        /// </summary>
        /// <param name="theFile">The file to be processed</param>
        /// <param name="statusList">The Status</param>
        /// <returns>The tracking id</returns>
        public string ProcessFile(FileInfo theFile, out StatusList statusList)
        {
            var trackingid = "";
            statusList = new StatusList();
            var nodeCounter = 0;
            var totalNodeCounter = 0;
            var processedNodeCounter = 0;
            var daMeta = new MetaDataAccess();
            var billingTable = new List<Billing>();
            var da = new BulkDataAccess();

            try
            {
                statusList.Add(new Status(DateTime.Now.ToString(CultureInfo.InvariantCulture) + " Processing File " + theFile.FullName,
                    StatusTypes.StatusSeverity.Informational));

                _log.Info(trackingid, $"Processing File{theFile.FullName} ");

                DateTime trackingIdDate;
                Helpers.SetTrackingData(theFile.Name, out trackingid, out trackingIdDate);
                var xsdFile = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\" +
                                  _config.InputXSD;

                // This logic sets-up a custom iteration using linq and yield.  Each element is traversed forward-only, greatly saving resources.
                var getCustomerElements = InputFileXMLStream.StreamData(theFile.FullName).Select(el => el);

                var schemas = new XmlSchemaSet();
                schemas.Add(Constants.FileNamespace, XmlReader.Create(new StreamReader(xsdFile)));

                foreach (var element in getCustomerElements)
                {
                    //Validate the node
                    var errors = false;
                    var errStatusList = new StatusList();

                    var doc = new XDocument(new XElement(Constants.RootNode));
                    var xElement = doc.Element(Constants.RootNode);
                    xElement?.Add(element);

                    doc.Validate(schemas, (o, e) =>
                    {
                        errStatusList.Add(new Status(e.Message, StatusTypes.StatusSeverity.Error));

                        string elementData = string.Concat(element.Elements());

                        daMeta.LogErrorsByInputType(
                            Helpers.ExtractElements(elementData, BulkLoadConstants.RunType.Bill, _clientId, trackingid),
                            e.Message, BulkLoadConstants.RunType.Bill, _config.ImportSpecificLoggingOn);

                        _log.ErrorException(trackingid,
                            new InsightsBulkLoadException(
                                $"Validation Error for file {theFile.Name} data: {elementData}",
                                e.Exception));

                        errors = true;

                    }, true);


                    if (nodeCounter == 0)
                    {
                        //Create the datatable for storage
                        billingTable = new List<Billing>();
                    }

                    //Parse the tracking id out of the file name.  If we don't have one, then use a datetime stamp
                    if (!errors)
                    {
                        nodeCounter++;
                        var xmlNode = element.GetXmlNode();
                        ProcessNode(trackingid, trackingIdDate, xmlNode, billingTable);
                    }
                    else
                    {
                        statusList.AddRange(errStatusList);
                        errors = false;
                        errStatusList.Clear();
                    }

                    totalNodeCounter++;
                    if (billingTable.Count >= _config.MaxBatchSize)
                    {
                        processedNodeCounter += nodeCounter;
                        nodeCounter = 0;

                        var results = da.WriteBillingData(billingTable, ref statusList);
                        if (results != billingTable.Count)
                        {
                            statusList.Add(new Status("Not all data was inserted", StatusTypes.StatusSeverity.Error));
                        }

                        billingTable.Clear();
                    }
                }

                if (billingTable.Count > 0)
                {
                    processedNodeCounter += nodeCounter;

                    //Less than max rows, so write it out
                    var results = da.WriteBillingData(billingTable, ref statusList);
                    if (results != billingTable.Count)
                    {
                        statusList.Add(new Status("Not all data was inserted", StatusTypes.StatusSeverity.Error));
                        _log.Error(trackingid, "Not all data was inserted");
                    }
                }

                statusList.Add(
                    new Status(DateTime.Now.ToString(CultureInfo.InvariantCulture) + " Processed " + processedNodeCounter + " customers out of " +
                        totalNodeCounter, StatusTypes.StatusSeverity.Informational));

                _log.Info(trackingid,
                    $"Processed {processedNodeCounter} customers out of {totalNodeCounter}");

            }
            catch (Exception ex)
            {
                statusList.Add(new Status(ex, StatusTypes.StatusSeverity.Error));
            }

            return trackingid;
        }


        /// <summary>
        /// Process the XML Node
        /// </summary>
        /// <param name="trackingid">The tracking Id</param>
        /// <param name="trackingdate">The tracking date</param>
        /// <param name="rootNode">The xml node</param>
        /// <param name="billingTable">The data table</param>
        private void ProcessNode(string trackingid, DateTime trackingdate, XmlNode rootNode, ICollection<Billing> billingTable)
        {
            //For some reason, customer is coming in as a child node, so just get all children
            var cNode = rootNode.FirstChild;
            if (!cNode.HasChildNodes)
            {
                return;
            }

            //Load the Billing Table
            foreach (XmlElement accountNode in cNode.ChildNodes)
            {
                if (!accountNode.HasChildNodes)
                {
                    continue;
                }

                foreach (XmlElement billNode in accountNode.ChildNodes)
                {
                    foreach (XmlElement premiseNode in billNode.ChildNodes)
                    {
                        foreach (XmlElement serviceNode in premiseNode.ChildNodes)
                        {
                            var bill = new Billing
                            {
                                ClientId = _clientId,
                                CustomerId = Helpers.GetText(cNode, BillingNode_Customerid),
                                AccountId = Helpers.GetText(accountNode, BillingNode_Accountid),
                                PremiseId = Helpers.GetText(premiseNode, BillingNode_PremiseId),
                                BillStartDate = Convert.ToDateTime(Helpers.GetText(billNode, BillingNode_BillDate)),
                                BillEndDate = Convert.ToDateTime(Helpers.GetText(billNode, BillingNode_BillEndDate)),
                                TotalUnits = Convert.ToDecimal(Helpers.GetText(serviceNode, BillingNode_TotalUnits)),
                                TotalCost = Convert.ToDecimal(Helpers.GetText(serviceNode, BillingNode_TotalCost)),
                                CommodityId = Convert.ToInt32(Helpers.GetText(serviceNode, BillingNode_CommodityId)),
                                BillPeriodTypeId = Convert.ToInt32(Helpers.GetText(billNode, BillingNode_BillPeriodTypeId)),
                                BillCycleScheduleId = Helpers.GetText(billNode, BillingNode_BillCycleScheduleId),
                                UOMId = Convert.ToInt32(Helpers.GetText(serviceNode, BillingNode_UOM)),
                                ServiceContractId = Helpers.GetText(serviceNode, BillingNode_ServiceContractId),

                                ServicePointId = Helpers.GetText(serviceNode, BillingNode_ServicePointId, true),
             
                                MeterId = Helpers.GetText(serviceNode, BillingNode_MeterId),
                                AMIStartDate = Helpers.GetDate(serviceNode, BillingNode_AMIStartDate),
                                AMIEndDate = Helpers.GetDate(serviceNode, BillingNode_AMIEndDate),
                                RateClass = Helpers.GetText(serviceNode, BillingNode_RateClass),
                                MeterType = Helpers.GetText(serviceNode, BillingNode_MeterType),
                                ReplacedMeterId = Helpers.GetText(serviceNode, BillingNode_ReplacedMeterId),
                                ReadQuality = Helpers.GetText(serviceNode, BillingNode_ReadQuality),
                                ReadDate = Helpers.GetDate(serviceNode, BillingNode_ReadDate),
                                DueDate = Helpers.GetDate(billNode, BillingNode_DueDate),
                                TrackingId = trackingid,
                                TrackingDate = trackingdate,
                                SourceId = Helpers.ConvertSourceToId(Helpers.GetText(cNode, BillingNode_Source))
                            };

                            // The bill start date must be earlier than the end date.
                            if (bill.BillEndDate.Subtract(bill.BillStartDate).Ticks < 0)
                            {
                                throw new ApplicationException("Bill Start Date greater than Bill End Date");
                            }

                            billingTable.Add(bill);
                        }
                    }
                }
            }
        }
    }
}

