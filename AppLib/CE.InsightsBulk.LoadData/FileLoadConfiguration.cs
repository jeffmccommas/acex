﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using CE.InsightsMeta.Model;
using CE.InsightsBulk.Infrastructure.DataAccess;
using System.Xml.Linq;

namespace CE.InsightsBulk.LoadData
{
    public class FileLoadConfiguration
    {

        const string FILEPATH = "FilePath";
        const string CONFIG_FILESEARCHPATTERN = "FileSearchPattern";
        const string CONFIG_FILEWORKINGEXTENSION = "FileWorkingExtension";
        const string CONFIG_FILEDONEEXTENSION = "FileDoneExtension";
        const string ComponentName = "BulkLoad";
        int _clientID;
        string _environment;    
        const string xmlRoot = "BulkLoad";
        const string XML_CUSTOMER_FILESEARCHPATTERN = "CustomerFileSearchPattern";
        const string XML_CUSTOMER_FILEPATH = "CustomerFilePath";
        const string XML_BILL_FILESEARCHPATTERN = "BillFileSearchPattern";
        const string XML_BILL_FILEPATH = "BillFilePath";
        const string XML_PROFILE_FILESEARCHPATTERN = "ProfileFileSearchPattern";
        const string XML_PROFILE_FILEPATH = "ProfileFilePath";
        const string XML_CUSTOMER_ARCHIVE_PATH = "CustomerArchivePath";
        const string XML_BILL_ARCHIVE_PATH = "BillArchivePath";
        const string XML_PROFILE_ARCHIVE_PATH = "ProfileArchivePath";
        const string XML_FILE_OUTPUT_EXTENSION = "FileOutputExtension";
        const string CONFIG_ENVIRONMENT = "Environment";
        const string MAX_BATCHSIZE = "MaxBatchSize";
        

       

    public FileLoadConfiguration(int clientID)
    {
        _clientID = clientID;
    }
    protected FileLoadConfiguration()
    {

    }
    private string _fileOutputExtension;

    public string FileOutputExtension
    {
        get { return _fileOutputExtension; }
        set { _fileOutputExtension = value; }
    }
    
    private string _customerFilePath;

	public string CustomerFilePath
	{
		get { return _customerFilePath;}
		set { _customerFilePath = value;}
	}
    private string _billFilePath;

    public string BillFilePath
    {
        get { return _billFilePath; }
        set { _billFilePath = value; }
    }
    private string _customerArchivePath;

    public string CustomerArchivePath
    {
        get { return _customerArchivePath; }
        set { _customerArchivePath = value; }
    }
    
    private string _profileFilePath;

    public string ProfileFilePath
    {
        get { return _profileFilePath; }
        set { _profileFilePath = value; }
    }

        private string _customerFileSearchPattern;

	public string CustomerFileSearchPattern
	{
		get { return _customerFileSearchPattern;}
		set { _customerFileSearchPattern = value;}
	}

    private string _billFileSearchPattern;

    public string BillFileSearchPattern
    {
        get { return _billFileSearchPattern; }
        set { _billFileSearchPattern = value; }
    }

    private string _profileFileSearchPattern;

    public string profileFileSearchPattern
    {
        get { return _profileFileSearchPattern; }
        set { _profileFileSearchPattern = value; }
    }

	private string _fileWorkingExtension;

	public string FileWorkingExtension
	{
		get { return _fileWorkingExtension;}
		set { _fileWorkingExtension = value;}
	}
        private int _maxBatchSize;

	public int MaxBatchSize
	{
		get { return _maxBatchSize;}
		set { _maxBatchSize = value;}
	}
	
	private string _fileDoneExtension;

	public string FileDoneExtension
	{
		get { return _fileDoneExtension;}
		set { _fileDoneExtension = value;}
	}
	

 
        public void LoadConfiguration()
        {
            BulkFileConfiguration config = new BulkFileConfiguration();
            string xmlConfig;
            try
            {
                //load settings from appconfig
                var appSettings = ConfigurationManager.AppSettings;
                _fileDoneExtension = appSettings[CONFIG_FILEDONEEXTENSION];
                _fileWorkingExtension = appSettings[CONFIG_FILEWORKINGEXTENSION];
                _environment = appSettings[CONFIG_ENVIRONMENT];
                _maxBatchSize = Convert.ToInt32(appSettings[MAX_BATCHSIZE]);

                    //Load Settings from file
                config.GetConfiguration(_clientID, ComponentName,_environment,out xmlConfig);
                XDocument xdoc = XDocument.Parse(xmlConfig);
                _customerFilePath = (from x in xdoc.Elements(xmlRoot)
                           select x.Element(XML_CUSTOMER_FILEPATH).Value).ToList().FirstOrDefault();
                _customerFileSearchPattern =  (from x in xdoc.Elements(xmlRoot)
                           select x.Element(XML_CUSTOMER_FILESEARCHPATTERN).Value).ToList().FirstOrDefault();
                _billFilePath = (from x in xdoc.Elements(xmlRoot)
                                     select x.Element(XML_BILL_FILEPATH).Value).ToList().FirstOrDefault();
                _billFileSearchPattern = (from x in xdoc.Elements(xmlRoot)
                                              select x.Element(XML_BILL_FILESEARCHPATTERN).Value).ToList().FirstOrDefault();
                _profileFilePath = (from x in xdoc.Elements(xmlRoot)
                                 select x.Element(XML_PROFILE_FILEPATH).Value).ToList().FirstOrDefault();
                _profileFileSearchPattern = (from x in xdoc.Elements(xmlRoot)
                                          select x.Element(XML_PROFILE_FILESEARCHPATTERN).Value).ToList().FirstOrDefault();
                _fileOutputExtension = (from x in xdoc.Elements(xmlRoot)
                                        select x.Element(XML_FILE_OUTPUT_EXTENSION).Value).ToList().FirstOrDefault();
                _customerArchivePath = (from x in xdoc.Elements(xmlRoot)
                                        select x.Element(XML_CUSTOMER_ARCHIVE_PATH).Value).ToList().FirstOrDefault();

            }
            catch (ConfigurationErrorsException)
            {
               
                throw;
            }
        }
    }
}
