﻿using Aclara.UFx.StatusManagement;
using CE.InsightsBulk.Infrastructure;
using CE.InsightsBulk.Infrastructure.DataAccess;
using CE.InsightsBulk.Infrastructure.Interfaces;
using CE.InsightsBulk.Infrastructure.SharedClasses;
using CE.InsightsBulk.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;

namespace CE.InsightsBulk.LoadData
{
    public class ProfileInputFile : IBulkFile
    {
        private readonly int _clientId;
        private readonly IConfig _config;
        private readonly ILogger _log;

        private const string PremiseNode_Customerid = "CustomerId";
        private const string PremiseNode_Accountid = "AccountId";
        private const string PremiseNode_PremiseId = "PremiseId";
        private const string AttributeNode_Source = "Source";
        private const string AttributeNode_ModifiedDate = "ModifiedDate";
        private const string AttributeNode_CreatedDate = "CreateDate";
        private const string AttributeNode_Key = "AttributeKey";
        private const string AttributeNode_Value = "AttributeValue";



        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="o"></param>
        /// <param name="config"></param>
        /// <param name="log"></param>
        public ProfileInputFile(Options o, IConfig config, ILogger log)
        {
            _clientId = Convert.ToInt32(o.ClientId);
            _config = config;
            _log = log;

        }

        /// <summary>
        /// Process the Profile file
        /// </summary>
        /// <param name="theFile">The file to be processed</param>
        /// <param name="statusList">The Status</param>
        /// <returns>The tracking id</returns>
        public string ProcessFile(FileInfo theFile, out StatusList statusList)
        {
            var trackingid = "";
            statusList = new StatusList();
            var nodeCounter = 0;
            var totalNodeCounter = 0;
            var processedNodeCounter = 0;
            var attributeTable = new List<PremiseAttribute>();
            var da = new BulkDataAccess();
            var daMeta = new MetaDataAccess();

            try
            {
                statusList.Add(new Status(DateTime.Now.ToString(CultureInfo.InvariantCulture) + " Processing File " + theFile.FullName,
                    StatusTypes.StatusSeverity.Informational));

                _log.Info(trackingid, $"Processing File{theFile.FullName} ");

                DateTime trackingIdDate;
                Helpers.SetTrackingData(theFile.Name, out trackingid, out trackingIdDate);
                var xsdFile = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\" +
                                  _config.InputXSD;


                // This logic sets-up a custom iteration using linq and yield.  Each element is traversed forward-only, greatly saving resources.
                var getCustomerElements = InputFileXMLStream.StreamData(theFile.FullName).Select(el => el);

                var schemas = new XmlSchemaSet();
                schemas.Add(Constants.FileNamespace, XmlReader.Create(new StreamReader(xsdFile)));

                foreach (var element in getCustomerElements)
                {
                    //Validate the node
                    var errors = false;

                    var errStatusList = new StatusList();

                    var doc = new XDocument(new XElement(Constants.RootNode));
                    var xElement = doc.Element(Constants.RootNode);
                    xElement?.Add(element);

                    doc.Validate(schemas, (o, e) =>
                    {
                        errStatusList.Add(new Status(e.Message, StatusTypes.StatusSeverity.Error));

                        var elementData = string.Concat(element.Elements());

                        daMeta.LogErrorsByInputType(
                            Helpers.ExtractElements(elementData, BulkLoadConstants.RunType.Profile, _clientId,
                                trackingid), e.Message, BulkLoadConstants.RunType.Profile,
                            _config.ImportSpecificLoggingOn);

                        _log.ErrorException(trackingid,
                            new InsightsBulkLoadException($"Validation Error for file {theFile.Name} data: {elementData}",
                                e.Exception));

                        errors = true;

                    }, true);


                    if (nodeCounter == 0)
                    {
                        //Create the datatable for storage
                        attributeTable = new List<PremiseAttribute>(); //TODO
                    }

                    //Parse the tracking id out of the file name.  If we don't have one, then use a datetime stamp
                    if (!errors)
                    {
                        nodeCounter++;
                        var xmlNode = element.GetXmlNode();
                        ProcessNode(trackingid, trackingIdDate, xmlNode, attributeTable);//TODO
                    }
                    else
                    {
                        statusList.AddRange(errStatusList);
                        errors = false;
                        errStatusList.Clear();

                    }
 
                    totalNodeCounter++;
                    if (attributeTable.Count >= _config.MaxBatchSize)
                    {
                        //WriteOutdata
                        processedNodeCounter += nodeCounter;
                        var results = da.WritePremiseAttributeData(attributeTable, ref statusList);//TODO
                        nodeCounter = 0;
                        if (results != attributeTable.Count)
                        {
                            statusList.Add(new Status("Not all data was inserted", StatusTypes.StatusSeverity.Error));
                        }
                        attributeTable.Clear();

                    }
                }

                if (attributeTable.Count > 0)
                {
                    processedNodeCounter += nodeCounter;

                    //Less than max rows, so write it out
                    var results = da.WritePremiseAttributeData(attributeTable, ref statusList); //TODO

                    if (results != attributeTable.Count)
                    {
                        statusList.Add(new Status("Not all data was inserted", StatusTypes.StatusSeverity.Error));
                        _log.ErrorException(trackingid, new InsightsBulkLoadException("Not all data was inserted"));
                    }
                }

                statusList.Add(
                    new Status(DateTime.Now.ToString(CultureInfo.InvariantCulture) + " Processed " + processedNodeCounter + " customers out of " +
                        totalNodeCounter, StatusTypes.StatusSeverity.Informational));

                _log.Info(trackingid,$"Processed {processedNodeCounter} customers out of {totalNodeCounter}");

            }
            catch (Exception ex)
            {
                statusList.Add(new Status(ex, StatusTypes.StatusSeverity.Error));
                _log.ErrorException(trackingid, ex);
            }

            return trackingid;
        }

        /// <summary>
        /// Process the XML Node
        /// </summary>
        /// <param name="trackingid">The tracking Id</param>
        /// <param name="trackingdate">The tracking date</param>
        /// <param name="rootNode">The xml node</param>
        /// <param name="premiseAttributeTable">The data table</param>
        private void ProcessNode(string trackingid, DateTime trackingdate, XmlNode rootNode,
            ICollection<PremiseAttribute> premiseAttributeTable)
        {
            //For some reason, customer is coming in as a child node, so just get all children
            var cNode = rootNode.FirstChild;

            //Load the Premise Attribute Table
            if (!cNode.HasChildNodes)
            {
                return;
            }

            foreach (XmlElement accountNode in cNode.ChildNodes)
            {
                if (!accountNode.HasChildNodes)
                {
                    continue;
                }

                foreach (XmlElement premiseNode in accountNode.ChildNodes)
                {
                    foreach (XmlElement attributeNode in premiseNode.ChildNodes)
                    {
                        var premiseAttribute = new PremiseAttribute
                        {
                            ClientID = _clientId,
                            CustomerID = Helpers.GetText(cNode, PremiseNode_Customerid),
                            AccountID = Helpers.GetText(accountNode, PremiseNode_Accountid),
                            PremiseID = Helpers.GetText(premiseNode, PremiseNode_PremiseId),
                            TrackingID = trackingid,
                            TrackingDate = trackingdate,
                            OptionValue = Helpers.GetText(attributeNode, AttributeNode_Value),
                            AttributeKey = Helpers.GetText(attributeNode, AttributeNode_Key),
                            UpdateDate = Convert.ToDateTime(Helpers.GetText(attributeNode, AttributeNode_ModifiedDate)),
                            SourceId = Helpers.ConvertSourceToId(Helpers.GetText(attributeNode, AttributeNode_Source))
                        };

                        var strDateTime = Helpers.GetText(attributeNode, AttributeNode_CreatedDate);
                        if (strDateTime.Length == 0)
                        {
                            premiseAttribute.CreateDate = null;
                        }
                        else
                        {
                            premiseAttribute.CreateDate = Convert.ToDateTime(strDateTime);
                        }

                        premiseAttributeTable.Add(premiseAttribute);
                    }
                }
            }
        }
    }
}
