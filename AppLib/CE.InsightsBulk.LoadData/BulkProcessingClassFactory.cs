﻿using CE.InsightsBulk.Infrastructure;
using CE.InsightsBulk.Infrastructure.Interfaces;
using CE.InsightsBulk.Infrastructure.NLog;
using System;

namespace CE.InsightsBulk.LoadData
{
    public class BulkProcessingClassFactory
    {
        private const string CustomerFileType = "cust";
        private const string BillingFileType = "bill";
        private const string ProfileFileType = "prof";
        private const string ActionItemFileType = "act";

        /// <summary>
        /// Get metadata access
        /// </summary>
        /// <returns></returns>
        public IMetaDataAccess CreateMetaDataAccess()
        {
            return new MetaDataAccess();
        }

        /// <summary>
        /// Create a BulkFileLoader
        /// </summary>
        /// <param name="options">The program options</param>
        /// <param name="config">The current config</param>
        /// <param name="log">The log file</param>
        /// <returns>A BulkFileLoader</returns>
        public IBulkFile CreateBulkFileLoader(Options options,IConfig config, BulkLoadLogger log)
        {
            IBulkFile bulkFile;

            if (config == null)
            {
                config = CreateConfig(options);
            }

            switch (options.FileType)
            {
                case CustomerFileType:
                    bulkFile = new CustomerInputFile(options, config,log);
                    break;
                case BillingFileType:
                    bulkFile =  new BillInputFile(options, config,log);
                    break;
                case ProfileFileType:
                    bulkFile = new ProfileInputFile(options, config,log);
                    break;
                case ActionItemFileType:
                    bulkFile = new ActionItemFile(options, config, log);
                    break;
                default:
                    bulkFile = new CustomerInputFile(options,config,log);
                    break;
            }
            return bulkFile;
        }

        /// <summary>
        /// Create a FileLoad Configuration
        /// </summary>
        /// <param name="options">The program options</param>
        /// <returns>The FileLoadConfiguration</returns>
        public FileLoadConfiguration CreateConfig(Options options)
        {
            try
            {
                var config = new FileLoadConfiguration(Convert.ToInt32(options.ClientId));
                switch (options.FileType)
                {
                    case CustomerFileType:
                        config.LoadCustomerConfiguration();
                        break;
                    case BillingFileType:
                        config.LoadBillingConfiguration();
                        break;
                    case ProfileFileType:
                        config.LoadProfileConfiguration();
                        break;
                    case ActionItemFileType:
                        config.LoadActionItemConfiguration();
                        break;
                }
                return config;
            }
            catch (Exception)
            {
                throw new System.Configuration.ConfigurationErrorsException(
                    "Cannot Create Configuration. Check InsightsBulkConfiguration");
            }

        }

    }
}
