﻿namespace CE.InsightsBulk.LoadData
{
    public class Constants
    {
        public const string FileNamespace = "Aclara:Insights";
        public const string RootNode = "Customers";
    }
}
